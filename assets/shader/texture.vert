#version 430
layout(location = 0) in vec3 in_pos;
layout(location = 3) in vec2 in_uv;
uniform mat4 MODEL_MATRIX;
layout(std140, binding = 0) uniform frame_data {
  mat4 VIEW_MATRIX;
  mat4 PROJECTION_MATRIX;
};
uniform mat4 projection_matrix;
out vec2 pass_uv;
void main() {
  gl_Position = projection_matrix * VIEW_MATRIX * MODEL_MATRIX * vec4(in_pos, 1.0);
  pass_uv = in_uv;
}