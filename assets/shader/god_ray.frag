#version 430
out vec4 frag_color;
in vec2 pass_uv;

uniform sampler2D depth_sampler;
uniform sampler2D shadow_sampler_0;
uniform sampler2D shadow_sampler_1;
uniform sampler2DShadow shadow_sampler_2;

uniform mat4 inv_vp_matrix;
uniform mat4 shadow_matrices[5];
uniform vec3 light_color;
uniform vec3 cam_world_pos;

vec3 calc_shadow_pos(int index, vec4 world_pos) {
  vec4 res1 = shadow_matrices[index] * world_pos;
  vec3 res = res1.xyz / res1.w;
  return res * 0.5 + 0.5;
  // return res;
}

// dithering to allow for smooth low quality god_rays
const float ditherPattern[4][4] = {{0.0f, 0.5f, 0.125f, 0.625f},
                                   {0.75f, 0.22f, 0.875f, 0.375f},
                                   {0.1875f, 0.6875f, 0.0625f, 0.5625},
                                   {0.9375f, 0.4375f, 0.8125f, 0.3125}};

void main() {
  const float depth = texture(depth_sampler, pass_uv).r;
  vec4 fragment_world_pos =
      inv_vp_matrix * vec4(pass_uv.x * 2 - 1, (pass_uv.y) * 2 - 1, depth * 2 - 1, 1.0);
  fragment_world_pos = fragment_world_pos / fragment_world_pos.w;
  const vec3 ray_vec = (fragment_world_pos.xyz - cam_world_pos.xyz);
  const float ray_vec_length = length(ray_vec);
  const vec3 direction = ray_vec / ray_vec_length;

  const int num_samples = 32;

  const float step_length = ray_vec_length / num_samples;
  const vec3 step_world = direction * step_length;
  const float dither = ditherPattern[int(mod(gl_FragCoord.x, 4))][int(mod(gl_FragCoord.y, 4))];
  const vec4 start_pos_world = vec4(cam_world_pos.xyz + step_world * dither, 1);

  vec3 pos_shadow = calc_shadow_pos(4, start_pos_world);
  const vec3 step_shadow =
      calc_shadow_pos(4, vec4(start_pos_world.xyz + step_world, 1)) - pos_shadow;
  float ray = 0;
  for (int i = 0; i < num_samples; i++) {
    ray += texture(shadow_sampler_2, pos_shadow).r;
    pos_shadow += step_shadow;
  }

  ray = ray / num_samples;
  ray *= smoothstep(0, 300, min(ray_vec_length, 300));
  ray = ray * 0.3;
  // ray = min(ray, 0.2);
  frag_color = vec4(light_color * ray, 1);
  // frag_color = vec4(1);
}
