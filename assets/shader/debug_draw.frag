#version 430
out vec4 frag_color;
in vec3 pass_color;

void main()
{
  frag_color = vec4(pass_color, 1.0f);
}
