#version 430
layout(location = 0) in vec3 in_pos;
layout(location = 3) in vec2 in_uv;

out vec2 pass_uv;

void main() {
  gl_Position = vec4(in_pos, 1.0);
  pass_uv = in_uv;
}