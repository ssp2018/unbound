#version 430
layout(location = 0) out vec4 frag_color;

in vec2 pass_uv;
in vec4 pass_color;

uniform sampler2D texture_sampler;

void main() {
    frag_color = texture(texture_sampler, pass_uv) * pass_color;
    // frag_color = vec4(1,1,0,1);

  //gamma
  float gamma = 2.2;
  frag_color.rgb = pow(frag_color.rgb, vec3(1.0/gamma));
}