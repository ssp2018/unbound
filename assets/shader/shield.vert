#version 430
layout(location = 0) in vec3 in_pos;  // the position variable has attribute position 0
layout(location = 2) in vec3 in_normal;
layout(location = 3) in vec2 in_uv;

out vec3 pass_normal;
out vec4 pass_position;
out vec2 pass_uv;

uniform mat4 MODEL_MATRIX;

layout(std140, binding = 0) uniform frame_data {
  mat4 VIEW_MATRIX;
  mat4 PROJECTION_MATRIX;
};

void main() {
  vec4 world_pos = MODEL_MATRIX * vec4(in_pos, 1.0);

  pass_position = world_pos;
  gl_Position = PROJECTION_MATRIX * VIEW_MATRIX * world_pos;
  mat3 normal_matrix = mat3(MODEL_MATRIX);
  normal_matrix = inverse(normal_matrix);
  normal_matrix = transpose(normal_matrix);
  pass_normal = normalize(normal_matrix * in_normal);
  pass_uv = in_uv;
}