#version 430
out vec4 frag_color;
in vec2 pass_uv;

uniform sampler2D color_sampler;
// uniform sampler2D ssao_sampler;
uniform sampler2D glow_sampler;
uniform sampler2D god_ray_sampler;

uniform bool god_ray_active;
uniform bool glow_active;
uniform float saturation;
uniform vec4 damage_color;
uniform vec3 god_ray_color;
uniform float radial_blur_radius;

vec4 multisample(sampler2D texture_sampler, vec2 coord) {
  // const float multisample_dirs[4] = {-1.5, -0.5, 0.5, 1.5};
  const float multisample_dirs[2] = {-0.5, 0.5};
  const vec2 radius_tex = 1.0 / textureSize(texture_sampler, 0);

  vec4 value = vec4(0, 0, 0, 0);
  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 2; j++) {
      vec2 dir = vec2(multisample_dirs[i], multisample_dirs[j]);
      value += texture(texture_sampler, coord + radius_tex * dir);
    }
  }
  return value / 4.0;
}

vec4 radial_sample(sampler2D texture_sampler, vec2 uv, float radius) {
  if (radius < 0.0001) {
    return texture(texture_sampler, uv);
  }

  vec4 res = vec4(0, 0, 0, 0);

  vec2 normal = uv * 2 - vec2(1, 1);

  if (length(normal) > 0.5) {
    normal = normal - 0.5 * normalize(normal);
    normal = vec2(abs(normal.x) * normal.x, abs(normal.y) * normal.y);
    res += texture(texture_sampler, uv - 2.0 * radius * normal) * 0.1784;
    res += texture(texture_sampler, uv - 1.0 * radius * normal) * 0.210431;

    res += texture(texture_sampler, uv) * 0.222338;

    res += texture(texture_sampler, uv + 1.0 * radius * normal) * 0.210431;
    res += texture(texture_sampler, uv + 2.0 * radius * normal) * 0.1784;

  } else {
    res = texture(texture_sampler, uv);
  }
  return res;
}

void main() {
  // frag_color.xyz = texture(color_sampler, pass_uv).xyz *  texture(ssao_sampler, pass_uv).r;
  // vec3 glow = texture(glow_sampler, pass_uv).xyz;
  // glow = pow(glow, vec3(2.2));
  // vec3 res = texture(color_sampler, pass_uv).xyz;
  vec4 res = radial_sample(color_sampler, pass_uv, radial_blur_radius);
  if (god_ray_active) {
    // res += god_ray_color * texture(god_ray_sampler, pass_uv).x;
    res += god_ray_color * multisample(god_ray_sampler, pass_uv).x;
  }
  if (glow_active) {
    res.xyz += texture(glow_sampler, pass_uv).xyz;
  }
  // res *= vec3(texture(ssao_sampler, pass_uv).xyz);

  // damage color
  float damage_value = length(pass_uv * 2 - vec2(1, 1));
  damage_value = max(damage_value - 0.5, 0);
  damage_value = damage_value * damage_value;
  vec4 v = damage_value * damage_color;
  res += v.xyz * v.a;

  float a = res.r * 0.3 + res.g * 0.59 + res.b * 0.11;
  frag_color.rgb = mix(vec3(a), res.rgb, saturation);
  frag_color.w = 1;

  // gamma
  // float gamma = 2.2;
  // frag_color.rgb = pow(frag_color.rgb, vec3(1.0/gamma));
}
