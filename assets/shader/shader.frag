#version 430

#define MAX_NUM_POINT_LIGHTS 10

layout(location = 0) out vec4 frag_color;
// layout (location = 1) out vec3 out_position;
// layout (location = 2) out vec3 out_normal;

in vec3 pass_normal;
// in vec3 pass_normal_view;
in vec4 pass_position;
// in vec4 pass_position_view;
in vec3 pass_color;
in float pass_depth;
in vec3 pass_shadow_pos[4];
// in float pass_ambient_normal_modifier;

// uniform vec3 DIRECTIONAL_LIGHT;
// uniform vec3 DIRECTIONAL_LIGHT_COLOR;
uniform sampler2D ssao_sampler;
uniform sampler2DShadow DEPTH_MAP_0;
uniform sampler2DShadow DEPTH_MAP_1;
uniform sampler2DShadow DEPTH_MAP_2;
uniform sampler2DShadow DEPTH_MAP_3;
// uniform sampler2D texture_sampler;

uniform float shadow_depths[4];

layout(binding = 1) uniform frag_frame_data {
  vec4 frustum_limits;
  vec4 bias;
  vec3 DIRECTIONAL_LIGHT_COLOR;
  vec3 DIRECTIONAL_LIGHT;
  bvec2 bools;  // x = dir_light, y = shadows
  vec3 cam_pos;
  vec2 windows_size;
  float time_passed;
};

// point lights
uniform vec3 POINT_LIGHT_POSITION[MAX_NUM_POINT_LIGHTS];
uniform vec3 POINT_LIGHT_COLOR[MAX_NUM_POINT_LIGHTS];
uniform float POINT_LIGHT_RADIUS[MAX_NUM_POINT_LIGHTS];
uniform int POINT_LIGHT_COUNT;

float shadow_sample(int index, sampler2DShadow shadow_sampler, float bias) {
  float res = 1;
  // return texture(shadow_sampler, vec3(pass_shadow_pos[index].xy, pass_shadow_pos[index].z -
  // bias)).r;
  float x, y;
  float diff = 0.0005;
  if (texture(shadow_sampler, vec3(pass_shadow_pos[index].xy, pass_shadow_pos[index].z - bias)).r <
      1) {
    res = 0;
    for (y = -0.5; y <= 0.5; y += 1.0)
      for (x = -0.5; x <= 0.5; x += 1.0)
        res += texture(shadow_sampler, vec3(pass_shadow_pos[index].xy + vec2(x * diff, y * diff),
                                            pass_shadow_pos[index].z - bias))
                   .r;
    res /= 4;
  }

  return res;
}

vec2 get_d_shadow_depth(vec3 shadow_pos_dx, vec3 shadow_pos_dy, float texture_size) {
  const vec2 shadow_texel_size = vec2(1.0 / texture_size, 1.0 / texture_size);

  mat2 shadow_to_screen_d =
      inverse(mat2(shadow_pos_dx.x, shadow_pos_dy.x, shadow_pos_dx.y, shadow_pos_dy.y));

  vec2 d_right = shadow_to_screen_d * vec2(shadow_texel_size.x, 0);
  vec2 d_up = shadow_to_screen_d * vec2(0, shadow_texel_size.y);

  vec2 d_depth = vec2(d_right.x * shadow_pos_dx.z + d_right.y * shadow_pos_dy.z,
                      d_up.x * shadow_pos_dx.z + d_up.y * shadow_pos_dy.z);

  return d_depth;
}

// float get_shadow_bias(vec3 shadow_pos_dx, vec3 shadow_pos_dy) {
//  vec2 d_depth = get_d_shadow_depth(shadow_pos_dx, shadow_pos_dy);
//  return (abs(d_depth.x) + abs(d_depth.y)) * 2.0;
//}

float multisample_shadow(sampler2DShadow depth_map, vec3 shadow_pos, vec3 shadow_pos_dx,
                         vec3 shadow_pos_dy, float radius, float slope_bias_modifier) {
  const float multisample_dirs[4] = {-1.5, -0.5, 0.5, 1.5};
  const float tex_size = textureSize(depth_map, 0).x;
  const float radius_tex = radius / tex_size;

  vec2 d_depth = abs(get_d_shadow_depth(shadow_pos_dx, shadow_pos_dy, tex_size));

  float slope_bias = (d_depth.x + d_depth.y) * slope_bias_modifier;
  // slope_bias = min(slope_bias, 0.1);
  float shadow = 0;
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      vec2 dir = vec2(multisample_dirs[i], multisample_dirs[j]);
      shadow += texture(depth_map, vec3(shadow_pos.xy + dir * radius_tex,
                                        shadow_pos.z - slope_bias /*- dot(d_depth, abs(dir))*/))
                    .r;
    }
  }
  shadow /= 16.0;
  return shadow;
}

void main() {
  float shadow = 1.0;
  vec3 result;

  vec3 shadow_pos_dx[4];
  vec3 shadow_pos_dy[4];
  for (int i = 0; i < 4; i++) {
    shadow_pos_dx[i] = dFdx(pass_shadow_pos[i]);
    shadow_pos_dy[i] = dFdy(pass_shadow_pos[i]);
  }

  const float LAST_BLEND = 40.0;
  if (bools.y) {
    vec3 projCoords;
    if (pass_depth < frustum_limits.x) {
      vec3 shadow_pos = pass_shadow_pos[0];
      shadow_pos.z -= 0.075 / shadow_depths[0];
      float radius = 2 + smoothstep(frustum_limits.x * 0.75, frustum_limits.x, pass_depth);
      shadow_pos.z -= 0.005 * (radius - 2);
      shadow_pos.z = max(shadow_pos.z, 0.0001);
      shadow = multisample_shadow(DEPTH_MAP_0, shadow_pos, shadow_pos_dx[0], shadow_pos_dy[0],
                                  radius, 0);

    } else if (pass_depth < frustum_limits.y) {
      vec3 shadow_pos = pass_shadow_pos[1];
      shadow_pos.z -= 0.2 / shadow_depths[1];
      float radius = (pass_depth - frustum_limits.x) / (frustum_limits.y - frustum_limits.x);

      radius = 0.5 + smoothstep(frustum_limits.x, frustum_limits.y * 0.8, pass_depth);
      // radius * 0.6;
      // radius = max(radius, 0.5);
      shadow_pos.z -= 0.005 * (radius - 0.5);
      shadow_pos.z = max(shadow_pos.z, 0.0001);
      shadow = multisample_shadow(DEPTH_MAP_1, shadow_pos, shadow_pos_dx[1], shadow_pos_dy[1],
                                  radius, 0);
    } else if (pass_depth < frustum_limits.z - LAST_BLEND) {
      vec3 shadow_pos = pass_shadow_pos[2];
      shadow_pos.z -= 1 / shadow_depths[2];
      float radius = (pass_depth - frustum_limits.y) / (frustum_limits.z - frustum_limits.y);
      radius = max(radius, 0.45);

      radius = 0.5 + (pass_depth - frustum_limits.y) / (frustum_limits.z - frustum_limits.y) * 2.5;
      shadow_pos.z -= 0.005 * (radius - 0.5);
      shadow_pos.z = max(shadow_pos.z, 0.0001);
      shadow = multisample_shadow(DEPTH_MAP_2, shadow_pos, shadow_pos_dx[2], shadow_pos_dy[2],
                                  radius, 0);

    } else if (pass_depth < frustum_limits.z) {
      vec3 shadow_pos = pass_shadow_pos[2];
      shadow_pos.z -= 1 / shadow_depths[2];
      float radius = (pass_depth - frustum_limits.y) / (frustum_limits.z - frustum_limits.y);
      radius = max(radius, 0.45);

      radius = 0.5 + (pass_depth - frustum_limits.y) / (frustum_limits.z - frustum_limits.y) * 2.5;
      shadow_pos.z -= 0.005 * (radius - 0.5);
      shadow_pos.z = max(shadow_pos.z, 0.0001);
      shadow = multisample_shadow(DEPTH_MAP_2, shadow_pos, shadow_pos_dx[2], shadow_pos_dy[2],
                                  radius, 0);

      shadow_pos = pass_shadow_pos[3];
      shadow_pos.z -= 15 / shadow_depths[3];
      shadow_pos.z = max(shadow_pos.z, 0.0001);

      float shadow_2 =
          multisample_shadow(DEPTH_MAP_3, shadow_pos, shadow_pos_dx[3], shadow_pos_dy[3], 0.5, 0);
      shadow = mix(shadow, shadow_2, (pass_depth - (frustum_limits.z - LAST_BLEND)) / LAST_BLEND);

    } else {
      vec3 shadow_pos = pass_shadow_pos[3];
      shadow_pos.z -= 17 / shadow_depths[3];
      shadow_pos.z = max(shadow_pos.z, 0.0001);
      shadow =
          multisample_shadow(DEPTH_MAP_3, shadow_pos, shadow_pos_dx[3], shadow_pos_dy[3], 0.5, 0);
    }
  }

  vec3 ambient_modifier =
      vec3(0.2, 0.2, 0.8) * texture(ssao_sampler, vec2(gl_FragCoord.x / windows_size.x,
                                                       gl_FragCoord.y / windows_size.y))
                                .x -
      0.1;

  result += ambient_modifier * pass_color;
  if (bools.x) {
    float diffuse_modifier =
        max(dot(normalize(pass_normal), normalize(-DIRECTIONAL_LIGHT)), 0) * shadow;
    result += diffuse_modifier * pass_color * DIRECTIONAL_LIGHT_COLOR;
  }

  float fake_global_illum_factor = max(dot(normalize(pass_normal), vec3(0, 0, -1)), 0);
  result += fake_global_illum_factor * pass_color * vec3(0, 0, 0.5);

  vec3 point_light_result = vec3(0, 0, 0);
  for (int i = 0; i < POINT_LIGHT_COUNT; i++) {
    float light_distance = length(POINT_LIGHT_POSITION[i] - pass_position.xyz);
    if (light_distance < POINT_LIGHT_RADIUS[i]) {
      // add point light
      // vec3 point_light_position = vec3(0,4,87);
      vec3 light_dir = normalize(POINT_LIGHT_POSITION[i] - pass_position.xyz);
      float diff = max(dot(pass_normal, light_dir), 0.0);

      // float attenuation = 1.0 / (( light_distance * light_distance * 1));

      const float constant = 1;
      const float linear = 3;
      const float quadratic = 1;

      float attenuation = 1.0 / (constant + linear * light_distance +
                                 quadratic * (light_distance * light_distance));

      point_light_result += POINT_LIGHT_COLOR[i] * diff * attenuation * 0.1 * POINT_LIGHT_RADIUS[i];
    }
  }

  result += point_light_result * pass_color;

  // vec3 cam_pos = vec3(50,0,80);
  vec3 pos = pass_position.xyz;
  float limit = 50;
  float falloff_distance = 2000;
  float l = abs(length(pos - cam_pos));
  float fog_res = l;
  float n = (limit - cam_pos.z) / normalize(pos - cam_pos).z;
  if (cam_pos.z > limit) {
    // fog_res = min(l - n, falloff_distance);
    fog_res = min(pos.z - limit, falloff_distance);
    // backwards rays, skip
    if (n < 0) {
      fog_res = 0;
    }
    // infinite length ray, skip
    if (l < n) {
      fog_res = 0;
    }
  } else {
    fog_res =
        min(min(pos.z - limit, 0), falloff_distance) + min(cam_pos.z - limit, falloff_distance);
  }

  float gamma = 2.2;

  // fog_res = min(fog_res, falloff_distance);
  float z_fog = (fog_res * fog_res) / falloff_distance;

  float depth_fog = (pass_depth * pass_depth * pass_depth) / 1000000000;
  const vec3 depth_fog_color = pow(vec3(178.0, 188.0, 237) / 255, vec3(gamma));
  const vec3 z_fog_color = pow(vec3(178.0, 188.0, 237) / 255, vec3(gamma));
  result = mix(result, depth_fog_color, min(depth_fog, 1));
  result = mix(result, z_fog_color, min(z_fog, 1));

  frag_color.rgb = pow(result, vec3(1.0 / gamma));
  frag_color.a = 1;

  // out_position = pass_position_view.xyz;
  // out_normal = pass_normal_view;
}
