#version 430
layout(location = 0) in vec3 in_pos;  // the position variable has attribute position 0
layout(location = 2) in vec3 in_normal;
layout(location = 3) in vec2 in_uv;
out vec3 pass_normal;
out vec2 pass_uv;
out vec4 pass_position;
out vec4 pass_shadow_pos[4];
out float pass_depth;

uniform mat4 MODEL_MATRIX;
layout(std140, binding = 0) uniform frame_data {
  mat4 VIEW_MATRIX;
  mat4 PROJECTION_MATRIX;
};
uniform mat3 NORMAL_MATRIX;
uniform mat4 LIGHT_SPACE_MATRIX[4];

void main() {
  vec4 world_pos = MODEL_MATRIX * vec4(in_pos, 1.0);

  pass_position = VIEW_MATRIX * world_pos;
  gl_Position = PROJECTION_MATRIX * pass_position;
  pass_depth = gl_Position.z;
  mat3 normal_matrix = mat3(MODEL_MATRIX);
  normal_matrix = inverse(normal_matrix);
  normal_matrix = transpose(normal_matrix);
  pass_normal = normalize(normal_matrix * in_normal);
  pass_shadow_pos[0] = LIGHT_SPACE_MATRIX[0] * world_pos;
  pass_shadow_pos[1] = LIGHT_SPACE_MATRIX[1] * world_pos;
  pass_shadow_pos[2] = LIGHT_SPACE_MATRIX[2] * world_pos;
  pass_shadow_pos[3] = LIGHT_SPACE_MATRIX[3] * world_pos;
  pass_uv = in_uv;
}