#version 430

#define MAX_NUM_POINT_LIGHTS 10

layout(location = 0) out vec4 frag_color;

in vec4 pass_position;
in vec3 pass_color;
in float pass_depth;
in vec3 pass_shadow_pos;

// uniform vec3 DIRECTIONAL_LIGHT;
// uniform vec3 DIRECTIONAL_LIGHT_COLOR;
uniform sampler2D ssao_sampler;
uniform sampler2DShadow DEPTH_MAP_0;
uniform sampler2DShadow DEPTH_MAP_1;
uniform sampler2DShadow DEPTH_MAP_2;
uniform sampler2DShadow DEPTH_MAP_3;
// uniform sampler2D texture_sampler;

uniform float shadow_depths[4];

layout(binding = 1) uniform frag_frame_data {
  vec4 frustum_limits;
  vec4 bias;
  vec3 DIRECTIONAL_LIGHT_COLOR;
  vec3 DIRECTIONAL_LIGHT;
  bvec2 bools;  // x = dir_light, y = shadows
  vec3 cam_pos;
  vec2 windows_size;
};



vec2 get_d_shadow_depth(vec3 shadow_pos_dx, vec3 shadow_pos_dy) {
  const vec2 shadow_texel_size = vec2(1.0 / 2048.0, 1.0 / 2048.0);

  mat2 shadow_to_screen_d =
      inverse(mat2(shadow_pos_dx.x, shadow_pos_dy.x, shadow_pos_dx.y, shadow_pos_dy.y));

  vec2 d_right = shadow_to_screen_d * vec2(shadow_texel_size.x, 0);
  vec2 d_up = shadow_to_screen_d * vec2(0, shadow_texel_size.y);

  vec2 d_depth = vec2(d_right.x * shadow_pos_dx.z + d_right.y * shadow_pos_dy.z,
                      d_up.x * shadow_pos_dx.z + d_up.y * shadow_pos_dy.z);

  return d_depth;
}


float multisample_shadow(sampler2DShadow depth_map, vec3 shadow_pos, vec3 shadow_pos_dx,
                         vec3 shadow_pos_dy, float radius, float slope_bias_modifier) {
  const float multisample_dirs[4] = {-1.5, -0.5, 0.5, 1.5};
  const float radius_tex = radius / 2048.0;

  vec2 d_depth = abs(get_d_shadow_depth(shadow_pos_dx, shadow_pos_dy));

  float slope_bias = (d_depth.x + d_depth.y) * slope_bias_modifier;
  float shadow = 0;
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      vec2 dir = vec2(multisample_dirs[i], multisample_dirs[j]);
      shadow += texture(depth_map, vec3(shadow_pos.xy + dir * radius_tex,
                                        shadow_pos.z - slope_bias))
                    .r;
    }
  }
  shadow /= 16.0;
  return shadow;
}

void main() {
  float shadow = 1.0;
  vec3 result;

  vec3 shadow_pos_dx = dFdx(pass_shadow_pos);
  vec3 shadow_pos_dy = dFdy(pass_shadow_pos);

  if (bools.y) {
    vec3 projCoords;
      vec3 shadow_pos = pass_shadow_pos;
      shadow_pos.z -= 3 / shadow_depths[3];
      shadow =
          multisample_shadow(DEPTH_MAP_3, shadow_pos, shadow_pos_dx, shadow_pos_dy, 2.0, 0);
  }

  shadow = min(shadow + 0.65, 1);
  vec3 pos = pass_position.xyz;
  float limit = 50;
  float falloff_distance = 2000;
  float l = abs(length(pos - cam_pos));
  float fog_res = l;
  float n = (limit - cam_pos.z) / normalize(pos - cam_pos).z;
  if (cam_pos.z > limit) {
    // fog_res = min(l - n, falloff_distance);
    fog_res = min(pos.z - limit, falloff_distance);
    // backwards rays, skip
    if (n < 0) {
      fog_res = 0;
    }
    // infinite length ray, skip
    if (l < n) {
      fog_res = 0;
    }
  } else {
    fog_res =
        min(min(pos.z - limit, 0), falloff_distance) + min(cam_pos.z - limit, falloff_distance);
  }

  float gamma = 2.2;

  // fog_res = min(fog_res, falloff_distance);
  float z_fog = (fog_res * fog_res) / falloff_distance;

  float depth_fog = (pass_depth * pass_depth * pass_depth) / 1000000000;
  const vec3 depth_fog_color = pow(vec3(178.0, 188.0, 237) / 255, vec3(gamma));
  const vec3 z_fog_color = pow(vec3(178.0, 188.0, 237) / 255, vec3(gamma));
  result = mix(vec3(shadow), depth_fog_color, min(depth_fog, 1));
  result = mix(result, z_fog_color, min(z_fog, 1));

  frag_color.rgb = pow(result, vec3(1.0 / gamma));
  frag_color.a = 1;
  
}
