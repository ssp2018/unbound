#version 430
out vec4 frag_color;
in vec2 pass_uv;

uniform sampler2D depth_sampler;
uniform sampler2D noise_sampler;

uniform vec3 samples[64];
uniform mat4 projection_matrix;
uniform mat4 inv_projection_matrix;

const int kernelSize = 16;
const float bias = 0.1;

vec3 get_pos(vec2 uv) {
  float depth = texture(depth_sampler, uv).r;
  vec4 pos = inv_projection_matrix * vec4(uv * 2 - 1, depth, 1);
  return pos.xyz / pos.w;
}

vec3 get_normal(vec3 position) {
  vec3 X = dFdx(position);
  vec3 Y = dFdy(position);
  return normalize(cross(X, Y));
}

void main() {
  float my_depth = texture(depth_sampler, pass_uv).r;
  vec3 my_offset = vec3(pass_uv.xy, gl_FragCoord.z);
  vec2 texel_size = vec2(1.0 / 1920.0, 1.0 / 1080.0);

  float depth_samples[4] = {texture(depth_sampler, pass_uv + vec2(texel_size.x, texel_size.y)).r,
                            texture(depth_sampler, pass_uv + vec2(-texel_size.x, texel_size.y)).r,
                            texture(depth_sampler, pass_uv + vec2(texel_size.x, -texel_size.y)).r,
                            texture(depth_sampler, pass_uv + vec2(-texel_size.x, -texel_size.y)).r};

  float depth_distance = 0.0;
  for (int i = 0; i < 4; i++) {
    depth_distance += abs(my_depth - depth_samples[i]);
  }

  depth_distance /= 4.0;
  if (depth_distance < 0.000011) {
    frag_color.xyz = vec3(1, 1, 1);
    return;
  }
  vec3 fragPos = get_pos(pass_uv);

  vec3 randomVec =
      texture(noise_sampler, vec2(sin(gl_FragCoord.x * fragPos.x), sin(gl_FragCoord.y * fragPos.y)))
          .xyz;
  vec3 normal = get_normal(fragPos);

  float radius = (-fragPos.z) * 0.3 + 0.3;

  vec3 tangent = normalize(randomVec - normal * dot(randomVec, normal));
  vec3 bitangent = cross(normal, tangent);
  mat3 TBN = mat3(tangent, bitangent, normal);

  float occlusion = 0.0;
  for (int i = 0; i < kernelSize; ++i) {
    // get sample position
    vec3 samplePos = TBN * samples[i];  // From tangent to view-space
    samplePos = fragPos + samplePos * radius;

    vec4 offset = vec4(samplePos, 1.0);
    offset = projection_matrix * offset;      // from view to clip-space
    offset.xy /= offset.w;                    // perspective divide
    offset.xy = offset.xy * 0.5 + vec2(0.5);  // transform to range 0.0 - 1.0
    float sampleDepth = get_pos(offset.xy).z;
    // occlusion += (sampleDepth >= samplePos.z + bias ? 1.0 : 0.0);
    float rangeCheck = smoothstep(0.0, 1.0, radius / abs(fragPos.z - sampleDepth) * 0.2);
    occlusion += (sampleDepth >= samplePos.z + bias ? 1.0 : 0.0) * rangeCheck;
    // occlusion += 1 - rangeCheck;
  }
  occlusion /= kernelSize;
  occlusion = 1 - occlusion;
  // occlusion = 1;

  // v3...
  frag_color.xyz = occlusion.xxx;
  // frag_color.y = normal_from_texture.x;
  // frag_color.xyz = fragPos.zzz * -0.01;
  // frag_color.xyz = normal.xyz;
  // frag_color.xyz = randomVec.xyz;

  // int iterations = 64;
  //    	for (int j = 0; j < iterations; ++j)
  // 	{
  // 		vec3 ray = radiusDepth * reflection(samples[j], randomVec);
  //     		vec3 hemiRay = position + sign(dot(ray,normal)) * ray;

  //     		float occDepth = texture(depth_sampler, clamp(hemiRay.xy,0.0,1.0)).r;
  //     		float difference = depth - occDepth;

  //     		occlusion += step(m_Falloff, difference) * (1.0-smoothstep(m_Falloff,
  //     m_Area, difference));
  // 	}

  // vec3 tangent   = normalize(randomVec - normal * dot(randomVec, normal));
  // vec3 bitangent = cross(normal, tangent);
  // mat3 TBN       = mat3(tangent, bitangent, normal);

  // frag_color.z = 0;

  // fragPos = (inverse(projection_matrix) * vec4(fragPos, 1)).xyz;
  // frag_color.xyz = fragPos;

  // float occlusion = 0.0;
  // for(int i = 0; i < kernelSize; ++i)
  // {
  //     // get sample position
  //   vec3 sampl = TBN * samples[i]; // From tangent to view-space
  //   sampl = fragPos + sampl * radius;

  //   vec4 offset = vec4(sampl, 1.0);
  //   offset      = projection_matrix * offset;    // from view to clip-space
  //   offset.xyz /= offset.w;               // perspective divide
  //   offset.xyz  = offset.xyz * 0.5 + 0.5; // transform to range 0.0 - 1.0

  //   float sampleDepth = texture(position_sampler, offset.xy).z;
  //   // occlusion += (sampleDepth >= sampl.z + bias ? 1.0 : 0.0);
  //   float rangeCheck = smoothstep(0.0, 1.0, radius / abs(fragPos.z - sampleDepth));
  //   occlusion       += (sampleDepth >= sampl.z + bias ? 1.0 : 0.0) * rangeCheck;

  // }

  // occlusion = 1.0 - (occlusion / kernelSize);
  // float ao = 1.0 - occlusion * (1.0 / iterations);
  //   	float final = clamp(ao + m_Base,0.0,1.0);

  // frag_color.w = 1;
  // frag_color = vec4(final, final, final, 1);
  // frag_color.xyz = fragPos.xyz;
  // frag_color.xyz = fragPos_depth.xyz;
  // frag_color.y = normal_from_texture.x;
  // fragment_depth = normal.z;
  // frag_color = vec4(normal, 1);
  // frag_color = vec4(texture(depth_sampler, pass_uv).rgb, 1);

  // fragment_depth =
}
