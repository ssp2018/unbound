#version 430
layout(location = 0) in vec3 in_pos;  // the position variable has attribute position 0
layout(location = 2) in vec3 in_normal;
layout(location = 1) in vec3 in_color;
layout(location = 5) in vec4 in_model_matrix_0;
layout(location = 6) in vec4 in_model_matrix_1;
layout(location = 7) in vec4 in_model_matrix_2;
layout(location = 8) in vec4 in_model_matrix_3;
out vec3 pass_normal;
// out vec3 pass_normal_view;
out vec4 pass_position;
// out vec4 pass_position_view;
out vec3 pass_shadow_pos[4];
out vec3 pass_color;
out float pass_depth;
// out float pass_ambient_normal_modifier;

uniform mat4 MODEL_MATRIX;
uniform mat3 NORMAL_MATRIX;
uniform mat4 LIGHT_SPACE_MATRIX[4];

layout(std140, binding = 0) uniform frame_data {
  mat4 VIEW_MATRIX;
  mat4 PROJECTION_MATRIX;
};

layout(std140, binding = 3) uniform MODEL_MATRICES {
  mat4 model_matrices[100];
};

float compute_ambient_normal_modifier(vec3 vertex_normal) {
  vec3 normal = normalize(vertex_normal);
  const float min_length = 0.577;  // smallest average axis length on 3d unit sphere
  const float max_length = 1.0;
  const float length_range = max_length - min_length;
  const float max_length_sum = max_length * 3.0;

  // These two are tweakable. Lower values give darker color.
  const float min_modifier = 0.7;
  const float max_modifier = 1.0;

  const float modifier_range = max_modifier - min_modifier;
  const float normalization_ratio = modifier_range / length_range;

  float average_length = (abs(normal.x) + abs(normal.y) + abs(normal.z)) / max_length_sum;

  return min_modifier + normalization_ratio * (average_length - min_length);
}

vec3 calc_shadow_pos(int index, vec4 world_pos) {
  vec4 res1 = LIGHT_SPACE_MATRIX[index] * world_pos;
  vec3 res = res1.xyz / res1.w;
  return res * 0.5 + 0.5;
}

void main() {
  // mat4 model_matrix = mat4(in_model_matrix_0, in_model_matrix_1, in_model_matrix_2, in_model_matrix_3);
  mat4 model_matrix = model_matrices[gl_InstanceID];
  vec4 world_pos = model_matrix * vec4(in_pos, 1.0);

  pass_position = world_pos;
  vec4 view_pos = VIEW_MATRIX * world_pos;
  pass_depth = -view_pos.z;
  gl_Position = PROJECTION_MATRIX * view_pos;
  mat3 normal_matrix = mat3(model_matrix);
  normal_matrix = inverse(normal_matrix);
  normal_matrix = transpose(normal_matrix);
  pass_normal = normalize(normal_matrix * in_normal);
  // normal_matrix = mat3(VIEW_MATRIX * MODEL_MATRIX);
  // normal_matrix = inverse(normal_matrix);
  // normal_matrix = transpose(normal_matrix);
  // pass_normal_view = normalize(normal_matrix * in_normal);
  // pass_ambient_normal_modifier = compute_ambient_normal_modifier(pass_normal);
  pass_shadow_pos[0] = calc_shadow_pos(0, world_pos);
  pass_shadow_pos[1] = calc_shadow_pos(1, world_pos);
  pass_shadow_pos[2] = calc_shadow_pos(2, world_pos);
  pass_shadow_pos[3] = calc_shadow_pos(3, world_pos);

  pass_color = in_color;
}