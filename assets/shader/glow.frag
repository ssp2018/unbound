#version 430
layout (location = 0) out vec4 frag_color;
// layout (location = 1) out vec3 out_position;
// layout (location = 2) out vec3 out_normal;
layout (location = 1) out vec4 out_glow;

in vec3 pass_color;
in vec3 pass_normal;

void main()
{


  // if (bools.x) {
  // }

  // frag_color = pow(vec4(pass_color * (diffuse_modifier + 0.2 * diffuse_modifier2), 1), vec4(1.0/2.2));
  frag_color = pow(vec4(pass_color, 1), vec4(1.0/2.2));
  // frag_color = vec4(1,0,0,1);
  // out_position = pass_position.xyz;
  // out_normal = pass_normal;
  out_glow = pow(vec4(pass_color, 1), vec4(1.0/2.2));
  // out_glow = pow(vec4(1,0,0, 1), vec4(2.2));
  // out_glow = vec4(pass_color,1);
  // out_glow = vec4(1,0,0,1);
}
