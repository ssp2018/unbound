#version 430
layout(location = 0) in vec3 in_pos;  // the position variable has attribute position 0

uniform mat4 MODEL_MATRIX;
layout(std140, binding = 0) uniform frame_data {
  mat4 VIEW_MATRIX;
  mat4 PROJECTION_MATRIX;
};

void main() {
  gl_Position = PROJECTION_MATRIX * VIEW_MATRIX *  MODEL_MATRIX * vec4(in_pos, 1.0);
}