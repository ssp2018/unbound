#version 430
layout(location = 0) in vec3 in_pos;
layout(location = 1) in vec2 in_uv;
layout(location = 2) in vec3 in_offset;
layout(location = 3) in vec4 in_color;
layout(location = 4) in float in_size;
layout(location = 5) in float in_rotation;

out vec2 pass_uv;
out vec4 pass_color;

uniform mat4 MODEL_MATRIX;
layout(std140, binding = 0) uniform frame_data {
  mat4 VIEW_MATRIX;
  mat4 PROJECTION_MATRIX;
};

const float size = 0.2;

void main() {
  float s = sin(in_rotation);
  float c = cos(in_rotation);
  vec3 pos = vec3(c*in_pos.x-s*in_pos.z, in_pos.y, s*in_pos.x+c*in_pos.z);
  gl_Position = PROJECTION_MATRIX * VIEW_MATRIX * ((MODEL_MATRIX * vec4(pos * in_size, 1.0)) + vec4(in_offset, 0));
  pass_uv = in_uv;
  pass_color = in_color;
}