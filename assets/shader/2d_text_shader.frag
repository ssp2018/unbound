#version 430
out vec4 frag_color;
in vec2 pass_uv;

uniform sampler2D TEXTURE;

uniform float THICKNESS;
uniform float OUTLINE_THICKNESS;
uniform vec3 OUTLINE_COLOR;
uniform vec3 COLOR;

void main() {
  const float middle = 192.0 / 255.0;
  const float base_thickness = 0.02;

  const float thickness = base_thickness + THICKNESS;

  // const float text_gamma_in = 0.005;
  // const float text_gamma_out = 0.005;
  // const float alpha_gamma_in = 0.003;
  // const float alpha_gamma_out = 0.003;
  const float text_gamma_in = 0.01;
  const float text_gamma_out = 0.01;
  const float alpha_gamma_in = 0.01;
  const float alpha_gamma_out = 0.01;

  float dist = texture(TEXTURE, vec2(pass_uv.x, 1 - pass_uv.y)).r;
  float outline_mix =
      smoothstep(middle - thickness - text_gamma_out, middle - thickness + text_gamma_in, dist);
  float alpha = smoothstep(middle - thickness - OUTLINE_THICKNESS - alpha_gamma_out,
                           middle - thickness - OUTLINE_THICKNESS + alpha_gamma_in, dist);
  frag_color.rgb = mix(OUTLINE_COLOR, COLOR, outline_mix);
  frag_color.a = alpha;
}
