#version 430

layout(location = 0) out vec4 frag_color;

in vec3 pass_normal;
in vec4 pass_position;
in vec2 pass_uv;

uniform sampler2D TEXTURE_SAMPLER;
uniform float time_passed;
uniform vec4 color;

layout(binding = 1) uniform frag_frame_data {
  vec4 frustum_limits;
  vec4 bias;
  vec3 DIRECTIONAL_LIGHT_COLOR;
  vec3 DIRECTIONAL_LIGHT;
  bvec2 bools;  // x = dir_light, y = shadows
  vec3 cam_pos;
  vec2 windows_size;
};

void main() {
  vec3 I = normalize(pass_position.xyz - vec3(-cam_pos.x, -cam_pos.y, cam_pos.z));
  float bias = 0.2;
  float scale = 1;
  float power = 1;

  float R = max(0, min(1, bias + scale * (1 + dot(I, pass_normal) * power)));

  float power2 = 1.5;
  float R2 = max(0, min(1, bias + scale * (1 + dot(I, pass_normal) * power2)));

  frag_color.xyz = color.xyz;
  frag_color.a = R2 + texture(TEXTURE_SAMPLER,
                              vec2(pass_uv.x + time_passed * 0.1, pass_uv.y + time_passed * 0.02))
                              .x *
                          0.9;
  frag_color.a *= color.a;
}
