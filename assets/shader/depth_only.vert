#version 430
layout(location = 0) in vec3 in_pos;  // the position variable has attribute position 0

layout(std140, binding = 0) uniform frame_data {
  mat4 VIEW_MATRIX;
  mat4 PROJECTION_MATRIX;
};

layout(std140, binding = 3) uniform MODEL_MATRICES { mat4 model_matrices[100]; };

void main() {
  // mat4 model_matrix = mat4(in_model_matrix_0, in_model_matrix_1, in_model_matrix_2,
  // in_model_matrix_3);
  mat4 model_matrix = model_matrices[gl_InstanceID];
  vec4 world_pos = model_matrix * vec4(in_pos, 1.0);
  gl_Position = PROJECTION_MATRIX * VIEW_MATRIX * world_pos;
}