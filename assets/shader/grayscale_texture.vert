#version 430
layout(location = 0) in vec3 in_pos;
layout(location = 3) in vec2 in_uv;
uniform mat4 MODEL_MATRIX;
uniform mat4 VIEW_MATRIX;
uniform mat4 PROJECTION_MATRIX;
out vec2 pass_uv;
void main() {
  gl_Position = PROJECTION_MATRIX * VIEW_MATRIX * MODEL_MATRIX * vec4(in_pos, 1.0);
  pass_uv = in_uv;
}