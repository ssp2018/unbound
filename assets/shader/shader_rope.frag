#version 430
layout (location = 0) out vec4 frag_color;
// layout (location = 1) out vec3 out_position;
// layout (location = 2) out vec3 out_normal;

in vec3 pass_normal;
in vec4 pass_position;
in float pass_depth;
in vec2 pass_uv;
in vec4 pass_shadow_pos[4];

uniform vec3 DIRECTIONAL_LIGHT;
uniform vec3 DIRECTIONAL_LIGHT_COLOR;
uniform sampler2DShadow DEPTH_MAP_0;
uniform sampler2DShadow DEPTH_MAP_1;
uniform sampler2DShadow DEPTH_MAP_2;
uniform sampler2DShadow DEPTH_MAP_3;

uniform sampler2D texture_sampler;

uniform float frustum_limits[3];
uniform float bias[3];

uniform float rope_length;

void main()
{


  float shadow = 1.0;
  vec3 projCoords;
  if (pass_depth < frustum_limits[0]) {
    vec3 projCoords = pass_shadow_pos[0].xyz / pass_shadow_pos[0].w;
    projCoords = projCoords * 0.5 + 0.5;
    shadow = texture(DEPTH_MAP_0, vec3(projCoords.xy, projCoords.z - bias[0])).r;
    frag_color = vec4(1,0,0,1);
  } else if (pass_depth < frustum_limits[1]) {
    vec3 projCoords = pass_shadow_pos[1].xyz / pass_shadow_pos[1].w;
    projCoords = projCoords * 0.5 + 0.5;
    shadow = texture(DEPTH_MAP_1, vec3(projCoords.xy, projCoords.z - bias[1])).r;
    frag_color = vec4(1,1,0,1);
  } else if (pass_depth < frustum_limits[2]) {
    vec3 projCoords = pass_shadow_pos[2].xyz / pass_shadow_pos[2].w;
    projCoords = projCoords * 0.5 + 0.5;
    shadow = texture(DEPTH_MAP_2, vec3(projCoords.xy, projCoords.z - bias[2])).r;
    frag_color = vec4(0,1,0,1);
  } else {
    // vec3 projCoords = pass_shadow_pos[3].xyz / pass_shadow_pos[3].w;
    // projCoords = projCoords * 0.5 + 0.5;
    // shadow = texture(DEPTH_MAP_3, vec3(projCoords.xy, projCoords.z - 0.001)).r;
    // frag_color = vec4(0,0,1,1);
  }
  
  vec3 diffuse = DIRECTIONAL_LIGHT_COLOR * max(dot(normalize(pass_normal), normalize(DIRECTIONAL_LIGHT)), 0) * shadow;

  // frag_color = vec4((vec3(0.2f, 0.2f, 0.2f) + diffuse * 0.7) * pass_color, 1.f);
  // out_position = pass_position.xyz;
  // out_normal = pass_normal;
  // frag_color = vec4(pass_uv, 0, 1);
  frag_color = vec4(texture(texture_sampler, vec2(pass_uv.x, (1-pass_uv.y)*rope_length)).r * pow(vec3(0.3994, 0.3189, 0.2817), vec3(2.2)), 1);

    frag_color.w = 1;
  //gamma
  float gamma = 2.2;
  frag_color.rgb = pow(frag_color.rgb, vec3(1.0/gamma));
}
