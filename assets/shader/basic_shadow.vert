#version 430
layout(location = 0) in vec3 in_pos;
uniform mat4 VIEW_PROJECTION_MATRIX;
// uniform mat4 MODEL_MATRIX;


layout(std140, binding = 3) uniform MODEL_MATRICES {
  mat4 model_matrices[100];
};
void main() {
  gl_Position = VIEW_PROJECTION_MATRIX * model_matrices[gl_InstanceID] * vec4(in_pos, 1.0);
  float w = gl_Position.w;
  float z = gl_Position.z;
  z /= w;
  z = max(z, -0.99999);
  z *= w;
  gl_Position.z = z;
}