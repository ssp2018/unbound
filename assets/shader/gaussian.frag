#version 430
out vec4 frag_color;
in vec2 pass_uv;

uniform sampler2D texture_sampler;

uniform vec2 direction;
uniform float radius;


void main()
{
  vec4 res = vec4(0,0,0,0);
	// float radius = 0.002;

  // res += texture(texture_sampler, vec2(pass_uv.x - 4.0*radius*direction.x, pass_uv.y - 4.0*radius*direction.y)) * 0.0162162162;
	// res += texture(texture_sampler, vec2(pass_uv.x - 3.0*radius*direction.x, pass_uv.y - 3.0*radius*direction.y)) * 0.0540540541;
	res += texture(texture_sampler, pass_uv - 3.5*radius*direction) * 0.106595;
	res += texture(texture_sampler, pass_uv - 2.2*radius*direction) * 0.140367;
	res += texture(texture_sampler, pass_uv - 1.0*radius*direction) * 0.165569;
	
	res += texture(texture_sampler, pass_uv) * 0.174938;
	
	res += texture(texture_sampler, pass_uv + 1.0*radius*direction) * 0.165569;
	res += texture(texture_sampler, pass_uv + 2.2*radius*direction) * 0.140367;
	res += texture(texture_sampler, pass_uv + 3.5*radius*direction) * 0.106595;
	// res += texture(texture_sampler, vec2(pass_uv.x + 3.0*radius*direction.x, pass_uv.y + 3.0*radius*direction.y)) * 0.0540540541;
	// res += texture(texture_sampler, vec2(pass_uv.x + 4.0*radius*direction.x, pass_uv.y + 4.0*radius*direction.y)) * 0.0162162162;


  frag_color = res;
	// frag_color = vec4(0,1,0,1);
}
