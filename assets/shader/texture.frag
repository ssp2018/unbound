#version 430
out vec4 frag_color;
in vec2 pass_uv;

uniform sampler2D texture_sampler;

void main()
{
  frag_color = texture(texture_sampler, vec2(pass_uv.x, 1 - pass_uv.y));
  if(frag_color.a < 0.05) {
    discard;
  }

  //frag_color.w = 1;
  //gamma
  float gamma = 2.2;
  frag_color.rgb = pow(frag_color.rgb, vec3(1.0/gamma));
}
