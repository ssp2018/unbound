#version 430
layout(location = 0) in vec3 in_pos;
uniform mat4 VIEW_PROJECTION_MATRIX;
uniform mat4 MODEL_MATRIX;



void main() {
  gl_Position = VIEW_PROJECTION_MATRIX * MODEL_MATRIX * vec4(in_pos, 1.0);
  float w = gl_Position.w;
  float z = gl_Position.z;
  z /= w;
  z = max(z, -0.99999);
  z *= w;
  gl_Position.z = z;
}