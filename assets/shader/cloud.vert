#version 430
layout(location = 0) in vec3 in_pos;  // the position variable has attribute position 0
layout(location = 2) in vec3 in_normal;
layout(location = 1) in vec3 in_color;
out vec4 pass_position;
out vec3 pass_shadow_pos;
out vec3 pass_color;
out float pass_depth;

uniform mat4 MODEL_MATRIX;
uniform mat3 NORMAL_MATRIX;
uniform mat4 LIGHT_SPACE_MATRIX[4];

layout(std140, binding = 0) uniform frame_data {
  mat4 VIEW_MATRIX;
  mat4 PROJECTION_MATRIX;
};

layout(std140, binding = 3) uniform MODEL_MATRICES {
  mat4 model_matrices[100];
};

vec3 calc_shadow_pos(int index, vec4 world_pos) {
  vec4 res1 = LIGHT_SPACE_MATRIX[index] * world_pos;
  vec3 res = res1.xyz / res1.w;
  return res * 0.5 + 0.5;
}

void main() {
  vec4 world_pos = model_matrices[gl_InstanceID] * vec4(in_pos, 1.0);

  pass_position = world_pos;
  vec4 view_pos = VIEW_MATRIX * world_pos;
  pass_depth = -view_pos.z;
  gl_Position = PROJECTION_MATRIX * view_pos;
  pass_shadow_pos = calc_shadow_pos(3, world_pos);

  pass_color = in_color;
}