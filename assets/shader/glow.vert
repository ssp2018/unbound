#version 430
layout(location = 0) in vec3 in_pos;  // the position variable has attribute position 0
layout(location = 2) in vec3 in_normal;
layout(location = 1) in vec3 in_color;
out vec3 pass_color;
out vec3 pass_normal;

layout(std140, binding = 3) uniform MODEL_MATRICES {
  mat4 model_matrices[100];
};

layout(std140, binding = 0) uniform frame_data {
  mat4 VIEW_MATRIX;
  mat4 PROJECTION_MATRIX;
};

void main() {
  vec4 world_pos = model_matrices[gl_InstanceID] * vec4(in_pos, 1.0);
  gl_Position = PROJECTION_MATRIX * VIEW_MATRIX *  world_pos;
  pass_color = in_color;
  mat3 normal_matrix = mat3(model_matrices[gl_InstanceID]);
  normal_matrix = inverse(normal_matrix);
  normal_matrix = transpose(normal_matrix);
  pass_normal = normalize(normal_matrix * in_normal);
}