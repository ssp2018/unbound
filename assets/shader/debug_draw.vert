#version 430
layout(location = 0) in vec3 in_pos;
layout(location = 1) in vec3 in_color;

layout(std140, binding = 0) uniform frame_data {
  mat4 VIEW_MATRIX;
  mat4 PROJECTION_MATRIX;
};

out vec3 pass_color;

void main() {
  gl_Position = PROJECTION_MATRIX * VIEW_MATRIX * vec4(in_pos, 1.0f);
  pass_color = in_color;
}