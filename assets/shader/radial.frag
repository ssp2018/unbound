#version 430
out vec4 frag_color;
in vec2 pass_uv;

uniform sampler2D texture_sampler;

uniform float radius;


void main()
{
  vec4 res = vec4(0,0,0,0);

	vec2 normal = pass_uv * 2 - vec2(1,1);

if (length(normal) > 0.5) {
	normal = normal - 0.5 * normalize(normal);
	normal = vec2(abs(normal.x) * normal.x, abs(normal.y) * normal.y);
  // res += texture(texture_sampler, vec2(pass_uv.x - 4.0*radius*normal.x, pass_uv.y - 4.0*radius*normal.y)) * 0.0162162162;
	// res += texture(texture_sampler, vec2(pass_uv.x - 3.0*radius*normal.x, pass_uv.y - 3.0*radius*normal.y)) * 0.026;
	res += texture(texture_sampler, pass_uv - 2.0*radius*normal) * 0.1784;
	res += texture(texture_sampler, pass_uv - 1.0*radius*normal) * 0.210431;
	
	res += texture(texture_sampler, vec2(pass_uv.x, pass_uv.y)) * 0.222338;
	
	res += texture(texture_sampler, pass_uv + 1.0*radius*normal) * 0.210431;
	res += texture(texture_sampler, pass_uv + 2.0*radius*normal) * 0.1784;
	// res += texture(texture_sampler, vec2(pass_uv.x + 3.0*radius*normal.x, pass_uv.y + 3.0*radius*normal.y)) * 0.006;
	// res += texture(texture_sampler, vec2(pass_uv.x + 4.0*radius*normal.x, pass_uv.y + 4.0*radius*normal.y)) * 0.0162162162;

} else {
	res = texture(texture_sampler, vec2(pass_uv.x, pass_uv.y));
}


  frag_color = res;
}
