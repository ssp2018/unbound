#version 430

layout(location = 0) out vec4 entity_handle;
layout(location = 1) out vec4 out_pos;
layout(location = 2) out vec3 out_normal;

in vec3 pass_normal;
in vec4 pass_position;

uniform vec4 color;

void main() {
    entity_handle = color;
    out_pos = pass_position;
    out_normal = pass_normal;
}