local cpp = require("cpp")
local ref = require("reflect")
local os = require("os")
local math = require("math")
-- Let's keep the dependencies clean.

-- UTILITY
-- Miscellaneous utility functions for making life easier.
-- If you don't know where to put a function, and it has
-- no dependencies to the editor scripts,  feel free to add
-- it here until you find a better place for it.

-- Validate table against a template table.
-- Checks that the keys and value types match
-- the template.
-- Returns true if valid.
function is_table_valid(dirty_table, template_table)
  for k, v in pairs(template_table) do
    if dirty_table[k] == nil then
      print("Table lacks '" .. k .. "'.")
      return false
    end
    if type(dirty_table[k]) ~= type(v) then
      print("Table's '" .. k .. "' should be '" .. type(v) .. "', is '" .. type(dirty_table[k]) .. "'.")
      return false
    end
  end

  return true
end

-- Set default entity meta information
function set_meta(context, entity, name)
  if name == nil or name == "" then
    name = generate_name(entity)
  end

  local meta = entity.EditorMetaComponent

  if meta then
    meta.name = name
  else
    meta = cpp.EditorMetaComponent.new()
    meta.name = name
    entity.EditorMetaComponent = meta
  end
end

local visuals = {
  "ModelComponent"
}

-- Set entity's default visual components if entity
-- has no visuals.
function set_visuals(context, entity)
  if not entity.TransformComponent then
    local pos = cpp.vec3.new()
    pos.x = 0
    pos.y = 0
    pos.z = 0
    local cameras = context.entity_manager:get_entities({"DebugCameraComponent", "TransformComponent"})
    if cameras and #cameras > 0 then
      local transform = cameras[1].TransformComponent.transform
      local dir = transform:get_up()
      local distance = -5
      dir.x = dir.x * distance
      dir.y = dir.y * distance
      dir.z = dir.z * distance
      pos = transform:get_position()
      pos.x = pos.x + dir.x
      pos.y = pos.y + dir.y
      pos.z = pos.z + dir.z
    end

    local transform = cpp.TransformComponent.new()
    transform.transform:set_position(pos, context.frame_context)
    entity.TransformComponent = transform
  end

  local has_visual = false
  for i, visual in ipairs(visuals) do
    if entity[visual] then
      has_visual = true
      break
    end
  end

  if not has_visual then
    local model = cpp.ModelComponent.new()
    model.asset = cpp.load_model("assets/model/box.fbx")
    entity.ModelComponent = model
  end
end

-- Create standard entity
function create_entity(context, name)
  local entity = context.entity_manager:create_entity()
  return fill_entity(context, entity, name)
end

-- Fill entity with default components, if necessary
function fill_entity(context, entity, name)
  set_meta(context, entity, name)
  set_visuals(context, entity)
  return entity
end

-- Get entity name from id
function get_entity_name(context, entity_id)
  local entity = context.entity_manager:get_entity(entity_id)
  if entity.is_valid then
    return entity.EditorMetaComponent.name
  else
    return ""
  end
end

-- Get scene entity table from id
function get_scene_entity(context, entity_id)
  local scene = context.entities
  return scene[get_entity_name(context, entity_id)]
end

-- Set scene entity table with name from id
function set_scene_entity(context, entity_id, value)
  local scene = context.entities
  scene[get_entity_name(context, entity_id)] = value
end

-- Get component from scene entity
function get_scene_entity_component(context, entity_id, component)
  local entity = get_scene_entity(context, entity_id)
  if not entity or not entity.components then
    return nil
  end

  return entity.components[component]
end

-- Get entity from name
function get_entity(context, entity_name)
  if context.name_map then
    local entity_id = context.name_map[entity_name]
    if entity_id then
      return context.entity_manager:get_entity(entity_id)
    end
  end
  local entities = context.entity_manager:get_entities({"EditorMetaComponent"})
  for i, entity in ipairs(entities) do
    if entity.EditorMetaComponent.name == entity_name then
      return entity
    end
  end
  return nil
end

-- Check if a registered component of specified name exists.
function is_component(context, component_name)
  for i, name in ipairs(context.components) do
    if name == component_name then
      return true
    end
  end
  return false
end

-- Append element to table
function append(the_table, element)
  the_table[#the_table + 1] = element
end

-- Append the source table to the target table
-- If "do_overwrite" is true, conflicting keys in
-- target table are overwritten.
function append_table(target, source, do_overwrite)
  if do_overwrite then
    for k, v in pairs(source) do
      target[k] = v
    end
  else
    for k, v in pairs(source) do
      if target[k] == nil then
        target[k] = v
      end
    end
  end
end

-- Recursively get a value from table using path,
-- where path is a table of path parts.
-- First and last are optional, and specify
-- where to look for parts in path.
function get_table_value(the_table, path, first, last)
  if first == nil then
    first = 1
  end

  if last == nil then
    last = #path
  end

  local val = the_table
  for i = first, last do
    if val == nil then
      return nil
    end
    val = val[path[i]]
  end

  return val
end

-- Recursively set a value to a table using path,
-- where path is a table of path parts.
-- Will create missing parent entries.
-- First and last are optional, and specify
-- where to look for parts in path.
function set_table_value(the_table, path, value, first, last)
  if the_table == nil then
    return
  end

  if first == nil then
    first = 1
  end

  if last == nil then
    last = #path
  end

  local val = the_table
  for i = first, (last - 1) do
    if val[path[i]] == nil then
      val[path[i]] = {}
    end
    val = val[path[i]]
  end

  val[path[last]] = value
end

-- Glue together a table of values into
-- a string with a delimiter. Return result.
-- First and last are optional, and specify
-- what section in parts to implode.
function implode(parts, delimiter, first, last)
  if parts == nil then
    return ""
  end

  if first == nil then
    first = 1
  end

  if last == nil then
    last = #parts
  end

  local str = parts[first]
  first = first + 1
  for i = first, last do
    str = str .. delimiter .. parts[i]
  end
  return str
end

-- Perform a deep copy of table.
-- Does not do deep copies of userdata.
function copy_table(table)
  if type(table) ~= "table" then
    return table
  end

  local copy = {}
  for k, v in pairs(table) do
    copy[k] = copy_table(v)
  end
  return copy
end

-- Return key of value if exists, else nil
function find_value(table, value)
  if not table or type(table) ~= "table" then
    if not ref.has_pairs(table) then
      return nil
    end
  end
  for k, v in pairs(table) do
    if v == value then
      return k
    end
  end
  return nil
end

-- UNIMPLEMENTED
-- If you need this function, feel free to implement it.
function to_table(reflectable)
  print("UNIMPLEMENTED editor/utility/to_table")
  return nil
  -- local table = {}
  -- if type(reflectable) == "userdata" then
  --   if not reflectable then
  --     return nil
  --   end
  --   local members = ref.members(reflectable)
  --   if not members then
  --     return nil
  --   end
  --   for i, member in ipairs(members) do
  --     table[member] = to_table(reflectable[member])
  --   end
  -- elseif type(reflectable) == "table" then
  --   for k, v in pairs(reflectable) do
  --     table[member] = to_table(reflectable[member])
  --   end
  -- end

  -- return reflectable
end

-- Generate a (hopefully unique) id
function generate_id()
  return os.time() .. "_" .. math.random(0, 9999)
end

-- Generate a (hopefully unique) name depending
-- on the components attached to entity.
function generate_name(entity)
  if entity.PresetInstanceComponent then
    return generate_preset_instance_name()
  else
    return generate_entity_name()
  end
end

-- Generate a (hopefully unique) name for entity.
function generate_entity_name()
  return "entity" .. generate_id()
end

-- Generate a (hopefully unique) name for preset instance.
function generate_preset_instance_name()
  return "preset_instance" .. generate_id()
end

-- Return true if entity is selected
function is_entity_selected(context, entity)
  for i, selection in ipairs(context.selected_entities) do
    if selection.id == entity.get_handle then
      return true
    end
  end
  return false
end

-- Return true if preset is selected
function is_preset_selected(context, name)
  return context.selected_preset and context.selected_preset == name
end
