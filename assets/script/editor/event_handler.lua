local scene = require("editor/scene")
local event_types = require("editor/event")
local utility = require("editor/utility")
local gui = require("editor/gui")
local history = require("editor/history")
local cpp = require("cpp")

-- EVENT HANDLER
-- Handlers for all events and some
-- helper functions

local function mesh_build_node_helper(context, event, action_type)
  if #context.selected_entities < 1 then
    return
  end

  local entities = {}
  for i, selection in ipairs(context.selected_entities) do
    local entity = context.entity_manager:get_entity(selection.id)
    if entity.MeshNodeComponent then
      local node = entity.MeshNodeComponent
      utility.append(
        entities,
        {
          name = entity.EditorMetaComponent.name,
          mesh_name = entity.MeshNodeComponent.mesh_name
        }
      )
    end
  end

  if #entities < 1 then
    return
  end

  for i = 1, #entities do
    for j = 2, #entities do
      if entities[i].mesh_name == entities[j].mesh_names and entities[i].name ~= entities[j].name then
        utility.append(
          context.actions,
          {
            type = action_type,
            entity_name = entities[i].name,
            target_name = entities[j].name
          }
        )
      end
    end
  end
end

-- Handlers for all events
local handlers = {
  load_scene = function(context, event)
    history.create(context)
    scene.load(context)
  end,
  create_entity = function(context, event)
    utility.append(
      context.actions,
      {
        type = "create_entity",
        entity_name = utility.generate_entity_name()
      }
    )
  end,
  save_scene = function(context, event)
    scene.save(context)
  end,
  gui_edit = function(context, event)
    gui.handle_dirty_path(context, event.path)
  end,
  remove_entity = function(context, event)
    utility.append(
      context.actions,
      {
        type = "remove_entity",
        entity_name = utility.get_entity_name(context, event.entity_id)
      }
    )
  end,
  select = function(context, event)
    scene.select_entity(context, event.entity_id)
  end,
  deselect = function(context, event)
    scene.deselect_entity(context, event.entity_id)
  end,
  transform = function(context, event)
    scene.transform_selection(context)
  end,
  create_preset = function(context, event)
    if #context.selected_entities == 0 then
      return
    end
    local selections = {}
    for i, selection in ipairs(context.selected_entities) do
      utility.append(selections, utility.get_entity_name(context, selection.id))
    end
    utility.append(
      context.actions,
      {
        type = "create_preset",
        name = "preset" .. utility.generate_id(),
        entity_names = selections
      }
    )
  end,
  create_preset_instance = function(context, event)
    local name = context.selected_preset
    if not name then
      return
    end

    utility.append(
      context.actions,
      {
        type = "create_preset_instance",
        preset_name = name,
        entity_name = utility.generate_preset_instance_name()
      }
    )
  end,
  copy = function(context, event)
    if #context.selected_entities == 0 then
      return
    end

    context.clipboard = {}
    for i, entity in ipairs(context.selected_entities) do
      utility.append(context.clipboard, entity.id)
    end
  end,
  paste = function(context, event)
    if #context.clipboard == 0 then
      return
    end

    for i, id in ipairs(context.clipboard) do
      local entity = context.entity_manager:get_entity(id)
      utility.append(
        context.actions,
        {
          type = "clone",
          entity_name = utility.get_entity_name(context, id),
          clone_name = utility.generate_name(entity)
        }
      )
    end
  end,
  delete = function(context, event)
    if #context.selected_entities == 0 then
      return
    end

    for i, entity in ipairs(context.selected_entities) do
      utility.append(
        context.actions,
        {
          type = "delete",
          entity_name = utility.get_entity_name(context, entity.id)
        }
      )
    end
  end,
  undo = function(context, event)
    history.undo(context)
  end,
  redo = function(context, event)
    history.redo(context)
  end,
  attach = function(context, event)
    if #context.selected_entities == 0 then
      return
    end

    scene.transform_selection(context)
    for i, entity in ipairs(context.selected_entities) do
      utility.append(
        context.actions,
        {
          type = "attach",
          entity_name = utility.get_entity_name(context, entity.id),
          target_name = utility.get_entity_name(context, event.target)
        }
      )
    end
  end,
  detach = function(context, event)
    if #context.selected_entities == 0 then
      return
    end
    scene.transform_selection(context)
    for i, entity in ipairs(context.selected_entities) do
      utility.append(
        context.actions,
        {
          type = "detach",
          entity_name = utility.get_entity_name(context, entity.id)
        }
      )
    end
  end,
  connect_mesh_build_nodes = function(context, event)
    mesh_build_node_helper(context, event, "connect_mesh_build_nodes")
  end,
  disconnect_mesh_build_nodes = function(context, event)
    mesh_build_node_helper(context, event, "disconnect_mesh_build_nodes")
  end
}

-- Potential custom validators for events
-- that contain optional data.
-- Return true if valid.
local custom_validators = {}

-- Validate event data.
-- Return true if valid.
local function is_event_valid(event)
  -- Check if it fits the standard event.
  if not utility.is_table_valid(event, event_types.event) then
    print("Event does not match standard event.")
    return false
  end

  -- Check if event exists in event list.
  if not event_types[event.type] then
    print("Event type does not exist.")
    return false
  end

  -- We must have a handler
  if not handlers[event.type] then
    print("Event handler does not exist.")
    return false
  end

  -- Use the custom validator if exists
  local custom_validator = custom_validators[event.type]
  if custom_validator then
    return custom_validator(event)
  end

  return utility.is_table_valid(event, event_types[event.type])
end

-- Handle event implementation
-- Simply validate it and pass it through to an event handler.
local function handle_single_event(context, event)
  print("Event! " .. event.type)
  if not is_event_valid(event) then
    print("Invalid event : ")
    print(event)
    return
  end

  handlers[event.type](context, event)
end

-- Handle events pushed to private event queue.
-- Events triggered by the client may cause
-- further events. This function will handle the events
-- in this queue until it is empty.
local function handle_all_events(context)
  local num_events = #context.events
  while num_events > 0 do
    local events = context.events
    context.events = {}

    for i, event in ipairs(events) do
      handle_single_event(context, event)
    end
    history.update(context)
    num_events = #context.events
  end
end

-- Handle event
function handle_event(context, event)
  utility.append(context.events, event)
  handle_all_events(context)
end
