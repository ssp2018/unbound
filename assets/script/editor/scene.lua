local cpp = require("cpp")
local ref = require("reflect")
local utility = require("editor/utility")
local gui = require("editor/gui")
local serializer = require("editor/serializer")
local mesh = require("editor/mesh")

-- SCENE
-- Responsible for common scene operations

local function apply_relative_transforms(context)
  for name, scene_entity in pairs(context.entities) do
    if scene_entity.components.AttachToEntityComponent then
      local entity = utility.get_entity(context, name)
      scene_entity.components.TransformComponent =
        serializer.create_reflectable_table(context, entity.TransformComponent)
    end
  end
end

-- Save scene to file
function save(context)
  apply_relative_transforms(context)
  local scene = {
    entities = context.entities,
    presets = context.presets
  }
  cpp.save_scene(scene, context.path)
end

local function create_empty_entity(context, name)
  local entity = context.entity_manager:create_entity()
  utility.set_meta(context, entity, name)
  return entity
end

local function upload_entity(context, entity, table)
  for component_name, component_desc in pairs(table["components"]) do
    local component = serializer.create_reflectable(context, cpp[component_name], component_desc)
    entity[component_name] = component
  end
  return entity
end

local function upload_entity_release(context, entity, table)
  for component_name, component_desc in pairs(table["components"]) do
    local skip = false
    if component_name == "ModelComponent" then
      if component_desc.asset == "assets/model/editor_enemy_spawn.fbx" then
        -- Do nothing
        skip = true
      elseif component_desc.asset == "assets/model/editor_boss_spawn.fbx" then
        -- Do nothing
        skip = true
      elseif component_desc.asset == "assets/model/editor_quest_item.fbx" then
        -- Do nothing
        skip = true
      elseif component_desc.asset ==  "assets/model/editor_player_checkpoint.fbx" then
        -- Do nothing
        skip = true
      elseif component_desc.asset ==  "assets/model/editor_checkpoint_area.fbx" then
        -- Do nothing
        skip = true
      end
    end

    if skip == false then
      local component = serializer.create_reflectable(context, cpp[component_name], component_desc)
      entity[component_name] = component
    end
  end
  return entity
end

local function load_entity(context, entity, table)
  if not table then
    table = {components = {}}
  end
  upload_entity(context, entity, table)
  local name = entity.EditorMetaComponent.name
  utility.fill_entity(context, entity, name)

  if table.components.PresetInstanceComponent then
    load_preset_instance(context, entity)
  end

  gui.refresh_entity(context, entity)
  context.entities[name] = table
  return entity
end

-- Upload scene into entity manager
function upload(scene, frame_context, entity_manager)
  if scene.entity_manager then
    apply_relative_transforms(scene)
  end
  local context = {
    frame_context = frame_context,
    entity_manager = entity_manager,
    name_map = {}
  }
  local scene_entities = scene.entities
  local presets = scene.presets
  local preset_children = {}
  local entities = {}

  local function recursive_upload_entities(entity_table, is_preset_dummy, parent_name, parent_desc, cumulative_transform)
    for name, entity_desc in pairs(entity_table) do
      if entity_desc.components.PresetInstanceComponent then
        local recursive_transform
        if cumulative_transform == nil then
          -- Only executes this if this is a top-level preset
          recursive_transform = cpp.Transform.new()
          recursive_transform:set_model_matrix(
            serializer.create_reflectable(context, cpp.Transform, entity_desc.components.TransformComponent.transform):get_model_matrix(

            ),
            context.frame_context
          )
        else
          -- Only executes this for presets within presets
          recursive_transform = cpp.Transform.new()
          recursive_transform:set_model_matrix(
            cpp.mat4_multiply(
              cumulative_transform:get_model_matrix(),
              serializer.create_reflectable(context, cpp.Transform, entity_desc.relative_transform):get_model_matrix()
            ),
            context.frame_context
          )
        end

        local preset = presets[entity_desc.components.PresetInstanceComponent.name]

        local full_name
        if is_preset_dummy then
          full_name = parent_name .. "/" .. name
        else
          full_name = name
        end

        recursive_upload_entities(preset, true, full_name, entity_desc, recursive_transform)
      elseif is_preset_dummy then
        local entity = context.entity_manager:create_entity()
        context.name_map[parent_name .. "/" .. name] = entity.get_handle
        preset_children[parent_name .. "/" .. name] = {
          template = entity_table[name],
          instance = parent_desc,
          entity = entity,
          parent_transform = cumulative_transform
        }
        if cumulative_transform == nil then
          print(preset_children[parent_name .. "/" .. name])
        end
      else
        local entity = context.entity_manager:create_entity()
        entities[name] = entity
        context.name_map[name] = entity.get_handle
      end
    end
  end

  recursive_upload_entities(scene_entities, false)

  -- for name, entity_desc in pairs(scene_entities) do
  --   if entity_desc.components.PresetInstanceComponent then
  --     local preset = presets[entity_desc.components.PresetInstanceComponent.name]
  --     for k, v in pairs(preset) do
  --       local entity = context.entity_manager:create_entity()
  --       context.name_map[name .. "/" .. k] = entity.get_handle
  --       preset_children[name .. "/" .. k] = {
  --         template = preset[k],
  --         instance = entity_desc,
  --         entity = entity
  --       }
  --     end
  --   else
  --     local entity = context.entity_manager:create_entity()
  --     entities[name] = entity
  --     context.name_map[name] = entity.get_handle
  --   end
  -- end

  for name, entity in pairs(entities) do
    local scene_entity = scene_entities[name]
    upload_entity_release(context, entity, scene_entity)
  end

  for name, child in pairs(preset_children) do
    upload_entity_release(context, child.entity, child.template)

    local relative_transform = serializer.create_reflectable(context, cpp.Transform, child.template.relative_transform)
    if relative_transform then
      local transform =
        cpp.mat4_multiply(child.parent_transform:get_model_matrix(), relative_transform:get_model_matrix())

      local transform_component = cpp.TransformComponent.new()
      transform_component.transform:set_model_matrix(transform, context.frame_context)
      child.entity.TransformComponent = transform_component
    end
  end
end

-- Create entity from table
function create_entity(context, name, table)
  local entity = create_empty_entity(context, name)
  load_entity(context, entity, table)
  local mesh_nodes = context.entity_manager:get_entities({"MeshNodeComponent"})
  if table.components.MeshNodeComponent then
    for i, neighbor_id in ipairs(entity.MeshNodeComponent.neighbors) do
      local neighbor = context.entity_manager:get_entity(neighbor_id)
      mesh.connect(context, entity, neighbor)
    end
  end
  return entity
end

function create_preset_instance(context, preset_name, name, table)
  if not table then
    table = {}
  end

  if not table.components then
    table.components = {}
  end

  if not table.components.PresetInstanceComponent then
    table.components.PresetInstanceComponent = {
      name = preset_name
    }
  end
  return create_entity(context, name, table)
end

function load_preset_instance(context, entity)
  --local preset_instance_entity = utility.create_entity(context, name)
  entity.ModelComponent.asset = cpp.load_model("assets/model/editor_preset_instance.fbx")
  spawn_preset_dummies(context, entity, entity.EditorMetaComponent.name)
  return entity
end

-- Load scene into context
function load(context)
  create(context)
  local scene = cpp.load_scene(context.path)
  if scene then
    for k, v in pairs(scene) do
      context[k] = v
    end
  end

  gui.clear()
  local scene_entities = context.entities
  local entities = {}
  for name, entity_desc in pairs(scene_entities) do
    utility.append(entities, create_empty_entity(context, name))
  end
  for i, entity in ipairs(entities) do
    load_entity(context, entity, scene_entities[entity.EditorMetaComponent.name])
  end

  local mesh_nodes = context.entity_manager:get_entities({"MeshNodeComponent"})
  for i, node in ipairs(mesh_nodes) do
    for j, neighbor_id in ipairs(node.MeshNodeComponent.neighbors) do
      local neighbor = context.entity_manager:get_entity(neighbor_id)
      mesh.connect(context, node, neighbor)
    end
  end

  for name, preset_desc in pairs(context.presets) do
    gui.refresh_preset(context, name)
  end
end

-- Generate empty scene
function create(context)
  context.entities = {}
  context.presets = {}
  context.selected_entities = {}
  context.selected_preset = nil
  context.meshes = {}

  local cameras = context.entity_manager:get_entities({"ProjectionComponent", "TransformComponent"})
  local camera_transform
  if #cameras > 0 then
    camera_transform = cpp.TransformComponent.new()
    camera_transform.transform:set_model_matrix(
      cameras[1].TransformComponent.transform:get_model_matrix(),
      context.frame_context
    )
  else
    camera_transform =
      serializer.create_reflectable(
      context,
      cpp.TransformComponent,
      {
        transform = {
          position = {0, 0, 0}
        }
      }
    )
  end

  context.entity_manager:clear()

  -- Create some miscellaneous entities
  -- Camera
  local camera = context.entity_manager:create_entity()
  camera.TransformComponent = camera_transform

  local projection =
    serializer.create_reflectable(
    context,
    cpp.ProjectionComponent,
    {
      aspectRatio = 1280 / 720,
      field_of_view = 90,
      near = 0.1,
      far = 1000
    }
  )
  projection.projection_matrix =
    cpp.perspective(
    cpp.radians(projection.field_of_view) / projection.aspectRatio,
    projection.aspectRatio,
    projection.near,
    projection.far
  )
  camera.ProjectionComponent = projection
  camera.DebugCameraComponent = cpp.DebugCameraComponent.new()
  camera.DebugCameraComponent.speed = 10

  -- Directional light
  local light = context.entity_manager:create_entity()
  light.DirectionalLightComponent =
    serializer.create_reflectable(
    context,
    cpp.DirectionalLightComponent,
    {
      direction = {0, 1, -1},
      has_shadow = true,
      color = {1, 1, 1}
    }
  )
end

-- Create a preset
function create_preset(context, name, entity_ids)
  local preset = {}

  local entities = context.entity_manager:get_entities_from_handles(entity_ids)
  local average_position = serializer.create_reflectable(context, cpp.vec3, {0, 0, 0})
  for i, entity in ipairs(entities) do
    cpp.vec3_add(average_position, entity.TransformComponent.transform:get_position())
  end
  cpp.vec3_divide(average_position, #entities)

  local tmp_transform = cpp.Transform.new()
  tmp_transform:set_position(average_position, context.frame_context)
  local preset_space = cpp.mat4_inverse(tmp_transform:get_model_matrix())

  for i, entity in ipairs(entities) do
    local entity_name = entity.EditorMetaComponent.name
    local preset_entity = utility.copy_table(context.entities[entity_name])
    local transform = cpp.Transform.new()
    transform:set_model_matrix(
      cpp.mat4_multiply(preset_space, entity.TransformComponent.transform:get_model_matrix()),
      context.frame_context
    )
    preset_entity.relative_transform = serializer.create_reflectable_table(context, transform)

    preset_entity.AttachToEntityComponent = nil
    preset[entity_name] = preset_entity
  end
  context.presets[name] = preset
  gui.refresh_preset(context, name)

  context.selected_preset = name
  gui.refresh_preset_selection(context)
  return preset
end

-- Remove existing preset
function remove_preset(context, name)
  context.presets[name] = nil
  gui.remove_preset(context, name)

  if context.selected_preset == name then
    refresh_preset_selection(context)
    context.selected_preset = nil
  end
end

-- Remove a preset instance
function remove_preset_instance(context, entity_id)
  deselect_entity(context, entity_id)
  local entity = context.entity_manager:get_entity(entity_id)
  for i, dummy in ipairs(entity.PresetInstanceComponent.dummies) do
    context.entity_manager:destroy_entity(context.entity_manager:get_entity(dummy))
  end
  utility.set_scene_entity(context, entity_id, nil)
  context.entity_manager:destroy_entity(entity)
  local key = utility.find_value(context.clipboard, entity_id)
  if key ~= nil then
    context.clipboard[key] = nil
  end
end

-- Spawn dummy entities for a preset instance
function spawn_preset_dummies(context, entity, preset_instance_name)
  local preset_description = context.presets[entity.PresetInstanceComponent.name]
  if not preset_description then
    print(
      "Preset instance '" ..
        preset_instance_name .. "' points to nonexistent preset '" .. entity.PresetInstanceComponent.name .. "'."
    )
    return
  end
  local dummies = entity.PresetInstanceComponent.dummies

  for ent_name, ent_desc in pairs(preset_description) do
    spawn_preset_dummy(context, dummies, entity, preset_instance_name, ent_name, ent_desc, ent_desc.relative_transform)
  end
end

-- Spawns a SINGLE dummy entity (is recursive in case the dummy is an instance)
function spawn_preset_dummy(
  context,
  dummies,
  root_entity,
  preset_instance_name,
  ent_name,
  ent_desc,
  cumulative_transform)
  local preset_dummy_entity = utility.create_entity(context)
  utility.append(dummies, preset_dummy_entity.get_handle)

  preset_dummy_entity.PresetDummyComponent =
    serializer.create_reflectable(context, cpp.PresetDummyComponent, {parent = preset_instance_name})
  preset_dummy_entity.AttachToEntityComponent =
    serializer.create_reflectable(
    context,
    cpp.AttachToEntityComponent,
    {
      entity_handle = preset_instance_name,
      offset_transform = cumulative_transform
    }
  )

  gui.refresh_entity(context, preset_dummy_entity)

  -- If the current dummy is ALSO an instance, recursively add dummies to the original instance
  if ent_desc.components.PresetInstanceComponent ~= nil then
    preset_dummy_entity.ModelComponent.asset = cpp.load_model("assets/model/editor_preset_instance_fake.fbx")

    local recursive_preset_description = context.presets[ent_desc.components.PresetInstanceComponent.name]

    for recursive_ent_name, recursive_ent_desc in pairs(recursive_preset_description) do
      local cumulative_transform_mat = serializer.create_reflectable(context, cpp.mat4, cumulative_transform)
      local relative_transform_mat =
        serializer.create_reflectable(context, cpp.mat4, recursive_ent_desc.relative_transform)

      local total_transform_mat = cpp.mat4_multiply(cumulative_transform_mat, relative_transform_mat)

      local total_transform_table = serializer.create_reflectable_table(context, total_transform_mat)

      spawn_preset_dummy(
        context,
        dummies,
        root_entity,
        preset_instance_name,
        recursive_ent_name,
        recursive_ent_desc,
        total_transform_table
      )
    end
  else
    -- Copy over some visual data into dummy
    for comp_name, comp_desc in pairs(ent_desc.components) do
      if comp_name == "ModelComponent" then
        if comp_desc.asset ~= nil then
          preset_dummy_entity.ModelComponent.asset = cpp.load_model(comp_desc.asset)

          preset_dummy_entity.StaticComponent =
            serializer.create_reflectable(
            context,
            cpp.StaticComponent,
            {
              contents = {
                path = comp_desc.asset,
                type = "Triangle"
              }
            }
          )
        end
      elseif comp_name == "ParticlesComponent" then
        preset_dummy_entity.ParticlesComponent =
          serializer.create_reflectable(context, cpp.ParticlesComponent, comp_desc)
      elseif comp_name == "PointLightComponent" then
        preset_dummy_entity.PointLightComponent =
          serializer.create_reflectable(context, cpp.PointLightComponent, comp_desc)
      end
    end
  end
end

-- Update all preset instances after changing
-- their preset of origin.
function refresh_all_preset_instances(context)
end

-- Apply transform to selection
function transform_selection(context)
  local selections = {}
  for k, v in ipairs(context.selected_entities) do
    if context.entity_manager:is_valid(v.id) then
      utility.append(selections, v)
    end
  end

  if #selections == 0 then
    return
  end

  for i, selection in ipairs(selections) do
    local entity = context.entity_manager:get_entity(selection.id)

    local after = serializer.create_reflectable_table(context, entity.TransformComponent)

    local before = utility.get_scene_entity_component(context, selection.id, "TransformComponent")
    if not before then
      before = serializer.create_reflectable_table(context, cpp.TransformComponent.new())
    end
    utility.append(
      context.actions,
      {
        type = "transform",
        entity_name = entity.EditorMetaComponent.name,
        before = before,
        after = after
      }
    )
  end
end

-- Create a clone and return it
function clone_entity(context, entity, name)
  if entity.PresetDummyComponent then
    return
  end

  local clone
  if entity.PresetInstanceComponent then
    local preset_name = entity.PresetInstanceComponent.name
    local scene_preset = context.presets[preset_name]
    clone = create_preset_instance(context, preset_name, name)
  else
    local scene_entity = utility.get_scene_entity(context, entity.get_handle)
    clone = create_entity(context, name, scene_entity)
    utility.set_scene_entity(context, clone.get_handle, utility.copy_table(scene_entity))
  end

  clone.TransformComponent.transform:translate(
    serializer.create_reflectable(context, cpp.vec3, {0, 0, 1}),
    context.frame_context
  )
  local scene_clone = utility.get_scene_entity(context, clone.get_handle)
  scene_clone.components.TransformComponent = serializer.create_reflectable_table(context, clone.TransformComponent)
  for i, v in pairs(context.selected_entities) do
    utility.append(context.events, {type = "deselect", entity_id = v.id})
  end
  utility.append(context.events, {type = "select", entity_id = clone.get_handle})

  if scene_clone.components.MeshNodeComponent then
    mesh.remove_vertex(context, clone)
  end
  if scene_clone.components.AttachToEntityComponent then
    clone.AttachToEntityComponent = nil
    scene_clone.components.AttachToEntityComponent = nil
  end
  return clone
end

-- Remove entity
function remove_entity(context, entity_id)
  local entity = context.entity_manager:get_entity(entity_id)
  if entity.PresetInstanceComponent then
    remove_preset_instance(context, entity_id)
    return
  end
  if entity.MeshNodeComponent then
    mesh.remove_vertex(context, entity)
  end
  gui.remove_entity(context, entity_id)
  deselect_entity(context, entity_id)
  utility.set_scene_entity(context, entity_id, nil)
  context.entity_manager:destroy_entity(context.entity_manager:get_entity(entity_id))

  local key = utility.find_value(context.clipboard, entity_id)
  if key ~= nil then
    context.clipboard[key] = nil
  end
end

-- Deselect all entities and select this one instead
function select_entity_exclusive(context, entity_id)
  for i, v in pairs(context.selected_entities) do
    deselect_entity(context, v.id)
  end
  select_entity(context, entity_id)
end

-- Select this entity
function select_entity(context, entity_id)
  local key = nil
  for k, v in pairs(context.selected_entities) do
    if v.id == entity_id then
      key = k
      break
    end
  end

  if key == nil then
    local entity = context.entity_manager:get_entity(entity_id)
    entity.EditorSelectedComponent = cpp.EditorSelectedComponent.new()
    entity.HighlightComponent = cpp.HighlightComponent.new()
    gui.refresh_selection(context, entity)

    utility.append(
      context.selected_entities,
      {
        id = entity_id,
        transform = entity.TransformComponent.transform:get_model_matrix()
      }
    )
  end
end

-- Deselect this entity
function deselect_entity(context, entity_id)
  local key = nil
  for k, v in pairs(context.selected_entities) do
    if v.id == entity_id then
      key = k
      break
    end
  end

  if key ~= nil then
    context.selected_entities[key] = nil
    local entity = context.entity_manager:get_entity(entity_id)
    entity.EditorSelectedComponent = nil
    entity.HighlightComponent = nil
    gui.remove_selection(context, entity)
  end
end
