local event_handler = require("editor/event_handler")
local editor_context = require("editor/context")

-- EDITOR
-- The functions in this module are the only ones that need to be exposed to the client.

-- Returns a new editor context.
function create_context(entity_manager, frame_context, component_names)
  return editor_context.create(entity_manager, frame_context, component_names)
end

-- Handle a user input event.
-- All interaction with the editor is done via events. See "event.lua" for
-- available events and how to create them.
function handle_event(context, event)
  event_handler.handle_event(context, event)
end
