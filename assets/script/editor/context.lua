local gui = require("editor/gui")
local scene = require("editor/scene")
local history = require("editor/history")
local utility = require("editor/utility")
-- CONTEXT
-- Context utilities

local hidden_components = {
  EditorMetaComponent = true
}

-- Create a new editor context
function create(entity_manager, frame_context, component_names)
  local context = {
    selected_entities = {},
    entity_manager = entity_manager, -- cor::EntityManager*
    frame_context = frame_context, -- cor::FramContext*
    components = {}, -- The names of all registered components
    visible_components = {}, -- The names of all components visible in GUI
    hidden_components = hidden_components, -- Map of components not visible in gui. Keys are the names
    events = {}, -- If you want to trigger events internally, add them to this list.
    actions = {}, -- If you want to trigger actions, add them to this list.
    path = "assets/script/scene/default_editor_scene.lua" -- File path to scene file
  }

  -- Copy over the names to a table, because sol
  -- insists on pushing the vector as a pointer
  -- instead of by value.
  for i, name in ipairs(component_names) do
    context.components[i] = name
    if not hidden_components[name] then
      utility.append(context.visible_components, name)
    end
  end

  gui.create(context)
  scene.create(context)
  history.create(context)
  return context
end
