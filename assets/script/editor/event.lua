local cpp = require("cpp")

-- EVENT
-- Describes all events that can be sent to the editor using handle_event.

-- All events contain the following data.
event = {
  type = "name_of_the_event_type" -- The event type name. E.g. for a load_scene event its value is "load_scene".
}

-- Load a new scene.
-- The path is specified in context
load_scene = {}

-- Save current scene to file
-- The path is specified in context
save_scene = {}

-- Notify editor when a value in the GUI has changed.
gui_edit = {
  path = {"path", "parts", "to", "entry"} -- The parts of the path to the GUI entry.
}

-- Create a default, empty entity.
create_entity = {}

-- Remove entity
remove_entity = {
  entity_id = cpp.EntityHandle.new()
}

-- Select entity
select = {
  entity_id = cpp.EntityHandle.new()
}

-- Deselect entity
deselect = {
  entity_id = cpp.EntityHandle.new()
}

-- Apply transform to selections
transform = {}

-- Create preset from selection
create_preset = {}

-- Create preset instance of selected preset
create_preset_instance = {}

-- Put selection into clipboard
copy = {}

-- Clone entities from clipboard
paste = {}

-- Delete selected entities
delete = {}

-- Undo latest action
undo = {}

-- Redo latest undone action
redo = {}

-- Attach selection to target entity
attach = {
  target = cpp.EntityHandle.new()
}

-- Detach selection from all entities
detach = {}

-- Connect selected mesh build nodes with eachother
connect_mesh_build_nodes = {}

-- Disconnect selected mesh build nodes from eachother
disconnect_mesh_build_nodes = {}
