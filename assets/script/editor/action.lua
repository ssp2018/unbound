local cpp = require("cpp")

-- ACTION
-- Describes all actions that can be stored in history

-- All actions contain the following data.
action = {
  -- The action type name. E.g. for a create_entity action its value is "create_entity".
  type = "name_of_the_action_type"
}

-- Create a default entity
create_entity = {
  entity_name = "the_name"
}

-- Remove an entity
remove_entity = {
  entity_name = "name"
}

-- Edit the value of *something*.
-- If the scene description of the value
-- was nil before applying the action,
-- "is_new" is set to true.
-- "is_new", "before" and "after" are optional and are
-- interpreted as nil if not included.
edit_value = {
  path = {"path", "to", "something"},
  is_new = true,
  before = "value_before", -- May be of any type
  after = "value_after" -- May be of any type
}

-- Toggle a component of an entity.
-- "before" and "after" specify the component's data
-- in a scene description format.
-- "before" and "after" are optional and are
-- interpreted as nil if not included.
toggle_component = {
  entity_name = "name",
  component = "component_name",
  before = {},
  after = {}
}

-- Create a preset from entities
create_preset = {
  name = "preset_name",
  entity_names = {}
}

-- Create a preset instance from a preset
create_preset_instance = {
  preset_name = "preset_name",
  entity_name = "instance_name"
}

-- Transform an entity
-- "before" and "after" is
-- the transform before and after
-- in table form.
transform = {
  entity_name = "name",
  before = {},
  after = {}
}

-- Clone entity
clone = {
  entity_name = "name",
  clone_name = "name"
}

-- Rename an entity
rename_entity = {
  before = "name_before",
  after = "name_after"
}

-- Rename a preset
rename_preset = {
  before = "name_before",
  after = "name_after"
}

-- Delete entity
delete = {
  entity_name = "name"
}

-- Attach entity to target
attach = {
  entity_name = "name",
  target_name = "name"
}

-- Detach entity from all entities
detach = {entity_name = "name"}

-- Connect entity_name to target_name
connect_mesh_build_nodes = {
  entity_name = "name",
  target_name = "name"
}

-- Disconnect entity_name from target_name
disconnect_mesh_build_nodes = {
  entity_name = "name",
  target_name = "name"
}
