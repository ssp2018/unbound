local cpp = require("cpp")
local utility = require("editor/utility")
local serializer = require("editor/serializer")

-- MESH
-- Utilities for mesh creation.

-- Get entity's mesh.
-- Return nil if none exists.
local function get_mesh_if(context, entity)
  return context.meshes[entity.MeshNodeComponent.mesh]
end

-- Get entity's mesh, or create one if
-- none exists.
local function get_mesh(context, entity)
  local mesh_name = entity.MeshNodeComponent.mesh
  local mesh = context.meshes[mesh_name]
  if not mesh then
    mesh = {}
    mesh.builder = cpp.MeshBuilder.new()
    mesh.faces = {}
    mesh.nodes = {}
    mesh.node_ids = {}
    mesh.node_names = {}
    context.meshes[mesh_name] = mesh
  end
  return mesh
end

-- Get entity's vertex id.
-- Return nil if none exists.
local function get_vertex_id_if(context, entity)
  local mesh = get_mesh_if(context, entity)
  if not mesh then
    return nil
  end

  return mesh.node_ids[entity.EditorMetaComponent.name]
end

-- Get entity's vertex id, or create one if
-- none exists.
local function get_vertex_id(context, entity)
  local mesh = get_mesh(context, entity)

  local name = entity.EditorMetaComponent.name
  local id = mesh.node_ids[name]
  if id == nil then
    id = mesh.builder:create_vertex()
    mesh.node_ids[name] = id
    mesh.node_names[id] = name
  end
  return id
end

local function add_neighbor(context, entity, neighbor)
  if utility.find_value(entity.MeshNodeComponent.neighbors, neighbor.get_handle) == nil then
    utility.append(entity.MeshNodeComponent.neighbors, neighbor.get_handle)
    local comp = utility.get_scene_entity(context, entity.get_handle).components.MeshNodeComponent
    if not comp.neighbors then
      comp.neighbors = {}
    end
    utility.append(comp.neighbors, neighbor.EditorMetaComponent.name)
    return true
  end
  return false
end

local function remove_neighbor(context, entity, neighbor)
  local neighbors = entity.MeshNodeComponent.neighbors
  local key = utility.find_value(neighbors, neighbor.get_handle)
  if key == nil then
    return false
  end
  neighbors[key] = nil

  local comp = utility.get_scene_entity(context, entity.get_handle).components.MeshNodeComponent
  if not comp.neighbors then
    return false
  end

  key = utility.find_value(comp.neighbors, neighbor.EditorMetaComponent.name)
  if key == nil then
    return false
  end
  comp.neighbors[key] = nil

  return true
end

local function create_face(context, mesh, face_id)
  local face = mesh.builder:get_face(face_id)

  local entity = context.entity_manager:create_entity()
  entity.ModelComponent =
    serializer.create_reflectable(
    context,
    cpp.ModelComponent,
    {
      asset = "assets/model/triangle.fbx"
    }
  )
  entity.StickyTriangleComponent =
    serializer.create_reflectable(
    context,
    cpp.StickyTriangleComponent,
    {
      points = {
        mesh.node_names[face[1]],
        mesh.node_names[face[2]],
        mesh.node_names[face[3]]
      }
    }
  )
  entity.TransformComponent = cpp.TransformComponent.new()
  mesh.faces[face_id] = entity.get_handle
end

local function destroy_face(context, mesh, face_id)
  local face = mesh.faces[face_id]
  context.entity_manager:destroy_entity(context.entity_manager:get_entity(face))
  mesh.faces[face_id] = nil
end

function connect(context, source_entity, target_entity)
  local source_id = get_vertex_id(context, source_entity)
  local target_id = get_vertex_id(context, target_entity)
  if source_id == target_id then
    return false
  end
  local mesh = get_mesh(context, source_entity)

  local is_getting_connected = add_neighbor(context, source_entity, target_entity)
  add_neighbor(context, target_entity, source_entity)

  local face_ids = mesh.builder:connect_vertices(source_id, target_id)
  for i, face_id in ipairs(face_ids) do
    create_face(context, mesh, face_id)
  end
  return is_getting_connected
end

function disconnect(context, source_entity, target_entity)
  local source_id = get_vertex_id(context, source_entity)
  local target_id = get_vertex_id(context, target_entity)
  local mesh = get_mesh(context, source_entity)

  local is_getting_disconnected = remove_neighbor(context, source_entity, target_entity)
  remove_neighbor(context, target_entity, source_entity)

  local face_ids = mesh.builder:disconnect_vertices(source_id, target_id)
  for i, face_id in ipairs(face_ids) do
    destroy_face(context, mesh, face_id)
  end
  return is_getting_disconnected
end

function remove_vertex(context, entity)
  local id = get_vertex_id_if(context, entity)
  if id == nil then
    return
  end

  local neighbors = {}
  for i, v in ipairs(entity.MeshNodeComponent.neighbors) do
    utility.append(neighbors, context.entity_manager:get_entity(v))
  end
  for i, neighbor in ipairs(neighbors) do
    disconnect(context, entity, neighbor)
  end

  local name = entity.EditorMetaComponent.name
  local mesh = get_mesh(context, entity)
  mesh.node_ids[name] = nil
  mesh.node_names[id] = nil
  mesh.builder:destroy_vertex(id)
end

function get_meshes(scene)
  local meshes = {}
  for name, entity in pairs(scene.entities) do
    if entity.components.MeshNodeComponent then
      local node = entity.components.MeshNodeComponent
      local mesh = meshes[node.mesh]
      if not mesh then
        mesh = {
          positions = {},
          nodes = {},
          name_to_index = {}
        }
        meshes[node.mesh] = mesh
      end
      utility.append(mesh.positions, entity.components.TransformComponent.transform.position)
      utility.append(mesh.nodes, utility.copy_table(node.neighbors))
      mesh.name_to_index[name] = #mesh.positions
    end
  end

  for mesh_name, mesh in pairs(meshes) do
    for i, node in ipairs(mesh.nodes) do
      for j, neighbor in ipairs(node) do
        node[j] = mesh.name_to_index[node[j]]
      end
    end
  end

  return meshes
end
