local action_handler = require("editor/action_handler")
local utility = require("editor/utility")

-- HISTORY
-- Responsible for undo/redo operations
-- and holding the action history.

-- Push an action group to the history list.
-- An action group is simply a table of actions
-- from the same tick. If multiple actions are
-- pushed in the same tick, those are regarded
-- as one group. Undos and redos are performed
-- on whole groups, which means that multiple actions
-- can be undone in one swoop.
local function push_action_group(context, group)
  if #group == 0 then
    return
  end
  local history = context.history

  local index = context.history_index + 1

  history[index] = group

  if index < #history then
    for i = index + 1, #history do
      history[i] = nil
    end
  end

  context.history_index = index
end

-- Undo top action group at history index and move back
function undo(context)
  if context.history_index == 0 then
    return
  end

  local actions = context.history[context.history_index]
  for i, action in ipairs(actions) do
    action_handler.handle_action(context, action, false)
  end
  context.history_index = context.history_index - 1
end

-- Redo top action group at history index and move forward
function redo(context)
  if context.history_index >= #context.history then
    return
  end
  context.history_index = context.history_index + 1
  local actions = context.history[context.history_index]
  for i, action in ipairs(actions) do
    action_handler.handle_action(context, action, true)
  end
end

-- Fill history with the actions in context.actions
function update(context)
  local num_actions = #context.actions
  local action_group = {}
  while num_actions > 0 do
    local actions = context.actions
    context.actions = {}

    for i, action in ipairs(actions) do
      utility.append(action_group, action)
      action_handler.handle_action(context, action, true)
    end
    num_actions = #context.actions
  end
  push_action_group(context, action_group)
end

-- Create history context
function create(context)
  context.history = {}
  context.history_index = 0
end
