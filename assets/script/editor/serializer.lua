local cpp = require("cpp")
local ref = require("reflect")
local utility = require("editor/utility")

-- SCENE
-- Responsible for common scene operations

-- Helper for loading an asset
local function load_asset(asset, path)
  local type_name = ref.type_name(asset)
  if path and type(path) == "string" then
    return cpp[type_name].new(cpp.FilePath.new(path))
  end
  return cpp[type_name].new()
end

-- Asset loader func
local function asset_loader(context, asset, desc)
  return load_asset(asset, desc)
end

-- Asset writer func
local function asset_writer(context, data)
  return data:get_path():get_string()
end

-- Custom handlers for loading userdata from table
local custom_loaders = {}
custom_loaders = {
  TransformComponent = function(context, component, desc)
    custom_loaders.Transform(context, component.transform, desc.transform)
    return component
  end,
  ModelComponent = function(context, component, desc)
    component.asset = cpp.ModelAsset.new(cpp.FilePath.new(desc.asset))
    return component
  end,
  Transform = function(context, transform, desc)
    if not desc then
      return
    end

    if desc.position then
      transform:set_position(create_reflectable(context, cpp.vec3, desc.position), context.frame_context)
    end

    if desc.rotation then
      cpp.set_transform_rotation(transform, create_reflectable(context, cpp.quat, desc.rotation), context.frame_context)
    end

    -- print(desc.scale)
    -- print(type(desc.scale))
    if desc.scale and type(desc.scale) == "number" then
      transform:set_scale(desc.scale, context.frame_context)
    end
    return transform
  end,
  StaticComponentContents = function(context, data, desc)
    local path = ""
    if desc.path then
      path = desc.path
    end
    return cpp.load_static_component_contents(context.frame_context, desc.type, path)
  end,
  CharacterControllerComponentContents = function(context, data, desc)
    if desc.height then
      data:set_height(context.frame_context, desc.height)
    end
    if desc.radius then
      data:set_radius(context.frame_context, desc.radius)
    end
    if desc.mass then
      data:set_mass(context.frame_context, desc.mass)
    end
    if desc.gravity then
      data:set_gravity(create_reflectable(context, cpp.vec3, desc.gravity))
    end
    if desc.turning_speed then
      data:set_turning_speed(desc.turning_speed)
    end
    if desc.offset_transform then
      data:set_offset_transform(create_reflectable(context, cpp.mat4, desc.offset_transform))
    end
    if desc.use_navmesh_collision ~= nil then
      data:set_use_navmesh_collision(desc.use_navmesh_collision)
    end
    return data
  end,
  mat4 = function(context, data, desc)
    return create_reflectable(context, cpp.Transform, desc):get_model_matrix()
  end,
  -- AttachToEntityComponent = function(context, component, desc)
  --   component.entity_handle = desc.entity_handle
  --   custom_loaders.Transform(context, component.offset_transform, desc.offset_transform)
  --   return component
  -- end,
  EntityHandle = function(context, component, desc)
    local entity = utility.get_entity(context, desc)
    if entity then
      return entity.get_handle
    else
      print("Failed to deserialize entity handle: ")
      print(desc)
      return cpp.Entity.new().get_handle
    end
  end,
  SlowMotionFactor = function(context, factor, desc)
    factor:set_factor(desc)
    return factor
  end,
	AnimationStateMachine = function( context, state_machine, desc )
		if desc.name and desc.start_state then
			state_machine:set_machine_and_state(desc.name, desc.start_state)
			return state_machine
		end
	end, 
  Object = function(context, object, desc)
    if desc.path and desc.name then
      return cpp.Object.new(desc.path, desc.name)
    end
    return object
  end,
  AudioComponent = function(context, audio_component, desc)
    cpp.create_audio_class(desc, audio_component)

    -- local test_table = {}
    -- cpp.write_audio_class(test_table, audio_component)
    -- print(test_table)

    return audio_component
  end,
  ModelAsset = asset_loader,
  TextureAsset = asset_loader,
  MaterialAsset = asset_loader,
  MeshAsset = asset_loader,
  FontAsset = asset_loader,
  ShaderAsset = asset_loader,
  SoundBufferAsset = asset_loader,
  AnimationBlueprintAsset = asset_loader
}

-- Custom handlers for converting userdata to table
local custom_writers = {}
custom_writers = {
  Transform = function(context, data)
    local position = data:get_position()
    local rotation = data:get_rotation()
    local scale = data:get_scale()
    return {
      position = {position.x, position.y, position.z},
      rotation = {rotation.x, rotation.y, rotation.z, rotation.w},
      scale = scale
    }
  end,
  EntityHandle = function(context, data)
    return utility.get_entity_name(context, data)
  end,
  StaticComponentContents = function(context, data)
    return cpp.write_static_component_contents(data)
  end,
  SlowMotionFactor = function(context, data)
    return data:get_factor()
  end,
  CharacterControllerComponentContents = function(context, data)
    return {
      height = data:get_height(),
      radius = data:get_radius(),
      mass = data:get_mass(),
      use_navmesh_collision = data:get_use_navmesh_collission(),
      gravity = create_reflectable_table(context, data:get_gravity()),
      turning_speed = data:get_turning_speed(),
      offset_transform = create_reflectable_table(context, data:get_offset_transform())
    }
  end,
  mat4 = function(context, data)
    local transform = cpp.Transform.new()
    transform:set_model_matrix(data, context.frame_context)
    return create_reflectable_table(context, transform)
  end,
	AnimationStateMachine = function( context, state_machine )
		return {
			name = state_machine:get_machine(),
			start_state = state_machine:get_state(),
		}
	 end, 
  Object = function(context, object)
    return {
      path = object:get_script_path(),
      name = object:get_name()
    }
  end,
  AudioComponent = function(context, audio_component)
    local desc = {}
    cpp.write_audio_class(desc, audio_component)
    return desc
  end,
  ModelAsset = asset_writer,
  TextureAsset = asset_writer,
  MaterialAsset = asset_writer,
  MeshAsset = asset_writer,
  FontAsset = asset_writer,
  ShaderAsset = asset_writer,
  SoundBufferAsset = asset_writer,
  AnimationBlueprintAsset = asset_writer
}

-- Load table into reflectable
function load_reflectable_helper(context, reflectable, table)
  if not table or not reflectable then
    return nil
  end

  local loader = custom_loaders[ref.type_name(reflectable)]
  if loader then
    return loader(context, reflectable, table)
  end

  if type(table) ~= "table" then
    return nil
  end

  local members = ref.members(reflectable)
  if not members then
    if ref.has_pairs(reflectable) then
      local element_type = ref.element_type(reflectable)
      if element_type then
        for key, v in pairs(table) do
          local element = element_type.new()
          local load_v = load_reflectable_helper(context, element, v)
          if load_v == nil then
            reflectable[key] = v
          else
            reflectable[key] = load_v
          end
        end
      else
        for key, v in pairs(table) do
          local load_v = load_reflectable_helper(context, reflectable[key], v)
          if load_v == nil then
            reflectable[key] = v
          else
            reflectable[key] = load_v
          end
        end
      end
      return reflectable
    else
      return nil
    end
  end

  for k, v in pairs(table) do
    local key = nil
    if type(k) == "number" then
      key = members[k]
    else
      key = k
      if reflectable[key] == nil then
        local n = tonumber(key)
        if n ~= nil then
          key = members[n]
        end
      end
    end

    local load_v = load_reflectable_helper(context, reflectable[key], v)
    if load_v == nil then
      reflectable[key] = v
    else
      reflectable[key] = load_v
    end
  end
  return reflectable
end

-- Load value into reflectable
function load_reflectable(context, reflectable, value)
  if type(value) == "table" or type(value) == "userdata" then
    return load_reflectable_helper(context, reflectable, value)
  else
    local loader = custom_loaders[ref.type_name(reflectable)]
    if loader then
      return loader(context, reflectable, value)
    else
      return value
    end
  end
end

-- Create reflectable of specified type and load table into it.
function create_reflectable(context, type, table)
  return load_reflectable(context, type.new(), table)
end

-- Convert reflectable to table
function create_reflectable_table(context, reflectable)
  local writer = custom_writers[ref.type_name(reflectable)]
  if writer then
    return writer(context, reflectable)
  end

  if not reflectable then
    return nil
  end
  local members = ref.members(reflectable)
  if not members then
    return nil
  end

  local table = {}
  for i, member in ipairs(members) do
    local child = create_reflectable_table(context, reflectable[member])
    if child then
      table[member] = child
    else
      table[member] = reflectable[member]
    end
  end
  return table
end
