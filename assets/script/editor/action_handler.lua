local scene = require("editor/scene")
local action_types = require("editor/action")
local utility = require("editor/utility")
local gui = require("editor/gui")
local cpp = require("cpp")
local serializer = require("editor/serializer")
local mesh = require("editor/mesh")

-- ACTION HANDLER
-- Handlers for all actions and some
-- helper functions

-- Handlers for all actions
-- If you need to keep data between
-- apply and undo, just store this data
-- in the action table. See create_entity
-- for an example.
local handlers = {}
handlers = {
  create_entity = {
    apply = function(context, action)
      local entity = utility.create_entity(context, action.entity_name)
      local transform
      if action.transform then
        transform = action.transform
        action.transform = nil
        entity.TransformComponent = serializer.create_reflectable(context, cpp.TransformComponent, transform)
      else
        transform = serializer.create_reflectable_table(context, entity.TransformComponent)
      end

      utility.set_scene_entity(
        context,
        entity.get_handle,
        {
          components = {
            TransformComponent = transform
          }
        }
      )
      gui.refresh_entity(context, entity)
      scene.select_entity_exclusive(context, entity.get_handle)
    end,
    undo = function(context, action)
      local entity = utility.get_entity(context, action.entity_name)
      action.transform = utility.get_scene_entity(context, entity.get_handle).components.TransformComponent
      scene.remove_entity(context, entity.get_handle)
    end
  },
  toggle_component = {
    apply = function(context, action)
      local entity = utility.get_entity(context, action.entity_name)
      if action.after then
        entity[action.component] = cpp[action.component].new()
      else
        entity[action.component] = nil
      end
      local scene_entity = context.entities[action.entity_name]
      scene_entity.components[action.component] = action.after
      gui.refresh_entity(context, entity)
    end,
    undo = function(context, action)
      local entity = utility.get_entity(context, action.entity_name)
      if action.after then
        entity[action.component] = nil
      else
        local component = cpp[action.component].new()
        serializer.load_reflectable(context, component, action.before)
        entity[action.component] = component
      end
      local scene_entity = context.entities[action.entity_name]
      scene_entity.components[action.component] = action.before
      gui.refresh_entity(context, entity)
    end
  },
  edit_value = {
    apply = function(context, action)
      local info = gui.get_path_info(context, action.path)
      if info.type == "entity" then
        utility.set_table_value(info.scene_entity["components"], action.path, action.after, info.content_offset)
        local component = action.path[info.content_offset]
        --local value = utility.get_table_value(info.entity, action.path, info.content_offset)
        serializer.load_reflectable(context, info.entity[component], info.scene_entity["components"][component])
        --   utility.set_table_value(
        --   info.entity,
        --   action.path,
        --   serializer.load_reflectable(context, info.entity[component], info.scene_entity["components"][component]),
        --   info.content_offset
        -- )
        gui.refresh_entity(context, info.entity)
      else
        print("Ignoring unknown path info:")
        print(info)
        print("Path:")
        print(action.path)
      end
    end,
    undo = function(context, action)
      local info = gui.get_path_info(context, action.path)
      if info.type == "entity" then
        local value = utility.get_table_value(info.entity, action.path, info.content_offset)
        utility.set_table_value(
          info.entity,
          action.path,
          serializer.load_reflectable(context, value, action.before),
          info.content_offset
        )

        local old_scene_value
        if action.is_new then
          old_scene_value = nil
        else
          old_scene_value = action.before
        end
        utility.set_table_value(info.scene_entity["components"], action.path, old_scene_value, info.content_offset)
        gui.refresh_entity(context, info.entity)
      else
        print("Ignoring unknown path info:")
        print(info)
        print("Path:")
        print(action.path)
      end
    end
  },
  create_preset = {
    apply = function(context, action)
      local ids = {}
      for i, v in ipairs(action.entity_names) do
        utility.append(ids, utility.get_entity(context, v).get_handle)
      end
      scene.create_preset(context, action.name, ids)
    end,
    undo = function(context, action)
      scene.remove_preset(context, action.name)
    end
  },
  create_preset_instance = {
    apply = function(context, action)
      local entity = scene.create_preset_instance(context, action.preset_name, action.entity_name)

      local transform
      if action.transform then
        transform = action.transform
        action.transform = nil
        entity.TransformComponent = serializer.create_reflectable(context, cpp.TransformComponent, transform)
      else
        transform = serializer.create_reflectable_table(context, entity.TransformComponent)
      end

      local scene_entity = utility.get_scene_entity(context, entity.get_handle)
      scene_entity.components.TransformComponent = transform
      gui.refresh_entity(context, entity)
      scene.select_entity_exclusive(context, entity.get_handle)
    end,
    undo = function(context, action)
      local entity = utility.get_entity(context, action.entity_name)
      action.transform = utility.get_scene_entity(context, entity.get_handle).components.TransformComponent
      scene.remove_entity(context, entity.get_handle)
    end
  },
  transform = {
    apply = function(context, action)
      local scene_entity = context.entities[action.entity_name]
      local entity = utility.get_entity(context, action.entity_name)
      local table = action.after
      entity.TransformComponent = serializer.create_reflectable(context, cpp.TransformComponent, table)
      scene_entity.components.TransformComponent = table
      gui.refresh_entity(context, entity)
    end,
    undo = function(context, action)
      local scene_entity = context.entities[action.entity_name]
      local entity = utility.get_entity(context, action.entity_name)
      local table = action.before
      entity.TransformComponent = serializer.create_reflectable(context, cpp.TransformComponent, table)
      scene_entity.components.TransformComponent = table
      gui.refresh_entity(context, entity)
    end
  },
  clone = {
    apply = function(context, action)
      scene.clone_entity(context, utility.get_entity(context, action.entity_name), action.clone_name)
    end,
    undo = function(context, action)
      scene.remove_entity(context, utility.get_entity(context, action.clone_name).get_handle)
    end
  },
  rename_entity = {
    apply = function(context, action)
      context.entities[action.after] = context.entities[action.before]
      context.entities[action.before] = nil
      local entity = utility.get_entity(context, action.before)
      gui.remove_entity(context, entity.get_handle)
      entity.EditorMetaComponent.name = action.after
      gui.refresh_entity(context, entity)
    end,
    undo = function(context, action)
      context.entities[action.before] = context.entities[action.after]
      context.entities[action.after] = nil
      local entity = utility.get_entity(context, action.after)
      gui.remove_entity(context, entity.get_handle)
      entity.EditorMetaComponent.name = action.before
      gui.refresh_entity(context, entity)
    end
  },
  rename_preset = {
    apply = function(context, action)
      context.presets[action.after] = context.presets[action.before]
      context.presets[action.before] = nil
      gui.remove_preset(context, action.before)
      if context.selected_preset == action.before then
        context.selected_preset = action.after
      end
      gui.refresh_preset(context, action.after)

      local instances = context.entity_manager:get_entities({"PresetInstanceComponent"})
      for i, instance in ipairs(instances) do
        if instance.PresetInstanceComponent.name == action.before then
          instance.PresetInstanceComponent.name = action.after
          context.entities[instance.EditorMetaComponent.name].components.PresetInstanceComponent.name = action.after
          gui.refresh_entity(context, instance)
        end
      end
    end,
    undo = function(context, action)
      context.presets[action.before] = context.presets[action.after]
      context.presets[action.after] = nil
      gui.remove_preset(context, action.after)
      if context.selected_preset == action.after then
        context.selected_preset = action.before
      end
      gui.refresh_preset(context, action.before)

      local instances = context.entity_manager:get_entities({"PresetInstanceComponent"})
      for i, instance in ipairs(instances) do
        if instance.PresetInstanceComponent.name == action.after then
          instance.PresetInstanceComponent.name = action.before
          context.entities[instance.EditorMetaComponent.name].components.PresetInstanceComponent.name = action.before
          gui.refresh_entity(context, instance)
        end
      end
    end
  },
  delete = {
    apply = function(context, action)
      action.scene_entity = context.entities[action.entity_name]
      scene.remove_entity(context, utility.get_entity(context, action.entity_name).get_handle)
    end,
    undo = function(context, action)
      scene.create_entity(context, action.entity_name, action.scene_entity)
      action.scene_entity = nil
    end
  },
  attach = {
    apply = function(context, action)
      local components = context.entities[action.entity_name].components
      local entity = utility.get_entity(context, action.entity_name)
      action.attach_before = components.AttachToEntityComponent
      if action.attach_after then
        components.AttachToEntityComponent = action.attach_after
        entity.AttachToEntityComponent =
          serializer.create_reflectable(context, cpp.AttachToEntityComponent, components.AttachToEntityComponent)
      else
        components.AttachToEntityComponent =
          serializer.create_reflectable_table(context, entity.AttachToEntityComponent)
      end
      action.attach_after = components.AttachToEntityComponent
      gui.refresh_entity(context, entity)
    end,
    undo = function(context, action)
      local components = context.entities[action.entity_name].components
      local entity = utility.get_entity(context, action.entity_name)
      components.AttachToEntityComponent = action.attach_before
      entity.AttachToEntityComponent =
        serializer.create_reflectable(context, cpp.AttachToEntityComponent, components.AttachToEntityComponent)
      action.attach_before = nil
      gui.refresh_entity(context, entity)
    end
  },
  detach = {
    apply = function(context, action)
      local components = context.entities[action.entity_name].components
      local entity = utility.get_entity(context, action.entity_name)
      action.attach_before = components.AttachToEntityComponent
      components.AttachToEntityComponent = nil
      entity.AttachToEntityComponent = nil
      gui.refresh_entity(context, entity)
    end,
    undo = function(context, action)
      local components = context.entities[action.entity_name].components
      local entity = utility.get_entity(context, action.entity_name)
      components.AttachToEntityComponent = action.attach_before
      entity.AttachToEntityComponent =
        serializer.create_reflectable(context, cpp.AttachToEntityComponent, components.AttachToEntityComponent)
      action.attach_before = nil
    end
  },
  connect_mesh_build_nodes = {
    apply = function(context, action)
      -- They belong to the same mesh.
      -- They have MeshNodeComponents
      -- They are not the same entities
      local source = utility.get_entity(context, action.entity_name)
      local target = utility.get_entity(context, action.target_name)
      action.is_getting_connected = mesh.connect(context, source, target)
    end,
    undo = function(context, action)
      if not action.is_getting_connected then
        return
      end
      local source = utility.get_entity(context, action.entity_name)
      local target = utility.get_entity(context, action.target_name)
      mesh.disconnect(context, source, target)
    end
  },
  disconnect_mesh_build_nodes = {
    apply = function(context, action)
      local source = utility.get_entity(context, action.entity_name)
      local target = utility.get_entity(context, action.target_name)
      action.is_getting_disconnected = mesh.disconnect(context, source, target)
    end,
    undo = function(context, action)
      if not action.is_getting_disconnected then
        return
      end
      local source = utility.get_entity(context, action.entity_name)
      local target = utility.get_entity(context, action.target_name)
      mesh.connect(context, source, target)
    end
  }
}
-- Potential custom validators for actions
-- that contain optional data.
-- Return true if valid.
local custom_validators = {
  edit_value = function(action)
    return utility.is_table_valid(action, {path = {}})
  end,
  toggle_component = function(action)
    return utility.is_table_valid(
      action,
      {
        entity_name = "name",
        component = "component_name"
      }
    )
  end
}

-- Validate action data.
-- Return true if valid.
local function is_action_valid(action)
  -- Check if it fits the standard action.
  if not utility.is_table_valid(action, action_types.action) then
    return false
  end

  -- Check if action exists in action list.
  if not action_types[action.type] then
    return false
  end

  -- We must have a handler
  if not handlers[action.type] then
    return false
  end

  -- Use the custom validator if exists
  local custom_validator = custom_validators[action.type]
  if custom_validator then
    return custom_validator(action)
  end

  return utility.is_table_valid(action, action_types[action.type])
end

-- Handle action
-- Simply validate it and pass it through to an action handler.
-- To apply the action, set is_forward to true.
-- To undo the action, set is_forward to false.
function handle_action(context, action, is_forward)
  print("Action! " .. action.type)
  if not is_action_valid(action) then
    print("Invalid action : ")
    print(action)
    return
  end

  if is_forward then
    handlers[action.type].apply(context, action)
  else
    handlers[action.type].undo(context, action)
  end
end
