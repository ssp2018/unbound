local cpp = require("cpp")
local ref = require("reflect")
local utility = require("editor/utility")
local serializer = require("editor/serializer")

-- GUI
-- Responsible for GUI operations.

-- Map of special-case types that use serialization
-- for the GUI.
local serialize_types = {
  ModelAsset = true,
  TextureAsset = true,
  MaterialAsset = true,
  MeshAsset = true,
  FontAsset = true,
  ShaderAsset = true,
  SoundBufferAsset = true,
  AnimationBlueprintAsset = true,
  EntityHandle = true,
  SlowMotionFactor = true,
  StaticComponentContents = true,
  CharacterControllerComponentContents = true,
  mat4 = true,
  Transform = true,
  AnimationState = true
}

-- Invoke BSE_EDIT_NAMED(obj, path) recursively
function bse_edit(context, path, obj)
  if type(obj) == "table" then
    for k, v in pairs(obj) do
      local new_val = bse_edit(context, path .. "/" .. k, v)
      if new_val then
        obj[k] = new_val
      end
    end
    return nil
  end

  if type(obj) == "userdata" then
    if serialize_types[ref.type_name(obj)] == true then
      return bse_edit(context, path, serializer.create_reflectable_table(context, obj))
    end
  end

  local members = ref.members(obj)
  if members then
    for i, member_name in ipairs(members) do
      local member = obj[member_name]
      if type(member) ~= "function" then
        local new_val = bse_edit(context, path .. "/" .. member_name, obj[member_name])
        if new_val then
          obj[member_name] = new_val
        end
      end
    end
    return nil
  end

  if ref.is_valid_userdata(obj) then
    if ref.has_ipairs(obj) then
      for i, v in ipairs(obj) do
        local new_val = bse_edit(context, path .. "/[" .. i .. "]", v)
        if new_val then
          obj[i] = new_val
        end
      end
      return nil
    end

    if ref.has_pairs(obj) then
      for k, v in pairs(obj) do
        local new_val = bse_edit(context, path .. "/" .. k, v)
        if new_val then
          obj[k] = new_val
        end
      end
      return nil
    end
    return nil
  elseif ref.is_invalid_userdata(obj) then
    return nil
  end
  return cpp.edit(path, obj)
end

-- Helper for retrieving entity path info
local function get_entity_info(context, entity_name)
  return {
    entity = utility.get_entity(context, entity_name),
    scene_entity = context.entities[entity_name],
    entity_name = entity_name
  }
end

-- Returns a table of some useful data of what
-- the path points to, where path
-- is a table of path parts.
function get_path_info(context, path)
  if #path < 2 then
    return nil
  end

  if path[1] ~= "editor" then
    return nil
  end

  if path[2] == "preset" then
    local name = path[3]
    local info = {
      location = "preset",
      name = name,
      content_offset = 4
    }

    if path[4] == "name" then
      info.type = "name"
    else
      info.type = "preset"
    end
    return info
  end

  if path[2] == "selection" then
    local name = path[3]

    local info = {
      location = "selection",
      content_offset = 4
    }
    if path[4] == "name" then
      info.type = "name"
    else
      info.type = "entity"
    end
    utility.append_table(info, get_entity_info(context, name))
    return info
  end

  if path[2] == "entities" then
    local name = path[3]
    local info = {
      location = "entities",
      content_offset = 4
    }
    if path[4] == "name" then
      info.type = "name"
    else
      info.type = "entity"
    end
    utility.append_table(info, get_entity_info(context, name))
    return info
  end

  if path[2] == "scene" then
    if path[3] == "load" then
      return {
        location = "scene",
        type = "load"
      }
    elseif path[3] == "path" then
      return {
        location = "scene",
        type = "path"
      }
    end
  end

  if path[2] == "presets" then
    local name = path[3]
    local info = {
      location = "presets",
      name = name,
      content_offset = 4
    }
    if path[4] == "select" then
      info.type = "select"
    elseif path[4] == "name" then
      info.type = "name"
    end
    return info
  end

  print("Unsupported path '" .. utility.implode(path, "/") .. "'.")
  return nil
end

-- Invoke bse edit on the GUI entry at path.
-- Return the new value. If value hasn't changed,
-- return nil.
function refresh_path(context, path)
end

-- Helper for pushing a rename entity action
local function push_rename_entity_action(context, path, path_info)
  local before = path_info.entity_name
  local after = cpp.edit(utility.implode(path, "/"), before)
  if not after or type(after) ~= "string" or not context.entities[before] or context.entities[after] then
    return
  end

  utility.append(
    context.actions,
    {
      type = "rename_entity",
      before = before,
      after = after
    }
  )
end

-- Helper for pushing a rename preset action
local function push_rename_preset_action(context, path, path_info)
  local before = path_info.name
  local after = cpp.edit(utility.implode(path, "/"), before)
  if not after or type(after) ~= "string" or not context.presets[before] or context.presets[after] then
    return
  end

  utility.append(
    context.actions,
    {
      type = "rename_preset",
      before = before,
      after = after
    }
  )
end

-- Handler for dirty entity view path
function handle_dirty_entity_view(context, path, info)
  if info.type == "entity" then
    local item = path[info.content_offset]
    if item == "select" then
      for i, v in pairs(context.selected_entities) do
        utility.append(context.events, {type = "deselect", entity_id = v.id})
      end
      utility.append(context.events, {type = "select", entity_id = info.entity.get_handle})
    elseif item == "components" then
      local component = path[info.content_offset + 1]
      local before
      local after
      if info.entity[component] then
        before = info.scene_entity[component]
        after = nil
      else
        before = nil
        after = {}
      end
      utility.append(
        context.actions,
        {
          type = "toggle_component",
          entity_name = info.entity_name,
          component = component,
          before = before,
          after = after
        }
      )
    elseif utility.is_component(context, item) then
      local value_path = utility.implode(path, "/")
      local component = path[info.content_offset]
      local table = serializer.create_reflectable_table(context, info.entity[component])
      local old_value = utility.get_table_value(table, path, info.content_offset + 1)
      if old_value == nil then
        local n = tonumber(path[#path])
        if n ~= nil then
          path[#path] = n
          old_value = utility.get_table_value(table, path, info.content_offset + 1)
        --old_value = utility.get_table_value(table, path, info.content_offset + 1, #path - 1)
        --old_value = old_value[n]
        end
      end
      local new_value = bse_edit(context, value_path, old_value)
      if type(old_value) == "userdata" and serialize_types[ref.type_name(old_value)] == true then
        old_value = serializer.create_reflectable_table(context, old_value)
      end
      if new_value == nil then
        -- Value hasn't changed
        return
      end
      local old_scene_value = utility.get_table_value(info.scene_entity["components"], path, info.content_offset)
      utility.append(
        context.actions,
        {
          type = "edit_value",
          path = path,
          is_new = (old_scene_value == nil),
          before = old_value,
          after = new_value
        }
      )
    else
      print("Unknown path '" .. utility.implode(path, "/") .. "'.")
    end
  elseif info.type == "name" then
    push_rename_entity_action(context, path, info)
  end
end

-- Handle a GUI entry path whose value has changed.
function handle_dirty_path(context, path)
  local info = get_path_info(context, path)

  if info == nil then
    return
  end

  if info.location == "selection" then
    handle_dirty_entity_view(context, path, info)
    return
  end

  if info.location == "entities" then
    handle_dirty_entity_view(context, path, info)
    return
  end

  if info.location == "scene" then
    if info.type == "load" then
      utility.append(context.events, {type = "load_scene"})
    elseif info.type == "path" then
      local new_path = cpp.edit(utility.implode(path, "/"), context.path)
      if new_path ~= nil then
        context.path = new_path
      end
    end
    return
  end

  if info.location == "presets" then
    if info.type == "select" then
      if context.selected_preset then
        cpp.remove_entry("editor/preset/" .. context.selected_preset)
      end
      context.selected_preset = info.name
      refresh_preset_selection(context)
    elseif info.type == "name" then
      push_rename_preset_action(context, path, info)
    end
    return
  end

  if info.location == "preset" then
    if info.type == "name" then
      push_rename_preset_action(context, path, info)
    end
    return
  end

  print("Unknown path '" .. utility.implode(path, "/") .. "'.")
end

-- Refresh/add component toggles for entity at path
function refresh_component_toggles(context, path, entity)
  local entity_components = entity.get_component_names
  for i, name in ipairs(context.visible_components) do
    local is_enabled = false
    for j, entity_component in ipairs(entity_components) do
      if entity_component == name then
        is_enabled = true
        break
      end
    end

    cpp.edit(path .. "/" .. name, is_enabled)
  end
end

-- Refresh/add entity view with path_prefix
local function refresh_entity_helper(context, entity, path_prefix)
  local path = path_prefix .. "/" .. entity.EditorMetaComponent.name
  cpp.remove_entry(path)
  cpp.edit(path .. "/name", entity.EditorMetaComponent.name)
  refresh_component_toggles(context, path .. "/components", entity)
  local component_names = entity.get_component_names
  if component_names and #component_names > 0 then
    for i, name in ipairs(component_names) do
      if not context.hidden_components[name] then
        bse_edit(context, path .. "/" .. name, entity[name])
      end
    end
  end
  bse_edit(context, path .. "/select", true)
end

-- Refresh/add entity view in entities list
function refresh_entity(context, entity)
  refresh_entity_helper(context, entity, "editor/entities")

  if utility.is_entity_selected(context, entity) then
    refresh_selection(context, entity)
  end
end

-- Remove preset view from presets list
function remove_preset(context, name)
  cpp.remove_entry("editor/presets/" .. name)

  if utility.is_preset_selected(context, name) then
    cpp.remove_entry("editor/preset/" .. name)
  end
end

local function refresh_preset_helper(context, name, path_prefix)
  local path = path_prefix .. "/" .. name
  cpp.remove_entry(path)
  bse_edit(context, path, context.presets[name])
  cpp.edit(path .. "/select", true)
  cpp.edit(path .. "/name", name)
end

-- Refresh/add preset view to presets list
function refresh_preset(context, name)
  refresh_preset_helper(context, name, "editor/presets")
  if utility.is_preset_selected(context, name) then
    refresh_preset_selection(context)
  end
end

-- Refresh/add preset selection to preset list.
function refresh_preset_selection(context)
  local prefix = "editor/preset"
  local name = context.selected_preset
  refresh_preset_helper(context, name, prefix)
  cpp.remove_entry(prefix .. "/" .. name .. "/select")
end

-- Remove entity view from entities list
function remove_entity(context, entity_id)
  local entity = context.entity_manager:get_entity(entity_id)
  local path = "editor/entities/" .. entity.EditorMetaComponent.name
  cpp.remove_entry(path)

  if utility.is_entity_selected(context, entity) then
    remove_selection(context, entity)
  end
end

-- Generate basic GUI
function create(context)
  cpp.edit("editor/scene/load", false)
  bse_edit(context, "editor/scene/path", context.path)
end

-- Clear GUI contents
function clear()
  cpp.remove_entry("editor/entities")
  cpp.remove_entry("editor/presets")
  cpp.remove_entry("editor/preset")
  cpp.remove_entry("editor/selection")
end

-- Refresh/add entity selection to selection list
function refresh_selection(context, entity)
  refresh_entity_helper(context, entity, "editor/selection")
  cpp.remove_entry("editor/selection/" .. entity.EditorMetaComponent.name .. "/select")
end

-- Remove entity selection from selection list
function remove_selection(context, entity)
  cpp.remove_entry("editor/selection/" .. entity.EditorMetaComponent.name)
end
