local func = require("ai/ranged_grunt/ranged_grunt_functionality").funcs
local const = require("ai/ranged_grunt/ranged_grunt_functionality").consts
local shared = require("ai/shared_functionality").funcs
local cpp = require("cpp")
local math = require("math")

ranged_grunt_transition_table = {}

ranged_grunt_transition_table["walking_around"] = {
    update = function(self, e)
        local ai = e:get_entity().AIComponent

        if ai.state_time > 20 then
            return "vulnerable_action"
        end

        if shared.look_at_surroundings(e) then
            return "battle_stance"
        end

        if shared.listen_to_surroundings(e) then
            return "investigate"
        end

        return ""
    end
}

ranged_grunt_transition_table["vulnerable_action"] = {
    update = function(self, e)
        local ai = e:get_entity().AIComponent

        if shared.look_at_surroundings(e) then
            return "battle_stance"
        end

        if ai.state_time > self.vulnerable_time then
            return "walking_around"
        end

        if (shared.listen_to_surroundings(e)) then
            return "investigate"
        end

        return ""
    end
}

ranged_grunt_transition_table["investigate"] = {
    investigation_time = 5,
    update = function(self, e)
        local ai = e:get_entity().AIComponent

        if shared.look_at_surroundings(e) then
            return "battle_stance"
        elseif shared.listen_to_surroundings(e) then
            return "investigate"
        elseif ai.state_time > self.investigation_time then
            return "return_home"
        end

        return ""
    end
}

ranged_grunt_transition_table["return_home"] = {
    update = function(self, e)
        local ai = e:get_entity().AIComponent
        local tc = e:get_entity().TransformComponent

        if shared.get_distance(e, ai.spawn_point, tc.transform:get_position()) < 5 then
            return "walking_around"
        elseif shared.look_at_surroundings(e) then
            return "battle_stance"
        end
        return ""
    end
}

ranged_grunt_transition_table["flee"] = {
    update = function(self, e)
        local ai = e:get_entity().AIComponent

        if not ai.has_target or ai.state_time > 10 then
            return "return_home"
        elseif ai.morale_data.morale + ai.morale_data.morale_boost > ai.morale_data.morale_reenter_combat_threshold then
            return "battle_stance"
        end
        return ""
    end
}

ranged_grunt_transition_table["battle_stance"] = {
    arrow_fire_rate = 3,
    update = function(self, e)
        local ai = e:get_entity().AIComponent
        local tc = e:get_entity().TransformComponent

        if not ai.has_target then
            return "return_home"
        elseif ai.morale_data.morale + ai.morale_data.morale_boost < ai.morale_data.morale_flee_threshold then
            return "flee"
        elseif shared.get_distance(tc, shared.get_target_pos(e)) > const.long_range then
            return "sprint"
        end

        return ""
    end
}

ranged_grunt_transition_table["sprint"] = {
    update = function(self, e)
        local ai = e:get_entity().AIComponent

        if not ai.has_target then
            return "return_home"
        elseif ai.morale_data.morale + ai.morale_data.morale_boost < ai.morale_data.morale_flee_threshold then
            return "flee"
        elseif shared.get_distance(tc, shared.get_target_pos(e)) < const.medium_range then
            return "battle_stance"
        end

        return ""
    end
}
