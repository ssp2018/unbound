local func = require("ai/ranged_grunt/ranged_grunt_functionality").funcs
local const = require("ai/ranged_grunt/ranged_grunt_functionality").consts
local shared = require("ai/shared_functionality").funcs
local cpp = require("cpp")

ranged_grunt_exit_block_table = {}

ranged_grunt_exit_block_table["walking_around"] = {
    update = function(self, e)
    end
}

ranged_grunt_exit_block_table["vulnerable_action"] = {
    update = function(self, e)
    end
}

ranged_grunt_exit_block_table["investigate"] = {
    update = function(self, e)
    end
}

ranged_grunt_exit_block_table["return_home"] = {
    update = function(self, e)
    end
}

ranged_grunt_exit_block_table["flee"] = {
    update = function(self, e)
    end
}

ranged_grunt_exit_block_table["battle_stance"] = {
    update = function(self, e)
    end
}

ranged_grunt_exit_block_table["sprint"] = {
    update = function(self, e)
    end
}
