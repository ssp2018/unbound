local func = require("ai/ranged_grunt/ranged_grunt_functionality").funcs
local const = require("ai/ranged_grunt/ranged_grunt_functionality").consts
local shared = require("ai/shared_functionality").funcs
local cpp = require("cpp")
local math = require("math")

ranged_grunt_update_table = {}

ranged_grunt_update_table["walking_around"] = {
    wait_min = 0,
    wait_max = 1,
    update = function(self, e)
        local ai = e:get_entity().AIComponent
        local cc = e:get_entity().CharacterControllerComponent

        -- extract the state from ai component
        local state = ai.lua_state

        if (not state.waiting and not ai.funcs:is_moving()) then
            --set wait time of the ai before going to next location
            state.wait_until = ai.total_state_time + math.random(self.wait_min, self.wait_max)
            state.waiting = true
        elseif state.wait_until < ai.total_state_time and state.waiting then
            radius = ai.exploration_radius
            s_point = ai.spawn_point
            -- calculates the new x and y coordinates randomly inside their patrolling area
            x = math.random(s_point.x - radius, s_point.x + radius)
            y = math.random(s_point.y - radius, s_point.y + radius)
            -- start walking towards the new position
            ai.funcs:go_to(x, y)
            state.waiting = false
        end
    end
}

ranged_grunt_update_table["vulnerable_action"] = {
    update = function(self, e)
        --empty
    end
}

ranged_grunt_update_table["investigate"] = {
    startle_time = 2,
    update = function(self, e)
        local ai = e:get_entity().AIComponent

        -- Wait until the startle animation is over, then go to the search spot
        if ai.state_time > self.startle_time and ai.actions_taken == 0 then
            ai.actions_taken = 1
            ai.funcs:go_to(ai.search_location.x, ai.search_location.y)
        end
    end
}

ranged_grunt_update_table["return_home"] = {
    update = function(self, e)
    end
}

ranged_grunt_update_table["flee"] = {
    update = function(self, e)
        if ai.has_target then
            shared.back_away_from_target(e)
        end
    end
}

ranged_grunt_update_table["battle_stance"] = {
    update = function(self, e)
        local ai = e:get_entity().AIComponent
        local tc = e:get_entity().TransformComponent
        local state = ai.lua_state

        angle = shared.get_target_angle(e)

        if state.angle_location ~= 0 and angle < 45 then
            state.angle_location = 0
            e:set_turning_speed_multiple(1.0)
        elseif state.angle_location ~= 3 and angle > 135 then
            state.angle_location = 3
            e:set_turning_speed_multiple(1.4)
        elseif state.angle_location ~= 2 and angle > 90 then
            state.angle_location = 2
            e:set_turning_speed_multiple(1.2)
        elseif state.angle_location ~= 1 and angle > 45 then
            state.angle_location = 1
            e:set_turning_speed_multiple(1.1)
        end

        local target_pos = shared.get_target_pos(e)
        if func.get_distance(e, target_pos, tc) > const.long_range then
            ai.funcs:go_to(target_pos.x, target_pos.y)
        end

        e:turn_towards(target_pos.x, target_pos.y, 0)

        if ai.state_time > arrow_fire_rate then
            ai.state_time = ai.state_time - arrow_fire_rate
            func.shoot_arrow(e)
        end
    end
}

ranged_grunt_update_table["sprint"] = {
    update = function(self, e)
        local ai = e:get_entity().AIComponent

        if (ai.has_target) then
            local target_pos = shared.get_target_pos(e)
            ai.funcs:go_to(target_pos.x, target_pos.y)
            e:turn_towards(target_pos.x, target_pos.y, 0)
        end
    end
}
