local func = require("ai/ranged_grunt/ranged_grunt_functionality").funcs
local const = require("ai/ranged_grunt/ranged_grunt_functionality").consts
local shared = require("ai/shared_functionality").funcs
local cpp = require("cpp")

ranged_grunt_enter_block_table = {}

ranged_grunt_enter_block_table["walking_around"] = {
    update = function(self, e)
        local cc = e:get_entity().CharacterControllerComponent
        cc.contents:set_face_moving_direction(true)
        local hp = e:get_entity().HealthComponent
        hp.health_points = hp.max_health_points
    end
}

ranged_grunt_enter_block_table["vulnerable_action"] = {
    update = function(self, e)
        local cc = e:get_entity().CharacterControllerComponent
        cc.contents:set_face_moving_direction(true)
    end
}

ranged_grunt_enter_block_table["investigate"] = {
    update = function(self, e)
        local cc = e:get_entity().CharacterControllerComponent
        cc.contents:set_face_moving_direction(true)
    end
}

ranged_grunt_enter_block_table["return_home"] = {
    update = function(self, e)
        local cc = e:get_entity().CharacterControllerComponent
        cc.contents:set_face_moving_direction(true)
    end
}

ranged_grunt_enter_block_table["flee"] = {
    update = function(self, e)
        local cc = e:get_entity().CharacterControllerComponent
        cc.contents:set_face_moving_direction(true)
    end
}

ranged_grunt_enter_block_table["battle_stance"] = {
    update = function(self, e)
        local cc = e:get_entity().CharacterControllerComponent
        cc.contents:set_face_moving_direction(false)
        local state = ai.lua_state
        state.angle_location = 0
    end
}

ranged_grunt_enter_block_table["sprint"] = {
    update = function(self, e)
        local cc = e:get_entity().CharacterControllerComponent
        cc.contents:set_face_moving_direction(true)
    end
}
