local math = require("math")
local cpp = require("cpp")
local reflect = require("reflect")

consts = {}

funcs = {
    --GET_TARGET_ENTITY
    get_target_entity = function(e)
        local entity = e:get_entity()
        local mgr = e:get_entity_manager()

        if not entity.is_valid then
            print("get_target_entity failed, entity not valid")
            return nil
        end

        local ai = entity.AIComponent

        if not ai.has_target then
            print("get_target_entity failed, entity does not have target")
            return entity
        end

        return mgr:get_entity(ai.target)
    end,
    --GET_TARGET_POS
    get_target_pos = function(e)
        local mgr = e:get_entity_manager()
        local entity = e:get_entity()

        if not entity.is_valid then
            print("get_target_pos failed, entity not valid")
            return nil
        end

        local ai = e:get_entity().AIComponent

        if not ai.has_target then
            print("get_target_pos failed, entity does not have target")
            return nil
        end

        local target = mgr:get_entity(ai.target)
        local target_tc = target.TransformComponent
        return target_tc.transform:get_position()
    end,
    --GET_DISTANCE
    get_distance = function(e, first, second)
        local var = {cpp.vec3.new(), cpp.vec3.new()}
        local param = {first, second}

        for i = 1, 2 do
            if reflect.type_name(param[i]) == "Entity" then
                local tc_curr = param[i].TransformComponent
                var[i] = tc_curr.transform:get_position()
            elseif reflect.type_name(param[i]) == "TransformComponent" then
                var[i] = param[i].transform:get_position()
            elseif reflect.type_name(param[i]) == "vec2" then
                var[i] = cpp.vec3_fill(param[i].x, param[i].y, 0)
            elseif reflect.type_name(param[i]) == "vec3" then
                var[i] = param[i]
            end
        end

        local tmp = cpp.vec3.new()
        tmp.x = (var[1].x - var[2].x)
        tmp.y = (var[1].y - var[2].y)
        tmp.z = (var[1].z - var[2].z)

        local dist = math.sqrt(tmp.x * tmp.x + tmp.y * tmp.y + tmp.z * tmp.z)
        return dist
    end,
    --BACK_AWAY_FROM
    back_away_from = function(e, pos)
        local entity = e:get_entity()
        local tc = entity.TransformComponent
        local ai = entity.AIComponent

        if reflect.type_name(pos) == "vec2" then
            local char_pos = tc.transform:get_position()
            local dir = cpp.normalize_vec2((pos.x - char_pos.x) * 2, (pos.y - char_pos.y) * 2)
            local dest = cpp.vec2.new(char_pos.x - dir.x, char_pos.y - dir.y)
            ai.funcs:go_to(dest.x, dest.y)
        elseif reflect.type_name(pos) == "vec3" then
            local char_pos = tc.transform:get_position()
            local dir = cpp.normalize_vec2((pos.x - char_pos.x) * 2, (pos.y - char_pos.y) * 2, (pos.z - char_pos.z) * 2)
            local dest = cpp.vec3.new(char_pos.x - dir.x, char_pos.y - dir.y, char_pos.z - dir.z)
            ai.funcs:go_to(dest.x, dest.y, dest.z)
        else
            print("back_away_from failed, unknown parameter type")
        end
    end,
    --BACK_AWAY_FROM_TARGET
    back_away_from_target = function(e)
        local entity = e:get_entity()
        local mgr = e:get_entity_manager()
        if not entity.is_valid then
            return
        end

        local target_pos = entity.TransformComponent.transform:get_position()
        funcs.back_away_from(e, entity, target_pos)
    end,
    --LISTEN_TO_SURROUNDINGS
    listen_to_surroundings = function(e)
        return e:listen_to_surroundings()
    end,
    --LOOK_AT_SURROUNDINGS
    look_at_surroundings = function(e)
        return e:look_at_surroundings()
    end,
    --GET_TARGET_ANGLE
    get_target_angle = function(e)
        local ret = 0
        local entity = e:get_entity()
        local entity_mgr = e:get_entity_manager()

        if not entity.is_valid then
            print("get_target_angle failed, entity not valid")
            return ret
        end

        local ai = entity.AIComponent
        if entity.CharacterControllerComponent ~= nil and entity.TransformComponent ~= nil then
            local ccc = entity.CharacterControllerComponent
            local tc = entity.TransformComponent

            if entity_mgr:is_valid(ai.target) then
                local e_ent = entity_mgr:get_entity(ai.target)
                local e_ccc = e_ent.CharacterControllerComponent
                local e_tc = e_ent.TransformComponent

                local look = cpp.mat4_vec4_mul(tc.transform:get_model_matrix(), cpp.vec4_fill(0, 1, 0, 0))

                local look_s = cpp.vec3.new()
                look_s.x = look.x
                look_s.y = look.y
                look_s.z = look.z
                look_s = cpp.normalize_vec3(look_s)

                local pos_one = e_tc.transform:get_position()
                local pos_two = tc.transform:get_position()

                local pos_two_vec_3 = cpp.vec3.new()
                pos_two_vec_3.x = pos_two.x
                pos_two_vec_3.y = pos_two.y
                pos_two_vec_3.z = pos_two.z

                local to_target = cpp.vec3.new()
                to_target.x = pos_one.x
                to_target.y = pos_one.y
                to_target.z = pos_one.z

                to_target = cpp.vec3_subtract(to_target, pos_two_vec_3)

                to_target.z = 0
                to_target = cpp.normalize_vec3(to_target)

                ret = cpp.angle_vec3(look_s, to_target)
                ret = (ret / 3.1415926) * 180
            end
        else
            print("get_target_angle failed, entity does not own required components")
        end
        return ret
    end,
    --AIM_PREDICTION
    aim_prediction = function(start, target, target_velocity, projectile_speed)
        local direction = cpp.vec3.new()
        direction.x = target.x - start.x + 0
        direction.y = target.y - start.y + 0
        direction.z = target.z - start.z + 0

        local distance = 1
        distance = cpp.distance_vec3(start, target)

        --target_velocity.x = target_velocity.x * (distance / projectile_speed)
        --target_velocity.y = target_velocity.y * (distance / projectile_speed)
        --target_velocity.z = target_velocity.z * (distance / projectile_speed)
        target_velocity = cpp.vec3_scalar_multiplication(target_velocity, (distance / projectile_speed))

        local velocity = cpp.vec3.new()
        velocity = cpp.vec3_add(direction, target_velocity)
        --velocity.x = direction.x + target_velocity.x
        --velocity.y = direction.y + target_velocity.y
        --velocity.z = direction.z + target_velocity.z + (9.82 * distance / projectile_speed)
        velocity.z = velocity.z + (9.82 * distance / projectile_speed)

        velocity = cpp.normalize_vec3(velocity)
        velocity = cpp.vec3_scalar_multiplication(velocity, projectile_speed)
        --velocity.x = velocity.x * projectile_speed
        --velocity.y = velocity.y * projectile_speed
        --velocity.z = velocity.z * projectile_speed

        return velocity
    end,
    --GO_TO
    go_to = function(e, pos)
        local entity = e:get_entity()
        if not entity.is_valid then
            return
        end

        local ai = entity.AIComponent
        local destination = cpp.vec3.new()

        if reflect.type_name(pos) == "vec2" then
            destination.x = pos.x
            destination.y = pos.y
        elseif reflect.type_name(pos) == "vec3" then
            destination = pos
        elseif reflect.type_name(pos) == "vec4" then
            destination.x = pos.x
            destination.y = pos.y
            destination.z = pos.z
        else
            print("go_to failed, unknown parameter type")
        end

        ai.funcs.m_path_finding.m_destination = destination
        ai.funcs.m_path_finding.m_need_path = true
        ai.funcs.m_path_finding.m_on_the_move = true
    end,
    -- Flies to target location
    fly_to = function(e, goal)
        local sc = e:get_entity().SteeringComponent
        if reflect.type_name(goal) == "EntityHandle" then
            cpp.set_goal_entity(sc, goal)
        elseif reflect.type_name(goal) == "vec3" then
            cpp.set_goal_vec(sc, goal)
        end

        sc.on_the_move = true
    end
}
