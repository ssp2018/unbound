local math = require("math")
local cpp = require("cpp")

consts = {}

local audio_wrapper = cpp.AudioScriptWrapper.new()

funcs = {
    print_func = function(string)
        print(string)
    end,
    skull_fire_attack = function(e, target_entity)
        local entity = e:get_entity()
        local entity_mgr = e:get_entity_manager()
        local event_mgr = e:get_event_manager()

        -- shoot projectile
        if entity.is_valid and target_entity.is_valid then
            local entity_ai_component = entity.AIComponent
            local entity_transform_component = entity.TransformComponent

            local target_tc = target_entity.TransformComponent
            local start = cpp.vec3.new()
            start = entity_transform_component.transform:get_position()
            start.z = start.z - 4
            local target = cpp.vec3.new()
            target = target_tc.transform:get_position()

            local ball_entity = entity_mgr:create_entity()

            local transform_component = cpp.TransformComponent.new()
            transform_component.transform:set_position(start, e:get_frame_context())
            ball_entity.TransformComponent = transform_component

            local lifetime_component = cpp.LifetimeComponent.new()
            lifetime_component.time_left = 30
            ball_entity.LifetimeComponent = lifetime_component

            local homing_component = cpp.HomingComponent.new()
            --homing_component.speed = 100
            homing_component.speed = 70
            --homing_component.turn_speed = 23
            homing_component.turn_speed = 18
            homing_component.target = target_entity.get_handle
            ball_entity.HomingComponent = homing_component

            local model_component = cpp.ModelComponent.new()
            model_component.asset = e:get_model_asset("model/skull_projectile.fbx")
            ball_entity.ModelComponent = model_component

            local projectile_component = cpp.PhysicsProjectileComponent.new()
            projectile_component.contents:set_radius(1.2)
            --projectile_component.contents:set_velocity(cpp.normalize_vec3(target - start) * speed) --crashes
            local velocity = cpp.vec3.new()
            velocity.x = (target.x - start.x)
            velocity.y = (target.y - start.y)
            velocity.z = (target.z - start.z)
            velocity = cpp.normalize_vec3(velocity)
            velocity.x = velocity.x * homing_component.speed * 2
            velocity.y = velocity.y * homing_component.speed * 2
            velocity.z = velocity.z * homing_component.speed * 2
            projectile_component.contents:set_velocity(velocity)
            projectile_component.contents:set_gravity(cpp.vec3.new())
            --projectile_component.contents:disable_collision_reports_for(0.4)
            --projectile_component.contents:disable_destruction_for(0.4)
            projectile_component.contents:disable_collision_reports_for(0.3)
            projectile_component.contents:disable_destruction_for(0.3)
            local base = cpp.create_mat4()
            local scale = cpp.vec3_fill(2, 2, 2)
            projectile_component.contents:set_offset_transform(cpp.mat4_scale(base, scale))
            ball_entity.PhysicsProjectileComponent = projectile_component

            local damage_component = cpp.DamageComponent.new()
            damage_component.damage_value = entity_ai_component.started_attack_damage
            damage_component.impact_damage_radius = 2
            ball_entity.DamageComponent = damage_component

            local destructible_component = cpp.DestructibleComponent.new()
            destructible_component.health_points = 1
            destructible_component.max_health_points = 1
            ball_entity.DestructibleComponent = destructible_component

            local particle_component = cpp.ParticlesComponent.new()
            particle_component.max_particles = 20
            particle_component.spawn_per_second = 20
            particle_component.min_start_velocity = cpp.vec3.new() --may crash
            local max_vel = cpp.vec3.new()
            max_vel.z = 0.5
            particle_component.max_start_velocity = max_vel --may crash
            particle_component.min_start_size = 0.5
            particle_component.max_start_size = 1
            particle_component.max_life = 0.4
            particle_component.min_start_color = cpp.vec4_fill(0.3, 0, 0.3, 1)
            particle_component.max_start_color = cpp.vec4_fill(0.4, 0, 0.4, 1)
            particle_component.min_end_color = cpp.vec4_fill(0, 0, 0, 1)
            particle_component.max_end_color = cpp.vec4_fill(0, 0, 0, 1)
            particle_component.texture = e:get_texture_asset("texture/swirl_particle01.png")
            ball_entity.ParticlesComponent = particle_component

            local explode_component = cpp.ExplodeComponent.new()
            explode_component.max_damage = 3
            explode_component.damage_radius = 10
            explode_component.particle_speed = 40
            explode_component.particle_amount = 1000
            explode_component.particle_start_size = 0.1
            explode_component.particle_end_size = 1.3
            explode_component.time = 0.9
            explode_component.explode_on_impact = true
            explode_component.texture = e:get_texture_asset("texture/explosion.png")
            ball_entity.ExplodeComponent = explode_component

            audio_wrapper:attach_audio_component(ball_entity)
            local fireball_shooting = cpp.PlaySoundEvent.new()
            fireball_shooting.id = ball_entity.get_handle
            local path = "assets/audio/effects/flying_skull/Shooting/post/shooting" .. (math.random(1, 3)) .. ".ogg"
            fireball_shooting.buffer_path = cpp.FilePath.new(path)
            fireball_shooting.settings.loop = false
            --fireball_shooting.settings.rolloff_factor = 0.5
            event_mgr:send_event(fireball_shooting)

            local fireball_moving_pse = cpp.PlaySoundEvent.new()
            fireball_moving_pse.id = ball_entity.get_handle
            fireball_moving_pse.buffer_path =
                cpp.FilePath.new("assets/audio/effects/flying_skull/Shooting/post/fire_ball.ogg")
            fireball_moving_pse.settings.loop = true
            event_mgr:send_event(fireball_moving_pse)

            local sound_settings = cpp.SoundSettings.new()
            audio_wrapper:set_collision_sound(
                "audio/effects/flying_skull/Shooting/post/Fireball-hitting.ogg",
                ball_entity,
                1,
                sound_settings
            ) --MaterialType::GERERIC = 1

        --local effect = cpp.SpecialEffectProjectileComponent.new()
        --effect.type = 0 --EffectType::ROCK = 0
        --effect.owner = entity.get_handle
        --ball_entity.SpecialEffectProjectileComponent = effect
        end
    end
    --...
}
