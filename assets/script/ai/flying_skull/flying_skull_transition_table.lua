local func = require("ai/flying_skull/flying_skull_functionality").funcs
local const = require("ai/flying_skull/flying_skull_functionality").consts
local shared = require("ai/shared_functionality").funcs
local flying_skull_update_table = require("ai/flying_skull/flying_skull_update_table").flying_skull_update_table
local cpp = require("cpp")

flying_skull_transition_table = {}

flying_skull_transition_table["flying_around"] = {
  update = function(self, e)
    if shared.look_at_surroundings(e) then
      return "pursue"
    end

    if shared.listen_to_surroundings(e) then
      return "investigate"
    end
  end
}

flying_skull_transition_table["investigate"] = {
  investigation_time = 5,
  update = function(self, e)
    local ai = e:get_entity().AIComponent

    if shared.look_at_surroundings(e) then
      return "pursue"
    elseif not shared.listen_to_surroundings(e) and ai.state_time > self.investigation_time then
      return "return_home"
    end

    return ""
  end
}

flying_skull_transition_table["return_home"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local tc = e:get_entity().TransformComponent
    local sc = e:get_entity().SteeringComponent

    if shared.get_distance(e, ai.spawn_point, tc.transform:get_position()) < sc.stop_dist then
      return "flying_around"
    elseif shared.look_at_surroundings(e) then
      return "pursue"
    end
    return ""
  end
}

flying_skull_transition_table["pursue"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent

    if not ai.has_target then
      return "return_home"
    end
    local tc = e:get_entity().TransformComponent
    local target_distance = shared.get_distance(e, tc, shared.get_target_pos(e))
    if target_distance < (ai.aggro_range * 0.5) then
      return "combat"
    end

  end
}

flying_skull_transition_table["combat"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local tc = e:get_entity().TransformComponent
    local target_distance = shared.get_distance(e, tc, shared.get_target_pos(e))
    if target_distance > (ai.aggro_range * 0.7) then
      return "pursue"
    end
  end
}
