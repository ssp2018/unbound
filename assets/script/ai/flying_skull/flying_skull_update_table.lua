local func = require("ai/flying_skull/flying_skull_functionality").funcs
local const = require("ai/flying_skull/flying_skull_functionality").consts
local shared = require("ai/shared_functionality").funcs
local math = require("math")
local cpp = require("cpp")

flying_skull_update_table = {}

-- Define all actions for a melee grunt
flying_skull_update_table["flying_around"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local sc = e:get_entity().SteeringComponent
    --local state = ai.lua_state
    --state.flying_away = false;
    --sc.on_the_move = false
    --if ai.actions_taken == 0 then
    --state.flying_away = false;
    --ai.actions_taken = 1
    --end

    if not sc.on_the_move then
      radius = ai.exploration_radius
      s_point = ai.spawn_point
      local new_point = cpp.vec3.new()
      new_point.x = math.random(math.floor(s_point.x - radius), math.floor(s_point.x + radius))
      new_point.y = math.random(math.floor(s_point.y - radius), math.floor(s_point.y + radius))
      new_point.z = math.random(math.floor(s_point.z - radius), math.floor(s_point.z + radius))
      --if state.flying_away == false then
      --new_point.x = 200
      --new_point.y = -30
      --new_point.z = 120
      --state.flying_away = true
      --else
      --new_point = ai.spawn_point
      --state.flying_away = false
      --end
      --ai.funcs:go_to(x, y)
      shared.fly_to(e, new_point)
    end
  end
}

flying_skull_update_table["investigate"] = {
  startle_time = 1,
  update = function(self, e)
    ai = e:get_entity().AIComponent

    -- Wait until the startle animation is over, then go to the search spot
    if ai.state_time > self.startle_time and ai.actions_taken == 0 then
      ai.actions_taken = 1
      --ai.funcs:go_to(ai.search_location.x, ai.search_location.y)
      shared.fly_to(e, ai.search_location)
    --add functionality to change z-pos
    end
  end
}

flying_skull_update_table["return_home"] = {
  update = function(self, e)
    local sc = e:get_entity().SteeringComponent
    local ai = e:get_entity().AIComponent
    local tc = e:get_entity().TransformComponent
    --print(ai.spawn_point.x .. " " .. tc.transform:get_position().x)
    --ai.funcs:go_to(ai.spawn_point.x, ai.spawn_point.y)
    shared.fly_to(e, ai.spawn_point)
    --add functionality to change z-pos
  end
}

flying_skull_update_table["pursue"] = {
  attack_frequency = 5,
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local ccc = e:get_entity().CharacterControllerComponent
    local tc = e:get_entity().TransformComponent
    --local target_pos = shared.get_target_pos(e)
    local pos = tc.transform:get_position()
    --e:turn_towards(target_pos.x, target_pos.y, target_pos.z)
    e:turn_towards_target()
    shared.fly_to(e, ai.target)

    --distance = cpp.distance_vec3(pos, target_pos)

    local ti = e:get_time_info()

    --pos.x = target_pos.x - pos.x
    --pos.y = target_pos.y - pos.y
    --pos.z = target_pos.z - pos.z

    --pos = cpp.normalize_vec3(pos)

    --local speed = 2 * math.log(distance + 10)

    --pos.x = pos.x * ti.dt * speed
    --pos.y = pos.y * ti.dt * speed
    --pos.z = pos.z * ti.dt * speed

    --ccc.contents:set_move_vector(pos)

    if ai.state_time > self.attack_frequency then --shoot ball of fire
      ai.actions_taken = ai.actions_taken + 1
      ai.started_attack_damage = ai.base_attack_damage
      --ai.start_attack_this_frame = true
      ai.state_time = ai.state_time - self.attack_frequency

      func.skull_fire_attack(e, e:get_entity_manager():get_entity(ai.target))
    end

    --...
  end
}
flying_skull_update_table["combat"] = {
  attack_frequency = 5,
  update = function(self, e)
    local sc = e:get_entity().SteeringComponent
    --sc.accel_multiply = 25.0
    --sc.max_speed = 50.0
    local ai = e:get_entity().AIComponent
    local ccc = e:get_entity().CharacterControllerComponent
    local tc = e:get_entity().TransformComponent
    --local target_pos = shared.get_target_pos(e)
    local pos = tc.transform:get_position()
    local move_pos = pos
    local right_vec = tc.transform:get_right()
    cpp.vec3_scalar_multiplication(right_vec, 10.0)
    cpp.vec3_add(move_pos, right_vec)
    shared.fly_to(e, move_pos)
    --e:turn_towards(target_pos.x, target_pos.y, target_pos.z)
    e:turn_towards_target()

    if ai.state_time > self.attack_frequency then --shoot ball of fire
      ai.actions_taken = ai.actions_taken + 1
      ai.started_attack_damage = ai.base_attack_damage
      --ai.start_attack_this_frame = true
      ai.state_time = ai.state_time - self.attack_frequency

      func.skull_fire_attack(e, e:get_entity_manager():get_entity(ai.target))
    end
  end
}
