local math = require("math")
local func = require("ai/flying_skull/flying_skull_functionality").funcs
local const = require("ai/flying_skull/flying_skull_functionality").consts
local cpp = require("cpp")

flying_skull_enter_block_table = {}

-- Define all actions for a melee grunt
flying_skull_enter_block_table["flying_around"] = {
  update = function(self, e)
    local ccc = e:get_entity().CharacterControllerComponent
    ccc.contents:set_face_moving_direction(true);
  end
}

flying_skull_enter_block_table["investigate"] = {
  update = function(self, e)
    local ccc = e:get_entity().CharacterControllerComponent
    ccc.contents:set_face_moving_direction(true);
  end
}

flying_skull_enter_block_table["return_home"] = {
  update = function(self, e)
    local ccc = e:get_entity().CharacterControllerComponent
    ccc.contents:set_face_moving_direction(true);
  end
}

flying_skull_enter_block_table["pursue"] = {
  update = function(self, e)
    local event_mgr = e:get_event_manager();
    local aggro = cpp.PlaySoundEvent.new();
    aggro.id = e:get_entity().get_handle;
    local path = "assets/audio/effects/flying_skull/Aggro/post/" .. (math.random(1, 3)) .. ".ogg"
    aggro.buffer_path = cpp.FilePath.new(path);
    event_mgr:send_event(aggro);
    local ccc = e:get_entity().CharacterControllerComponent
    ccc.contents:set_face_moving_direction(false);
  end
}

flying_skull_enter_block_table["combat"] = {
  update = function(self, e)  
    --print("Combat")
    local ccc = e:get_entity().CharacterControllerComponent
    ccc.contents:set_face_moving_direction(false);
  end
}

