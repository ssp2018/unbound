local func = require("ai/flying_skull/flying_skull_functionality").funcs
local const = require("ai/flying_skull/flying_skull_functionality").consts
local cpp = require("cpp")

flying_skull_exit_block_table = {}

-- Define all actions for a melee grunt
flying_skull_exit_block_table["flying_around"] = {
  update = function(self, e)
  end
}

flying_skull_exit_block_table["investigate"] = {
  update = function(self, e)
  end
}

flying_skull_exit_block_table["return_home"] = {
  update = function(self, e)
  end
}

flying_skull_exit_block_table["pursue"] = {
  update = function(self, e)
    local sc = e:get_entity().SteeringComponent
    sc.on_the_move = false;
  end
}

flying_skull_exit_block_table["combat"] = {
  update = function(self, e)
  end
}