local table = {
  melee_grunt = {
    update = require("ai/melee_grunt/melee_grunt_update_table").grunt_update_table,
    transition = require("ai/melee_grunt/melee_grunt_transition_table").grunt_transition_table,
    enter_block = require("ai/melee_grunt/melee_grunt_enter_block_table").grunt_enter_block_table,
    exit_block = require("ai/melee_grunt/melee_grunt_exit_block_table").grunt_exit_block_table
  },
  ranged_grunt = {
    update = require("ai/ranged_grunt/ranged_grunt_update_table").ranged_grunt_update_table,
    transition = require("ai/ranged_grunt/ranged_grunt_transition_table").ranged_grunt_transition_table,
    enter_block = require("ai/ranged_grunt/ranged_grunt_enter_block_table").ranged_grunt_enter_block_table,
    exit_block = require("ai/ranged_grunt/ranged_grunt_exit_block_table").ranged_grunt_exit_block_table
  },
  boss_golem = {
    update = require("ai/boss_golem/boss_golem_update_table").boss_golem_update_table,
    transition = require("ai/boss_golem/boss_golem_transition_table").boss_golem_transition_table,
    enter_block = require("ai/boss_golem/boss_golem_enter_block_table").boss_golem_enter_block_table,
    exit_block = require("ai/boss_golem/boss_golem_exit_block_table").boss_golem_exit_block_table
  },
  flying_skull = {
    update = require("ai/flying_skull/flying_skull_update_table").flying_skull_update_table,
    transition = require("ai/flying_skull/flying_skull_transition_table").flying_skull_transition_table,
    enter_block = require("ai/flying_skull/flying_skull_enter_block_table").flying_skull_enter_block_table,
    exit_block = require("ai/flying_skull/flying_skull_exit_block_table").flying_skull_exit_block_table
  },
  boss_cole = {
    update = require("ai/boss_cole/boss_cole_update_table").boss_cole_update_table,
    transition = require("ai/boss_cole/boss_cole_transition_table").boss_cole_transition_table,
    enter_block = require("ai/boss_cole/boss_cole_enter_block_table").boss_cole_enter_block_table,
    exit_block = require("ai/boss_cole/boss_cole_exit_block_table").boss_cole_exit_block_table
  }
}

access_function_table = function(file, type, state, e)
  if state == "" then
    return function(e)
      --[[empty]]
    end
  else
    return table[file][type][state]:update(e)
  end --return update function
end
