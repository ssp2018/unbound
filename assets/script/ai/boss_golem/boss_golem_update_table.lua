local func = require("ai/boss_golem/boss_golem_functionality").funcs
local const = require("ai/boss_golem/boss_golem_functionality").consts
local shared = require("ai/shared_functionality").funcs
local math = require("math")
local cpp = require("cpp")

boss_golem_update_table = {}

boss_golem_update_table["walking_around"] = {
  wait_min = 0,
  wait_max = 1,
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local cc = e:get_entity().CharacterControllerComponent
    local tc = e:get_entity().TransformComponent
    local state = ai.lua_state

    e:set_repeat_intervall("walking", 0.8)
    if (not state.waiting and not ai.funcs:is_moving()) then
      --set wait time of the ai between 1 and 4 seconds before going to next location
      state.wait_until = ai.total_state_time + math.random(self.wait_min, self.wait_max)
      state.waiting = true
    elseif state.wait_until < ai.total_state_time and state.waiting then
      e:set_repeat_intervall("walking", 0)
      radius = ai.exploration_radius
      s_point = ai.spawn_point
      local vec = cpp.vec3.new()
      local e_pos = tc.transform:get_position()
      --calculates the new x and y coordinates randomly inside their patrolling area
      vec.x = math.random(math.floor(s_point.x - radius + 0.5), math.floor(s_point.x + radius + 0.5))
      vec.y = math.random(math.floor(s_point.y - radius + 0.5), math.floor(s_point.y + radius + 0.5))
      vec.z = e_pos.z + 10.0

      -- start walking towards the new position
      shared.go_to(e, vec)
      state.waiting = false
    end
  end
}

boss_golem_update_table["vulnerable_action"] = {
  update = function(self, e)
  end
}


boss_golem_update_table["flee"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local state = ai.lua_state

    if state.threat_pos[1] == nil then
      target_pos = shared.get_target_pos(e, e:get_entity())
      state.threat_pos[1] = target_pos.x
      state.threat_pos[2] = target_pos.y
    end

    if state.threat_pos[1] ~= nil and state.threat_pos[1] ~= -666 then
      shared.back_away_from(e, state.threat_pos[1], state.threat_pos[2])
    end
  end
}

boss_golem_update_table["battle_stance"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local tc = e:get_entity().TransformComponent
    local ccc = e:get_entity().CharacterControllerComponent

    local target_pos = shared.get_target_pos(e)

    local state = ai.lua_state

    angle = shared.get_target_angle(e)

    if angle < 45 then
      if state.angle_location ~= 0 then
        state.angle_location = 0
        state.turn_speed = 1.0
      end

      if ai.has_target and shared.get_distance(e, e:get_entity(), shared.get_target_pos(e)) < const.close_range then
        shared.back_away_from_target(e)
      elseif ai.has_target then
        ai.funcs:go_to(target_pos.x, target_pos.y, target_pos.z)
      end
    elseif angle > 135 then
      if state.angle_location ~= 3 then
        state.angle_location = 3
        state.turn_speed = 1.5
      end
      if ai.has_target and shared.get_distance(e, e:get_entity(), shared.get_target_pos(e)) < const.close_range then
        shared.back_away_from_target(e)
      end
    elseif angle > 90 then
      if state.angle_location ~= 2 then
        state.angle_location = 2
        state.turn_speed = 1.4
      end
      if ai.has_target and shared.get_distance(e, e:get_entity(), shared.get_target_pos(e)) < const.close_range then
        shared.back_away_from_target(e)
      end
    elseif angle > 45 then
      if state.angle_location ~= 1 then
        state.angle_location = 1
        state.turn_speed = 1.2
      end
      if ai.has_target and shared.get_distance(e, e:get_entity(), shared.get_target_pos(e)) < const.close_range then
        shared.back_away_from_target(e)
      end
    end

    --print("lua: " .. state.turn_speed .. ", c++: " .. ccc.contents:get_turning_speed())

    ccc.contents:set_turning_speed(state.turn_speed + (state.turn_speed * ai.state_time / 2))
    e:turn_towards(target_pos.x, target_pos.y, 0)
  end
}

boss_golem_update_table["stunned"] = {
  update = function(self, e)
    --empty
  end
}

boss_golem_update_table["return_home"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    ai.funcs:go_to(ai.spawn_point.x, ai.spawn_point.y, ai.spawn_point.z)
  end
}

boss_golem_update_table["melee_attack_front"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local target_pos = shared.get_target_pos(e)

    if ai.actions_taken == 0 then
      e:turn_towards(target_pos.x, target_pos.y, 0)
    end
  end
}

boss_golem_update_table["melee_attack_behind"] = {
  update = function(self, e)
  end
}

boss_golem_update_table["rock_throw"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local target_pos = shared.get_target_pos(e)

    local state = ai.lua_state

    if ai.damage_taken_previous_frame ~= 0 then
      state.damage_taken = state.damage_taken + ai.damage_taken_previous_frame
    end

    --if ai.actions_taken == 0 then
    e:turn_towards(target_pos.x, target_pos.y, 0)
    --end
  end
}

boss_golem_update_table["rock_dropped"] = {
  update = function(self, e)
    --empty
  end
}

boss_golem_update_table["ground_slam"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    --print(ai.actions_taken)
    if ai.actions_taken > 0 and ai.state_time > (0.2 * ai.actions_taken) then
      ai.started_attack_damage = 1
      ai.actions_taken = ai.actions_taken + 1
      func.ground_slam(e)
    end
  end
}

boss_golem_update_table["chain_hook_throw"] = {
  update = function(self, e)
    if e:get_entity().AIComponent.actions_taken == 0 then
      e:turn_towards_target()
    end
  end
}

boss_golem_update_table["chain_hook_pull"] = {
  update = function(self, e)
    --empty
  end
}

boss_golem_update_table["gravel_fling"] = {
  update = function(self, e)
    if e:get_entity().AIComponent.actions_taken == 0 then
      e:turn_towards_target()
    end
  end
}
