--local cpp = require("cpp");

boss_weakspot_collission_callback = function(collision_event, tsc_name, second_handle , boss_entityh, event_mgr, entity_mgr)
  -- Get the first collider entity, assume it's the arrow
  local arrow_entity = entity_mgr.get_entity(entity_mgr, collision_event.entity_1);
  if arrow_entity.NameComponent ~= nil and arrow_entity.NameComponent.name == "player_arrow" then
    -- The assmumed arrow entity was the arrow entity
    local boss_entity = entity_mgr.get_entity(entity_mgr, boss_entityh);
    if boss_entity.HealthComponent ~= nil and arrow_entity.DamageComponent ~= nil then
      boss_entity.HealthComponent.health_points = boss_entity.HealthComponent.health_points - arrow_entity.DamageComponent.damage_value * 2;
      local weakspot_entity = entity_mgr.get_entity(entity_mgr, collision_event.entity_2);
      boss_entity.AIComponent.actions_taken = boss_entity.AIComponent.actions_taken + 1;
      entity_mgr:destroy_entity(entity_mgr.get_entity(entity_mgr, collision_event.entity_2));
    end
  end
end