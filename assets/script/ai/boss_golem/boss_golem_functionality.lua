local math = require("math")
local cpp = require("cpp")
local shared = require("ai/shared_functionality").funcs

--these values cant be modified after creation (except locally)
consts = {
    --ranges:
    close_range = 5,
    low_range = 30,
    medium_range = 60,
    long_range = 160,
    --misc:
    state_time_out = 6
}
local audio_wrapper = cpp.AudioScriptWrapper.new()

funcs = {
    print_func = function(string)
        print(string)
    end,
    --BOSS ROCK THROW
    -- rock_throw = function(e, target_entity)
    --     local entity = e:get_entity()
    --     if entity.is_valid and target_entity.is_valid then
    --         local boss_ai = entity.AIComponent
    --         local boss_tc = entity.TransformComponent
    --         local boss_ccc = entity.CharacterControllerComponent
    --         local target_tc = target_entity.TransformComponent

    --         local target_dc = target_entity.DynamicComponent
    --         local target_ccc = target_entity.CharacterControllerComponent
    --         local target_ppc = target_entity.PhysicsProjectileComponent

    --         local target_velocity = cpp.vec3.new()

    --         -- If target has any component including velocity
    --         if target_dc ~= nil then
    --             --target_velocity = target_dc.contents:get_velocity()
    --             --print("dc")
    --         elseif target_ccc ~= nil then
    --             --print("ccc")
    --             target_velocity = target_ccc.contents:get_velocity()
    --         elseif target_ppc ~= nil then
    --         --print("ppc")
    --         --target_velocity = target_ppc.contents:get_velocity()
    --         end

    --         local start = cpp.vec3.new()
    --         start = boss_tc.transform:get_position()
    --         start.z = start.z + boss_ccc.contents:get_height() + 20
    --         local target = cpp.vec3.new()
    --         target = target_tc.transform:get_position()

    --         local entity_mgr = e:get_entity_manager()
    --         local stone_entity = entity_mgr:create_entity()

    --         local transform_component = cpp.TransformComponent.new()
    --         transform_component.transform:set_position(start, e:get_frame_context())
    --         stone_entity.TransformComponent = transform_component

    --         local model_component = cpp.ModelComponent.new()
    --         model_component.asset = cpp.ModelAsset.new(cpp.FilePath.new("assets/model/boulder-boss.fbx"))
    --         stone_entity.ModelComponent = model_component

    --         local projectile_component = cpp.PhysicsProjectileComponent.new()
    --         projectile_component.contents:set_radius(10)
    --         projectile_component.contents:set_length(10)

    --         projectile_component.contents:set_velocity(shared.aim_prediction(start, target, target_velocity, 120))
    --         projectile_component.contents:disable_collision_reports_for(0.4)
    --         projectile_component.contents:disable_destruction_for(0.4)

    --         local base = cpp.create_mat4()
    --         local scale = cpp.vec3_fill(5, 5, 5)
    --         projectile_component.contents:set_offset_transform(cpp.mat4_scale(base, scale))
    --         stone_entity.PhysicsProjectileComponent = projectile_component

    --         local damage_component = cpp.DamageComponent.new()
    --         damage_component.damage_value = boss_ai.started_attack_damage
    --         damage_component.impact_damage_radius = 15
    --         stone_entity.DamageComponent = damage_component

    --         local fracture_component = cpp.FractureOnImpactComponent.new()
    --         fracture_component.radius = 20
    --         fracture_component.fragment_amount = 60
    --         fracture_component.fragment_size = 5
    --         --fracture_component.texture = e:get_texture_asset("texture/dirt_particles/4_cropped.png")
    --         fracture_component.texture = e:get_texture_asset("texture/rocks/rock_01.png")
    --         stone_entity.FractureOnImpactComponent = fracture_component

    --         local sound_settings = cpp.SoundSettings.new()
    --         sound_settings.gain_min = 1
    --         audio_wrapper:attach_audio_component(stone_entity)
    --         audio_wrapper:set_collision_sound(
    --             "audio/effects/boss1/rock_collission_sound.ogg",
    --             stone_entity,
    --             1,
    --             sound_settings
    --         )

    --         local event_mgr = e:get_event_manager()
    --         local play_sound_event = cpp.PlaySoundEvent.new()
    --         play_sound_event.id = stone_entity.get_handle
    --         play_sound_event.buffer_path = e:get_asset_path("audio/effects/boss1/rock_throwing_sound.ogg")
    --         play_sound_event.settings.gain_min = 1
    --         play_sound_event.settings.loop = false
    --         event_mgr:send_event(play_sound_event)

    --         local effect = cpp.SpecialEffectProjectileComponent.new()
    --         effect.type = 0 -- 0 = gmp::EffectType::ROCK
    --         effect.owner = entity.get_handle
    --         stone_entity.SpecialEffectProjectileComponent = effect
    --     else
    --         print("WARNING: boss rock_throw not executed, entity or target_entity not valid")
    --         if entity == nil then
    --             print("entity is fucked")
    --         end
    --         if target_entity == nil then
    --             print("target_entity is fucked")
    --         end
    --     end
    -- end,
    --ROCK DROPPED STATE
    -- rock_dropped = function(e)
    --     local entity_mgr = e:get_entity_manager()
    --     local boss_entity = e:get_entity()
    --
    --     -- remove the rock the golem currently was holding
    --     local atj_enteties = entity_mgr:get_entities({"AttachToJointComponent"})
    --     for i = 1, #atj_enteties do
    --         if atj_enteties[i].AttachToJointComponent.entity_handle == boss_entity.get_handle then
    --             entity_mgr:destroy_entity(atj_enteties[i])
    --             break
    --         end
    --     end
    --
    --     -- Create weakspot entitie on the head
    --     local weakspot_entity = entity_mgr:create_entity()
    --     -- Create transform components for the two hitboxes
    --     local tc = cpp.TransformComponent.new()
    --     local boss_tc = boss_entity.TransformComponent
    --     --tc.transform:set_position(boss_tc.transform:get_position(), e:get_frame_context())
    --     weakspot_entity.TransformComponent = tc
    --
    --     -- create a static component for the weakspot
    --     local s_c = cpp.StaticComponent.new()
    --     s_c.contents:set_collission_shape(e:get_frame_context(), e:get_capsule_shared_pointer(300, 450))
    --     s_c.contents:set_is_trigger(true)
    --     weakspot_entity.StaticComponent = s_c
    --
    --     -- attaches the hitbox entity to the joint named "head
    --     local atjc = cpp.AttachToJointComponent.new()
    --     atjc.joint_name = "neck"
    --     atjc.entity_handle = boss_entity.get_handle
    --     atjc.offset_matrix = cpp.create_mat4()
    --     local tmp_vec = cpp.vec3.new()
    --     tmp_vec.x = 0.4
    --     tmp_vec.y = 0.4
    --     tmp_vec.z = 0.4
    --     atjc.offset_matrix = cpp.mat4_scale(atjc.offset_matrix, tmp_vec)
    --     tmp_vec.x = 1
    --     tmp_vec.y = 0
    --     tmp_vec.z = 0
    --     atjc.offset_matrix = cpp.get_mat4_rot(atjc.offset_matrix, tmp_vec, 90)
    --     tmp_vec.x = -265
    --     tmp_vec.y = 0
    --     tmp_vec.z = 265
    --     atjc.offset_matrix = cpp.mat4_translate(atjc.offset_matrix, tmp_vec)
    --     weakspot_entity.AttachToJointComponent = atjc
    --
    --     --attaches a model to the hitbox enteties
    --     local m_c = cpp.ModelComponent.new()
    --     m_c.asset = e:get_model_asset("model/bump.fbx")
    --     weakspot_entity.ModelComponent = m_c
    --
    --     -- remove the lifetime component when switching to animation notifiers
    --     local lt_c = cpp.LifetimeComponent.new()
    --     lt_c.time_left = 3
    --     weakspot_entity.LifetimeComponent = lt_c
    --
    --     tsc = cpp.TriggerScriptComponent.new()
    --     tsc.on_touch_callback =
    --     tsc.on_touch_callback =
    --         cpp.Object.new(
    --         "assets/script/ai/boss_golem/boss_head_weakspot_callback.lua",
    --         "boss_weakspot_collission_callback"
    --     )
    --     tsc.target_entity = e:get_entity().get_handle
    --     tsc.name = "boss_weakspot_tsc"
    --     weakspot_entity.TriggerScriptComponent = tsc
    --     --print("spawned weakspot");
    -- end,
    destroy_rock = function(e)
        local entity_mgr = e:get_entity_manager()
        local boss_entity = e:get_entity()

        local atj_enteties = entity_mgr:get_entities({"AttachToJointComponent"})
        for i = 1, #atj_enteties do
            if atj_enteties[i].AttachToJointComponent.entity_handle == boss_entity.get_handle then
                atj_enteties[i].DestroyedComponent = cpp.DestroyedComponent.new()
                local rock_tc = atj_enteties[i].TransformComponent
                local rock_pos = rock_tc.transform:get_position()

                local emitter_entity = entity_mgr:create_entity()
                local tc = cpp.TransformComponent.new()
                tc.transform:set_position(cpp.vec3_fill(rock_pos.x, rock_pos.y, rock_pos.z), e:get_frame_context())
                emitter_entity.TransformComponent = tc

                local emitter_component = cpp.ParticlesComponent.new()
                emitter_component.max_particles = 100
                emitter_component.spawn_per_second = 20
                emitter_component.texture = e:get_texture_asset("texture/rocks/rock_02.png")
                emitter_component.min_start_color = cpp.vec4_fill(1, 1, 1, 1)
                emitter_component.max_start_color = cpp.vec4_fill(1, 1, 1, 1)
                emitter_component.min_end_color = cpp.vec4_fill(1, 1, 1, 1)
                emitter_component.max_end_color = cpp.vec4_fill(1, 1, 1, 1)
                emitter_component.velocity_mode = 1
                emitter_component.min_rotation_speed = -2
                emitter_component.max_rotation_speed = 2
                emitter_component.min_start_velocity = cpp.vec3_fill(-50, -50, -50)
                emitter_component.max_start_velocity = cpp.vec3_fill(50, 50, 50)
                emitter_component.min_start_size = 3
                emitter_component.max_start_size = 7
                emitter_component.min_life = 1
                emitter_component.max_life = 1
                emitter_component.min_spawn_pos = cpp.vec3_fill(-5, -5, -5)
                emitter_component.max_spawn_pos = cpp.vec3_fill(5, 5, 5)
                emitter_entity.ParticlesComponent = emitter_component

                local emitter_death = cpp.EmitterDeathCountdownComponent.new()
                emitter_death.death_countdown = 0.6
                emitter_entity.EmitterDeathCountdownComponent = emitter_death

                break
            end
        end
    end,
    -- --BOSS CHAIN HOOK
    -- chain_throw = function(e, target_entity)
    --     local entity = e:get_entity()
    --     if entity.is_valid and target_entity.is_valid then
    --         local boss_ai = entity.AIComponent
    --         local boss_tc = entity.TransformComponent

    --         local target_tc = target_entity.TransformComponent
    --         local target_ccc = target_entity.CharacterControllerComponent

    --         local target_velocity = cpp.vec3.new()

    --         if target_ccc ~= nil then
    --             target_velocity = target_ccc.contents:get_velocity()
    --         end

    --         local start = cpp.vec3.new()
    --         start = boss_tc.transform:get_position()
    --         start.z = start.z + 25
    --         local target_pos = cpp.vec3.new()
    --         target_pos = target_tc.transform:get_position()

    --         local entity_mgr = e:get_entity_manager()
    --         local hook_entity = entity_mgr:create_entity()

    --         local transform_component = cpp.TransformComponent.new()
    --         transform_component.transform:set_position(start, e:get_frame_context())
    --         hook_entity.TransformComponent = transform_component

    --         local model_component = cpp.ModelComponent.new()
    --         model_component.asset = e:get_model_asset("model/hook.fbx")
    --         hook_entity.ModelComponent = model_component

    --         local projectile_component = cpp.PhysicsProjectileComponent.new()
    --         projectile_component.contents:set_radius(4)
    --         projectile_component.contents:set_length(2)
    --         projectile_component.contents:set_velocity(
    --             shared.aim_prediction(start, target_pos, target_velocity, 120)
    --         )
    --         projectile_component.contents:disable_collision_reports_for(0.4)
    --         projectile_component.contents:disable_destruction_for(0.4)

    --         hook_entity.PhysicsProjectileComponent = projectile_component

    --         local hook_component = cpp.HookComponent.new()
    --         hook_component.owner = entity.get_handle
    --         hook_component.origin = boss_tc.transform:get_position()
    --         hook_component.hook_pull_in_speed = 70
    --         hook_component.release_distance = 6
    --         hook_entity.HookComponent = hook_component

    --         local effect = cpp.SpecialEffectProjectileComponent.new()
    --         effect.type = 2 -- 2 = EffectType::HOOK
    --         effect.owner = entity.get_handle
    --         hook_entity.SpecialEffectProjectileComponent = effect

    --         local sound_settings = cpp.SoundSettings.new()
    --         audio_wrapper:attach_audio_component(hook_entity)
    --         audio_wrapper:set_collision_sound(
    --             "audio/effects/boss1/chain_throw_hit_sound1.ogg",
    --             hook_entity,
    --             1,
    --             sound_settings
    --         )

    --         local event_mgr = e:get_event_manager()
    --         local play_sound_event = cpp.PlaySoundEvent.new()
    --         play_sound_event.id = hook_entity.get_handle
    --         local path = "audio/effects/boss1/chain_throw_sound" .. (math.random(1, 2)) .. ".ogg"
    --         play_sound_event.buffer_path = e:get_asset_path(path)
    --         play_sound_event.settings.pitch = 1
    --         play_sound_event.settings.gain = 5
    --         play_sound_event.settings.loop = false
    --         event_mgr:send_event(play_sound_event)

    --         --connect rope between ai entity and hook entity
    --         local rope = cpp.RopeComponent.new()
    --         rope.other = hook_entity.get_handle
    --         rope.scale = 1.0
    --         entity.RopeComponent = rope
    --     else
    --         print("WARNING: boss chain_throw not executed, entity or target_entity not valid")
    --         if entity == nil then
    --             print("entity is fucked")
    --         end
    --         if target_entity == nil then
    --             print("target_entity is fucked")
    --         end
    --     end
    -- end,
    --BOSS GROUND SLAM
    ground_slam = function(e, entity)
        local entity = e:get_entity()
        if entity.is_valid then
            local entity_mgr = e:get_entity_manager()

            local boss_ai = entity.AIComponent
            local boss_tc = entity.TransformComponent

            local shockwave_damage = 2
            local shockwave_width = 5
            local radius_end = shockwave_width * (boss_ai.actions_taken) + 3
            local radius_start = radius_end - shockwave_width

            --local boss_pos = cpp.vec3.new()
            local boss_pos = boss_tc.transform:get_position()
            local boss_look = boss_tc.transform:get_forward()
            local chockwave_center_point = cpp.vec3_add(boss_pos, cpp.vec3_scalar_multiplication(boss_look, 2))

            local entities =
                entity_mgr:get_entities({"HealthComponent", "TransformComponent", "CharacterControllerComponent"})
            for i, curr_ent in ipairs(entities) do
                if curr_ent.get_handle ~= entity.get_handle then
                    local e_tc = curr_ent.TransformComponent
                    local e_ccc = curr_ent.CharacterControllerComponent

                    local ent_pos = e_tc.transform:get_position()
                    local dist = cpp.distance_vec3(ent_pos, chockwave_center_point)
                    if (dist > radius_start and dist < radius_end and e_ccc.contents:on_ground()) then
                        -- reduce hp
                        local e_hp = curr_ent.HealthComponent
                        e_hp.health_points = e_hp.health_points - boss_ai.started_attack_damage

                        -- knock back (this is a brute force approach, dont use)
                        local knock = cpp.KnockedBackStunComponent.new()
                        knock.max_time = 1
                        local dir = cpp.normalize_vec3(cpp.vec3_subtract(ent_pos, chockwave_center_point))
                        dir.z = 1
                        knock.movement = cpp.vec3_scalar_multiplication(dir, 50)
                        curr_ent.KnockedBackStunComponent = knock
                    end
                end
            end

            -- create shockwave particles
            local transform_component = cpp.TransformComponent.new()
            local emitter_component = cpp.ParticlesComponent.new()

            emitter_component.max_particles = 100
            emitter_component.spawn_per_second = 20
            emitter_component.texture = e:get_texture_asset("texture/explosion.png")
            emitter_component.min_start_color = cpp.vec4_fill(0.3, 0, 0, 1)
            emitter_component.max_start_color = cpp.vec4_fill(0.5, 0.1, 0, 1)
            emitter_component.min_end_color = cpp.vec4_fill(0.3, 0, 0, 1)
            emitter_component.max_end_color = cpp.vec4_fill(0.5, 0.1, 0, 1)
            emitter_component.velocity_mode = 1
            emitter_component.min_rotation_speed = -2
            emitter_component.max_rotation_speed = 2
            emitter_component.min_start_velocity = cpp.vec3_fill(0, 0, 50)
            emitter_component.max_start_velocity = cpp.vec3_fill(0, 0, 80)
            emitter_component.min_start_size = 3
            emitter_component.max_start_size = 7
            emitter_component.min_life = 1
            emitter_component.max_life = 1
            chockwave_center_point.z = chockwave_center_point.z + 0.5

            local dist = radius_end - (shockwave_width / 2)

            for i = 0, 3 do
                local emitter_entity = entity_mgr:create_entity()

                if i == 0 then
                    transform_component.transform:set_position(
                        cpp.vec3_fill(
                            chockwave_center_point.x,
                            chockwave_center_point.y + dist,
                            chockwave_center_point.z
                        ),
                        e:get_frame_context()
                    )
                    emitter_component.min_spawn_pos = cpp.vec3_fill(-dist, -1, -20)
                    emitter_component.max_spawn_pos = cpp.vec3_fill(dist, 1, 0)
                elseif i == 1 then
                    transform_component.transform:set_position(
                        cpp.vec3_fill(
                            chockwave_center_point.x + dist,
                            chockwave_center_point.y,
                            chockwave_center_point.z
                        ),
                        e:get_frame_context()
                    )
                    emitter_component.min_spawn_pos = cpp.vec3_fill(-1, -dist, -20)
                    emitter_component.max_spawn_pos = cpp.vec3_fill(1, dist, 0)
                elseif i == 2 then
                    transform_component.transform:set_position(
                        cpp.vec3_fill(
                            chockwave_center_point.x,
                            chockwave_center_point.y - dist,
                            chockwave_center_point.z
                        ),
                        e:get_frame_context()
                    )
                    emitter_component.min_spawn_pos = cpp.vec3_fill(-dist, -1, -20)
                    emitter_component.max_spawn_pos = cpp.vec3_fill(dist, 1, 0)
                elseif i == 3 then
                    transform_component.transform:set_position(
                        cpp.vec3_fill(
                            chockwave_center_point.x - dist,
                            chockwave_center_point.y,
                            chockwave_center_point.z
                        ),
                        e:get_frame_context()
                    )
                    emitter_component.min_spawn_pos = cpp.vec3_fill(-1, dist, -20)
                    emitter_component.max_spawn_pos = cpp.vec3_fill(1, -dist, 0)
                end

                -- emitter_component.spawn_duration_left = 0.4

                emitter_entity.TransformComponent = transform_component
                emitter_entity.ParticlesComponent = emitter_component

                local emitter_death = cpp.EmitterDeathCountdownComponent.new()
                emitter_death.death_countdown = 0.3
                emitter_entity.EmitterDeathCountdownComponent = emitter_death
            end
        else
            print("WARNING: boss ground_slam not executed, entity not valid")
            if entity == nil then
                print("entity is fucked")
            end
        end
    end
    --BOSS GRAVEL FLING
    --     gravel_fling = function(e, target_entity)
    --         local entity = e:get_entity()
    --         if entity.is_valid and target_entity.is_valid then
    --             local boss_ai = entity.AIComponent
    --             local boss_tc = entity.TransformComponent
    --             local target_tc = target_entity.TransformComponent

    --             local start = boss_tc.transform:get_position()
    --             start.z = start.z + 25
    --             local target = target_tc.transform:get_position()
    --             local target_ccc = target_entity.CharacterControllerComponent

    --             local target_velocity = cpp.vec3.new()

    --             if target_ccc ~= nil then
    --                 target_velocity = target_ccc.contents:get_velocity()
    --             end

    --             local target_pos = cpp.vec3.new()
    --             target_pos = target_tc.transform:get_position()

    --             local entity_mgr = e:get_entity_manager()
    --             local gravel_entity = entity_mgr:create_entity()

    --             local transform_component = cpp.TransformComponent.new()
    --             transform_component.transform:set_position(start, e:get_frame_context())
    --             gravel_entity.TransformComponent = transform_component

    --             local model_component = cpp.ModelComponent.new()
    --             model_component.asset = e:get_model_asset("model/gravel_mesh_2.fbx")
    --             gravel_entity.ModelComponent = model_component

    --             local projectile_component = cpp.PhysicsProjectileComponent.new()
    --             projectile_component.contents:set_radius(5)
    --             projectile_component.contents:set_length(5)
    --             projectile_component.contents:set_velocity(
    --                 shared.aim_prediction(start, target_pos, target_velocity, 120)
    --             )
    --             projectile_component.contents:disable_collision_reports_for(0.4)
    --             projectile_component.contents:disable_destruction_for(10)
    --             local base = cpp.create_mat4()
    --             base = cpp.mat4_translate(base, cpp.vec3_fill(5, 1, 1))
    --             local scale = cpp.vec3_fill(20, 20, 20)
    --             projectile_component.contents:set_offset_transform(cpp.mat4_scale(base, scale))
    --             gravel_entity.PhysicsProjectileComponent = projectile_component

    --             local damage_component = cpp.DamageComponent.new()
    --             damage_component.damage_value = boss_ai.started_attack_damage
    --             damage_component.impact_damage_radius = 12
    --             gravel_entity.DamageComponent = damage_component

    --             local name = cpp.NameComponent.new()
    --             name.name = "gravel"
    --             gravel_entity.NameComponent = name

    --             local continue_movement = cpp.ContinueMovementAfterImpactComponent.new()
    --             gravel_entity.ContinueMovementAfterImpactComponent = continue_movement

    --             local sound_settings = cpp.SoundSettings.new()
    --             audio_wrapper:attach_audio_component(gravel_entity)
    --             audio_wrapper:set_collision_sound(
    --                 "audio/effects/boss1/gravel_combined.ogg",
    --                 gravel_entity,
    --                 1,
    --                 sound_settings
    --             )

    --             local event_mgr = e:get_event_manager()
    --             local play_sound_event = cpp.PlaySoundEvent.new()
    --             play_sound_event.id = entity.get_handle
    --             play_sound_event.buffer_path = e:get_asset_path("audio/effects/boss1/sweep.ogg")
    --             play_sound_event.settings.gain_min = 1
    --             event_mgr:send_event(play_sound_event)

    --             local effect = cpp.SpecialEffectProjectileComponent.new()
    --             effect.type = 1 -- 1 = EffectType::GRAVEL
    --             effect.owner = entity.get_handle
    --             gravel_entity.SpecialEffectProjectileComponent = effect
    --         else
    --             print("WARNING: boss gravel_fling not executed, entity or target_entity not valid")
    --             if entity == nil then
    --                 print("entity is fucked")
    --             end
    --             if target_entity == nil then
    --                 print("target_entity is fucked")
    --             end
    --         end
    --     end,
    --     --BOSS MELEE ATTACK FRONT
    --     melee_attack_front = function(e)
    --         local entity = e:get_entity()
    --         if entity.is_valid then
    --             local boss_handle = entity.get_handle
    --             local boss_ai = entity.AIComponent
    --             local boss_tc = entity.TransformComponent

    --             if boss_ai.actions_taken == 1 then
    --                 --create hitbox
    --                 local entity_mgr = e:get_entity_manager()
    --                 local hitbox_entity = entity_mgr:create_entity()

    --                 local random_sound = audio_wrapper:get_random_sound_from_group("boss_melee_attack")
    --                 if random_sound ~= nil then
    --                     audio_wrapper:attach_audio_component(hitbox_entity)
    --                     audio_wrapper:play_delayed_sound_from_group(
    --                         hitbox_entity.get_handle,
    --                         "boss_melee_attack",
    --                         random_sound,
    --                         0.5
    --                     )
    --                 else
    --                     print("'boss_melee_attack' sound group does not exist")
    --                 end

    --                 local damage_component = cpp.DamageComponent.new()
    --                 damage_component.damage_value = boss_ai.started_attack_damage
    --                 damage_component.impact_damage_radius = 0
    --                 hitbox_entity.DamageComponent = damage_component

    --                 local transform_component = cpp.TransformComponent.new()
    --                 transform_component.transform:set_position(boss_tc.transform:get_position(), e:get_frame_context())
    --                 hitbox_entity.TransformComponent = transform_component

    --                 local sc = cpp.StaticComponent.new()
    --                 sc.contents:set_collission_shape(e:get_frame_context(), e:get_capsule_shared_pointer(200, 80))
    --                 sc.contents:set_is_trigger(true)
    --                 hitbox_entity.StaticComponent = sc

    --                 local atjc = cpp.AttachToJointComponent.new()
    --                 atjc.entity_handle = entity.get_handle
    --                 atjc.joint_name = "R_hand"
    --                 -- atjc.offset_matrix = ... --used for fine-tuning the hitbox placement
    --                 hitbox_entity.AttachToJointComponent = atjc

    --                 local fc = cpp.FactionComponent.new()
    --                 fc.type = 2 -- HUMAN
    --                 hitbox_entity.FactionComponent = fc
    --             else
    --                 --destroy hitbox
    --                 local entity_mgr = e:get_entity_manager()
    --                 local entities = entity_mgr:get_entities({"StaticComponent", "AttachToJointComponent"})
    --                 for i, curr_ent in ipairs(entities) do
    --                     local atjc = curr_ent.AttachToJointComponent
    --                     local tc = curr_ent.TransformComponent
    --                     local owner = atjc.entity_handle
    --                     if owner == boss_handle then
    --                         local temp2 = cpp.vec3.new()
    --                         entity_mgr:destroy_entity(curr_ent)
    --                         return
    --                     end
    --                 end
    --             end
    --         else
    --             print("WARNING: boss melee not executed, entity not valid")
    --             if entity == nil then
    --                 print("entity is fucked")
    --             end
    --         end
    --     end,
    --     --BOSS MELEE ATTACK BACK
    --     melee_attack_behind = function(e)
    --         local entity = e:get_entity()
    --         funcs.melee_attack_front(e, entity)
    --     end
}
