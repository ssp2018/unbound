local func = require("ai/boss_golem/boss_golem_functionality").funcs
local const = require("ai/boss_golem/boss_golem_functionality").consts
local cpp = require("cpp")

boss_golem_enter_block_table = {}

boss_golem_enter_block_table["walking_around"] = {
  update = function(self, e)
    e:activate_repeating_audio_group("walking", true)
    e:set_repeat_intervall("walking", 0.8)
    local cc = e:get_entity().CharacterControllerComponent
    cc.contents:set_face_moving_direction(true)
    local ai = e:get_entity().AIComponent
    local hp = e:get_entity().HealthComponent
    local state = ai.lua_state
    state.wait_until = 0
    state.waiting = true
    hp.health_points = hp.max_health_points
  end
}

boss_golem_enter_block_table["vulnerable_action"] = {
  update = function(self, e)
    e:activate_repeating_audio_group("walking", false)
    local cc = e:get_entity().CharacterControllerComponent
    cc.contents:set_face_moving_direction(true)
  end
}

boss_golem_enter_block_table["flee"] = {
  update = function(self, e)
    e:activate_repeating_audio_group("walking", true)
    e:set_repeat_intervall("walking", 0.3)
    local cc = e:get_entity().CharacterControllerComponent
    cc.contents:set_face_moving_direction(true)
    local ai = e:get_entity().AIComponent
    local state = ai.lua_state
    state.threat_pos = {nil, nil}
  end
}

boss_golem_enter_block_table["battle_stance"] = {
  update = function(self, e)
    e:activate_repeating_audio_group("walking", true)
    e:set_repeat_intervall("walking", 0.5)
    local cc = e:get_entity().CharacterControllerComponent
    cc.contents:set_face_moving_direction(false)
    local ai = e:get_entity().AIComponent
    local state = ai.lua_state
    state.angle_location = -1
    state.turn_speed = 1.0
    state.previous_state = ""
  end
}

boss_golem_enter_block_table["stunned"] = {
  update = function(self, e)
    --empty
  end
}

boss_golem_enter_block_table["return_home"] = {
  update = function(self, e)
    e:activate_repeating_audio_group("walking", true)
    e:set_repeat_intervall("walking", 1)
    local cc = e:get_entity().CharacterControllerComponent
    cc.contents:set_face_moving_direction(true)
  end
}

boss_golem_enter_block_table["rock_throw"] = {
  update = function(self, e)
    e:activate_repeating_audio_group("walking", false)
    entity = e:get_entity()
    local ai = entity.AIComponent
    ai.started_attack_damage = 1
    local state = ai.lua_state
    state.damage_taken = 0
    state.anm_triggers = 0
    entity.TriggerScriptComponent = cpp.TriggerScriptComponent.new()
    entity.TriggerScriptComponent.on_touch_callback =
      cpp.Object.new("assets/script/ai/boss_golem/boss_golem_anim_notify_callbacks.lua", "notify_event_handler")
  end
}

boss_golem_enter_block_table["melee_attack_front"] = {
  update = function(self, e)
    e:activate_repeating_audio_group("walking", false)
    entity = e:get_entity()
    entity.TriggerScriptComponent = cpp.TriggerScriptComponent.new()
    entity.TriggerScriptComponent.on_touch_callback =
      cpp.Object.new("assets/script/ai/boss_golem/boss_golem_anim_notify_callbacks.lua", "notify_event_handler")
  end
}

boss_golem_enter_block_table["melee_attack_behind"] = {
  update = function(self, e)
    e:activate_repeating_audio_group("walking", false)
    entity = e:get_entity()
    entity.TriggerScriptComponent = cpp.TriggerScriptComponent.new()
    entity.TriggerScriptComponent.on_touch_callback =
      cpp.Object.new("assets/script/ai/boss_golem/boss_golem_anim_notify_callbacks.lua", "notify_event_handler")
  end
}

boss_golem_enter_block_table["rock_dropped"] = {
  update = function(self, e)
    e:activate_repeating_audio_group("walking", false)
    entity = e:get_entity()
    entity.TriggerScriptComponent = cpp.TriggerScriptComponent.new()
    entity.TriggerScriptComponent.on_touch_callback =
      cpp.Object.new("assets/script/ai/boss_golem/boss_golem_anim_notify_callbacks.lua", "notify_event_handler")
  end
}

boss_golem_enter_block_table["ground_slam"] = {
  update = function(self, e)
    e:activate_repeating_audio_group("walking", false)
    entity = e:get_entity()
    entity.TriggerScriptComponent = cpp.TriggerScriptComponent.new()
    entity.TriggerScriptComponent.on_touch_callback =
      cpp.Object.new("assets/script/ai/boss_golem/boss_golem_anim_notify_callbacks.lua", "notify_event_handler")
  end
}

boss_golem_enter_block_table["chain_hook_throw"] = {
  update = function(self, e)
    e:activate_repeating_audio_group("walking", false)
    entity = e:get_entity()
    entity.TriggerScriptComponent = cpp.TriggerScriptComponent.new()
    entity.TriggerScriptComponent.on_touch_callback =
      cpp.Object.new("assets/script/ai/boss_golem/boss_golem_anim_notify_callbacks.lua", "notify_event_handler")
    -- Create the hook in the hand
    local entity_mgr = e:get_entity_manager()
    local hook = entity_mgr:create_entity()
    local tc = cpp.TransformComponent.new()
    --tc.transform:set_scale(1.5, e:get_frame_context())
    hook.TransformComponent = tc
    local atjc = cpp.AttachToJointComponent.new()
    atjc.entity_handle = entity.get_handle
    atjc.joint_name = "R_weapon"
    hook.AttachToJointComponent = atjc
    local mc = cpp.ModelComponent.new()
    mc.asset = cpp.ModelAsset.new(cpp.FilePath.new("assets/model/hook.fbx"))
    hook.ModelComponent = mc
    local nc = cpp.NameComponent.new()
    nc.name = "hrh"
    hook.NameComponent = nc

  end
}

boss_golem_enter_block_table["chain_hook_pull"] = {
  update = function(self, e)
    e:activate_repeating_audio_group("walking", false)
  end
}
boss_golem_enter_block_table["gravel_fling"] = {
  update = function(self, e)
    e:activate_repeating_audio_group("walking", false)
    local cc = e:get_entity().CharacterControllerComponent
    cc.contents:set_face_moving_direction(false)
    cc.contents:set_turning_speed(2.0)
    entity = e:get_entity()
    entity.TriggerScriptComponent = cpp.TriggerScriptComponent.new()
    entity.TriggerScriptComponent.on_touch_callback =
      cpp.Object.new("assets/script/ai/boss_golem/boss_golem_anim_notify_callbacks.lua", "notify_event_handler")
  end
}
