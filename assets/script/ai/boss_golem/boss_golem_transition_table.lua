local func = require("ai/boss_golem/boss_golem_functionality").funcs
local const = require("ai/boss_golem/boss_golem_functionality").consts
local shared = require("ai/shared_functionality").funcs
local boss_golem_update_table = require("ai/boss_golem/boss_golem_update_table").boss_golem_update_table
local math = require("math")
local cpp = require("cpp")

--local state_time_out = 6

boss_golem_transition_table = {}

boss_golem_transition_table["walking_around"] = {
  vulnerable_action_cooldown = 7,
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local cc = e:get_entity().CharacterControllerComponent

    if ai.state_time > self.vulnerable_action_cooldown then
      return "vulnerable_action"
    end

    if shared.look_at_surroundings(e, e:get_entity()) then
      return "battle_stance"
    end

    return ""
  end
}

boss_golem_transition_table["vulnerable_action"] = {
  vulnerable_time = 3,
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local cc = e:get_entity().CharacterControllerComponent

    if shared.look_at_surroundings(e, e:get_entity()) then
      return "battle_stance"
    end

    if ai.state_time > self.vulnerable_time then
      return "walking_around"
    end

    return ""
  end
}

boss_golem_transition_table["flee"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local cc = e:get_entity().CharacterControllerComponent

    -- when the ai has no target it has no threat to any entity
    if ai.has_target == false or (e.state_time > 10 and shared.get_dist_to_target(e, e:get_entity()) > long_range) then
      return "return_home"
    end
    return ""
  end
}

boss_golem_transition_table["battle_stance"] = {
  wait_time = 1,
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local cc = e:get_entity().CharacterControllerComponent
    local tc = e:get_entity().TransformComponent
    local state = ai.lua_state
    if not ai.has_target then
      return "return_home"
    end

    local target = shared.get_target_entity(e)
    local target_ccc = target.CharacterControllerComponent

    local target_on_ground = target_ccc.contents:on_ground()
    local target_distance = shared.get_distance(e, tc, shared.get_target_pos(e))

    --if target is in close range, do a melee attack or ground slam
    if target_distance < (const.low_range / 2) then
      local rand = math.random(1, 2)
      if rand == 1 then
        if ai.previous_state ~= "ground_slam" then
          return "ground_slam"
        else
          return "rock_throw"
        end
      else
        angle = shared.get_target_angle(e)
        if angle <= 90 then
          return "melee_attack_front"
        else
          return "melee_attack_behind"
        end
      end
    end

    --if Golem looks at target (more or less)
    if state.angle_location == 0 then
      --vary between chain_hook_throw, rock_throw, gravel_fling and ground_slam (higher chance of chain_hook_throw when target standing on ground)
      if ai.state_time > self.wait_time and (target_distance > const.low_range) and (target_distance < const.long_range) then
        if ai.previous_state ~= "chain_hook_throw" and target_on_ground then
          local rand = math.random(1, 4)
          if rand == 1 then
            return "chain_hook_throw"
          elseif rand == 2 then
            if ai.previous_state ~= "ground_slam" then
              return "ground_slam"
            else
              return "gravel_fling"
            end
          end
        else
          if target_distance < const.medium_range then
            local rand = math.random(1, 10)
            if rand < 3 and ai.previous_state ~= "ground_slam" and target_distance < const.medium_range then
              return "ground_slam"
            elseif rand < 7 and ai.previous_state ~= "rock_throw" and target_distance > const.close_range then
              return "rock_throw"
            elseif rand <= 10 and ai.previous_state ~= "gravel_fling" and target_distance > const.close_range then
              return "gravel_fling"
            end
          end

          local rand = math.random(1, 10)
          if rand < 4 then
            return "gravel_fling"
          elseif rand < 8 and ai.previous_state ~= "rock_throw" then
            return "rock_throw"
          elseif rand <= 10 then
            return "chain_hook_throw"
          end
        end
      end
    end
    return ""
  end
}

boss_golem_transition_table["stunned"] = {
  update = function(self, e)
    ai = e:get_entity().AIComponent

    if ai.state_time > tonumber(ai.misc) then
      return "battle_stance"
    end

    return ""
  end
}

boss_golem_transition_table["return_home"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local cc = e:get_entity().CharacterControllerComponent
    local tc = e:get_entity().TransformComponent

    local pos = tc.transform:get_position()
    local boss_2d_pos = cpp.vec2.new()
    boss_2d_pos.x = pos.x
    boss_2d_pos.y = pos.y

    if shared.get_distance(e, ai.spawn_point, boss_2d_pos) < 3 then
      return "walking_around"
    elseif shared.look_at_surroundings(e, e:get_entity()) then
      return "battle_stance"
    end

    return ""
  end
}

boss_golem_transition_table["melee_attack_front"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent

    --failsafe due to animation system
    if ai.state_time > const.state_time_out then
      return "battle_stance"
    end

    if e:get_entity().AIComponent.actions_taken > 3 then
      return "battle_stance"
    end

    return ""
  end
}

boss_golem_transition_table["melee_attack_behind"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent

    --failsafe due to animation system
    if ai.state_time > const.state_time_out then
      return "battle_stance"
    end

    if e:get_entity().AIComponent.actions_taken > 3 then
      return "battle_stance"
    end

    return ""
  end
}

boss_golem_transition_table["rock_throw"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent

    --failsafe due to animation system
    if ai.state_time > const.state_time_out then
      return "battle_stance"
    end

    if ai.actions_taken > 2 then
      return "battle_stance"
    elseif ai.actions_taken == 1 and ai.frame_hit_count == 6 then
      func.destroy_rock(e)
      return "rock_dropped"
    end
  end
}

boss_golem_transition_table["rock_dropped"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent

    --failsafe due to animation system
    if ai.state_time > const.state_time_out then
      return "battle_stance"
    end

    if ai.actions_taken > 1 then
      return "battle_stance"
    end

    return ""
  end
}

boss_golem_transition_table["ground_slam"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent

    --failsafe due to animation system
    if ai.state_time > const.state_time_out then
      return "battle_stance"
    end

    if ai.actions_taken == -1 then
      return "battle_stance"
    end

    return ""
  end
}

boss_golem_transition_table["chain_hook_throw"] = {
  update = function(self, e)
    local entity = e:get_entity()

    local ai = entity.AIComponent

    --failsafe due to animation system
    if ai.state_time > const.state_time_out then
      return "battle_stance"
    end

    if entity.AIComponent.actions_taken == 1 and entity.HookHitComponent ~= nil then
      return "chain_hook_pull"
    elseif entity.AIComponent.actions_taken == 2 then
      return "battle_stance"
    end

    return ""
  end
}

boss_golem_transition_table["chain_hook_pull"] = {
  cc_pull_threshold = 6,
  update = function(self, e)
    local ai = e:get_entity().AIComponent

    --failsafe due to animation system
    if ai.state_time > const.state_time_out then
      return "battle_stance"
    end

    local tc = e:get_entity().TransformComponent
    local entity_mgr = e:get_entity_manager()
    local hhc = e:get_entity().HookHitComponent
    local hook_target = entity_mgr:get_entity(hhc.target)
    local hook_target_tc = hook_target.TransformComponent

    -- If the golem has finished its pulling animation or if the player is close enough
    if ai.actions_taken == 1 or shared.get_distance(e, tc, hook_target_tc) < self.cc_pull_threshold then
      --return "battle_stance"
      return "ground_slam"
    end

    return ""
  end
}

boss_golem_transition_table["gravel_fling"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent

    --failsafe due to animation system
    if ai.state_time > const.state_time_out then
      return "battle_stance"
    end

    if e:get_entity().AIComponent.actions_taken > 1 then
      return "battle_stance"
    end

    return ""
  end
}
