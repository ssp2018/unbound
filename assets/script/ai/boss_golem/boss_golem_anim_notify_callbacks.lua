local math = require("math")
local cpp = require("cpp")
local shared = require("ai/shared_functionality").funcs

--these values cant be modified after creation (except locally)
consts = {
    close_range = 5,
    low_range = 30,
    medium_range = 60,
    long_range = 160
}

local audio_wrapper = cpp.AudioScriptWrapper.new()

notify_event_handler = function(animation_name, boss_entity, event_mgr, entity_mgr, frame_context)
    if boss_entity.AIComponent ~= nil then
        local ai = boss_entity.AIComponent

        if (animation_name == "boss_anm_rockthrow" and ai.state_name == "rock_throw") then
            boss_rock_throw_anim_callback(boss_entity, event_mgr, entity_mgr, frame_context)
        elseif (animation_name == "boss_anm_groundslam" and ai.state_name == "ground_slam") then
            boss_ground_slam_anim_callback(boss_entity, event_mgr, entity_mgr, frame_context)
        elseif
            ((animation_name == "boss_anm_attackfront" or animation_name == "boss_anm_attackbehind") and
                ai.state_name == "melee_attack_behind" or
                ai.state_name == "melee_attack_front")
         then
            boss_melee_attack_anim_callback(boss_entity, event_mgr, entity_mgr, frame_context)
        elseif (animation_name == "boss_anm_gravelfling" and ai.state_name == "gravel_fling") then
            boss_gravel_fling_anim_callback(boss_entity, event_mgr, entity_mgr, frame_context)
        elseif (animation_name == "boss_anm_hook" and ai.state_name == "chain_hook_throw") then
            boss_hook_throw_anim_callback(boss_entity, event_mgr, entity_mgr, frame_context)
        elseif (animation_name == "boss_anm_hookpull" and ai.state_name == "chain_hook_pull") then
            boss_hook_pull_anim_callback(boss_entity, event_mgr, entity_mgr, frame_context)
        elseif (animation_name == "boss_anm_wounded" and ai.state_name == "rock_throw") then
            boss_rock_dropped_anim_callback(boss_entity, event_mgr, entity_mgr, frame_context)
        end
    end
end

boss_rock_throw_anim_callback = function(boss_entity, event_mgr, entity_mgr, frame_context)
    if boss_entity.AIComponent ~= nil then
        local boss_ai = boss_entity.AIComponent
        local target_entity = entity_mgr:get_entity(boss_ai.target)
        local state = boss_ai.lua_state
        state.anm_triggers = state.anm_triggers + 1

        --print("ROCK THROW actions: " .. boss_ai.actions_taken .. ", triggers: " .. state.anm_triggers)

        if target_entity.is_valid then
            if boss_ai.actions_taken == 0 then
                holding_rock_entity = entity_mgr:create_entity()

                holding_rock_entity.NameComponent = cpp.NameComponent.new()
                holding_rock_entity.NameComponent.name = "brh"

                local transform_component = cpp.TransformComponent.new()
                transform_component.transform:set_scale(5, frame_context)
                holding_rock_entity.TransformComponent = transform_component

                local model_component = cpp.ModelComponent.new()
                model_component.asset = cpp.ModelAsset.new(cpp.FilePath.new("assets/model/boulder-boss.fbx"))
                holding_rock_entity.ModelComponent = model_component

                local atjc = cpp.AttachToJointComponent.new()
                atjc.joint_name = "L_weapon"
                atjc.entity_handle = boss_entity.get_handle
                atjc.offset_matrix = cpp.create_mat4()
                local tmp_vec = cpp.vec3.new()
                tmp_vec.x = 0
                tmp_vec.y = -220
                tmp_vec.z = 0
                atjc.offset_matrix = cpp.mat4_translate(atjc.offset_matrix, tmp_vec)
                holding_rock_entity.AttachToJointComponent = atjc

                boss_ai.actions_taken = boss_ai.actions_taken + 1
            elseif boss_ai.actions_taken == 1 then
                --print(boss_ai.actions_taken)
                -- Clean up the rock the boss currently is holding
                local name_enteties = entity_mgr:get_entities({"NameComponent"})
                for i = 1, #name_enteties do
                    if name_enteties[i].NameComponent.name == "brh" then
                        entity_mgr:destroy_entity(name_enteties[i])
                        break
                    end
                end

                -- Spawn the new rock
                local boss_tc = boss_entity.TransformComponent
                local boss_ccc = boss_entity.CharacterControllerComponent

                local target_tc = target_entity.TransformComponent

                local target_dc = target_entity.DynamicComponent
                local target_ccc = target_entity.CharacterControllerComponent
                local target_ppc = target_entity.PhysicsProjectileComponent

                local target_velocity = cpp.vec3.new()

                -- If target has any component including velocity
                if target_dc ~= nil then
                    --target_velocity = target_dc.contents:get_velocity()
                    --print("dc")
                elseif target_ccc ~= nil then
                    --print("ccc")
                    target_velocity = target_ccc.contents:get_velocity()
                elseif target_ppc ~= nil then
                --print("ppc")
                --target_velocity = target_ppc.contents:get_velocity()
                end

                local start = cpp.vec3.new()
                start = boss_tc.transform:get_position()
                start.z = start.z + boss_ccc.contents:get_height() + 20
                local target = cpp.vec3.new()
                target = target_tc.transform:get_position()

                local stone_entity = entity_mgr:create_entity()

                local transform_component = cpp.TransformComponent.new()
                transform_component.transform:set_position(start, frame_context)
                transform_component.transform:set_scale(5, frame_context)
                stone_entity.TransformComponent = transform_component

                local model_component = cpp.ModelComponent.new()
                model_component.asset = cpp.ModelAsset.new(cpp.FilePath.new("assets/model/boulder-boss.fbx"))
                stone_entity.ModelComponent = model_component

                local projectile_component = cpp.PhysicsProjectileComponent.new()
                projectile_component.contents:set_radius(10)
                projectile_component.contents:set_length(10)
                projectile_component.contents:set_mass(200)
                projectile_component.contents:set_velocity(shared.aim_prediction(start, target, target_velocity, 120))
                projectile_component.contents:disable_collision_reports_for(0.4)
                projectile_component.contents:disable_destruction_for(1000)

                local base = cpp.create_mat4()
                local scale = cpp.vec3_fill(5, 5, 5)
                projectile_component.contents:set_offset_transform(cpp.mat4_scale(base, scale))
                stone_entity.PhysicsProjectileComponent = projectile_component

                local damage_component = cpp.DamageComponent.new()
                damage_component.damage_value = boss_ai.started_attack_damage
                damage_component.impact_damage_radius = 25
                stone_entity.DamageComponent = damage_component

                local fracture_component = cpp.FractureOnImpactComponent.new()
                fracture_component.radius = 20
                fracture_component.fragment_amount = 60
                fracture_component.fragment_size = 5

                local knock = cpp.KnockBackComponent.new()
                knock.max_time = 1
                knock.radius = 50
                knock.force = 50
                knock.force_dir_override[3].apply_force = true
                knock.force_dir_override[3].force = 3
                stone_entity.KnockBackComponent = knock

                local dc = cpp.DestructibleComponent.new()
                dc.health_points = 1
                dc.max_health_points = 1
                stone_entity.DestructibleComponent = dc

                fracture_component.texture = cpp.TextureAsset.new(cpp.FilePath.new("assets/texture/rocks/rock_01.png"))
                stone_entity.FractureOnImpactComponent = fracture_component

                local sound_settings = cpp.SoundSettings.new()
                sound_settings.gain_min = 1
                audio_wrapper:attach_audio_component(stone_entity)
                audio_wrapper:set_collision_sound(
                    "audio/effects/boss1/rock_collission_sound.ogg",
                    stone_entity,
                    1,
                    sound_settings
                )

                local play_sound_event = cpp.PlaySoundEvent.new()
                play_sound_event.id = stone_entity.get_handle
                play_sound_event.buffer_path = cpp.FilePath.new("assets/audio/effects/boss1/rock_throwing_sound.ogg")
                play_sound_event.settings.gain_min = 1
                play_sound_event.settings.loop = false
                event_mgr:send_event(play_sound_event)

                --local effect = cpp.SpecialEffectProjectileComponent.new()
                --effect.type = 0 -- 0 = gmp::EffectType::ROCK
                --effect.owner = boss_entity.get_handle
                --stone_entity.SpecialEffectProjectileComponent = effect
                boss_ai.actions_taken = boss_ai.actions_taken + 1
            elseif boss_ai.actions_taken == 2 then
                boss_ai.actions_taken = boss_ai.actions_taken + 1
            end
        else
            print("the boss target entity is invalid")
        end
    else
        if boss_entity == nil then
            print("boss entity has no ai component!")
        end
    end
end

boss_ground_slam_anim_callback = function(boss_entity, event_mgr, entity_mgr, frame_context)
    boss_ai = boss_entity.AIComponent
    if boss_ai.actions_taken == 0 then
        local play_sound_event = cpp.PlaySoundEvent.new()
        play_sound_event.id = boss_entity.get_handle
        play_sound_event.buffer_path = cpp.FilePath.new("assets/audio/effects/boss1/ground_slam.ogg")
        play_sound_event.settings.pitch = 1
        play_sound_event.settings.gain = 5
        play_sound_event.settings.loop = false
        event_mgr:send_event(play_sound_event)
        boss_ai.actions_taken = boss_ai.actions_taken + 1
    elseif boss_ai.actions_taken ~= 0 then
        boss_ai.actions_taken = -1
    end
end

boss_melee_attack_anim_callback = function(boss_entity, event_mgr, entity_mgr, frame_context)
    local boss_ai = boss_entity.AIComponent
    local boss_tc = boss_entity.TransformComponent

    if boss_ai.actions_taken == 0 then
        --create hitbox
        local hitbox_entity = entity_mgr:create_entity()

        --local random_sound = audio_wrapper:get_random_sound_from_group("boss_melee_attack")
        --if random_sound ~= nil then
        --    audio_wrapper:attach_audio_component(hitbox_entity)
        --    audio_wrapper:play_delayed_sound_from_group(
        --        hitbox_entity.get_handle,
        --        "boss_melee_attack",
        --        random_sound,
        --        0.5
        --    )
        --else
        --    print("'boss_melee_attack' sound group does not exist")
        --end

        local play_sound_event_delayed = cpp.PlaySoundDelayedEvent.new()
        play_sound_event_delayed.id = boss_entity.get_handle
        play_sound_event_delayed.buffer_path = cpp.FilePath.new("assets/audio/effects/boss1/melee1.ogg")
        play_sound_event_delayed.delay_time = 0.5
        play_sound_event_delayed.settings.gain_min = 0.2
        event_mgr:send_event(play_sound_event_delayed)

        local damage_component = cpp.DamageComponent.new()
        damage_component.damage_value = boss_ai.started_attack_damage
        damage_component.impact_damage_radius = 0
        hitbox_entity.DamageComponent = damage_component

        local transform_component = cpp.TransformComponent.new()
        transform_component.transform:set_position(boss_tc.transform:get_position(), frame_context)
        hitbox_entity.TransformComponent = transform_component

        local sc = cpp.StaticComponent.new()
        --fix capsule thingie
        sc.contents:set_collission_shape(frame_context, cpp.make_shared_CCS(200, 80))
        sc.contents:set_is_trigger(true)
        hitbox_entity.StaticComponent = sc

        local atjc = cpp.AttachToJointComponent.new()
        atjc.entity_handle = boss_entity.get_handle
        atjc.joint_name = "R_hand"
        -- atjc.offset_matrix = ... --used for fine-tuning the hitbox placement
        hitbox_entity.AttachToJointComponent = atjc

        --local knock = cpp.KnockBackComponent.new()
        --knock.max_time = 1
        --knock.radius = 0
        --knock.force = 70
        --knock.force_dir_override[3].apply_force = true
        --knock.force_dir_override[3].force = 3
        --hitbox_entity.KnockBackComponent = knock

        local fc = cpp.FactionComponent.new()
        fc.type = 2 -- HUMAN
        hitbox_entity.FactionComponent = fc
        boss_ai.actions_taken = boss_ai.actions_taken + 1
    elseif boss_ai.actions_taken == 1 then
        --destroy hitbox
        local entities = entity_mgr:get_entities({"StaticComponent", "AttachToJointComponent"})
        for i, curr_ent in ipairs(entities) do
            local atjc = curr_ent.AttachToJointComponent
            local owner = atjc.entity_handle
            if owner == boss_entity.get_handle then
                local temp2 = cpp.vec3.new()
                entity_mgr:destroy_entity(curr_ent)
                return
            end
        end
        boss_ai.actions_taken = boss_ai.actions_taken + 1
    elseif boss_ai.actions_taken == 2 then
        boss_ai.actions_taken = boss_ai.actions_taken + 1
    end
end

boss_gravel_fling_anim_callback = function(boss_entity, event_mgr, entity_mgr, frame_context)
    local boss_ai = boss_entity.AIComponent
    local target_entity = entity_mgr:get_entity(boss_ai.target)
    if boss_ai.actions_taken == 0 then
        boss_ai.actions_taken = boss_ai.actions_taken + 1
        if target_entity.is_valid then
            --local effect = cpp.SpecialEffectProjectileComponent.new()
            --effect.type = 1 -- 1 = EffectType::GRAVEL
            --effect.owner = boss_entity.get_handle
            --gravel_entity.SpecialEffectProjectileComponent = effect
            local boss_tc = boss_entity.TransformComponent
            local target_tc = target_entity.TransformComponent

            local start = boss_tc.transform:get_position()
            start.z = start.z + 25
            local target = target_tc.transform:get_position()
            local target_ccc = target_entity.CharacterControllerComponent

            local target_velocity = cpp.vec3.new()

            if target_ccc ~= nil then
                target_velocity = target_ccc.contents:get_velocity()
            end

            local target_pos = cpp.vec3.new()
            target_pos = target_tc.transform:get_position()

            local gravel_entity = entity_mgr:create_entity()

            local transform_component = cpp.TransformComponent.new()
            transform_component.transform:set_position(start, frame_context)
            gravel_entity.TransformComponent = transform_component

            local model_component = cpp.ModelComponent.new()
            model_component.asset = cpp.ModelAsset.new(cpp.FilePath.new("assets/model/gravel_mesh_2.fbx"))
            gravel_entity.ModelComponent = model_component

            local projectile_component = cpp.PhysicsProjectileComponent.new()
            projectile_component.contents:set_radius(5)
            projectile_component.contents:set_length(5)
            projectile_component.contents:set_velocity(shared.aim_prediction(start, target_pos, target_velocity, 120))
            projectile_component.contents:disable_collision_reports_for(0.4)
            projectile_component.contents:disable_destruction_for(10)
            local base = cpp.create_mat4()
            base = cpp.mat4_translate(base, cpp.vec3_fill(5, 1, 1))
            local scale = cpp.vec3_fill(20, 20, 20)
            projectile_component.contents:set_offset_transform(cpp.mat4_scale(base, scale))
            gravel_entity.PhysicsProjectileComponent = projectile_component

            local damage_component = cpp.DamageComponent.new()
            damage_component.damage_value = boss_ai.started_attack_damage
            damage_component.impact_damage_radius = 12
            gravel_entity.DamageComponent = damage_component

            local name = cpp.NameComponent.new()
            name.name = "gravel"
            gravel_entity.NameComponent = name

            local continue_movement = cpp.ContinueMovementAfterImpactComponent.new()
            gravel_entity.ContinueMovementAfterImpactComponent = continue_movement

            local sound_settings = cpp.SoundSettings.new()
            audio_wrapper:attach_audio_component(gravel_entity)
            audio_wrapper:set_collision_sound(
                "audio/effects/boss1/gravel_combined.ogg",
                gravel_entity,
                1,
                sound_settings
            )

            local play_sound_event = cpp.PlaySoundEvent.new()
            play_sound_event.id = boss_entity.get_handle
            play_sound_event.buffer_path = cpp.FilePath.new("assets/audio/effects/boss1/sweep.ogg")
            play_sound_event.settings.gain_min = 1
            event_mgr:send_event(play_sound_event)
        else
            if target_entity == nil then
                print("target_entity is fucked")
            end
        end
    elseif boss_ai.actions_taken == 1 then
        boss_ai.actions_taken = boss_ai.actions_taken + 1
    end
end

boss_hook_throw_anim_callback = function(boss_entity, event_mgr, entity_mgr, frame_context)
    local boss_ai = boss_entity.AIComponent
    local target_entity = entity_mgr:get_entity(boss_ai.target)
    if boss_ai.actions_taken == 0 then
        -- Remove hook from hand
        local start = cpp.vec3.new()
        local name_entities = entity_mgr:get_entities({"NameComponent", "TransformComponent"})
        for i = 1, #name_entities do
            if name_entities[i].NameComponent.name == "hrh" then
                start = name_entities[i].TransformComponent.transform:get_position()
                entity_mgr:destroy_entity(name_entities[i])
                break
            end
        end
        boss_ai.actions_taken = boss_ai.actions_taken + 1
        if target_entity.is_valid then
            local boss_tc = boss_entity.TransformComponent

            local target_tc = target_entity.TransformComponent
            local target_ccc = target_entity.CharacterControllerComponent

            local target_velocity = cpp.vec3.new()

            if target_ccc ~= nil then
                target_velocity = target_ccc.contents:get_velocity()
            end
            local target_pos = cpp.vec3.new()
            target_pos = target_tc.transform:get_position()
            target_pos.z = target_pos.z - 1.8

            local hook_entity = entity_mgr:create_entity()

            local transform_component = cpp.TransformComponent.new()
            transform_component.transform:set_position(start, frame_context)
            hook_entity.TransformComponent = transform_component

            local model_component = cpp.ModelComponent.new()
            model_component.asset = cpp.ModelAsset.new(cpp.FilePath.new("assets/model/hook.fbx"))
            hook_entity.ModelComponent = model_component

            local projectile_component = cpp.PhysicsProjectileComponent.new()
            projectile_component.contents:set_radius(1.5)
            projectile_component.contents:set_length(3)
            projectile_component.contents:set_mass(100)
            projectile_component.contents:set_velocity(shared.aim_prediction(start, target_pos, target_velocity, 120))
            projectile_component.contents:disable_collision_reports_for(0.1)
            projectile_component.contents:disable_destruction_for(1000)

            hook_entity.PhysicsProjectileComponent = projectile_component

            local hook_component = cpp.HookComponent.new()
            hook_component.owner = boss_entity.get_handle
            hook_component.origin = boss_tc.transform:get_position()
            hook_component.hook_pull_in_speed = 70
            hook_component.release_distance = 6
            hook_entity.HookComponent = hook_component

            --local effect = cpp.SpecialEffectProjectileComponent.new()
            --effect.type = 2 -- 2 = EffectType::HOOK
            --effect.owner = boss_entity.get_handle
            --hook_entity.SpecialEffectProjectileComponent = effect

            local dc = cpp.DestructibleComponent.new()
            dc.health_points = 1
            dc.max_health_points = 1
            hook_entity.DestructibleComponent = dc

            local sound_settings = cpp.SoundSettings.new()
            audio_wrapper:attach_audio_component(hook_entity)
            audio_wrapper:set_collision_sound(
                "audio/effects/boss1/chain_throw_hit_sound1.ogg",
                hook_entity,
                1,
                sound_settings
            )

            local play_sound_event = cpp.PlaySoundEvent.new()
            play_sound_event.id = hook_entity.get_handle
            local path = "assets/audio/effects/boss1/chain_throw_sound" .. (math.random(1, 2)) .. ".ogg"
            play_sound_event.buffer_path = cpp.FilePath.new(path)
            play_sound_event.settings.pitch = 1
            play_sound_event.settings.gain = 5
            play_sound_event.settings.loop = false
            event_mgr:send_event(play_sound_event)

            --connect rope between ai entity and hook entity
            local rope_entity = entity_mgr:create_entity()
            local rope = cpp.RopeComponent.new()
            rope.other = hook_entity.get_handle
            rope.scale = 1.0
            rope_entity.RopeComponent = rope
            local rope_atjc = cpp.AttachToJointComponent.new()
            rope_atjc.entity_handle = boss_entity.get_handle
            rope_atjc.joint_name = "R_weapon"
            rope_entity.AttachToJointComponent = rope_atjc
            local rope_tc = cpp.TransformComponent.new()
            rope_entity.TransformComponent = rope_tc

            
            hook_entity.HookComponent.rope  = rope_entity.get_handle
            
        else
            if target_entity == nil then
                print("target_entity is fucked")
            end
        end
    elseif boss_ai.actions_taken == 1 then
        boss_ai.actions_taken = boss_ai.actions_taken + 1
    end
end

boss_hook_pull_anim_callback = function(boss_entity, event_mgr, entity_mgr, frame_context)
    if boss_entity.AIComponent ~= nil then
        local boss_ai = boss_entity.AIComponent
        boss_ai.actions_taken = boss_ai.actions_taken + 1
    end
end

boss_rock_dropped_anim_callback = function(boss_entity, event_mgr, entity_mgr, frame_context)
    if boss_entity.is_valid then
        boss_ai = boss_entity.AIComponent
        if boss_ai.actions_taken == 0 then
            -- remove the rock the golem currently was holding
            local atj_enteties = entity_mgr:get_entities({"AttachToJointComponent"})
            for i = 1, #atj_enteties do
                if atj_enteties[i].AttachToJointComponent.entity_handle == boss_entity.get_handle then
                    --entity_mgr:destroy_entity(atj_enteties[i])
                    local destroy = cpp.DestroyedComponent.new()
                    atj_enteties[i].DestroyedComponent = destroy
                    break
                end
            end

            -- Create weakspot entitie on the head
            local weakspot_entity = entity_mgr:create_entity()
            -- Create transform components for the two hitboxes
            local tc = cpp.TransformComponent.new()
            local boss_tc = boss_entity.TransformComponent
            --tc.transform:set_position(boss_tc.transform:get_position(), e:get_frame_context())
            weakspot_entity.TransformComponent = tc

            -- create a static component for the weakspot
            local s_c = cpp.StaticComponent.new()

            s_c.contents:set_collission_shape(frame_context, cpp.make_shared_CCS(200, 80))
            s_c.contents:set_is_trigger(true)
            weakspot_entity.StaticComponent = s_c

            -- attaches the hitbox entity to the joint named "head
            local atjc = cpp.AttachToJointComponent.new()
            atjc.joint_name = "neck"
            atjc.entity_handle = boss_entity.get_handle
            atjc.offset_matrix = cpp.create_mat4()
            local tmp_vec = cpp.vec3.new()
            tmp_vec.x = 0.4
            tmp_vec.y = 0.4
            tmp_vec.z = 0.4
            atjc.offset_matrix = cpp.mat4_scale(atjc.offset_matrix, tmp_vec)
            tmp_vec.x = 1
            tmp_vec.y = 0
            tmp_vec.z = 0
            atjc.offset_matrix = cpp.get_mat4_rot(atjc.offset_matrix, tmp_vec, 90)
            tmp_vec.x = -290
            tmp_vec.y = 0
            tmp_vec.z = 290
            atjc.offset_matrix = cpp.mat4_translate(atjc.offset_matrix, tmp_vec)
            weakspot_entity.AttachToJointComponent = atjc

            --attaches a model to the hitbox enteties
            local m_c = cpp.ModelComponent.new()
            m_c.asset = cpp.ModelAsset.new(cpp.FilePath.new("assets/model/bump.fbx"))
            weakspot_entity.ModelComponent = m_c

            tsc = cpp.TriggerScriptComponent.new()
            tsc.on_touch_callback =
                cpp.Object.new(
                "assets/script/ai/boss_golem/boss_head_weakspot_callback.lua",
                "boss_weakspot_collission_callback"
            )
            tsc.target_entity = boss_entity.get_handle
            tsc.name = "boss_weakspot_tsc"
            weakspot_entity.TriggerScriptComponent = tsc

            local name_component = cpp.NameComponent.new()
            name_component.name = "ws"
            weakspot_entity.NameComponent = name_component

            boss_ai.actions_taken = boss_ai.actions_taken + 1
        elseif boss_ai.actions_taken == 1 then
            local name_enteties = entity_mgr:get_entities({"NameComponent"})
            for i = 1, #name_enteties do
                if name_enteties[i].NameComponent.name == "ws" then
                    entity_mgr:destroy_entity(name_enteties[i])
                    break
                end
            end
            boss_ai.actions_taken = boss_ai.actions_taken + 1
        end
    end
end
