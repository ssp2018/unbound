local func = require("ai/boss_golem/boss_golem_functionality").funcs
local const = require("ai/boss_golem/boss_golem_functionality").consts
local cpp = require("cpp")

boss_golem_exit_block_table = {}

boss_golem_exit_block_table["walking_around"] = {
  update = function(self, e)
  end
}

boss_golem_exit_block_table["vulnerable_action"] = {
  update = function(self, e)
  end
}


boss_golem_exit_block_table["flee"] = {
  update = function(self, e)
  end
}

boss_golem_exit_block_table["battle_stance"] = {
  update = function(self, e)
  end
}

boss_golem_exit_block_table["stunned"] = {
  update = function(self, e)
    --empty
  end
}

boss_golem_exit_block_table["return_home"] = {
  update = function(self, e)
  end
}

boss_golem_exit_block_table["rock_throw"] = {
  update = function(self, e)
    --remember to destroy the stone (if created and not thrown) on death!
  end
}

boss_golem_exit_block_table["melee_attack_front"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent

    if ai.actions_taken == 1 then
      func.melee_attack_front(e)
    end
  end
}

boss_golem_exit_block_table["melee_attack_behind"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent

    if ai.actions_taken == 1 then
      func.melee_attack_front(e)
    end
  end
}

boss_golem_exit_block_table["rock_dropped"] = {
  update = function(self, e)
  end
}

boss_golem_exit_block_table["ground_slam"] = {
  update = function(self, e)
  end
}

boss_golem_exit_block_table["chain_hook_throw"] = {
  update = function(self, e)
    --remember to destroy the hook (if created and not thrown) on death!

    local entity_mgr = e:get_entity_manager()
    local hook_entities = entity_mgr:get_entities({"HookComponent"})
    for i = 1, #hook_entities do
      local hc = hook_entities[i].HookComponent
      if entity_mgr:is_valid(hc.rope) then
        local rope = entity_mgr:get_entity(hc.rope)
        local destroy = cpp.DestroyedComponent.new()
        rope.DestroyedComponent = destroy
      end
    end
    -- remove anchor if stuck to hand
    local aic = e:get_entity().AIComponent
    if aic ~= nil and aic.actions_taken < 1 then 
      local name_entities = entity_mgr:get_entities({"NameComponent"})
      for i = 1, #name_entities do
          if name_entities[i].NameComponent.name == "hrh" then
              entity_mgr:destroy_entity(name_entities[i])
              break
          end
      end
    end
  end
}

boss_golem_exit_block_table["chain_hook_pull"] = {
  update = function(self, e)
    -- Remove hook when leaving chain hook hit state.
    local entity_mgr = e:get_entity_manager()
    local entity = e:get_entity()
    local hhc = entity.HookHitComponent
    if hhc ~= nil then
      local hook = entity_mgr:get_entity(hhc.hook)
      if hook ~= nil then
        local ltc = cpp.LifetimeComponent.new()
        ltc.time_left = 0.0
        hook.LifetimeComponent = ltc
      end
      local rope = entity_mgr:get_entity(hhc.rope)
      if rope ~= nil then 
        local destroy = cpp.DestroyedComponent.new()
        rope.DestroyedComponent = destroy
      end
      entity.HookHitComponent = nil
    end
  end
}
boss_golem_exit_block_table["gravel_fling"] = {
  update = function(self, e)
  end
}
