local func = require("ai/melee_grunt/melee_grunt_functionality").funcs
local const = require("ai/melee_grunt/melee_grunt_functionality").consts
local cpp = require("cpp")

grunt_enter_block_table = {}

grunt_enter_block_table["walking_around"] = {
  update = function(self, e)
    local cc = e:get_entity().CharacterControllerComponent
    cc.contents:set_face_moving_direction(true)
    local ai = e:get_entity().AIComponent
    ai.movement_speed = ai.speed_base
    local hp = e:get_entity().HealthComponent
    hp.health_points = hp.max_health_points
    local state = ai.lua_state
    state.wait_until = 0
    state.waiting = true
  end
}

grunt_enter_block_table["vulnerable_action"] = {
  update = function(self, e)
    local cc = e:get_entity().CharacterControllerComponent
    local ai = e:get_entity().AIComponent
    cc.contents:set_face_moving_direction(true)
  end
}

grunt_enter_block_table["investigate"] = {
  update = function(self, e)
    local cc = e:get_entity().CharacterControllerComponent
    cc.contents:set_face_moving_direction(true)
    local ai = e:get_entity().AIComponent
    ai.movement_speed = ai.speed_base * const.speed_investigate
  end
}

grunt_enter_block_table["flee"] = {
  update = function(self, e)
    local cc = e:get_entity().CharacterControllerComponent
    cc.contents:set_face_moving_direction(true)
    local ai = e:get_entity().AIComponent
    ai.movement_speed = ai.speed_base * const.speed_sprint
  end
}

grunt_enter_block_table["battle_stance"] = {
  update = function(self, e)
    local cc = e:get_entity().CharacterControllerComponent
    cc.contents:set_face_moving_direction(true)
    local ai = e:get_entity().AIComponent
    ai.movement_speed = ai.speed_base * const.speed_battle
    local state = ai.lua_state
    state.angle_location = 0
  end
}

grunt_enter_block_table["return_home"] = {
  update = function(self, e)
    local cc = e:get_entity().CharacterControllerComponent
    cc.contents:set_face_moving_direction(true)
    local ai = e:get_entity().AIComponent
    ai.movement_speed = ai.speed_base
  end
}

grunt_enter_block_table["raise_shield"] = {
  update = function(self, e)
    local cc = e:get_entity().CharacterControllerComponent
    cc.contents:set_face_moving_direction(false)
    local ai = e:get_entity().AIComponent
    ai.movement_speed = ai.speed_base * const.speed_shield_raise
    ai.damage_taken_previous_frame = 0
  end
}

grunt_enter_block_table["sprint"] = {
  update = function(self, e)
    local cc = e:get_entity().CharacterControllerComponent
    cc.contents:set_face_moving_direction(false)
    local ai = e:get_entity().AIComponent
    ai.movement_speed = ai.speed_base * const.speed_sprint
  end
}

grunt_enter_block_table["shield_sword_poke"] = {
  update = function(self, e)
    local cc = e:get_entity().CharacterControllerComponent
    cc.contents:set_face_moving_direction(false)
    local state = ai.lua_state
    state.attack_finished = false
  end
}

grunt_enter_block_table["melee_attack"] = {
  update = function(self, e)
    local cc = e:get_entity().CharacterControllerComponent
    cc.contents:set_face_moving_direction(false)
    local ai = e:get_entity().AIComponent
    ai.started_attack_damage = ai.base_attack_damage
    local state = ai.lua_state
    state.attack_finished = false
  end
}
