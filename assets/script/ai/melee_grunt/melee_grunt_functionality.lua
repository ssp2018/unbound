local shared = require("ai/shared_functionality").funcs
local math = require("math")
local cpp = require("cpp")

consts = {
    --ranges:
    close_range = 1.5,
    low_range = 4,
    medium_range = 10,
    long_range = 20,
    --speeds:
    speed_sprint = 2.5,
    speed_battle = 1.5,
    speed_shield_raise = 0.75,
    speed_investigate = 1.5
}

funcs = {
    melee_attack = function(e, duration)
        local entity = e:get_entity()
        if entity.is_valid then
            local grunt_handle = entity.get_handle
            local grunt_ai = entity.AIComponent
            local grunt_tc = entity.TransformComponent

            if grunt_ai.actions_taken == 1 then
                --create hitbox
                local entity_mgr = e:get_entity_manager()
                local hitbox_entity = entity_mgr:create_entity()

                --local ac = entity.AudioComponent
                --local random_sound = ac.audio:get_random_sound_from_regular_group("sword_swinging")
                --local delay_snd = ac.audio:get_sound_from_group("sword_swinging", random_sound)
                --if (delay_snd ~= nil) then
                --    ac.audio:play_sound_delayed(delay_snd, 2)
                --end

                --local tc = cpp.TransformComponent.new()
                --hitbox_entity.TransformComponent = tc

                local transform_component = cpp.TransformComponent.new()
                transform_component.transform:set_position(grunt_tc.transform:get_position(), e:get_frame_context())
                hitbox_entity.TransformComponent = transform_component

                local sc = cpp.StaticComponent.new()
                sc.contents:set_collission_shape(e:get_frame_context(), e:get_capsule_shared_pointer(0.1, 1))
                sc.contents:set_is_trigger(true)
                hitbox_entity.StaticComponent = sc

                local dc = cpp.DamageComponent.new()
                dc.friendly_fire = false
                dc.damage_value = grunt_ai.started_attack_damage
                --dc.damage_value = 0.0f
                hitbox_entity.DamageComponent = dc

                local fc = cpp.FactionComponent.new()
                fc.type = 2
                hitbox_entity.FactionComponent = fc

                local atjc = cpp.AttachToJointComponent.new()
                atjc.entity_handle = entity.get_handle
                atjc.joint_name = "R_hand"
                local tmp_vec = cpp.vec3.new()
                tmp_vec.x = 0
                tmp_vec.y = 0
                tmp_vec.z = -50
                atjc.offset_matrix = cpp.mat4_translate(atjc.offset_matrix, tmp_vec)
                -- atjc.offset_matrix = ... --used for fine-tuning the hitbox placement
                hitbox_entity.AttachToJointComponent = atjc

                local ltc = cpp.LifetimeComponent.new()
                ltc.time_left = duration
                hitbox_entity.LifetimeComponent = ltc
            else
                --destroy hitbox
                local entity_mgr = e:get_entity_manager()
                local entities = entity_mgr:get_entities({"StaticComponent", "AttachToJointComponent"})
                for i, curr_ent in ipairs(entities) do
                    local atjc = curr_ent.AttachToJointComponent
                    local tc = curr_ent.TransformComponent
                    local owner = atjc.entity_handle
                    if owner == grunt_handle then
                        local temp2 = cpp.vec3.new()
                        entity_mgr:destroy_entity(curr_ent)
                        return
                    end
                end
            end
        end
    end
}
