local func = require("ai/melee_grunt/melee_grunt_functionality").funcs
local const = require("ai/melee_grunt/melee_grunt_functionality").consts
local cpp = require("cpp")

grunt_exit_block_table = {}

grunt_exit_block_table["walking_around"] = {
  update = function(self, e)
  end
}

grunt_exit_block_table["vulnerable_action"] = {
  update = function(self, e)
  end
}

grunt_exit_block_table["investigate"] = {
  update = function(self, e)
  end
}

grunt_exit_block_table["flee"] = {
  update = function(self, e)
  end
}

grunt_exit_block_table["battle_stance"] = {
  update = function(self, e)
  end
}

grunt_exit_block_table["return_home"] = {
  update = function(self, e)
  end
}

grunt_exit_block_table["raise_shield"] = {
  update = function(self, e)
  end
}

grunt_exit_block_table["sprint"] = {
  update = function(self, e)
  end
}

grunt_exit_block_table["shield_sword_poke"] = {
  update = function(self, e)
  end
}

grunt_exit_block_table["melee_attack"] = {
  update = function(self, e)
  end
}
