local grunt_update_table = require("ai/melee_grunt/melee_grunt_update_table").grunt_update_table
local func = require("ai/melee_grunt/melee_grunt_functionality").funcs
local const = require("ai/melee_grunt/melee_grunt_functionality").consts
local shared = require("ai/shared_functionality").funcs
local cpp = require("cpp")

grunt_transition_table = {}

grunt_transition_table["walking_around"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent

    if ai.state_time > 20 then
      return "vulnerable_action"
    end

    if shared.look_at_surroundings(e) then
      return "battle_stance"
    end

    if shared.listen_to_surroundings(e) then
      return "investigate"
    end

    return ""
  end
}

grunt_transition_table["vulnerable_action"] = {
  vulnerable_time = 10,
  update = function(self, e)
    local ai = e:get_entity().AIComponent

    if shared.look_at_surroundings(e) then
      return "battle_stance"
    end

    if ai.state_time > self.vulnerable_time then
      return "walking_around"
    end

    if (shared.listen_to_surroundings(e)) then
      return "investigate"
    end

    return ""
  end
}

grunt_transition_table["investigate"] = {
  investigation_time = 10,
  update = function(self, e)
    local ai = e:get_entity().AIComponent

    if shared.look_at_surroundings(e) then
      return "battle_stance"
    elseif shared.listen_to_surroundings(e) then
      return "investigate"
    elseif ai.state_time > self.investigation_time then
      return "return_home"
    end

    return ""
  end
}
grunt_transition_table["flee"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local cc = e:get_entity().CharacterControllerComponent
    local tc = e:get_entity().TransformComponent

    if
      not ai.has_target or
        (ai.state_time > 10 and shared.get_distance(e, tc, shared.get_target_pos(e)) > const.long_range)
     then
      return "return_home"
    elseif ai.morale_data.morale + ai.morale_data.morale_boost > ai.morale_data.morale_reenter_combat_threshold then
      return "battle_stance"
    end
    return ""
  end
}

grunt_transition_table["battle_stance"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local tc = e:get_entity().TransformComponent
    if not ai.has_target then
      return "return_home"
    elseif ai.damage_taken_previous_frame ~= 0 then
      return "raise_shield"
    elseif shared.get_distance(e,tc, shared.get_target_pos(e)) < const.close_range then
      return "melee_attack"
    elseif ai.morale_data.morale + ai.morale_data.morale_boost < ai.morale_data.morale_flee_threshold then
      return "flee"
    elseif shared.get_distance(e,tc, shared.get_target_pos(e)) > const.long_range then
      return "sprint"
    end

    return ""
  end
}

grunt_transition_table["return_home"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local tc = e:get_entity().TransformComponent

    local param = cpp.vec2.new()
    param.x = ai.spawn_point.x
    param.y = ai.spawn_point.y

    if shared.get_distance(e, ai.spawn_point, tc.transform:get_position()) < 1 then
      return "walking_around"
    elseif shared.look_at_surroundings(e) then
      return "battle_stance"
    end
    return ""
  end
}

grunt_transition_table["raise_shield"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local tc = e:get_entity().TransformComponent
    if not ai.has_target then
      return "return_home"
    elseif shared.get_distance(e, tc, shared.get_target_pos(e)) < const.close_range then
      return "shield_sword_poke"
    elseif (ai.has_target and shared.get_distance(e, tc, shared.get_target_pos(e)) > const.long_range) then
      return "sprint"
    elseif ai.state_time > 5 then
      return "battle_stance"
    end

    return ""
  end
}

grunt_transition_table["sprint"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local tc = e:get_entity().TransformComponent
    if ai.has_target == false then
      return "return_home"
    elseif ai.morale_data.morale + ai.morale_data.morale_boost < ai.morale_data.morale_flee_threshold then
      return "flee"
    elseif shared.get_distance(e, tc, shared.get_target_pos(e)) < const.medium_range then
      return "battle_stance"
    end

    return ""
  end
}

grunt_transition_table["shield_sword_poke"] = {
  tot_attack_time = grunt_update_table["shield_sword_poke"].wind_up_time +
    grunt_update_table["shield_sword_poke"].freeze_time +
    grunt_update_table["shield_sword_poke"].swing_time,
  update = function(self, e)
    local ai = e:get_entity().AIComponent

    local state = ai.lua_state
    if state.attack_finished then
      return "raise_shield"
    end

    return ""
  end
}

grunt_transition_table["melee_attack"] = {
  tot_attack_time = grunt_update_table["melee_attack"].wind_up_time + grunt_update_table["melee_attack"].freeze_time +
    grunt_update_table["melee_attack"].swing_time,
  update = function(self, e)
    local ai = e:get_entity().AIComponent

    local state = ai.lua_state
    if state.attack_finished then
      return "battle_stance"
    end

    return ""
  end
}
