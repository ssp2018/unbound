local func = require("ai/melee_grunt/melee_grunt_functionality").funcs
local const = require("ai/melee_grunt/melee_grunt_functionality").consts
local shared = require("ai/shared_functionality").funcs
local math = require("math")
local cpp = require("cpp")

grunt_update_table = {}

grunt_update_table["walking_around"] = {
  wait_min = 0,
  wait_max = 1,
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local cc = e:get_entity().CharacterControllerComponent
    local tc = e:get_entity().TransformComponent
    -- extract the state from ai component
    local state = ai.lua_state

    if (not state.waiting and not ai.funcs:is_moving()) then
      --set wait time of the ai before going to next location
      state.wait_until = ai.total_state_time + math.random(self.wait_min, self.wait_max)
      state.waiting = true
    elseif state.wait_until < ai.total_state_time and state.waiting then
      radius = ai.exploration_radius
      s_point = ai.spawn_point
      -- calculates the new x and y coordinates randomly inside their patrolling area
      x = math.random(math.floor(s_point.x - radius), math.floor(s_point.x + radius))
      y = math.random(math.floor(s_point.y - radius), math.floor(s_point.y + radius))
      -- start walking towards the new position
      ai.funcs:go_to(x, y, tc.transform:get_position().z + 50)
      state.waiting = false
    end
  end
}

grunt_update_table["vulnerable_action"] = {
  update = function(self, e)
    --empty
  end
}

grunt_update_table["investigate"] = {
  startle_time = 2,
  update = function(self, e)
    local ai = e:get_entity().AIComponent

    -- Wait until the startle animation is over, then go to the search spot
    if ai.state_time > self.startle_time and ai.actions_taken == 0 then
      ai.actions_taken = 1
      ai.funcs:go_to(ai.search_location.x, ai.search_location.y)
    end
  end
}

grunt_update_table["flee"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    if ai.has_target then
      shared.back_away_from_target(e)
    end
  end
}

grunt_update_table["battle_stance"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local tc = e:get_entity().TransformComponent
    local ccc = e:get_entity().CharacterControllerComponent
    local state = ai.lua_state
    --ccc.contents:set_turning_speed(5)
    --angle = shared.get_target_angle(e)

    --if state.angle_location ~= 0 and angle < 45 then
      --state.angle_location = 0
      --ccc.contents:set_turning_speed(1.0)
    --elseif state.angle_location ~= 3 and angle > 135 then
      --state.angle_location = 3
      --ccc.contents:set_turning_speed(1.4)
    --elseif state.angle_location ~= 2 and angle > 90 then
      --state.angle_location = 2
      --ccc.contents:set_turning_speed(1.2)
    --elseif state.angle_location ~= 1 and angle > 45 then
      --state.angle_location = 1
      --ccc.contents:set_turning_speed(1.1)
    --end

    local target_pos = shared.get_target_pos(e)
    ai.funcs:go_to(target_pos.x, target_pos.y, target_pos.z)
    e:turn_towards(target_pos.x, target_pos.y, 0)
  end
}

grunt_update_table["return_home"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    ai.funcs:go_to(ai.spawn_point.x, ai.spawn_point.y, ai.spawn_point.z)
  end
}

grunt_update_table["raise_shield"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local target_pos = shared.get_target_pos(e)

    if ai.has_target then
      ai.funcs:go_to(target_pos.x, target_pos.y, target_pos.z)
    end
    e:turn_towards(target_pos.x, target_pos.y, 0)
  end
}

grunt_update_table["sprint"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    --get the position of the target
    if (ai.has_target) then
      local target_pos = shared.get_target_pos(e)
      ai.funcs:go_to(target_pos.x, target_pos.y, target_pos.z)
      e:turn_towards(target_pos.x, target_pos.y, 0)
    end
  end
}

grunt_update_table["shield_sword_poke"] = {
  wind_up_time = 1.0,
  freeze_time = 0.5,
  swing_time = 0.5,
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local target_pos = shared.get_target_pos(e)

    if ai.state_time < self.wind_up_time then
      e:turn_towards(target_pos.x, target_pos.y, 0)
    elseif ai.actions_taken == 0 and ai.state_time > (self.wind_up_time + self.freeze_time) then
      ai.actions_taken = 1
      --ai.misc = tostring(self.swing_time)
      --ai.start_attack_this_frame = true
      func.melee_attack(e)
    elseif ai.actions_taken == 1 and ai.state_time > (self.wind_up_time + self.freeze_time + self.swing_time - 0.1) then
      ai.actions_taken = 2
      --ai.misc = tostring(self.swing_time)
      --ai.start_attack_this_frame = true
      func.melee_attack(e)
      local state = ai.lua_state
      state.attack_finished = true
    end
  end
}

grunt_update_table["melee_attack"] = {
  wind_up_time = 0.0,
  freeze_time = 0.2,
  swing_time = 1.0,
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local target_pos = shared.get_target_pos(e)

    if ai.state_time < self.wind_up_time then
      e:turn_towards(target_pos.x, target_pos.y, 0)
    elseif ai.actions_taken == 0 and ai.state_time > (self.wind_up_time + self.freeze_time) then
      ai.actions_taken = 1
      --ai.misc = tostring(self.swing_time)
      --ai.start_attack_this_frame = true
      func.melee_attack(e, self.swing_time)
    elseif ai.actions_taken == 1 and ai.state_time > (self.wind_up_time + self.freeze_time + self.swing_time - 0.1) then
      ai.actions_taken = 2
      --ai.misc = tostring(self.swing_time)
      --ai.start_attack_this_frame = true
      --func.melee_attack(e)
      local state = ai.lua_state
      state.attack_finished = true
    end
  end
}
