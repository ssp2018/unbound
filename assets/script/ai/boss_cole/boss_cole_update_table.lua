local func = require("ai/boss_cole/boss_cole_functionality").funcs
local const = require("ai/boss_cole/boss_cole_functionality").consts
local shared = require("ai/shared_functionality").funcs
local math = require("math")
local cpp = require("cpp")
local table = require("table")

local audio_wrapper = cpp.AudioScriptWrapper.new()

local hide_remaining_armors = function(e)
  local cole = e:get_entity()
  local aic = cole.AIComponent
  if cole.TransformComponent ~= nil and cole.COLEComponent ~= nil and cole.CharacterControllerComponent ~= nil then
    if aic.has_target and cole.COLEComponent.armor_left > 0 and cole.COLEComponent.armor_left ~= 56 then
      local entity_mgr = e:get_entity_manager()
      
      local mass = cpp.vec3_fill(0, 0, 0)

      local counter = 0
      for i , armors in ipairs(cole.COLEComponent.body) do
        if entity_mgr:is_valid(cole.COLEComponent.body[i]) then
          cpp.vec3_add(mass, entity_mgr:get_entity(cole.COLEComponent.body[i]).TransformComponent.transform:get_position())
          counter = counter + 1
        end
      end
      if counter ~= 0 then
        cpp.vec3_scalar_multiplication(mass, 1 / counter)
      end
      -- create a vec from the target to cole
      local target_pos = entity_mgr:get_entity(aic.target).TransformComponent.transform:get_position()
      local vec_from_target = cole.TransformComponent.transform:get_position()
      cpp.vec3_subtract(vec_from_target, target_pos)
      vec_from_target = cpp.normalize_vec3(vec_from_target)

      local vec_to_center_of_mass = cpp.vec3_fill(mass.x, mass.y, mass.z)
      vec_to_center_of_mass = cpp.normalize_vec3(cpp.vec3_subtract(vec_to_center_of_mass, cole.TransformComponent.transform:get_position()))

      local default_quat = cpp.quat.new()
      default_quat.w = 1
      default_quat = cpp.normalize_quat(default_quat)

      local rotation_quat = cpp.normalize_quat(cpp.rotate_vec_to_vec(vec_to_center_of_mass, vec_from_target))
      
      rotation_quat = cpp.normalize_quat(cpp.quat_slerp(default_quat, rotation_quat, cole.COLEComponent.rotation_percentage))

      rotation_quat = cpp.mul_quats(rotation_quat, cole.TransformComponent.transform:get_rotation())
      cole.TransformComponent.transform:set_rotation(cpp.normalize_quat(rotation_quat), e:get_frame_context())

      local model_matrix = cole.TransformComponent.transform:get_model_matrix()
      local up_vec = cpp.mat4_vec4_mul(model_matrix, cpp.vec4_fill(0, 0, 1, 0))

      cole.CharacterControllerComponent.contents:set_up(cpp.vec3_fill(up_vec.x, up_vec.y, up_vec.z))
    end
  end
end

boss_cole_update_table = {}

boss_cole_update_table["idle"] = {
  update = function(self, e)
    --empty
    local colec = e:get_entity().COLEComponent
    if(colec.armor_left < 56) then
      func.spawn_body(e)
    end
  end
}

boss_cole_update_table["battle_stance"] = {
  update = function(self, e)
    hide_remaining_armors(e)
  end
}

boss_cole_update_table["shoot_lazer"] = {
  update = function(self, e)
    local entity = e:get_entity()
    local entity_mgr = e:get_entity_manager();
    local aic = entity.AIComponent;

    hide_remaining_armors(e)
    
    -- Charging beam
    if aic.state_time < const.stop_charge_beam then
      -- TODO: Create beam charging particles
      --if aic.has_target then
        -- TODO: turning does not work
        --e:turn_towards_target();
      --end

    -- Shoot non damaging beam
    elseif aic.state_time > const.stop_charge_beam and aic.state_time < const.stop_non_damaging_beam then
      if aic.actions_taken == 0 then
        func.spawn_non_damaging_lazer(e)
        aic.actions_taken = 1
      else
        if entity.COLEComponent ~= nil then
          func.update_safe_lazer_size(e)
        end
      end

    -- Shoot damaging lazer
    elseif aic.state_time > const.stop_shooting_non_damaging_beam and aic.state_time < const.stop_damaging_lazer then
      if aic.actions_taken == 1 then
        aic.actions_taken = 2
        func.spawn_damaging_lazer(e)
      elseif aic.actions_taken == 2 then
        func.update_dangerous_lazer_size(e)
      end

    -- Shrink damagin lazer
    elseif aic.state_time > const.stop_damaging_lazer and aic.state_time < const.stop_shrinking_damaging_lazer then
      func.dangerous_lazer_shrink(e)
    end
  end
}

boss_cole_update_table["teleport"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    --print(ai.actions_taken)
    if(ai.actions_taken == 0) then

       local entity_mgr = e:get_entity_manager()

       -- Teleport sound

       if e:get_entity().TransformComponent ~= nil then
        local teleport_entity = entity_mgr:create_entity()

        local pc = cpp.ParticlesComponent.new()
        pc.spawn_shape = 1
        pc.size_mode = 1
        
        pc.max_particles = 1
        pc.spawn_per_second = 60

        pc.min_spawn_pos = cpp.vec3_fill(0, 0, 0)
        pc.max_spawn_pos = cpp.vec3_fill(0, 0, 0)
        pc.min_start_color = cpp.vec4_fill(0.8, 0.14, 0.7, 1)
        pc.max_start_color = cpp.vec4_fill(0.8, 0.14, 0.7, 1)
        pc.min_end_color = cpp.vec4_fill(0.8, 0.14, 0.7, 1)
        pc.max_end_color = cpp.vec4_fill(0.8, 0.14, 0.7, 1)

        pc.min_start_size = 6
        pc.max_start_size = 0
        pc.min_end_size = 0
        pc.max_end_size = 0

        teleport_entity.ParticlesComponent = pc
     
        local tc = cpp.TransformComponent.new()
        tc.transform:set_position(e:get_entity().TransformComponent.transform:get_position(), e:get_frame_context())
        teleport_entity.TransformComponent = tc

        local ltc = cpp.LifetimeComponent.new()
        ltc.time_left = 1.2
        teleport_entity.LifetimeComponent = ltc

        audio_wrapper:attach_audio_component(teleport_entity)

        local event_mgr = e:get_event_manager()
        local teleport_sound = cpp.PlaySoundEvent.new()
        teleport_sound.id = teleport_entity.get_handle
        teleport_sound.settings.rolloff_factor = 0.01
        teleport_sound.settings.gain = 0.7
        local path = "assets/audio/effects/cole/teleport/post/t2.ogg"
        teleport_sound.buffer_path = cpp.FilePath.new(path)
        event_mgr:send_event(teleport_sound)
      end

      local entities = entity_mgr:get_entities({"NameComponent", "TransformComponent"})
      -- Find the telepoints placed in the scene
      local tele_points = {}
      for i, entity in ipairs(entities) do
        local nc = entity.NameComponent
        if(nc.name == "cole_teleport_point") then
          local tc = entity.TransformComponent
          tele_points[#tele_points+1] = tc.transform:get_position()
        end
      end
      local tc = e:get_entity().TransformComponent
      --print(tele_points)
      if(#tele_points ~= 0) then
        local choice
        --print(ai.has_target)
        if(ai.has_target) then
          local dists = {}
          local target_pos = shared.get_target_pos(e)
          -- Calculate the distances
          for i, point in ipairs(tele_points) do
            dists[i] = cpp.distance_vec3(point, target_pos)
          end
          local remove_amount = math.floor(#tele_points / 2)
          local temp = {}
          for i=1,#dists do
              temp[i] = i
          end
          table.sort(temp,function(a,b)
              return dists[a] < dists[b]
          end)
          sorted_indices = temp
          choice = math.floor(math.random(remove_amount + 1, #sorted_indices))
          choice = sorted_indices[choice]
          --print("Calculated: " .. choice)
        else
          choice = math.floor(math.random(1, #tele_points))
          --print("Random: " .. choice)
        end
        tc.transform:set_position(tele_points[choice], e:get_frame_context())
      end
      ai.actions_taken = ai.actions_taken + 1

      if e:get_entity().SpawnComponent ~= nil and e:get_entity().COLEComponent ~= nil then
        local cole_entity = e:get_entity()
        local cole_pos = cole_entity.TransformComponent.transform:get_position()
        cole_entity.SpawnComponent.max_spawns = cole_entity.SpawnComponent.max_spawns + 1
        cole_entity.COLEComponent.laser_rate_of_fire = cole_entity.COLEComponent.laser_rate_of_fire - 0.5
        for index, skull in ipairs(cole_entity.SpawnComponent.spawned_enteties) do
          if entity_mgr:is_valid(skull) and entity_mgr:get_entity(skull).AIComponent ~= nil then
            entity_mgr:get_entity(skull).AIComponent.spawn_point = cpp.vec3_fill(cole_pos.x, cole_pos.y, cole_pos.z)
          end
        end
      end
    end
  end
}
