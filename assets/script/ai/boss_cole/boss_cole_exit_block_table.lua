local func = require("ai/boss_cole/boss_cole_functionality").funcs
local const = require("ai/boss_cole/boss_cole_functionality").consts
local cpp = require("cpp")

boss_cole_exit_block_table = {}

boss_cole_exit_block_table["idle"] = {
  update = function(self, e)
    local event_mgr = e:get_event_manager()
    local aggro_sound = cpp.PlaySoundEvent.new()
    aggro_sound.id = e:get_entity().get_handle
    aggro_sound.settings.rolloff_factor = 0.01
    local path = "assets/audio/effects/cole/cole/post/aggro.ogg"
    aggro_sound.buffer_path = cpp.FilePath.new(path)
    event_mgr:send_event(aggro_sound)
  end
}

boss_cole_exit_block_table["battle_stance"] = {
  update = function(self, e)
  end
}

boss_cole_exit_block_table["shoot_lazer"] = {
  update = function(self, e)
  end
}

boss_cole_exit_block_table["teleport"] = {
  update = function(self, e)
  end
}
