local math = require("math")
local cpp = require("cpp")
local shared = require("ai/shared_functionality").funcs

consts = {
    stop_charge_beam = 1,
    stop_non_damaging_beam = 1 + 0.7,
    stop_shooting_non_damaging_beam = 1.7 + 0.3,
    stop_damaging_lazer = 2.0 + 0.2,
    stop_shrinking_damaging_lazer = 2.2 + 0.6,

    beam_uniform_scale = 10,
    beam_max_length = 100,
    beam_acceleration = 10,
    beam_thickness_acceleration = 5, --2
    beam_max_thiccness = 10
}
local audio_wrapper = cpp.AudioScriptWrapper.new()

size_function = function(scale, start_val, end_val, x)
    return scale * (2 * normalize_length(start_val, end_val, x) - 1) ^ 2
end

normalize_length = function(start_val, end_val, x)
    return (x - start_val) / (end_val - start_val)
end

funcs = {
    print_func = function(string)
        print(string)
    end,
    spawn_body = function(e)
        local entity = e:get_entity()
        local entity_mgr = e:get_entity_manager()
        if entity.COLEComponent ~= nil and entity.ModelComponent ~= nil then
            local index = 1
            for x = 1, 4 do
                for y = 1, 4 do
                    for z = 1, 4 do
                        if x < 2 or x > 3 or y < 2 or y > 3 or z < 2 or z > 3 then
                            if not entity_mgr:is_valid(entity.COLEComponent.body[index]) then
                                local health_component = cpp.HealthComponent.new()
                                health_component.health_points = 3
                                health_component.max_health_points = 3
                                health_component.death_callback =
                                    cpp.Object.new("assets/script/gmp/death_callbacks.lua", "armor_death_callback")

                                local faction_component = cpp.FactionComponent.new()
                                faction_component.type = 3
                                faction_component.sound_category = 4

                                local transform_component = cpp.TransformComponent.new()
                                transform_component.transform:set_scale(1.5, e:get_frame_context())

                                local static_component = cpp.StaticComponent.new()
                                static_component.contents:set_collission_shape(
                                    e:get_frame_context(),
                                    e:get_box_shared_pointer(1, 1, 1)
                                )

                                local model_component = cpp.ModelComponent.new()
                                model_component.asset = cpp.ModelAsset.new(cpp.FilePath.new("assets/model/armor.fbx"))

                                local attach_to_joint_component = cpp.AttachToJointComponent.new()
                                attach_to_joint_component.entity_handle = entity.get_handle
                                attach_to_joint_component.joint_name = "j_" .. tostring(x) .. "_" .. tostring(y) .. "_" .. tostring(z)

                                local box = entity_mgr:create_entity()
                                box.HealthComponent = health_component
                                box.FactionComponent = faction_component
                                box.TransformComponent = transform_component
                                box.ModelComponent = model_component
                                box.StaticComponent = static_component
                                box.AttachToJointComponent = attach_to_joint_component
                                entity.COLEComponent.body[index] = box.get_handle
                            end
                            index = index + 1
                        end
                    end
                end
            end
            entity.COLEComponent.armor_left = 56
            entity.COLEComponent.last_tele_armor_amount = 56
            entity.COLEComponent.woundable = false
            if entity.ShieldEffectComponent == nil then
                entity.ShieldEffectComponent = cpp.ShieldEffectComponent.new()
            end
        else
            print("COLEComponent not attached to COLE, and thats not very good...")
        end
    end,
    highlight_laser_beam_target = function(e, target_entity)
        local entity = e:get_entity()
        if entity.is_valid and target_entity.is_valid then
        else
            print("entity or target is invalid")
        end
    end,
    spawn_non_damaging_lazer = function(e)
        local entity = e:get_entity()
        local entity_mgr = e:get_entity_manager()
        local aic = entity.AIComponent
        if (entity_mgr:is_valid(aic.target)) then
            local colec = entity.COLEComponent
            local beam = entity_mgr:create_entity()
            colec.death_beam = beam.get_handle

            beam.TransformComponent = cpp.TransformComponent.new()

            -- rotate the beam to aim towards the player
            local beam_forward = cpp.vec3_fill(0, 0, 1)
            colec.beam_dir = entity_mgr:get_entity(aic.target).TransformComponent.transform:get_position()

            -- aim prediction starts here
            local target_vel = cpp.vec3_fill(0, 0, 0)
            local target_entity = entity_mgr:get_entity(aic.target)
            if target_entity.CharacterControllerComponent ~= nil then
                target_vel = target_entity.CharacterControllerComponent.contents:get_velocity()
            end
            if target_entity.DynamicComponent ~= nil then
                target_vel = DynamicComponent.CharacterControllerComponent.contents:get_velocity()
            end
            
            -- extend the velocity vector by [-0.2, 0.2]
            local tmp_vec = cpp.vec3_fill(target_vel.x, target_vel.y, target_vel.z)
            cpp.vec3_add(target_vel, cpp.vec3_scalar_multiplication(tmp_vec, math.random() * 0.1 * math.random(-1, 1)))
            
            cpp.vec3_add(colec.beam_dir, target_vel)
            -- aim prediction ends here

            colec.beam_dir =
                cpp.normalize_vec3(
                cpp.vec3_subtract(colec.beam_dir, entity.TransformComponent.transform:get_position())
            )
            colec.beam_rotation_axis = cpp.normalize_vec3(cpp.cross(beam_forward, colec.beam_dir))
            colec.angle = cpp.angle_vec3(beam_forward, colec.beam_dir)
            beam.TransformComponent.transform:set_rotation_vec(
                colec.beam_rotation_axis,
                colec.angle,
                e:get_frame_context()
            )
            beam.TransformComponent.transform:set_scale_vector(
                cpp.vec3_fill(
                    consts.beam_uniform_scale,
                    consts.beam_uniform_scale,
                    consts.beam_uniform_scale * consts.beam_max_length
                ),
                e:get_frame_context()
            )
            local beam_position_offset = cpp.vec3_fill(colec.beam_dir.x, colec.beam_dir.y, colec.beam_dir.z)
            cpp.vec3_scalar_multiplication(beam_position_offset, consts.beam_uniform_scale * consts.beam_max_length / 2)
            cpp.vec3_add(beam_position_offset, entity.TransformComponent.transform:get_position())
            beam.TransformComponent.transform:set_position(beam_position_offset, e:get_frame_context())

            beam.StaticComponent = cpp.StaticComponent.new()
            beam.StaticComponent.contents:set_collission_shape(e:get_frame_context(), cpp.make_shared_CCS(0.01, 1)) -- radius of the ending half spheres, height
            beam.StaticComponent.contents:set_is_trigger(true)

            beam.ModelComponent = cpp.ModelComponent.new()
            beam.ModelComponent.asset = cpp.ModelAsset.new(cpp.FilePath.new("assets/model/lazer.fbx"))

            beam.LifetimeComponent = cpp.LifetimeComponent.new()
            beam.LifetimeComponent.time_left = consts.stop_non_damaging_beam - aic.state_time

            local event_mgr = e:get_event_manager();
            local lazer_sound = cpp.PlaySoundEvent.new();
            lazer_sound.id = e:get_entity().get_handle;
            lazer_sound.settings.rolloff_factor = 0.01
            local path = "assets/audio/effects/cole/laser/post/l3_1_start.ogg"
            lazer_sound.buffer_path = cpp.FilePath.new(path);
            event_mgr:send_event(lazer_sound);
        end
    end,
    update_safe_lazer_size = function(e)
        local entity = e:get_entity()
        local entity_mgr = e:get_entity_manager()
        local colec = entity.COLEComponent
        if (entity_mgr:is_valid(colec.death_beam)) then
            local size =
                size_function(
                0.5,
                consts.stop_charge_beam,
                consts.stop_non_damaging_beam,
                entity.AIComponent.state_time
            )
            local tc = entity_mgr:get_entity(entity.COLEComponent.death_beam).TransformComponent
            tc.transform:set_scale_vector(
                cpp.vec3_fill(
                    consts.beam_uniform_scale * (1 - size),
                    consts.beam_uniform_scale * (1 - size),
                    consts.beam_uniform_scale * consts.beam_max_length
                ),
                e:get_frame_context()
            )
            local beam_position_offset = cpp.vec3_fill(colec.beam_dir.x, colec.beam_dir.y, colec.beam_dir.z)
            cpp.vec3_scalar_multiplication(beam_position_offset, consts.beam_uniform_scale * consts.beam_max_length / 2)
            cpp.vec3_add(beam_position_offset, entity.TransformComponent.transform:get_position())
            tc.transform:set_position(beam_position_offset, e:get_frame_context())
        end
    end,
    spawn_damaging_lazer = function(e)
        local entity = e:get_entity()
        local entity_mgr = e:get_entity_manager()

        local colec = entity.COLEComponent
        local beam = entity_mgr:create_entity()
        colec.death_beam = beam.get_handle

        -- Set the beam position to cole's position
        beam.TransformComponent = cpp.TransformComponent.new()
        local tc = beam.TransformComponent.transform
        tc:set_rotation_vec(entity.COLEComponent.beam_rotation_axis, entity.COLEComponent.angle, e:get_frame_context())
        tc:set_position(entity.TransformComponent.transform:get_position(), e:get_frame_context())
        -- Make sure that the beam dissappears when it should change state
        beam.LifetimeComponent = cpp.LifetimeComponent.new()
        beam.LifetimeComponent.time_left = consts.stop_shrinking_damaging_lazer - entity.AIComponent.state_time

        beam.StaticComponent = cpp.StaticComponent.new()
        beam.StaticComponent.contents:set_collission_shape(e:get_frame_context(), cpp.make_shared_CCS(0.05, 1)) -- radius of the ending half spheres, height
        beam.StaticComponent.contents:set_is_trigger(true)

        -- Give the beam the model
        beam.ModelComponent = cpp.ModelComponent.new()
        beam.ModelComponent.asset = cpp.ModelAsset.new(cpp.FilePath.new("assets/model/big_lazer.fbx"))

        beam.DamageComponent = cpp.DamageComponent.new()

        beam.FactionComponent = cpp.FactionComponent.new();
        beam.FactionComponent.type = 3;

        local event_mgr = e:get_event_manager();
        local lazer_sound = cpp.PlaySoundEvent.new();
        lazer_sound.id = e:get_entity().get_handle;
        lazer_sound.settings.rolloff_factor = 0.01
        local path = "assets/audio/effects/cole/laser/post/l3_1_end.ogg"
        lazer_sound.buffer_path = cpp.FilePath.new(path);
        event_mgr:send_event(lazer_sound);
    
    end,
    update_dangerous_lazer_size = function(e)
        local entity = e:get_entity()
        local entity_mgr = e:get_entity_manager()
        local aic = entity.AIComponent
        local colec = entity.COLEComponent

        if entity_mgr:is_valid(colec.death_beam) and aic.has_target then
            local tc = entity_mgr:get_entity(colec.death_beam).TransformComponent

            local percentage =
                normalize_length(consts.stop_shooting_non_damaging_beam, consts.stop_damaging_lazer, aic.state_time)
            local length = math.min(consts.beam_max_length, consts.beam_max_length * percentage)
            local thiccness = math.min(percentage * consts.beam_thickness_acceleration, consts.beam_max_thiccness)
            tc.transform:set_scale_vector(
                cpp.vec3_fill(
                    consts.beam_uniform_scale * thiccness,
                    consts.beam_uniform_scale * thiccness,
                    consts.beam_uniform_scale * length
                ),
                e:get_frame_context()
            )
            local beam_position_offset = cpp.vec3_fill(colec.beam_dir.x, colec.beam_dir.y, colec.beam_dir.z)
            cpp.vec3_scalar_multiplication(beam_position_offset, consts.beam_uniform_scale * length / 2)
            cpp.vec3_add(beam_position_offset, entity.TransformComponent.transform:get_position())
            tc.transform:set_position(beam_position_offset, e:get_frame_context())
        end
    end,
    dangerous_lazer_shrink = function(e)
        local entity = e:get_entity()
        local entity_mgr = e:get_entity_manager()
        local aic = entity.AIComponent
        local colec = entity.COLEComponent
        if entity_mgr:is_valid(entity.COLEComponent.death_beam) then
            local percentage =
                normalize_length(consts.stop_damaging_lazer, consts.stop_shrinking_damaging_lazer, aic.state_time)

            local tc = entity_mgr:get_entity(entity.COLEComponent.death_beam).TransformComponent
            tc.transform:set_scale_vector(
                cpp.vec3_fill(
                    consts.beam_uniform_scale * (1 - percentage) * consts.beam_max_thiccness,
                    consts.beam_uniform_scale * (1 - percentage) * consts.beam_max_thiccness,
                    consts.beam_uniform_scale * consts.beam_max_length
                ),
                e:get_frame_context()
            )
            local beam_position_offset = cpp.vec3_fill(colec.beam_dir.x, colec.beam_dir.y, colec.beam_dir.z)
            cpp.vec3_scalar_multiplication(beam_position_offset, consts.beam_uniform_scale * consts.beam_max_length / 2)
            cpp.vec3_add(beam_position_offset, entity.TransformComponent.transform:get_position())
            tc.transform:set_position(beam_position_offset, e:get_frame_context())
        end
    end
}
