local func = require("ai/boss_cole/boss_cole_functionality").funcs
local const = require("ai/boss_cole/boss_cole_functionality").consts
local cpp = require("cpp")

boss_cole_enter_block_table = {}

boss_cole_enter_block_table["idle"] = {
  update = function(self, e)
    func.spawn_body(e)
    local cole = e:get_entity()
    if cole.COLEComponent ~= nil then
      cole.COLEComponent.laser_rate_of_fire = 5
    end
    if cole.SpawnComponent ~= nil then
      cole.SpawnComponent.max_spawns = 0
    end
  end
}

boss_cole_enter_block_table["battle_stance"] = {
  update = function(self, e)
    local cole = e:get_entity()
    if cole.SpawnComponent ~= nil then
      cole.SpawnComponent.max_spawns = 3
    end
  end
}

boss_cole_enter_block_table["shoot_lazer"] = {
  update = function(self, e)
  end
}

boss_cole_enter_block_table["teleport"] = {
  update = function(self, e)
    local colec = e:get_entity().COLEComponent
    colec.last_tele_armor_amount = colec.armor_left
  end
}
