local func = require("ai/boss_cole/boss_cole_functionality").funcs
local const = require("ai/boss_cole/boss_cole_functionality").consts
local shared = require("ai/shared_functionality").funcs
local boss_cole_update_table = require("ai/boss_cole/boss_cole_update_table").boss_cole_update_table
local math = require("math")
local cpp = require("cpp")

boss_cole_transition_table = {}

boss_cole_transition_table["idle"] = {
  update = function(self, e)
    if shared.look_at_surroundings(e, e:get_entity()) then
      return "battle_stance"
    end
    return ""
  end
}

boss_cole_transition_table["battle_stance"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    local colec = e:get_entity().COLEComponent
    if not ai.has_target then
      return "idle"
    elseif ai.state_time > colec.laser_rate_of_fire then
      return "shoot_lazer"
    elseif colec.last_tele_armor_amount - colec.armor_left >= 15 then
      return "teleport"
    end
  end
}

boss_cole_transition_table["shoot_lazer"] = {
  update = function(self, e)
    if e:get_entity().AIComponent.state_time > const.stop_shrinking_damaging_lazer then
      return "battle_stance"
    end
  end
}

boss_cole_transition_table["teleport"] = {
  update = function(self, e)
    local ai = e:get_entity().AIComponent
    if ai.actions_taken >= 1 then
      return "battle_stance"
    end
  end
}
