local cpp = require("cpp")
local string = require("string")

function default_death_callback(e)
  local nc = e:get_entity().NameComponent
  if nc ~= nil then
    print("Entity " .. nc.name .. " died")
  end

  e:get_entity().DestroyedComponent = cpp.DestroyedComponent.new()
end

function player_death_callback(e)
  e:get_event_manager():send_event(cpp.PlayerZeroHealth.new())
end

function grunt_death_callback(e)
  local tc = e:get_entity().TransformComponent
  local p = tc.transform:get_position()
  if p.z > 55 then
    local spawn_event = cpp.SpawnHealthOrb.new()
    spawn_event.position = cpp.vec3_fill(p.x, p.y, p.z + 1.1)
    spawn_event.delay = 1
    e:get_event_manager():send_event(spawn_event)
  end

  ai_death_callback(e)
end

function ai_death_callback(e)
  cpp.ai_death_callback(e:get_entity().get_handle, e:get_event_manager(), e:get_entity_manager())

  local entity = e:get_entity()
  local life = cpp.LifetimeComponent.new()
  life.time_left = 5
  entity.LifetimeComponent = life

  entity.HealthComponent = nil
end

function skull_death_callback(e)
  local ahc = e:get_entity().AttachedHandlesComponent
  for i = 1, 5 do
    local handle = ahc.handles[i]
    if e:get_entity_manager():is_valid(handle) then
      local entity = e:get_entity_manager().get_entity(e:get_entity_manager(), handle)
      entity.DestroyedComponent = cpp.DestroyedComponent.new()
    end
  end

  cpp.skull_death_callback(
    e,
    e:get_entity().get_handle,
    e:get_event_manager(),
    e:get_entity_manager(),
    e:get_frame_context()
  ) --this is retarded...
  e:get_event_manager():send_event(cpp.KilledSkullEvent.new())
end

function golem_death_callback(e)
  if e:get_entity().is_valid then
    local health_component = e:get_entity().HealthComponent
    local killing_arrow_handle = health_component.last_hit_by

    cpp.ai_death_callback(e:get_entity().get_handle, e:get_event_manager(), e:get_entity_manager())
    --WARNING health_component will be invalid after ai_death_callback

    e:get_entity().JointLookAtComponent = nil

    if e:get_entity_manager():is_valid(killing_arrow_handle) then
      local arrow = e:get_entity_manager():get_entity(killing_arrow_handle)

      if arrow.is_valid then
        local ev = cpp.KilledGolemEvent.new()
        ev.golem_handle = e:get_entity().get_handle
        ev.killing_arrow_handle = killing_arrow_handle
        ev.arrow_frame_num = arrow.FrameTagComponent.frame_num
        ev.arrow_final_point = arrow.TransformComponent.transform:get_position()
        e:get_event_manager():send_event(ev)
      end
    end
		local event_mgr = e:get_event_manager()
		local ev = cpp.AnimationEvent.new()
		ev.animation_name = ""
		ev.handle = e:get_entity().get_handle
		ev.blend_time = 0.0
		event_mgr:send_event(ev)
  end
end

function armor_death_callback(e, armor_entity, event_mgr, entity_mgr)
  local entities = entity_mgr:get_entities({"NameComponent", "COLEComponent"})
  local curr_armor_handle = armor_entity.get_handle
  -- Make sure that the armor belong to the correct cole instance
  for i, curr_ent in ipairs(entities) do
    if curr_ent.NameComponent.name == "COLE" then
      for j, armors in ipairs(curr_ent.COLEComponent.body) do
        if (curr_armor_handle == curr_ent.COLEComponent.body[j]) then
          if curr_ent.COLEComponent.armor_left > 0 then
            curr_ent.COLEComponent.armor_left = curr_ent.COLEComponent.armor_left - 1
          end
          if curr_ent.COLEComponent.armor_left == 0 then
            curr_ent.ShieldEffectComponent = nil
            curr_ent.COLEComponent.woundable = true
          end
        end
      end
    end
  end
  armor_entity.DestroyedComponent = cpp.DestroyedComponent.new()
end

function cole_box_death_callback(e, cole_entity, event_mgr, entity_mgr)
  for i, armors in ipairs(cole_entity.COLEComponent.body) do
    if entity_mgr:is_valid(cole_entity.COLEComponent.body[i]) then
      local armor_entity = entity_mgr:get_entity(cole_entity.COLEComponent.body[i])
      armor_entity.DestroyedComponent = cpp.DestroyedComponent.new()
    end
  end
  cole_entity.DestroyedComponent = cpp.DestroyedComponent.new()
end

function generic_branch_death_callback(e)
  local entity = e:get_entity()
  local entity_mgr = e:get_entity_manager()

  entity.DestroyedComponent = cpp.DestroyedComponent.new()

  -- Stump
  -- Transform
  local etc = entity.TransformComponent
  local stump = entity_mgr:create_entity()
  local stc = cpp.TransformComponent.new()
  stc.transform:set_model_matrix(etc.transform:get_model_matrix(), e:get_frame_context())
  stump.TransformComponent = stc
  -- Model
  local etc = entity.ModelComponent;
  local smc = cpp.ModelComponent.new();
  local path = string.sub(etc.asset:get_path():get_string(),0,-5) .. "_stump.fbx";
  smc.asset = cpp.ModelAsset.new(cpp.FilePath.new(path));
  stump.ModelComponent = smc;
  -- Collision
  local sc = cpp.StaticComponent.new();
  sc.contents = cpp.load_static_component_contents(e:get_frame_context(), "Triangle", cpp.FilePath.new(path));
  --stump.StaticComponent = sc;
end
