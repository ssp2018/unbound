local cpp = require("cpp")

function weak_spot_callback(collision_event, trigger_name, colliding_entity, target_entity, event_mgr)
  sound_event = cpp.PlaySoundEvent.new();

  sound_event.id = target_entity;
  sound_event.buffer_path = cpp.FilePath.new("assets/audio/effects/bow_and_arrow/hit/post/hit_weak_spot.ogg");
  sound_event.pitch = 1.0;
  sound_event.gain = 1.0;
  sound_event.loop = false;

  event_mgr:send_event(sound_event);

  weak_spot_event = cpp.WeakSpotHit.new();

  weak_spot_event.target_entity = target_entity;
  weak_spot_event.hit_point = collision_event.hit_point;
  weak_spot_event.hit_point_normal = collision_event.hit_point_normal;

  event_mgr:send_event(weak_spot_event);

  print(">>>>>>>> Weak Spot Hit: " .. trigger_name)
end

function pickup_effects(handle, entity_mgr, event_mgr)
  local entity = entity_mgr.get_entity(entity_mgr, handle);

  -- Particle burst
  local ppc = entity.ParticlesComponent;
  ppc.spawn_per_second = 350;
  ppc.spawn_duration_left = 0.05;
  ppc.min_life = 0.4;
  ppc.max_life = 0.6;
  ppc.min_start_size = 0.2;
  ppc.max_start_size = 0.3;
  ppc.min_spawn_pos = cpp.vec3_fill(0, 0, 0);
  ppc.max_spawn_pos = cpp.vec3_fill(0, 0, 0);
  ppc.min_start_velocity = cpp.vec3_fill(-15, -15, -15);
  ppc.max_start_velocity = cpp.vec3_fill(15, 15, 15);
  ppc.velocity_mode = 1;  -- Decelerate
  ppc.min_rotation_speed = -4;
  ppc.max_rotation_speed = 4;
  ppc.texture = cpp.TextureAsset.new(cpp.FilePath.new("assets/texture/explosion.png"))
  entity.ParticlesComponent = ppc;

  -- Play sound
  local sound_event = cpp.PlaySoundEvent.new();
  sound_event.id = handle;
  sound_event.buffer_path = cpp.FilePath.new("assets/audio/effects/pickup/post/pickup.ogg");
  sound_event.settings.gain = 0.7;
  sound_event.settings.loop = false;
  event_mgr:send_event(sound_event);
end

--
function destroy(handle, entity_mgr, event_mgr)
  local entity = entity_mgr.get_entity(entity_mgr, handle);
  -- Removing components
  entity.StaticComponent = nil;
  entity.ModelComponent = nil;
  entity.TriggerScriptComponent = nil;
  entity.PointLightComponent = nil;
  local puc = entity.PickupComponent;
  if puc.respawn_cooldown < 0 then
    local ltc = cpp.LifetimeComponent.new();
    ltc.time_left = 3;
    entity.LifetimeComponent = ltc;
    puc.time_to_respawn = -1;
  else
    puc.time_to_respawn = puc.respawn_cooldown;
  end
  entity = puc;
end

function pickup_note_callback(collision_event, trigger_name, colliding_entity, target_entity, event_mgr, entity_mgr)
  local player = entity_mgr.get_entity(entity_mgr, colliding_entity);
  if player.NameComponent ~= nil and player.NameComponent.name == "player" then
    -- Unique interaction
    event_mgr:send_event(cpp.TookHornNoteEvent.new());

    pickup_effects(target_entity, entity_mgr, event_mgr);
    destroy(target_entity, entity_mgr, event_mgr);
  end
end

function pickup_horn_callback(collision_event, trigger_name, colliding_entity, target_entity, event_mgr, entity_mgr)
  local player = entity_mgr.get_entity(entity_mgr, colliding_entity);
  if player.NameComponent ~= nil and player.NameComponent.name == "player" then
    -- Unique interaction
    event_mgr:send_event(cpp.TookHornEvent.new());

  	-- Play sound
  	local sound_event = cpp.PlaySoundEvent.new();
  	sound_event.id = target_entity;
  	sound_event.buffer_path = cpp.FilePath.new("assets/audio/effects/pickup/post/horn.ogg");
  	sound_event.settings.gain = 0.7;
  	sound_event.settings.loop = false;
  	event_mgr:send_event(sound_event);

    destroy(target_entity, entity_mgr, event_mgr);
  end
end

function pickup_explosive_arrows_callback(collision_event, trigger_name, colliding_entity, target_entity, event_mgr, entity_mgr)
  local player = entity_mgr.get_entity(entity_mgr, colliding_entity);
  if player.NameComponent ~= nil and player.NameComponent.name == "player" then
    -- Unique interaction
    event_mgr:send_event(cpp.AddExplosiveArrowsEvent.new());

    pickup_effects(target_entity, entity_mgr, event_mgr);
    destroy(target_entity, entity_mgr, event_mgr);
  end
end

function pickup_health_orb_callback(collision_event, trigger_name, colliding_entity, target_entity, event_mgr, entity_mgr)
  local player = entity_mgr.get_entity(entity_mgr, colliding_entity);
  if player.NameComponent ~= nil and player.NameComponent.name == "player" then
    -- Unique interaction
    player.HealthComponent.health_points = player.HealthComponent.health_points + 1;
    if player.HealthComponent.health_points > player.HealthComponent.max_health_points then
      player.HealthComponent.health_points = player.HealthComponent.max_health_points;
    end

    pickup_effects(target_entity, entity_mgr, event_mgr);
    destroy(target_entity, entity_mgr, event_mgr);
  end
end
