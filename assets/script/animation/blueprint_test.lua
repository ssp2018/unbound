-- robot_anm_testanimation fall
-- robot_anm_testanimation2 idle
-- robot_anm_testanimation3 run


local state_machine1 = {
  start = "move",
  states = {
    move = {
      blendspace = {
        inputs = {
          speed = function(animation, e, context)
            return e:get_char_ctrl_component().contents:get_speed()
          end
        },
        animations = {
          robot_anm_testanimation2 = 0,
          robot_anm_testanimation3 = 0.5, --walk
          robot_anm_testanimation3 = 1,
        }
      }
    },
    fall = {
      animation = "robot_anm_testanimation",
    }
  },

  transitions = {
    {
      in_state = "move",
      out_state = "fall",
      predicate = function(animation, e, context)
        return not e:get_char_ctrl_component().contents:on_ground()
      end,
      bidirectional = true
    }
  }
}

local animation_graph = {
  state_machine1 = {
    type = "state_machine",
    node = state_machine1,
  },

  blend = {
    type = "blend",
    inputs = {
      {
        input = "jump",
        weight = 1,
      },
      {
        input = "state_machine1",
        weight = 1,
      }
    }
  },

  jump = {
    type = "state",
    animation = "robot_anm_testanimation2",
    event_inputs = {
      "jump_animation"
    },
  },

  outputs = {
    "blend"
  }
}

local event_graph = {
  jump = {
    type = "event",
    processor = function(animation, e, context, data)
      
    end
  },

  jump_animation = {
    type = "animation_event",
    inputs = {"jump"}

  }
}

animation_blueprint1 = {
  event_graph = event_graph,
  animation_graph = animation_graph
}