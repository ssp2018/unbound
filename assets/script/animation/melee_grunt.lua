local animation_graph = {
  state_machine1 = {
    type = "state_machine",
    node = {
      start = "idle",
      states = {
        idle = {
          animation = "grunt_anm_idle",
        },
        run = {
          animation = "grunt_anm_run",
        },
        attack = {
          animation = "grunt_anm_attack",
        },
        dead = {
          animation = "grunt_anm_death",
        }
      },

      transitions = {
        {
          in_state = "idle",
          out_state = "run",
          predicate = function(animation, e, context)
            ccc = e:get_char_ctrl_component()
            if ccc ~= nil then
              return ccc.contents:get_speed() > 0.5
            end
          end,
          bidirectional = true
        },
        {
          in_state = "idle",
          out_state = "attack",
          predicate = function(animation, e, context)
            ai = e:get_ai_component()
            if ai ~= nil and (ai.state_name == "melee_attack" or ai.state_name == "shield_sword_poke") then
              return true
            end
          end,
          bidirectional = true
        },
        {
          in_state = "run",
          out_state = "attack",
          predicate = function(animation, e, context)
            ai = e:get_ai_component()
            if ai ~= nil and (ai.state_name == "melee_attack" or ai.state_name == "shield_sword_poke") then
              return true
            end
          end,
          bidirectional = true
        },
        {
          in_state = "idle",
          out_state = "dead",
          predicate = function(animation, e, context)
            return not e:get_ai_component()
          end,
          bidirectional = false
        },
        {
          in_state = "run",
          out_state = "dead",
          predicate = function(animation, e, context)
            return not e:get_ai_component()
          end,
          bidirectional = false
        },
      }
    }
  },

  outputs = {
    "state_machine1"
  }
}

local event_graph = {
}

animation_blueprint = {
  event_graph = event_graph,
  animation_graph = animation_graph
}