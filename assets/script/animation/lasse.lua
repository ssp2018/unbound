local animation_graph = {
  state_machine1 = {
    type = "state_machine",
    node = {
      start = "run",
      states = {
        idle = {
          animation = "lasse_anm_idle",
        },
		run = {
          animation = "lasse_anm_run",
        },
		jump = {
          animation = "lasse_anm_jump",
        },
      },

      transitions = {
        {
          in_state = "run",
          out_state = "run",
          predicate = function(animation, e, context)
            return true
          end,
          bidirectional = true
        },
      }
    }
  },

  outputs = {
    "state_machine1"
  }
}

local event_graph = {
}

animation_blueprint = {
  event_graph = event_graph,
  animation_graph = animation_graph
}