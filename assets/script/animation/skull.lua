local animation_graph = {
  state_machine1 = {
    type = "state_machine",
    node = {
      start = "idle",
      states = {
        idle = {
          animation = "skull_anm_idle"
        },
        aggro = {
          animation = "skull_anm_aggro"
        }
      },
      transitions = {
        {
          in_state = "idle",
          out_state = "aggro",
          predicate = function(animation, e, context)
            if not e:get_ai_component() then
              return false
            end
            return e:get_ai_component().state_name == "pursue"
            end,
          bidirectional = true
        }
      }
    }
  },
  outputs = {
    "state_machine1"
  }
}

local event_graph = {}

animation_blueprint = {
  event_graph = event_graph,
  animation_graph = animation_graph
}