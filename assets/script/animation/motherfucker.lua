local animation_graph = {
  state_machine1 = {
    type = "state_machine",
    node = {
      start = "idle",
      states = {
        idle = {
          animation = "motherfucker_anm_1",
        },
      },

      transitions = {
        {
          in_state = "idle",
          out_state = "idle",
          predicate = function(animation, e, context)
            return true
          end,
          bidirectional = true
        },
      }
    }
  },

  outputs = {
    "state_machine1"
  }
}

local event_graph = {
}

animation_blueprint = {
  event_graph = event_graph,
  animation_graph = animation_graph
}