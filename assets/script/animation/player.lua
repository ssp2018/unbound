local animation_graph = {
  state_machine1 = {
    type = "state_machine",
    node = {
      start = "idle",
      states = {
        idle = {
          animation = "player_mesh_anm_idle",
        },
        run = {
          animation = "player_mesh_anm_run",
        },
        jump = {
          animation = "player_mesh_anm_jump",
        },
        attack = {
          animation = "player_mesh_anm_attack",
        },
        swing = {
          animation = "player_mesh_anm_swing",
        },
        dash = {
          animation = "player_mesh_anm_dash",
        },
        death = {
          animation = "player_mesh_anm_death",
        };

      },

      transitions = {
        {
          in_state = "idle",
          out_state = "run",
          predicate = function(animation, e, context)
            return e:get_char_ctrl_component().contents:get_speed() > 0.5 and e:get_char_ctrl_component().contents:on_ground() and not e:get_player_component().is_swinging
          end,
          bidirectional = true
        },
        {
          in_state = "idle",
          out_state = "jump",
          predicate = function(animation, e, context)
            return not e:get_char_ctrl_component().contents:on_ground() and not e:get_player_component().is_swinging
          end,
          bidirectional = true
        },
        {
          in_state = "run",
          out_state = "jump",
          predicate = function(animation, e, context)
            return not e:get_char_ctrl_component().contents:on_ground() and not e:get_player_component().is_swinging
          end,
          bidirectional = true
        },
        {
          in_state = "idle",
          out_state = "attack",
          predicate = function(animation, e, context)
            return e:get_player_component().is_attacking
          end,
          bidirectional = true
        },
        {
          in_state = "jump",
          out_state = "attack",
          predicate = function(animation, e, context)
            return e:get_player_component().is_attacking
          end,
          bidirectional = true
        },
        {
          in_state = "run",
          out_state = "attack",
          predicate = function(animation, e, context)
            return e:get_player_component().is_attacking
          end,
          bidirectional = true
        },
        {
          in_state = "idle",
          out_state = "swing",
          predicate = function(animation, e, context)
            return e:get_player_component().is_swinging
          end,
          bidirectional = true
        },
        {
          in_state = "run",
          out_state = "swing",
          predicate = function(animation, e, context)
            return e:get_player_component().is_swinging
          end,
          bidirectional = true
        },
        {
          in_state = "jump",
          out_state = "swing",
          predicate = function(animation, e, context)
            return not e:get_char_ctrl_component().contents:on_ground() and e:get_player_component().is_swinging
          end,
          bidirectional = true
        },
        {
          in_state = "idle",
          out_state = "dash",
          predicate = function(animation, e, context)
            return e:get_char_ctrl_component().contents:on_ground() and e:get_player_component().is_dashing
          end,
          bidirectional = true
        },
        {
          in_state = "run",
          out_state = "dash",
          predicate = function(animation, e, context)
            return e:get_char_ctrl_component().contents:on_ground() and e:get_player_component().is_dashing
          end,
          bidirectional = true
        },
        {
          in_state = "idle",
          out_state = "death",
          predicate = function(animation, e, context)
            return e:get_player_component().is_dead
          end,
          bidirectional = false
        },
        {
          in_state = "run",
          out_state = "death",
          predicate = function(animation, e, context)
            return e:get_player_component().is_dead
          end,
          bidirectional = false
        },
        {
          in_state = "jump",
          out_state = "death",
          predicate = function(animation, e, context)
            return e:get_player_component().is_dead
          end,
          bidirectional = false
        },
        {
          in_state = "attack",
          out_state = "death",
          predicate = function(animation, e, context)
            return e:get_player_component().is_dead
          end,
          bidirectional = false
        },
        {
          in_state = "dash",
          out_state = "death",
          predicate = function(animation, e, context)
            return e:get_player_component().is_dead
          end,
          bidirectional = false
        },
      }
    }
  },

  outputs = {
    "state_machine1"
  }
}

local event_graph = {
}

animation_blueprint = {
  event_graph = event_graph,
  animation_graph = animation_graph
}