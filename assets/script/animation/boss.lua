local player_animation = require("animation/player").animation_blueprint.animation_graph


local animation_graph = {
  state_machine1 = {
    type = "state_machine",
    node = {
      start = "idle",
      states = {
        idle = {
          animation = "boss_anm_idle",
        },
        -- run = {
        --   animation = "boss_anm_walk",
        -- },
        battle_stance = {
          animation = "boss_anm_walk",
        },
        dead = {
          animation = "boss_anm_death",
        },
      },

      transitions = {
        -- {
        --   in_state = "idle",
        --   out_state = "run",
        --   predicate = function(animation, e, context)
        --   if not e:get_char_ctrl_component() then
        --     return false
        --   end
        --   return e:get_char_ctrl_component().contents:get_speed() > 0.5
        --   end,
        --   bidirectional = true
        -- },
        -- {
        --   in_state = "battle_stance",
        --   out_state = "run",
        --   predicate = function(animation, e, context)
        --   if not e:get_char_ctrl_component() then
        --     return false
        --   end
        --   return e:get_char_ctrl_component().contents:get_speed() > 0.5
        --   end,
        --   bidirectional = true
        -- },
        {
          in_state = "idle",
          out_state = "battle_stance",
          predicate = function(animation, e, context)
          if not e:get_ai_component() then
            return false
          end
          return e:get_ai_component().state_name == "battle_stance"
          end,
          bidirectional = true
        },
        -- {
        --   in_state = "run",
        --   out_state = "battle_stance",
        --   predicate = function(animation, e, context)
        --   if not e:get_ai_component() then
        --     return false
        --   end
        --   return e:get_ai_component().state_name == "battle_stance"
        --   end,
        --   bidirectional = true
        -- },
        {
          in_state = "idle",
          out_state = "dead",
          predicate = function(animation, e, context)
          return not e:get_ai_component()
          end,
          bidirectional = false
        },
        -- {
        --   in_state = "run",
        --   out_state = "dead",
        --   predicate = function(animation, e, context)
        --   return not e:get_ai_component()
        --   end,
        --   bidirectional = false
        -- },
      }

      --transitions = player_animation.state_machine1.transitions,

    }
  },

  blend = {
    type = "blend",
    inputs = {
      {
        input = "chain_hook_throw",
        weight = 1,
      },
      {
        input = "chain_hook_pull",
        weight = 1,
      },
      {
        input = "rock_throw",
        weight = 1,
      },
      {
        input = "rock_dropped",
        weight = 1,
      },
      {
        input = "melee_attack_front",
        weight = 1,
      },
      {
        input = "melee_attack_behind",
        weight = 1,
      },
      {
        input = "state_machine1",
        weight = 0,
      },
      {
        input = "ground_slam",
        weight = 1,
      },
      {
        input = "gravel_fling",
        weight = 1,
      },
      {
        input = "lasso_swing",
        weight = 1,
      }
    }
  },

  outputs = {
    "blend"
  },

  ground_slam = {
    type = "state",
    animation = "boss_anm_groundslam",
    event_inputs = {
      "ground_slam"
    },
  },

  melee_attack_front = {
    type = "state",
    animation = "boss_anm_attackfront",
    event_inputs = {
      "melee_attack_front"
    },
  },

  melee_attack_behind = {
    type = "state",
    animation = "boss_anm_attackbehind",
    event_inputs = {
      "melee_attack_behind"
    },
  },

  rock_throw = {
    type = "state",
    animation = "boss_anm_rockthrow",
    event_inputs = {
      "rock_throw"
    },
  },

  rock_dropped = {
    type = "state",
    animation = "boss_anm_wounded",
    event_inputs = {
      "rock_dropped"
    },
  },

  chain_hook_throw = {
    type = "state",
    animation = "boss_anm_hook",
    event_inputs = {
      "chain_hook_throw"
    },
  },

  chain_hook_pull = {
    type = "state",
    animation = "boss_anm_hookpull",
    event_inputs = {
      "chain_hook_pull"
    },
  },

  gravel_fling = {
    type = "state",
    animation = "boss_anm_gravelfling",
    event_inputs = {
      "gravel_fling"
    },
  },

  lasso_swing = {
    type = "state",
    animation = "boss_anm_lasso",
    event_inputs = {
      "lasso_swing"
    },
  }
}

local event_graph = {
}

animation_blueprint = {
  event_graph = event_graph,
  animation_graph = animation_graph
}
