scene = {
  { 
    GraphicTextComponent = {
      font = "arial",
      text = "text1",
      color = {255, 0 , 0},
    },
    Transform2dComponent = {
      position = {0.01, 0.01},
      size = {20, 20},
    }
  },
  {
    GraphicTextComponent = {
      font = "arial",
      text = "text2",
      color = {0, 255 , 0},
    },
    Transform2dComponent = {
      position = {0.05, 0.05},
      size = {50, 10},
      rotation = 45
    }
  },
  {
    TextureComponent = {
      path = "assets/texture/hud/heart.png",
    },
    Transform2dComponent = {
      position = {0.05, 0.01},
      size = {20, 20},
    }
  },
}