scene = {
  entities = {
    entity1542630891_4409 = {
      components = {
        ModelComponent = {
          asset = "assets/model/ltree1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -11.636524200439,
              -28.493919372559,
              0.0
            },
            rotation = {
              0.70660197734833,
              0.0070272237062454,
              0.0062127192504704,
              0.70754903554916
            },
            scale = 3.0
          }
        }
      }
    },
    entity1542809309_5222 = {
      components = {
        ModelComponent = {
          asset = "assets/model/ltree1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -95.493804931641,
              -316.89962768555,
              -16.999893188477
            },
            rotation = {
              0.70660197734833,
              0.0070272251032293,
              0.0062127192504704,
              0.70754903554916
            },
            scale = 3.0
          }
        }
      }
    },
    entity1542812882_504 = {
      components = {
        ModelComponent = {
          asset = "assets/model/home_tree1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/home_tree1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -465.90319824219,
              49.078750610352,
              -11.875151634216
            },
            rotation = {
              0.70635366439819,
              -0.020007656887174,
              -0.02085723914206,
              0.70726883411407
            },
            scale = 3.18270611763
          }
        }
      }
    },
    entity1542813170_9617 = {
      components = {
        DirectionalLightComponent = {
          color = {
            x = 1.0,
            y = 1.0,
            z = 1.0
          },
          direction = {
            x = -0.33333000540733,
            y = 0.66666001081467,
            z = -0.46665999293327
          },
          has_shadow = true,
          is_cascade = true
        },
        TransformComponent = {
          transform = {
            position = {
              -466.37347412109,
              41.049415588379,
              518.98083496094
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1542881250_1764 = {
      components = {
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -458.26892089844,
              36.882148742676,
              520.4521484375
            },
            rotation = {
              -0.16372627019882,
              0.070921003818512,
              -0.45364433526993,
              0.87313842773438
            },
            scale = 1.2383905649185
          }
        }
      }
    },
    entity1542881478_4413 = {
      components = {
        ModelComponent = {
          asset = "assets/model/tree3.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/tree3.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -267.21585083008,
              115.84502410889,
              -16.71167755127
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 3.6640086174011
          }
        }
      }
    },
    entity1542881667_1254 = {
      components = {
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -449.82467651367,
              50.241828918457,
              500.23065185547
            },
            rotation = {
              -0.21607920527458,
              0.67095571756363,
              0.21364906430244,
              0.67637437582016
            },
            scale = 1.1754680871964
          }
        }
      }
    },
    platform_curved_railing = {
      components = {
        ModelComponent = {
          asset = "assets/model/platform_curved_railing.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved_railing.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -465.98236083984,
              37.115142822266,
              520.73425292969
            },
            rotation = {
              0.32682237029076,
              0.62454581260681,
              -0.33239075541496,
              0.62661474943161
            },
            scale = 1.4834102392197
          }
        }
      }
    },
    preset_instance1542355982_5635 = {
      components = {
        PresetInstanceComponent = {
          name = "player"
        },
        TransformComponent = {
          transform = {
            position = {
              -466.64517211914,
              56.611865997314,
              522.60461425781
            },
            rotation = {
              0.0,
              0.0,
              0.99392753839493,
              0.11003646254539
            },
            scale = 1.0000011920929
          }
        }
      }
    },
    preset_instance1542638583_2904 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -11.392868995667,
              -24.313329696655,
              130.30400085449
            },
            rotation = {
              0.16741624474525,
              -0.831647336483,
              -0.15262925624847,
              0.50699001550674
            },
            scale = 1.0000042915344
          }
        }
      }
    },
    preset_instance1542638631_3107 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -13.122458457947,
              -28.033983230591,
              176.0923614502
            },
            rotation = {
              0.5388560295105,
              -0.39414262771606,
              0.35006326436996,
              0.65707039833069
            },
            scale = 1.0000046491623
          }
        }
      }
    },
    preset_instance1542700577_157 = {
      components = {
        PresetInstanceComponent = {
          name = "branch2"
        },
        TransformComponent = {
          transform = {
            position = {
              -12.857340812683,
              -30.981552124023,
              119.64507293701
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1542700636_3210 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -11.079109191895,
              -28.926931381226,
              322.69674682617
            },
            rotation = {
              0.16864438354969,
              -0.021872928366065,
              -0.075583331286907,
              0.98253130912781
            },
            scale = 1.3044391870499
          }
        }
      }
    },
    preset_instance1542700701_1888 = {
      components = {
        PresetInstanceComponent = {
          name = "branch2"
        },
        TransformComponent = {
          transform = {
            position = {
              -11.297657966614,
              -27.26372718811,
              398.27871704102
            },
            rotation = {
              -0.09226094186306,
              -0.0090997852385044,
              0.59950894117355,
              0.7949805855751
            },
            scale = 0.96386736631393
          }
        }
      }
    },
    preset_instance1542702805_2753 = {
      components = {
        PresetInstanceComponent = {
          name = "branch3"
        },
        TransformComponent = {
          transform = {
            position = {
              -11.813658714294,
              -29.24764251709,
              255.0207824707
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1542705677_8280 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -13.781276702881,
              -26.81116104126,
              373.7551574707
            },
            rotation = {
              -0.19943459331989,
              0.36385759711266,
              0.61571437120438,
              0.66987258195877
            },
            scale = 1.943284869194
          }
        }
      }
    },
    preset_instance1542723463_2365 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -8.732120513916,
              -26.410970687866,
              483.01220703125
            },
            rotation = {
              0.13639789819717,
              -0.099615179002285,
              -0.22247916460037,
              0.96019554138184
            },
            scale = 1.5967619419098
          }
        }
      }
    },
    preset_instance1542723873_8539 = {
      components = {
        PresetInstanceComponent = {
          name = "branch3"
        },
        TransformComponent = {
          transform = {
            position = {
              -11.289582252502,
              -29.21245765686,
              445.93887329102
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.2527257204056
          }
        }
      }
    },
    preset_instance1542724679_8753 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -226.10827636719,
              -58.899078369141,
              263.54833984375
            },
            rotation = {
              -6.9478300890324e-10,
              4.1066332556738e-09,
              -0.80729287862778,
              0.59015101194382
            },
            scale = 1.0000023841858
          }
        }
      }
    },
    preset_instance1542808971_2682 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -122.18402099609,
              -502.87850952148,
              253.76739501953
            },
            rotation = {
              -1.1414334755155e-10,
              -4.2979418635181e-11,
              0.99836260080338,
              -0.057203184813261
            },
            scale = 1.0000027418137
          }
        }
      }
    },
    preset_instance1542809070_3464 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -486.95602416992,
              281.86169433594,
              240.19575500488
            },
            rotation = {
              0.0,
              0.0,
              0.86794930696487,
              -0.49665275216103
            },
            scale = 0.99999952316284
          }
        }
      }
    },
    preset_instance1542809104_9253 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -295.45956420898,
              -604.64093017578,
              232.49702453613
            },
            rotation = {
              0.0,
              0.0,
              0.76142501831055,
              0.64825296401978
            },
            scale = 1.0000007152557
          }
        }
      }
    },
    preset_instance1542809316_1673 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -93.359466552734,
              -314.17660522461,
              400.98626708984
            },
            rotation = {
              0.099358625710011,
              0.038840666413307,
              -0.011870235204697,
              0.99422246217728
            },
            scale = 1.3978143930435
          }
        }
      }
    },
    preset_instance1542809360_3957 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -96.382354736328,
              -316.12921142578,
              464.92211914063
            },
            rotation = {
              0.63512402772903,
              0.34160041809082,
              0.69229888916016,
              -0.025473793968558
            },
            scale = 1.0710437297821
          }
        }
      }
    },
    preset_instance1542809413_6695 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -96.273681640625,
              -314.32192993164,
              481.75402832031
            },
            rotation = {
              0.054642200469971,
              0.16053369641304,
              0.89562612771988,
              0.41121405363083
            },
            scale = 1.2163020372391
          }
        }
      }
    },
    preset_instance1542809446_6359 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -94.573120117188,
              -315.04006958008,
              482.37957763672
            },
            rotation = {
              0.20207247138023,
              0.021439872682095,
              0.0069159246049821,
              0.97911143302917
            },
            scale = 1.0000004768372
          }
        }
      }
    },
    preset_instance1542809467_9206 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -95.810729980469,
              -315.5908203125,
              316.48999023438
            },
            rotation = {
              0.15205968916416,
              -0.03142986446619,
              -0.012541870586574,
              0.98779183626175
            },
            scale = 1.0000007152557
          }
        }
      }
    },
    preset_instance1542809505_8978 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -96.945251464844,
              -317.76538085938,
              274.61654663086
            },
            rotation = {
              0.13499842584133,
              -0.097440354526043,
              -0.52560180425644,
              0.83428025245667
            },
            scale = 1.0000027418137
          }
        }
      }
    },
    preset_instance1542809539_7273 = {
      components = {
        PresetInstanceComponent = {
          name = "branch3"
        },
        TransformComponent = {
          transform = {
            position = {
              -93.810028076172,
              -317.35778808594,
              291.93991088867
            },
            rotation = {
              -0.025708325207233,
              -0.0016558935167268,
              0.43060749769211,
              0.90217155218124
            },
            scale = 1.003112912178
          }
        }
      }
    },
    preset_instance1542809583_8052 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -95.586486816406,
              -314.85971069336,
              133.40953063965
            },
            rotation = {
              0.10639534890652,
              0.014693864621222,
              -0.044973731040955,
              0.99319761991501
            },
            scale = 1.0000014305115
          }
        }
      }
    },
    preset_instance1542809750_9530 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree2"
        },
        TransformComponent = {
          transform = {
            position = {
              -233.21406555176,
              429.44348144531,
              289.35565185547
            },
            rotation = {
              -2.6416546727859e-09,
              -6.2786714716268e-10,
              -0.3022668659687,
              0.95322334766388
            },
            scale = 1.0000010728836
          }
        }
      }
    },
    preset_instance1542814628_5166 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -474.03890991211,
              45.480407714844,
              492.53359985352
            },
            rotation = {
              -1.2900104096047e-08,
              -1.8253418332392e-08,
              0.81664437055588,
              0.57714122533798
            },
            scale = 1.0000003576279
          }
        }
      }
    },
    preset_instance1542814628_6044 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -463.81704711914,
              58.189208984375,
              545.23675537109
            },
            rotation = {
              0.080755174160004,
              0.00095203868113458,
              -0.21080893278122,
              0.97418546676636
            },
            scale = 1.0000005960464
          }
        }
      }
    },
    preset_instance1542814655_6338 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -471.63491821289,
              44.734424591064,
              573.47253417969
            },
            rotation = {
              0.061318203806877,
              0.1384047716856,
              0.92196500301361,
              0.35646146535873
            },
            scale = 1.0000007152557
          }
        }
      }
    },
    preset_instance1542814683_7233 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -475.25155639648,
              57.524646759033,
              377.51284790039
            },
            rotation = {
              0.096040934324265,
              0.055973254144192,
              0.66976392269135,
              0.73420667648315
            },
            scale = 1.9904621839523
          }
        }
      }
    },
    preset_instance1542814711_7758 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -458.69946289063,
              58.928630828857,
              315.55270385742
            },
            rotation = {
              0.06939996778965,
              -0.016349323093891,
              -0.54305440187454,
              0.83666497468948
            },
            scale = 1.55925989151
          }
        }
      }
    },
    preset_instance1542877704_9801 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -457.32864379883,
              61.800048828125,
              523.57464599609
            },
            rotation = {
              3.4482461330754e-10,
              -1.7167842703003e-09,
              0.45055320858955,
              0.89274954795837
            },
            scale = 1.0000011920929
          }
        }
      }
    },
    preset_instance1542878986_2210 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -476.40615844727,
              60.122119903564,
              523.98114013672
            },
            rotation = {
              7.3175909776069e-09,
              -1.420490391979e-09,
              0.97524380683899,
              0.22113238275051
            },
            scale = 1.0000039339066
          }
        }
      }
    },
    preset_instance1542881414_9185 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -456.07943725586,
              58.045043945313,
              544.42810058594
            },
            rotation = {
              -1.2059454546076e-09,
              3.6466893948983e-09,
              0.90436339378357,
              -0.42676323652267
            },
            scale = 0.56187927722931
          }
        }
      }
    },
    preset_instance1542881725_9353 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -448.90151977539,
              55.929027557373,
              503.63415527344
            },
            rotation = {
              3.9333047235912e-11,
              -1.0956918011784e-10,
              0.94119322299957,
              -0.33786872029305
            },
            scale = 1.0000013113022
          }
        }
      }
    },
    preset_instance1542881854_9734 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -439.96737670898,
              -631.19897460938,
              264.22018432617
            },
            rotation = {
              4.2134590261256e-10,
              -2.0038520842647e-10,
              -0.37231820821762,
              0.9281051158905
            },
            scale = 1.0000017881393
          }
        }
      }
    },
    preset_instance1542888345_2924 = {
      components = {
        PresetInstanceComponent = {
          name = "branch3"
        },
        TransformComponent = {
          transform = {
            position = {
              -94.50252532959,
              -315.41616821289,
              758.86151123047
            },
            rotation = {
              -3.7678787173645e-11,
              7.8762989186298e-11,
              0.43154439330101,
              0.90209174156189
            },
            scale = 0.99999988079071
          }
        }
      }
    },
    preset_instance1542888345_7774 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -94.513450622559,
              -315.39541625977,
              690.01959228516
            },
            rotation = {
              0.11717074364424,
              -0.013647044077516,
              0.014335000887513,
              0.99291455745697
            },
            scale = 1.0000019073486
          }
        }
      }
    },
    preset_instance1542888345_7833 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -94.687561035156,
              -314.74078369141,
              655.92102050781
            },
            rotation = {
              0.0,
              0.0,
              0.44606050848961,
              0.89500284194946
            },
            scale = 1.0000004768372
          }
        }
      }
    },
    preset_instance1542888345_9849 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -94.504081726074,
              -315.41201782227,
              641.72204589844
            },
            rotation = {
              0.0,
              0.0,
              0.68275421857834,
              0.73064815998077
            },
            scale = 1.0000002384186
          }
        }
      }
    },
    preset_instance1542888419_2101 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -95.754760742188,
              -316.21496582031,
              900.26965332031
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1542888419_2788 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -93.930969238281,
              -316.0466003418,
              1035.2585449219
            },
            rotation = {
              0.11951238662004,
              -0.070521548390388,
              -0.51333105564117,
              0.84689712524414
            },
            scale = 1.0000003576279
          }
        }
      }
    },
    preset_instance1542888419_3896 = {
      components = {
        PresetInstanceComponent = {
          name = "branch3"
        },
        TransformComponent = {
          transform = {
            position = {
              -95.791450500488,
              -316.21813964844,
              948.98181152344
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1542888419_5898 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -95.749794006348,
              -316.12872314453,
              1005.5101928711
            },
            rotation = {
              0.0,
              0.0,
              0.79126292467117,
              0.61147618293762
            },
            scale = 1.0000010728836
          }
        }
      }
    },
    torch_body = {
      components = {
        ModelComponent = {
          asset = "assets/model/torch.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -455.04730224609,
              48.445201873779,
              523.60131835938
            },
            rotation = {
              -0.42370340228081,
              0.55534034967422,
              0.56512171030045,
              -0.43898746371269
            },
            scale = 0.76319235563278
          }
        }
      }
    },
    torch_tip = {
      components = {
        ParticlesComponent = {
          max_end_color = {
            w = 0.3999999165535,
            x = 0.20000000298023,
            y = 0.0
          },
          max_end_size = 0.0099999997764826,
          max_particles = 8.0,
          max_rotation_speed = 9.1999998092651,
          max_spawn_pos = {
            x = 0.10000000149012,
            y = 0.10000000149012
          },
          max_start_color = {
            y = 0.40000000596046
          },
          max_start_size = 0.49999991059303,
          max_start_velocity = {
            z = 0.59999990463257
          },
          min_end_color = {
            w = 0.20000000298023,
            x = 0.20000000298023
          },
          min_rotation_speed = 7.0,
          min_spawn_pos = {
            x = -0.10000000149012,
            y = -0.10000000149012
          },
          min_start_color = {
            x = 1.0,
            y = 0.20000001788139
          },
          min_start_velocity = {
            z = 0.40000000596046
          },
          offset_from_transform_component = {
            z = -0.60000002384186
          },
          size_mode = 1.0,
          spawn_per_second = 4.0,
          velocity_mode = 2.0
        },
        PointLightComponent = {
          color = {
            x = 1.0,
            y = 0.10000001639128
          },
          radius = 200.0
        },
        TransformComponent = {
          transform = {
            position = {
              -455.78475952148,
              48.693035125732,
              525.13000488281
            },
            rotation = {
              0.0093260360881686,
              -0.0045132432132959,
              0.0057966369204223,
              0.99992954730988
            },
            scale = 0.18929043412209
          }
        }
      }
    }
  },
  presets = {
    Tree1 = {
      entity1542630891_4409 = {
        components = {
          ModelComponent = {
            asset = "assets/model/ltree1.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/ltree1.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -11.636524200439,
                -28.493919372559,
                0.0
              },
              rotation = {
                0.70660197734833,
                0.0070272237062454,
                0.0062127192504704,
                0.70754903554916
              },
              scale = 3.0
            }
          }
        },
        relative_transform = {
          position = {
            0.063735008239746,
            -0.52435493469238,
            -270.47436523438
          },
          rotation = {
            0.70660197734833,
            0.0070272237062454,
            0.0062127187848091,
            0.70754909515381
          },
          scale = 3.0
        }
      },
      preset_instance1542638583_2904 = {
        components = {
          PresetInstanceComponent = {
            name = "branch1"
          },
          TransformComponent = {
            transform = {
              position = {
                -11.392868995667,
                -24.313329696655,
                130.30400085449
              },
              rotation = {
                0.16741624474525,
                -0.831647336483,
                -0.15262925624847,
                0.50699001550674
              },
              scale = 1.0000042915344
            }
          }
        },
        relative_transform = {
          position = {
            0.3073902130127,
            3.6562347412109,
            -140.17036437988
          },
          rotation = {
            0.16741625964642,
            -0.831647336483,
            -0.15262924134731,
            0.5069899559021
          },
          scale = 1.0000044107437
        }
      },
      preset_instance1542638631_3107 = {
        components = {
          PresetInstanceComponent = {
            name = "branch1"
          },
          TransformComponent = {
            transform = {
              position = {
                -13.122458457947,
                -28.033983230591,
                176.0923614502
              },
              rotation = {
                0.5388560295105,
                -0.39414262771606,
                0.35006326436996,
                0.65707039833069
              },
              scale = 1.0000046491623
            }
          }
        },
        relative_transform = {
          position = {
            -1.4221992492676,
            -0.064418792724609,
            -94.38200378418
          },
          rotation = {
            0.5388560295105,
            -0.39414265751839,
            0.35006326436996,
            0.65707039833069
          },
          scale = 1.000004529953
        }
      },
      preset_instance1542700577_157 = {
        components = {
          PresetInstanceComponent = {
            name = "branch2"
          },
          TransformComponent = {
            transform = {
              position = {
                -12.857340812683,
                -30.981552124023,
                119.64507293701
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            -1.1570816040039,
            -3.0119876861572,
            -150.82928466797
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      preset_instance1542700636_3210 = {
        components = {
          PresetInstanceComponent = {
            name = "branch1"
          },
          TransformComponent = {
            transform = {
              position = {
                -11.079109191895,
                -28.926931381226,
                322.69674682617
              },
              rotation = {
                0.16864438354969,
                -0.021872928366065,
                -0.075583331286907,
                0.98253130912781
              },
              scale = 1.3044391870499
            }
          }
        },
        relative_transform = {
          position = {
            0.62115001678467,
            -0.95736694335938,
            52.222381591797
          },
          rotation = {
            0.16864436864853,
            -0.021872924640775,
            -0.075583331286907,
            0.98253130912781
          },
          scale = 1.3044391870499
        }
      },
      preset_instance1542700701_1888 = {
        components = {
          PresetInstanceComponent = {
            name = "branch2"
          },
          TransformComponent = {
            transform = {
              position = {
                -11.297657966614,
                -27.26372718811,
                398.27871704102
              },
              rotation = {
                -0.09226094186306,
                -0.0090997852385044,
                0.59950894117355,
                0.7949805855751
              },
              scale = 0.96386736631393
            }
          }
        },
        relative_transform = {
          position = {
            0.40260124206543,
            0.70583724975586,
            127.80435180664
          },
          rotation = {
            -0.092260949313641,
            -0.0090997898951173,
            0.59950894117355,
            0.7949805855751
          },
          scale = 0.96386736631393
        }
      },
      preset_instance1542702805_2753 = {
        components = {
          PresetInstanceComponent = {
            name = "branch3"
          },
          TransformComponent = {
            transform = {
              position = {
                -11.813658714294,
                -29.24764251709,
                255.0207824707
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            -0.11339950561523,
            -1.2780780792236,
            -15.453582763672
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      preset_instance1542705677_8280 = {
        components = {
          PresetInstanceComponent = {
            name = "branch5"
          },
          TransformComponent = {
            transform = {
              position = {
                -13.781276702881,
                -26.81116104126,
                373.7551574707
              },
              rotation = {
                -0.19943459331989,
                0.36385759711266,
                0.61571437120438,
                0.66987258195877
              },
              scale = 1.943284869194
            }
          }
        },
        relative_transform = {
          position = {
            -2.0810174942017,
            1.1584033966064,
            103.28079223633
          },
          rotation = {
            -0.19943459331989,
            0.36385759711266,
            0.61571437120438,
            0.66987258195877
          },
          scale = 1.943284869194
        }
      },
      preset_instance1542723463_2365 = {
        components = {
          PresetInstanceComponent = {
            name = "branch5"
          },
          TransformComponent = {
            transform = {
              position = {
                -8.732120513916,
                -26.410970687866,
                483.01220703125
              },
              rotation = {
                0.13639789819717,
                -0.099615179002285,
                -0.22247916460037,
                0.96019554138184
              },
              scale = 1.5967619419098
            }
          }
        },
        relative_transform = {
          position = {
            2.9681386947632,
            1.55859375,
            212.53784179688
          },
          rotation = {
            0.13639791309834,
            -0.099615179002285,
            -0.22247916460037,
            0.96019554138184
          },
          scale = 1.5967619419098
        }
      },
      preset_instance1542723873_8539 = {
        components = {
          PresetInstanceComponent = {
            name = "branch3"
          },
          TransformComponent = {
            transform = {
              position = {
                -11.289582252502,
                -29.21245765686,
                445.93887329102
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.2527257204056
            }
          }
        },
        relative_transform = {
          position = {
            0.41067695617676,
            -1.2428932189941,
            175.46450805664
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.2527257204056
        }
      }
    },
    Tree2 = {
      entity1542809309_5222 = {
        components = {
          ModelComponent = {
            asset = "assets/model/ltree1.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/ltree1.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                374.35131835938,
                -29.763900756836,
                -12.35758972168
              },
              rotation = {
                0.70660197734833,
                0.0070272251032293,
                0.0062127192504704,
                0.70754903554916
              },
              scale = 3.0
            }
          }
        },
        relative_transform = {
          position = {
            -0.1343994140625,
            -1.1061515808105,
            -331.38854980469
          },
          rotation = {
            0.70660197734833,
            0.0070272237062454,
            0.0062127187848091,
            0.70754909515381
          },
          scale = 3.0
        }
      },
      preset_instance1542809316_1673 = {
        components = {
          PresetInstanceComponent = {
            name = "branch5"
          },
          TransformComponent = {
            transform = {
              position = {
                376.48565673828,
                -27.040897369385,
                405.62857055664
              },
              rotation = {
                0.099358625710011,
                0.038840666413307,
                -0.011870235204697,
                0.99422246217728
              },
              scale = 1.3978143930435
            }
          }
        },
        relative_transform = {
          position = {
            1.9999389648438,
            1.6168518066406,
            86.597595214844
          },
          rotation = {
            0.099358625710011,
            0.038840666413307,
            -0.011870235204697,
            0.99422246217728
          },
          scale = 1.3978143930435
        }
      },
      preset_instance1542809360_3957 = {
        components = {
          PresetInstanceComponent = {
            name = "branch5"
          },
          TransformComponent = {
            transform = {
              position = {
                373.46276855469,
                -28.993511199951,
                469.56442260742
              },
              rotation = {
                0.63512402772903,
                0.34160041809082,
                0.69229888916016,
                -0.025473793968558
              },
              scale = 1.0710437297821
            }
          }
        },
        relative_transform = {
          position = {
            -1.02294921875,
            -0.33576202392578,
            150.53344726563
          },
          rotation = {
            0.63512402772903,
            0.34160041809082,
            0.69229888916016,
            -0.025473793968558
          },
          scale = 1.0710437297821
        }
      },
      preset_instance1542809413_6695 = {
        components = {
          PresetInstanceComponent = {
            name = "branch5"
          },
          TransformComponent = {
            transform = {
              position = {
                373.57144165039,
                -27.186223983765,
                486.39633178711
              },
              rotation = {
                0.054642200469971,
                0.16053369641304,
                0.89562612771988,
                0.41121405363083
              },
              scale = 1.2163020372391
            }
          }
        },
        relative_transform = {
          position = {
            -0.91427612304688,
            1.4715251922607,
            167.36535644531
          },
          rotation = {
            0.054642200469971,
            0.16053369641304,
            0.89562612771988,
            0.41121405363083
          },
          scale = 1.2163020372391
        }
      },
      preset_instance1542809446_6359 = {
        components = {
          PresetInstanceComponent = {
            name = "branch1"
          },
          TransformComponent = {
            transform = {
              position = {
                375.27200317383,
                -27.904357910156,
                487.02188110352
              },
              rotation = {
                0.20207250118256,
                0.021439880132675,
                0.0069159250706434,
                0.97911143302917
              },
              scale = 1.0000004768372
            }
          }
        },
        relative_transform = {
          position = {
            0.78628540039063,
            0.75339126586914,
            167.99090576172
          },
          rotation = {
            0.20207248628139,
            0.021439876407385,
            0.0069159246049821,
            0.97911143302917
          },
          scale = 1.0000004768372
        }
      },
      preset_instance1542809467_9206 = {
        components = {
          PresetInstanceComponent = {
            name = "branch1"
          },
          TransformComponent = {
            transform = {
              position = {
                374.03439331055,
                -28.45512008667,
                321.13229370117
              },
              rotation = {
                0.15205965936184,
                -0.03142986446619,
                -0.012541870586574,
                0.98779183626175
              },
              scale = 1.0000007152557
            }
          }
        },
        relative_transform = {
          position = {
            -0.45132446289063,
            0.20262908935547,
            2.101318359375
          },
          rotation = {
            0.152059674263,
            -0.03142986446619,
            -0.012541870586574,
            0.98779183626175
          },
          scale = 1.0000007152557
        }
      },
      preset_instance1542809505_8978 = {
        components = {
          PresetInstanceComponent = {
            name = "branch1"
          },
          TransformComponent = {
            transform = {
              position = {
                372.89987182617,
                -30.629661560059,
                279.25885009766
              },
              rotation = {
                0.13499842584133,
                -0.097440354526043,
                -0.52560180425644,
                0.83428025245667
              },
              scale = 1.0000027418137
            }
          }
        },
        relative_transform = {
          position = {
            -1.5858459472656,
            -1.9719123840332,
            -39.772125244141
          },
          rotation = {
            0.13499842584133,
            -0.097440354526043,
            -0.52560180425644,
            0.83428025245667
          },
          scale = 1.0000027418137
        }
      },
      preset_instance1542809539_7273 = {
        components = {
          PresetInstanceComponent = {
            name = "branch3"
          },
          TransformComponent = {
            transform = {
              position = {
                376.03509521484,
                -30.222076416016,
                296.58221435547
              },
              rotation = {
                -0.025708323344588,
                -0.0016558935167268,
                0.43060749769211,
                0.90217161178589
              },
              scale = 1.0031130313873
            }
          }
        },
        relative_transform = {
          position = {
            1.5493774414063,
            -1.5643272399902,
            -22.448760986328
          },
          rotation = {
            -0.025708325207233,
            -0.001655895030126,
            0.43060749769211,
            0.90217155218124
          },
          scale = 1.0031130313873
        }
      },
      preset_instance1542809583_8052 = {
        components = {
          PresetInstanceComponent = {
            name = "branch1"
          },
          TransformComponent = {
            transform = {
              position = {
                374.25863647461,
                -27.723993301392,
                138.05183410645
              },
              rotation = {
                0.10639534890652,
                0.014693868346512,
                -0.044973731040955,
                0.99319761991501
              },
              scale = 1.0000014305115
            }
          }
        },
        relative_transform = {
          position = {
            -0.22708129882813,
            0.93375587463379,
            -180.97914123535
          },
          rotation = {
            0.10639534890652,
            0.01469386741519,
            -0.044973731040955,
            0.99319761991501
          },
          scale = 1.0000014305115
        }
      }
    },
    branch1 = {
      entity1542638324_7561 = {
        components = {
          ModelComponent = {
            asset = "assets/model/branch1.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/branch1.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -10.568613052368,
                -25.330171585083,
                131.19218444824
              },
              rotation = {
                0.54150640964508,
                -0.45398938655853,
                -0.45522418618202,
                0.54169678688049
              },
              scale = 5.9962530136108
            }
          }
        },
        relative_transform = {
          position = {
            0.0,
            0.0,
            0.0
          },
          rotation = {
            0.54150640964508,
            -0.45398938655853,
            -0.45522418618202,
            0.54169678688049
          },
          scale = 5.9962530136108
        }
      }
    },
    branch2 = {
      entity1542639219_3795 = {
        components = {
          ModelComponent = {
            asset = "assets/model/branch2.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/branch2.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -13.087126731873,
                -29.591833114624,
                104.50563812256
              },
              rotation = {
                0.020391419529915,
                0.15186339616776,
                0.34227550029755,
                0.92702168226242
              },
              scale = 5.6400361061096
            }
          }
        },
        relative_transform = {
          position = {
            0.0,
            0.0,
            0.0
          },
          rotation = {
            0.020391419529915,
            0.15186339616776,
            0.34227550029755,
            0.92702168226242
          },
          scale = 5.6400361061096
        }
      }
    },
    branch3 = {
      entity1542702482_8458 = {
        components = {
          ModelComponent = {
            asset = "assets/model/branch3.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/branch3.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -11.770669937134,
                -28.262519836426,
                249.21772766113
              },
              rotation = {
                0.4286003112793,
                0.52815306186676,
                0.35326960682869,
                0.64230579137802
              },
              scale = 7.2702283859253
            }
          }
        },
        relative_transform = {
          position = {
            0.0,
            0.0,
            0.0
          },
          rotation = {
            0.4286003112793,
            0.52815306186676,
            0.35326960682869,
            0.64230579137802
          },
          scale = 7.2702283859253
        }
      }
    },
    branch5 = {
      entity1542705535_8829 = {
        components = {
          ModelComponent = {
            asset = "assets/model/branch5.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/branch5.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -64.423698425293,
                3.5395348072052,
                292.43762207031
              },
              rotation = {
                -0.39743614196777,
                -0.012220916338265,
                0.91681200265884,
                -0.036754716187716
              },
              scale = 2.394593000412
            }
          }
        },
        relative_transform = {
          position = {
            0.0,
            0.0,
            0.0
          },
          rotation = {
            -0.39743614196777,
            -0.01222091820091,
            0.91681200265884,
            -0.036754716187716
          },
          scale = 2.394593000412
        }
      }
    },
    player = {
      player = {
        components = {
          CharacterControllerComponent = {
            contents = {
              gravity = {
                x = 0.0,
                y = 0.0,
                z = 0.0
              },
              height = 0.80000001192093,
              mass = 80.0,
              offset_transform = {
                position = {
                  [3] = -0.69999998807907
                },
                rotation = {},
                scale = 1.0
              },
              radius = 0.30000001192093,
              turning_speed = 10.0
            }
          },
          ModelComponent = {
            asset = "assets/model/player_mesh.fbx"
          },
          ParticlesComponent = {
            max_life = 0.20000000298023,
            min_life = 0.10000000149012,
            spawn_per_second = 0.0
          },
          PlayerComponent = {},
          PlayerDamageVisualComponent = {
            color = {
              x = 1.0
            }
          },
          RadialComponent = {
            blur_radius = 0.0
          },
          SlowMotionComponent = {
            factor = 0.85000002384186
          },
          TransformComponent = {
            transform = {
              position = {
                -0.011647939682007,
                1.9561524391174,
                8.6799917221069
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            0.0,
            0.0,
            0.0
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      }
    },
    preset1542352401_7694 = {
      entity1542352208_9049 = {
        components = {
          AttachToEntityComponent = {
            entity_handle = "platform",
            offset_transform = {
              position = {
                1.3356764316559,
                0.094623267650604,
                2.7289505004883
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 0.58206957578659
            }
          },
          ModelComponent = {
            asset = "assets/model/hill01.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/hill01.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                1.4915218353271,
                4.5999827384949,
                79.764846801758
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 2.0358080863953
            }
          }
        },
        relative_transform = {
          position = {
            2.0702769756317,
            1.6400372982025,
            0.87398529052734
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 2.0358080863953
        }
      },
      entity1542352230_8904 = {
        components = {
          ModelComponent = {
            asset = "assets/model/hill01.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/hill01.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -0.047735527157784,
                0.010818481445313,
                86.6875
              },
              rotation = {
                0.0,
                -0.38850998878479,
                0.0,
                0.92144453525543
              },
              scale = 1.1553937196732
            }
          }
        },
        relative_transform = {
          position = {
            0.53101968765259,
            -2.949126958847,
            7.7966384887695
          },
          rotation = {
            0.0,
            -0.38850998878479,
            0.0,
            0.92144453525543
          },
          scale = 1.1553937196732
        }
      },
      platform = {
        components = {
          ModelComponent = {
            asset = "assets/model/hill01.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/hill01.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -3.1800518035889,
                4.2690348625183,
                70.220245361328
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 3.4975340366364
            }
          }
        },
        relative_transform = {
          position = {
            -2.6012966632843,
            1.309089422226,
            -8.6706161499023
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 3.4975340366364
        }
      }
    },
    torch_preset = {
      torch_body = {
        components = {
          ModelComponent = {
            asset = "assets/model/torch.fbx"
          },
          TransformComponent = {
            transform = {
              position = {
                -455.04730224609,
                48.445201873779,
                523.60131835938
              },
              rotation = {
                -0.42370340228081,
                0.55534034967422,
                0.56512171030045,
                -0.43898746371269
              },
              scale = 0.76319235563278
            }
          }
        },
        relative_transform = {
          position = {
            0.36871337890625,
            -0.12391662597656,
            -0.76434326171875
          },
          rotation = {
            -0.42370340228081,
            0.55534034967422,
            0.56512171030045,
            -0.43898746371269
          },
          scale = 0.76319235563278
        }
      },
      torch_tip = {
        components = {
          ParticlesComponent = {
            max_end_color = {
              w = 0.3999999165535,
              x = 0.20000000298023,
              y = 0.0
            },
            max_end_size = 0.0099999997764826,
            max_particles = 8.0,
            max_rotation_speed = 9.1999998092651,
            max_spawn_pos = {
              x = 0.10000000149012,
              y = 0.10000000149012
            },
            max_start_color = {
              y = 0.40000000596046
            },
            max_start_size = 0.49999991059303,
            max_start_velocity = {
              z = 0.59999990463257
            },
            min_end_color = {
              w = 0.20000000298023,
              x = 0.20000000298023
            },
            min_rotation_speed = 7.0,
            min_spawn_pos = {
              x = -0.10000000149012,
              y = -0.10000000149012
            },
            min_start_color = {
              x = 1.0,
              y = 0.20000001788139
            },
            min_start_velocity = {
              z = 0.40000000596046
            },
            offset_from_transform_component = {
              z = -0.60000002384186
            },
            size_mode = 1.0,
            spawn_per_second = 4.0,
            velocity_mode = 2.0
          },
          PointLightComponent = {
            color = {
              x = 1.0,
              y = 0.10000001639128
            },
            radius = 200.0
          },
          TransformComponent = {
            transform = {
              position = {
                -455.78475952148,
                48.693035125732,
                525.13000488281
              },
              rotation = {
                0.0093260360881686,
                -0.0045132432132959,
                0.0057966369204223,
                0.99992954730988
              },
              scale = 0.18929043412209
            }
          }
        },
        relative_transform = {
          position = {
            -0.36874389648438,
            0.12391662597656,
            0.76434326171875
          },
          rotation = {
            0.0093260360881686,
            -0.0045132432132959,
            0.0057966369204223,
            0.99992954730988
          },
          scale = 0.18929043412209
        }
      }
    },
    tree1 = {
      branch1 = {
        components = {
          ModelComponent = {
            asset = "assets/model/branch1.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/branch1.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                9.6640958786011,
                49.375057220459,
                134.74984741211
              },
              rotation = {
                2.1918904025142e-08,
                1.9010945706555e-08,
                0.65521669387817,
                0.75544100999832
              },
              scale = 8.2171440124512
            }
          }
        },
        relative_transform = {
          position = {
            0.59966659545898,
            -5.2656135559082,
            71.804702758789
          },
          rotation = {
            2.1918904025142e-08,
            1.9010945706555e-08,
            0.65521669387817,
            0.75544100999832
          },
          scale = 8.2171440124512
        }
      },
      entity1542359855_514 = {
        components = {
          ModelComponent = {
            asset = "assets/model/branch1.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/branch1.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                5.8241033554077,
                53.843925476074,
                58.593662261963
              },
              rotation = {
                0.0,
                0.0,
                0.59897601604462,
                0.80076694488525
              },
              scale = 8.217137336731
            }
          }
        },
        relative_transform = {
          position = {
            -3.2403259277344,
            -0.79674530029297,
            -4.3514785766602
          },
          rotation = {
            0.0,
            0.0,
            0.59897601604462,
            0.80076694488525
          },
          scale = 8.217137336731
        }
      },
      entity1542359869_1571 = {
        components = {
          ModelComponent = {
            asset = "assets/model/branch1.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/branch1.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                7.8632664680481,
                55.312454223633,
                154.37452697754
              },
              rotation = {
                0.0,
                0.0,
                0.66088080406189,
                0.75049090385437
              },
              scale = 8.2171087265015
            }
          }
        },
        relative_transform = {
          position = {
            -1.201162815094,
            0.67178344726563,
            91.429382324219
          },
          rotation = {
            0.0,
            0.0,
            0.66088080406189,
            0.75049090385437
          },
          scale = 8.2171087265015
        }
      },
      tree = {
        components = {
          ModelComponent = {
            asset = "assets/model/tree1.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/tree1.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                12.90625,
                60.031242370605,
                -95.937469482422
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 6.8691353797913
            }
          }
        },
        relative_transform = {
          position = {
            3.8418207168579,
            5.3905715942383,
            -158.88261413574
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 6.8691353797913
        }
      }
    }
  }
}
