scene = {
  entities = {
    enemy_spawn = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 4.0,
          max_spawns = 1.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -77.386703491211,
              2004.3774414063,
              317.48248291016
            },
            rotation = {
              0.0,
              0.0,
              0.98403996229172,
              0.17794762551785
            },
            scale = 0.54843991994858
          }
        }
      }
    },
    entity1544093802_4958 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Don't forget to double jump!"
        },
        TransformComponent = {
          transform = {
            position = {
              -72.222129821777,
              286.06930541992,
              42.803638458252
            },
            rotation = {
              -0.2253979742527,
              0.0032100891694427,
              0.013868654146791,
              0.9741627573967
            },
            scale = 7.6099109649658
          }
        }
      }
    },
    entity1544093853_9168 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "The blue bar in the lower-left corner of your screen indicates your STAMINA level."
        },
        TransformComponent = {
          transform = {
            position = {
              -64.675636291504,
              347.18099975586,
              73.066177368164
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.3219397068024
          }
        }
      }
    },
    entity1544094016_7694 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Dashing depletes your STAMINA. But don't worry, it regenerates automatically."
        },
        TransformComponent = {
          transform = {
            position = {
              -64.678337097168,
              347.17886352539,
              71.699638366699
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.3219397068024
          }
        }
      }
    },
    entity1544102004_4579 = {
      components = {
        ModelComponent = {
          asset = "assets/model/branch1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/branch1.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              45.019199371338,
              441.42687988281,
              112.61029052734
            },
            rotation = {
              0.7070021033287,
              0.0,
              0.0,
              0.7072114944458
            },
            scale = 10.361478805542
          }
        }
      }
    },
    entity1544102142_7300 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Aim here"
        },
        TransformComponent = {
          transform = {
            position = {
              -54.349761962891,
              419.42736816406,
              135.50158691406
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 6.5222105979919
          }
        }
      }
    },
    entity1544102204_6088 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "When you are close enough to the"
        },
        TransformComponent = {
          transform = {
            position = {
              -96.035079956055,
              450.07681274414,
              89.409645080566
            },
            rotation = {
              0.056290209293365,
              -0.0092246988788247,
              -0.062221597880125,
              0.99643099308014
            },
            scale = 7.1099925041199
          }
        }
      }
    },
    entity1544103122_9579 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "object you are aiming at, your crosshair"
        },
        TransformComponent = {
          transform = {
            position = {
              -96.043334960938,
              450.07818603516,
              81.348915100098
            },
            rotation = {
              0.056290160864592,
              -0.0092246755957603,
              -0.062221601605415,
              0.99643105268478
            },
            scale = 7.1099286079407
          }
        }
      }
    },
    entity1544103159_5054 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "turns GREEN."
        },
        TransformComponent = {
          transform = {
            position = {
              -96.052505493164,
              450.08358764648,
              74.104560852051
            },
            rotation = {
              0.05629014223814,
              -0.0092246411368251,
              -0.062221944332123,
              0.99643099308014
            },
            scale = 7.1098909378052
          }
        }
      }
    },
    entity1544103284_3519 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "This means you can shoot rope at it."
        },
        TransformComponent = {
          transform = {
            position = {
              -96.072868347168,
              450.10440063477,
              66.373184204102
            },
            rotation = {
              0.056287407875061,
              -0.0092240003868937,
              -0.062217969447374,
              0.99643141031265
            },
            scale = 7.1101922988892
          }
        }
      }
    },
    entity1544103326_8642 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Press E/X while in air to shoot a rope."
        },
        TransformComponent = {
          transform = {
            position = {
              -96.070426940918,
              450.10659790039,
              59.297752380371
            },
            rotation = {
              0.056287847459316,
              -0.0092239715158939,
              -0.062217961996794,
              0.99643141031265
            },
            scale = 7.1101922988892
          }
        }
      }
    },
    entity1544103489_9576 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Good job!"
        },
        TransformComponent = {
          transform = {
            position = {
              -40.800079345703,
              482.55514526367,
              62.528999328613
            },
            rotation = {
              -0.22540497779846,
              0.0032104856800288,
              0.013868995942175,
              0.97416114807129
            },
            scale = 2.3871645927429
          }
        }
      }
    },
    entity1544103526_1723 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "You can control your swinging with WASD/Left-Stick."
        },
        TransformComponent = {
          transform = {
            position = {
              -52.805553436279,
              482.55325317383,
              57.214881896973
            },
            rotation = {
              -0.22540497779846,
              0.0032103864941746,
              0.013869422487915,
              0.97416114807129
            },
            scale = 2.3871645927429
          }
        }
      }
    },
    entity1544103579_850 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Swing over here and jump onto this platform."
        },
        TransformComponent = {
          transform = {
            position = {
              -53.255359649658,
              481.0647277832,
              54.604888916016
            },
            rotation = {
              -0.22539953887463,
              0.0032103338744491,
              0.013869571499527,
              0.97416239976883
            },
            scale = 2.3870928287506
          }
        }
      }
    },
    entity1544104451_5313 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Now, swing yourself to the canopy over there!"
        },
        TransformComponent = {
          transform = {
            position = {
              -141.23208618164,
              584.46374511719,
              139.66648864746
            },
            rotation = {
              0.02741614356637,
              0.029574200510979,
              -0.017134645953774,
              0.99903959035873
            },
            scale = 9.6808748245239
          }
        }
      }
    },
    entity1544104559_5666 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "After attaching a rope, both your"
        },
        TransformComponent = {
          transform = {
            position = {
              -73.107719421387,
              712.33813476563,
              243.36212158203
            },
            rotation = {
              0.36663216352463,
              0.03364584967494,
              -0.0060166292823851,
              0.92973792552948
            },
            scale = 9.6806211471558
          }
        }
      }
    },
    entity1544104632_11 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "double-jump and dash is REPLENISHED."
        },
        TransformComponent = {
          transform = {
            position = {
              -73.104438781738,
              718.10211181641,
              236.45777893066
            },
            rotation = {
              0.36663219332695,
              0.033645384013653,
              -0.0060163796879351,
              0.92973792552948
            },
            scale = 9.6807403564453
          }
        }
      }
    },
    entity1544104715_6114 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Sweet!"
        },
        TransformComponent = {
          transform = {
            position = {
              -64.464721679688,
              855.56311035156,
              199.48767089844
            },
            rotation = {
              0.03980403020978,
              0.0095196682959795,
              0.13375851511955,
              0.99016851186752
            },
            scale = 9.6805982589722
          }
        }
      }
    },
    entity1544104791_6065 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Come here."
        },
        TransformComponent = {
          transform = {
            position = {
              -137.232421875,
              1078.0046386719,
              223.77502441406
            },
            rotation = {
              0.039037078619003,
              0.012300863862038,
              0.20321832597256,
              0.97827762365341
            },
            scale = 9.6810092926025
          }
        }
      }
    },
    entity1544105396_6242 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "(kinda far, remember to dash)"
        },
        TransformComponent = {
          transform = {
            position = {
              -150.80123901367,
              1071.8975830078,
              214.75393676758
            },
            rotation = {
              0.03903716430068,
              0.012299926951528,
              0.20321869850159,
              0.97827756404877
            },
            scale = 6.2314124107361
          }
        }
      }
    },
    entity1544105463_2692 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Let's slow down."
        },
        TransformComponent = {
          transform = {
            position = {
              -133.47654724121,
              1091.1918945313,
              199.2525177002
            },
            rotation = {
              0.034381445497274,
              0.022199651226401,
              0.45505625009537,
              0.88952171802521
            },
            scale = 0.98302501440048
          }
        }
      }
    },
    entity1544105529_7127 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "The floating rocks up ahead are quite small and hard to hit."
        },
        TransformComponent = {
          transform = {
            position = {
              -137.1455078125,
              1086.3562011719,
              198.28182983398
            },
            rotation = {
              0.03438039496541,
              0.02220100350678,
              0.45505940914154,
              0.8895201086998
            },
            scale = 0.98306912183762
          }
        }
      }
    },
    entity1544105559_4653 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Hold SHIFT/Left-Stick to slow down time."
        },
        TransformComponent = {
          transform = {
            position = {
              -137.13381958008,
              1086.3498535156,
              197.28590393066
            },
            rotation = {
              0.034380536526442,
              0.022200733423233,
              0.45505940914154,
              0.8895201086998
            },
            scale = 0.98306912183762
          }
        }
      }
    },
    entity1544105608_4213 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "This gives you time to think and aim."
        },
        TransformComponent = {
          transform = {
            position = {
              -137.1357421875,
              1086.3428955078,
              196.2720489502
            },
            rotation = {
              0.034380562603474,
              0.022200979292393,
              0.45506104826927,
              0.88951927423477
            },
            scale = 0.98307430744171
          }
        }
      }
    },
    entity1544105632_1983 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "BEWARE: slowmotion depletes your stamina."
        },
        TransformComponent = {
          transform = {
            position = {
              -137.13116455078,
              1086.3360595703,
              195.2477722168
            },
            rotation = {
              0.034380488097668,
              0.022200813516974,
              0.45506310462952,
              0.88951820135117
            },
            scale = 0.98307102918625
          }
        }
      }
    },
    entity1544106247_6089 = {
      components = {
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -177.42837524414,
              1150.1674804688,
              209.38383483887
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544106520_1292 = {
      components = {
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -183.92330932617,
              1252.6351318359,
              270.69281005859
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544106528_9836 = {
      components = {
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -214.69052124023,
              1213.7380371094,
              286.63055419922
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544106534_1163 = {
      components = {
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -193.54032897949,
              1203.4871826172,
              253.46408081055
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544106541_7059 = {
      components = {
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -130.2271270752,
              1232.5460205078,
              249.65998840332
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544106543_2471 = {
      components = {
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -182.11601257324,
              1326.5939941406,
              284.70989990234
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544106544_3747 = {
      components = {
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -133.76184082031,
              1291.1158447266,
              247.68272399902
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544106546_1943 = {
      components = {
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -208.27633666992,
              1284.3551025391,
              282.62484741211
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544106547_2584 = {
      components = {
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -108.86750793457,
              1358.9910888672,
              243.68690490723
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544106549_6067 = {
      components = {
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -253.36720275879,
              1392.4182128906,
              250.91525268555
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544106551_1134 = {
      components = {
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -173.21040344238,
              1426.56640625,
              244.93955993652
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544107138_1208 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "You can also ZOOM by using holding Right-Click/Left-Trigger."
        },
        TransformComponent = {
          transform = {
            position = {
              -137.12757873535,
              1086.3433837891,
              192.9038848877
            },
            rotation = {
              0.034380529075861,
              0.022201092913747,
              0.45506474375725,
              0.88951736688614
            },
            scale = 0.98309397697449
          }
        }
      }
    },
    entity1544107217_6143 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = ":)"
        },
        TransformComponent = {
          transform = {
            position = {
              -129.80830383301,
              1230.6082763672,
              249.8723449707
            },
            rotation = {
              -0.095287449657917,
              0.89725762605667,
              0.049182675778866,
              0.42828738689423
            },
            scale = 0.81047558784485
          }
        }
      }
    },
    entity1544107536_350 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "You're the man now, dawg."
        },
        TransformComponent = {
          transform = {
            position = {
              -149.78952026367,
              1905.8588867188,
              348.91757202148
            },
            rotation = {
              0.040548089891672,
              0.0055696475319564,
              0.03600337356329,
              0.99851316213608
            },
            scale = 9.6812534332275
          }
        }
      }
    },
    entity1544107592_5169 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Here's a reward"
        },
        TransformComponent = {
          transform = {
            position = {
              -132.17938232422,
              1905.8060302734,
              340.02258300781
            },
            rotation = {
              0.040547322481871,
              0.0055694747716188,
              0.036003321409225,
              0.99851322174072
            },
            scale = 9.6813983917236
          }
        }
      }
    },
    entity1544107607_6629 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "->"
        },
        TransformComponent = {
          transform = {
            position = {
              -101.23969268799,
              1905.1396484375,
              329.57907104492
            },
            rotation = {
              -0.030825415626168,
              0.68515026569366,
              -0.12960639595985,
              0.71611529588699
            },
            scale = 7.2480735778809
          }
        }
      }
    },
    entity1544107915_8214 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Protip: You can spam ropes. You don't have to wait for them to hit."
        },
        TransformComponent = {
          transform = {
            position = {
              -84.652755737305,
              1864.5504150391,
              319.54067993164
            },
            rotation = {
              0.040443196892738,
              -0.0062840105965734,
              -0.25165122747421,
              0.96695220470428
            },
            scale = 1.0976091623306
          }
        }
      }
    },
    entity1544108617_8346 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "These are EXPLOSIVE ARROWS."
        },
        TransformComponent = {
          transform = {
            position = {
              -100.31436920166,
              1901.1339111328,
              320.44046020508
            },
            rotation = {
              0.040439885109663,
              -0.0062841372564435,
              -0.2516630589962,
              0.96694922447205
            },
            scale = 0.21726763248444
          }
        }
      }
    },
    entity1544108702_5474 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Press R/Right-Button to switch to explosive arrows. Press Q/Left-Button to switch back to normal arrows."
        },
        TransformComponent = {
          transform = {
            position = {
              -100.31349945068,
              1901.1303710938,
              320.2451171875
            },
            rotation = {
              0.040439907461405,
              -0.0062840585596859,
              -0.25166127085686,
              0.9669497013092
            },
            scale = 0.21726720035076
          }
        }
      }
    },
    entity1544108762_6450 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Press Left-Click/Right-Trigger to FIRE an arrow."
        },
        TransformComponent = {
          transform = {
            position = {
              -100.30924987793,
              1901.1942138672,
              320.03521728516
            },
            rotation = {
              0.040438015013933,
              -0.0062840525060892,
              -0.25167134404182,
              0.96694719791412
            },
            scale = 0.21726970374584
          }
        }
      }
    },
    entity1544108798_5713 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Hold Left-Click/Right-Trigger to CHARGE up a special arrow of the current arrow type."
        },
        TransformComponent = {
          transform = {
            position = {
              -100.31179046631,
              1901.1497802734,
              319.86013793945
            },
            rotation = {
              0.040439236909151,
              -0.0062840534374118,
              -0.2516647875309,
              0.96694880723953
            },
            scale = 0.21726806461811
          }
        }
      }
    },
    entity1544108894_2645 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "A charged up arrow deals more damage."
        },
        TransformComponent = {
          transform = {
            position = {
              -100.30969238281,
              1901.1319580078,
              319.68167114258
            },
            rotation = {
              0.040439274162054,
              -0.0062839486636221,
              -0.25166243314743,
              0.96694940328598
            },
            scale = 0.21726748347282
          }
        }
      }
    },
    entity1544109039_7181 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Use explosive arrows to clear the path ahead."
        },
        TransformComponent = {
          transform = {
            position = {
              -100.31053924561,
              1901.1412353516,
              319.46365356445
            },
            rotation = {
              0.040439777076244,
              -0.0062838750891387,
              -0.25165891647339,
              0.96695029735565
            },
            scale = 0.21727335453033
          }
        }
      }
    },
    entity1544109085_1846 = {
      components = {
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -92.105155944824,
              1914.0955810547,
              317.01330566406
            },
            rotation = {
              -8.4480298312428e-09,
              -1.8607650531521e-07,
              -0.050346679985523,
              0.99873179197311
            },
            scale = 4.0973510742188
          }
        }
      }
    },
    entity1544109305_8407 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "A charged up arrow deals more damage."
        },
        TransformComponent = {
          transform = {
            position = {
              -100.30969238281,
              1901.1319580078,
              320.68167114258
            },
            rotation = {
              0.040439274162054,
              -0.0062839486636221,
              -0.25166243314743,
              0.96694940328598
            },
            scale = 0.21726748347282
          }
        }
      }
    },
    entity1544109310_747 = {
      components = {
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -101.16619873047,
              1915.0865478516,
              326.03622436523
            },
            rotation = {
              0.038220573216677,
              0.70187658071518,
              -0.038988918066025,
              0.71020305156708
            },
            scale = 4.0976700782776
          }
        }
      }
    },
    entity1544109459_5556 = {
      components = {
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -78.087196350098,
              1954.0949707031,
              325.57220458984
            },
            rotation = {
              -0.70199662446976,
              0.03596555441618,
              0.7103236913681,
              0.036707121878862
            },
            scale = 4.097936630249
          }
        }
      }
    },
    entity1544109500_812 = {
      components = {
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -92.120674133301,
              1914.0905761719,
              335.27035522461
            },
            rotation = {
              0.051844350993633,
              0.99865514039993,
              4.075230754097e-05,
              -0.00026446007541381
            },
            scale = 4.0973434448242
          }
        }
      }
    },
    entity1544109538_5484 = {
      components = {
        ModelComponent = {
          asset = "assets/model/hill01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hill01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              93.331436157227,
              2030.1207275391,
              270.79010009766
            },
            rotation = {
              0.082474574446678,
              -0.078663669526577,
              -0.80786746740341,
              0.57823884487152
            },
            scale = 60.796558380127
          }
        }
      }
    },
    entity1544109670_6687 = {
      components = {
        ModelComponent = {
          asset = "assets/model/hill01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hill01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -249.08396911621,
              2045.7258300781,
              266.26364135742
            },
            rotation = {
              0.063463799655437,
              0.094669163227081,
              0.70740789175034,
              0.69755584001541
            },
            scale = 60.799285888672
          }
        }
      }
    },
    entity1544109772_97 = {
      components = {
        ModelComponent = {
          asset = "assets/model/hill01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hill01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -101.0159072876,
              1978.7468261719,
              363.34295654297
            },
            rotation = {
              -0.46856105327606,
              0.53049844503403,
              -0.5266050696373,
              0.47085988521576
            },
            scale = 7.7153868675232
          }
        }
      }
    },
    entity1544109837_8760 = {
      components = {
        ModelComponent = {
          asset = "assets/model/hill01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hill01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -80.008369445801,
              1980.8166503906,
              361.89410400391
            },
            rotation = {
              -0.45120847225189,
              -0.54533326625824,
              0.54699945449829,
              0.44700574874878
            },
            scale = 7.7153816223145
          }
        }
      }
    },
    entity1544109937_8494 = {
      components = {
        ModelComponent = {
          asset = "assets/model/hill01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hill01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -101.0283203125,
              1985.4943847656,
              357.67465209961
            },
            rotation = {
              -0.42403247952461,
              0.57597231864929,
              -0.47644355893135,
              0.51132559776306
            },
            scale = 7.7154078483582
          }
        }
      }
    },
    entity1544109969_2327 = {
      components = {
        ModelComponent = {
          asset = "assets/model/hill01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hill01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -90.875564575195,
              1982.5112304688,
              361.20529174805
            },
            rotation = {
              0.51584714651108,
              0.59678000211716,
              -0.48651707172394,
              -0.37557488679886
            },
            scale = 7.7153997421265
          }
        }
      }
    },
    entity1544110122_5037 = {
      components = {
        ModelComponent = {
          asset = "assets/model/hill01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hill01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -303.71875,
              2030.9152832031,
              -48.855937957764
            },
            rotation = {
              0.063462913036346,
              0.094668932259083,
              0.7074071764946,
              0.69755667448044
            },
            scale = 96.468894958496
          }
        }
      }
    },
    entity1544110144_1619 = {
      components = {
        ModelComponent = {
          asset = "assets/model/hill01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hill01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              93.227058410645,
              2004.0628662109,
              -6.8738574981689
            },
            rotation = {
              0.082476809620857,
              -0.078667216002941,
              -0.80786556005478,
              0.57824075222015
            },
            scale = 82.825080871582
          }
        }
      }
    },
    entity1544110166_6983 = {
      components = {
        ModelComponent = {
          asset = "assets/model/hill01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hill01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              307.68026733398,
              1938.1572265625,
              -14.66185092926
            },
            rotation = {
              0.082475163042545,
              -0.078664168715477,
              -0.80786621570587,
              0.57824033498764
            },
            scale = 87.033592224121
          }
        }
      }
    },
    entity1544110185_5419 = {
      components = {
        ModelComponent = {
          asset = "assets/model/hill01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hill01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              194.02325439453,
              2074.1416015625,
              -93.042816162109
            },
            rotation = {
              0.082475326955318,
              -0.07866445183754,
              -0.80786573886871,
              0.57824099063873
            },
            scale = 100.31898498535
          }
        }
      }
    },
    entity1544110255_2507 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -94.426765441895,
              1946.6928710938,
              318.9953918457
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544110311_1496 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -90.428421020508,
              1946.3876953125,
              318.93518066406
            },
            rotation = {
              0.0,
              0.0,
              -0.48935481905937,
              0.87208479642868
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    entity1544110318_1186 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -85.31867980957,
              1946.0056152344,
              318.65328979492
            },
            rotation = {
              0.0,
              0.0,
              -0.48935481905937,
              0.87208479642868
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    entity1544110320_9194 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -87.229873657227,
              1946.5443115234,
              321.40084838867
            },
            rotation = {
              0.059617847204208,
              -0.16859668493271,
              -0.44709384441376,
              0.87642908096313
            },
            scale = 0.99999958276749
          }
        }
      }
    },
    entity1544110324_7794 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -91.493995666504,
              1946.7990722656,
              322.16784667969
            },
            rotation = {
              0.0,
              0.0,
              -0.48935481905937,
              0.87208479642868
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    entity1544110327_7536 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -80.835739135742,
              1947.2399902344,
              318.60046386719
            },
            rotation = {
              -0.052316799759865,
              -0.45832806825638,
              -0.26161077618599,
              0.84779608249664
            },
            scale = 0.99999898672104
          }
        }
      }
    },
    entity1544110344_1301 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -85.315559387207,
              1949.1079101563,
              317.7160949707
            },
            rotation = {
              0.0,
              0.0,
              -0.48935481905937,
              0.87208479642868
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    entity1544110344_1359 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -94.423645019531,
              1949.7951660156,
              318.05819702148
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544110344_5694 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -91.490875244141,
              1949.9013671875,
              321.23065185547
            },
            rotation = {
              0.0,
              0.0,
              -0.48935481905937,
              0.87208479642868
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    entity1544110344_6002 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -80.832618713379,
              1950.3422851563,
              317.66326904297
            },
            rotation = {
              -0.052316904067993,
              -0.45832806825638,
              -0.26161077618599,
              0.847796022892
            },
            scale = 0.99999928474426
          }
        }
      }
    },
    entity1544110344_6451 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -87.226753234863,
              1949.6466064453,
              320.46365356445
            },
            rotation = {
              0.059617847204208,
              -0.16859668493271,
              -0.44709384441376,
              0.87642908096313
            },
            scale = 0.99999958276749
          }
        }
      }
    },
    entity1544110344_8 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -90.425300598145,
              1949.4899902344,
              317.99798583984
            },
            rotation = {
              0.0,
              0.0,
              -0.48935481905937,
              0.87208479642868
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    entity1544110348_354 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -94.42366027832,
              1952.7681884766,
              317.94909667969
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544110348_4385 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -85.315574645996,
              1952.0809326172,
              317.60699462891
            },
            rotation = {
              0.0,
              0.0,
              -0.48935481905937,
              0.87208479642868
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    entity1544110348_6551 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -80.832633972168,
              1953.3153076172,
              317.55416870117
            },
            rotation = {
              -0.052316904067993,
              -0.45832806825638,
              -0.26161077618599,
              0.847796022892
            },
            scale = 0.99999928474426
          }
        }
      }
    },
    entity1544110348_6774 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -87.226768493652,
              1952.6196289063,
              320.35455322266
            },
            rotation = {
              0.059617847204208,
              -0.16859668493271,
              -0.44709384441376,
              0.87642908096313
            },
            scale = 0.99999958276749
          }
        }
      }
    },
    entity1544110348_8702 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -90.425315856934,
              1952.4630126953,
              317.88888549805
            },
            rotation = {
              0.0,
              0.0,
              -0.48935481905937,
              0.87208479642868
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    entity1544110348_9363 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -91.49089050293,
              1952.8743896484,
              321.12155151367
            },
            rotation = {
              0.0,
              0.0,
              -0.48935481905937,
              0.87208479642868
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    entity1544110356_2046 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -87.068908691406,
              1948.3430175781,
              323.9167175293
            },
            rotation = {
              0.059617847204208,
              -0.16859668493271,
              -0.44709384441376,
              0.87642908096313
            },
            scale = 0.99999958276749
          }
        }
      }
    },
    entity1544110356_3317 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -85.15771484375,
              1947.8043212891,
              321.16915893555
            },
            rotation = {
              0.0,
              0.0,
              -0.48935481905937,
              0.87208479642868
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    entity1544110356_4016 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -91.333030700684,
              1948.5977783203,
              324.68371582031
            },
            rotation = {
              0.0,
              0.0,
              -0.48935481905937,
              0.87208479642868
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    entity1544110356_7205 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -80.674774169922,
              1949.0386962891,
              321.11633300781
            },
            rotation = {
              -0.052316896617413,
              -0.45832803845406,
              -0.26161077618599,
              0.84779608249664
            },
            scale = 0.99999928474426
          }
        }
      }
    },
    entity1544110356_7734 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -94.265800476074,
              1948.4915771484,
              321.51126098633
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544110356_8316 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -90.267456054688,
              1948.1864013672,
              321.45104980469
            },
            rotation = {
              0.0,
              0.0,
              -0.48935481905937,
              0.87208479642868
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    entity1544110365_3227 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -94.426765441895,
              1946.6928710938,
              319.9953918457
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544110365_409 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -87.229873657227,
              1946.5443115234,
              322.40084838867
            },
            rotation = {
              0.059617847204208,
              -0.16859668493271,
              -0.44709384441376,
              0.87642908096313
            },
            scale = 0.99999958276749
          }
        }
      }
    },
    entity1544110365_6886 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -91.493995666504,
              1946.7990722656,
              323.16784667969
            },
            rotation = {
              0.0,
              0.0,
              -0.48935481905937,
              0.87208479642868
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    entity1544110365_7042 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -80.835739135742,
              1947.2399902344,
              319.60046386719
            },
            rotation = {
              -0.052316799759865,
              -0.45832806825638,
              -0.26161077618599,
              0.84779608249664
            },
            scale = 0.99999898672104
          }
        }
      }
    },
    entity1544110365_9267 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -90.428421020508,
              1946.3876953125,
              319.93518066406
            },
            rotation = {
              0.0,
              0.0,
              -0.48935481905937,
              0.87208479642868
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    entity1544110365_9724 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -85.31867980957,
              1946.0056152344,
              319.65328979492
            },
            rotation = {
              0.0,
              0.0,
              -0.48935481905937,
              0.87208479642868
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    entity1544110367_1372 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -93.875381469727,
              1951.4506835938,
              320.86022949219
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544110367_1770 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -84.767295837402,
              1950.7634277344,
              320.51812744141
            },
            rotation = {
              0.0,
              0.0,
              -0.48935481905937,
              0.87208479642868
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    entity1544110367_3 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -89.87703704834,
              1951.1455078125,
              320.80001831055
            },
            rotation = {
              0.0,
              0.0,
              -0.48935481905937,
              0.87208479642868
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    entity1544110367_3193 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -90.942611694336,
              1951.5568847656,
              324.03268432617
            },
            rotation = {
              0.0,
              0.0,
              -0.48935481905937,
              0.87208479642868
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    entity1544110367_4888 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -80.284355163574,
              1951.9978027344,
              320.46530151367
            },
            rotation = {
              -0.05231687054038,
              -0.45832803845406,
              -0.26161077618599,
              0.84779608249664
            },
            scale = 0.99999922513962
          }
        }
      }
    },
    entity1544110367_7780 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -86.678489685059,
              1951.3021240234,
              323.26568603516
            },
            rotation = {
              0.059617847204208,
              -0.16859668493271,
              -0.44709384441376,
              0.87642908096313
            },
            scale = 0.99999958276749
          }
        }
      }
    },
    entity1544110380_1466 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -85.588172912598,
              1950.5651855469,
              323.94705200195
            },
            rotation = {
              0.0,
              0.0,
              0.91630667448044,
              0.40047731995583
            },
            scale = 1.0000003576279
          }
        }
      }
    },
    entity1544110380_3482 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -84.462440490723,
              1950.3721923828,
              327.17971801758
            },
            rotation = {
              0.0,
              0.0,
              0.91630667448044,
              0.40047731995583
            },
            scale = 1.0000003576279
          }
        }
      }
    },
    entity1544110380_4323 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -88.692977905273,
              1949.7811279297,
              326.41271972656
            },
            rotation = {
              0.16185501217842,
              0.076040394604206,
              0.91643953323364,
              0.35799363255501
            },
            scale = 0.99999916553497
          }
        }
      }
    },
    entity1544110380_5797 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -94.824043273926,
              1947.8376464844,
              323.61233520508
            },
            rotation = {
              0.46125668287277,
              -0.0066162459552288,
              0.86955720186234,
              0.1762632727623
            },
            scale = 0.99999856948853
          }
        }
      }
    },
    entity1544110380_7028 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -90.672645568848,
              1949.9322509766,
              323.66516113281
            },
            rotation = {
              0.0,
              0.0,
              0.91630667448044,
              0.40047731995583
            },
            scale = 1.0000003576279
          }
        }
      }
    },
    entity1544110380_7283 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -81.60799407959,
              1951.0555419922,
              324.00726318359
            },
            rotation = {
              0.0,
              0.0,
              0.99507260322571,
              -0.099148944020271
            },
            scale = 1.0000011920929
          }
        }
      }
    },
    entity1544110392_2893 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -94.823417663574,
              1947.8455810547,
              327.06198120117
            },
            rotation = {
              0.46125671267509,
              -0.0066161984577775,
              0.86955720186234,
              0.1762632727623
            },
            scale = 0.99999731779099
          }
        }
      }
    },
    entity1544110392_2969 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -85.587547302246,
              1950.5731201172,
              327.39669799805
            },
            rotation = {
              0.0,
              0.0,
              0.91630667448044,
              0.40047731995583
            },
            scale = 1.0000003576279
          }
        }
      }
    },
    entity1544110392_3165 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -81.607368469238,
              1951.0634765625,
              327.45690917969
            },
            rotation = {
              0.0,
              0.0,
              0.99507260322571,
              -0.099148944020271
            },
            scale = 1.0000011920929
          }
        }
      }
    },
    entity1544110392_3822 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -88.692352294922,
              1949.7890625,
              329.86236572266
            },
            rotation = {
              0.16185501217842,
              0.076040394604206,
              0.91643953323364,
              0.35799363255501
            },
            scale = 0.99999916553497
          }
        }
      }
    },
    entity1544110392_7062 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -84.461814880371,
              1950.3801269531,
              330.62936401367
            },
            rotation = {
              0.0,
              0.0,
              0.91630667448044,
              0.40047731995583
            },
            scale = 1.0000003576279
          }
        }
      }
    },
    entity1544110392_7077 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -90.672019958496,
              1949.9401855469,
              327.11480712891
            },
            rotation = {
              0.0,
              0.0,
              0.91630667448044,
              0.40047731995583
            },
            scale = 1.0000003576279
          }
        }
      }
    },
    entity1544110405_3649 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -89.882965087891,
              1950.9254150391,
              328.87994384766
            },
            rotation = {
              0.93973863124847,
              0.23092733323574,
              -0.14409966766834,
              -0.20687958598137
            },
            scale = 0.99999886751175
          }
        }
      }
    },
    entity1544110405_3690 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -96.164016723633,
              1953.5972900391,
              332.19833374023
            },
            rotation = {
              0.96348875761032,
              -0.24404910206795,
              -0.046582773327827,
              -0.099797233939171
            },
            scale = 0.99999958276749
          }
        }
      }
    },
    entity1544110405_4259 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -92.510452270508,
              1952.0040283203,
              331.75485229492
            },
            rotation = {
              0.95967012643814,
              0.2586575448513,
              0.0082121873274446,
              -0.10982761532068
            },
            scale = 0.99999928474426
          }
        }
      }
    },
    entity1544110405_4679 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -93.774398803711,
              1952.8359375,
              328.67874145508
            },
            rotation = {
              0.95967012643814,
              0.2586575448513,
              0.0082121836021543,
              -0.10982761532068
            },
            scale = 0.99999928474426
          }
        }
      }
    },
    entity1544110405_8043 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -87.83667755127,
              1949.9167480469,
              331.38705444336
            },
            rotation = {
              0.95967012643814,
              0.2586575448513,
              0.0082121854647994,
              -0.10982761532068
            },
            scale = 0.99999928474426
          }
        }
      }
    },
    entity1544110405_8992 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -84.496719360352,
              1946.7769775391,
              330.60879516602
            },
            rotation = {
              0.86455821990967,
              0.093332260847092,
              -0.46774733066559,
              -0.1582422554493
            },
            scale = 0.99999749660492
          }
        }
      }
    },
    entity1544110442_479 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -80.669494628906,
              1951.2719726563,
              330.36044311523
            },
            rotation = {
              0.0,
              0.0,
              0.99507260322571,
              -0.099148914217949
            },
            scale = 1.0000011920929
          }
        }
      }
    },
    entity1544110446_5321 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -79.96508026123,
              1950.8056640625,
              326.45309448242
            },
            rotation = {
              0.0,
              0.0,
              0.99507260322571,
              -0.099148914217949
            },
            scale = 1.0000011920929
          }
        }
      }
    },
    entity1544110452_1693 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -80.396087646484,
              1951.1329345703,
              332.89733886719
            },
            rotation = {
              0.0,
              0.0,
              0.99507260322571,
              -0.099148914217949
            },
            scale = 1.0000011920929
          }
        }
      }
    },
    entity1544110454_1125 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -83.37816619873,
              1951.33984375,
              333.32794189453
            },
            rotation = {
              0.0,
              0.0,
              0.99507260322571,
              -0.099148914217949
            },
            scale = 1.0000011920929
          }
        }
      }
    },
    entity1544110463_2138 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -87.294387817383,
              1951.9810791016,
              333.42013549805
            },
            rotation = {
              0.89487081766129,
              0.43252354860306,
              -0.012344590388238,
              -0.10944017022848
            },
            scale = 0.99999904632568
          }
        }
      }
    },
    entity1544110463_801 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -92.40746307373,
              1952.2166748047,
              333.78784179688
            },
            rotation = {
              0.89487081766129,
              0.43252354860306,
              -0.012344590388238,
              -0.10944017022848
            },
            scale = 0.99999904632568
          }
        }
      }
    },
    entity1544110463_8453 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -96.390647888184,
              1952.3647460938,
              334.23141479492
            },
            rotation = {
              0.99206072092056,
              -0.060713712126017,
              -0.064320228993893,
              -0.089399553835392
            },
            scale = 0.99999874830246
          }
        }
      }
    },
    entity1544110468_3840 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -95.46231842041,
              1951.2054443359,
              327.03622436523
            },
            rotation = {
              0.96348875761032,
              -0.24404910206795,
              -0.046582769602537,
              -0.099797233939171
            },
            scale = 0.99999958276749
          }
        }
      }
    },
    entity1544110468_4035 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -91.808753967285,
              1949.6121826172,
              326.59274291992
            },
            rotation = {
              0.95967012643814,
              0.2586575448513,
              0.0082121333107352,
              -0.10982768982649
            },
            scale = 0.99999928474426
          }
        }
      }
    },
    entity1544110468_5242 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -87.134979248047,
              1947.5249023438,
              326.22494506836
            },
            rotation = {
              0.95967012643814,
              0.2586575448513,
              0.0082121333107352,
              -0.10982768982649
            },
            scale = 0.99999928474426
          }
        }
      }
    },
    entity1544110480_5371 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -96.700538635254,
              1946.5407714844,
              324.24313354492
            },
            rotation = {
              0.6992968916893,
              0.094628252089024,
              0.68609541654587,
              0.17692503333092
            },
            scale = 0.99999731779099
          }
        }
      }
    },
    entity1544110480_8174 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -95.758766174316,
              1951.3189697266,
              316.5080871582
            },
            rotation = {
              0.65615195035934,
              -0.25968274474144,
              0.68491238355637,
              -0.18145133554935
            },
            scale = 1.0000002384186
          }
        }
      }
    },
    entity1544110480_8928 = {
      components = {
        DestructibleComponent = {
          health_points = 1.0,
          max_health_points = 1.0
        },
        ModelComponent = {
          asset = "assets/model/boulder-boss.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/boulder-boss.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -96.082763671875,
              1949.2137451172,
              319.90628051758
            },
            rotation = {
              0.6992968916893,
              0.094628252089024,
              0.68609541654587,
              0.17692503333092
            },
            scale = 0.99999731779099
          }
        }
      }
    },
    entity1544111234_648 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Well done!"
        },
        TransformComponent = {
          transform = {
            position = {
              -83.056686401367,
              2047.6528320313,
              322.55462646484
            },
            rotation = {
              0.037826105952263,
              0.0070126731880009,
              0.018331043422222,
              0.99909156560898
            },
            scale = 1.6268281936646
          }
        }
      }
    },
    entity1544111321_6747 = {
      components = {
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              -93.539291381836,
              2052.8610839844,
              309.36248779297
            },
            rotation = {
              -0.52612119913101,
              -0.46614214777946,
              0.53258293867111,
              -0.47144815325737
            },
            scale = 5.0673565864563
          }
        }
      }
    },
    entity1544111388_3253 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "This is the end."
        },
        TransformComponent = {
          transform = {
            position = {
              -84.358192443848,
              2047.365234375,
              320.81610107422
            },
            rotation = {
              0.037826493382454,
              0.0070127388462424,
              0.018330987542868,
              0.99909156560898
            },
            scale = 1.6268401145935
          }
        }
      }
    },
    entity1544111419_4006 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "And you made it all this way!"
        },
        TransformComponent = {
          transform = {
            position = {
              -86.60595703125,
              2046.8166503906,
              319.72561645508
            },
            rotation = {
              0.037826456129551,
              0.0070127351209521,
              0.018330555409193,
              0.99909162521362
            },
            scale = 1.6268408298492
          }
        }
      }
    },
    entity1544111472_1891 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Now you are ready for the real deal."
        },
        TransformComponent = {
          transform = {
            position = {
              -86.631141662598,
              2047.1905517578,
              318.03213500977
            },
            rotation = {
              0.037823844701052,
              0.0070126093924046,
              0.01833189278841,
              0.99909162521362
            },
            scale = 1.168980717659
          }
        }
      }
    },
    entity1544111516_9954 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544111710_4717",
            "entity1544111667_3459",
            "entity1544111657_5495"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -95.312492370605,
              1957.0001220703,
              317.25
            },
            rotation = {
              -8.4480298312428e-09,
              -1.8607650531521e-07,
              -0.050346683710814,
              0.99873185157776
            },
            scale = 0.99999994039536
          }
        }
      }
    },
    entity1544111657_5495 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544111516_9954",
            "entity1544111667_3459"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -79.249977111816,
              1956.0001220703,
              317.25
            },
            rotation = {
              -8.4480298312428e-09,
              -1.8607650531521e-07,
              -0.050346683710814,
              0.99873185157776
            },
            scale = 0.99999994039536
          }
        }
      }
    },
    entity1544111667_3459 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544111516_9954",
            "entity1544111710_4717",
            "entity1544111657_5495"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -70.782012939453,
              2041.2907714844,
              317.12384033203
            },
            rotation = {
              -8.4480413775623e-09,
              -1.8607659058034e-07,
              -0.050346706062555,
              0.99873185157776
            },
            scale = 0.99999994039536
          }
        }
      }
    },
    entity1544111710_4717 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544111516_9954",
            "entity1544111667_3459"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -86.17724609375,
              2042.9826660156,
              316.99404907227
            },
            rotation = {
              -8.4480431539191e-09,
              -1.860766047912e-07,
              -0.050346709787846,
              0.99873185157776
            },
            scale = 0.99999994039536
          }
        }
      }
    },
    entity1544112776_1369 = {
      components = {
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -74.569313049316,
              2003.8713378906,
              316.99374389648
            },
            rotation = {
              0.053805731236935,
              -0.031798068434,
              -0.70705276727676,
              0.70439350605011
            },
            scale = 0.98679757118225
          }
        }
      }
    },
    entity1544112856_9535 = {
      components = {
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -75.333557128906,
              1998.6027832031,
              317.18182373047
            },
            rotation = {
              0.053805731236935,
              -0.031798068434,
              -0.70705276727676,
              0.70439350605011
            },
            scale = 0.98679667711258
          }
        }
      }
    },
    entity1544112863_6761 = {
      components = {
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -73.893440246582,
              2009.3314208984,
              317.01794433594
            },
            rotation = {
              0.053805731236935,
              -0.031798068434,
              -0.70705276727676,
              0.70439350605011
            },
            scale = 0.98679667711258
          }
        }
      }
    },
    entity1544112970_3579 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 4.0,
          max_spawns = 1.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -77.804290771484,
              1998.6154785156,
              317.5705871582
            },
            rotation = {
              -8.4480289430644e-09,
              -1.8607650531521e-07,
              -0.050346679985523,
              0.99873185157776
            },
            scale = 0.54843783378601
          }
        }
      }
    },
    entity1544112978_4783 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 4.0,
          max_spawns = 1.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -76.673095703125,
              2009.6147460938,
              317.29220581055
            },
            rotation = {
              -8.4480298312428e-09,
              -1.8607651952607e-07,
              -0.050346687436104,
              0.99873185157776
            },
            scale = 0.54843264818192
          }
        }
      }
    },
    entity1544113210_3987 = {
      components = {
        ParticlesComponent = {
          life_time = 10.0,
          max_end_color = {
            w = 0.10000000149012,
            x = 0.30000001192093,
            y = 0.30000001192093,
            z = 0.30000001192093
          },
          max_life = 5.0,
          max_particles = 100.0,
          max_spawn_pos = {
            x = 0.0,
            y = 0.0
          },
          max_start_color = {
            w = 0.20000000298023,
            x = 0.20000000298023,
            y = 0.20000000298023,
            z = 0.20000000298023
          },
          max_start_size = 0.25,
          max_start_velocity = {
            y = 1.0,
            z = 2.0
          },
          min_end_color = {
            w = 0.10000000149012,
            x = 0.30000001192093,
            y = 0.30000001192093,
            z = 0.30000001192093
          },
          min_end_size = 0.69999998807907,
          min_start_color = {
            x = 0.10000000149012,
            y = 0.10000000149012,
            z = 0.10000000149012
          },
          min_start_size = 0.10000000149012,
          min_start_velocity = {
            y = 0.5,
            z = 0.5
          },
          spawn_per_second = 5.0
        },
        TransformComponent = {
          transform = {
            position = {
              -74.969024658203,
              2000.5712890625,
              322.01696777344
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 0.32826596498489
          }
        }
      }
    },
    entity1544113562_9691 = {
      components = {
        ParticlesComponent = {
          life_time = 10.0,
          max_end_color = {
            w = 0.10000000149012,
            x = 0.30000001192093,
            y = 0.30000001192093,
            z = 0.30000001192093
          },
          max_life = 5.0,
          max_particles = 100.0,
          max_spawn_pos = {
            x = 0.0,
            y = 0.0
          },
          max_start_color = {
            w = 0.20000000298023,
            x = 0.20000000298023,
            y = 0.20000000298023,
            z = 0.20000000298023
          },
          max_start_size = 0.25,
          max_start_velocity = {
            y = 1.0,
            z = 2.0
          },
          min_end_color = {
            w = 0.10000000149012,
            x = 0.30000001192093,
            y = 0.30000001192093,
            z = 0.30000001192093
          },
          min_end_size = 0.69999998807907,
          min_start_color = {
            x = 0.10000000149012,
            y = 0.10000000149012,
            z = 0.10000000149012
          },
          min_start_size = 0.10000000149012,
          min_start_velocity = {
            y = 0.5,
            z = 0.5
          },
          spawn_per_second = 5.0
        },
        TransformComponent = {
          transform = {
            position = {
              -74.135864257813,
              2005.7907714844,
              321.85229492188
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 0.32826596498489
          }
        }
      }
    },
    entity1544113569_3127 = {
      components = {
        ParticlesComponent = {
          life_time = 10.0,
          max_end_color = {
            w = 0.10000000149012,
            x = 0.30000001192093,
            y = 0.30000001192093,
            z = 0.30000001192093
          },
          max_life = 5.0,
          max_particles = 100.0,
          max_spawn_pos = {
            x = 0.0,
            y = 0.0
          },
          max_start_color = {
            w = 0.20000000298023,
            x = 0.20000000298023,
            y = 0.20000000298023,
            z = 0.20000000298023
          },
          max_start_size = 0.25,
          max_start_velocity = {
            y = 1.0,
            z = 2.0
          },
          min_end_color = {
            w = 0.10000000149012,
            x = 0.30000001192093,
            y = 0.30000001192093,
            z = 0.30000001192093
          },
          min_end_size = 0.69999998807907,
          min_start_color = {
            x = 0.10000000149012,
            y = 0.10000000149012,
            z = 0.10000000149012
          },
          min_start_size = 0.10000000149012,
          min_start_velocity = {
            y = 0.5,
            z = 0.5
          },
          spawn_per_second = 5.0
        },
        TransformComponent = {
          transform = {
            position = {
              -73.587074279785,
              2011.2919921875,
              321.8415222168
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 0.32826596498489
          }
        }
      }
    },
    entity1544426664_7381 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_player_checkpoint.fbx"
        },
        NameComponent = {
          name = "spawn_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -43.78694152832,
              -30.354808807373,
              110.88830566406
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544426697_4292 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_checkpoint_area.fbx"
        },
        NameComponent = {
          name = "checkpoint_area"
        },
        TransformComponent = {
          transform = {
            position = {
              -42.941307067871,
              -31.793739318848,
              115.95741271973
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 16.702669143677
          }
        }
      }
    },
    entity1544426741_2956 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_player_checkpoint.fbx"
        },
        NameComponent = {
          name = "spawn_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -45.367126464844,
              9.9084053039551,
              109.18428039551
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544426741_6365 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_checkpoint_area.fbx"
        },
        NameComponent = {
          name = "checkpoint_area"
        },
        TransformComponent = {
          transform = {
            position = {
              -46.389850616455,
              10.144672393799,
              107.94964599609
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 16.702669143677
          }
        }
      }
    },
    entity1544426752_2071 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_checkpoint_area.fbx"
        },
        NameComponent = {
          name = "checkpoint_area"
        },
        TransformComponent = {
          transform = {
            position = {
              -45.149215698242,
              105.99385070801,
              101.70737457275
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 21.520017623901
          }
        }
      }
    },
    entity1544426752_2158 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_player_checkpoint.fbx"
        },
        NameComponent = {
          name = "spawn_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -38.615459442139,
              110.14156341553,
              102.48113250732
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.2884169816971
          }
        }
      }
    },
    entity1544426781_3649 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_player_checkpoint.fbx"
        },
        NameComponent = {
          name = "spawn_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -42.56254196167,
              337.49914550781,
              69.636024475098
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 3.46107006073
          }
        }
      }
    },
    entity1544426781_633 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_checkpoint_area.fbx"
        },
        NameComponent = {
          name = "checkpoint_area"
        },
        TransformComponent = {
          transform = {
            position = {
              -43.110469818115,
              337.95193481445,
              67.017784118652
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 57.809146881104
          }
        }
      }
    },
    entity1544426806_3167 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_checkpoint_area.fbx"
        },
        NameComponent = {
          name = "checkpoint_area"
        },
        TransformComponent = {
          transform = {
            position = {
              -35.528926849365,
              495.87133789063,
              66.334953308105
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 17.556413650513
          }
        }
      }
    },
    entity1544426806_8918 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_player_checkpoint.fbx"
        },
        NameComponent = {
          name = "spawn_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -35.362522125244,
              495.73382568359,
              67.130104064941
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0511127710342
          }
        }
      }
    },
    entity1544426848_3199 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_checkpoint_area.fbx"
        },
        NameComponent = {
          name = "checkpoint_area"
        },
        TransformComponent = {
          transform = {
            position = {
              -124.34056091309,
              1041.1070556641,
              141.60746765137
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 156.66421508789
          }
        }
      }
    },
    entity1544426848_4089 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_player_checkpoint.fbx"
        },
        NameComponent = {
          name = "spawn_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -104.29825592041,
              1073.8107910156,
              195.43167114258
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 5.9628872871399
          }
        }
      }
    },
    entity1544426888_2022 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_checkpoint_area.fbx"
        },
        NameComponent = {
          name = "checkpoint_area"
        },
        TransformComponent = {
          transform = {
            position = {
              -101.5284576416,
              1878.3685302734,
              319.76129150391
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 46.599552154541
          }
        }
      }
    },
    entity1544426888_2524 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_player_checkpoint.fbx"
        },
        NameComponent = {
          name = "spawn_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -101.13027954102,
              1870.9796142578,
              319.96508789063
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 2.793555021286
          }
        }
      }
    },
    light = {
      components = {
        DirectionalLightComponent = {
          color = {
            x = 1.0,
            y = 1.0,
            z = 1.0
          },
          direction = {
            x = -0.33333000540733,
            y = 0.66666001081467,
            z = -0.46665999293327
          },
          has_shadow = true,
          is_cascade = true
        },
        TransformComponent = {
          transform = {
            position = {
              -35.444900512695,
              -16.045906066895,
              117.59289550781
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1544088000_6911 = {
      components = {
        PresetInstanceComponent = {
          name = "platform01"
        },
        TransformComponent = {
          transform = {
            position = {
              -43.929714202881,
              -33.524501800537,
              91.042747497559
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 4.225977897644
          }
        }
      }
    },
    preset_instance1544093004_6911 = {
      components = {
        PresetInstanceComponent = {
          name = "platform01"
        },
        TransformComponent = {
          transform = {
            position = {
              -43.411304473877,
              9.9053287506104,
              88.572578430176
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 4.259964466095
          }
        }
      }
    },
    preset_instance1544093210_7487 = {
      components = {
        PresetInstanceComponent = {
          name = "platform01"
        },
        TransformComponent = {
          transform = {
            position = {
              -41.848747253418,
              106.2474899292,
              78.278503417969
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 5.0590505599976
          }
        }
      }
    },
    preset_instance1544093430_2030 = {
      components = {
        PresetInstanceComponent = {
          name = "platform01"
        },
        TransformComponent = {
          transform = {
            position = {
              -39.068691253662,
              332.50286865234,
              -13.473812103271
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 16.273065567017
          }
        }
      }
    },
    preset_instance1544103425_2261 = {
      components = {
        PresetInstanceComponent = {
          name = "platform01"
        },
        TransformComponent = {
          transform = {
            position = {
              -35.974033355713,
              494.67581176758,
              40.783584594727
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 4.9128684997559
          }
        }
      }
    },
    preset_instance1544103927_8732 = {
      components = {
        PresetInstanceComponent = {
          name = "tree_one_branch"
        },
        TransformComponent = {
          transform = {
            position = {
              34.083938598633,
              598.80181884766,
              -626.89477539063
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1544103949_4266 = {
      components = {
        PresetInstanceComponent = {
          name = "tree_one_branch"
        },
        TransformComponent = {
          transform = {
            position = {
              26.75407409668,
              734.92321777344,
              -557.37487792969
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1544104140_2029 = {
      components = {
        PresetInstanceComponent = {
          name = "tree_one_branch"
        },
        TransformComponent = {
          transform = {
            position = {
              33.248519897461,
              831.86206054688,
              -497.98968505859
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1544104280_7616 = {
      components = {
        PresetInstanceComponent = {
          name = "tree_one_branch"
        },
        TransformComponent = {
          transform = {
            position = {
              -93.263107299805,
              990.93566894531,
              -717.9609375
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 0.8055831193924
          }
        }
      }
    },
    preset_instance1544106627_907 = {
      components = {
        PresetInstanceComponent = {
          name = "preset1544106626_9179"
        },
        TransformComponent = {
          transform = {
            position = {
              -160.56651306152,
              1273.2030029297,
              304.24447631836
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1544106792_6421 = {
      components = {
        PresetInstanceComponent = {
          name = "preset1544106626_9179"
        },
        TransformComponent = {
          transform = {
            position = {
              -156.83801269531,
              1417.2761230469,
              315.51602172852
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1544106819_8790 = {
      components = {
        PresetInstanceComponent = {
          name = "preset1544106626_9179"
        },
        TransformComponent = {
          transform = {
            position = {
              -142.13087463379,
              1610.8677978516,
              324.44253540039
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1544106900_409 = {
      components = {
        PresetInstanceComponent = {
          name = "preset1544106626_9179"
        },
        TransformComponent = {
          transform = {
            position = {
              -134.24401855469,
              1732.2453613281,
              405.04803466797
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1544106933_4046 = {
      components = {
        PresetInstanceComponent = {
          name = "tree_one_branch"
        },
        TransformComponent = {
          transform = {
            position = {
              -96.917175292969,
              1873.4844970703,
              -5.04638671875
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 0.28650096058846
          }
        }
      }
    },
    preset_instance1544107505_3677 = {
      components = {
        PresetInstanceComponent = {
          name = "Pickup_Explosive"
        },
        TransformComponent = {
          transform = {
            position = {
              -103.98703765869,
              1902.5380859375,
              319.15875244141
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.995885848999
          }
        }
      }
    },
    preset_instance1544109728_4098 = {
      components = {
        PresetInstanceComponent = {
          name = "preset1544109725_2808"
        },
        TransformComponent = {
          transform = {
            position = {
              -86.094543457031,
              1967.6898193359,
              326.13754272461
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1544109878_6699 = {
      components = {
        PresetInstanceComponent = {
          name = "preset1544109725_2808"
        },
        TransformComponent = {
          transform = {
            position = {
              -81.845161437988,
              2011.0573730469,
              325.73400878906
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1544113101_5495 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -77.179534912109,
              2001.1331787109,
              319.03131103516
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 0.65011614561081
          }
        }
      }
    },
    preset_instance1544113162_7668 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -76.456855773926,
              2006.7043457031,
              318.87985229492
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1544113622_1677 = {
      components = {
        PresetInstanceComponent = {
          name = "player"
        },
        TransformComponent = {
          transform = {
            position = {
              -44.323017120361,
              -34.250316619873,
              111.94076538086
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    text_dash = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Press V/Y to dash"
        },
        TransformComponent = {
          transform = {
            position = {
              -61.319431304932,
              202.24711608887,
              73.228981018066
            },
            rotation = {
              -0.22540701925755,
              0.0032103774137795,
              0.013869101181626,
              0.97416067123413
            },
            scale = 7.6099348068237
          }
        }
      }
    },
    text_dash_2 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "You can dash two times"
        },
        TransformComponent = {
          transform = {
            position = {
              -70.24787902832,
              294.80877685547,
              60.283908843994
            },
            rotation = {
              -0.22540554404259,
              0.0032101988326758,
              0.013868841342628,
              0.974161028862
            },
            scale = 7.609911441803
          }
        }
      }
    },
    text_double_jump = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Press SPACE/A again to doublejump"
        },
        TransformComponent = {
          transform = {
            position = {
              -94.794609069824,
              72.818687438965,
              97.360221862793
            },
            rotation = {
              -0.22540429234505,
              0.0032102989498526,
              0.013868958689272,
              0.97416126728058
            },
            scale = 7.6099333763123
          }
        }
      }
    },
    text_jump = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.10000000149012,
          text = "Press SPACE/A to jump"
        },
        TransformComponent = {
          transform = {
            position = {
              -64.701705932617,
              -1.0413588285446,
              109.74115753174
            },
            rotation = {
              -0.22538602352142,
              0.0032097934745252,
              0.013871045783162,
              0.97416549921036
            },
            scale = 4.9211173057556
          }
        }
      }
    },
    tree01 = {
      components = {
        ModelComponent = {
          asset = "assets/model/ltree1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1.fbx",
            type = "Triangle"
          }
        },
        MaterialComponent = {
          material = 2
        },
        TransformComponent = {
          transform = {
            position = {
              43.571887969971,
              440.28576660156,
              -1371.6726074219
            },
            rotation = {
              0.70700019598007,
              0.0,
              0.0,
              0.70721334218979
            },
            scale = 4.2433805465698
          }
        }
      }
    }
  },
  presets = {
    Pickup_Explosive = {
      entity1542963051_7944 = {
        components = {
          ModelComponent = {
            asset = "assets/model/bomb_arrow_pickup.fbx"
          },
          ParticlesComponent = {
            max_end_color = {
              w = 1.0,
              x = 0.2,
              y = 0.0,
              z = 0.0
            },
            max_start_color = {
              w = 1.0,
              x = 0.3,
              y = 0.02,
              z = 0.0
            },
            min_end_color = {
              w = 1.0,
              x = 0.0,
              y = 0.0,
              z = 0.0
            },
            min_start_color = {
              w = 1.0,
              x = 0.0,
              y = 0.0,
              z = 0.0
            },
            spawn_per_second = 0.0
          },
          PickupComponent = {
            position = {
              x = -448.28573608398,
              y = 47.945159912109,
              z = 503.16680908203
            },
            respawn_cooldown = 10.0,
            player_range = 0,
            type = 1.0
          },
          TransformComponent = {
            transform = {
              position = {
                -448.28573608398,
                47.945159912109,
                503.16680908203
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            0.0,
            0.0,
            0.0
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      }
    },
    checkpoint = {
      entity1544100873_5186 = {
        components = {
          ModelComponent = {
            asset = "assets/model/editor_player_checkpoint.fbx"
          },
          NameComponent = {
            name = "spawn_point"
          },
          TransformComponent = {
            transform = {
              position = {
                -776.40576171875,
                -96.101280212402,
                237.14265441895
              },
              rotation = {
                0.00036721955984831,
                0.00010899553308263,
                0.92105281352997,
                0.38943752646446
              },
              scale = 2.2971346378326
            }
          }
        }
      }
    },
    checkpoint_area = {
      entity1544100026_8289 = {
        components = {
          ModelComponent = {
            asset = "assets/model/editor_checkpoint_area.fbx"
          },
          NameComponent = {
            name = "checkpoint_area"
          },
          TransformComponent = {
            transform = {
              position = {
                -91.995941162109,
                -255.58233642578,
                162.40544128418
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 230.0
            }
          }
        }
      }
    },
    platform01 = {
      entity1544087931_2728 = {
        components = {
          ModelComponent = {
            asset = "assets/model/hill01.fbx"
          },
          TransformComponent = {
            transform = {
              position = {
                -27.975276947021,
                -20.935512542725,
                101.08126068115
              },
              rotation = {
                2.234777269905e-07,
                0.99999046325684,
                -2.7681485903486e-07,
                0.0043655629269779
              },
              scale = 0.99999898672104
            }
          }
        },
        relative_transform = {
          position = {
            0.020853042602539,
            0.068920135498047,
            -1.9428787231445
          },
          rotation = {
            2.2347774120135e-07,
            0.99999046325684,
            -2.7681485903486e-07,
            0.0043655633926392
          },
          scale = 0.99999892711639
        }
      },
      platform01 = {
        components = {
          ModelComponent = {
            asset = "assets/model/hill01.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/hill01.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -28.016983032227,
                -21.073350906372,
                104.96701812744
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            -0.020853042602539,
            -0.068918228149414,
            1.9428787231445
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      }
    },
    player = {
      player = {
        components = {
          AimOffsetComponent = {
            m_joint_name = "spine_01"
          },
          AnimationComponent = {
            attack_state_machine = {
              name = "player_attack",
              start_state = "none"
            },
            state_machine = {
              name = "player",
              start_state = "idle"
            }
          },
          CharacterControllerComponent = {
            contents = {
              gravity = {
                x = 0.0,
                y = 0.0,
                z = -9.82
              },
              height = 0.25000001192093,
              mass = 80.0,
              offset_transform = {
                position = {
                  [3] = -0.69999998807907
                },
                rotation = {
                  0,
                  0,
                  1,
                  0
                },
                scale = 1.0
              },
              radius = 0.60000001192093,
              turning_speed = 10.0,
              use_navmesh_collision = false
            }
          },
          FactionComponent = {
            type = 0
          },
          HealthComponent = {
            death_callback = {
              name = "player_death_callback",
              path = "assets/script/gmp/death_callbacks.lua"
            },
            health_points = 10,
            max_health_points = 10
          },
          MaterialComponent = {
            material = 6
          },
          ModelComponent = {
            asset = "assets/model/player_mesh.fbx"
          },
          NameComponent = {
            name = "player"
          },
          PlayerComponent = {},
          PlayerDamageVisualComponent = {
            color = {
              x = 1.0
            }
          },
          RadialComponent = {
            blur_radius = 0.0
          },
          SlowMotionComponent = {
            factor = 0.85000002384186
          },
          TransformComponent = {
            transform = {
              position = {
                -0.011647939682007,
                1.9561524391174,
                8.6799917221069
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            0.0,
            0.0,
            0.0
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      }
    },
    preset1544106626_9179 = {
      entity1544106247_6089 = {
        components = {
          ModelComponent = {
            asset = "assets/model/boulder-boss.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/boulder-boss.fbx",
              type = "Triangle"
            }
          },
        MaterialComponent = {
          material = 2
        },
          TransformComponent = {
            transform = {
              position = {
                -177.42837524414,
                1150.1674804688,
                209.38383483887
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            0.69972229003906,
            -134.61572265625,
            -47.378921508789
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      entity1544106520_1292 = {
        components = {
          ModelComponent = {
            asset = "assets/model/boulder-boss.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/boulder-boss.fbx",
              type = "Triangle"
            }
          },
        MaterialComponent = {
          material = 2
        },
          TransformComponent = {
            transform = {
              position = {
                -183.92330932617,
                1252.6351318359,
                270.69281005859
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            -5.7952117919922,
            -32.148071289063,
            13.930053710938
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      entity1544106528_9836 = {
        components = {
          ModelComponent = {
            asset = "assets/model/boulder-boss.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/boulder-boss.fbx",
              type = "Triangle"
            }
          },
        MaterialComponent = {
          material = 2
        },
          TransformComponent = {
            transform = {
              position = {
                -214.69052124023,
                1213.7380371094,
                286.63055419922
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            -36.562423706055,
            -71.045166015625,
            29.867797851563
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      entity1544106534_1163 = {
        components = {
          ModelComponent = {
            asset = "assets/model/boulder-boss.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/boulder-boss.fbx",
              type = "Triangle"
            }
          },
        MaterialComponent = {
          material = 2
        },
          TransformComponent = {
            transform = {
              position = {
                -193.54032897949,
                1203.4871826172,
                253.46408081055
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            -15.412231445313,
            -81.296020507813,
            -3.2986755371094
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      entity1544106541_7059 = {
        components = {
          ModelComponent = {
            asset = "assets/model/boulder-boss.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/boulder-boss.fbx",
              type = "Triangle"
            }
          },
        MaterialComponent = {
          material = 2
        },
          TransformComponent = {
            transform = {
              position = {
                -130.2271270752,
                1232.5460205078,
                249.65998840332
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            47.900970458984,
            -52.237182617188,
            -7.1027679443359
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      entity1544106543_2471 = {
        components = {
          ModelComponent = {
            asset = "assets/model/boulder-boss.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/boulder-boss.fbx",
              type = "Triangle"
            }
          },
        MaterialComponent = {
          material = 2
        },
          TransformComponent = {
            transform = {
              position = {
                -182.11601257324,
                1326.5939941406,
                284.70989990234
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            -3.9879150390625,
            41.810791015625,
            27.947143554688
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      entity1544106544_3747 = {
        components = {
          ModelComponent = {
            asset = "assets/model/boulder-boss.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/boulder-boss.fbx",
              type = "Triangle"
            }
          },
        MaterialComponent = {
          material = 2
        },
          TransformComponent = {
            transform = {
              position = {
                -133.76184082031,
                1291.1158447266,
                247.68272399902
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            44.366256713867,
            6.3326416015625,
            -9.0800323486328
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      entity1544106546_1943 = {
        components = {
          ModelComponent = {
            asset = "assets/model/boulder-boss.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/boulder-boss.fbx",
              type = "Triangle"
            }
          },
          MaterialComponent = {
            material = 2
          },
          TransformComponent = {
            transform = {
              position = {
                -208.27633666992,
                1284.3551025391,
                282.62484741211
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            -30.148239135742,
            -0.4281005859375,
            25.862091064453
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      entity1544106547_2584 = {
        components = {
          ModelComponent = {
            asset = "assets/model/boulder-boss.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/boulder-boss.fbx",
              type = "Triangle"
            }
          },
          MaterialComponent = {
            material = 2
          },
          TransformComponent = {
            transform = {
              position = {
                -108.86750793457,
                1358.9910888672,
                243.68690490723
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            69.260589599609,
            74.207885742188,
            -13.07585144043
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      entity1544106549_6067 = {
        components = {
          ModelComponent = {
            asset = "assets/model/boulder-boss.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/boulder-boss.fbx",
              type = "Triangle"
            }
          },
          MaterialComponent = {
            material = 2
          },
          TransformComponent = {
            transform = {
              position = {
                -253.36720275879,
                1392.4182128906,
                250.91525268555
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            -75.239105224609,
            107.63500976563,
            -5.8475036621094
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      entity1544106551_1134 = {
        components = {
          ModelComponent = {
            asset = "assets/model/boulder-boss.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/boulder-boss.fbx",
              type = "Triangle"
            }
          },
          MaterialComponent = {
            material = 2
          },
          TransformComponent = {
            transform = {
              position = {
                -173.21040344238,
                1426.56640625,
                244.93955993652
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            4.9176940917969,
            141.783203125,
            -11.823196411133
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      }
    },
    preset1544109725_2808 = {
      entity1544109085_1846 = {
        components = {
          ModelComponent = {
            asset = "assets/model/platform_straight.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/platform_straight.fbx",
              type = "Triangle"
            }
          },
          MaterialComponent = {
            material = 2
          },
          TransformComponent = {
            transform = {
              position = {
                -92.105155944824,
                1914.0955810547,
                317.01330566406
              },
              rotation = {
                -8.4480298312428e-09,
                -1.8607650531521e-07,
                -0.050346679985523,
                0.99873179197311
              },
              scale = 4.0973510742188
            }
          }
        },
        relative_transform = {
          position = {
            -1.2353515625,
            -10.246337890625,
            -8.959716796875
          },
          rotation = {
            -8.4480298312428e-09,
            -1.8607650531521e-07,
            -0.050346679985523,
            0.99873179197311
          },
          scale = 4.0973510742188
        }
      },
      entity1544109310_747 = {
        components = {
          ModelComponent = {
            asset = "assets/model/platform_straight.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/platform_straight.fbx",
              type = "Triangle"
            }
          },
          MaterialComponent = {
            material = 2
          },
          TransformComponent = {
            transform = {
              position = {
                -101.16619873047,
                1915.0865478516,
                326.03622436523
              },
              rotation = {
                0.038220573216677,
                0.70187658071518,
                -0.038988918066025,
                0.71020305156708
              },
              scale = 4.0976700782776
            }
          }
        },
        relative_transform = {
          position = {
            -10.296394348145,
            -9.25537109375,
            0.063201904296875
          },
          rotation = {
            0.038220573216677,
            0.70187658071518,
            -0.038988918066025,
            0.71020305156708
          },
          scale = 4.0976705551147
        }
      },
      entity1544109459_5556 = {
        components = {
          ModelComponent = {
            asset = "assets/model/platform_straight.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/platform_straight.fbx",
              type = "Triangle"
            }
          },
          MaterialComponent = {
            material = 2
          },
          TransformComponent = {
            transform = {
              position = {
                -78.087196350098,
                1954.0949707031,
                325.57220458984
              },
              rotation = {
                -0.70199662446976,
                0.03596555441618,
                0.7103236913681,
                0.036707121878862
              },
              scale = 4.097936630249
            }
          }
        },
        relative_transform = {
          position = {
            12.782608032227,
            29.753051757813,
            -0.40081787109375
          },
          rotation = {
            -0.70199662446976,
            0.03596555441618,
            0.7103236913681,
            0.036707121878862
          },
          scale = 4.097936630249
        }
      },
      entity1544109500_812 = {
        components = {
          ModelComponent = {
            asset = "assets/model/platform_straight.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/platform_straight.fbx",
              type = "Triangle"
            }
          },
          MaterialComponent = {
            material = 2
          },
          TransformComponent = {
            transform = {
              position = {
                -92.120674133301,
                1914.0905761719,
                335.27035522461
              },
              rotation = {
                0.051844350993633,
                0.99865514039993,
                4.075230754097e-05,
                -0.00026446007541381
              },
              scale = 4.0973434448242
            }
          }
        },
        relative_transform = {
          position = {
            -1.2508697509766,
            -10.251342773438,
            9.2973327636719
          },
          rotation = {
            0.051844354718924,
            0.99865514039993,
            4.075230754097e-05,
            -0.00026446007541381
          },
          scale = 4.0973434448242
        }
      }
    },
    torch_preset = {
      torch_body = {
        components = {
          ModelComponent = {
            asset = "assets/model/torch.fbx"
          },
          TransformComponent = {
            transform = {
              position = {
                -455.04730224609,
                48.445201873779,
                523.60131835938
              },
              rotation = {
                -0.42370340228081,
                0.55534034967422,
                0.56512171030045,
                -0.43898746371269
              },
              scale = 0.76319235563278
            }
          }
        },
        relative_transform = {
          position = {
            0.36871337890625,
            -0.12391662597656,
            -0.76434326171875
          },
          rotation = {
            -0.42370340228081,
            0.55534034967422,
            0.56512171030045,
            -0.43898746371269
          },
          scale = 0.76319235563278
        }
      },
      torch_tip = {
        components = {
          ParticlesComponent = {
            max_end_color = {
              w = 1.0,
              x = 0.20000000298023,
              y = 0.0,
              z = 0.0
            },
            max_end_size = 0.2,
            max_particles = 8.0,
            max_rotation_speed = 9.1999998092651,
            max_spawn_pos = {
              x = 0.10000000149012,
              y = 0.10000000149012,
              z = -0.2
            },
            max_start_color = {
              w = 1.0,
              x = 1.0,
              y = 0.3,
              z = 0.0
            },
            max_start_size = 0.5,
            max_start_velocity = {
              z = 0.8
            },
            min_end_color = {
              w = 1.0,
              x = 0.0,
              y = 0.0,
              z = 0.0
            },
            min_end_size = 0.1,
            min_rotation_speed = 7.0,
            min_spawn_pos = {
              x = -0.10000000149012,
              y = -0.10000000149012,
              z = -0.3
            },
            min_start_color = {
              w = 1.0,
              x = 0.8,
              y = 0.0,
              z = 0.0
            },
            min_start_size = 0.4,
            min_start_velocity = {
              z = 0.40000000596046
            },
            size_mode = 1.0,
            spawn_per_second = 5.0,
            velocity_mode = 0.0
          },
          PointLightComponent = {
            color = {
              x = 1.0,
              y = 0.10000001639128
            },
            radius = 130
          },
          TransformComponent = {
            transform = {
              position = {
                -455.78475952148,
                48.693035125732,
                525.13000488281
              },
              rotation = {
                0.0093260360881686,
                -0.0045132432132959,
                0.0057966369204223,
                0.99992954730988
              },
              scale = 0.18929043412209
            }
          }
        },
        relative_transform = {
          position = {
            -0.36874389648438,
            0.12391662597656,
            0.76434326171875
          },
          rotation = {
            0.0093260360881686,
            -0.0045132432132959,
            0.0057966369204223,
            0.99992954730988
          },
          scale = 0.18929043412209
        }
      }
    },
    tree_one_branch = {
      entity1544102004_4579 = {
        components = {
          ModelComponent = {
            asset = "assets/model/branch1.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/branch1.fbx",
              type = "Triangle"
            }
          },
        MaterialComponent = {
          material = 2
        },
          TransformComponent = {
            transform = {
              position = {
                45.019199371338,
                441.42687988281,
                112.61029052734
              },
              rotation = {
                0.7070021033287,
                0.0,
                0.0,
                0.7072114944458
              },
              scale = 10.361478805542
            }
          }
        },
        relative_transform = {
          position = {
            0.72365570068359,
            0.570556640625,
            742.14141845703
          },
          rotation = {
            0.7070021033287,
            0.0,
            0.0,
            0.7072114944458
          },
          scale = 10.361478805542
        }
      },
      tree01 = {
        components = {
          ModelComponent = {
            asset = "assets/model/ltree1.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/ltree1.fbx",
              type = "Triangle"
            }
          },
          MaterialComponent = {
            material = 2
          },
          TransformComponent = {
            transform = {
              position = {
                43.571887969971,
                440.28576660156,
                -1371.6726074219
              },
              rotation = {
                0.70700019598007,
                0.0,
                0.0,
                0.70721334218979
              },
              scale = 4.2433805465698
            }
          }
        },
        relative_transform = {
          position = {
            -0.72365570068359,
            -0.570556640625,
            -742.14147949219
          },
          rotation = {
            0.70700019598007,
            0.0,
            0.0,
            0.70721334218979
          },
          scale = 4.2433805465698
        }
      }
    }
  }
}
