scene = {
  entities = {
    lasse = {
      components = {
        ModelComponent = {
          asset = "assets/model/lasse.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              28.203125,
              120.75,
              155.5
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 38.351409912109
          }
        }
      }
    },
    terrain = {
      components = {
        ModelComponent = {
          asset = "assets/model/cool_terrain.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -79.087135314941,
              -48.859745025635,
              155.58227539063
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 38.351409912109
          }
        }
      }
    },
    tree = {
      components = {
        AttachToEntityComponent = {
          entity_handle = "terrain",
          offset_transform = {
            position = {
              -5.5971431732178,
              6.5077419281006,
              -0.0001264214515686
            },
            rotation = {
              0.00039367514546029,
              4.1159031044513e-11,
              0.70710670948029,
              0.70710676908493
            },
            scale = 1.0000001192093
          }
        },
        ModelComponent = {
          asset = "assets/model/tree1.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -225.6234588623,
              242.25227355957,
              17.093700408936
            },
            rotation = {
              0.00039367488352582,
              -1.9344745805228e-09,
              0.70710670948029,
              0.70710670948029
            },
            scale = 2.3548612594604
          }
        }
      }
    }
  },
  presets = {
    preset1541604664_5636 = {
      entity1541604661_12 = {
        components = {
          ModelComponent = {
            asset = "assets/model/axis.fbx"
          },
          TransformComponent = {
            transform = {
              position = {
                1.6480169296265,
                -2.2261321544647,
                1.060982465744
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            0.82400846481323,
            -1.1130660772324,
            0.53049123287201
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      origin_axis_handle = {
        components = {
          ModelComponent = {
            asset = "assets/model/axis.fbx"
          },
          TransformComponent = {
            transform = {
              position = {
                0.0,
                0.0,
                0.0
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            -0.82400846481323,
            1.1130660772324,
            -0.53049123287201
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      }
    }
  }
}
