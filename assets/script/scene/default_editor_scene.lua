scene = {
  entities = {
    entity1542630891_4409 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -11.651882171631,
              -28.474311828613,
              -235.59747314453
            },
            rotation = {
              0.70660197734833,
              0.0070272251032293,
              0.0062127192504704,
              0.70754903554916
            },
            scale = 3.0
          }
        }
      }
    },
    entity1542809309_5222 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -119.33051300049,
              -193.54183959961,
              -252.52976989746
            },
            rotation = {
              0.70660197734833,
              0.0070272251032293,
              0.0062127192504704,
              0.70754903554916
            },
            scale = 3.0
          }
        }
      }
    },
    entity1542812882_504 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/home_tree1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/home_tree1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -466.65368652344,
              50.639106750488,
              -397.03210449219
            },
            rotation = {
              0.70635366439819,
              -0.020007656887174,
              -0.02085723914206,
              0.70726883411407
            },
            scale = 3.18270611763
          }
        }
      }
    },
    entity1542813170_9617 = {
      components = {
        DirectionalLightComponent = {
          color = {
            x = 1.0,
            y = 1.0,
            z = 1.0
          },
          direction = {
            x = -0.33333000540733,
            y = 0.66666001081467,
            z = -0.46665999293327
          },
          has_shadow = true,
          is_cascade = true
        },
        TransformComponent = {
          transform = {
            position = {
              -466.37347412109,
              41.049415588379,
              518.98083496094
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1542881250_1764 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -459.01940917969,
              38.442504882813,
              135.29518127441
            },
            rotation = {
              -0.16372627019882,
              0.070921003818512,
              -0.45364433526993,
              0.87313842773438
            },
            scale = 1.2383905649185
          }
        }
      }
    },
    entity1542881478_4413 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/tree3.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/tree3.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -267.21585083008,
              115.84502410889,
              -16.71167755127
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 3.6640086174011
          }
        }
      }
    },
    entity1542881667_1254 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -449.58413696289,
              52.253398895264,
              130.31591796875
            },
            rotation = {
              -0.16147597134113,
              0.68614673614502,
              0.15861830115318,
              0.6913526058197
            },
            scale = 1.3973926305771
          }
        }
      }
    },
    entity1543229337_534 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -426.65762329102,
              284.76135253906,
              70.47428894043
            },
            rotation = {
              -0.021732624620199,
              0.015755454078317,
              -0.85493034124374,
              0.51804780960083
            },
            scale = 1.4160530567169
          }
        }
      }
    },
    entity1543229337_9972 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -400.28033447266,
              275.84140014648,
              69.929840087891
            },
            rotation = {
              -0.35064521431923,
              0.61149001121521,
              0.3493926525116,
              0.61729466915131
            },
            scale = 1.0152084827423
          }
        }
      }
    },
    entity1543229405_3867 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -407.93594360352,
              280.73910522461,
              70.020133972168
            },
            rotation = {
              -0.67927289009094,
              0.18830788135529,
              0.68246382474899,
              0.19331727921963
            },
            scale = 1.0151599645615
          }
        }
      }
    },
    entity1543229489_2842 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -412.71664428711,
              273.55136108398,
              69.908187866211
            },
            rotation = {
              -0.61314541101456,
              -0.34774056077003,
              0.61894547939301,
              -0.34646159410477
            },
            scale = 1.0152062177658
          }
        }
      }
    },
    entity1543229489_3063 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -405.10775756836,
              268.58102416992,
              69.998497009277
            },
            rotation = {
              0.19152738153934,
              0.67837154865265,
              -0.19655151665211,
              0.68154001235962
            },
            scale = 1.0151568651199
          }
        }
      }
    },
    entity1543229592_267 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -440.64758300781,
              291.6594543457,
              72.820732116699
            },
            rotation = {
              -0.063641026616096,
              0.053121596574783,
              -0.8462450504303,
              0.52630537748337
            },
            scale = 1.4160526990891
          }
        }
      }
    },
    entity1543229728_186 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -442.68383789063,
              295.14758300781,
              73.275276184082
            },
            rotation = {
              -0.35064521431923,
              0.61149001121521,
              0.34939268231392,
              0.61729466915131
            },
            scale = 1.0152081251144
          }
        }
      }
    },
    entity1543229728_310 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -450.33944702148,
              300.04531860352,
              73.365570068359
            },
            rotation = {
              -0.67927289009094,
              0.18830788135529,
              0.68246382474899,
              0.19331741333008
            },
            scale = 1.0151603221893
          }
        }
      }
    },
    entity1543229728_3911 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -447.51126098633,
              287.88720703125,
              73.343933105469
            },
            rotation = {
              0.19152738153934,
              0.67837154865265,
              -0.19655151665211,
              0.68154001235962
            },
            scale = 1.0151568651199
          }
        }
      }
    },
    entity1543229728_4556 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -455.12014770508,
              292.85754394531,
              73.253623962402
            },
            rotation = {
              -0.61314541101456,
              -0.34774056077003,
              0.61894547939301,
              -0.34646159410477
            },
            scale = 1.0152062177658
          }
        }
      }
    },
    entity1543229998_2506 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -434.22290039063,
              248.65374755859,
              75.981376647949
            },
            rotation = {
              -0.35064518451691,
              0.61149001121521,
              0.3493926525116,
              0.61729466915131
            },
            scale = 0.96939623355865
          }
        }
      }
    },
    entity1543229998_2649 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -446.09805297852,
              246.46705627441,
              75.960693359375
            },
            rotation = {
              -0.61314541101456,
              -0.34774056077003,
              0.61894547939301,
              -0.34646159410477
            },
            scale = 0.96938937902451
          }
        }
      }
    },
    entity1543229998_6169 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -438.83248901367,
              241.72102355957,
              76.046928405762
            },
            rotation = {
              0.19152736663818,
              0.67837136983871,
              -0.19655139744282,
              0.6815402507782
            },
            scale = 0.96934771537781
          }
        }
      }
    },
    entity1543229998_7321 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -441.53302001953,
              253.3304901123,
              76.067573547363
            },
            rotation = {
              -0.67927277088165,
              0.1883080303669,
              0.68246382474899,
              0.19331766664982
            },
            scale = 0.96935057640076
          }
        }
      }
    },
    entity1543233831_1246 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -434.43798828125,
              253.13583374023,
              75.62028503418
            },
            rotation = {
              -0.11866234242916,
              0.065432198345661,
              -0.45119097828865,
              0.88207972049713
            },
            scale = 1.4160608053207
          }
        }
      }
    },
    entity1543233831_3074 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -422.13150024414,
              262.10842895508,
              71.515701293945
            },
            rotation = {
              -0.064365968108177,
              0.054555457085371,
              -0.46627736091614,
              0.88060557842255
            },
            scale = 1.4160598516464
          }
        }
      }
    },
    entity1543233877_5733 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -395.65832519531,
              277.11834716797,
              69.930564880371
            },
            rotation = {
              -0.35064515471458,
              0.61148995161057,
              0.34939277172089,
              0.61729466915131
            },
            scale = 1.77678835392
          }
        }
      }
    },
    entity1543234928_4729 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -400.03656005859,
              276.86849975586,
              69.900421142578
            },
            rotation = {
              0.0,
              0.0,
              0.70442688465118,
              0.70977652072906
            },
            scale = 1.5591369867325
          }
        }
      }
    },
    entity1543235073_7620 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -442.76971435547,
              244.84535217285,
              75.939231872559
            },
            rotation = {
              0.0,
              0.0,
              -0.54937750101089,
              0.83557426929474
            },
            scale = 1.5591408014297
          }
        }
      }
    },
    entity1543235160_1918 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -436.33929443359,
              246.03828430176,
              75.89013671875
            },
            rotation = {
              -7.32456262309e-10,
              -5.9624821746596e-10,
              0.35367080569267,
              0.93536996841431
            },
            scale = 1.5591453313828
          }
        }
      }
    },
    entity1543248015_898 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/branch1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/branch1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -432.65725708008,
              -427.08935546875,
              119.70311737061
            },
            rotation = {
              -0.080621778964996,
              0.00062601384706795,
              0.99363821744919,
              -0.078630700707436
            },
            scale = 7.1409459114075
          }
        }
      }
    },
    entity1543248150_3856 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/branch1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/branch1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -474.55249023438,
              -560.96148681641,
              116.45243835449
            },
            rotation = {
              -0.060829658061266,
              0.052914839237928,
              0.80587267875671,
              0.58657395839691
            },
            scale = 17.304224014282
          }
        }
      }
    },
    entity1543248180_6838 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/branch2.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/branch2.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -581.52557373047,
              -484.02331542969,
              125.02117156982
            },
            rotation = {
              -0.0026537161320448,
              0.080580472946167,
              0.11898331344128,
              0.98961746692657
            },
            scale = 8.5590238571167
          }
        }
      }
    },
    entity1543248223_8075 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/branch2.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/branch2.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -537.42071533203,
              -396.99600219727,
              165.52340698242
            },
            rotation = {
              -0.54939264059067,
              0.38779670000076,
              -0.33793374896049,
              0.65846961736679
            },
            scale = 8.559063911438
          }
        }
      }
    },
    entity1543308509_6574 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 4.0,
          max_spawns = 4.0
        },
        TransformComponent = {
          transform = {
            position = {
              -349.66329956055,
              -100.02683258057,
              575.10772705078
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 8.5312271118164
          }
        }
      }
    },
    entity1543309806_7919 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 1.0,
          max_spawns = 4.0,
          min_dist_to_player = 220.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -396.07650756836,
              276.22802734375,
              70.472801208496
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 0.852519094944
          }
        }
      }
    },
    entity1543309906_1996 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 1.0,
          max_spawns = 4.0,
          min_dist_to_player = 200.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -435.07073974609,
              242.20156860352,
              76.753723144531
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 0.852519094944
          }
        }
      }
    },
    entity1543310228_2650 = {
      components = {
        ParticlesComponent = {
          life_time = 8.0,
          max_end_color = {
            w = 0.10000000149012,
            x = 0.0,
            y = 0.0
          },
          max_end_size = 1.0,
          max_spawn_pos = {
            x = 0.0,
            y = 0.0
          },
          max_start_color = {
            x = 0.30000001192093,
            y = 0.30000001192093,
            z = 0.30000001192093
          },
          max_start_size = 0.0,
          min_end_color = {
            w = 0.10000000149012
          },
          min_end_size = 1.0,
          min_start_color = {
            x = 0.30000001192093,
            y = 0.30000001192093,
            z = 0.30000001192093
          },
          min_start_velocity = {
            x = 3.3000001907349
          },
          size_mode = 1.0,
          velocity_mode = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -401.38543701172,
              273.48852539063,
              77.516578674316
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 0.29256850481033
          }
        }
      }
    },
    entity1543310713_2579 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -122.4909362793,
              -188.61962890625,
              103.97512054443
            },
            rotation = {
              -0.70464819669724,
              0.018495263531804,
              0.70895612239838,
              0.022583549842238
            },
            scale = 0.94335848093033
          }
        }
      }
    },
    entity1543310713_3814 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -115.2067565918,
              -197.8404083252,
              103.99520874023
            },
            rotation = {
              0.015152919106185,
              0.70472848415375,
              -0.019221190363169,
              0.70905482769012
            },
            scale = 0.94338113069534
          }
        }
      }
    },
    entity1543310713_9037 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -111.73336029053,
              -197.69718933105,
              103.72319793701
            },
            rotation = {
              -0.042647421360016,
              0.036331761628389,
              -0.832892537117,
              0.55059170722961
            },
            scale = 1.6832448244095
          }
        }
      }
    },
    entity1543310713_92 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -123.2679901123,
              -197.02932739258,
              104.05902099609
            },
            rotation = {
              -0.5140882730484,
              -0.48226973414421,
              0.5200269818306,
              -0.48239099979401
            },
            scale = 0.9433319568634
          }
        }
      }
    },
    entity1543310713_9396 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -114.50952148438,
              -189.4239654541,
              104.07910919189
            },
            rotation = {
              -0.48470288515091,
              0.51179444789886,
              0.4848521053791,
              0.51773351430893
            },
            scale = 0.94333559274673
          }
        }
      }
    },
    entity1543310713_9952 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -94.551933288574,
              -204.9694519043,
              101.86306762695
            },
            rotation = {
              0.00050115736667067,
              0.00036295101745054,
              -0.84063172340393,
              0.54160684347153
            },
            scale = 1.6832441091537
          }
        }
      }
    },
    entity1543310771_1535 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -29.787063598633,
              -236.35284423828,
              107.49957275391
            },
            rotation = {
              0.49410128593445,
              0.50272768735886,
              -0.50003015995026,
              0.50308907032013
            },
            scale = 1.0411667823792
          }
        }
      }
    },
    entity1543310771_3962 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -20.746175765991,
              -227.28813171387,
              107.4774017334
            },
            rotation = {
              -0.50037944316864,
              0.49647855758667,
              0.50070810317993,
              0.50241500139236
            },
            scale = 1.0411616563797
          }
        }
      }
    },
    entity1543310771_4684 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -21.352903366089,
              -232.65267944336,
              107.36172485352
            },
            rotation = {
              0.0,
              0.0,
              0.46431419253349,
              0.88567054271698
            },
            scale = 1.6746557950974
          }
        }
      }
    },
    entity1543310771_4922 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -26.09783744812,
              -227.47268676758,
              107.30899047852
            },
            rotation = {
              1.6622873910688e-10,
              -9.2971230714056e-10,
              0.98953121900558,
              0.14431908726692
            },
            scale = 1.6746753454208
          }
        }
      }
    },
    entity1543310771_5065 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -44.896965026855,
              -227.01473999023,
              106.74021148682
            },
            rotation = {
              -0.076345920562744,
              -0.054477382451296,
              0.55133938789368,
              0.82899236679077
            },
            scale = 1.6777445077896
          }
        }
      }
    },
    entity1543310771_5229 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -29.684736251831,
              -227.03231811523,
              107.40699005127
            },
            rotation = {
              -0.70481848716736,
              -0.010103991255164,
              0.70928865671158,
              -0.0061934636905789
            },
            scale = 1.0412160158157
          }
        }
      }
    },
    entity1543310771_5701 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -20.937026977539,
              -236.60752868652,
              107.38478851318
            },
            rotation = {
              -0.0067608081735671,
              0.70485579967499,
              0.0028299712575972,
              0.70931273698807
            },
            scale = 1.0411953926086
          }
        }
      }
    },
    entity1543310771_9724 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -61.82067489624,
              -219.77598571777,
              103.28844451904
            },
            rotation = {
              -0.04401832446456,
              -0.0052575054578483,
              0.54313606023788,
              0.83847361803055
            },
            scale = 1.6777526140213
          }
        }
      }
    },
    entity1543310807_2736 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -18.185367584229,
              -239.6342010498,
              107.21820068359
            },
            rotation = {
              -0.0067607634700835,
              0.70485585927963,
              0.0028299016412348,
              0.70931267738342
            },
            scale = 1.6991007328033
          }
        }
      }
    },
    entity1543310807_3618 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -32.460697174072,
              -224.00714111328,
              107.3318939209
            },
            rotation = {
              -0.70481848716736,
              -0.010104553773999,
              0.70928865671158,
              -0.0061946203932166
            },
            scale = 1.699164390564
          }
        }
      }
    },
    entity1543310807_521 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -17.87392616272,
              -224.42610168457,
              107.36933898926
            },
            rotation = {
              -0.50037944316864,
              0.49647906422615,
              0.50070762634277,
              0.50241500139236
            },
            scale = 1.6990550756454
          }
        }
      }
    },
    entity1543310807_7227 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -32.627552032471,
              -239.21862792969,
              107.40548706055
            },
            rotation = {
              0.49410080909729,
              0.50272709131241,
              -0.50003266334534,
              0.5030876994133
            },
            scale = 1.6990735530853
          }
        }
      }
    },
    entity1543310824_6704 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -35.512390136719,
              -242.13333129883,
              107.24645996094
            },
            rotation = {
              0.494100689888,
              0.50272941589355,
              -0.50003135204315,
              0.50308674573898
            },
            scale = 2.3676307201385
          }
        }
      }
    },
    entity1543310824_805 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -15.387519836426,
              -242.71243286133,
              106.9854888916
            },
            rotation = {
              -0.0067607690580189,
              0.70485591888428,
              0.0028298769611865,
              0.70931261777878
            },
            scale = 2.367648601532
          }
        }
      }
    },
    entity1543310824_8463 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -14.953527450562,
              -221.52026367188,
              107.19612884521
            },
            rotation = {
              -0.50038069486618,
              0.49647879600525,
              0.50070738792419,
              0.50241416692734
            },
            scale = 2.3675889968872
          }
        }
      }
    },
    entity1543310824_9676 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -35.27970123291,
              -220.93855285645,
              107.03596496582
            },
            rotation = {
              -0.704818546772,
              -0.010104351677001,
              0.70928865671158,
              -0.0061941966414452
            },
            scale = 2.3677244186401
          }
        }
      }
    },
    entity1543310953_3565 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -113.89178466797,
              -199.49601745605,
              103.87572479248
            },
            rotation = {
              0.015152881853282,
              0.70472854375839,
              -0.01922114379704,
              0.70905476808548
            },
            scale = 1.2820665836334
          }
        }
      }
    },
    entity1543310953_3795 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -124.84712982178,
              -198.39376831055,
              103.96242523193
            },
            rotation = {
              -0.5140882730484,
              -0.48226973414421,
              0.5200269818306,
              -0.48239099979401
            },
            scale = 1.2819973230362
          }
        }
      }
    },
    entity1543310953_9917 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -123.79111480713,
              -186.96490478516,
              103.84838104248
            },
            rotation = {
              -0.70464819669724,
              0.018495190888643,
              0.70895612239838,
              0.022583499550819
            },
            scale = 1.2820292711258
          }
        }
      }
    },
    entity1543310953_9979 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -112.94424438477,
              -188.05801391602,
              103.98973083496
            },
            rotation = {
              -0.48470270633698,
              0.5117946267128,
              0.48485255241394,
              0.51773309707642
            },
            scale = 1.2820047140121
          }
        }
      }
    },
    entity1543310988_9115 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -29.621831893921,
              -232.45854187012,
              107.66729736328
            },
            rotation = {
              -3.1936495115126e-10,
              2.0384447463329e-09,
              -0.74868434667587,
              0.66292661428452
            },
            scale = 1.6746870279312
          }
        }
      }
    },
    entity1543311006_8722 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -29.102432250977,
              -234.95837402344,
              123.22793579102
            },
            rotation = {
              -9.5229013563625e-10,
              1.880392952458e-09,
              -0.6265315413475,
              0.77939605712891
            },
            scale = 1.6746908426285
          }
        }
      }
    },
    entity1543311021_8928 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -30.472726821899,
              -234.53840637207,
              123.26852416992
            },
            rotation = {
              -0.57739549875259,
              -0.40433385968208,
              0.58329147100449,
              -0.40360829234123
            },
            scale = 1.1253426074982
          }
        }
      }
    },
    entity1543311034_6009 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -31.154941558838,
              -234.62637329102,
              123.03016662598
            },
            rotation = {
              -0.57739543914795,
              -0.40433397889137,
              0.58329147100449,
              -0.40360820293427
            },
            scale = 1.3646734952927
          }
        }
      }
    },
    entity1543311126_1776 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 1.0,
          max_spawns = 4.0,
          min_dist_to_player = 300.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -17.695795059204,
              -235.8173828125,
              108.1322479248
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 0.852519094944
          }
        }
      }
    },
    entity1543322274_6211 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_boss_spawn.fbx"
        },
        NameComponent = {
          name = "boss_golem_spawn"
        },
        TransformComponent = {
          transform = {
            position = {
              -509.826171875,
              -467.52731323242,
              227.36680603027
            },
            rotation = {
              0.0,
              0.70710688829422,
              0.70710664987564,
              0.0
            },
            scale = 28.051485061646
          }
        }
      }
    },
    entity1543322488_9460 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 5.0,
          max_spawns = 2.0
        },
        TransformComponent = {
          transform = {
            position = {
              -458.48904418945,
              -583.67547607422,
              708.15625
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 8.5312271118164
          }
        }
      }
    },
    entity1543323308_2753 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_quest_item.fbx"
        },
        NameComponent = {
          name = "horn_spawn"
        },
        TransformComponent = {
          transform = {
            position = {
              -395.25036621094,
              283.90734863281,
              70.705894470215
            },
            rotation = {
              0.0,
              0.70710688829422,
              0.70710664987564,
              0.0
            },
            scale = 2.7955737113953
          }
        }
      }
    },
    entity1543323400_6173 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_quest_item.fbx"
        },
        NameComponent = {
          name = "horn_spawn"
        },
        TransformComponent = {
          transform = {
            position = {
              -10.188965797424,
              -238.32562255859,
              108.35067749023
            },
            rotation = {
              0.0,
              0.70710688829422,
              0.70710664987564,
              0.0
            },
            scale = 2.7955737113953
          }
        }
      }
    },
    entity1543323466_5889 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 1.0,
          max_spawns = 4.0,
          min_dist_to_player = 300.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -34.629989624023,
              -231.83967590332,
              107.98442077637
            },
            rotation = {
              -1.6970658212045e-10,
              -2.139815963309e-10,
              -0.6213880777359,
              0.78350293636322
            },
            scale = 0.85251951217651
          }
        }
      }
    },
    entity1543323549_8958 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 1.0,
          max_spawns = 4.0,
          min_dist_to_player = 200.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -446.57189941406,
              244.09214782715,
              76.403213500977
            },
            rotation = {
              0.0,
              0.0,
              -0.31382685899734,
              0.94948023557663
            },
            scale = 0.85251939296722
          }
        }
      }
    },
    entity1543324088_2464 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -80.237564086914,
              -315.73489379883,
              103.80033111572
            },
            rotation = {
              -0.53884243965149,
              -0.45860579609871,
              0.54337531328201,
              -0.45174419879913
            },
            scale = 0.94335436820984
          }
        }
      }
    },
    entity1543324088_2897 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -66.550849914551,
              -300.27163696289,
              103.58784484863
            },
            rotation = {
              -0.00046807885519229,
              -0.0090077258646488,
              -0.25183010101318,
              0.96772944927216
            },
            scale = 1.8094037771225
          }
        }
      }
    },
    entity1543324088_2903 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -66.792449951172,
              -307.392578125,
              103.69984436035
            },
            rotation = {
              -0.45445263385773,
              0.53527367115021,
              0.46096909046173,
              0.54264390468597
            },
            scale = 1.5956063270569
          }
        }
      }
    },
    entity1543324088_4926 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -69.706367492676,
              -320.78286743164,
              103.70716094971
            },
            rotation = {
              0.06545627862215,
              0.70399212837219,
              -0.064113773405552,
              0.70427268743515
            },
            scale = 1.5955234766006
          }
        }
      }
    },
    entity1543324088_5466 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -83.688758850098,
              -317.85797119141,
              103.6538848877
            },
            rotation = {
              -0.538842856884,
              -0.4586056470871,
              0.54337567090988,
              -0.45174345374107
            },
            scale = 1.595560669899
          }
        }
      }
    },
    entity1543324088_5482 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -80.80020904541,
              -304.60284423828,
              103.92936706543
            },
            rotation = {
              -0.70050317049026,
              0.056415405124426,
              0.70830482244492,
              0.066459253430367
            },
            scale = 1.5955202579498
          }
        }
      }
    },
    entity1543324088_5671 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -70.247764587402,
              -309.54721069336,
              103.82749176025
            },
            rotation = {
              -0.45445245504379,
              0.53527373075485,
              0.46096959710121,
              0.54264354705811
            },
            scale = 0.94338256120682
          }
        }
      }
    },
    entity1543324088_6211 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -38.091480255127,
              -250.63719177246,
              107.12175750732
            },
            rotation = {
              -0.032945014536381,
              -0.091280594468117,
              0.96206659078598,
              0.25497123599052
            },
            scale = 1.7795244455338
          }
        }
      }
    },
    entity1543324088_6881 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -56.71558380127,
              -282.86114501953,
              103.62437438965
            },
            rotation = {
              0.0036050854250789,
              0.0039538498967886,
              -0.26252019405365,
              0.96491169929504
            },
            scale = 1.8097130060196
          }
        }
      }
    },
    entity1543324088_7123 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -78.529754638672,
              -307.89779663086,
              103.96318817139
            },
            rotation = {
              -0.70050370693207,
              0.056414972990751,
              0.7083044052124,
              0.066458933055401
            },
            scale = 0.94333618879318
          }
        }
      }
    },
    entity1543324088_9520 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -71.970603942871,
              -317.46411132813,
              103.83184051514
            },
            rotation = {
              0.065456531941891,
              0.70399183034897,
              -0.064113900065422,
              0.70427292585373
            },
            scale = 0.94333219528198
          }
        }
      }
    },
    entity1543324139_6697 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 1.0,
          max_spawns = 4.0,
          min_dist_to_player = 300.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -68.968955993652,
              -317.39266967773,
              103.78679656982
            },
            rotation = {
              0.0,
              0.0,
              -0.042395390570164,
              0.99910092353821
            },
            scale = 0.85251933336258
          }
        }
      }
    },
    entity1543324139_7651 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -70.940498352051,
              -313.80444335938,
              103.64293670654
            },
            rotation = {
              0.0,
              0.0,
              0.42634817957878,
              0.90455913543701
            },
            scale = 1.6746557950974
          }
        }
      }
    },
    entity1543324160_4457 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -77.62052154541,
              -315.5549621582,
              103.74055480957
            },
            rotation = {
              -1.6804060365416e-09,
              5.1866322348104e-10,
              -0.58195227384567,
              0.81322294473648
            },
            scale = 1.674656867981
          }
        }
      }
    },
    entity1543324160_6233 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 1.0,
          max_spawns = 4.0,
          min_dist_to_player = 300.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -81.99617767334,
              -316.2356262207,
              104.23048400879
            },
            rotation = {
              -1.852569627291e-24,
              -3.645777704876e-17,
              0.89300888776779,
              -0.45003899931908
            },
            scale = 0.85251933336258
          }
        }
      }
    },
    entity1543324503_2384 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1031.3845214844,
              -269.97756958008,
              123.03231048584
            },
            rotation = {
              -0.70056968927383,
              0.077931843698025,
              0.70451694726944,
              0.082369171082973
            },
            scale = 1.2820301055908
          }
        }
      }
    },
    entity1543324503_4908 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1021.1655273438,
              -282.79382324219,
              122.7250213623
            },
            rotation = {
              0.030124589800835,
              -0.024479124695063,
              0.87721019983292,
              -0.47853481769562
            },
            scale = 1.6832423210144
          }
        }
      }
    },
    entity1543324503_5593 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1030.3811035156,
              -271.8274230957,
              123.15904998779
            },
            rotation = {
              -0.70056962966919,
              0.077932335436344,
              0.70451676845551,
              0.082370042800903
            },
            scale = 0.94332867860794
          }
        }
      }
    },
    entity1543324503_596 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1020.8749389648,
              -272.88040161133,
              123.17366027832
            },
            rotation = {
              -0.43975389003754,
              0.5508970618248,
              0.43940165638924,
              0.55682593584061
            },
            scale = 1.2820080518723
          }
        }
      }
    },
    entity1543324503_6320 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1005.4039306641,
              -292.8791809082,
              121.53494262695
            },
            rotation = {
              -0.010023092851043,
              0.01490933727473,
              0.88318687677383,
              -0.46867710351944
            },
            scale = 1.6832385063171
          }
        }
      }
    },
    entity1543324503_7471 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -975.69323730469,
              -312.94174194336,
              124.2455368042
            },
            rotation = {
              -0.059534642845392,
              -0.010970600880682,
              0.46969872713089,
              0.8807487487793
            },
            scale = 1.6777527332306
          }
        }
      }
    },
    entity1543324503_7493 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1034.3481445313,
              -281.06579589844,
              123.14635467529
            },
            rotation = {
              -0.55297666788101,
              -0.43713581562042,
              0.55890440940857,
              -0.4367550611496
            },
            scale = 1.2819981575012
          }
        }
      }
    },
    entity1543324503_7560 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -960.34521484375,
              -322.85513305664,
              128.35485839844
            },
            rotation = {
              -0.09595488011837,
              -0.057062916457653,
              0.47852402925491,
              0.87094849348068
            },
            scale = 1.6777449846268
          }
        }
      }
    },
    entity1543324503_8569 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1022.6480102539,
              -273.96340942383,
              123.26303863525
            },
            rotation = {
              -0.43975409865379,
              0.55089616775513,
              0.43940144777298,
              0.55682682991028
            },
            scale = 0.94333612918854
          }
        }
      }
    },
    entity1543324503_8937 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1023.7336425781,
              -283.99594116211,
              123.05965423584
            },
            rotation = {
              0.074608288705349,
              0.70093077421188,
              -0.079027250409126,
              0.70490026473999
            },
            scale = 1.2820664644241
          }
        }
      }
    },
    entity1543324503_9219 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1024.7521972656,
              -282.142578125,
              123.17913818359
            },
            rotation = {
              0.074608311057091,
              0.70093238353729,
              -0.07902742177248,
              0.70489859580994
            },
            scale = 0.94338136911392
          }
        }
      }
    },
    entity1543324503_927 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1032.5625,
              -279.98651123047,
              123.24295043945
            },
            rotation = {
              -0.55297619104385,
              -0.43713584542274,
              0.55890446901321,
              -0.43675541877747
            },
            scale = 0.94333183765411
          }
        }
      }
    },
    entity1543324565_1996 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -936.34362792969,
              -345.43716430664,
              133.13746643066
            },
            rotation = {
              -0.55297619104385,
              -0.43713584542274,
              0.55890446901321,
              -0.43675541877747
            },
            scale = 0.94333183765411
          }
        }
      }
    },
    entity1543324565_2557 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -938.12921142578,
              -346.51644897461,
              133.04086303711
            },
            rotation = {
              -0.55297666788101,
              -0.43713581562042,
              0.55890440940857,
              -0.4367550611496
            },
            scale = 1.2819981575012
          }
        }
      }
    },
    entity1543324565_512 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -927.51483154297,
              -349.44659423828,
              132.95416259766
            },
            rotation = {
              0.074608288705349,
              0.70093077421188,
              -0.079027250409126,
              0.70490026473999
            },
            scale = 1.2820664644241
          }
        }
      }
    },
    entity1543324565_5221 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -926.42919921875,
              -339.4140625,
              133.15756225586
            },
            rotation = {
              -0.43975409865379,
              0.55089616775513,
              0.43940144777298,
              0.55682682991028
            },
            scale = 0.94333612918854
          }
        }
      }
    },
    entity1543324565_5528 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -928.53332519531,
              -347.59323120117,
              133.0736541748
            },
            rotation = {
              0.074608311057091,
              0.70093238353729,
              -0.07902742177248,
              0.70489859580994
            },
            scale = 0.94338136911392
          }
        }
      }
    },
    entity1543324565_7903 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -924.65612792969,
              -338.3310546875,
              133.06817626953
            },
            rotation = {
              -0.43975389003754,
              0.5508970618248,
              0.43940165638924,
              0.55682593584061
            },
            scale = 1.2820080518723
          }
        }
      }
    },
    entity1543324565_7919 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -934.16223144531,
              -337.27807617188,
              133.05355834961
            },
            rotation = {
              -0.70056962966919,
              0.077932335436344,
              0.70451676845551,
              0.082370042800903
            },
            scale = 0.94332867860794
          }
        }
      }
    },
    entity1543324565_9098 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -935.16564941406,
              -335.42822265625,
              132.92683410645
            },
            rotation = {
              -0.70056968927383,
              0.077931843698025,
              0.70451694726944,
              0.082369171082973
            },
            scale = 1.2820301055908
          }
        }
      }
    },
    entity1543324625_3054 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -946.86669921875,
              -351.76678466797,
              132.59127807617
            },
            rotation = {
              -0.55297642946243,
              -0.43713554739952,
              0.55890446901321,
              -0.43675553798676
            },
            scale = 2.9342777729034
          }
        }
      }
    },
    entity1543324625_3949 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -922.57183837891,
              -358.47338867188,
              132.39283752441
            },
            rotation = {
              0.074608251452446,
              0.70092910528183,
              -0.079027064144611,
              0.70490193367004
            },
            scale = 2.9344470500946
          }
        }
      }
    },
    entity1543324625_4818 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -924.9033203125,
              -354.23141479492,
              132.66644287109
            },
            rotation = {
              0.074608042836189,
              0.70093125104904,
              -0.079027108848095,
              0.70489978790283
            },
            scale = 2.1592376232147
          }
        }
      }
    },
    entity1543324625_525 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -920.08715820313,
              -335.51089477539,
              132.85835266113
            },
            rotation = {
              -0.43975409865379,
              0.55089616775513,
              0.43940144777298,
              0.55682682991028
            },
            scale = 2.1591408252716
          }
        }
      }
    },
    entity1543324625_6519 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -916.02874755859,
              -333.03161621094,
              132.65377807617
            },
            rotation = {
              -0.43975406885147,
              0.55089676380157,
              0.43940165638924,
              0.55682611465454
            },
            scale = 2.9343044757843
          }
        }
      }
    },
    entity1543324625_7969 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -937.78692626953,
              -330.62164306641,
              132.62034606934
            },
            rotation = {
              -0.70056962966919,
              0.077932313084602,
              0.70451676845551,
              0.082370020449162
            },
            scale = 2.1591243743896
          }
        }
      }
    },
    entity1543324625_8755 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -940.08361816406,
              -326.38766479492,
              132.33036804199
            },
            rotation = {
              -0.70056968927383,
              0.077931776642799,
              0.70451694726944,
              0.082369022071362
            },
            scale = 2.9343528747559
          }
        }
      }
    },
    entity1543324625_9141 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -942.77978515625,
              -349.29635620117,
              132.81251525879
            },
            rotation = {
              -0.55297619104385,
              -0.43713584542274,
              0.55890446901321,
              -0.43675553798676
            },
            scale = 2.1591258049011
          }
        }
      }
    },
    entity1543324671_8 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -947.96044921875,
              -329.92514038086,
              131.86805725098
            },
            rotation = {
              -0.10485926270485,
              -0.057583048939705,
              0.49632930755615,
              0.85985225439072
            },
            scale = 1.6777496337891
          }
        }
      }
    },
    entity1543324796_1547 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_quest_item.fbx"
        },
        NameComponent = {
          name = "horn_spawn"
        },
        TransformComponent = {
          transform = {
            position = {
              -1025.3714599609,
              -284.62390136719,
              123.55369567871
            },
            rotation = {
              0.0,
              0.70710688829422,
              0.70710664987564,
              0.0
            },
            scale = 2.7955737113953
          }
        }
      }
    },
    entity1543324796_2197 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 5.0,
          max_spawns = 4.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -939.94158935547,
              -343.58197021484,
              133.40827941895
            },
            rotation = {
              -1.6970658212045e-10,
              -2.139815963309e-10,
              -0.6213880777359,
              0.78350293636322
            },
            scale = 0.85251951217651
          }
        }
      }
    },
    entity1543324796_7685 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -935.68951416016,
              -343.54266357422,
              132.99057006836
            },
            rotation = {
              -3.1936603361871e-10,
              2.0384454124667e-09,
              -0.74868434667587,
              0.66292661428452
            },
            scale = 1.3684666156769
          }
        }
      }
    },
    entity1543324877_4362 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 5.0,
          max_spawns = 4.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -919.37091064453,
              -334.11480712891,
              133.58264160156
            },
            rotation = {
              0.0,
              0.0,
              0.94555282592773,
              0.32546862959862
            },
            scale = 0.85252046585083
          }
        }
      }
    },
    entity1543324877_9816 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -924.71075439453,
              -336.39987182617,
              133.30401611328
            },
            rotation = {
              -1.7946605312957e-09,
              -8.9422463878108e-10,
              0.86755496263504,
              0.49734133481979
            },
            scale = 1.2148249149323
          }
        }
      }
    },
    entity1543324892_2952 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -923.62847900391,
              -340.4010925293,
              133.38241577148
            },
            rotation = {
              -1.9422836672334e-09,
              1.1617686812571e-09,
              0.20150189101696,
              0.97948813438416
            },
            scale = 1.214831829071
          }
        }
      }
    },
    entity1543324892_6355 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 5.0,
          max_spawns = 4.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -922.13568115234,
              -343.46044921875,
              133.54627990723
            },
            rotation = {
              3.5671607334642e-10,
              -1.4808460013782e-10,
              0.38195851445198,
              0.92417949438095
            },
            scale = 0.85252374410629
          }
        }
      }
    },
    entity1543324913_4769 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 5.0,
          max_spawns = 4.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -929.72186279297,
              -333.37902832031,
              133.42149353027
            },
            rotation = {
              5.1480369966939e-10,
              1.8255767730846e-10,
              0.94249391555786,
              -0.33422335982323
            },
            scale = 0.85252463817596
          }
        }
      }
    },
    entity1543324913_8743 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -927.85961914063,
              -338.66189575195,
              133.54080200195
            },
            rotation = {
              -2.4496842332411e-09,
              -5.1158272063034e-10,
              0.97888231277466,
              -0.20442463457584
            },
            scale = 1.2148082256317
          }
        }
      }
    },
    entity1543326644_7969 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              648.12530517578,
              -96.869277954102,
              -451.31442260742
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.7125681638718
          }
        }
      }
    },
    entity1543327524_2080 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 5.0,
          max_spawns = 1.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -1034.9853515625,
              -283.37396240234,
              123.46485900879
            },
            rotation = {
              -9.5458314863794e-11,
              -2.5588303698143e-10,
              -0.34952467679977,
              0.93692719936371
            },
            scale = 0.85251969099045
          }
        }
      }
    },
    entity1543327524_2693 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1031.5650634766,
              -280.84774780273,
              123.0471496582
            },
            rotation = {
              -9.3234187037439e-10,
              1.8406470791987e-09,
              -0.5077977180481,
              0.86147636175156
            },
            scale = 1.3684678077698
          }
        }
      }
    },
    entity1543327578_8447 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 5.0,
          max_spawns = 1.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -1029.7200927734,
              -268.44198608398,
              123.72760772705
            },
            rotation = {
              3.0616970070341e-10,
              -2.3544868832381e-10,
              0.99158275127411,
              -0.12947443127632
            },
            scale = 0.85252219438553
          }
        }
      }
    },
    entity1543327578_8876 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1029.5522460938,
              -272.69104003906,
              123.30989837646
            },
            rotation = {
              -3.6858414098617e-09,
              -2.3861137510295e-09,
              0.99895489215851,
              0.045707106590271
            },
            scale = 1.368470788002
          }
        }
      }
    },
    entity1543328352_5591 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              553.447265625,
              -53.499031066895,
              -425.06069946289
            },
            rotation = {
              -0.0022466157097369,
              -0.0050570089370012,
              0.0071957395412028,
              0.99995881319046
            },
            scale = 1.4147193431854
          }
        }
      }
    },
    entity1543328363_1430 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              634.80938720703,
              -315.54205322266,
              -510.71719360352
            },
            rotation = {
              -4.2227017438279e-09,
              1.6430129479161e-09,
              -0.42554852366447,
              0.90493559837341
            },
            scale = 1.7125766277313
          }
        }
      }
    },
    entity1543328363_645 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              607.82562255859,
              -214.96012878418,
              -484.46328735352
            },
            rotation = {
              -0.0041849906556308,
              -0.0036201546899974,
              -0.41901949048042,
              0.90796041488647
            },
            scale = 1.4147248268127
          }
        }
      }
    },
    entity1543328512_6462 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              386.19250488281,
              -641.42156982422,
              -427.56436157227
            },
            rotation = {
              0.0,
              0.0,
              0.79779988527298,
              0.60292232036591
            },
            scale = 1.7125755548477
          }
        }
      }
    },
    entity1543328512_6720 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              370.31344604492,
              -744.3427734375,
              -401.31063842773
            },
            rotation = {
              0.0026800027117133,
              -0.004841435700655,
              0.80210518836975,
              0.5971571803093
            },
            scale = 1.4147194623947
          }
        }
      }
    },
    entity1543328512_9311 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              600.19580078125,
              -594.54022216797,
              -486.96713256836
            },
            rotation = {
              -3.8567495863617e-09,
              -2.3782606994871e-09,
              0.46538522839546,
              0.88510823249817
            },
            scale = 1.7125794887543
          }
        }
      }
    },
    entity1543328512_9747 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              510.79937744141,
              -647.95544433594,
              -460.71322631836
            },
            rotation = {
              0.00036493697552942,
              -0.0055214460007846,
              0.47173434495926,
              0.88172340393066
            },
            scale = 1.4147288799286
          }
        }
      }
    },
    entity1543328532_1755 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              798.27392578125,
              -245.88725280762,
              -294.96008300781
            },
            rotation = {
              -0.0022466965019703,
              -0.0050572734326124,
              0.0071959435008466,
              0.99995881319046
            },
            scale = 1.1151907444
          }
        }
      }
    },
    entity1543328532_3048 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              764.65472412109,
              -714.49237060547,
              -323.06475830078
            },
            rotation = {
              0.00036494137020782,
              -0.0055214380845428,
              0.47173446416855,
              0.88172328472137
            },
            scale = 1.1152182817459
          }
        }
      }
    },
    entity1543328532_4680 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              841.14056396484,
              -373.16564941406,
              -341.78707885742
            },
            rotation = {
              -0.0041849785484374,
              -0.0036201472394168,
              -0.41901931166649,
              0.90796047449112
            },
            scale = 1.1152166128159
          }
        }
      }
    },
    entity1543328532_6521 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              653.91137695313,
              -790.47332763672,
              -276.23831176758
            },
            rotation = {
              0.0026800327468663,
              -0.0048413495533168,
              0.80210548639297,
              0.59715676307678
            },
            scale = 1.1152147054672
          }
        }
      }
    },
    entity1543328532_7109 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              835.12615966797,
              -672.38568115234,
              -343.76089477539
            },
            rotation = {
              -3.8567482540941e-09,
              -2.3782580349518e-09,
              0.46538546681404,
              0.88510811328888
            },
            scale = 1.3500126600266
          }
        }
      }
    },
    entity1543328532_8652 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              872.90856933594,
              -280.07565307617,
              -315.65579223633
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.3500027656555
          }
        }
      }
    },
    entity1543328532_9339 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              862.41168212891,
              -452.4533996582,
              -362.48284912109
            },
            rotation = {
              -4.2227017438279e-09,
              1.6430131699607e-09,
              -0.42554861307144,
              0.90493559837341
            },
            scale = 1.3500102758408
          }
        }
      }
    },
    entity1543328532_9717 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              666.42864990234,
              -709.34173583984,
              -296.93374633789
            },
            rotation = {
              0.0,
              0.0,
              0.79780024290085,
              0.60292184352875
            },
            scale = 1.3500099182129
          }
        }
      }
    },
    entity1543328553_1580 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -175.85823059082,
              -1308.1727294922,
              -417.08654785156
            },
            rotation = {
              -8.0699953386443e-09,
              -9.9074928816556e-11,
              0.54935389757156,
              0.83558976650238
            },
            scale = 2.1957728862762
          }
        }
      }
    },
    entity1543328553_1606 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              19.621461868286,
              -1407.0860595703,
              -476.48983764648
            },
            rotation = {
              -4.4606323079677e-09,
              4.6087630933833e-10,
              0.14154656231403,
              0.98993164300919
            },
            scale = 1.7125849723816
          }
        }
      }
    },
    entity1543328553_1876 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              371.28094482422,
              -1051.68359375,
              -440.83703613281
            },
            rotation = {
              -5.5577293878173e-09,
              -2.639262586257e-09,
              -0.33541592955589,
              0.94207012653351
            },
            scale = 1.9936231374741
          }
        }
      }
    },
    entity1543328553_2915 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              325.314453125,
              -958.23803710938,
              -414.58340454102
            },
            rotation = {
              -0.0038127182051539,
              -0.0040105567313731,
              -0.32862341403961,
              0.944444835186
            },
            scale = 1.4147217273712
          }
        }
      }
    },
    entity1543328553_4844 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1.6002216339111,
              -1537.9146728516,
              -286.45642089844
            },
            rotation = {
              -5.988834761439e-09,
              5.9698437304689e-09,
              0.54935324192047,
              0.83559024333954
            },
            scale = 1.3500146865845
          }
        }
      }
    },
    entity1543328553_4981 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              312.63238525391,
              -1462.6815185547,
              -352.00561523438
            },
            rotation = {
              -1.2211683220187e-08,
              4.3440713248799e-09,
              -0.70442634820938,
              0.70977711677551
            },
            scale = 1.3500133752823
          }
        }
      }
    },
    entity1543328553_5882 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              222.76599121094,
              -1212.7390136719,
              -500.23992919922
            },
            rotation = {
              -1.6527513579945e-08,
              -1.9851991162056e-09,
              -0.70442676544189,
              0.709776699543
            },
            scale = 1.7125841379166
          }
        }
      }
    },
    entity1543328553_6259 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              346.25469970703,
              -1387.7928466797,
              -331.30975341797
            },
            rotation = {
              -0.0051567871123552,
              -0.0020067216828465,
              -0.69929248094559,
              0.71481424570084
            },
            scale = 1.1152204275131
          }
        }
      }
    },
    entity1543328553_6349 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -62.573585510254,
              -1592.8801269531,
              -265.76095581055
            },
            rotation = {
              0.00090085039846599,
              -0.005459850654006,
              0.55534595251083,
              0.83160102367401
            },
            scale = 1.1152182817459
          }
        }
      }
    },
    entity1543328553_6648 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              152.49501037598,
              -1615.8856201172,
              -333.28356933594
            },
            rotation = {
              -1.5805285968895e-08,
              3.3150679978888e-09,
              0.14154659211636,
              0.98993158340454
            },
            scale = 1.3500180244446
          }
        }
      }
    },
    entity1543328553_6895 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -83.417434692383,
              -1391.9865722656,
              -450.23593139648
            },
            rotation = {
              -0.00150817388203,
              -0.0053239888511598,
              0.14866288006306,
              0.98887246847153
            },
            scale = 1.4147325754166
          }
        }
      }
    },
    entity1543328553_7632 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              393.46978759766,
              -1262.0612792969,
              -284.48275756836
            },
            rotation = {
              -0.0038128350861371,
              -0.0040107183158398,
              -0.32862347364426,
              0.944444835186
            },
            scale = 1.1151903867722
          }
        }
      }
    },
    entity1543328553_779 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              429.70532226563,
              -1335.7250976563,
              -305.17849731445
            },
            rotation = {
              -9.5614165473989e-09,
              1.9396804162852e-09,
              -0.3354152739048,
              0.94207036495209
            },
            scale = 1.350007891655
          }
        }
      }
    },
    entity1543328553_8554 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              265.41864013672,
              -1117.7344970703,
              -473.98602294922
            },
            rotation = {
              -0.0051568006165326,
              -0.0020067163277417,
              -0.69928997755051,
              0.71481674909592
            },
            scale = 1.4147282838821
          }
        }
      }
    },
    entity1543328553_9226 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -253.20790100098,
              -1377.90234375,
              -390.83337402344
            },
            rotation = {
              0.00090088183060288,
              -0.005459853913635,
              0.55534440279007,
              0.83160203695297
            },
            scale = 1.4147243499756
          }
        }
      }
    },
    entity1543328553_9347 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              71.269668579102,
              -1603.9827880859,
              -312.58743286133
            },
            rotation = {
              -0.0015081763267517,
              -0.0053239576518536,
              0.14866359531879,
              0.98887234926224
            },
            scale = 1.115219950676
          }
        }
      }
    },
    entity1543328574_1667 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -932.54913330078,
              -1548.26953125,
              -468.38128662109
            },
            rotation = {
              0.0003270227171015,
              -0.0055238585919142,
              0.46567764878273,
              0.88493710756302
            },
            scale = 1.4147319793701
          }
        }
      }
    },
    entity1543328574_3506 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -463.15222167969,
              -1651.5208740234,
              -411.48223876953
            },
            rotation = {
              -2.2148665124178e-08,
              -1.7880699587636e-08,
              0.96387177705765,
              -0.26636669039726
            },
            scale = 1.7125924825668
          }
        }
      }
    },
    entity1543328574_4198 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -489.2099609375,
              -1529.4930419922,
              -444.63098144531
            },
            rotation = {
              0.0046040611341596,
              -0.003069446189329,
              0.98677045106888,
              0.16202914714813
            },
            scale = 1.4147365093231
          }
        }
      }
    },
    entity1543328574_4533 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -842.42755126953,
              -1496.0837402344,
              -494.63540649414
            },
            rotation = {
              2.1383144144238e-08,
              -7.2174699994321e-08,
              0.45930513739586,
              0.88827854394913
            },
            scale = 1.7125903367996
          }
        }
      }
    },
    entity1543328574_6086 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -549.86273193359,
              -1209.9500732422,
              -327.67831420898
            },
            rotation = {
              -5.2876924172551e-08,
              -4.6899661043653e-08,
              0.98559409379959,
              0.16912798583508
            },
            scale = 1.5372774600983
          }
        }
      }
    },
    entity1543328574_6121 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -388.75653076172,
              -1354.2719726563,
              -260.15576171875
            },
            rotation = {
              0.0054727382957935,
              -0.00081841909559444,
              0.96191442012787,
              -0.27329483628273
            },
            scale = 1.1152174472809
          }
        }
      }
    },
    entity1543328574_6126 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -470.35540771484,
              -1363.2587890625,
              -280.85122680664
            },
            rotation = {
              4.0442269977348e-08,
              -2.2171569469265e-08,
              0.96387177705765,
              -0.26636669039726
            },
            scale = 1.3500229120255
          }
        }
      }
    },
    entity1543328574_6358 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -564.01330566406,
              -1457.0404052734,
              -470.88507080078
            },
            rotation = {
              -2.3604648902165e-08,
              4.1712526765991e-09,
              0.98559409379959,
              0.16912804543972
            },
            scale = 1.7125917673111
          }
        }
      }
    },
    entity1543328574_6390 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -359.63903808594,
              -1640.1209716797,
              -385.22845458984
            },
            rotation = {
              0.0054728267714381,
              -0.00081844558008015,
              0.96191483736038,
              -0.27329328656197
            },
            scale = 1.4147251844406
          }
        }
      }
    },
    entity1543328574_7453 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -938.52362060547,
              -1275.3686523438,
              -299.57321166992
            },
            rotation = {
              3.0925686456129e-09,
              -3.0160947517288e-08,
              0.79364812374115,
              0.60837709903717
            },
            scale = 1.350012421608
          }
        }
      }
    },
    entity1543328574_7554 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -840.37725830078,
              -1281.8670654297,
              -325.70440673828
            },
            rotation = {
              0.00032703913166188,
              -0.0055238367058337,
              0.46567487716675,
              0.88493859767914
            },
            scale = 1.1152215003967
          }
        }
      }
    },
    entity1543328574_7650 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1057.0544433594,
              -1540.0275878906,
              -435.23239135742
            },
            rotation = {
              -1.9744119228449e-08,
              -5.0673527596246e-08,
              0.79364740848541,
              0.60837799310684
            },
            scale = 1.7125800848007
          }
        }
      }
    },
    entity1543328574_8237 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -952.15252685547,
              -1356.3234863281,
              -278.87744140625
            },
            rotation = {
              0.0026468348223716,
              -0.0048598777502775,
              0.79799252748489,
              0.60264194011688
            },
            scale = 1.1151901483536
          }
        }
      }
    },
    entity1543328574_8467 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -490.89489746094,
              -1267.0653076172,
              -306.98226928711
            },
            rotation = {
              0.0046040322631598,
              -0.0030695067252964,
              0.98677057027817,
              0.1620284318924
            },
            scale = 1.1152219772339
          }
        }
      }
    },
    entity1543328574_8521 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1074.3430175781,
              -1642.7219238281,
              -408.97863769531
            },
            rotation = {
              0.0026466883718967,
              -0.0048597431741655,
              0.7979936003685,
              0.60264050960541
            },
            scale = 1.4147237539291
          }
        }
      }
    },
    entity1543328574_9164 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -769.33624267578,
              -1240.7313232422,
              -346.40048217773
            },
            rotation = {
              2.9898333808731e-10,
              -1.8484380248651e-08,
              0.45930552482605,
              0.88827836513519
            },
            scale = 1.3500194549561
          }
        }
      }
    },
    entity1543328594_1291 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1169.5836181641,
              -1366.7406005859,
              -427.24069213867
            },
            rotation = {
              3.7514521267212e-08,
              -4.0866893868952e-08,
              0.92197281122208,
              -0.38725456595421
            },
            scale = 1.7125865221024
          }
        }
      }
    },
    entity1543328594_2433 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1668.9302978516,
              -885.30017089844,
              -252.16410827637
            },
            rotation = {
              -0.0034257210791111,
              -0.0043456628918648,
              -0.23994010686874,
              0.97077190876007
            },
            scale = 1.1152209043503
          }
        }
      }
    },
    entity1543328594_2743 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1225.6977539063,
              -1255.2932128906,
              -460.38955688477
            },
            rotation = {
              0.0049583246000111,
              -0.0024565532803535,
              0.99938207864761,
              0.034711360931396
            },
            scale = 1.4147346019745
          }
        }
      }
    },
    entity1543328594_3461 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1375.0694580078,
              -798.47125244141,
              -403.49063110352
            },
            rotation = {
              -6.2953438018098e-09,
              2.6296236299572e-08,
              -0.24692443013191,
              0.96903473138809
            },
            scale = 1.7125993967056
          }
        }
      }
    },
    entity1543328594_3619 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1316.4090576172,
              -1204.1442871094,
              -486.64492797852
            },
            rotation = {
              7.3355323593205e-08,
              -1.5866854496949e-08,
              0.99912178516388,
              0.041900914162397
            },
            scale = 2.4224696159363
          }
        }
      }
    },
    entity1543328594_3764 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1502.0065917969,
              -1311.1428222656,
              -317.71282958984
            },
            rotation = {
              0.0049583036452532,
              -0.0024565542116761,
              0.99938201904297,
              0.034712933003902
            },
            scale = 1.1152249574661
          }
        }
      }
    },
    entity1543328594_3849 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1619.7570800781,
              -951.03686523438,
              -272.85952758789
            },
            rotation = {
              -4.1412892670678e-08,
              -2.5754134824751e-08,
              -0.246925547719,
              0.96903443336487
            },
            scale = 1.350029706955
          }
        }
      }
    },
    entity1543328594_5090 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1692.2012939453,
              -1017.5709228516,
              -298.99066162109
            },
            rotation = {
              -0.0049492474645376,
              -0.0024746041744947,
              -0.63024133443832,
              0.77637958526611
            },
            scale = 1.1152281761169
          }
        }
      }
    },
    entity1543328594_6385 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1466.9676513672,
              -882.87933349609,
              -436.63931274414
            },
            rotation = {
              -0.0049492125399411,
              -0.0024746409617364,
              -0.63024228811264,
              0.77637875080109
            },
            scale = 1.4147397279739
          }
        }
      }
    },
    entity1543328594_6429 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1437.4464111328,
              -715.08197021484,
              -377.23684692383
            },
            rotation = {
              -0.0034258149098605,
              -0.0043457322753966,
              -0.2399415075779,
              0.97077161073685
            },
            scale = 1.4147303104401
          }
        }
      }
    },
    entity1543328594_6620 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1381.1103515625,
              -1369.6373291016,
              -270.88577270508
            },
            rotation = {
              0.005532774142921,
              -0.00011298704339424,
              0.91914767026901,
              -0.39387425780296
            },
            scale = 1.115185379982
          }
        }
      }
    },
    entity1543328594_7518 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1491.4086914063,
              -984.10974121094,
              -462.89340209961
            },
            rotation = {
              1.8151645519993e-08,
              1.6047717821266e-08,
              -0.63582110404968,
              0.77183645963669
            },
            scale = 1.7126042842865
          }
        }
      }
    },
    entity1543328594_7553 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1711.4694824219,
              -1097.3718261719,
              -319.68667602539
            },
            rotation = {
              -1.5401601771714e-08,
              6.8102046668628e-08,
              -0.63582164049149,
              0.77183604240417
            },
            scale = 1.3500301837921
          }
        }
      }
    },
    entity1543328594_8222 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1457.7742919922,
              -1398.9976806641,
              -291.58157348633
            },
            rotation = {
              2.7947763214797e-08,
              -1.2204789179293e-08,
              0.92197185754776,
              -0.38725689053535
            },
            scale = 1.350021481514
          }
        }
      }
    },
    entity1543328594_8650 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1573.5172119141,
              -1270.8251953125,
              -338.4089050293
            },
            rotation = {
              1.6267348357246e-08,
              -7.2416206364778e-09,
              0.99912172555923,
              0.041902247816324
            },
            scale = 1.3500272035599
          }
        }
      }
    },
    entity1543328594_9663 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1072.3317871094,
              -1329.4945068359,
              -400.9870300293
            },
            rotation = {
              0.0055325278081,
              -0.00011305454972899,
              0.9191467165947,
              -0.39387655258179
            },
            scale = 1.4147320985794
          }
        }
      }
    },
    entity1543328614_1086 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1621.0443115234,
              -509.49575805664,
              -416.96524047852
            },
            rotation = {
              2.7855769246798e-08,
              -4.7973717443028e-08,
              0.98365235328674,
              -0.18007779121399
            },
            scale = 1.7125922441483
          }
        }
      }
    },
    entity1543328614_1138 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1814.2514648438,
              -423.33520507813,
              -260.61032104492
            },
            rotation = {
              0.0053792390972376,
              -0.0012993298005313,
              0.98231583833694,
              -0.18714961409569
            },
            scale = 1.1151776313782
          }
        }
      }
    },
    entity1543328614_2073 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1896.1580810547,
              -417.8039855957,
              -281.30612182617
            },
            rotation = {
              2.4671923171127e-08,
              -1.7925476214486e-08,
              0.98365205526352,
              -0.18007951974869
            },
            scale = 1.3500291109085
          }
        }
      }
    },
    entity1543328614_350 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1517.1408691406,
              -516.51147460938,
              -390.71157836914
            },
            rotation = {
              0.005378954578191,
              -0.0012993558775634,
              0.98231554031372,
              -0.18715113401413
            },
            scale = 1.414736032486
          }
        }
      }
    },
    entity1543328614_3797 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1569.0184326172,
              92.543502807617,
              -393.21517944336
            },
            rotation = {
              -4.9723264394785e-10,
              2.7034573690798e-08,
              -0.44939830899239,
              0.89333146810532
            },
            scale = 1.712605714798
          }
        }
      }
    },
    entity1543328614_3874 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1899.4274902344,
              -319.49716186523,
              -307.43737792969
            },
            rotation = {
              0.0043145446106791,
              -0.0034646836575121,
              0.9685732126236,
              0.24866712093353
            },
            scale = 1.1152256727219
          }
        }
      }
    },
    entity1543328614_3990 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1686.0635986328,
              -300.28479003906,
              -476.36865234375
            },
            rotation = {
              6.823318443594e-08,
              -3.1260711352843e-08,
              0.9667741060257,
              0.25563225150108
            },
            scale = 2.0470447540283
          }
        }
      }
    },
    entity1543328614_4252 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1872.1875,
              137.08486938477,
              -241.88865661621
            },
            rotation = {
              -0.0042795375920832,
              -0.0035079403314739,
              -0.44295138120651,
              0.89652854204178
            },
            scale = 1.1152235269547
          }
        }
      }
    },
    entity1543328614_4443 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1687.8592529297,
              54.505935668945,
              -426.36386108398
            },
            rotation = {
              -0.0053653712384403,
              -0.0013532524462789,
              -0.78235912322998,
              0.62280297279358
            },
            scale = 1.4147392511368
          }
        }
      }
    },
    entity1543328614_5277 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1752.5354003906,
              -27.115631103516,
              -452.61795043945
            },
            rotation = {
              2.1176081332897e-08,
              1.1772020691581e-08,
              -0.78683203458786,
              0.61716723442078
            },
            scale = 1.712611913681
          }
        }
      }
    },
    entity1543328614_537 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1948.8323974609,
              26.798919677734,
              -288.71520996094
            },
            rotation = {
              -0.005365380551666,
              -0.0013532093726099,
              -0.78236079216003,
              0.62280088663101
            },
            scale = 1.1152312755585
          }
        }
      }
    },
    entity1543328614_5989 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1625.1967773438,
              -384.78723144531,
              -450.11410522461
            },
            rotation = {
              0.0043145851232111,
              -0.0034646964631975,
              0.96857368946075,
              0.24866527318954
            },
            scale = 1.4147357940674
          }
        }
      }
    },
    entity1543328614_6145 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1999.8150634766,
              -37.544372558594,
              -309.41122436523
            },
            rotation = {
              -4.0683970081723e-10,
              6.9820451642499e-08,
              -0.78683221340179,
              0.6171669960022
            },
            scale = 1.350035071373
          }
        }
      }
    },
    entity1543328614_6344 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1590.6300048828,
              194.41371154785,
              -366.96139526367
            },
            rotation = {
              -0.0042796698398888,
              -0.0035079882945865,
              -0.44295230507851,
              0.89652812480927
            },
            scale = 1.4147343635559
          }
        }
      }
    },
    entity1543328614_7674 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1855.1499023438,
              56.779113769531,
              -262.58407592773
            },
            rotation = {
              -4.5979682283814e-08,
              -1.6252835521868e-08,
              -0.44939935207367,
              0.89333099126816
            },
            scale = 1.350035905838
          }
        }
      }
    },
    entity1543328614_965 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1947.4114990234,
              -252.88558959961,
              -328.13345336914
            },
            rotation = {
              1.4331041242599e-08,
              -1.0568189878768e-08,
              0.96677416563034,
              0.25563198328018
            },
            scale = 1.3500314950943
          }
        }
      }
    },
    entity1543328632_1800 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1752.5008544922,
              574.47320556641,
              -437.41207885742
            },
            rotation = {
              -0.0021453443914652,
              -0.0051005911082029,
              0.027117796242237,
              0.99961692094803
            },
            scale = 1.4147442579269
          }
        }
      }
    },
    entity1543328632_2347 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1656.1691894531,
              534.90734863281,
              -463.66616821289
            },
            rotation = {
              5.3711612935103e-09,
              3.0701940545441e-08,
              0.019926201552153,
              0.99980145692825
            },
            scale = 1.7126100063324
          }
        }
      }
    },
    entity1543328632_2456 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1412.0610351563,
              674.37683105469,
              -487.41683959961
            },
            rotation = {
              -6.5414724303992e-08,
              -4.1305735720698e-08,
              -0.78562879562378,
              0.61869817972183
            },
            scale = 2.4997069835663
          }
        }
      }
    },
    entity1543328632_3686 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1334.5555419922,
              474.65762329102,
              -318.48571777344
            },
            rotation = {
              -0.0053627854213119,
              -0.001363655901514,
              -0.78114706277847,
              0.62432253360748
            },
            scale = 1.1152323484421
          }
        }
      }
    },
    entity1543328632_3770 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1708.3928222656,
              413.08511352539,
              -273.63241577148
            },
            rotation = {
              -1.4922648006177e-08,
              -4.6060289804473e-08,
              0.44349333643913,
              0.89627766609192
            },
            scale = 1.3500462770462
          }
        }
      }
    },
    entity1543328632_4772 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1347.6997070313,
              756.24786376953,
              -461.16223144531
            },
            rotation = {
              -0.0053628301247954,
              -0.0013636759249493,
              -0.78114539384842,
              0.62432461977005
            },
            scale = 1.4147390127182
          }
        }
      }
    },
    entity1543328632_5116 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1913.8515625,
              629.19012451172,
              -378.00973510742
            },
            rotation = {
              0.00022913274005987,
              -0.0055289384908974,
              0.44992491602898,
              0.8930492401123
            },
            scale = 1.4147369861603
          }
        }
      }
    },
    entity1543328632_5492 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1577.7203369141,
              300.16851806641,
              -320.45953369141
            },
            rotation = {
              -5.284767112812e-08,
              3.780141710763e-08,
              0.019925337284803,
              0.9998015165329
            },
            scale = 1.3500292301178
          }
        }
      }
    },
    entity1543328632_6619 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1653.6602783203,
              331.35510253906,
              -299.76348876953
            },
            rotation = {
              -0.0021453874651343,
              -0.0051005696877837,
              0.027113733813167,
              0.99961704015732
            },
            scale = 1.1152334213257
          }
        }
      }
    },
    entity1543328632_6721 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1251.0189208984,
              896.53521728516,
              -401.75985717773
            },
            rotation = {
              -0.0042728628031909,
              -0.0035162505228072,
              -0.44120380282402,
              0.89738988876343
            },
            scale = 1.414742231369
          }
        }
      }
    },
    entity1543328632_8030 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1385.2874755859,
              410.11416625977,
              -339.18182373047
            },
            rotation = {
              -1.3536430643057e-08,
              -9.0138101427328e-09,
              -0.785629093647,
              0.61869776248932
            },
            scale = 1.3500394821167
          }
        }
      }
    },
    entity1543328632_8374 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1780.8488769531,
              374.48934936523,
              -252.93695068359
            },
            rotation = {
              0.00022918148897588,
              -0.0055287797003984,
              0.44992581009865,
              0.89304876327515
            },
            scale = 1.1152240037918
          }
        }
      }
    },
    entity1543328632_8933 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1229.0091552734,
              794.74737548828,
              -428.01354980469
            },
            rotation = {
              -6.2371974252073e-08,
              1.6629346788477e-08,
              -0.44765907526016,
              0.89420431852341
            },
            scale = 1.7125977277756
          }
        }
      }
    },
    entity1543328632_9342 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1240.9915771484,
              505.0029296875,
              -292.35443115234
            },
            rotation = {
              -2.7079867237489e-08,
              -1.6337564190394e-08,
              -0.44765755534172,
              0.89420509338379
            },
            scale = 1.3500368595123
          }
        }
      }
    },
    entity1543328632_9542 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1821.9383544922,
              678.14654541016,
              -404.26342773438
            },
            rotation = {
              -2.3334143506304e-08,
              1.7728758905378e-08,
              0.44349458813667,
              0.89627707004547
            },
            scale = 1.7126132249832
          }
        }
      }
    },
    entity1543328632_9700 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1258.3406982422,
              585.24224853516,
              -271.65863037109
            },
            rotation = {
              -0.0042729992419481,
              -0.0035165112931281,
              -0.44120433926582,
              0.8973895907402
            },
            scale = 1.1151764392853
          }
        }
      }
    },
    entity1543328653_1885 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1057.3209228516,
              758.05633544922,
              -281.76623535156
            },
            rotation = {
              -0.0023112399503589,
              -0.005027583334595,
              -0.0056265802122653,
              0.99996882677078
            },
            scale = 1.1152338981628
          }
        }
      }
    },
    entity1543328653_209 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1046.5032958984,
              961.33685302734,
              -445.66891479492
            },
            rotation = {
              6.3734630906254e-09,
              3.0509514914456e-08,
              -0.012816644273698,
              0.9999178647995
            },
            scale = 1.7126134634018
          }
        }
      }
    },
    entity1543328653_2409 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -784.36755371094,
              819.08367919922,
              -321.1845703125
            },
            rotation = {
              -1.382421732643e-08,
              -8.5657889670188e-09,
              -0.80546253919601,
              0.59264665842056
            },
            scale = 1.350041270256
          }
        }
      }
    },
    entity1543328653_2870 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -724.20880126953,
              1162.0161132813,
              -443.16497802734
            },
            rotation = {
              -0.0054045836441219,
              -0.0011873659677804,
              -0.80116671323776,
              0.59841561317444
            },
            scale = 1.4147425889969
          }
        }
      }
    },
    entity1543328653_2959 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -634.17169189453,
              904.32659912109,
              -274.35717773438
            },
            rotation = {
              -2.7600169261177e-08,
              -1.5442234158058e-08,
              -0.47669252753258,
              0.87907010316849
            },
            scale = 1.3500401973724
          }
        }
      }
    },
    entity1543328653_4650 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1202.5443115234,
              1115.1179199219,
              -386.26617431641
            },
            rotation = {
              -2.274116894796e-08,
              1.8483156338789e-08,
              0.41391444206238,
              0.91031581163406
            },
            scale = 1.7126172780991
          }
        }
      }
    },
    entity1543328653_4942 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1297.4637451172,
              1072.2807617188,
              -360.01248168945
            },
            rotation = {
              4.7997153160395e-05,
              -0.0055334637872875,
              0.42044696211815,
              0.90730023384094
            },
            scale = 1.414740562439
          }
        }
      }
    },
    entity1543328653_5575 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1140.0401611328,
              1007.1215820313,
              -419.41482543945
            },
            rotation = {
              -0.0023111796472222,
              -0.0050276163965464,
              -0.0056227594614029,
              0.99996888637543
            },
            scale = 1.4147472381592
          }
        }
      }
    },
    entity1543328653_5683 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1181.4146728516,
              809.42169189453,
              -234.93969726563
            },
            rotation = {
              4.8051864723675e-05,
              -0.0055333054624498,
              0.42044904828072,
              0.90729928016663
            },
            scale = 1.1152250766754
          }
        }
      }
    },
    entity1543328653_6018 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -646.23284912109,
              985.52905273438,
              -253.66137695313
            },
            rotation = {
              -0.0043858285062015,
              -0.0033747365232557,
              -0.47034671902657,
              0.88246434926987
            },
            scale = 1.1151764392853
          }
        }
      }
    },
    entity1543328653_6057 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1106.5875244141,
              843.19281005859,
              -255.63516235352
            },
            rotation = {
              -1.642256819423e-08,
              -4.5546840965471e-08,
              0.4139116704464,
              0.9103170633316
            },
            scale = 1.3500500917435
          }
        }
      }
    },
    entity1543328653_6593 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -603.25305175781,
              1192.6650390625,
              -410.01629638672
            },
            rotation = {
              -6.1794018790806e-08,
              1.8662404954739e-08,
              -0.47669476270676,
              0.87906891107559
            },
            scale = 1.712601184845
          }
        }
      }
    },
    entity1543328653_7067 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -729.52093505859,
              880.16888427734,
              -300.48846435547
            },
            rotation = {
              -0.0054045468568802,
              -0.0011873503681272,
              -0.80117148160934,
              0.59840923547745
            },
            scale = 1.115238070488
          }
        }
      }
    },
    entity1543328653_8031 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -793.78924560547,
              1084.5311279297,
              -469.41934204102
            },
            rotation = {
              -6.6731814740706e-08,
              -3.9142086905031e-08,
              -0.80546277761459,
              0.59264636039734
            },
            scale = 2.0178489685059
          }
        }
      }
    },
    entity1543328653_8293 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -618.55499267578,
              1295.6755371094,
              -383.76260375977
            },
            rotation = {
              -0.0043856995180249,
              -0.003374463878572,
              -0.47034713625908,
              0.88246417045593
            },
            scale = 1.4147435426712
          }
        }
      }
    },
    entity1543328653_9011 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -983.58380126953,
              721.96697998047,
              -302.46228027344
            },
            rotation = {
              -5.1581650950538e-08,
              3.951092608645e-08,
              -0.012817430309951,
              0.9999178647995
            },
            scale = 1.350025177002
          }
        }
      }
    },
    entity1543328674_1153 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -21.387336730957,
              1163.23046875,
              -301.00769042969
            },
            rotation = {
              -6.3936539618226e-08,
              -1.1571153812895e-08,
              0.73106306791306,
              0.68230986595154
            },
            scale = 1.3500267267227
          }
        }
      }
    },
    entity1543328674_1942 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -525.90594482422,
              1497.4005126953,
              -408.56170654297
            },
            rotation = {
              -5.5385690700405e-08,
              -3.3154417877768e-08,
              0.32953000068665,
              0.94414508342743
            },
            scale = 1.7126032114029
          }
        }
      }
    },
    entity1543328674_2496 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -340.48754882813,
              817.66436767578,
              -358.5578918457
            },
            rotation = {
              0.0041256775148213,
              -0.0036878802347928,
              0.95408582687378,
              0.29948219656944
            },
            scale = 1.4147317409515
          }
        }
      }
    },
    entity1543328674_3562 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -136.88714599609,
              1352.3825683594,
              -319.72998046875
            },
            rotation = {
              -2.9655577993282e-09,
              -1.5990194057736e-08,
              -0.10357289016247,
              0.99462187290192
            },
            scale = 1.3500425815582
          }
        }
      }
    },
    entity1543328674_5363 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -253.74446105957,
              1077.982421875,
              -444.21432495117
            },
            rotation = {
              -1.8280756464151e-08,
              2.524408060367e-08,
              0.73106378316879,
              0.6823091506958
            },
            scale = 1.7126162052155
          }
        }
      }
    },
    entity1543328674_5720 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -483.96905517578,
              1379.8804931641,
              -441.71038818359
            },
            rotation = {
              -0.0027583036571741,
              -0.0047969943843782,
              -0.096415109932423,
              0.99532580375671
            },
            scale = 1.4147440195084
          }
        }
      }
    },
    entity1543328674_5852 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -315.63568115234,
              1474.1781005859,
              -252.20678710938
            },
            rotation = {
              -0.00045469289761968,
              -0.0055152107961476,
              0.33631259202957,
              0.94173413515091
            },
            scale = 1.1151752471924
          }
        }
      }
    },
    entity1543328674_7200 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -202.87986755371,
              1401.2136230469,
              -299.03387451172
            },
            rotation = {
              -0.0027582859620452,
              -0.0047969524748623,
              -0.096423514187336,
              0.99532502889633
            },
            scale = 1.1152395009995
          }
        }
      }
    },
    entity1543328674_7389 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -89.765106201172,
              958.02282714844,
              -233.48510742188
            },
            rotation = {
              0.0041255885735154,
              -0.00368772004731,
              0.95408642292023,
              0.29948034882545
            },
            scale = 1.1152238845825
          }
        }
      }
    },
    entity1543328674_7461 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -130.45297241211,
              1029.3260498047,
              -254.18057250977
            },
            rotation = {
              2.2642680619356e-08,
              -4.2796209243079e-08,
              0.95192009210587,
              0.30634644627571
            },
            scale = 1.35005235672
          }
        }
      }
    },
    entity1543328674_7829 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -290.48910522461,
              980.53857421875,
              -417.9602355957
            },
            rotation = {
              0.0021640288177878,
              -0.0050926832482219,
              0.73594206571579,
              0.677021920681
            },
            scale = 1.4147489070892
          }
        }
      }
    },
    entity1543328674_7873 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -50.350631713867,
              1086.4136962891,
              -280.31164550781
            },
            rotation = {
              0.0021639172919095,
              -0.0050927512347698,
              0.73593974113464,
              0.67702442407608
            },
            scale = 1.1152331829071
          }
        }
      }
    },
    entity1543328674_8188 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -400.25814819336,
              1317.9301757813,
              -467.96463012695
            },
            rotation = {
              -1.5947437148611e-08,
              -7.57027862619e-08,
              -0.10357301682234,
              0.99462181329727
            },
            scale = 1.7126259803772
          }
        }
      }
    },
    entity1543328674_8252 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -392.09851074219,
              908.11334228516,
              -384.81158447266
            },
            rotation = {
              -2.8974994847886e-08,
              -4.3857188991581e-09,
              0.95192098617554,
              0.30634364485741
            },
            scale = 1.7126189470291
          }
        }
      }
    },
    entity1543328674_9211 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -627.01068115234,
              1472.4367675781,
              -382.30801391602
            },
            rotation = {
              -0.00045479933032766,
              -0.0055149351246655,
              0.33631247282028,
              0.9417342543602
            },
            scale = 1.4147441387177
          }
        }
      }
    },
    entity1543328674_9706 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -235.93557739258,
              1493.8551025391,
              -272.90258789063
            },
            rotation = {
              -7.1483614583201e-09,
              -3.0808006812322e-08,
              0.32953217625618,
              0.94414436817169
            },
            scale = 1.3500417470932
          }
        }
      }
    },
    entity1543328686_1161 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              230.47868347168,
              833.91668701172,
              -253.20185852051
            },
            rotation = {
              -6.3507030745313e-08,
              -1.8776367127771e-08,
              0.28352501988411,
              0.95896488428116
            },
            scale = 1.3500566482544
          }
        }
      }
    },
    entity1543328686_1287 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              149.24917602539,
              822.02252197266,
              -232.50640869141
            },
            rotation = {
              -0.00072059774538502,
              -0.0054863025434315,
              0.29041841626167,
              0.95688372850418
            },
            scale = 1.1152292490005
          }
        }
      }
    },
    entity1543328686_1459 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              701.4990234375,
              762.79315185547,
              -271.92395019531
            },
            rotation = {
              -7.4858789389509e-08,
              -2.7039744665558e-08,
              -0.59411692619324,
              0.80437868833542
            },
            scale = 1.3500469923019
          }
        }
      }
    },
    entity1543328686_1669 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              685.78771972656,
              1035.30859375,
              -440.7317199707
            },
            rotation = {
              0.0055172005668283,
              0.00042564730392769,
              0.87648618221283,
              -0.48139524459839
            },
            scale = 1.414747595787
          }
        }
      }
    },
    entity1543328686_174 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              109.95208740234,
              1106.6606445313,
              -357.57925415039
            },
            rotation = {
              -0.00072073825867847,
              -0.0054865446873009,
              0.29041585326195,
              0.95688456296921
            },
            scale = 1.4147303104401
          }
        }
      }
    },
    entity1543328686_3263 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              712.22924804688,
              844.18371582031,
              -251.22814941406
            },
            rotation = {
              -0.0048119146376848,
              -0.0027331789024174,
              -0.58830446004868,
              0.80862057209015
            },
            scale = 1.1151765584946
          }
        }
      }
    },
    entity1543328686_4093 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              533.65087890625,
              722.13537597656,
              -318.7512512207
            },
            rotation = {
              2.8361757387074e-08,
              4.0399761758181e-08,
              0.87993901968002,
              -0.47508662939072
            },
            scale = 1.3500490188599
          }
        }
      }
    },
    entity1543328686_5105 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              597.58239746094,
              979.94226074219,
              -466.98599243164
            },
            rotation = {
              1.1131919563923e-07,
              5.0214293167983e-08,
              0.87993931770325,
              -0.47508609294891
            },
            scale = 1.7126343250275
          }
        }
      }
    },
    entity1543328686_5217 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              810.50842285156,
              1031.5170898438,
              -407.58312988281
            },
            rotation = {
              -8.3666101602375e-08,
              1.852907338673e-08,
              -0.5941196680069,
              0.8043766617775
            },
            scale = 1.7126116752625
          }
        }
      }
    },
    entity1543328686_5357 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              254.43606567383,
              738.51428222656,
              -279.33294677734
            },
            rotation = {
              -0.0029868364799768,
              -0.0046580168418586,
              -0.14439570903778,
              0.98950457572937
            },
            scale = 1.1152336597443
          }
        }
      }
    },
    entity1543328686_5580 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              320.73370361328,
              930.98559570313,
              -443.23568725586
            },
            rotation = {
              -4.9624556908157e-08,
              2.0809967082869e-08,
              -0.15150953829288,
              0.9884557723999
            },
            scale = 1.7126243114471
          }
        }
      }
    },
    entity1543328686_6815 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              243.390625,
              1000.7252807617,
              -416.98153686523
            },
            rotation = {
              -0.0029867775738239,
              -0.0046580419875681,
              -0.14439305663109,
              0.98950493335724
            },
            scale = 1.4147535562515
          }
        }
      }
    },
    entity1543328686_7837 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              603.17980957031,
              765.78680419922,
              -298.05517578125
            },
            rotation = {
              0.0055170650593936,
              0.00042554465471767,
              0.87649101018906,
              -0.48138639330864
            },
            scale = 1.1152410507202
          }
        }
      }
    },
    entity1543328686_8653 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              315.40783691406,
              683.54144287109,
              -300.02893066406
            },
            rotation = {
              -8.2264584477798e-08,
              4.6109093432278e-08,
              -0.15151116251945,
              0.98845553398132
            },
            scale = 1.3500329256058
          }
        }
      }
    },
    entity1543328686_903 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              212.36279296875,
              1122.8306884766,
              -773.318359375
            },
            rotation = {
              -1.0317246790237e-07,
              4.2080486650775e-08,
              0.28352916240692,
              0.95896363258362
            },
            scale = 3.4295284748077
          }
        }
      }
    },
    entity1543328686_9500 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              824.12341308594,
              1134.7658691406,
              -381.3293762207
            },
            rotation = {
              -0.0048117451369762,
              -0.0027329153381288,
              -0.58830326795578,
              0.80862140655518
            },
            scale = 1.4147447347641
          }
        }
      }
    },
    entity1543328697_1784 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              417.87921142578,
              221.59002685547,
              -324.08599853516
            },
            rotation = {
              -5.7606474257454e-08,
              7.4742608546785e-08,
              -0.52581816911697,
              0.85059702396393
            },
            scale = 1.3500349521637
          }
        }
      }
    },
    entity1543328697_1839 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              467.10467529297,
              387.12744140625,
              -277.25894165039
            },
            rotation = {
              -6.6361337758281e-08,
              8.6471994009685e-09,
              -0.11386425793171,
              0.99349629878998
            },
            scale = 1.3500583171844
          }
        }
      }
    },
    entity1543328697_1920 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              827.10821533203,
              224.38208007813,
              -491.04345703125
            },
            rotation = {
              1.2224269596572e-07,
              1.0834736441367e-09,
              0.99563372135162,
              -0.093346014618874
            },
            scale = 2.4937288761139
          }
        }
      }
    },
    entity1543328697_3225 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              928.20031738281,
              199.36170959473,
              -464.78875732422
            },
            rotation = {
              0.0052446317858994,
              -0.0017647668719292,
              0.99492138624191,
              -0.10050291568041
            },
            scale = 1.4147492647171
          }
        }
      }
    },
    entity1543328697_4544 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              676.89892578125,
              71.636795043945,
              -322.11227416992
            },
            rotation = {
              0.0052444757893682,
              -0.001764778746292,
              0.99492239952087,
              -0.10049268603325
            },
            scale = 1.1152410507202
          }
        }
      }
    },
    entity1543328697_5271 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              1012.0825805664,
              106.98541259766,
              -431.64016723633
            },
            rotation = {
              -7.075810515289e-08,
              4.8226240778604e-08,
              -0.86126315593719,
              0.50815922021866
            },
            scale = 1.712613940239
          }
        }
      }
    },
    entity1543328697_6001 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              597.20593261719,
              91.353332519531,
              -342.8083190918
            },
            rotation = {
              4.3989292208835e-08,
              2.5949676185633e-08,
              0.99563366174698,
              -0.093346565961838
            },
            scale = 1.350047826767
          }
        }
      }
    },
    entity1543328697_602 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              415.09420776367,
              303.63781738281,
              -303.39004516602
            },
            rotation = {
              -0.0045699062757194,
              -0.0031199681106955,
              -0.5196790099144,
              0.85434365272522
            },
            scale = 1.115234375
          }
        }
      }
    },
    entity1543328697_6149 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              599.62744140625,
              389.59225463867,
              -467.29275512695
            },
            rotation = {
              -3.7542623232412e-08,
              3.8551320358238e-08,
              -0.52581650018692,
              0.85059803724289
            },
            scale = 1.7126257419586
          }
        }
      }
    },
    entity1543328697_6314 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              1095.8303222656,
              168.88848876953,
              -405.38641357422
            },
            rotation = {
              -0.005497173871845,
              -0.0006347184535116,
              -0.8575684428215,
              0.51434010267258
            },
            scale = 1.4147437810898
          }
        }
      }
    },
    entity1543328697_6446 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              743.02136230469,
              -1.1885375976563,
              -295.98098754883
            },
            rotation = {
              -8.0241896682764e-08,
              4.3181662690017e-09,
              -0.86126124858856,
              0.50816243886948
            },
            scale = 1.3500481843948
          }
        }
      }
    },
    entity1543328697_6864 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              809.03790283203,
              47.611396789551,
              -275.28524780273
            },
            rotation = {
              -0.0054974257946014,
              -0.00063489691819996,
              -0.85756999254227,
              0.51433748006821
            },
            scale = 1.115177154541
          }
        }
      }
    },
    entity1543328697_7343 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              579.66082763672,
              663.25695800781,
              -381.63632202148
            },
            rotation = {
              -0.0028079452458769,
              -0.0047683487646282,
              -0.10670870542526,
              0.99427491426468
            },
            scale = 1.414729475975
          }
        }
      }
    },
    entity1543328697_8004 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              402.13745117188,
              437.31661987305,
              -256.56350708008
            },
            rotation = {
              -0.0028077142778784,
              -0.0047681708820164,
              -0.10670577734709,
              0.99427527189255
            },
            scale = 1.1152300834656
          }
        }
      }
    },
    entity1543328697_8226 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              596.09899902344,
              493.67498779297,
              -441.03863525391
            },
            rotation = {
              -0.0045698643662035,
              -0.0031200191006064,
              -0.51967662572861,
              0.85434514284134
            },
            scale = 1.4147546291351
          }
        }
      }
    },
    entity1543328697_834 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ltree1_lowpoly.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1_lowpoly.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              662.07012939453,
              599.58911132813,
              -407.89004516602
            },
            rotation = {
              -8.073924107066e-08,
              7.8767484978925e-08,
              -0.11386022716761,
              0.99349677562714
            },
            scale = 1.7126297950745
          }
        }
      }
    },
    entity1543330961_2775 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 5.0,
          max_spawns = 1.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -1025.6125488281,
              -268.44360351563,
              123.41094207764
            },
            rotation = {
              3.0616970070341e-10,
              -2.3544868832381e-10,
              0.99158275127411,
              -0.12947443127632
            },
            scale = 0.85252219438553
          }
        }
      }
    },
    entity1543336638_2418 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -432.58224487305,
              248.95832824707,
              75.792037963867
            },
            rotation = {
              -0.35064536333084,
              0.61148983240128,
              0.34939244389534,
              0.61729484796524
            },
            scale = 1.2363148927689
          }
        }
      }
    },
    entity1543336638_2895 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -438.46102905273,
              240.11672973633,
              75.875640869141
            },
            rotation = {
              0.1915275156498,
              0.67837131023407,
              -0.19655151665211,
              0.6815402507782
            },
            scale = 1.2362542152405
          }
        }
      }
    },
    entity1543336638_509 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -447.72714233398,
              246.16955566406,
              75.765686035156
            },
            rotation = {
              -0.61314541101456,
              -0.3477403819561,
              0.61894547939301,
              -0.34646183252335
            },
            scale = 1.2362992763519
          }
        }
      }
    },
    entity1543336638_9926 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -441.90493774414,
              254.9228515625,
              75.901992797852
            },
            rotation = {
              -0.67927247285843,
              0.18830801546574,
              0.68246382474899,
              0.1933187097311
            },
            scale = 1.2362533807755
          }
        }
      }
    },
    entity1543475433_4398 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              -305.56628417969,
              416.05938720703,
              418.13513183594
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543475498_1851 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              168.84802246094,
              5.0778255462646,
              1074.6020507813
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543475581_5778 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              111.01715087891,
              -507.76583862305,
              1021.2261962891
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543475620_6898 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              -573.4775390625,
              -835.80358886719,
              1164.4903564453
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543475963_1352 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              -203.3512878418,
              321.94198608398,
              1004.7051391602
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543475989_7380 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              -1009.4924316406,
              -5.5163702964783,
              1116.2935791016
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543476047_3423 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              -335.68966674805,
              387.65014648438,
              36.291854858398
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543476099_4445 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              -1166.2174072266,
              -247.92643737793,
              -118.89346313477
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543476218_6874 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              -813.39959716797,
              -642.92553710938,
              86.0615234375
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543476289_8982 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              70.782409667969,
              177.83390808105,
              302.06634521484
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543476381_3892 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              -710.66564941406,
              496.15933227539,
              50.171138763428
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543675645_8127 = {
      components = {
        CutsceneWaypointComponent = {
          fov = 50.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -171.64668273926,
              -361.53509521484,
              283.57827758789
            },
            rotation = {
              -0.22227272391319,
              0.82753425836563,
              0.49789288640022,
              -0.13373343646526
            },
            scale = 1.0000425577164
          }
        }
      }
    },
    entity1543676288_8127 = {
      components = {
        CutsceneWaypointComponent = {
          index = 1.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -176.12948608398,
              -354.23440551758,
              277.31884765625
            },
            rotation = {
              -0.22815458476543,
              0.84943336248398,
              0.45953440666199,
              -0.12343671172857
            },
            scale = 1.0000061988831
          }
        }
      }
    },
    entity1543676324_3710 = {
      components = {
        CutsceneWaypointComponent = {
          index = 2.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -182.14331054688,
              -344.44329833984,
              271.03540039063
            },
            rotation = {
              -0.18064992129803,
              0.80309897661209,
              0.5539573431015,
              -0.12461514025927
            },
            scale = 1.0000196695328
          }
        }
      }
    },
    entity1543676355_3668 = {
      components = {
        CutsceneWaypointComponent = {
          index = 3.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -193.94345092773,
              -327.96148681641,
              263.14584350586
            },
            rotation = {
              0.4398620724678,
              0.62165647745132,
              0.5290824174881,
              0.37434807419777
            },
            scale = 1.0000215768814
          }
        }
      }
    },
    entity1543676387_349 = {
      components = {
        CutsceneWaypointComponent = {
          index = 4.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -195.37074279785,
              -321.49237060547,
              260.16653442383
            },
            rotation = {
              0.57281172275543,
              0.44783756136894,
              0.42285966873169,
              0.54084926843643
            },
            scale = 1.0000318288803
          }
        }
      }
    },
    entity1543698010_7678 = {
      components = {
        CutsceneWaypointComponent = {
          index = 5.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -193.37109375,
              -313.73034667969,
              258.51290893555
            },
            rotation = {
              0.64754468202591,
              0.33069443702698,
              0.31225171685219,
              0.61141312122345
            },
            scale = 1.000044465065
          }
        }
      }
    },
    entity1543698060_6023 = {
      components = {
        CutsceneWaypointComponent = {
          index = 6.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -190.03857421875,
              -305.1994934082,
              257.47402954102
            },
            rotation = {
              0.68013274669647,
              0.25708422064781,
              0.24274864792824,
              0.64218401908875
            },
            scale = 1.0000487565994
          }
        }
      }
    },
    entity1543698510_8127 = {
      components = {
        CutsceneWaypointComponent = {
          index = 7.0,
          type = 1.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              12.266147613525,
              -272.64688110352,
              149.21522521973
            },
            rotation = {
              0.40352883934975,
              0.15024648606777,
              0.31493645906448,
              0.84581643342972
            },
            scale = 1.000039100647
          }
        }
      }
    },
    entity1543698539_3710 = {
      components = {
        CutsceneWaypointComponent = {
          index = 8.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              24.66287612915,
              -258.91690063477,
              149.1183013916
            },
            rotation = {
              0.42543935775757,
              0.25101816654205,
              0.44184571504593,
              0.74884152412415
            },
            scale = 1.0000458955765
          }
        }
      }
    },
    entity1543701084_8127 = {
      components = {
        CutsceneWaypointComponent = {
          index = 9.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              31.436010360718,
              -241.99629211426,
              149.1234588623
            },
            rotation = {
              0.37996488809586,
              0.3156535923481,
              0.55561250448227,
              0.66879314184189
            },
            scale = 1.0000426769257
          }
        }
      }
    },
    entity1543701102_3710 = {
      components = {
        CutsceneWaypointComponent = {
          index = 10.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              32.386432647705,
              -224.43606567383,
              148.49154663086
            },
            rotation = {
              0.32141327857971,
              0.37510377168655,
              0.66025447845459,
              0.56573367118835
            },
            scale = 1.0000488758087
          }
        }
      }
    },
    entity1543701289_4509 = {
      components = {
        CutsceneWaypointComponent = {
          index = 11.0,
          type = 1.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -227.32846069336,
              -87.701644897461,
              859.21240234375
            },
            rotation = {
              0.0063679534941912,
              0.09300359338522,
              0.99332427978516,
              0.067946024239063
            },
            scale = 1.0000762939453
          }
        }
      }
    },
    entity1543701358_2813 = {
      components = {
        CutsceneWaypointComponent = {
          index = 12.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -239.94955444336,
              -85.446136474609,
              859.24627685547
            },
            rotation = {
              0.0063681621104479,
              0.093001700937748,
              0.99332451820374,
              0.067945122718811
            },
            scale = 1.000076174736
          }
        }
      }
    },
    entity1543701380_9494 = {
      components = {
        CutsceneWaypointComponent = {
          index = 13.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -251.1919708252,
              -85.417190551758,
              859.18737792969
            },
            rotation = {
              0.0063683222979307,
              0.09300285577774,
              0.9933243393898,
              0.067946128547192
            },
            scale = 1.0000764131546
          }
        }
      }
    },
    entity1543701430_6942 = {
      components = {
        CutsceneWaypointComponent = {
          index = 14.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -263.56460571289,
              -84.900375366211,
              859.07073974609
            },
            rotation = {
              0.0030402215197682,
              0.093171641230583,
              0.99511808156967,
              0.032401446253061
            },
            scale = 1.0000604391098
          }
        }
      }
    },
    entity1543701457_8732 = {
      components = {
        CutsceneWaypointComponent = {
          index = 15.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -275.09060668945,
              -84.880310058594,
              859.22631835938
            },
            rotation = {
              -0.00083013257244602,
              0.093217849731445,
              0.99560528993607,
              -0.0089358240365982
            },
            scale = 1.0000607967377
          }
        }
      }
    },
    entity1543701486_9353 = {
      components = {
        CutsceneWaypointComponent = {
          index = 16.0,
          type = 1.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -952.783203125,
              -344.40084838867,
              453.2287902832
            },
            rotation = {
              -0.31347540020943,
              0.53333133459091,
              0.67733520269394,
              -0.39813059568405
            },
            scale = 1.0000702142715
          }
        }
      }
    },
    entity1543701548_2906 = {
      components = {
        CutsceneWaypointComponent = {
          index = 17.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -921.45495605469,
              -411.57870483398,
              443.13143920898
            },
            rotation = {
              0.51149123907089,
              -0.34797209501266,
              -0.44192332029343,
              0.64961212873459
            },
            scale = 1.0000780820847
          }
        }
      }
    },
    entity1543701582_6058 = {
      components = {
        CutsceneWaypointComponent = {
          index = 18.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -831.44848632813,
              -380.66955566406,
              396.89846801758
            },
            rotation = {
              -0.36384302377701,
              0.50032556056976,
              0.6354187130928,
              -0.46209919452667
            },
            scale = 1.0000762939453
          }
        }
      }
    },
    entity1543701636_8083 = {
      components = {
        CutsceneWaypointComponent = {
          index = 19.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -703.82025146484,
              -428.57192993164,
              353.11248779297
            },
            rotation = {
              0.2920606136322,
              -0.36544266343117,
              -0.69041401147842,
              0.551797747612
            },
            scale = 1.000062584877
          }
        }
      }
    },
    entity1543701682_48 = {
      components = {
        CutsceneWaypointComponent = {
          index = 20.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -615.31994628906,
              -388.68829345703,
              347.79516601563
            },
            rotation = {
              -0.14964999258518,
              0.34413149952888,
              0.85001784563065,
              -0.36965933442116
            },
            scale = 1.0000655651093
          }
        }
      }
    },
    entity1543701713_1052 = {
      components = {
        CutsceneWaypointComponent = {
          index = 21.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -545.62579345703,
              -287.24453735352,
              317.4697265625
            },
            rotation = {
              0.49634343385696,
              -0.11649273335934,
              -0.19655016064644,
              0.83752053976059
            },
            scale = 1.0000627040863
          }
        }
      }
    },
    entity1543701750_3944 = {
      components = {
        CutsceneWaypointComponent = {
          index = 22.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -508.50100708008,
              -197.8292388916,
              282.5068359375
            },
            rotation = {
              0.50654405355453,
              -0.057772219181061,
              -0.097462639212608,
              0.85473769903183
            },
            scale = 1.0000513792038
          }
        }
      }
    },
    entity1543701777_2768 = {
      components = {
        CutsceneWaypointComponent = {
          index = 23.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -481.36798095703,
              -96.176681518555,
              222.82911682129
            },
            rotation = {
              0.50879621505737,
              -0.032423421740532,
              -0.054691836237907,
              0.8585359454155
            },
            scale = 1.0000531673431
          }
        }
      }
    },
    entity1543701813_3167 = {
      components = {
        CutsceneWaypointComponent = {
          index = 24.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -518.43206787109,
              -2.7346074581146,
              170.85903930664
            },
            rotation = {
              0.4511322081089,
              -0.23748698830605,
              -0.40072032809258,
              0.76125091314316
            },
            scale = 1.0000585317612
          }
        }
      }
    },
    entity1543701845_8242 = {
      components = {
        CutsceneWaypointComponent = {
          index = 25.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -501.85089111328,
              13.525741577148,
              161.11180114746
            },
            rotation = {
              0.45997178554535,
              -0.21987999975681,
              -0.37100994586945,
              0.77616387605667
            },
            scale = 1.0000562667847
          }
        }
      }
    },
    entity1543701860_8534 = {
      components = {
        CutsceneWaypointComponent = {
          index = 26.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -482.89743041992,
              20.330007553101,
              153.90312194824
            },
            rotation = {
              0.47438383102417,
              -0.18677538633347,
              -0.31514668464661,
              0.80047333240509
            },
            scale = 1.0000501871109
          }
        }
      }
    },
    entity1543701874_3115 = {
      components = {
        CutsceneWaypointComponent = {
          index = 27.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -467.79681396484,
              22.502979278564,
              150.38526916504
            },
            rotation = {
              0.50979423522949,
              -0.006072087213397,
              -0.010230399668217,
              0.86021411418915
            },
            scale = 1.0000512599945
          }
        }
      }
    },
    entity1543701896_1976 = {
      components = {
        CutsceneWaypointComponent = {
          index = 28.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -459.09854125977,
              23.81090927124,
              148.97975158691
            },
            rotation = {
              0.50187033414841,
              0.089749455451965,
              0.15145362913609,
              0.84683710336685
            },
            scale = 1.0000562667847
          }
        }
      }
    },
    entity1543701950_8196 = {
      components = {
        CutsceneWaypointComponent = {
          index = 29.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -454.71841430664,
              24.471298217773,
              148.98435974121
            },
            rotation = {
              0.4943825006485,
              0.12455975264311,
              0.21019065380096,
              0.83420062065125
            },
            scale = 1.0000706911087
          }
        }
      }
    },
    entity1543701969_6298 = {
      components = {
        CutsceneWaypointComponent = {
          index = 30.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -452.14743041992,
              25.501958847046,
              149.09796142578
            },
            rotation = {
              0.48817503452301,
              0.14702066779137,
              0.24809047579765,
              0.82372397184372
            },
            scale = 1.0000691413879
          }
        }
      }
    },
    entity1543702024_1744 = {
      components = {
        CutsceneWaypointComponent = {
          index = 31.0,
          type = 1.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -468.79187011719,
              39.594291687012,
              139.02976989746
            },
            rotation = {
              0.63203573226929,
              -0.080585516989231,
              -0.097469836473465,
              0.76454985141754
            },
            scale = 1.0000705718994
          }
        }
      }
    },
    entity1543702055_3226 = {
      components = {
        CutsceneWaypointComponent = {
          index = 32.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -468.28234863281,
              41.11336517334,
              139.28581237793
            },
            rotation = {
              0.63203674554825,
              -0.080585703253746,
              -0.097469657659531,
              0.76454901695251
            },
            scale = 1.0000705718994
          }
        }
      }
    },
    entity1543702069_9631 = {
      components = {
        CutsceneWaypointComponent = {
          index = 33.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -467.73727416992,
              42.689765930176,
              139.1994934082
            },
            rotation = {
              0.63203525543213,
              -0.080583699047565,
              -0.097468428313732,
              0.76455062627792
            },
            scale = 1.000053524971
          }
        }
      }
    },
    entity1543702087_7093 = {
      components = {
        CutsceneWaypointComponent = {
          index = 34.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -467.24786376953,
              44.127193450928,
              139.06414794922
            },
            rotation = {
              0.63203543424606,
              -0.080585494637489,
              -0.097469635307789,
              0.76455014944077
            },
            scale = 1.0000547170639
          }
        }
      }
    },
    entity1543702106_1975 = {
      components = {
        CutsceneWaypointComponent = {
          index = 35.0,
          type = 0.0
        },
        ModelComponent = {
          asset = "assets/model/editor_camera_waypoint.fbx"
        },
        TransformComponent = {
          transform = {
            position = {
              -466.80996704102,
              45.427909851074,
              139.01042175293
            },
            rotation = {
              0.63203561306,
              -0.080585613846779,
              -0.097469508647919,
              0.76454997062683
            },
            scale = 1.0000547170639
          }
        }
      }
    },
    entity1543705320_8127 = {
      components = {
        DestructibleComponent = {
          health_points = 1,
          max_health_points = 1
        },
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/bookshelf.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/bookshelf.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -455.5397644043,
              56.897647857666,
              136.04737854004
            },
            rotation = {
              -0.0021177656017244,
              0.0068719764240086,
              0.80885779857635,
              0.58796036243439
            },
            scale = 3.0124955177307
          }
        }
      }
    },
    entity1543705407_3710 = {
      components = {
        DestructibleComponent = {
          health_points = 1,
          max_health_points = 1
        },
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/bookshelf.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/bookshelf.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -455.08926391602,
              57.121856689453,
              142.81216430664
            },
            rotation = {
              -0.0021177590824664,
              0.0068719862028956,
              0.80885857343674,
              0.58795928955078
            },
            scale = 3.012496471405
          }
        }
      }
    },
    entity1543705464_3668 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/doorway.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/doorway.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -467.02194213867,
              41.763633728027,
              136.09480285645
            },
            rotation = {
              0.0045714750885963,
              0.0055509647354484,
              -0.045612495392561,
              0.9989333152771
            },
            scale = 3.2045423984528
          }
        }
      }
    },
    entity1543705578_349 = {
      components = {
        DestructibleComponent = {
          health_points = 1,
          max_health_points = 1
        },
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/bookshelf.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/bookshelf.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -470.16946411133,
              67.624336242676,
              135.91184997559
            },
            rotation = {
              -0.0027592228725553,
              0.0039449599571526,
              0.98872816562653,
              -0.14964433014393
            },
            scale = 3.0124959945679
          }
        }
      }
    },
    entity1543706537_8127 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/ceiling_deco.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ceiling_deco.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -465.25717163086,
              51.905883789063,
              135.41778564453
            },
            rotation = {
              0.0040056928992271,
              0.0059718857519329,
              0.052457291632891,
              0.99859726428986
            },
            scale = 3.2099311351776
          }
        }
      }
    },
    entity1543707072_1995 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved_railing.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved_railing.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -466.87084960938,
              36.086082458496,
              135.46746826172
            },
            rotation = {
              0.32682266831398,
              0.62454551458359,
              -0.33239060640335,
              0.62661498785019
            },
            scale = 1.9158189296722
          }
        }
      }
    },
    entity1543707259_3352 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_straight.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_straight.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -456.80383300781,
              35.058979034424,
              135.11845397949
            },
            rotation = {
              -0.16372628509998,
              0.070921003818512,
              -0.45364433526993,
              0.87313842773438
            },
            scale = 1.2383905649185
          }
        }
      }
    },
    entity1543707290_3283 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -444.9270324707,
              50.942409515381,
              130.10507202148
            },
            rotation = {
              -0.16147837042809,
              0.6861452460289,
              0.15862107276917,
              0.69135290384293
            },
            scale = 2.1621029376984
          }
        }
      }
    },
    entity1543707323_2507 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -454.27362060547,
              63.852416992188,
              130.29447937012
            },
            rotation = {
              -0.59406739473343,
              0.37941604852676,
              0.5956466794014,
              0.38513946533203
            },
            scale = 1.3973684310913
          }
        }
      }
    },
    entity1543707323_6253 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -452.83169555664,
              68.470779418945,
              130.08363342285
            },
            rotation = {
              -0.59406805038452,
              0.37941259145737,
              0.59564876556396,
              0.38513866066933
            },
            scale = 2.1621022224426
          }
        }
      }
    },
    entity1543995053_1589 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543995104_7996",
            "entity1543995092_8270",
            "entity1543995510_6174",
            "entity1543995756_9784",
            "entity1543995958_1126"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -32.707675933838,
              -233.35580444336,
              107.58248901367
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543995092_8270 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543995104_7996",
            "entity1543995053_1589",
            "entity1543995510_6174",
            "entity1543998339_1191",
            "entity1544080840_6272",
            "entity1544080809_9040"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -45.749320983887,
              -223.0451965332,
              107.11907958984
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543995104_7996 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543995053_1589",
            "entity1543995092_8270",
            "entity1543995756_9784",
            "entity1544080840_6272"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -46.903465270996,
              -230.64161682129,
              107.18561553955
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543995510_6174 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543995053_1589",
            "entity1543995092_8270",
            "entity1543998212_5057",
            "entity1543998339_1191"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -31.079669952393,
              -224.75466918945,
              107.54006958008
            },
            rotation = {
              0.0,
              0.0,
              -1.8626441544711e-07,
              1.0
            },
            scale = 0.99999850988388
          }
        }
      }
    },
    entity1543995756_9784 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543995053_1589",
            "entity1543995104_7996",
            "entity1543995958_1126",
            "entity1543996071_9691"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -45.628379821777,
              -239.64543151855,
              107.34239196777
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543995958_1126 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543995053_1589",
            "entity1543995756_9784",
            "entity1543996071_9691",
            "entity1543996234_7474",
            "entity1543996281_9324"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -29.182159423828,
              -236.89164733887,
              107.55368041992
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543996071_9691 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543995958_1126",
            "entity1543995756_9784",
            "entity1543996234_7474",
            "entity1544014961_5270"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -40.483337402344,
              -247.3733215332,
              107.4309387207
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543996234_7474 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543995958_1126",
            "entity1543996071_9691",
            "entity1543996281_9324",
            "entity1543996367_3894",
            "entity1544014961_5270",
            "entity1544015012_9561"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -34.369850158691,
              -251.86911010742,
              107.42753601074
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543996281_9324 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543996234_7474",
            "entity1543995958_1126",
            "entity1543996367_3894",
            "entity1543996442_2303",
            "entity1543996472_8056"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -22.765779495239,
              -238.94125366211,
              107.47319030762
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543996367_3894 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543996234_7474",
            "entity1543996281_9324",
            "entity1543996442_2303"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -22.380075454712,
              -254.166015625,
              107.24945831299
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543996442_2303 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543996281_9324",
            "entity1543996367_3894",
            "entity1543996472_8056",
            "entity1543996549_8344"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -11.620676040649,
              -249.14344787598,
              107.10145568848
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543996472_8056 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543996281_9324",
            "entity1543996442_2303",
            "entity1543996549_8344",
            "entity1543996619_9161",
            "entity1543996678_3165"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -17.721115112305,
              -231.62327575684,
              107.53301239014
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543996549_8344 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543996472_8056",
            "entity1543996442_2303",
            "entity1543996619_9161"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -4.2631006240845,
              -236.80624389648,
              107.1156463623
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543996619_9161 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543996472_8056",
            "entity1543996549_8344",
            "entity1543996678_3165",
            "entity1543996766_5477"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -4.9769377708435,
              -223.58763122559,
              107.35559082031
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543996678_3165 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543996472_8056",
            "entity1543996619_9161",
            "entity1543996766_5477",
            "entity1543998150_1098",
            "entity1543998212_5057"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -19.862777709961,
              -226.1293182373,
              107.59970092773
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1543996766_5477 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543996678_3165",
            "entity1543996619_9161",
            "entity1543998150_1098"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -9.8005800247192,
              -215.82765197754,
              107.33237457275
            },
            rotation = {
              1.6622374310327e-10,
              -9.2969942855348e-10,
              0.98953139781952,
              0.14431780576706
            },
            scale = 0.99998587369919
          }
        }
      }
    },
    entity1543998150_1098 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543996678_3165",
            "entity1543996766_5477",
            "entity1543998212_5057",
            "entity1543998301_3816"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -17.229869842529,
              -210.88850402832,
              107.15328979492
            },
            rotation = {
              1.6623358245482e-10,
              -9.2971541576503e-10,
              0.9895321726799,
              0.14431247115135
            },
            scale = 0.99996817111969
          }
        }
      }
    },
    entity1543998212_5057 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543996678_3165",
            "entity1543998150_1098",
            "entity1543998301_3816",
            "entity1543998339_1191",
            "entity1543995510_6174"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -26.526134490967,
              -223.49914550781,
              107.54683685303
            },
            rotation = {
              1.6623664944593e-10,
              -9.2970869891573e-10,
              0.98953258991241,
              0.14430950582027
            },
            scale = 0.99995666742325
          }
        }
      }
    },
    entity1543998301_3816 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543998212_5057",
            "entity1543998150_1098",
            "entity1543998339_1191"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -28.507152557373,
              -209.56353759766,
              107.22204589844
            },
            rotation = {
              1.6622420107026e-10,
              -9.296420300231e-10,
              0.98953229188919,
              0.14431159198284
            },
            scale = 1.0000077486038
          }
        }
      }
    },
    entity1543998339_1191 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543998212_5057",
            "entity1543998301_3816",
            "entity1543995510_6174",
            "entity1543995092_8270"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -39.002861022949,
              -213.9839630127,
              107.19958496094
            },
            rotation = {
              1.6622418719248e-10,
              -9.296419190008e-10,
              0.98953229188919,
              0.14431159198284
            },
            scale = 1.0000077486038
          }
        }
      }
    },
    entity1544005777_2663 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_player_checkpoint.fbx"
        },
        NameComponent = {
          name = "spawn_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -10.45357131958,
              -34.782608032227,
              180.77557373047
            },
            rotation = {
              0.00029951371834613,
              0.00023879756918177,
              0.999880194664,
              0.015473900362849
            },
            scale = 2.297132730484
          }
        }
      }
    },
    entity1544012076_1325 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544012128_6774",
            "entity1544012092_4836",
            "entity1544014713_5452",
            "entity1544014804_3352",
            "entity1544014892_6008"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -82.467620849609,
              -310.71780395508,
              104.05367279053
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544012092_4836 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544012128_6774",
            "entity1544012076_1325",
            "entity1544014384_5133",
            "entity1544014892_6008"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -88.569679260254,
              -319.07208251953,
              103.73150634766
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544012128_6774 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544012076_1325",
            "entity1544012092_4836",
            "entity1544014384_5133",
            "entity1544014420_53",
            "entity1544012189_8919"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -79.339233398438,
              -318.53604125977,
              103.83121490479
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544012189_5231 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544012189_8919",
            "entity1544012189_7829",
            "entity1544014420_53",
            "entity1544014522_2070"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -63.044319152832,
              -321.39825439453,
              103.75038146973
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544012189_7829 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544012189_8919",
            "entity1544012189_5231",
            "entity1544014522_2070",
            "entity1544014543_5966",
            "entity1544014599_4182"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -67.047592163086,
              -313.810546875,
              103.80811309814
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544012189_8919 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544012189_7829",
            "entity1544012189_5231",
            "entity1544012128_6774",
            "entity1544014420_53"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -72.594467163086,
              -319.59030151367,
              103.88085174561
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544012774_5708 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -9.8111715316772,
              -33.654415130615,
              180.44955444336
            },
            rotation = {
              0.22287049889565,
              0.67462122440338,
              -0.19837431609631,
              0.67517596483231
            },
            scale = 0.94335776567459
          }
        }
      }
    },
    entity1544014049_5385 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_player_checkpoint.fbx"
        },
        NameComponent = {
          name = "spawn_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -646.50793457031,
              265.35125732422,
              147.77264404297
            },
            rotation = {
              3.9014292269712e-05,
              -0.00038106201100163,
              -0.62526905536652,
              0.78040915727615
            },
            scale = 2.2971439361572
          }
        }
      }
    },
    entity1544014049_7826 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -647.70843505859,
              265.86444091797,
              147.49084472656
            },
            rotation = {
              -0.35357230901718,
              0.61625701189041,
              0.37033516168594,
              0.59838598966599
            },
            scale = 0.94335806369781
          }
        }
      }
    },
    entity1544014203_9015 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_player_checkpoint.fbx"
        },
        NameComponent = {
          name = "spawn_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -466.53448486328,
              34.494422912598,
              136.73690795898
            },
            rotation = {
              -2.8875047064503e-05,
              -0.00038196457899176,
              0.99998372793198,
              0.0056891366839409
            },
            scale = 2.2972128391266
          }
        }
      }
    },
    entity1544014384_5133 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544012092_4836",
            "entity1544012128_6774",
            "entity1544014420_53"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -82.128234863281,
              -325.26455688477,
              103.63464355469
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544014420_53 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544012128_6774",
            "entity1544014384_5133",
            "entity1544012189_8919",
            "entity1544012189_5231"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -73.40234375,
              -326.75637817383,
              103.80152893066
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544014522_2070 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544012189_7829",
            "entity1544012189_5231",
            "entity1544014543_5966"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -60.555309295654,
              -314.13543701172,
              103.7186050415
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544014543_5966 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544012189_7829",
            "entity1544014522_2070",
            "entity1544014599_4182",
            "entity1544014637_3782",
            "entity1544015109_65"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -63.589653015137,
              -303.14190673828,
              103.88610839844
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544014599_4182 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544012189_7829",
            "entity1544014543_5966",
            "entity1544014637_3782",
            "entity1544014713_5452"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -71.381713867188,
              -308.05096435547,
              104.0266418457
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544014637_3782 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544014599_4182",
            "entity1544014543_5966",
            "entity1544014713_5452",
            "entity1544014735_9070",
            "entity1544015109_65",
            "entity1544015077_1879"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -70.246528625488,
              -299.41061401367,
              103.85286712646
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544014713_5452 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544014599_4182",
            "entity1544014637_3782",
            "entity1544014735_9070",
            "entity1544014804_3352",
            "entity1544012076_1325"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -75.860412597656,
              -306.20364379883,
              104.0587310791
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544014735_9070 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544014637_3782",
            "entity1544014713_5452",
            "entity1544014804_3352"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -78.251312255859,
              -298.63906860352,
              104.1051864624
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544014804_3352 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544014735_9070",
            "entity1544014713_5452",
            "entity1544012076_1325",
            "entity1544014892_6008"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -86.638366699219,
              -303.12445068359,
              104.16150665283
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544014892_6008 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544012076_1325",
            "entity1544014804_3352",
            "entity1544012092_4836"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -89.883361816406,
              -310.80120849609,
              103.82569885254
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544014961_5270 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543996071_9691",
            "entity1543996234_7474",
            "entity1544015012_9561",
            "entity1544015077_1879"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -50.763423919678,
              -264.92559814453,
              103.8441696167
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544015012_9561 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543996234_7474",
            "entity1544014961_5270",
            "entity1544015077_1879",
            "entity1544015109_65"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -43.977718353271,
              -268.96231079102,
              103.79586791992
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544015077_1879 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544015012_9561",
            "entity1544014961_5270",
            "entity1544015109_65",
            "entity1544014637_3782"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -60.5891456604,
              -281.64501953125,
              103.91329193115
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544015109_65 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544015077_1879",
            "entity1544015012_9561",
            "entity1544014637_3782",
            "entity1544014543_5966"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -54.04407119751,
              -285.71697998047,
              103.78668212891
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544016336_1253 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544016653_2630",
            "entity1544016372_1818",
            "entity1544024899_8566"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -579.27044677734,
              -426.646484375,
              212.18481445313
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544016372_1818 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544016653_2630",
            "entity1544016336_1253",
            "entity1544016730_9320"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -572.23919677734,
              -531.5556640625,
              212.17546081543
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544016653_2630 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544016336_1253",
            "entity1544016372_1818",
            "entity1544016730_9320",
            "entity1544024752_6705",
            "entity1544024899_8566"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -439.46246337891,
              -426.34182739258,
              212.1711730957
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544016730_9320 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544016653_2630",
            "entity1544016372_1818",
            "entity1544024752_6705"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -475.78720092773,
              -551.81707763672,
              212.2092590332
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544019515_2072 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544019598_2555",
            "entity1544019575_4885",
            "entity1544021302_1016",
            "entity1544021429_424"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -951.57287597656,
              -324.5178527832,
              132.47923278809
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544019575_4885 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544019598_2555",
            "entity1544019515_2072",
            "entity1544019840_7806",
            "entity1544019884_8244",
            "entity1544021246_5820",
            "entity1544021302_1016"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -938.01715087891,
              -338.70852661133,
              133.12564086914
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544019598_2555 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544019575_4885",
            "entity1544019515_2072",
            "entity1544019840_7806",
            "entity1544021429_424",
            "entity1544021469_8267"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -955.24871826172,
              -329.79626464844,
              132.47529602051
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544019840_7806 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544019575_4885",
            "entity1544019598_2555",
            "entity1544019884_8244",
            "entity1544019927_6089"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -958.03234863281,
              -342.04183959961,
              132.72543334961
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544019884_8244 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544019575_4885",
            "entity1544019840_7806",
            "entity1544019927_6089",
            "entity1544019957_1110",
            "entity1544020010_638"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -937.50006103516,
              -346.25225830078,
              133.21885681152
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544019927_6089 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544019884_8244",
            "entity1544019840_7806",
            "entity1544019957_1110"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -954.19006347656,
              -357.44146728516,
              132.89044189453
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544019957_1110 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544019884_8244",
            "entity1544019927_6089",
            "entity1544020010_638",
            "entity1544019991_730"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -947.64758300781,
              -364.5090637207,
              132.71147155762
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544019991_730 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544020010_638",
            "entity1544019957_1110",
            "entity1544020147_9074"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -936.61694335938,
              -369.29052734375,
              132.64953613281
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544020010_638 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544019884_8244",
            "entity1544019957_1110",
            "entity1544019991_730",
            "entity1544020147_9074",
            "entity1544020168_4805"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -928.91937255859,
              -348.14804077148,
              133.19012451172
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544020147_9074 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544020010_638",
            "entity1544019991_730",
            "entity1544020168_4805",
            "entity1544020234_2015"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -926.33941650391,
              -369.23455810547,
              132.54804992676
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544020168_4805 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544020010_638",
            "entity1544020147_9074",
            "entity1544020234_2015",
            "entity1544020276_6817"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -926.07885742188,
              -344.06051635742,
              133.29118347168
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544020234_2015 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544020168_4805",
            "entity1544020147_9074",
            "entity1544020276_6817",
            "entity1544020394_7271"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -913.23797607422,
              -362.50619506836,
              132.6559753418
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544020276_6817 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544020168_4805",
            "entity1544020234_2015",
            "entity1544020394_7271",
            "entity1544020417_6758",
            "entity1544020468_4349"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -919.84979248047,
              -340.23452758789,
              133.15737915039
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544020394_7271 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544020276_6817",
            "entity1544020234_2015",
            "entity1544020417_6758"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -905.48620605469,
              -349.76470947266,
              132.77857971191
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544020417_6758 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544020394_7271",
            "entity1544020276_6817",
            "entity1544020468_4349",
            "entity1544020551_6242"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -905.31097412109,
              -338.35775756836,
              132.86697387695
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544020468_4349 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544020276_6817",
            "entity1544020417_6758",
            "entity1544020551_6242",
            "entity1544020786_3745",
            "entity1544021213_5347",
            "entity1544021246_5820"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -923.3720703125,
              -333.70291137695,
              133.5930480957
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544020551_6242 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544020468_4349",
            "entity1544020417_6758",
            "entity1544020786_3745"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -910.02075195313,
              -325.48016357422,
              132.80787658691
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544020786_3745 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544020468_4349",
            "entity1544020551_6242",
            "entity1544021213_5347"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -918.63726806641,
              -318.46438598633,
              132.80255126953
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544021213_5347 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544020468_4349",
            "entity1544020786_3745",
            "entity1544021246_5820",
            "entity1544021302_1016"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -926.75225830078,
              -315.83853149414,
              132.47637939453
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544021246_5820 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544020468_4349",
            "entity1544021213_5347",
            "entity1544021302_1016",
            "entity1544019575_4885"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -931.90838623047,
              -336.69799804688,
              133.28022766113
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544021302_1016 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544021246_5820",
            "entity1544021213_5347",
            "entity1544019575_4885",
            "entity1544019515_2072"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -941.05413818359,
              -317.23526000977,
              132.48754882813
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544021429_424 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544019515_2072",
            "entity1544019598_2555",
            "entity1544021469_8267",
            "entity1544021466_278"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -973.31469726563,
              -310.80551147461,
              124.43202972412
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544021466_278 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544021429_424",
            "entity1544021469_8267",
            "entity1544021594_926",
            "entity1544021670_1253"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -991.56463623047,
              -298.60604858398,
              122.10615539551
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544021469_8267 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544021429_424",
            "entity1544019598_2555",
            "entity1544021466_278",
            "entity1544021594_926"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -976.85906982422,
              -316.73321533203,
              124.53382873535
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544021594_926 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544021466_278",
            "entity1544021469_8267",
            "entity1544021670_1253",
            "entity1544021683_9467"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -995.19055175781,
              -303.86373901367,
              122.15663909912
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544021670_1253 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544021466_278",
            "entity1544021594_926",
            "entity1544021683_9467",
            "entity1544021732_2416",
            "entity1544021751_9133"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1004.8834228516,
              -289.30471801758,
              121.58856201172
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544021683_9467 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544021670_1253",
            "entity1544021594_926",
            "entity1544021732_2416"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1008.7989501953,
              -294.09762573242,
              121.70529174805
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544021732_2416 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544021683_9467",
            "entity1544021670_1253",
            "entity1544021751_9133",
            "entity1544021895_5387",
            "entity1544021958_2879"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1020.8747558594,
              -286.48767089844,
              123.14324951172
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544021751_9133 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544021732_2416",
            "entity1544021670_1253",
            "entity1544021895_5387",
            "entity1544022467_4315",
            "entity1544022502_6495"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1017.4799804688,
              -281.15829467773,
              123.22158813477
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544021895_5387 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544021732_2416",
            "entity1544021751_9133",
            "entity1544021958_2879",
            "entity1544021939_1003",
            "entity1544022467_4315"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1023.8406982422,
              -280.83605957031,
              123.24909973145
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544021939_1003 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544021895_5387",
            "entity1544021958_2879",
            "entity1544021989_1953",
            "entity1544022066_6530"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1032.4393310547,
              -283.95663452148,
              123.29335021973
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544021958_2879 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544021732_2416",
            "entity1544021895_5387",
            "entity1544021939_1003",
            "entity1544021989_1953"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1027.9982910156,
              -288.36654663086,
              123.12670898438
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544021989_1953 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544021939_1003",
            "entity1544021958_2879",
            "entity1544022066_6530",
            "entity1544022098_8987"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1035.1612548828,
              -285.86709594727,
              123.23068237305
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544022066_6530 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544021939_1003",
            "entity1544021989_1953",
            "entity1544022098_8987",
            "entity1544022149_1904",
            "entity1544022181_771"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1035.8524169922,
              -277.66003417969,
              123.29495239258
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544022098_8987 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544021989_1953",
            "entity1544022066_6530",
            "entity1544022149_1904"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1039.0777587891,
              -278.78869628906,
              123.22438812256
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544022149_1904 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544022066_6530",
            "entity1544022098_8987",
            "entity1544022181_771",
            "entity1544022326_8608"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1037.7301025391,
              -271.46914672852,
              123.14028167725
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544022181_771 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544022149_1904",
            "entity1544022066_6530",
            "entity1544022326_8608",
            "entity1544022367_1637",
            "entity1544022396_8634"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1031.4951171875,
              -270.08828735352,
              123.2170715332
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544022326_8608 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544022149_1904",
            "entity1544022181_771",
            "entity1544022367_1637"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1033.3055419922,
              -267.20993041992,
              123.04821014404
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544022367_1637 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544022326_8608",
            "entity1544022181_771",
            "entity1544022396_8634",
            "entity1544022421_5609"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1027.0688476563,
              -265.32434082031,
              123.19914245605
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544022396_8634 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544022181_771",
            "entity1544022367_1637",
            "entity1544022421_5609",
            "entity1544022467_4315"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1024.9161376953,
              -270.43634033203,
              123.3002166748
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544022421_5609 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544022367_1637",
            "entity1544022396_8634",
            "entity1544022467_4315",
            "entity1544022502_6495"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1019.787109375,
              -267.46841430664,
              123.22703552246
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544022467_4315 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544022421_5609",
            "entity1544022396_8634",
            "entity1544022502_6495",
            "entity1544021751_9133",
            "entity1544021895_5387"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1022.6611328125,
              -275.83828735352,
              123.36904144287
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544022502_6495 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544022421_5609",
            "entity1544022467_4315",
            "entity1544021751_9133"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1016.3748168945,
              -274.77185058594,
              123.23783111572
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544022837_6518 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544022925_7078",
            "entity1544022894_5168",
            "entity1544022951_8056"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -393.06872558594,
              265.91027832031,
              69.962692260742
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544022894_5168 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544022925_7078",
            "entity1544022837_6518",
            "entity1544023188_8725",
            "entity1544023204_8926"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -398.52145385742,
              268.86236572266,
              70.105735778809
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544022925_7078 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544022894_5168",
            "entity1544022837_6518",
            "entity1544022951_8056",
            "entity1544022979_4851",
            "entity1544023188_8725"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -397.96939086914,
              271.37524414063,
              69.967254638672
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544022951_8056 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544022925_7078",
            "entity1544022837_6518",
            "entity1544022979_4851",
            "entity1544022995_2323"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -390.42181396484,
              272.17315673828,
              70.052307128906
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544022979_4851 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544022925_7078",
            "entity1544022951_8056",
            "entity1544022995_2323",
            "entity1544023059_3460",
            "entity1544023080_6105",
            "entity1544023136_9149"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -397.18402099609,
              279.21313476563,
              70.152282714844
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544022995_2323 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544022979_4851",
            "entity1544022951_8056",
            "entity1544023059_3460"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -390.77969360352,
              279.55484008789,
              70.031951904297
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023059_3460 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544022979_4851",
            "entity1544022995_2323",
            "entity1544023080_6105",
            "entity1544023092_7951"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -393.83734130859,
              284.79467773438,
              70.177101135254
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023080_6105 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544022979_4851",
            "entity1544023059_3460",
            "entity1544023092_7951",
            "entity1544023136_9149",
            "entity1544023478_2390"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -401.17544555664,
              281.86145019531,
              70.122276306152
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023092_7951 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023080_6105",
            "entity1544023059_3460"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -396.57229614258,
              287.26705932617,
              70.063018798828
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023136_9149 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544022979_4851",
            "entity1544023080_6105",
            "entity1544023478_2390",
            "entity1544023425_6659"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -402.78649902344,
              279.46392822266,
              70.053314208984
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023188_8725 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544022894_5168",
            "entity1544022925_7078",
            "entity1544023204_8926",
            "entity1544023274_9677"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -402.05786132813,
              271.0627746582,
              70.032028198242
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023204_8926 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544022894_5168",
            "entity1544023188_8725",
            "entity1544023274_9677",
            "entity1544023293_9235"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -404.73910522461,
              264.87124633789,
              70.101913452148
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023274_9677 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023188_8725",
            "entity1544023204_8926",
            "entity1544023293_9235",
            "entity1544023365_2489",
            "entity1544023354_6782"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -407.8844909668,
              269.65478515625,
              70.028114318848
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023293_9235 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023274_9677",
            "entity1544023204_8926",
            "entity1544023365_2489",
            "entity1544023696_3231"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -410.28692626953,
              266.37826538086,
              70.024482727051
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023354_6782 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023365_2489",
            "entity1544023274_9677",
            "entity1544023388_5065",
            "entity1544023425_6659"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -412.11907958984,
              273.98449707031,
              69.961166381836
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023365_2489 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023274_9677",
            "entity1544023293_9235",
            "entity1544023354_6782",
            "entity1544023388_5065",
            "entity1544023696_3231",
            "entity1544023719_6364"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -415.2194519043,
              270.79974365234,
              69.98265838623
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023388_5065 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023354_6782",
            "entity1544023365_2489",
            "entity1544023425_6659",
            "entity1544023408_5124",
            "entity1544082220_829"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -415.41241455078,
              275.36795043945,
              69.978202819824
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023408_5124 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023425_6659",
            "entity1544023388_5065",
            "entity1544023478_2390",
            "entity1544082220_829",
            "entity1544082206_1830"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -413.32791137695,
              280.84167480469,
              70.058219909668
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023425_6659 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023388_5065",
            "entity1544023354_6782",
            "entity1544023408_5124",
            "entity1544023136_9149",
            "entity1544023478_2390"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -410.03790283203,
              278.7490234375,
              70.051055908203
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023478_2390 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023080_6105",
            "entity1544023136_9149",
            "entity1544023425_6659",
            "entity1544023408_5124"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -407.3874206543,
              284.31088256836,
              70.140937805176
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023696_3231 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023293_9235",
            "entity1544023365_2489",
            "entity1544023719_6364",
            "entity1544023774_6349"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -422.5065612793,
              258.95401000977,
              71.969665527344
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023719_6364 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023365_2489",
            "entity1544023696_3231",
            "entity1544023774_6349",
            "entity1544023779_1967"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -426.86029052734,
              262.0673828125,
              72.468124389648
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023774_6349 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023696_3231",
            "entity1544023719_6364",
            "entity1544023779_1967",
            "entity1544023936_1773",
            "entity1544023929_4348",
            "entity1544024030_1254"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -430.14666748047,
              252.91610717773,
              75.866630554199
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023779_1967 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023774_6349",
            "entity1544023719_6364",
            "entity1544023929_4348",
            "entity1544024340_2099"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -434.23281860352,
              256.7678527832,
              75.976043701172
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023929_4348 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023936_1773",
            "entity1544023774_6349",
            "entity1544023779_1967",
            "entity1544024246_8980",
            "entity1544024328_5760",
            "entity1544024340_2099"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -439.86508178711,
              253.08149719238,
              76.182167053223
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544023936_1773 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023929_4348",
            "entity1544023774_6349",
            "entity1544024030_1254",
            "entity1544024098_7785",
            "entity1544024089_7640"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -433.12130737305,
              246.14575195313,
              76.049377441406
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544024030_1254 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023774_6349",
            "entity1544023936_1773",
            "entity1544024098_7785"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -428.98263549805,
              246.20620727539,
              75.841659545898
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544024089_7640 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544024098_7785",
            "entity1544023936_1773",
            "entity1544024127_845",
            "entity1544024164_1789"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -438.49401855469,
              240.71455383301,
              76.150123596191
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544024098_7785 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023936_1773",
            "entity1544024030_1254",
            "entity1544024089_7640",
            "entity1544024127_845"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -432.25848388672,
              239.42802429199,
              75.954658508301
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544024127_845 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544024089_7640",
            "entity1544024098_7785",
            "entity1544024164_1789",
            "entity1544024206_3913"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -439.68020629883,
              236.43788146973,
              75.956924438477
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544024164_1789 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544024089_7640",
            "entity1544024127_845",
            "entity1544024206_3913",
            "entity1544024246_8980"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -443.88088989258,
              241.59770202637,
              76.119430541992
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544024206_3913 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544024164_1789",
            "entity1544024127_845",
            "entity1544024246_8980",
            "entity1544024235_4590"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -445.67510986328,
              238.34117126465,
              75.918899536133
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544024235_4590 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544024206_3913",
            "entity1544024246_8980",
            "entity1544024261_7089"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -451.00064086914,
              243.90641784668,
              75.849578857422
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544024246_8980 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544024164_1789",
            "entity1544024206_3913",
            "entity1544024235_4590",
            "entity1544024261_7089",
            "entity1544024328_5760",
            "entity1544023929_4348"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -447.2038269043,
              248.71783447266,
              76.044898986816
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544024261_7089 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544024246_8980",
            "entity1544024235_4590",
            "entity1544024328_5760"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -451.06185913086,
              249.47807312012,
              75.806983947754
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544024328_5760 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544024246_8980",
            "entity1544024261_7089",
            "entity1544023929_4348",
            "entity1544024340_2099"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -447.47131347656,
              256.01647949219,
              75.994041442871
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544024340_2099 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023929_4348",
            "entity1544024328_5760",
            "entity1544023779_1967"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -440.55426025391,
              258.70416259766,
              75.956390380859
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544024752_6705 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544016730_9320",
            "entity1544016653_2630"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -430.09677124023,
              -493.91125488281,
              212.16841125488
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544024899_8566 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544016336_1253",
            "entity1544016653_2630"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -524.65356445313,
              -407.5295715332,
              212.17199707031
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544080809_9040 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543995092_8270",
            "entity1544080840_6272",
            "entity1544080961_9954",
            "entity1544080909_8096"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -61.02970123291,
              -216.80937194824,
              103.21778106689
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544080840_6272 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1543995092_8270",
            "entity1543995104_7996",
            "entity1544080809_9040",
            "entity1544080961_9954"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -63.034225463867,
              -222.36560058594,
              103.45801544189
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544080909_8096 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544080809_9040",
            "entity1544080961_9954",
            "entity1544081296_4331",
            "entity1544081248_1150"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -76.921661376953,
              -209.56944274902,
              101.9077911377
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544080961_9954 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544080809_9040",
            "entity1544080840_6272",
            "entity1544080909_8096",
            "entity1544081296_4331"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -79.290481567383,
              -215.18684387207,
              102.07144927979
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544081248_1150 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544081296_4331",
            "entity1544080909_8096",
            "entity1544081412_517",
            "entity1544081363_5447"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -94.199935913086,
              -201.98803710938,
              101.94304656982
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544081296_4331 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544080909_8096",
            "entity1544080961_9954",
            "entity1544081248_1150",
            "entity1544081412_517"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -96.435997009277,
              -207.65766906738,
              101.93027496338
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544081363_5447 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544081412_517",
            "entity1544081248_1150",
            "entity1544081464_9346",
            "entity1544081475_8174",
            "entity1544081834_2343"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -107.81323242188,
              -195.91088867188,
              103.94697570801
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544081412_517 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544081248_1150",
            "entity1544081296_4331",
            "entity1544081363_5447",
            "entity1544081464_9346",
            "entity1544081553_2948"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -110.36917877197,
              -201.71766662598,
              103.95908355713
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544081464_9346 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544081412_517",
            "entity1544081363_5447",
            "entity1544081475_8174",
            "entity1544081553_2948",
            "entity1544081583_5278",
            "entity1544081590_335"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -115.88339233398,
              -197.91654968262,
              104.0325012207
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544081475_8174 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544081464_9346",
            "entity1544081363_5447",
            "entity1544081761_3970",
            "entity1544081787_8499",
            "entity1544081834_2343"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -113.53881072998,
              -191.93716430664,
              104.14887237549
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544081553_2948 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544081412_517",
            "entity1544081464_9346",
            "entity1544081583_5278"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -116.39608001709,
              -204.4811706543,
              103.92713928223
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544081583_5278 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544081464_9346",
            "entity1544081553_2948",
            "entity1544081590_335",
            "entity1544081597_2102"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -123.68941497803,
              -204.42779541016,
              104.03771972656
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544081590_335 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544081464_9346",
            "entity1544081583_5278",
            "entity1544081597_2102",
            "entity1544081604_5340"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -121.96212768555,
              -197.70040893555,
              104.1051940918
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544081597_2102 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544081590_335",
            "entity1544081583_5278",
            "entity1544081604_5340",
            "entity1544081610_3182"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -128.83821105957,
              -199.75381469727,
              104.03666687012
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544081604_5340 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544081597_2102",
            "entity1544081590_335",
            "entity1544081610_3182",
            "entity1544081698_9244"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -124.29602813721,
              -193.43890380859,
              104.16915130615
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544081610_3182 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544081604_5340",
            "entity1544081597_2102",
            "entity1544081698_9244",
            "entity1544081729_1198"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -130.44012451172,
              -192.00788879395,
              103.99498748779
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544081698_9244 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544081604_5340",
            "entity1544081610_3182",
            "entity1544081729_1198",
            "entity1544081745_4030",
            "entity1544081761_3970"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -122.2003326416,
              -189.28762817383,
              104.0425567627
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544081729_1198 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544081698_9244",
            "entity1544081610_3182",
            "entity1544081745_4030"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -128.52369689941,
              -185.75758361816,
              103.92356872559
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544081745_4030 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544081698_9244",
            "entity1544081729_1198",
            "entity1544081761_3970",
            "entity1544081787_8499"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -122.29707336426,
              -181.5556640625,
              103.89868164063
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544081761_3970 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544081698_9244",
            "entity1544081745_4030",
            "entity1544081787_8499",
            "entity1544081475_8174"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -117.6312789917,
              -187.82269287109,
              104.14250183105
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544081787_8499 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544081761_3970",
            "entity1544081745_4030",
            "entity1544081475_8174",
            "entity1544081834_2343"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -114.20166778564,
              -182.12808227539,
              104.08937072754
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544081834_2343 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544081475_8174",
            "entity1544081787_8499",
            "entity1544081363_5447"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -108.03192901611,
              -187.55155944824,
              104.08887481689
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544082206_1830 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023408_5124",
            "entity1544082220_829",
            "entity1544082250_3653",
            "entity1544082243_5784"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -426.32369995117,
              287.39147949219,
              70.523849487305
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544082220_829 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544023408_5124",
            "entity1544023388_5065",
            "entity1544082206_1830",
            "entity1544082250_3653"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -428.55163574219,
              282.63552856445,
              70.656715393066
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544082243_5784 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544082250_3653",
            "entity1544082206_1830",
            "entity1544082355_1236",
            "entity1544082388_7940",
            "entity1544082402_5952"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -439.7360534668,
              293.78100585938,
              73.368698120117
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544082250_3653 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544082220_829",
            "entity1544082206_1830",
            "entity1544082243_5784",
            "entity1544082355_1236",
            "entity1544082513_5772"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -441.58972167969,
              288.55419921875,
              73.443145751953
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544082355_1236 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544082243_5784",
            "entity1544082250_3653",
            "entity1544082388_7940",
            "entity1544082493_9519",
            "entity1544082513_5772"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -443.17831420898,
              290.83139038086,
              73.292091369629
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544082388_7940 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544082243_5784",
            "entity1544082355_1236",
            "entity1544082402_5952",
            "entity1544082409_4084"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -442.91323852539,
              295.43212890625,
              73.374305725098
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544082402_5952 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544082243_5784",
            "entity1544082388_7940",
            "entity1544082409_4084",
            "entity1544082416_9719"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -440.68255615234,
              297.89019775391,
              73.348190307617
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544082409_4084 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544082402_5952",
            "entity1544082388_7940",
            "entity1544082416_9719",
            "entity1544082437_9429",
            "entity1544082425_6108"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -445.42919921875,
              298.73742675781,
              73.411399841309
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544082416_9719 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544082409_4084",
            "entity1544082402_5952",
            "entity1544082437_9429"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -443.69763183594,
              301.47637939453,
              73.379844665527
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544082425_6108 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544082437_9429",
            "entity1544082409_4084",
            "entity1544082448_4798",
            "entity1544082456_8146"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -449.80783081055,
              300.03955078125,
              73.411499023438
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544082437_9429 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544082416_9719",
            "entity1544082409_4084",
            "entity1544082425_6108",
            "entity1544082448_4798"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -447.93511962891,
              303.24890136719,
              73.438362121582
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544082448_4798 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544082437_9429",
            "entity1544082425_6108",
            "entity1544082456_8146",
            "entity1544082465_5415"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -453.21002197266,
              302.72909545898,
              73.530906677246
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544082456_8146 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544082448_4798",
            "entity1544082425_6108",
            "entity1544082465_5415",
            "entity1544082473_9570"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -453.31735229492,
              297.91387939453,
              73.425331115723
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544082465_5415 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544082448_4798",
            "entity1544082456_8146",
            "entity1544082473_9570",
            "entity1544082483_6191"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -457.63632202148,
              298.23278808594,
              73.304985046387
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544082473_9570 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544082456_8146",
            "entity1544082465_5415",
            "entity1544082483_6191",
            "entity1544082506_8780",
            "entity1544082493_9519"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -455.42126464844,
              294.28518676758,
              73.33512878418
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544082483_6191 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544082473_9570",
            "entity1544082465_5415",
            "entity1544082506_8780"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -458.02301025391,
              290.53778076172,
              73.302124023438
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544082493_9519 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544082473_9570",
            "entity1544082506_8780",
            "entity1544082513_5772",
            "entity1544082355_1236"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -448.85565185547,
              289.04629516602,
              73.385543823242
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544082506_8780 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544082473_9570",
            "entity1544082483_6191",
            "entity1544082493_9519",
            "entity1544082513_5772"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -453.1369934082,
              285.84289550781,
              73.394493103027
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544082513_5772 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544082493_9519",
            "entity1544082506_8780",
            "entity1544082355_1236",
            "entity1544082250_3653"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -445.67324829102,
              285.30676269531,
              73.411628723145
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544100026_8289 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_checkpoint_area.fbx"
        },
        NameComponent = {
          name = "checkpoint_area"
        },
        TransformComponent = {
          transform = {
            position = {
              -91.995941162109,
              -255.58233642578,
              162.40544128418
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 230.0
          }
        }
      }
    },
    entity1544100397_8110 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_checkpoint_area.fbx"
        },
        NameComponent = {
          name = "checkpoint_area"
        },
        TransformComponent = {
          transform = {
            position = {
              -448.99105834961,
              317.67236328125,
              91.554092407227
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 194.28503417969
          }
        }
      }
    },
    entity1544100483_6714 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_checkpoint_area.fbx"
        },
        NameComponent = {
          name = "checkpoint_area"
        },
        TransformComponent = {
          transform = {
            position = {
              -947.66137695313,
              -325.91094970703,
              126.80224609375
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 230.0
          }
        }
      }
    },
    entity1544100873_2697 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -775.09954833984,
              -96.034530639648,
              236.73030090332
            },
            rotation = {
              0.5189460515976,
              0.48526474833488,
              -0.49771043658257,
              0.49749109148979
            },
            scale = 0.94335800409317
          }
        }
      }
    },
    entity1544100873_5186 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_player_checkpoint.fbx"
        },
        NameComponent = {
          name = "spawn_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -776.40576171875,
              -96.101280212402,
              237.14265441895
            },
            rotation = {
              0.00036721955984831,
              0.00010899553308263,
              0.92105281352997,
              0.38943752646446
            },
            scale = 2.2971346378326
          }
        }
      }
    },
    entity1544101084_3182 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_checkpoint_area.fbx"
        },
        NameComponent = {
          name = "checkpoint_area"
        },
        TransformComponent = {
          transform = {
            position = {
              -506.95013427734,
              -480.88442993164,
              206.34823608398
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 201.0933380127
          }
        }
      }
    },
    entity1544101211_1493 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -283.71463012695,
              -623.30181884766,
              328.43746948242
            },
            rotation = {
              0.71045994758606,
              0.0056449077092111,
              -0.70311629772186,
              0.029021659865975
            },
            scale = 0.94335758686066
          }
        }
      }
    },
    entity1544101211_3722 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_player_checkpoint.fbx"
        },
        NameComponent = {
          name = "spawn_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -283.89297485352,
              -622.02215576172,
              328.70132446289
            },
            rotation = {
              0.00035624232259579,
              -0.00014079453831073,
              0.4845617711544,
              0.87475693225861
            },
            scale = 2.2971534729004
          }
        }
      }
    },
    entity1544111222_9316 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_checkpoint_area.fbx"
        },
        NameComponent = {
          name = "checkpoint_area"
        },
        TransformComponent = {
          transform = {
            position = {
              -463.43380737305,
              54.909973144531,
              132.08692932129
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 37.879428863525
          }
        }
      }
    },
    entity1544184560_1840 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_checkpoint_area.fbx"
        },
        NameComponent = {
          name = "checkpoint_area"
        },
        TransformComponent = {
          transform = {
            position = {
              -497.12005615234,
              -712.74786376953,
              885.79248046875
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 466.55471801758
          }
        }
      }
    },
    entity1544184647_4201 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_checkpoint_area.fbx"
        },
        NameComponent = {
          name = "checkpoint_area"
        },
        TransformComponent = {
          transform = {
            position = {
              -333.38043212891,
              -36.331893920898,
              658.01342773438
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 294.65545654297
          }
        }
      }
    },
    entity1544185238_1496 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -437.89169311523,
              243.55480957031,
              611.89544677734
            },
            rotation = {
              0.14548063278198,
              0.69542843103409,
              -0.12107825279236,
              0.69322055578232
            },
            scale = 0.94335782527924
          }
        }
      }
    },
    entity1544185238_5387 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_player_checkpoint.fbx"
        },
        NameComponent = {
          name = "spawn_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -438.33065795898,
              242.27334594727,
              612.50305175781
            },
            rotation = {
              0.00022414721024688,
              0.00031062337802723,
              0.98588627576828,
              -0.1674161106348
            },
            scale = 2.2971432209015
          }
        }
      }
    },
    entity1544185311_1963 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -807.02130126953,
              -405.23919677734,
              651.17315673828
            },
            rotation = {
              -0.37847754359245,
              0.6012806892395,
              0.39449790120125,
              0.58274149894714
            },
            scale = 0.94335746765137
          }
        }
      }
    },
    entity1544185311_8112 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_player_checkpoint.fbx"
        },
        NameComponent = {
          name = "spawn_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -805.78863525391,
              -405.66812133789,
              651.64581298828
            },
            rotation = {
              5.4558182455366e-05,
              -0.0003791470953729,
              -0.59283077716827,
              0.80532693862915
            },
            scale = 2.297167301178
          }
        }
      }
    },
    entity1544454608_8721 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_quest_item.fbx"
        },
        NameComponent = {
          name = "cole_teleport_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -205.13507080078,
              -224.88746643066,
              564.25408935547
            },
            rotation = {
              0.0,
              0.70710688829422,
              0.70710664987564,
              0.0
            },
            scale = 9.4959287643433
          }
        }
      }
    },
    entity1544454718_3223 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_quest_item.fbx"
        },
        NameComponent = {
          name = "cole_teleport_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -521.49334716797,
              -457.52932739258,
              618.79254150391
            },
            rotation = {
              0.0,
              0.70710688829422,
              0.70710664987564,
              0.0
            },
            scale = 9.4959287643433
          }
        }
      }
    },
    entity1544454808_8811 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_quest_item.fbx"
        },
        NameComponent = {
          name = "cole_teleport_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -748.44134521484,
              -206.36039733887,
              622.40924072266
            },
            rotation = {
              0.0,
              0.70710688829422,
              0.70710664987564,
              0.0
            },
            scale = 9.4959287643433
          }
        }
      }
    },
    entity1544454831_6616 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_quest_item.fbx"
        },
        NameComponent = {
          name = "cole_teleport_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -300.59820556641,
              72.541809082031,
              539.25567626953
            },
            rotation = {
              0.0,
              0.70710688829422,
              0.70710664987564,
              0.0
            },
            scale = 9.4959287643433
          }
        }
      }
    },
    entity1544454944_2165 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_quest_item.fbx"
        },
        NameComponent = {
          name = "cole_teleport_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -462.39013671875,
              -348.94772338867,
              497.67459106445
            },
            rotation = {
              0.0,
              0.70710688829422,
              0.70710664987564,
              0.0
            },
            scale = 9.4959287643433
          }
        }
      }
    },
    entity1544454992_3960 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_quest_item.fbx"
        },
        NameComponent = {
          name = "cole_teleport_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -739.56060791016,
              -289.52493286133,
              447.87710571289
            },
            rotation = {
              0.0,
              0.70710688829422,
              0.70710664987564,
              0.0
            },
            scale = 9.4959287643433
          }
        }
      }
    },
    entity1544455026_6412 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_quest_item.fbx"
        },
        NameComponent = {
          name = "cole_teleport_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -576.86029052734,
              134.21224975586,
              451.23962402344
            },
            rotation = {
              0.0,
              0.70710688829422,
              0.70710664987564,
              0.0
            },
            scale = 9.4959287643433
          }
        }
      }
    },
    entity1544455178_7991 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_quest_item.fbx"
        },
        NameComponent = {
          name = "cole_teleport_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -447.4211730957,
              -189.52062988281,
              584.44995117188
            },
            rotation = {
              0.0,
              0.70710688829422,
              0.70710664987564,
              0.0
            },
            scale = 9.4959287643433
          }
        }
      }
    },
    entity1544536302_2166 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_checkpoint_area.fbx"
        },
        NameComponent = {
          name = "checkpoint_area"
        },
        TransformComponent = {
          transform = {
            position = {
              -909.75433349609,
              -161.38342285156,
              652.97015380859
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 249.39842224121
          }
        }
      }
    },
    entity1544536430_1976 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -1024.8850097656,
              -272.17419433594,
              568.14013671875
            },
            rotation = {
              -0.53389167785645,
              0.46877130866051,
              0.54397308826447,
              0.44643738865852
            },
            scale = 0.94335794448853
          }
        }
      }
    },
    entity1544536430_226 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_player_checkpoint.fbx"
        },
        NameComponent = {
          name = "spawn_point"
        },
        TransformComponent = {
          transform = {
            position = {
              -1023.6265869141,
              -271.85507202148,
              568.556640625
            },
            rotation = {
              5.2430532377912e-05,
              -0.00037944744690321,
              -0.52375000715256,
              0.85187196731567
            },
            scale = 2.2971410751343
          }
        }
      }
    },
    entity1544628212_3382 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 1.0,
          max_spawns = 4.0,
          min_dist_to_player = 300.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -543.28228759766,
              -528.76788330078,
              91.759262084961
            },
            rotation = {
              -0.0056437882594764,
              0.006840668618679,
              0.98139488697052,
              -0.19179536402225
            },
            scale = 0.8525385260582
          }
        }
      }
    },
    entity1544628212_6388 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -547.96759033203,
              -540.86328125,
              91.167961120605
            },
            rotation = {
              -0.0067519601434469,
              0.0057493462227285,
              0.99980866909027,
              -0.017434796318412
            },
            scale = 1.6747208833694
          }
        }
      }
    },
    entity1544628621_8095 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -537.71618652344,
              -541.06805419922,
              91.159477233887
            },
            rotation = {
              -0.0065015470609069,
              0.0060306754894555,
              0.9996474981308,
              0.025024093687534
            },
            scale = 1.674729347229
          }
        }
      }
    },
    entity1544628665_812 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -568.96228027344,
              -532.35778808594,
              91.205619812012
            },
            rotation = {
              -0.0085419761016965,
              0.002382205799222,
              0.91469067335129,
              0.40405732393265
            },
            scale = 1.6747359037399
          }
        }
      }
    },
    entity1544628721_1383 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -575.77270507813,
              -509.47048950195,
              91.195739746094
            },
            rotation = {
              -0.00071460177423432,
              0.0088392794132233,
              0.71974503993988,
              0.6941819190979
            },
            scale = 1.6746927499771
          }
        }
      }
    },
    entity1544628795_4077 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -575.08575439453,
              -496.54446411133,
              91.153800964355
            },
            rotation = {
              -0.00071466079680249,
              0.0088391685858369,
              0.71974503993988,
              0.6941819190979
            },
            scale = 1.6747018098831
          }
        }
      }
    },
    entity1544628834_6506 = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/hut01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hut01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -577.16021728516,
              -479.00997924805,
              91.279922485352
            },
            rotation = {
              -0.00033912845537998,
              0.0088614784181118,
              0.7485368847847,
              0.66303378343582
            },
            scale = 1.6746838092804
          }
        }
      }
    },
    entity1544628880_7924 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 1.0,
          max_spawns = 4.0,
          min_dist_to_player = 300.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -564.16534423828,
              -519.22479248047,
              92.753707885742
            },
            rotation = {
              -0.0056436443701386,
              0.0068406593054533,
              0.98139601945877,
              -0.19178947806358
            },
            scale = 0.85254770517349
          }
        }
      }
    },
    entity1544628890_3633 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_enemy_spawn.fbx"
        },
        SpawnComponent = {
          delay = 1.0,
          max_spawns = 4.0,
          min_dist_to_player = 300.0,
          type = 0.0
        },
        TransformComponent = {
          transform = {
            position = {
              -562.82470703125,
              -480.89935302734,
              92.759086608887
            },
            rotation = {
              -0.0056437198072672,
              0.0068406783975661,
              0.98139572143555,
              -0.19179098308086
            },
            scale = 0.85255110263824
          }
        }
      }
    },
    entity1544630678_6173 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544630734_1150",
            "entity1544630733_3971",
            "entity1544631694_7629"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -587.54089355469,
              -442.63726806641,
              91.403312683105
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544630733_3971 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544630734_1150",
            "entity1544630678_6173",
            "entity1544630877_3606",
            "entity1544631694_7629",
            "entity1544631650_3601"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -554.56829833984,
              -451.94094848633,
              91.783683776855
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544630734_1150 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544630733_3971",
            "entity1544630678_6173",
            "entity1544630877_3606",
            "entity1544631034_1385"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -574.49102783203,
              -474.03002929688,
              91.339294433594
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544630877_3606 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544630733_3971",
            "entity1544630734_1150",
            "entity1544631034_1385",
            "entity1544630953_7915",
            "entity1544631199_9934"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -543.37426757813,
              -508.24609375,
              91.596755981445
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544630953_7915 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544630877_3606",
            "entity1544631034_1385",
            "entity1544631093_1001",
            "entity1544631199_9934",
            "entity1544631185_3505"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -528.51599121094,
              -538.43377685547,
              91.308601379395
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544631034_1385 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544630877_3606",
            "entity1544630734_1150",
            "entity1544630953_7915",
            "entity1544631093_1001"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -570.75,
              -521.66479492188,
              91.329216003418
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544631093_1001 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544630953_7915",
            "entity1544631034_1385"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -561.65417480469,
              -538.08856201172,
              91.434280395508
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544631185_3505 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544630953_7915",
            "entity1544631199_9934",
            "entity1544631472_9515"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -497.91314697266,
              -545.00531005859,
              91.615821838379
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544631199_9934 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544630877_3606",
            "entity1544630953_7915",
            "entity1544631185_3505",
            "entity1544631504_7475",
            "entity1544631472_9515"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -490.28918457031,
              -509.41802978516,
              92.564041137695
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544631472_9515 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544631531_8145",
            "entity1544631504_7475",
            "entity1544631199_9934",
            "entity1544631185_3505"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -457.18829345703,
              -543.29058837891,
              91.57746887207
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544631504_7475 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544631531_8145",
            "entity1544631472_9515",
            "entity1544631199_9934",
            "entity1544631805_4178"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -474.32955932617,
              -487.123046875,
              92.580352783203
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544631531_8145 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544631504_7475",
            "entity1544631472_9515",
            "entity1544631805_4178",
            "entity1544631832_5872"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -432.31228637695,
              -486.57135009766,
              91.480918884277
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544631650_3601 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544631694_7629",
            "entity1544630733_3971",
            "entity1544631778_6438",
            "entity1544631805_4178"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -521.3828125,
              -435.19345092773,
              92.217269897461
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544631694_7629 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544630733_3971",
            "entity1544630678_6173",
            "entity1544631650_3601",
            "entity1544631778_6438"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -554.00201416016,
              -413.76620483398,
              91.29035949707
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544631778_6438 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544631650_3601",
            "entity1544631694_7629",
            "entity1544631805_4178",
            "entity1544631822_3738"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -506.9443359375,
              -400.39135742188,
              91.341590881348
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544631805_4178 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544631504_7475",
            "entity1544631531_8145",
            "entity1544631832_5872",
            "entity1544631822_3738",
            "entity1544631778_6438",
            "entity1544631650_3601"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -475.26150512695,
              -444.44750976563,
              93.227569580078
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544631822_3738 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544631832_5872",
            "entity1544631805_4178",
            "entity1544631778_6438"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -470.77163696289,
              -408.01235961914,
              91.633995056152
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544631832_5872 = {
      components = {
        MeshNodeComponent = {
          mesh = "navmesh",
          neighbors = {
            "entity1544631531_8145",
            "entity1544631805_4178",
            "entity1544631822_3738"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -438.76742553711,
              -434.9765625,
              93.186958312988
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544701250_2648 = {
      components = {
        ModelComponent = {
          asset = "assets/model/editor_quest_item.fbx"
        },
        NameComponent = {
          name = "note_spawn"
        },
        TransformComponent = {
          transform = {
            position = {
              -557.49584960938,
              -534.05810546875,
              92.535865783691
            },
            rotation = {
              0.0,
              0.70710688829422,
              0.70710664987564,
              0.0
            },
            scale = 2.7955737113953
          }
        }
      }
    },
    entity1544711298_4265 = {
      components = {
        GraphicTextComponent = {
          color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          font = "arial",
          outline_thickness = 0.070000000298023,
          text = "The note says that it's in fact"
        },
        NameComponent = {
          name = "note_pickup_desc"
        },
        TransformComponent = {
          transform = {
            position = {
              -550.64862060547,
              -537.95971679688,
              96.246971130371
            },
            rotation = {
              0.0,
              0.0,
              0.99943268299103,
              0.033679272979498
            },
            scale = 1.0000270605087
          }
        }
      }
    },
    entity1544781224_1643 = {
      components = {
        GraphicTextComponent = {
          color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          font = "arial",
          outline_thickness = 0.070000000298023,
          text = "The horn is not here!"
        },
        NameComponent = {
          name = "note_pickup_desc"
        },
        TransformComponent = {
          transform = {
            position = {
              -550.66571044922,
              -537.98718261719,
              97.432685852051
            },
            rotation = {
              0.0,
              0.0,
              0.99914932250977,
              0.041238900274038
            },
            scale = 1.0000059604645
          }
        }
      }
    },
    entity1544781283_8852 = {
      components = {
        GraphicTextComponent = {
          color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          font = "arial",
          outline_thickness = 0.070000000298023,
          text = "in one of the 3 outer camps."
        },
        NameComponent = {
          name = "note_pickup_desc"
        },
        TransformComponent = {
          transform = {
            position = {
              -550.61505126953,
              -538.0185546875,
              95.032989501953
            },
            rotation = {
              0.0,
              0.0,
              0.99940174818039,
              0.034585278481245
            },
            scale = 1.0000026226044
          }
        }
      }
    },
    entity1544781482_9906 = {
      components = {
        GraphicTextComponent = {
          color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          font = "arial",
          outline_thickness = 0.070000000298023,
          text = "Raiders are destroying the trees!"
        },
        NameComponent = {
          name = "start_quest_desc"
        },
        TransformComponent = {
          transform = {
            position = {
              -474.05001831055,
              45.503429412842,
              139.19401550293
            },
            rotation = {
              0.0,
              0.0,
              0.82477158308029,
              0.56546610593796
            },
            scale = 0.76983451843262
          }
        }
      }
    },
    entity1544781834_51 = {
      components = {
        GraphicTextComponent = {
          color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          font = "arial",
          outline_thickness = 0.070000000298023,
          text = "Find the horn to call for"
        },
        NameComponent = {
          name = "start_quest_desc"
        },
        TransformComponent = {
          transform = {
            position = {
              -474.0466003418,
              45.504505157471,
              138.37168884277
            },
            rotation = {
              0.0,
              0.0,
              0.82477158308029,
              0.56546610593796
            },
            scale = 0.76983451843262
          }
        }
      }
    },
    entity1544781878_685 = {
      components = {
        GraphicTextComponent = {
          color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          font = "arial",
          outline_thickness = 0.070000000298023,
          text = "their leader and kill him!"
        },
        NameComponent = {
          name = "start_quest_desc"
        },
        TransformComponent = {
          transform = {
            position = {
              -474.04711914063,
              45.49976348877,
              137.56338500977
            },
            rotation = {
              0.0,
              0.0,
              0.82477158308029,
              0.56546610593796
            },
            scale = 0.76983451843262
          }
        }
      }
    },
    platform_curved_railing = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/platform_curved.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/platform_curved.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -466.73175048828,
              38.675674438477,
              135.80340576172
            },
            rotation = {
              0.32682237029076,
              0.62454581260681,
              -0.33239075541496,
              0.62661474943161
            },
            scale = 1.4834102392197
          }
        }
      }
    },
    preset_instance1542355982_5635 = {
      components = {
        PresetInstanceComponent = {
          name = "player"
        },
        TransformComponent = {
          transform = {
            position = {
              -464.57766723633,
              58.629878997803,
              136.09996032715
            },
            rotation = {
              0.0,
              0.0,
              -0.06761546432972,
              0.99771147966385
            },
            scale = 1.0000013113022
          }
        }
      }
    },
    preset_instance1542638583_2904 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -11.408226966858,
              -24.29372215271,
              -105.29347229004
            },
            rotation = {
              0.16741625964642,
              -0.831647336483,
              -0.15262924134731,
              0.5069899559021
            },
            scale = 1.0000044107437
          }
        }
      }
    },
    preset_instance1542638631_3107 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -13.137816429138,
              -28.014375686646,
              -59.505111694336
            },
            rotation = {
              0.5388560295105,
              -0.39414265751839,
              0.35006326436996,
              0.65707039833069
            },
            scale = 1.000004529953
          }
        }
      }
    },
    preset_instance1542700577_157 = {
      components = {
        PresetInstanceComponent = {
          name = "branch2"
        },
        TransformComponent = {
          transform = {
            position = {
              -12.872698783875,
              -30.961944580078,
              -115.95240020752
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1542700636_3210 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -11.094467163086,
              -28.90732383728,
              87.099273681641
            },
            rotation = {
              0.16864436864853,
              -0.021872924640775,
              -0.075583331286907,
              0.98253130912781
            },
            scale = 1.3044391870499
          }
        }
      }
    },
    preset_instance1542700701_1888 = {
      components = {
        PresetInstanceComponent = {
          name = "branch2"
        },
        TransformComponent = {
          transform = {
            position = {
              -11.313015937805,
              -27.244119644165,
              162.68124389648
            },
            rotation = {
              -0.092260949313641,
              -0.0090997898951173,
              0.59950894117355,
              0.7949805855751
            },
            scale = 0.96386736631393
          }
        }
      }
    },
    preset_instance1542702805_2753 = {
      components = {
        PresetInstanceComponent = {
          name = "branch3"
        },
        TransformComponent = {
          transform = {
            position = {
              -11.829016685486,
              -29.228034973145,
              19.423309326172
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1542705677_8280 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -13.796637535095,
              -26.79154586792,
              138.15759277344
            },
            rotation = {
              -0.12539906799793,
              0.3955269753933,
              0.47473242878914,
              0.7761846780777
            },
            scale = 1.943284869194
          }
        }
      }
    },
    preset_instance1542723463_2365 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -8.7474784851074,
              -26.391363143921,
              247.41473388672
            },
            rotation = {
              0.13639791309834,
              -0.099615179002285,
              -0.22247916460037,
              0.96019554138184
            },
            scale = 1.5967619419098
          }
        }
      }
    },
    preset_instance1542723873_8539 = {
      components = {
        PresetInstanceComponent = {
          name = "branch3"
        },
        TransformComponent = {
          transform = {
            position = {
              -11.304940223694,
              -29.192850112915,
              210.34140014648
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.2527257204056
          }
        }
      }
    },
    preset_instance1542724679_8753 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -226.10827636719,
              -58.899078369141,
              263.54833984375
            },
            rotation = {
              -6.9478300890324e-10,
              4.1066332556738e-09,
              -0.80729287862778,
              0.59015101194382
            },
            scale = 1.0000023841858
          }
        }
      }
    },
    preset_instance1542808971_2682 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -373.27108764648,
              -36.52225112915,
              253.79695129395
            },
            rotation = {
              -1.1271680117053e-10,
              -4.659202884616e-11,
              0.99603492021561,
              -0.088963270187378
            },
            scale = 1.0000040531158
          }
        }
      }
    },
    preset_instance1542809070_3464 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -521.26580810547,
              224.42358398438,
              240.23249816895
            },
            rotation = {
              0.0,
              0.0,
              0.86794936656952,
              -0.49665275216103
            },
            scale = 0.9999994635582
          }
        }
      }
    },
    preset_instance1542809104_9253 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -568.42584228516,
              -157.02270507813,
              350.20190429688
            },
            rotation = {
              0.0,
              0.0,
              0.76142507791519,
              0.64825296401978
            },
            scale = 1.0000005960464
          }
        }
      }
    },
    preset_instance1542809316_1673 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -117.19617462158,
              -190.81881713867,
              165.45639038086
            },
            rotation = {
              0.099358625710011,
              0.038840666413307,
              -0.011870235204697,
              0.99422246217728
            },
            scale = 1.3978143930435
          }
        }
      }
    },
    preset_instance1542809360_3957 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -120.21906280518,
              -192.77145385742,
              229.39224243164
            },
            rotation = {
              0.63512402772903,
              0.34160041809082,
              0.69229888916016,
              -0.025473793968558
            },
            scale = 1.0710437297821
          }
        }
      }
    },
    preset_instance1542809413_6695 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -120.11038970947,
              -190.9641418457,
              246.22415161133
            },
            rotation = {
              0.054642200469971,
              0.16053369641304,
              0.89562612771988,
              0.41121405363083
            },
            scale = 1.2163020372391
          }
        }
      }
    },
    preset_instance1542809446_6359 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -118.64604949951,
              -192.69204711914,
              273.00393676758
            },
            rotation = {
              0.20207247138023,
              0.021439872682095,
              0.0069159246049821,
              0.97911143302917
            },
            scale = 1.0000004768372
          }
        }
      }
    },
    preset_instance1542809467_9206 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -119.64743804932,
              -192.23306274414,
              80.960113525391
            },
            rotation = {
              0.15205974876881,
              -0.0314298607409,
              -0.012541869655252,
              0.98779183626175
            },
            scale = 1.0000007152557
          }
        }
      }
    },
    preset_instance1542809505_8978 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -120.78195953369,
              -194.40762329102,
              39.086669921875
            },
            rotation = {
              0.13499842584133,
              -0.097440354526043,
              -0.52560180425644,
              0.83428025245667
            },
            scale = 1.0000027418137
          }
        }
      }
    },
    preset_instance1542809539_7273 = {
      components = {
        PresetInstanceComponent = {
          name = "branch3"
        },
        TransformComponent = {
          transform = {
            position = {
              -117.64673614502,
              -194.00003051758,
              56.410034179688
            },
            rotation = {
              -0.025708328932524,
              -0.0016558935167268,
              0.43060749769211,
              0.90217155218124
            },
            scale = 1.0031126737595
          }
        }
      }
    },
    preset_instance1542809583_8052 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -95.601852416992,
              -314.84008789063,
              -102.18794250488
            },
            rotation = {
              0.10639534890652,
              0.014693863689899,
              -0.044973731040955,
              0.99319761991501
            },
            scale = 1.0000014305115
          }
        }
      }
    },
    preset_instance1542809750_9530 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree2"
        },
        TransformComponent = {
          transform = {
            position = {
              -324.02838134766,
              200.33604431152,
              289.34216308594
            },
            rotation = {
              -2.6416542286967e-09,
              -6.2786714716268e-10,
              -0.3022668659687,
              0.95322334766388
            },
            scale = 1.0000010728836
          }
        }
      }
    },
    preset_instance1542814628_5166 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -474.0735168457,
              45.466472625732,
              278.48587036133
            },
            rotation = {
              -1.2900104096047e-08,
              -1.8253418332392e-08,
              0.81664437055588,
              0.57714122533798
            },
            scale = 1.0000003576279
          }
        }
      }
    },
    preset_instance1542814655_6338 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -472.38540649414,
              46.294780731201,
              188.31561279297
            },
            rotation = {
              0.061318214982748,
              0.1384047716856,
              0.92196500301361,
              0.35646146535873
            },
            scale = 1.0000007152557
          }
        }
      }
    },
    preset_instance1542877704_9801 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -457.62857055664,
              62.94758605957,
              139.13410949707
            },
            rotation = {
              3.4482486310772e-10,
              -1.716784159278e-09,
              0.45055320858955,
              0.89274960756302
            },
            scale = 1.0000011920929
          }
        }
      }
    },
    preset_instance1542878986_2210 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -476.62841796875,
              62.492034912109,
              139.10807800293
            },
            rotation = {
              7.3175914216961e-09,
              -1.4204897258452e-09,
              0.97524380683899,
              0.22113241255283
            },
            scale = 1.0000039339066
          }
        }
      }
    },
    preset_instance1542881414_9185 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -456.11404418945,
              58.031108856201,
              330.38040161133
            },
            rotation = {
              -1.2059454546076e-09,
              3.6466893948983e-09,
              0.90436339378357,
              -0.42676323652267
            },
            scale = 0.56187927722931
          }
        }
      }
    },
    preset_instance1542881725_9353 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -449.72790527344,
              57.54114151001,
              133.75024414063
            },
            rotation = {
              3.9333047235912e-11,
              -1.0956918011784e-10,
              0.94119322299957,
              -0.33786872029305
            },
            scale = 1.0000013113022
          }
        }
      }
    },
    preset_instance1542881854_9734 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -501.17013549805,
              -273.23303222656,
              263.62915039063
            },
            rotation = {
              4.6147799470653e-10,
              -6.8729182067795e-11,
              -0.085323266685009,
              0.996353328228
            },
            scale = 1.0000038146973
          }
        }
      }
    },
    preset_instance1542888345_2924 = {
      components = {
        PresetInstanceComponent = {
          name = "branch3"
        },
        TransformComponent = {
          transform = {
            position = {
              -118.33922576904,
              -192.05838012695,
              523.33203125
            },
            rotation = {
              -3.7678780234751e-11,
              7.8762989186298e-11,
              0.43154439330101,
              0.90209174156189
            },
            scale = 0.99999988079071
          }
        }
      }
    },
    preset_instance1542888345_7774 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -118.35015106201,
              -192.03762817383,
              454.48980712891
            },
            rotation = {
              0.11717075109482,
              -0.013647044077516,
              0.014335000887513,
              0.99291455745697
            },
            scale = 1.0000019073486
          }
        }
      }
    },
    preset_instance1542888345_7833 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -118.524269104,
              -191.38302612305,
              420.39114379883
            },
            rotation = {
              0.0,
              0.0,
              0.44606050848961,
              0.89500284194946
            },
            scale = 1.0000004768372
          }
        }
      }
    },
    preset_instance1542888345_9849 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -118.34078216553,
              -192.05422973633,
              406.19216918945
            },
            rotation = {
              0.0,
              0.0,
              0.68275427818298,
              0.73064810037613
            },
            scale = 1.0000002384186
          }
        }
      }
    },
    preset_instance1542888419_2101 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -119.59146881104,
              -192.85720825195,
              664.74011230469
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1542888419_2788 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -117.76767730713,
              -192.68881225586,
              799.72900390625
            },
            rotation = {
              0.11951239407063,
              -0.070521548390388,
              -0.51333111524582,
              0.8468970656395
            },
            scale = 1.0000003576279
          }
        }
      }
    },
    preset_instance1542888419_3896 = {
      components = {
        PresetInstanceComponent = {
          name = "branch3"
        },
        TransformComponent = {
          transform = {
            position = {
              -119.62815093994,
              -192.86038208008,
              713.45227050781
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1542888419_5898 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -119.5864944458,
              -192.77096557617,
              769.98071289063
            },
            rotation = {
              0.0,
              0.0,
              0.79126310348511,
              0.61147600412369
            },
            scale = 1.0000011920929
          }
        }
      }
    },
    preset_instance1542889232_1820 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -10.611741065979,
              -25.614459991455,
              490.35055541992
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1542889232_360 = {
      components = {
        PresetInstanceComponent = {
          name = "branch3"
        },
        TransformComponent = {
          transform = {
            position = {
              -10.619596481323,
              -25.611949920654,
              595.80950927734
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1542889232_4035 = {
      components = {
        PresetInstanceComponent = {
          name = "branch3"
        },
        TransformComponent = {
          transform = {
            position = {
              -8.1945505142212,
              -25.46053314209,
              716.01849365234
            },
            rotation = {
              -0.64652979373932,
              -0.34606000781059,
              0.67701357603073,
              -0.062404621392488
            },
            scale = 1.0000020265579
          }
        }
      }
    },
    preset_instance1542889232_5833 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -10.256828308105,
              -28.196475982666,
              451.21026611328
            },
            rotation = {
              0.0,
              0.0,
              -0.65110605955124,
              0.75898677110672
            },
            scale = 1.0000013113022
          }
        }
      }
    },
    preset_instance1542889232_7092 = {
      components = {
        PresetInstanceComponent = {
          name = "branch2"
        },
        TransformComponent = {
          transform = {
            position = {
              -10.649074554443,
              -25.621234893799,
              558.03143310547
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1542889232_915 = {
      components = {
        PresetInstanceComponent = {
          name = "branch5"
        },
        TransformComponent = {
          transform = {
            position = {
              -10.649074554443,
              -25.621324539185,
              558.03131103516
            },
            rotation = {
              0.091046549379826,
              0.0126769002527,
              0.066345654428005,
              0.99355328083038
            },
            scale = 1.0000003576279
          }
        }
      }
    },
    preset_instance1542889232_9274 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -10.665386199951,
              -27.301303863525,
              683.21063232422
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1542889379_498 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -10.803365707397,
              -27.451648712158,
              837.51617431641
            },
            rotation = {
              -0.047898039221764,
              0.066193163394928,
              0.91934394836426,
              -0.38487789034843
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    preset_instance1542890030_9810 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -497.69683837891,
              -90.556213378906,
              280.52398681641
            },
            rotation = {
              0.0,
              0.0,
              0.43264824151993,
              0.90156280994415
            },
            scale = 1.0000027418137
          }
        }
      }
    },
    preset_instance1542890361_5787 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -525.58795166016,
              -38.566707611084,
              399.70083618164
            },
            rotation = {
              1.521202434851e-11,
              2.7888155326727e-11,
              -0.086835250258446,
              0.99622267484665
            },
            scale = 1.0000027418137
          }
        }
      }
    },
    preset_instance1542965408_6359 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -671.59356689453,
              -406.06735229492,
              454.6926574707
            },
            rotation = {
              -1.8260344347709e-09,
              -3.6745512188574e-10,
              0.98034781217575,
              -0.19727675616741
            },
            scale = 1.0000030994415
          }
        }
      }
    },
    preset_instance1543223113_4579 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -75.818489074707,
              -311.72280883789,
              264.49688720703
            },
            rotation = {
              2.3524309381584e-18,
              9.409725406995e-18,
              0.67543852329254,
              0.73741632699966
            },
            scale = 1.0000033378601
          }
        }
      }
    },
    preset_instance1543223194_6637 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -617.50915527344,
              -593.05511474609,
              367.34829711914
            },
            rotation = {
              0.0,
              0.0,
              -0.27344453334808,
              0.96188777685165
            },
            scale = 1.0000005960464
          }
        }
      }
    },
    preset_instance1543223194_6653 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -361.86212158203,
              -533.92193603516,
              435.06860351563
            },
            rotation = {
              -1.6680343551954e-17,
              -1.9233482834714e-17,
              -0.28355148434639,
              0.95895701646805
            },
            scale = 1.0000004768372
          }
        }
      }
    },
    preset_instance1543223194_7336 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -511.24423217773,
              -630.16638183594,
              416.39038085938
            },
            rotation = {
              -4.0835265724404e-11,
              4.1479906315312e-11,
              -0.7126225233078,
              0.70154768228531
            },
            scale = 1.0000042915344
          }
        }
      }
    },
    preset_instance1543229070_1140 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -681.49371337891,
              44.691886901855,
              300.50057983398
            },
            rotation = {
              1.4021976288414e-11,
              -3.8911721067514e-12,
              -0.2674001455307,
              0.96358561515808
            },
            scale = 1.0000030994415
          }
        }
      }
    },
    preset_instance1543229070_4046 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -771.26788330078,
              -93.207855224609,
              325.1374206543
            },
            rotation = {
              -1.2035652474651e-09,
              -2.3081712097195e-09,
              0.95510095357895,
              0.29628059267998
            },
            scale = 1.0000028610229
          }
        }
      }
    },
    preset_instance1543229155_3880 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -652.39819335938,
              264.05044555664,
              79.027168273926
            },
            rotation = {
              0.0,
              0.0,
              0.93447303771973,
              -0.35603389143944
            },
            scale = 0.99999934434891
          }
        }
      }
    },
    preset_instance1543229259_6548 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree2"
        },
        TransformComponent = {
          transform = {
            position = {
              -406.74307250977,
              275.09881591797,
              119.38437652588
            },
            rotation = {
              0.0,
              0.0,
              0.41693499684334,
              0.90893632173538
            },
            scale = 1.0000015497208
          }
        }
      }
    },
    preset_instance1543229866_6115 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -408.12799072266,
              279.6462097168,
              74.652786254883
            },
            rotation = {
              0.0,
              0.0,
              -0.42447590827942,
              0.90543925762177
            },
            scale = 1.0000016689301
          }
        }
      }
    },
    preset_instance1543229976_7931 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree2"
        },
        TransformComponent = {
          transform = {
            position = {
              -440.92385864258,
              248.15547180176,
              40.680549621582
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1543230031_7880 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -442.31283569336,
              252.6277923584,
              80.898796081543
            },
            rotation = {
              0.0,
              0.0,
              -0.49397903680801,
              0.86947387456894
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1543230136_8065 = {
      components = {
        PresetInstanceComponent = {
          name = "Pickup_Explosive"
        },
        TransformComponent = {
          transform = {
            position = {
              -455.33734130859,
              297.89312744141,
              74.679428100586
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1543246643_3415 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -412.99520874023,
              -612.02941894531,
              123.73815155029
            },
            rotation = {
              3.764969669362e-09,
              -1.7810578567534e-09,
              0.021781273186207,
              0.99976277351379
            },
            scale = 1.0000020265579
          }
        }
      }
    },
    preset_instance1543246672_4729 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -279.70257568359,
              -626.52807617188,
              -260.71951293945
            },
            rotation = {
              -7.7034823798938e-19,
              -1.2171502594502e-18,
              0.87963128089905,
              -0.47565621137619
            },
            scale = 1.0000070333481
          }
        }
      }
    },
    preset_instance1543246854_6575 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -489.05313110352,
              -6.4466667175293,
              221.13917541504
            },
            rotation = {
              1.2584633335422e-10,
              -2.2787073616914e-10,
              0.82388556003571,
              0.56675624847412
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1543310661_6699 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -25.760437011719,
              -231.49508666992,
              177.09907531738
            },
            rotation = {
              0.0,
              0.0,
              0.2656597495079,
              0.96406686306
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    preset_instance1543311104_4581 = {
      components = {
        PresetInstanceComponent = {
          name = "Pickup_Explosive"
        },
        TransformComponent = {
          transform = {
            position = {
              -33.914447784424,
              -240.82737731934,
              108.55497741699
            },
            rotation = {
              9.7954744315842e-10,
              -6.2907601350304e-10,
              0.93715357780457,
              -0.3489171564579
            },
            scale = 1.0000011920929
          }
        }
      }
    },
    preset_instance1543311170_4527 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -19.51756477356,
              -228.07525634766,
              110.86666107178
            },
            rotation = {
              0.0,
              0.0,
              0.99820667505264,
              -0.059862084686756
            },
            scale = 1.0000004768372
          }
        }
      }
    },
    preset_instance1543311189_7903 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -30.683269500732,
              -238.14031982422,
              127.43138885498
            },
            rotation = {
              -3.4407438009865e-10,
              -3.1377114795283e-10,
              0.67382001876831,
              0.73889553546906
            },
            scale = 1.0000022649765
          }
        }
      }
    },
    preset_instance1543311211_3092 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -31.827373504639,
              -228.39050292969,
              111.45937347412
            },
            rotation = {
              0.0,
              0.0,
              0.050204124301672,
              0.99873900413513
            },
            scale = 1.0000002384186
          }
        }
      }
    },
    preset_instance1543311235_8812 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -122.04705810547,
              -188.92065429688,
              108.94669342041
            },
            rotation = {
              7.8450280993625e-11,
              -3.8325294327013e-11,
              -0.43894982337952,
              0.89851158857346
            },
            scale = 1.0000017881393
          }
        }
      }
    },
    preset_instance1543324314_7455 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -931.30059814453,
              -342.09027099609,
              100.28987121582
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1543324314_7987 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -1028.0938720703,
              -277.1064453125,
              -46.883666992188
            },
            rotation = {
              -1.2428028606237e-09,
              -1.3874033033545e-09,
              0.74485659599304,
              0.66722464561462
            },
            scale = 1.0000001192093
          }
        }
      }
    },
    preset_instance1543324349_1169 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -856.25512695313,
              -131.19360351563,
              320.30157470703
            },
            rotation = {
              -1.5241429646151e-11,
              2.4793604669338e-11,
              0.5236941576004,
              0.85190635919571
            },
            scale = 1.000005364418
          }
        }
      }
    },
    preset_instance1543324796_6705 = {
      components = {
        PresetInstanceComponent = {
          name = "Pickup_Explosive"
        },
        TransformComponent = {
          transform = {
            position = {
              -1019.0586547852,
              -277.83731079102,
              124.19761657715
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1543324796_7561 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -1024.5379638672,
              -280.23635864258,
              126.47064208984
            },
            rotation = {
              2.6261157692886e-09,
              3.2327485133266e-09,
              0.97620695829391,
              0.21684084832668
            },
            scale = 1.0000025033951
          }
        }
      }
    },
    preset_instance1543325057_792 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -926.02197265625,
              -334.70178222656,
              136.47026062012
            },
            rotation = {
              -2.7927193890775e-09,
              2.6354831650366e-09,
              -0.48945188522339,
              0.87203031778336
            },
            scale = 0.72540521621704
          }
        }
      }
    },
    preset_instance1543325090_1430 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -925.57720947266,
              -343.4001159668,
              136.43406677246
            },
            rotation = {
              6.4164717983317e-09,
              -3.8996463835872e-09,
              0.91237491369247,
              0.40935558080673
            },
            scale = 0.72540384531021
          }
        }
      }
    },
    preset_instance1543325112_3738 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -937.50750732422,
              -340.16290283203,
              136.17660522461
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 0.81715083122253
          }
        }
      }
    },
    preset_instance1543325138_8141 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -923.46490478516,
              -226.76936340332,
              197.85540771484
            },
            rotation = {
              -4.3532369375932e-10,
              -4.9413956260125e-10,
              0.063152872025967,
              0.99800390005112
            },
            scale = 1.0000002384186
          }
        }
      }
    },
    preset_instance1543327191_7969 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -211.0238494873,
              -421.58441162109,
              311.22003173828
            },
            rotation = {
              1.2205453314706e-09,
              3.9179107180765e-10,
              0.26208639144897,
              0.96504443883896
            },
            scale = 1.0000040531158
          }
        }
      }
    },
    preset_instance1543327524_2293 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -1035.2021484375,
              -279.10934448242,
              125.97116851807
            },
            rotation = {
              0.0,
              0.0,
              0.38576349616051,
              0.9225977063179
            },
            scale = 1.0000005960464
          }
        }
      }
    },
    preset_instance1543328834_6729 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -812.30358886719,
              -406.94961547852,
              290.59457397461
            },
            rotation = {
              0.0,
              0.0,
              -0.68562209606171,
              0.72795760631561
            },
            scale = 1.0000091791153
          }
        }
      }
    },
    preset_instance1543328876_313 = {
      components = {
        PresetInstanceComponent = {
          name = "Tree1"
        },
        TransformComponent = {
          transform = {
            position = {
              -654.88116455078,
              -42.942947387695,
              175.99028015137
            },
            rotation = {
              0.0,
              0.0,
              -0.099452674388885,
              0.99504232406616
            },
            scale = 1.0000009536743
          }
        }
      }
    },
    preset_instance1543706602_3710 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -466.89166259766,
              56.707263946533,
              149.20245361328
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1543706614_5612 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -464.51181030273,
              55.469856262207,
              149.19444274902
            },
            rotation = {
              0.0,
              0.0,
              0.9432657957077,
              0.33203867077827
            },
            scale = 1.0
          }
        }
      }
    },
    preset_instance1543706627_3249 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -464.98553466797,
              57.883499145508,
              149.11277770996
            },
            rotation = {
              8.8338630732978e-20,
              -1.1996686049975e-20,
              -0.74618059396744,
              0.66574358940125
            },
            scale = 1.0000020265579
          }
        }
      }
    },
    preset_instance1543707219_2244 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -474.91818237305,
              38.971851348877,
              139.11535644531
            },
            rotation = {
              -2.4000099130284e-10,
              -1.0132094052651e-09,
              0.76726627349854,
              0.64132863283157
            },
            scale = 1.0000016689301
          }
        }
      }
    },
    preset_instance1543707237_970 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -459.79440307617,
              37.649494171143,
              139.4930267334
            },
            rotation = {
              -3.7169080313593e-11,
              -4.4794928183434e-11,
              0.76957166194916,
              0.63856047391891
            },
            scale = 1.0000007152557
          }
        }
      }
    },
    preset_instance1543707653_1437 = {
      components = {
        PresetInstanceComponent = {
          name = "branch1"
        },
        TransformComponent = {
          transform = {
            position = {
              -461.64770507813,
              68.27271270752,
              152.92417907715
            },
            rotation = {
              0.098897039890289,
              0.015953270718455,
              -0.45883733034134,
              0.88285511732101
            },
            scale = 1.0000003576279
          }
        }
      }
    },
    preset_instance1544108942_2544 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -457.93487548828,
              46.346740722656,
              138.79893493652
            },
            rotation = {
              0.0,
              0.0,
              -0.19962887465954,
              0.979871571064
            },
            scale = 1.0000015497208
          }
        }
      }
    },
    preset_instance1544109045_9575 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -475.66131591797,
              46.501071929932,
              139.02746582031
            },
            rotation = {
              0.0,
              0.0,
              0.86962121725082,
              -0.49371951818466
            },
            scale = 1.0000003576279
          }
        }
      }
    },
    preset_instance1544628962_2492 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -565.34112548828,
              -532.56829833984,
              94.035469055176
            },
            rotation = {
              0.0,
              0.0,
              0.8696141242981,
              -0.49373203516006
            },
            scale = 1.0000005960464
          }
        }
      }
    },
    preset_instance1544629002_1936 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -542.85491943359,
              -538.07177734375,
              94.563453674316
            },
            rotation = {
              0.0,
              0.0,
              -0.65248346328735,
              0.75780296325684
            },
            scale = 1.0000197887421
          }
        }
      }
    },
    preset_instance1544629036_3543 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -533.28985595703,
              -538.62408447266,
              94.405418395996
            },
            rotation = {
              0.0,
              0.0,
              -0.62099933624268,
              0.78381109237671
            },
            scale = 1.000004529953
          }
        }
      }
    },
    preset_instance1544629093_4645 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -573.27301025391,
              -512.38946533203,
              93.936828613281
            },
            rotation = {
              0.0,
              0.0,
              0.9910677075386,
              -0.13335983455181
            },
            scale = 1.0000064373016
          }
        }
      }
    },
    preset_instance1544629227_9289 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -572.18804931641,
              -499.32464599609,
              93.826622009277
            },
            rotation = {
              0.0,
              0.0,
              0.98681253194809,
              -0.16186746954918
            },
            scale = 0.99999994039536
          }
        }
      }
    },
    preset_instance1544629271_6376 = {
      components = {
        PresetInstanceComponent = {
          name = "torch_preset"
        },
        TransformComponent = {
          transform = {
            position = {
              -574.03161621094,
              -481.47048950195,
              94.678344726563
            },
            rotation = {
              0.0,
              0.0,
              0.98628848791122,
              -0.16503030061722
            },
            scale = 1.0000184774399
          }
        }
      }
    },
    preset_instance1544629781_6818 = {
      components = {
        PresetInstanceComponent = {
          name = "crystal"
        },
        TransformComponent = {
          transform = {
            position = {
              -517.49212646484,
              -477.57318115234,
              103.94952392578
            },
            rotation = {
              0.257837921381,
              -0.30978325009346,
              0.00072450627340004,
              0.91517943143845
            },
            scale = 27.303087234497
          }
        }
      }
    },
    preset_instance1544630014_9231 = {
      components = {
        PresetInstanceComponent = {
          name = "crystal"
        },
        TransformComponent = {
          transform = {
            position = {
              -499.50900268555,
              -490.59002685547,
              119.08160400391
            },
            rotation = {
              0.34315207600594,
              0.43421646952629,
              -0.21010428667068,
              0.8059521317482
            },
            scale = 17.840236663818
          }
        }
      }
    },
    preset_instance1544630091_1458 = {
      components = {
        PresetInstanceComponent = {
          name = "crystal"
        },
        TransformComponent = {
          transform = {
            position = {
              -504.34939575195,
              -476.34289550781,
              106.81774139404
            },
            rotation = {
              -0.52544742822647,
              -0.10147153586149,
              0.21915858983994,
              0.81582969427109
            },
            scale = 35.206295013428
          }
        }
      }
    },
    preset_instance1544630149_7381 = {
      components = {
        PresetInstanceComponent = {
          name = "crystal"
        },
        TransformComponent = {
          transform = {
            position = {
              -573.72021484375,
              -527.66754150391,
              88.228073120117
            },
            rotation = {
              -0.21542143821716,
              0.093678794801235,
              -0.088753759860992,
              0.96795696020126
            },
            scale = 6.6998844146729
          }
        }
      }
    },
    preset_instance1544630182_5178 = {
      components = {
        PresetInstanceComponent = {
          name = "crystal"
        },
        TransformComponent = {
          transform = {
            position = {
              -509.16342163086,
              -547.15222167969,
              91.18627166748
            },
            rotation = {
              -0.1639795601368,
              0.092743955552578,
              0.035788867622614,
              0.98144197463989
            },
            scale = 8.4446096420288
          }
        }
      }
    },
    preset_instance1544630187_5445 = {
      components = {
        PresetInstanceComponent = {
          name = "crystal"
        },
        TransformComponent = {
          transform = {
            position = {
              -574.42785644531,
              -525.36254882813,
              90.174652099609
            },
            rotation = {
              -0.22931592166424,
              -0.046183504164219,
              -0.088975965976715,
              0.96817588806152
            },
            scale = 3.3084754943848
          }
        }
      }
    },
    preset_instance1544701075_6788 = {
      components = {
        PresetInstanceComponent = {
          name = "crystal"
        },
        TransformComponent = {
          transform = {
            position = {
              -486.4013671875,
              -478.16680908203,
              105.00085449219
            },
            rotation = {
              1.00946849102e-07,
              0.44813469052315,
              -1.722201972143e-07,
              0.89396601915359
            },
            scale = 11.615348815918
          }
        }
      }
    },
    preset_instance1544701161_1037 = {
      components = {
        PresetInstanceComponent = {
          name = "crystal"
        },
        TransformComponent = {
          transform = {
            position = {
              -493.19155883789,
              -460.21990966797,
              119.13263702393
            },
            rotation = {
              -0.17840823531151,
              0.55380868911743,
              0.1228925511241,
              0.8039675951004
            },
            scale = 16.701808929443
          }
        }
      }
    },
    preset_instance1544788205_8082 = {
      components = {
        PresetInstanceComponent = {
          name = "crystal"
        },
        TransformComponent = {
          transform = {
            position = {
              -560.56768798828,
              -543.82830810547,
              105.04984283447
            },
            rotation = {
              -0.31239399313927,
              -0.18768745660782,
              -0.13081657886505,
              0.92199265956879
            },
            scale = 12.828159332275
          }
        }
      }
    },
    stump = {
      components = {
        MaterialComponent = {
          material = 2
        },
        ModelComponent = {
          asset = "assets/model/stump_arena.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/stump_arena.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -509.66888427734,
              -471.61651611328,
              -259.11331176758
            },
            rotation = {
              -1.8366215215337e-09,
              -1.3513740126925e-09,
              0.95486015081406,
              -0.29705572128296
            },
            scale = 3.3766059875488
          }
        }
      }
    }
  },
  presets = {
    Pickup_Explosive = {
      entity1542963051_7944 = {
        components = {
          ModelComponent = {
            asset = "assets/model/bomb_arrow_pickup.fbx"
          },
          ParticlesComponent = {
            max_end_color = {
              w = 1.0,
              x = 0.2,
              y = 0.0,
              z = 0.0
            },
            max_start_color = {
              w = 1.0,
              x = 0.3,
              y = 0.02,
              z = 0.0
            },
            min_end_color = {
              w = 1.0,
              x = 0.0,
              y = 0.0,
              z = 0.0
            },
            min_start_color = {
              w = 1.0,
              x = 0.0,
              y = 0.0,
              z = 0.0
            },
            spawn_per_second = 0.0
          },
          PickupComponent = {
            player_range = 250,
            position = {
              x = -448.28573608398,
              y = 47.945159912109,
              z = 503.16680908203
            },
            respawn_cooldown = 30.0,
            time_to_respawn = 0,
            type = 1.0
          },
          TransformComponent = {
            transform = {
              position = {
                -448.28573608398,
                47.945159912109,
                503.16680908203
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            0.0,
            0.0,
            0.0
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      }
    },
    Pickup_Heart = {
      entity1542961281_8341 = {
        components = {
          ModelComponent = {
            asset = "assets/model/health_upgrade.fbx"
          },
          ParticlesComponent = {
            max_end_color = {
              x = 0.20000000298023,
              y = 0.0
            },
            max_start_color = {
              x = 0.30000001192093,
              y = 0.019999999552965
            },
            spawn_per_second = 0.0
          },
          PickupComponent = {
            position = {
              x = -448.03109741211,
              y = 52.769943237305,
              z = 502.13317871094
            },
            time_to_respawn = 0.10000000149012
          },
          TransformComponent = {
            transform = {
              position = {
                -446.20825195313,
                53.56050491333,
                503.5075378418
              },
              rotation = {
                -1.5694838761959e-09,
                -2.1155597274003e-09,
                0.14658640325069,
                0.989197909832
              },
              scale = 1.0000014305115
            }
          }
        },
        relative_transform = {
          position = {
            0.0,
            0.0,
            0.0
          },
          rotation = {
            -1.5694840982405e-09,
            -2.1155599494449e-09,
            0.14658641815186,
            0.98919785022736
          },
          scale = 1.0000014305115
        }
      }
    },
    Tree1 = {
      entity1542630891_4409 = {
        components = {
          MaterialComponent = {
            material = 2
          },
          ModelComponent = {
            asset = "assets/model/ltree1.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/ltree1.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -11.636524200439,
                -28.493919372559,
                0.0
              },
              rotation = {
                0.70660197734833,
                0.0070272237062454,
                0.0062127192504704,
                0.70754903554916
              },
              scale = 3.0
            }
          }
        },
        relative_transform = {
          position = {
            -0.56267070770264,
            -1.2310581207275,
            -526.65014648438
          },
          rotation = {
            0.70660197734833,
            0.0070272232405841,
            0.0062127192504704,
            0.70754909515381
          },
          scale = 3.0
        }
      },
      preset_instance1542638583_2904 = {
        components = {
          PresetInstanceComponent = {
            name = "branch1"
          },
          TransformComponent = {
            transform = {
              position = {
                -11.392868995667,
                -24.313329696655,
                130.30400085449
              },
              rotation = {
                0.16741624474525,
                -0.831647336483,
                -0.15262925624847,
                0.50699001550674
              },
              scale = 1.0000042915344
            }
          }
        },
        relative_transform = {
          position = {
            -0.31901550292969,
            2.9495315551758,
            -396.34613037109
          },
          rotation = {
            0.16741625964642,
            -0.831647336483,
            -0.15262924134731,
            0.5069899559021
          },
          scale = 1.0000044107437
        }
      },
      preset_instance1542638631_3107 = {
        components = {
          PresetInstanceComponent = {
            name = "branch1"
          },
          TransformComponent = {
            transform = {
              position = {
                -13.122458457947,
                -28.033983230591,
                176.0923614502
              },
              rotation = {
                0.5388560295105,
                -0.39414262771606,
                0.35006326436996,
                0.65707039833069
              },
              scale = 1.0000046491623
            }
          }
        },
        relative_transform = {
          position = {
            -2.04860496521,
            -0.77112197875977,
            -350.55780029297
          },
          rotation = {
            0.5388560295105,
            -0.39414265751839,
            0.35006326436996,
            0.65707039833069
          },
          scale = 1.000004529953
        }
      },
      preset_instance1542700577_157 = {
        components = {
          PresetInstanceComponent = {
            name = "branch2"
          },
          TransformComponent = {
            transform = {
              position = {
                -12.857340812683,
                -30.981552124023,
                119.64507293701
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            -1.7834873199463,
            -3.7186908721924,
            -407.00506591797
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      preset_instance1542700636_3210 = {
        components = {
          PresetInstanceComponent = {
            name = "branch1"
          },
          TransformComponent = {
            transform = {
              position = {
                -11.079109191895,
                -28.926931381226,
                322.69674682617
              },
              rotation = {
                0.16864438354969,
                -0.021872928366065,
                -0.075583331286907,
                0.98253130912781
              },
              scale = 1.3044391870499
            }
          }
        },
        relative_transform = {
          position = {
            -0.0052556991577148,
            -1.6640701293945,
            -203.9533996582
          },
          rotation = {
            0.16864436864853,
            -0.021872924640775,
            -0.075583331286907,
            0.98253130912781
          },
          scale = 1.3044391870499
        }
      },
      preset_instance1542700701_1888 = {
        components = {
          PresetInstanceComponent = {
            name = "branch2"
          },
          TransformComponent = {
            transform = {
              position = {
                -11.297657966614,
                -27.26372718811,
                398.27871704102
              },
              rotation = {
                -0.09226094186306,
                -0.0090997852385044,
                0.59950894117355,
                0.7949805855751
              },
              scale = 0.96386736631393
            }
          }
        },
        relative_transform = {
          position = {
            -0.22380447387695,
            -0.00086593627929688,
            -128.37142944336
          },
          rotation = {
            -0.092260949313641,
            -0.0090997898951173,
            0.59950894117355,
            0.7949805855751
          },
          scale = 0.96386736631393
        }
      },
      preset_instance1542702805_2753 = {
        components = {
          PresetInstanceComponent = {
            name = "branch3"
          },
          TransformComponent = {
            transform = {
              position = {
                -11.813658714294,
                -29.24764251709,
                255.0207824707
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            -0.73980522155762,
            -1.9847812652588,
            -271.62936401367
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      preset_instance1542705677_8280 = {
        components = {
          PresetInstanceComponent = {
            name = "branch5"
          },
          TransformComponent = {
            transform = {
              position = {
                -13.781276702881,
                -26.81116104126,
                373.7551574707
              },
              rotation = {
                -0.19943459331989,
                0.36385759711266,
                0.61571437120438,
                0.66987258195877
              },
              scale = 1.943284869194
            }
          }
        },
        relative_transform = {
          position = {
            -2.707423210144,
            0.45170021057129,
            -152.89498901367
          },
          rotation = {
            -0.19943459331989,
            0.36385759711266,
            0.61571437120438,
            0.66987258195877
          },
          scale = 1.943284869194
        }
      },
      preset_instance1542723463_2365 = {
        components = {
          PresetInstanceComponent = {
            name = "branch5"
          },
          TransformComponent = {
            transform = {
              position = {
                -8.732120513916,
                -26.410970687866,
                483.01220703125
              },
              rotation = {
                0.13639789819717,
                -0.099615179002285,
                -0.22247916460037,
                0.96019554138184
              },
              scale = 1.5967619419098
            }
          }
        },
        relative_transform = {
          position = {
            2.3417329788208,
            0.85189056396484,
            -43.637939453125
          },
          rotation = {
            0.13639791309834,
            -0.099615179002285,
            -0.22247916460037,
            0.96019554138184
          },
          scale = 1.5967619419098
        }
      },
      preset_instance1542723873_8539 = {
        components = {
          PresetInstanceComponent = {
            name = "branch3"
          },
          TransformComponent = {
            transform = {
              position = {
                -11.289582252502,
                -29.21245765686,
                445.93887329102
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.2527257204056
            }
          }
        },
        relative_transform = {
          position = {
            -0.21572875976563,
            -1.9495964050293,
            -80.711273193359
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.2527257204056
        }
      },
      preset_instance1542889232_1820 = {
        components = {
          PresetInstanceComponent = {
            name = "branch1"
          },
          TransformComponent = {
            transform = {
              position = {
                -10.596383094788,
                -25.6340675354,
                725.94793701172
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            0.47747039794922,
            1.6287937164307,
            199.29779052734
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      preset_instance1542889232_360 = {
        components = {
          PresetInstanceComponent = {
            name = "branch3"
          },
          TransformComponent = {
            transform = {
              position = {
                -10.604238510132,
                -25.6315574646,
                831.40692138672
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            0.46961498260498,
            1.6313037872314,
            304.75677490234
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      preset_instance1542889232_4035 = {
        components = {
          PresetInstanceComponent = {
            name = "branch3"
          },
          TransformComponent = {
            transform = {
              position = {
                -8.1791925430298,
                -25.480140686035,
                951.61590576172
              },
              rotation = {
                -0.64652979373932,
                -0.34606000781059,
                0.67701357603073,
                -0.062404621392488
              },
              scale = 1.0000020265579
            }
          }
        },
        relative_transform = {
          position = {
            2.894660949707,
            1.7827205657959,
            424.96575927734
          },
          rotation = {
            -0.64652979373932,
            -0.34606000781059,
            0.67701357603073,
            -0.062404621392488
          },
          scale = 1.0000020265579
        }
      },
      preset_instance1542889232_5833 = {
        components = {
          PresetInstanceComponent = {
            name = "branch5"
          },
          TransformComponent = {
            transform = {
              position = {
                -10.241470336914,
                -28.216083526611,
                686.80773925781
              },
              rotation = {
                0.0,
                0.0,
                -0.65110605955124,
                0.75898677110672
              },
              scale = 1.0000013113022
            }
          }
        },
        relative_transform = {
          position = {
            0.83238315582275,
            -0.95322227478027,
            160.15759277344
          },
          rotation = {
            0.0,
            0.0,
            -0.65110605955124,
            0.75898677110672
          },
          scale = 1.0000013113022
        }
      },
      preset_instance1542889232_7092 = {
        components = {
          PresetInstanceComponent = {
            name = "branch2"
          },
          TransformComponent = {
            transform = {
              position = {
                -10.633716583252,
                -25.640842437744,
                793.62890625
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            0.44013690948486,
            1.6220188140869,
            266.97875976563
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      preset_instance1542889232_915 = {
        components = {
          PresetInstanceComponent = {
            name = "branch5"
          },
          TransformComponent = {
            transform = {
              position = {
                -10.633716583252,
                -25.64093208313,
                793.62872314453
              },
              rotation = {
                0.091046549379826,
                0.012676901184022,
                0.066345654428005,
                0.99355328083038
              },
              scale = 1.0000003576279
            }
          }
        },
        relative_transform = {
          position = {
            0.44013690948486,
            1.6219291687012,
            266.97857666016
          },
          rotation = {
            0.091046549379826,
            0.0126769002527,
            0.066345654428005,
            0.99355328083038
          },
          scale = 1.0000003576279
        }
      },
      preset_instance1542889232_9274 = {
        components = {
          PresetInstanceComponent = {
            name = "branch1"
          },
          TransformComponent = {
            transform = {
              position = {
                -10.65002822876,
                -27.320911407471,
                918.80810546875
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            0.42382526397705,
            -0.058050155639648,
            392.15795898438
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      preset_instance1542889379_498 = {
        components = {
          PresetInstanceComponent = {
            name = "branch1"
          },
          TransformComponent = {
            transform = {
              position = {
                -10.788007736206,
                -27.471256256104,
                1073.1137695313
              },
              rotation = {
                -0.047898039221764,
                0.066193163394928,
                0.91934394836426,
                -0.38487789034843
              },
              scale = 1.0000001192093
            }
          }
        },
        relative_transform = {
          position = {
            0.28584575653076,
            -0.20839500427246,
            546.46362304688
          },
          rotation = {
            -0.047898039221764,
            0.066193163394928,
            0.91934394836426,
            -0.38487789034843
          },
          scale = 1.0000001192093
        }
      }
    },
    Tree2 = {
      entity1542809309_5222 = {
        components = {
          MaterialComponent = {
            material = 2
          },
          ModelComponent = {
            asset = "assets/model/ltree1.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/ltree1.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -95.493804931641,
                -316.89962768555,
                -16.999893188477
              },
              rotation = {
                0.70660197734833,
                0.0070272251032293,
                0.0062127192504704,
                0.70754903554916
              },
              scale = 3.0
            }
          }
        },
        relative_transform = {
          position = {
            -0.29595947265625,
            -1.1307373046875,
            -568.51983642578
          },
          rotation = {
            0.70660197734833,
            0.0070272237062454,
            0.0062127187848091,
            0.70754909515381
          },
          scale = 3.0
        }
      },
      preset_instance1542809316_1673 = {
        components = {
          PresetInstanceComponent = {
            name = "branch5"
          },
          TransformComponent = {
            transform = {
              position = {
                -93.359466552734,
                -314.17660522461,
                400.98626708984
              },
              rotation = {
                0.099358625710011,
                0.038840666413307,
                -0.011870235204697,
                0.99422246217728
              },
              scale = 1.3978143930435
            }
          }
        },
        relative_transform = {
          position = {
            1.83837890625,
            1.59228515625,
            -150.53369140625
          },
          rotation = {
            0.099358625710011,
            0.038840666413307,
            -0.011870235204697,
            0.99422246217728
          },
          scale = 1.3978143930435
        }
      },
      preset_instance1542809360_3957 = {
        components = {
          PresetInstanceComponent = {
            name = "branch5"
          },
          TransformComponent = {
            transform = {
              position = {
                -96.382354736328,
                -316.12921142578,
                464.92211914063
              },
              rotation = {
                0.63512402772903,
                0.34160041809082,
                0.69229888916016,
                -0.025473793968558
              },
              scale = 1.0710437297821
            }
          }
        },
        relative_transform = {
          position = {
            -1.1845092773438,
            -0.36032104492188,
            -86.597839355469
          },
          rotation = {
            0.63512402772903,
            0.34160041809082,
            0.69229888916016,
            -0.025473793968558
          },
          scale = 1.0710437297821
        }
      },
      preset_instance1542809413_6695 = {
        components = {
          PresetInstanceComponent = {
            name = "branch5"
          },
          TransformComponent = {
            transform = {
              position = {
                -96.273681640625,
                -314.32192993164,
                481.75402832031
              },
              rotation = {
                0.054642200469971,
                0.16053369641304,
                0.89562612771988,
                0.41121405363083
              },
              scale = 1.2163020372391
            }
          }
        },
        relative_transform = {
          position = {
            -1.0758361816406,
            1.4469604492188,
            -69.765930175781
          },
          rotation = {
            0.054642200469971,
            0.16053369641304,
            0.89562612771988,
            0.41121405363083
          },
          scale = 1.2163020372391
        }
      },
      preset_instance1542809446_6359 = {
        components = {
          PresetInstanceComponent = {
            name = "branch1"
          },
          TransformComponent = {
            transform = {
              position = {
                -94.573120117188,
                -315.04006958008,
                482.37957763672
              },
              rotation = {
                0.20207247138023,
                0.021439872682095,
                0.0069159246049821,
                0.97911143302917
              },
              scale = 1.0000004768372
            }
          }
        },
        relative_transform = {
          position = {
            0.62472534179688,
            0.72882080078125,
            -69.140380859375
          },
          rotation = {
            0.20207247138023,
            0.021439872682095,
            0.0069159246049821,
            0.97911143302917
          },
          scale = 1.0000004768372
        }
      },
      preset_instance1542809467_9206 = {
        components = {
          PresetInstanceComponent = {
            name = "branch1"
          },
          TransformComponent = {
            transform = {
              position = {
                -95.810729980469,
                -315.5908203125,
                316.48999023438
              },
              rotation = {
                0.15205968916416,
                -0.03142986446619,
                -0.012541870586574,
                0.98779183626175
              },
              scale = 1.0000007152557
            }
          }
        },
        relative_transform = {
          position = {
            -0.61288452148438,
            0.17807006835938,
            -235.02996826172
          },
          rotation = {
            0.15205971896648,
            -0.03142986446619,
            -0.012541869655252,
            0.98779183626175
          },
          scale = 1.0000007152557
        }
      },
      preset_instance1542809505_8978 = {
        components = {
          PresetInstanceComponent = {
            name = "branch1"
          },
          TransformComponent = {
            transform = {
              position = {
                -96.945251464844,
                -317.76538085938,
                274.61654663086
              },
              rotation = {
                0.13499842584133,
                -0.097440354526043,
                -0.52560180425644,
                0.83428025245667
              },
              scale = 1.0000027418137
            }
          }
        },
        relative_transform = {
          position = {
            -1.7474060058594,
            -1.9964904785156,
            -276.90341186523
          },
          rotation = {
            0.13499842584133,
            -0.097440354526043,
            -0.52560180425644,
            0.83428025245667
          },
          scale = 1.0000027418137
        }
      },
      preset_instance1542809539_7273 = {
        components = {
          PresetInstanceComponent = {
            name = "branch3"
          },
          TransformComponent = {
            transform = {
              position = {
                -93.810028076172,
                -317.35778808594,
                291.93991088867
              },
              rotation = {
                -0.025708325207233,
                -0.0016558935167268,
                0.43060749769211,
                0.90217155218124
              },
              scale = 1.003112912178
            }
          }
        },
        relative_transform = {
          position = {
            1.3878173828125,
            -1.5888977050781,
            -259.58004760742
          },
          rotation = {
            -0.025708327069879,
            -0.0016558935167268,
            0.43060749769211,
            0.90217155218124
          },
          scale = 1.0031127929688
        }
      },
      preset_instance1542809583_8052 = {
        components = {
          PresetInstanceComponent = {
            name = "branch1"
          },
          TransformComponent = {
            transform = {
              position = {
                -95.586486816406,
                -314.85971069336,
                133.40953063965
              },
              rotation = {
                0.10639534890652,
                0.014693864621222,
                -0.044973731040955,
                0.99319761991501
              },
              scale = 1.0000014305115
            }
          }
        },
        relative_transform = {
          position = {
            -0.38864135742188,
            0.9091796875,
            -418.11041259766
          },
          rotation = {
            0.10639534890652,
            0.014693863689899,
            -0.044973731040955,
            0.99319761991501
          },
          scale = 1.0000014305115
        }
      },
      preset_instance1542888345_2924 = {
        components = {
          PresetInstanceComponent = {
            name = "branch3"
          },
          TransformComponent = {
            transform = {
              position = {
                -94.50252532959,
                -315.41616821289,
                758.86151123047
              },
              rotation = {
                -3.7678787173645e-11,
                7.8762989186298e-11,
                0.43154439330101,
                0.90209174156189
              },
              scale = 0.99999988079071
            }
          }
        },
        relative_transform = {
          position = {
            0.69532012939453,
            0.35272216796875,
            207.34155273438
          },
          rotation = {
            -3.7678783704198e-11,
            7.8762989186298e-11,
            0.43154439330101,
            0.90209174156189
          },
          scale = 0.99999988079071
        }
      },
      preset_instance1542888345_7774 = {
        components = {
          PresetInstanceComponent = {
            name = "branch5"
          },
          TransformComponent = {
            transform = {
              position = {
                -94.513450622559,
                -315.39541625977,
                690.01959228516
              },
              rotation = {
                0.11717074364424,
                -0.013647044077516,
                0.014335000887513,
                0.99291455745697
              },
              scale = 1.0000019073486
            }
          }
        },
        relative_transform = {
          position = {
            0.68439483642578,
            0.37347412109375,
            138.49963378906
          },
          rotation = {
            0.11717075109482,
            -0.013647044077516,
            0.014335000887513,
            0.99291455745697
          },
          scale = 1.0000019073486
        }
      },
      preset_instance1542888345_7833 = {
        components = {
          PresetInstanceComponent = {
            name = "branch5"
          },
          TransformComponent = {
            transform = {
              position = {
                -94.687561035156,
                -314.74078369141,
                655.92102050781
              },
              rotation = {
                0.0,
                0.0,
                0.44606050848961,
                0.89500284194946
              },
              scale = 1.0000004768372
            }
          }
        },
        relative_transform = {
          position = {
            0.51028442382813,
            1.0281066894531,
            104.40106201172
          },
          rotation = {
            0.0,
            0.0,
            0.44606050848961,
            0.89500284194946
          },
          scale = 1.0000004768372
        }
      },
      preset_instance1542888419_2101 = {
        components = {
          PresetInstanceComponent = {
            name = "branch1"
          },
          TransformComponent = {
            transform = {
              position = {
                -95.754760742188,
                -316.21496582031,
                900.26965332031
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            -0.55691528320313,
            -0.44607543945313,
            348.74969482422
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      preset_instance1542888419_2788 = {
        components = {
          PresetInstanceComponent = {
            name = "branch5"
          },
          TransformComponent = {
            transform = {
              position = {
                -93.930969238281,
                -316.0466003418,
                1035.2585449219
              },
              rotation = {
                0.11951238662004,
                -0.070521548390388,
                -0.51333105564117,
                0.84689712524414
              },
              scale = 1.0000003576279
            }
          }
        },
        relative_transform = {
          position = {
            1.2668762207031,
            -0.2777099609375,
            483.73858642578
          },
          rotation = {
            0.11951239407063,
            -0.070521548390388,
            -0.51333111524582,
            0.84689712524414
          },
          scale = 1.0000003576279
        }
      },
      preset_instance1542888419_3896 = {
        components = {
          PresetInstanceComponent = {
            name = "branch3"
          },
          TransformComponent = {
            transform = {
              position = {
                -95.791450500488,
                -316.21813964844,
                948.98181152344
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            -0.59360504150391,
            -0.44924926757813,
            397.46185302734
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      preset_instance1542888419_5898 = {
        components = {
          PresetInstanceComponent = {
            name = "branch1"
          },
          TransformComponent = {
            transform = {
              position = {
                -95.749794006348,
                -316.12872314453,
                1005.5101928711
              },
              rotation = {
                0.0,
                0.0,
                0.79126292467117,
                0.61147618293762
              },
              scale = 1.0000010728836
            }
          }
        },
        relative_transform = {
          position = {
            -0.55194854736328,
            -0.35983276367188,
            453.990234375
          },
          rotation = {
            0.0,
            0.0,
            0.79126298427582,
            0.61147606372833
          },
          scale = 1.0000011920929
        }
      }
    },
    branch1 = {
      entity1542638324_7561 = {
        components = {
          DestructibleComponent = {
            death_callback = {
              name = "generic_branch_death_callback",
              path = "assets/script/gmp/death_callbacks.lua"
            },
            health_points = 1,
            max_health_points = 1
          },
          MaterialComponent = {
            material = 2
          },
          ModelComponent = {
            asset = "assets/model/branch1.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/branch1.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -10.568613052368,
                -25.330171585083,
                131.19218444824
              },
              rotation = {
                0.54150640964508,
                -0.45398938655853,
                -0.45522418618202,
                0.54169678688049
              },
              scale = 5.9962530136108
            }
          }
        },
        relative_transform = {
          position = {
            0.0,
            0.0,
            0.0
          },
          rotation = {
            0.54150640964508,
            -0.45398938655853,
            -0.45522418618202,
            0.54169678688049
          },
          scale = 5.9962530136108
        }
      }
    },
    branch2 = {
      entity1542639219_3795 = {
        components = {
          DestructibleComponent = {
            death_callback = {
              name = "generic_branch_death_callback",
              path = "assets/script/gmp/death_callbacks.lua"
            },
            health_points = 1,
            max_health_points = 1
          },
          MaterialComponent = {
            material = 2
          },
          ModelComponent = {
            asset = "assets/model/branch2.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/branch2.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -13.087126731873,
                -29.591833114624,
                104.50563812256
              },
              rotation = {
                0.020391419529915,
                0.15186339616776,
                0.34227550029755,
                0.92702168226242
              },
              scale = 5.6400361061096
            }
          }
        },
        relative_transform = {
          position = {
            0.0,
            0.0,
            0.0
          },
          rotation = {
            0.020391419529915,
            0.15186339616776,
            0.34227550029755,
            0.92702168226242
          },
          scale = 5.6400361061096
        }
      }
    },
    branch3 = {
      entity1542702482_8458 = {
        components = {
          DestructibleComponent = {
            death_callback = {
              name = "generic_branch_death_callback",
              path = "assets/script/gmp/death_callbacks.lua"
            },
            health_points = 1,
            max_health_points = 1
          },
          MaterialComponent = {
            material = 2
          },
          ModelComponent = {
            asset = "assets/model/branch3.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/branch3.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -11.770669937134,
                -28.262519836426,
                249.21772766113
              },
              rotation = {
                0.4286003112793,
                0.52815306186676,
                0.35326960682869,
                0.64230579137802
              },
              scale = 7.2702283859253
            }
          }
        },
        relative_transform = {
          position = {
            0.0,
            0.0,
            0.0
          },
          rotation = {
            0.4286003112793,
            0.52815306186676,
            0.35326960682869,
            0.64230579137802
          },
          scale = 7.2702283859253
        }
      }
    },
    branch5 = {
      entity1542705535_8829 = {
        components = {
          DestructibleComponent = {
            death_callback = {
              name = "generic_branch_death_callback",
              path = "assets/script/gmp/death_callbacks.lua"
            },
            health_points = 1,
            max_health_points = 1
          },
          MaterialComponent = {
            material = 2
          },
          ModelComponent = {
            asset = "assets/model/branch5.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/branch5.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -64.423698425293,
                3.5395348072052,
                292.43762207031
              },
              rotation = {
                -0.39743614196777,
                -0.012220916338265,
                0.91681200265884,
                -0.036754716187716
              },
              scale = 2.394593000412
            }
          }
        },
        relative_transform = {
          position = {
            0.0,
            0.0,
            0.0
          },
          rotation = {
            -0.39743614196777,
            -0.01222091820091,
            0.91681200265884,
            -0.036754716187716
          },
          scale = 2.394593000412
        }
      }
    },
    crystal = {
      entity1544629345_2596 = {
        components = {
          ModelComponent = {
            asset = "assets/model/crystal.fbx"
          },
          PointLightComponent = {
            color = {
              z = 1.0
            },
            radius = 100.0
          },
          StaticComponent = {
            contents = {
              path = "assets/model/crystal.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -559.54351806641,
                -484.59829711914,
                91.41138458252
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            0.0,
            0.0,
            0.0
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      }
    },
    player = {
      player = {
        components = {
          AimOffsetComponent = {
            m_joint_name = "spine_01"
          },
          AnimationComponent = {
            attack_state_machine = {
              name = "player_attack",
              start_state = "none"
            },
            state_machine = {
              name = "player",
              start_state = "idle"
            }
          },
          CharacterControllerComponent = {
            contents = {
              gravity = {
                x = 0.0,
                y = 0.0,
                z = -9.82
              },
              height = 0.25000001192093,
              mass = 80.0,
              offset_transform = {
                position = {
                  [3] = -0.69999998807907
                },
                rotation = {
                  0,
                  0,
                  1,
                  0
                },
                scale = 1.0
              },
              radius = 0.60000001192093,
              turning_speed = 10.0,
              use_navmesh_collision = false
            }
          },
          FactionComponent = {
            type = 0
          },
          HealthComponent = {
            death_callback = {
              name = "player_death_callback",
              path = "assets/script/gmp/death_callbacks.lua"
            },
            health_points = 10,
            max_health_points = 10
          },
          AudioComponent = {},
          MaterialComponent = {
            material = 6
          },
          ModelComponent = {
            asset = "assets/model/player_mesh.fbx"
          },
          NameComponent = {
            name = "player"
          },
          PlayerComponent = {},
          PlayerDamageVisualComponent = {
            color = {
              x = 1.0
            }
          },
          RadialComponent = {
            blur_radius = 0.0
          },
          SlowMotionComponent = {
            factor = 0.85000002384186
          },
          TransformComponent = {
            transform = {
              position = {
                -0.011647939682007,
                1.9561524391174,
                8.6799917221069
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            0.0,
            0.0,
            0.0
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      }
    },
    preset1542352401_7694 = {
      entity1542352208_9049 = {
        components = {
          AttachToEntityComponent = {
            entity_handle = "platform",
            offset_transform = {
              position = {
                1.3356764316559,
                0.094623267650604,
                2.7289505004883
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 0.58206957578659
            }
          },
          ModelComponent = {
            asset = "assets/model/hill01.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/hill01.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                1.4915218353271,
                4.5999827384949,
                79.764846801758
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 2.0358080863953
            }
          }
        },
        relative_transform = {
          position = {
            2.0702769756317,
            1.6400372982025,
            0.87398529052734
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 2.0358080863953
        }
      },
      entity1542352230_8904 = {
        components = {
          ModelComponent = {
            asset = "assets/model/hill01.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/hill01.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -0.047735527157784,
                0.010818481445313,
                86.6875
              },
              rotation = {
                0.0,
                -0.38850998878479,
                0.0,
                0.92144453525543
              },
              scale = 1.1553937196732
            }
          }
        },
        relative_transform = {
          position = {
            0.53101968765259,
            -2.949126958847,
            7.7966384887695
          },
          rotation = {
            0.0,
            -0.38850998878479,
            0.0,
            0.92144453525543
          },
          scale = 1.1553937196732
        }
      },
      platform = {
        components = {
          ModelComponent = {
            asset = "assets/model/hill01.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/hill01.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -3.1800518035889,
                4.2690348625183,
                70.220245361328
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 3.4975340366364
            }
          }
        },
        relative_transform = {
          position = {
            -2.6012966632843,
            1.309089422226,
            -8.6706161499023
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 3.4975340366364
        }
      }
    },
    torch_preset = {
      torch_body = {
        components = {
          ModelComponent = {
            asset = "assets/model/torch.fbx"
          },
          TransformComponent = {
            transform = {
              position = {
                -455.04730224609,
                48.445201873779,
                523.60131835938
              },
              rotation = {
                -0.42370340228081,
                0.55534034967422,
                0.56512171030045,
                -0.43898746371269
              },
              scale = 0.76319235563278
            }
          }
        },
        relative_transform = {
          position = {
            0.36871337890625,
            -0.12391662597656,
            -0.76434326171875
          },
          rotation = {
            -0.42370340228081,
            0.55534034967422,
            0.56512171030045,
            -0.43898746371269
          },
          scale = 0.76319235563278
        }
      },
      torch_tip = {
        components = {
          ParticlesComponent = {
            max_end_color = {
              w = 1.0,
              x = 0.20000000298023,
              y = 0.0,
              z = 0.0
            },
            max_end_size = 0.2,
            max_particles = 8.0,
            max_rotation_speed = 9.1999998092651,
            max_spawn_pos = {
              x = 0.10000000149012,
              y = 0.10000000149012,
              z = -0.2
            },
            max_start_color = {
              w = 1.0,
              x = 1.0,
              y = 0.3,
              z = 0.0
            },
            max_start_size = 0.5,
            max_start_velocity = {
              z = 0.8
            },
            min_end_color = {
              w = 1.0,
              x = 0.0,
              y = 0.0,
              z = 0.0
            },
            min_end_size = 0.1,
            min_rotation_speed = 7.0,
            min_spawn_pos = {
              x = -0.10000000149012,
              y = -0.10000000149012,
              z = -0.3
            },
            min_start_color = {
              w = 1.0,
              x = 0.8,
              y = 0.0,
              z = 0.0
            },
            min_start_size = 0.4,
            min_start_velocity = {
              z = 0.40000000596046
            },
            size_mode = 1.0,
            spawn_per_second = 5.0,
            velocity_mode = 0.0
          },
          PointLightComponent = {
            color = {
              x = 1.0,
              y = 0.10000001639128
            },
            radius = 130
          },
          TransformComponent = {
            transform = {
              position = {
                -455.78475952148,
                48.693035125732,
                525.13000488281
              },
              rotation = {
                0.0093260360881686,
                -0.0045132432132959,
                0.0057966369204223,
                0.99992954730988
              },
              scale = 0.18929043412209
            }
          }
        },
        relative_transform = {
          position = {
            -0.36874389648438,
            0.12391662597656,
            0.76434326171875
          },
          rotation = {
            0.0093260360881686,
            -0.0045132432132959,
            0.0057966369204223,
            0.99992954730988
          },
          scale = 0.18929043412209
        }
      }
    },
    tree1 = {
      branch1 = {
        components = {
          MaterialComponent = {
            material = 2
          },
          ModelComponent = {
            asset = "assets/model/branch1.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/branch1.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                9.6640958786011,
                49.375057220459,
                134.74984741211
              },
              rotation = {
                2.1918904025142e-08,
                1.9010945706555e-08,
                0.65521669387817,
                0.75544100999832
              },
              scale = 8.2171440124512
            }
          }
        },
        relative_transform = {
          position = {
            0.59966659545898,
            -5.2656135559082,
            71.804702758789
          },
          rotation = {
            2.1918904025142e-08,
            1.9010945706555e-08,
            0.65521669387817,
            0.75544100999832
          },
          scale = 8.2171440124512
        }
      },
      entity1542359855_514 = {
        components = {
          MaterialComponent = {
            material = 2
          },
          ModelComponent = {
            asset = "assets/model/branch1.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/branch1.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                5.8241033554077,
                53.843925476074,
                58.593662261963
              },
              rotation = {
                0.0,
                0.0,
                0.59897601604462,
                0.80076694488525
              },
              scale = 8.217137336731
            }
          }
        },
        relative_transform = {
          position = {
            -3.2403259277344,
            -0.79674530029297,
            -4.3514785766602
          },
          rotation = {
            0.0,
            0.0,
            0.59897601604462,
            0.80076694488525
          },
          scale = 8.217137336731
        }
      },
      entity1542359869_1571 = {
        components = {
          MaterialComponent = {
            material = 2
          },
          ModelComponent = {
            asset = "assets/model/branch1.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/branch1.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                7.8632664680481,
                55.312454223633,
                154.37452697754
              },
              rotation = {
                0.0,
                0.0,
                0.66088080406189,
                0.75049090385437
              },
              scale = 8.2171087265015
            }
          }
        },
        relative_transform = {
          position = {
            -1.201162815094,
            0.67178344726563,
            91.429382324219
          },
          rotation = {
            0.0,
            0.0,
            0.66088080406189,
            0.75049090385437
          },
          scale = 8.2171087265015
        }
      },
      tree = {
        components = {
          MaterialComponent = {
            material = 2
          },
          ModelComponent = {
            asset = "assets/model/tree1.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/tree1.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                12.90625,
                60.031242370605,
                -95.937469482422
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 6.8691353797913
            }
          }
        },
        relative_transform = {
          position = {
            3.8418207168579,
            5.3905715942383,
            -158.88261413574
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 6.8691353797913
        }
      }
    }
  }
}
