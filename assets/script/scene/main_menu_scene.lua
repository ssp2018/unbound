scene = {
  entities = {
    BranchAA = {
      components = {
        ModelComponent = {
          asset = "assets/model/branch2.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/branch2.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -29.628519058228,
              10.573956489563,
              71.579986572266
            },
            rotation = {
              -1.7343987357421e-11,
              -2.9165510284646e-11,
              0.85950577259064,
              0.51112622022629
            },
            scale = 1.7153761386871
          }
        }
      },
      relative_transform = {
        1.58368,
        -0.659102,
        0,
        -0.718512,
        0.659102,
        1.58368,
        0,
        0.316228,
        0,
        0,
        1.71536,
        16.917,
        0,
        0,
        0,
        1
      }
    },
    BranchAB = {
      components = {
        ModelComponent = {
          asset = "assets/model/branch1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/branch1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -28.361734390259,
              12.177938461304,
              62.876880645752
            },
            rotation = {
              1.0574706665878e-09,
              4.7058388297216e-09,
              0.99787849187851,
              -0.065103627741337
            },
            scale = 2.415354013443
          }
        }
      },
      relative_transform = {
        -0.0648964,
        -2.41446,
        2.25187e-08,
        0.746064,
        2.41446,
        -0.0648964,
        -5.91631e-09,
        -1.10939,
        6.51922e-09,
        2.23516e-08,
        2.41534,
        8.21406,
        0,
        0,
        0,
        1
      }
    },
    HillA = {
      components = {
        ModelComponent = {
          asset = "assets/model/hill01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hill01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -24.925457000732,
              -23.566068649292,
              47.947231292725
            },
            rotation = {
              -2.1073840589247e-11,
              -1.0311402426355e-10,
              0.97974729537964,
              0.2002379745245
            },
            scale = 1.1061947345734
          }
        }
      }
    },
    HillB = {
      components = {
        ModelComponent = {
          asset = "assets/model/hill01.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/hill01.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -21.119876861572,
              -26.360147476196,
              48.601047515869
            },
            rotation = {
              0.0,
              0.0,
              0.99047875404358,
              -0.13766585290432
            },
            scale = 1.5267677307129
          }
        }
      }
    },
    entity1542099762_5635 = {
      components = {
        ModelComponent = {
          asset = "assets/model/tree1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/tree1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -17.295175552368,
              -34.212162017822,
              43.760284423828
            },
            rotation = {
              0.0,
              0.0,
              0.99047964811325,
              -0.13765923678875
            },
            scale = 1.5638760328293
          }
        }
      }
    },
    entity1542099780_1932 = {
      components = {
        ModelComponent = {
          asset = "assets/model/tree1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/tree1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -28.836761474609,
              -27.764219284058,
              43.743324279785
            },
            rotation = {
              0.0,
              0.0,
              0.43665519356728,
              0.89962893724442
            },
            scale = 0.64786392450333
          }
        }
      }
    },
    entity1542100401_1932 = {
      components = {
        ModelComponent = {
          asset = "assets/model/branch2.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/branch2.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -28.583679199219,
              -28.097099304199,
              62.221027374268
            },
            rotation = {
              -3.0697850927908e-09,
              -1.3003869092643e-08,
              0.95796811580658,
              0.28687465190887
            },
            scale = 0.49750748276711
          }
        }
      }
    },
    entity1542100811_12 = {
      components = {
        ModelComponent = {
          asset = "assets/model/tree1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/tree1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -30.174249649048,
              11.212029457092,
              29.531703948975
            },
            rotation = {
              -3.3468953303251e-11,
              -6.4304408152471e-12,
              -0.56098878383636,
              0.82782340049744
            },
            scale = 2.4153580665588
          }
        }
      },
      relative_transform = {
        -2.32379,
        0.658688,
        0,
        -0.0275526,
        -0.658688,
        -2.32379,
        0,
        0.793159,
        0,
        0,
        2.41534,
        -25.1311,
        0,
        0,
        0,
        1
      }
    },
    entity1542101239_8959 = {
      components = {
        ModelComponent = {
          asset = "assets/model/tree2.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/tree2.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -37.302852630615,
              -17.652696609497,
              20.738164901733
            },
            rotation = {
              -0.017019234597683,
              -0.0046761855483055,
              0.6209619641304,
              0.78364193439484
            },
            scale = 3.3714807033539
          }
        }
      },
      relative_transform = {
        1.58368,
        -0.659102,
        0,
        -0.718512,
        0.659102,
        1.58368,
        0,
        0.316228,
        0,
        0,
        1.71536,
        16.917,
        0,
        0,
        0,
        1
      }
    },
    entity1542101381_8228 = {
      components = {
        ModelComponent = {
          asset = "assets/model/tree2.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/tree2.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              112.84321594238,
              8.6828460693359,
              4.9801564216614
            },
            rotation = {
              -0.014271392486989,
              -0.010384947992861,
              0.85768520832062,
              0.51387214660645
            },
            scale = 3.9159729480743
          }
        }
      },
      relative_transform = {
        1.58368,
        -0.659102,
        0,
        -0.718512,
        0.659102,
        1.58368,
        0,
        0.316228,
        0,
        0,
        1.71536,
        16.917,
        0,
        0,
        0,
        1
      }
    },
    entity1542101382_1741 = {
      components = {
        ModelComponent = {
          asset = "assets/model/tree1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/tree1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              135.25799560547,
              39.208423614502,
              19.587087631226
            },
            rotation = {
              7.0183721057049e-09,
              6.4600738092224e-09,
              0.8780512213707,
              -0.47856667637825
            },
            scale = 2.8054132461548
          }
        }
      },
      relative_transform = {
        -2.32379,
        0.658688,
        0,
        -0.0275526,
        -0.658688,
        -2.32379,
        0,
        0.793159,
        0,
        0,
        2.41534,
        -25.1311,
        0,
        0,
        0,
        1
      }
    },
    entity1542101382_7104 = {
      components = {
        ModelComponent = {
          asset = "assets/model/ltree1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              282.80581665039,
              525.87060546875,
              -1443.7902832031
            },
            rotation = {
              0.0,
              0.70661723613739,
              0.70759600400925,
              0.0
            },
            scale = 4.1896548271179
          }
        }
      },
      relative_transform = {
        -2.32379,
        0.658688,
        0,
        -0.0275526,
        -0.658688,
        -2.32379,
        0,
        0.793159,
        0,
        0,
        2.41534,
        -25.1311,
        0,
        0,
        0,
        1
      }
    },
    entity1542101382_7465 = {
      components = {
        ModelComponent = {
          asset = "assets/model/tree1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/tree1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              96.904922485352,
              39.318645477295,
              15.193827629089
            },
            rotation = {
              -3.2415371720163e-08,
              2.3607359622702e-08,
              -0.23251408338547,
              0.97259300947189
            },
            scale = 2.8054320812225
          }
        }
      },
      relative_transform = {
        -2.32379,
        0.658688,
        0,
        -0.0275526,
        -0.658688,
        -2.32379,
        0,
        0.793159,
        0,
        0,
        2.41534,
        -25.1311,
        0,
        0,
        0,
        1
      }
    },
    entity1542101382_8589 = {
      components = {
        ModelComponent = {
          asset = "assets/model/tree1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/tree1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              142.99580383301,
              9.601448059082,
              31.720373153687
            },
            rotation = {
              -9.7962891132397e-09,
              -1.6869085683879e-08,
              0.87805414199829,
              -0.4785612821579
            },
            scale = 1.8164429664612
          }
        }
      }
    },
    entity1542101428_149 = {
      components = {
        ModelComponent = {
          asset = "assets/model/tree1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/tree1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -39.749168395996,
              124.39177703857,
              45.767398834229
            },
            rotation = {
              9.3572172232825e-09,
              1.8521355560353e-09,
              0.99877172708511,
              0.049547575414181
            },
            scale = 2.805447101593
          }
        }
      },
      relative_transform = {
        -2.32379,
        0.658688,
        0,
        -0.0275526,
        -0.658688,
        -2.32379,
        0,
        0.793159,
        0,
        0,
        2.41534,
        -25.1311,
        0,
        0,
        0,
        1
      }
    },
    entity1542101428_3039 = {
      components = {
        ModelComponent = {
          asset = "assets/model/tree1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/tree1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -57.144439697266,
              158.57339477539,
              41.374126434326
            },
            rotation = {
              -1.5347717763348e-08,
              3.7046891776527e-08,
              -0.70558696985245,
              0.7086233496666
            },
            scale = 2.8054654598236
          }
        }
      },
      relative_transform = {
        -2.32379,
        0.658688,
        0,
        -0.0275526,
        -0.658688,
        -2.32379,
        0,
        0.793159,
        0,
        0,
        2.41534,
        -25.1311,
        0,
        0,
        0,
        1
      }
    },
    entity1542101428_3644 = {
      components = {
        ModelComponent = {
          asset = "assets/model/tree1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/tree1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -15.864225387573,
              174.68304443359,
              11.143552780151
            },
            rotation = {
              1.727152820763e-08,
              -1.6382463385867e-08,
              0.99601113796234,
              0.089228816330433
            },
            scale = 5.5638418197632
          }
        }
      },
      relative_transform = {
        -2.32379,
        0.658688,
        0,
        -0.0275526,
        -0.658688,
        -2.32379,
        0,
        0.793159,
        0,
        0,
        2.41534,
        -25.1311,
        0,
        0,
        0,
        1
      }
    },
    entity1542101428_5135 = {
      components = {
        ModelComponent = {
          asset = "assets/model/tree2.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/tree2.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -77.138938903809,
              130.41622924805,
              31.160449981689
            },
            rotation = {
              -0.017592485994101,
              -0.0014187636552379,
              0.46385422348976,
              0.88573569059372
            },
            scale = 3.9160454273224
          }
        }
      },
      relative_transform = {
        1.58368,
        -0.659102,
        0,
        -0.718512,
        0.659102,
        1.58368,
        0,
        0.316228,
        0,
        0,
        1.71536,
        16.917,
        0,
        0,
        0,
        1
      }
    },
    entity1542101428_914 = {
      components = {
        ModelComponent = {
          asset = "assets/model/tree1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/tree1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -62.568099975586,
              104.0012512207,
              57.900657653809
            },
            rotation = {
              -1.7155439735461e-08,
              -9.2849630206615e-09,
              0.99877148866653,
              0.049553263932467
            },
            scale = 1.8164753913879
          }
        }
      }
    },
    entity1542102780_12 = {
      components = {
        ModelComponent = {
          asset = "assets/model/tree2.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/tree2.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              -38.925994873047,
              -49.400203704834,
              25.262771606445
            },
            rotation = {
              -0.01701931655407,
              -0.0046765399165452,
              0.62096631526947,
              0.78363847732544
            },
            scale = 3.3716406822205
          }
        }
      },
      relative_transform = {
        1.58368,
        -0.659102,
        0,
        -0.718512,
        0.659102,
        1.58368,
        0,
        0.316228,
        0,
        0,
        1.71536,
        16.917,
        0,
        0,
        0,
        1
      }
    },
    entity1543398717_12 = {
      components = {
        ModelComponent = {
          asset = "assets/model/ltree1.fbx"
        },
        StaticComponent = {
          contents = {
            path = "assets/model/ltree1.fbx",
            type = "Triangle"
          }
        },
        TransformComponent = {
          transform = {
            position = {
              561.65368652344,
              360.09146118164,
              -730.87426757813
            },
            rotation = {
              0.0,
              0.70661723613739,
              0.70759600400925,
              0.0
            },
            scale = 2.220123052597
          }
        }
      },
      relative_transform = {
        -2.32379,
        0.658688,
        0,
        -0.0275526,
        -0.658688,
        -2.32379,
        0,
        0.793159,
        0,
        0,
        2.41534,
        -25.1311,
        0,
        0,
        0,
        1
      }
    },
    entity1544774534_87 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              -52.548938751221,
              -61.77710723877,
              12.891921043396
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544774570_9187 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              169.74043273926,
              17.876806259155,
              15.237248420715
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544774591_2758 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              116.35073852539,
              147.97708129883,
              14.517925262451
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544774601_2728 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              -9.2763900756836,
              215.96887207031,
              25.067470550537
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544774611_5878 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              -124.60353088379,
              139.66638183594,
              8.3698806762695
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544774630_6911 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              -121.64452362061,
              112.96513366699,
              324.27429199219
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544774659_8375 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              -43.609115600586,
              -112.32881164551,
              317.54135131836
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544774673_7264 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              132.02072143555,
              -16.874710083008,
              314.94662475586
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544774689_4849 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              12.604372024536,
              205.54406738281,
              366.35192871094
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544776993_87 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              127.11965942383,
              740.19470214844,
              475.36141967773
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544777048_9187 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              132.12528991699,
              752.40222167969,
              308.99252319336
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544777053_2758 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              414.58233642578,
              768.37298583984,
              300.39947509766
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544777056_2728 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              412.77029418945,
              757.91235351563,
              504.93890380859
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544777067_5878 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              753.11529541016,
              457.23522949219,
              230.06791687012
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1544777081_6911 = {
      components = {
        NameComponent = {
          name = "world_rim"
        },
        TransformComponent = {
          transform = {
            position = {
              709.33502197266,
              339.11608886719,
              229.20498657227
            },
            rotation = {
              0.0,
              0.0,
              0.0,
              1.0
            },
            scale = 1.0
          }
        }
      }
    },
    entity1545035983_87 = {
      components = {
        GraphicTextComponent = {
          font = "arial",
          outline_color = {
            x = 0.0,
            y = 0.0,
            z = 0.0
          },
          outline_thickness = 0.03999999910593,
          text = "Unbound",
          thickness = 0.0099999997764826
        },
        TransformComponent = {
          transform = {
            position = {
              -27.881301879883,
              -28.235301971436,
              56.397789001465
            },
            rotation = {
              0.12343329936266,
              -0.021116938441992,
              -0.16355215013027,
              0.97855454683304
            },
            scale = 1.8025889396667
          }
        }
      }
    }
  },
  presets = {
    TreePreset01 = {
      entity1542017751_4481 = {
        components = {
          ModelComponent = {
            asset = "assets/model/tree1.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/tree1.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -5.4524731636047,
                -10.677422523499,
                33.31413269043
              },
              rotation = {
                0.0,
                0.0,
                0.99047875404358,
                -0.13766580820084
              },
              scale = 2.4153394699097
            }
          }
        },
        relative_transform = {
          position = {
            -0.027552604675293,
            0.79315853118896,
            -25.131114959717
          },
          rotation = {
            0.0,
            0.0,
            0.99047875404358,
            -0.13766579329967
          },
          scale = 2.4153397083282
        }
      },
      entity1542100304_12 = {
        components = {
          ModelComponent = {
            asset = "assets/model/branch1.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/branch1.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -4.6788563728333,
                -12.579969406128,
                66.659309387207
              },
              rotation = {
                4.1945522610831e-09,
                2.3740924781634e-09,
                0.71654325723648,
                0.69754266738892
              },
              scale = 2.4153368473053
            }
          }
        },
        relative_transform = {
          position = {
            0.74606418609619,
            -1.1093883514404,
            8.2140617370605
          },
          rotation = {
            4.1945522610831e-09,
            2.3740922561188e-09,
            0.71654325723648,
            0.69754266738892
          },
          scale = 2.4153368473053
        }
      },
      entity1542100344_5635 = {
        components = {
          ModelComponent = {
            asset = "assets/model/branch2.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/branch2.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                -6.1434326171875,
                -11.154353141785,
                75.362297058105
              },
              rotation = {
                0.0,
                0.0,
                0.1959145963192,
                0.98062098026276
              },
              scale = 1.715357542038
            }
          }
        },
        relative_transform = {
          position = {
            -0.71851205825806,
            0.31622791290283,
            16.917049407959
          },
          rotation = {
            0.0,
            0.0,
            0.1959145963192,
            0.98062098026276
          },
          scale = 1.715357542038
        }
      }
    },
    preset1541604664_5636 = {
      entity1541604661_12 = {
        components = {
          ModelComponent = {
            asset = "assets/model/axis.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/axis.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                1.6480169296265,
                -2.2261321544647,
                1.060982465744
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            0.82400846481323,
            -1.1130660772324,
            0.53049123287201
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      },
      origin_axis_handle = {
        components = {
          ModelComponent = {
            asset = "assets/model/axis.fbx"
          },
          StaticComponent = {
            contents = {
              path = "assets/model/axis.fbx",
              type = "Triangle"
            }
          },
          TransformComponent = {
            transform = {
              position = {
                0.0,
                0.0,
                0.0
              },
              rotation = {
                0.0,
                0.0,
                0.0,
                1.0
              },
              scale = 1.0
            }
          }
        },
        relative_transform = {
          position = {
            -0.82400846481323,
            1.1130660772324,
            -0.53049123287201
          },
          rotation = {
            0.0,
            0.0,
            0.0,
            1.0
          },
          scale = 1.0
        }
      }
    }
  }
}
