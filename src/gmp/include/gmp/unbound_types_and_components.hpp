#pragma once

#ifndef COR_UNBOUND_TYPES_AND_COMPONENTS_HPP
#define COR_UNBOUND_TYPES_AND_COMPONENTS_HPP

namespace gmp {

void initialize_unbound_types_and_components();
void initialize_unbound_types_and_components1();
void initialize_unbound_types_and_components2();
void initialize_unbound_types_and_components3();
void initialize_unbound_types_and_components4();
void initialize_unbound_types_and_components5();
void initialize_unbound_types_and_components6();
void initialize_unbound_types_and_components7();
void initialize_unbound_types_and_components8();
void initialize_unbound_types_and_components9();
}  // namespace gmp

#endif