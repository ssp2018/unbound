#pragma once

#ifndef COR_EVENT_RECORDER_HPP
#define COR_EVENT_RECORDER_HPP

#include <cor/event_manager.hpp>
#include <cor/frame_context.hpp>
#include <gmp/transform.hpp>
#include <unordered_map>

extern template class std::unique_ptr<std::vector<Event>>;

namespace gmp {
// Records and plays a given set of events
class EventRecorder {
 public:
  EventRecorder(cor::FrameContext& context);

  float update(cor::FrameContext& context, cor::SystemManager& system_mgr, float dt, float factor);

  // void start_recording(::bse::SerializedData restoration_point);
  // void stop_recording();

  void play(cor::FrameContext& context, cor::SystemManager& system_mgr);

  bool is_playing() const;

  void serialize(::bse::SerializedData& data) const;
  void deserialize(const ::bse::SerializedData& data, int& offset);
  bool is_recording() const;

 private:
  template <typename T>
  void add_listener_to_event(cor::FrameContext& context);

 private:
  struct FrameData {
    std::unique_ptr<std::vector<Event>> events;
    float dt = 0.f;
    float speed_factor;
    float unslowed_dt;
    uint16_t frame_num;
    std::unordered_map<cor::EntityHandle, Transform> transform_map;

    void serialize(::bse::SerializedData& data) const;
    void deserialize(const ::bse::SerializedData& data, int& offset);
  };

  static constexpr int FRAMES_PER_SEGMENT = 100;
  struct Segment {
    std::array<FrameData, FRAMES_PER_SEGMENT> frames;
    ::bse::SerializedData restoration_point;
    bool is_full = false;
    float recording_length = 0.f;

    // decltype(frames)::iterator end_frame;
    // decltype(frames)::iterator last_recorded_frame;

    void serialize(::bse::SerializedData& data) const;
    void deserialize(const ::bse::SerializedData& data, int& offset);

    // CLASS_SERFUN((Segment)) {
    //   SERIALIZE(self.frames);
    //   SERIALIZE(self.restoration_point);
    //   SERIALIZE(self.is_full);
    //   // int index = self.end_frame - std::begin(self.frames);
    //   // SERIALIZE(index);
    // }
    // CLASS_DESERFUN((Segment)) {
    //   DESERIALIZE(self.frames);
    //   DESERIALIZE(self.restoration_point);
    //   DESERIALIZE(self.is_full);
    //   // int index;
    //   // DESERIALIZE(index);
    //   // self.end_frame = std::begin(self.frames) + index;
    // }
  };

  enum class RecorderState {
    // IDLE,  //
    RECORDING,
    PLAYING,
    POST_PLAYBACK_DELAY
  };

  using SegmentIterator = std::array<Segment, 2>::iterator;
  using FrameIterator = decltype(Segment::frames)::iterator;

  struct Head {
    SegmentIterator segment;
    FrameIterator frame;
  };

 private:
  void step_recording_head();
  void step_playback_head();
  SegmentIterator next_segment(SegmentIterator it);
  SegmentIterator previous_segment(SegmentIterator it);
  Head next_frame(Head it);
  Head previous_frame(Head it);
  void rewind_playback_head();

  bse::SerializedData save_game_state(cor::FrameContext& context, cor::SystemManager& system_mgr);
  void restore_game_state(cor::FrameContext& context, cor::SystemManager& system_mgr,
                          bse::SerializedData& game_state);

 private:
  std::vector<cor::EventManager::ListenID> m_lids;

  std::array<Segment, 2> m_segments;

  RecorderState m_recording_state = RecorderState::RECORDING;

  Head m_recording_head;
  Head m_playback_head;

  bse::SerializedData m_end_playback_restoration_point;
  cor::SystemManager::ActiveFingerprint m_system_fingerprint_restoration;
  bool m_playback_just_started = false;
  bool m_playback_just_finished = false;

  float m_accumulated_playback_time = 0.f;
  cor::SystemManager::ActiveFingerprint m_system_fingerprint_continuous;

  float m_recording_length = 0.f;
  constexpr static float POST_PLAYBACK_DELAY_TIME = 2.5f;
  float m_post_playback_time = 2.5f;

 private:
};

}  // namespace gmp
#endif  // COR_EVENT_RECORDER_HPP
