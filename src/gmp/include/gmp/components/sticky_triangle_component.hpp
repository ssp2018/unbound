#pragma once
#ifndef GMP_STICKY_TRIANGLE_COMPONENT_HPP
#define GMP_STICKY_TRIANGLE_COMPONENT_HPP

#include "cor/entity_handle.hpp"
#include <cor/cor.hpp>

namespace gfx {
struct AABB;
}

namespace gmp {
class Transform;
}

namespace gmp {

// An entity with this component will get its
// transform updated towards its three points'
// transforms.
// Using this component only makes sense if
// your entity has a TransformComponent and
// a ModelComponent with the identity triangle
// model (triangle.fbx).
struct StickyTriangleComponent {
  std::array<cor::EntityHandle, 3> points;

  static gfx::AABB get_aabb(const Transform& transform);
};

}  // namespace gmp

UNBOUND_COMPONENT((gmp::StickyTriangleComponent), points)

#endif
