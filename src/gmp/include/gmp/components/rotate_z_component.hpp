#pragma once
#ifndef GMP_ROTATE_Z_COMPONENT_HPP
#define GMP_ROTATE_Z_COMPONENT_HPP
#include <cor/cor.hpp>

namespace gmp {

// component for handling z rotation
struct RotateZComponent {
  float rotation = 0;
};

};  // namespace gmp
UNBOUND_COMPONENT((gmp::RotateZComponent), rotation)

#endif
