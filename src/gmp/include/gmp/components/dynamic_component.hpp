#pragma once

#ifndef GMP_DYNAMIC_COMPONENT_HPP
#define GMP_DYNAMIC_COMPONENT_HPP

#include <bse/serialize.hpp>
#include <cor/cor.hpp>
#include <ext/ext.hpp>
#include <gmp/static_component_contents.hpp>

namespace gmp {
// Handles the contents of a DynamicComponent
class DynamicComponentContents : public StaticComponentContents {
  friend class ::gmp::PhysicsSystem;

 public:
  virtual ~DynamicComponentContents() = default;
  // Sets the mass of the object
  void set_mass(cor::FrameContext& context, float mass) {
    m_mass = mass;
    set_dirty();
  }

  float get_mass() const { return m_mass; }

  void apply_force(glm::vec3 force) { m_force_to_apply += force; }
  void apply_impulse(glm::vec3 impulse) { m_impulse_to_apply += impulse; }

  glm::vec3 get_velocity() const { return m_velocity; }

  CLASS_SERFUN((DynamicComponentContents)) {
    SERIALIZE_BASE_CLASS((StaticComponentContents));
    SERIALIZE(self.m_force_to_apply);
    SERIALIZE(self.m_impulse_to_apply);
    SERIALIZE(self.m_mass);
    SERIALIZE(self.m_velocity);
  }
  CLASS_DESERFUN((DynamicComponentContents)) {
    DESERIALIZE_BASE_CLASS((StaticComponentContents));
    DESERIALIZE(self.m_force_to_apply);
    DESERIALIZE(self.m_impulse_to_apply);
    DESERIALIZE(self.m_mass);
    DESERIALIZE(self.m_velocity);
  }

 private:
  // Force and impulse that the physics system will apply
  glm::vec3 m_force_to_apply = glm::vec3(0, 0, 0);
  glm::vec3 m_impulse_to_apply = glm::vec3(0, 0, 0);

  float m_mass = 1.0f;

  // Updated by PhysicsSystem
  glm::vec3 m_velocity = glm::vec3(0, 0, 0);
};

struct DynamicComponent {
  DynamicComponentContents contents;
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::DynamicComponent), contents)
UNBOUND_TYPE((gmp::DynamicComponentContents), set_collission_shape)

#endif  // GMP_DYNAMIC_COMPONENT_HPP