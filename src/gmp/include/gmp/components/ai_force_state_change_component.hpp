#pragma once
#ifndef AI_FORCE_STATE_CHANGE_COMPONENT_HPP
#define AI_FORCE_STATE_CHANGE_COMPONENT_HPP

#include <cor/cor.hpp>
#include <ext/ext.hpp>

namespace gmp {
struct AIForceStateChangeComponent {
  std::string new_state = "";

  // optional
  int new_actions_taken = 0;
  float exit_time = 1;
  std::string misc = "";
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::AIForceStateChangeComponent), new_state, new_actions_taken, exit_time, misc)

#endif
