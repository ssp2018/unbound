#pragma once
#ifndef GMP_AUDIO_COMPONENT_HPP
#define GMP_AUDIO_COMPONENT_HPP
#include "bse/file_path.hpp"  // for FilePath
#include <aud/sound_buffer.hpp>
#include <aud/sound_source.hpp>  // for SoundSource
#include <bse/asset.hpp>         // for Asset
#include <bse/reflect.hpp>       // for REFLECT, IMPL_REFLECT_1, IMPL_R...
#include <cor/cor.hpp>
#include <cor/entity_handle.hpp>
#include <ext/ext.hpp>
#include <gmp/material_type.hpp>
namespace aud {
class SoundBuffer;
}  // namespace aud

namespace gmp {

struct LoopingSoundSettings {
  LoopingSoundSettings(const LoopingSoundSettings& other) = default;
  LoopingSoundSettings(LoopingSoundSettings&& other) {
    silent_intervall_time = other.silent_intervall_time;
    silent_time_counter = other.silent_time_counter;
    cur_source_time_left = other.cur_source_time_left;
    settings = std::move(other.settings);
    active = other.active;
  };

  LoopingSoundSettings& operator=(const LoopingSoundSettings& other) = default;
  LoopingSoundSettings& operator=(LoopingSoundSettings&& other) {
    if (&other != this) {
      silent_intervall_time = other.silent_intervall_time;
      silent_time_counter = other.silent_time_counter;
      cur_source_time_left = other.cur_source_time_left;
      settings = std::move(other.settings);
      active = other.active;
    }
    return *this;
  };

  LoopingSoundSettings() = default;
  float silent_intervall_time = 0.f;
  float silent_time_counter = 0.f;
  float cur_source_time_left = 0.f;
  aud::SoundSettings settings;
  bool active;
};

struct SoundWithSettings {
  SoundWithSettings(){};
  SoundWithSettings(bse::Asset<aud::SoundBuffer> b, aud::SoundSettings s)
      : buffer(b), settings(s){};
  bse::Asset<aud::SoundBuffer> buffer;
  aud::SoundSettings settings;
};

struct DelayedSoundInfo {
  DelayedSoundInfo(cor::EntityHandle h, SoundWithSettings d, float tl)
      : handle(h), data(d), time_left(tl){};
  DelayedSoundInfo(){};
  SoundWithSettings data;
  float time_left;
  cor::EntityHandle handle;
};

// class containing Audio Component's functionality and variables
class AudioClass {
  friend class AudioSystem;

 public:
  AudioClass();
  ~AudioClass();
  AudioClass(const AudioClass& other) = delete;
  AudioClass(AudioClass&& other);  // move

  AudioClass& operator=(const AudioClass& other) = delete;
  AudioClass& operator=(AudioClass&& other);  // move

  // sets a collision sound effect to one of the material sounds
  void set_collision_sound(bse::Asset<aud::SoundBuffer> asset,
                           const std::vector<std::pair<MaterialType, aud::SoundSettings>>& args);

  // play a collision sound, depending on the material of hit entity
  void play_collision_sound(MaterialType hit_type, glm::vec3 pos);

  // set how often a sound in the periodic sound list should play
  void set_intervall_time_limit(std::string group, float val);

  // set a periodic sound group to start play its sounds
  void set_looping_sound_group_enabled(std::string group, bool val);

  // adds a new periodic sound group
  void add_periodic_sound_group(std::string group);

  // sets the current sound setting for the groups
  void set_ss_periodic_sound(std::string group, aud::SoundSettings settings);

  // handles playing sounds after a certain amount of time
  void play_sound_delayed(DelayedSoundInfo info);

  // get map with collission sound effects settings
  std::unordered_map<MaterialType, SoundWithSettings> get_collission_sound_effects();

  // set map with collission sound effects settings
  void set_collission_sound_effects(std::unordered_map<MaterialType, SoundWithSettings> col_set);

  // get map with periodic sound groups
  std::unordered_map<std::string, LoopingSoundSettings> get_periodic_sound_groups();

  // set map with periodic sound groups
  void set_periodic_sound_groups(
      std::unordered_map<std::string, LoopingSoundSettings> per_snd_grps);

  // get vector with delayed sounds
  std::vector<DelayedSoundInfo> get_delayed_sounds();

  // set vector with delayed sounds
  void set_delayed_sounds(std::vector<DelayedSoundInfo> delayed_snds);

  // Regular sounds currently being played from entity
  std::vector<bse::RuntimeAsset<aud::SoundSource>> sources;

 private:
  std::unordered_map<MaterialType,
                     SoundWithSettings>
      m_collision_sound_effects;  // collision sound effects

  std::unordered_map<std::string, LoopingSoundSettings> m_periodic_sound_groups;

  // sounds that should be played instantly or be delayed
  std::vector<DelayedSoundInfo> m_play_sound_vector;
};

struct AudioComponent {
  AudioClass audio;
};
}  // namespace gmp

REFLECT((gmp::AudioClass), set_collision_sound, play_collision_sound, sources,
        get_periodic_sound_groups, set_periodic_sound_groups)
UNBOUND_COMPONENT((gmp::AudioComponent), audio)
// REFLECT((gmp::AudioClass), set_collision_sound, play_collision_sound, sources)
UNBOUND_TYPE((aud::SoundSettings), loop, pitch, gain, gain_min, gain_max, rolloff_factor,
             max_distance)
UNBOUND_TYPE((gmp::LoopingSoundSettings), silent_intervall_time, silent_time_counter,
             cur_source_time_left, settings, active)

UNBOUND_TYPE((gmp::SoundWithSettings), buffer, settings)
UNBOUND_TYPE((gmp::DelayedSoundInfo), data, time_left, handle)
#endif
