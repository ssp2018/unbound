#pragma once
#ifndef GMP_EXPLODE_COMPONENT_HPP
#define GMP_EXPLODE_COMPONENT_HPP

#include <bse/asset.hpp>
#include <cor/cor.hpp>
#include <ext/ext.hpp>
#include <gfx/texture.hpp>

namespace gmp {
struct ExplodeComponent {
  float damage_radius = 0;         // Radius of explosion damage
  int max_damage = 0;              // Damage at center of explosion
  bool deal_damage = false;        // If true, variables above will deal damage
  float particle_speed = 0;        // Speed of explosion particles
  int particle_amount = 0;         // How many fragments the object will split into on impact
  float particle_start_size = 0;   // Start size of the fragments
  float particle_end_size = 0;     // End size of the fragments
  float time = 0;                  // How long the particles should live
  bool explode_on_impact = false;  // If the entity should explode when touching something
  // The colors it will have when exploding
  glm::vec4 min_start_color = glm::vec4(0.6, 0, 0, 1);
  glm::vec4 max_start_color = glm::vec4(0.7, 0.05, 0, 1);
  glm::vec4 min_end_color = glm::vec4(0, 0, 0, 1);
  glm::vec4 max_end_color = glm::vec4(0.1, 0, 0, 1);

  bse::Asset<gfx::Texture> texture =
      bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/explosion.png"_fp);
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::ExplodeComponent), damage_radius, max_damage, deal_damage, particle_speed,
                  particle_amount, particle_start_size, particle_end_size, time, min_start_color,
                  max_start_color, min_end_color, max_end_color, texture, explode_on_impact)

#endif
