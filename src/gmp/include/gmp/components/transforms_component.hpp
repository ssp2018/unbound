#pragma once
#ifndef GMP_TRANSFORMS_COMPONENT_HPP
#define GMP_TRANSFORMS_COMPONENT_HPP

#include <cor/cor.hpp>
#include <ext/ext.hpp>

namespace gmp {

// Component for multiple transforms
struct TransformsComponent {
  std::vector<glm::mat4x4> transforms;
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::TransformsComponent), transforms)

#endif  // GMP_ANIMATION_COMPONENT
