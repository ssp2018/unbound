#pragma once
#ifndef GMP_PLAYER_COMPONENT_HPP
#define GMP_PLAYER_COMPONENT_HPP
#include <cor/cor.hpp>

namespace gmp {
struct TransformComponent;
}

namespace gmp {

// Very simple component to keep track of the player entity
struct PlayerComponent {
  bool is_in_air = false;
  bool is_attacking = false;
  bool is_swinging = false;
  bool is_dashing = false;
  bool is_dead = false;
  float charge_time = 0.f;
  float max_charge_time = 0.f;
  float stamina = 0.f;
  float max_stamina = 0.f;
  float aim_angle = 0.f;
};

};  // namespace gmp
UNBOUND_COMPONENT((gmp::PlayerComponent), is_in_air, is_attacking, is_swinging, is_dashing, is_dead,
                  charge_time, max_charge_time, stamina, max_stamina, aim_angle)

#endif  // GMP_PLAYER_COMPONENT_HPP
