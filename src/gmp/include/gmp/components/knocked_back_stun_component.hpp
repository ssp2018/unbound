#ifndef KNOCKED_BACK_STUN_COMPONENT_HPP
#define KNOCKED_BACK_STUN_COMPONENT_HPP

#include <cor/cor.hpp>
#include <cor/entity_handle.hpp>
#include <ext/ext.hpp>

namespace gmp {
// Component for an entity currently in "knock back" cc
struct KnockedBackStunComponent {
  float max_time = 3;

  // no touching!
  float current_time = 0;
  float ground_time = 0;

  // used for knock-back of CCC, dont touch!
  glm::vec3 movement = glm::vec3(0, 0, 1);
};
}  // namespace gmp
UNBOUND_COMPONENT((gmp::KnockedBackStunComponent), max_time, current_time, ground_time, movement)
#endif