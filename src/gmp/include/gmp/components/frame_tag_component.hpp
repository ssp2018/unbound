#pragma once

#ifndef GMP_FRAME_TAG_COMPONENT_HPP
#define GMP_FRAME_TAG_COMPONENT_HPP

#include "cor/cor.hpp"
#include "cor/frame_context.hpp"

namespace gmp {
// Marks the entity with a frame number of certain importance
struct FrameTagComponent {
  // Needs to have the same type as the frame counter in FrameContext
  decltype(cor::FrameContext::frame_num) frame_num = 0;
};
}  // namespace gmp
UNBOUND_COMPONENT((gmp::FrameTagComponent), frame_num);
#endif  // GMP_FRAME_TAG_COMPONENT_HPP
