#pragma once
#ifndef GMP_TAKE_DAMAGE_OVER_TIME_COMPONENT_HPP
#define GMP_TAKE_DAMAGE_OVER_TIME_COMPONENT_HPP

#include <bse/reflect.hpp>
#include <cor/cor.hpp>
#include <ext/ext.hpp>

namespace gmp {
// representing DOT-effects
struct DamageOverTime {
  int tick_damage;      // damage dealt per tick
  float tick_interval;  // how often the damage ticks
  int ticks_remaining;  // how many more ticks of damage to apply before effect ends
  float tick_timer;     // timer ticking down to next DOT application
};

// component for a DOT effect applied on the entity
struct TakeDamageOverTimeComponent {
  std::vector<DamageOverTime> dots;
};
}  // namespace gmp
REFLECT((gmp::DamageOverTime), tick_damage, tick_interval, ticks_remaining, tick_timer)
UNBOUND_COMPONENT((gmp::TakeDamageOverTimeComponent), dots)

#endif
