#pragma once
#ifndef GMP_APPLY_DAMAGE_OVER_TIME_COMPONENT_HPP
#define GMP_APPLY_DAMAGE_OVER_TIME_COMPONENT_HPP

#include <bse/reflect.hpp>
#include <cor/cor.hpp>
#include <ext/ext.hpp>

namespace gmp {
// component for an entity's ability to deal DOT damage to another entity
// OBS! the entity must have a DamageComponent to apply dot
struct ApplyDamageOverTimeComponent {
  int tick_damage;      // damage dealt per tick
  float tick_interval;  // how often the damage ticks
  int tick_amount;      // how many ticks of damage to apply
};
}  // namespace gmp
UNBOUND_COMPONENT((gmp::ApplyDamageOverTimeComponent), tick_damage, tick_interval, tick_amount)

#endif
