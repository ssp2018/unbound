#pragma once
#ifndef GMP_TRANSFORM_2D_COMPONENT_HPP
#define GMP_TRANSFORM_2D_COMPONENT_HPP

#include <bse/asset.hpp>
#include <cor/cor.hpp>
#include <ext/ext.hpp>
#include <gfx/texture.hpp>

namespace gmp {

// texture info
struct TextureComponent {
  bse::Asset<gfx::Texture> texture;
};

}  // namespace gmp
UNBOUND_COMPONENT((gmp::TextureComponent), texture);

#endif
