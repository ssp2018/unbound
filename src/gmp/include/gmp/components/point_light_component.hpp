#pragma once
#ifndef GMP_POINT_LIGHT_COMPONENT_HPP
#define GMP_POINT_LIGHT_COMPONENT_HPP
#include <cor/cor.hpp>
#include <ext/ext.hpp>

namespace gmp {

// Point light component for light data
struct PointLightComponent {
  glm::vec3 color;
  float radius;
};

};  // namespace gmp

UNBOUND_COMPONENT((gmp::PointLightComponent), color, radius)

#endif
