#pragma once
#ifndef GMP_SHIELD_EFFECT_COMPONENT_HPP
#define GMP_SHIELD_EFFECT_COMPONENT_HPP

#include "bse/reflect.hpp"
#include "cor/entity_handle.hpp"
#include <cor/cor.hpp>

namespace gmp {
struct ShieldEffectComponent {
  float scale_offset = 1.1;
  glm::vec4 color = {0.76f, 0.125f, 0.666f, 1.0f};
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::ShieldEffectComponent), scale_offset, color)
#endif
