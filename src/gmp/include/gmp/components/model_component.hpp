#pragma once
#ifndef GMP_MODEL_COMPONENT_HPP
#define GMP_MODEL_COMPONENT_HPP

#include "bse/asset.hpp"
#include "gfx/model.hpp"
#include <cor/cor.hpp>

namespace gmp {

// Model component stores an asset and index to gfx resource
struct ModelComponent {
  bse::Asset<gfx::Model> asset;
};

};  // namespace gmp
UNBOUND_COMPONENT((gmp::ModelComponent), asset)

#endif
