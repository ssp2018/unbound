#pragma once

#ifndef GMP_ATTACH_TO_JOINT_COMPONENT_HPP
#define GMP_ATTACH_TO_JOINT_COMPONENT_HPP

#include "cor/entity_handle.hpp"
#include <cor/cor.hpp>
#include <ext/ext.hpp>

namespace gmp {
// Attaches the current entity's transform onto another entity's joint
struct AttachToJointComponent {
  cor::EntityHandle entity_handle;
  std::string joint_name;
  glm::mat4 offset_matrix = glm::mat4(1);
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::AttachToJointComponent), entity_handle, joint_name, offset_matrix);

#endif  // GMP_ATTACH_TO_JOINT_COMPONENT_HPP
