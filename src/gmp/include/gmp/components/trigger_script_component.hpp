#pragma once

#ifndef GMP_TRIGGER_SCRIPT_COMPONENT_HPP
#define GMP_TRIGGER_SCRIPT_COMPONENT_HPP

#include <cor/cor.hpp>
#include <ext/ext.hpp>
#include <scr/object.hpp>

namespace gmp {
// Triggers a script function when colliding with another entity
struct TriggerScriptComponent {
  std::string name;
  cor::EntityHandle target_entity;
  scr::Object on_touch_callback;
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::TriggerScriptComponent), name, target_entity, on_touch_callback)

#endif  // GMP_TRIGGER_SCRIPT_COMPONENT_HPP
