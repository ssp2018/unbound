#pragma once
#ifndef HOOK_HIT_COMPONENT_HPP
#define HOOK_HIT_COMPONENT_HPP

#include <cor/entity_handle.hpp>

namespace gmp {
struct HookHitComponent {
  cor::EntityHandle target;
  cor::EntityHandle hook;
  cor::EntityHandle rope;
};
}  // namespace gmp
UNBOUND_COMPONENT((gmp::HookHitComponent), target, hook, rope)
#endif
