#pragma once
#ifndef GMP_HEALTH_COMPONENT_HPP
#define GMP_HEALTH_COMPONENT_HPP

#include "cor/entity_handle.hpp"
#include "cor/frame_context.hpp"
#include <cor/cor.hpp>
#include <cor/entity.hpp>
#include <cor/script_type.hpp>
#include <ext/ext.hpp>
#include <scr/object.hpp>
#include <scr/script.hpp>

namespace cor {
struct FrameContext;
}

namespace gmp {
// component for an entities health points
struct HealthComponent {
  int health_points;      // current HP of the entity
  int max_health_points;  // the "original" max HP of the entity
  cor::EntityHandle last_hit_by;

  bse::Asset<aud::SoundBuffer> damage_sound;

  scr::Object death_callback =
      bse::Asset<scr::Script>(bse::ASSETS_ROOT / "script/gmp/death_callbacks.lua"_fp)
          ->return_value()["default_death_callback"];  // Script callback on death
};
}  // namespace gmp
UNBOUND_COMPONENT((gmp::HealthComponent), health_points, max_health_points, last_hit_by,
                  damage_sound, death_callback)

#endif
