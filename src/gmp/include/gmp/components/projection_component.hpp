#pragma once
#ifndef GMP_PROJECTION_COMPONENT_HPP
#define GMP_PROJECTION_COMPONENT_HPP

#include <cor/cor.hpp>
#include <ext/ext.hpp>

namespace gmp {

// projection component handling a projection matrix
struct ProjectionComponent {
  float aspectRatio;
  float near;
  float far;
  float field_of_view;  // in degrees
  glm::mat4 projection_matrix;
};

};  // namespace gmp
UNBOUND_COMPONENT((gmp::ProjectionComponent), aspectRatio, near, far, field_of_view,
                  projection_matrix)
#endif
