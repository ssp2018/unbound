#pragma once

#ifndef GMP_DEBUG_CAMERA_COMPONENT_HPP
#define GMP_DEBUG_CAMERA_COMPONENT_HPP

#include <cor/cor.hpp>

namespace gmp {

// Denotes an entity as controllable by the DebugCameraSystem
struct DebugCameraComponent {
  float speed = 1.f;
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::DebugCameraComponent), speed);

#endif  // GMP_DEBUG_CAMERA_COMPONENT_HPP
