#ifndef KNOCK_BACK_COMPONENT_HPP
#define KNOCK_BACK_COMPONENT_HPP

#include <cor/cor.hpp>
#include <cor/entity_handle.hpp>
#include <ext/ext.hpp>

namespace gmp {
struct ForceDir {
  bool apply_force = false;
  float force = 0;
};

// Component for an entity currently in "knock back" cc
struct KnockBackComponent {
  float max_time = 3;
  float radius = 0;
  float force = 5;

  std::array<ForceDir, 3> force_dir_override;
};
}  // namespace gmp
UNBOUND_TYPE((gmp::ForceDir), apply_force, force)
UNBOUND_COMPONENT((gmp::KnockBackComponent), max_time, radius, force, force_dir_override)
#endif