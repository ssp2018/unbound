#pragma once
#ifndef HOMING_COMPONENT_HPP
#define HOMING_COMPONENT_HPP

#include <cor/cor.hpp>
#include <cor/entity_handle.hpp>
#include <ext/ext.hpp>

namespace gmp {
struct HomingComponent {
  cor::EntityHandle target;
  float speed = 1.f;
  float turn_speed = 1.f;
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::HomingComponent), target, speed, turn_speed)

#endif
