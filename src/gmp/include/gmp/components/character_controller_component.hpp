#pragma once

#ifndef GMP_CHARACTER_CONTROLLER_COMPONENT_HPP
#define GMP_CHARACTER_CONTROLLER_COMPONENT_HPP
#include <cor/cor.hpp>
#include <gmp/physics_component_contents_base.hpp>
#include <phy/collision_groups.hpp>

namespace cor {
struct FrameContext;
}

namespace gmp {

// Handles the contents of a CharacterControllerComponent
class CharacterControllerComponentContents : public PhysicsComponentContentsBase {
  friend class ::gmp::PhysicsSystem;

 public:
  virtual ~CharacterControllerComponentContents() = default;
  // Sets the radius of the character controller capsule
  void set_radius(cor::FrameContext& context, float radius) {
    m_radius = radius;
    set_dirty();
  }

  float get_radius() const { return m_radius; }

  // Sets the height of the character controller capsule
  void set_height(cor::FrameContext& context, float height) {
    m_height = height;
    set_dirty();
  }

  float get_height() const { return m_height; }

  // Sets the mass of the object
  void set_mass(cor::FrameContext& context, float mass) {
    m_mass = mass;
    set_dirty();
  }

  float get_mass() const { return m_mass; }

  // Set move vector. During update, this vector will be used to move the character
  // controller.
  void set_move_vector(glm::vec3 move_vector) { m_move_vector += move_vector; }

  // Set turn speed. This only affects the visual rotation, the movement direction can change
  // arbitrarily
  void set_turning_speed(float speed) {
    m_turn_speed = speed;
    set_dirty();
  }
  float get_turning_speed() const { return m_turn_speed; }

  // Set base turn speed, representing the original turn speed of the entity
  void set_base_turning_speed(float base_speed) {
    m_base_turn_speed = base_speed;
    set_dirty();
  }
  float get_base_turning_speed() const { return m_base_turn_speed; }

  // Set facing angle in radians. This only affects the visual rotation. Only considered if
  // set_face_moving_direction(false) ha been called
  void set_facing_angle(float angle) {
    m_angle = angle;
    set_dirty();
  }

  // Returns facing angle. Only valid if the facing direction is set manually
  float get_facing_angle() const { return m_angle; }

  // Set facing direction with a 3D vector
  void set_facing_direction(glm::vec3 dir) {
    // Only works when z is up direction for the character
    glm::vec3 normal = glm::vec3(0, 0, 1);
    // Project the direction onto the plane corresponding to the normal.
    glm::vec3 plane_proj = dir - (glm::dot(dir, normal) * normal);
    plane_proj = glm::normalize(plane_proj);
    // Is the shortest angle between the two vectors
    float angle = glm::angle(plane_proj, glm::vec3(0, 1, 0));
    // Check which side of the unit circle the direction is in the x-axis
    float sign = glm::dot(dir, glm::vec3(1, 0, 0));
    if (sign > 0.0f) {
      angle = 2 * glm::pi<float>() - angle;
    }
    set_facing_angle(angle);
  }

  // If true character will turn toward its movement direction, otherwise, it will face toward
  // the angle determined by set_facing_angle()
  void set_face_moving_direction(bool face_moving_direction) {
    m_face_moving_direction = face_moving_direction;
    set_dirty();
  }
  bool get_face_moving_direction() const { return m_face_moving_direction; }

  // Set gravity for this character controller
  void set_gravity(glm::vec3 gravity) {
    m_gravity = gravity;
    set_dirty();
  }
  glm::vec3 get_gravity() const { return m_gravity; }

  glm::vec3 get_velocity() const { return m_velocity; };

  float get_speed() const { return glm::length(m_velocity); }

  // Returns true if character is on ground
  bool on_ground() const { return m_on_ground; }

  // Add velocity in up direction
  void jump(float strength) {
    m_jump_strength += strength;
    m_on_ground = false;
    set_dirty();
  }

  // Command the character controller to set velocity for a duration
  void dash(glm::vec3 dash_velocity, float duration) {
    m_dash_vector += dash_velocity;
    m_dash_duration += duration;
    set_dirty();
  }

  // Reset turn speed to the original value
  void reset_turn_speed() { m_turn_speed = m_base_turn_speed; }

  // Enter swinging mode, attatching to swing_center
  void start_swing(glm::vec3 swing_center) {
    m_start_swing = true;
    m_swing_center = swing_center;
    set_dirty();
  }

  // Leaves swinging mode
  void end_swing() {
    m_end_swing = true;
    set_dirty();
  }

  // Return the position swinging rope is attatched to
  glm::vec3 get_swing_center() const { return m_swing_center; }

  // Return distance from swinging attatchment point
  float get_rope_length() const { return m_rope_length; }

  // Set the distance to swinging attatchment point
  void set_rope_length(float length) {
    m_set_rope_length = length;
    m_rope_length = length;
    set_dirty();
  }

  void add_swing_velocity(glm::vec3 velocity) { m_swing_velocity += velocity; }

  // If set to true, character controller will only collide with the navmesh and other character
  // controllers
  void set_use_navmesh_collision(bool use_navmesh) {
    m_use_navmesh_collision = use_navmesh;
    set_dirty();
  }

  bool get_use_navmesh_collision() const { return m_use_navmesh_collision; }

  void set_up(glm::vec3 up) {
    m_up = up;
    set_dirty();
  }

  glm::vec3 get_up() const { return m_up; }

  void set_is_flying(bool is_flying) {
    m_is_flying = is_flying;
    set_dirty();
  }

  bool get_is_flying() const { return m_is_flying; }

  CLASS_SERFUN((CharacterControllerComponentContents)) {
    SERIALIZE_BASE_CLASS((PhysicsComponentContentsBase));
    SERIALIZE(self.m_gravity);
    SERIALIZE(self.m_move_vector);
    SERIALIZE(self.m_dash_vector);
    SERIALIZE(self.m_swing_center);
    SERIALIZE(self.m_gravity);
    SERIALIZE(self.m_swing_velocity);
    SERIALIZE(self.m_velocity);
    SERIALIZE(self.m_jump_strength);
    SERIALIZE(self.m_dash_duration);
    SERIALIZE(self.m_mass);
    SERIALIZE(self.m_radius);
    SERIALIZE(self.m_height);
    SERIALIZE(self.m_rope_length);
    SERIALIZE(self.m_set_rope_length);
    SERIALIZE(self.m_angle);
    SERIALIZE(self.m_turn_speed);
    SERIALIZE(self.m_base_turn_speed);
    SERIALIZE(self.m_face_moving_direction);
    SERIALIZE(self.m_on_ground);
    SERIALIZE(self.m_start_swing);
    SERIALIZE(self.m_end_swing);
    SERIALIZE(self.m_is_swinging);
    SERIALIZE(self.m_use_navmesh_collision);
    SERIALIZE(self.m_is_flying);
    SERIALIZE(self.m_up);
    SERIALIZE(self.m_desired_direction);
    SERIALIZE(self.m_facing_direction);
  }
  CLASS_DESERFUN((CharacterControllerComponentContents)) {
    DESERIALIZE_BASE_CLASS((PhysicsComponentContentsBase));
    DESERIALIZE(self.m_gravity);
    DESERIALIZE(self.m_move_vector);
    DESERIALIZE(self.m_dash_vector);
    DESERIALIZE(self.m_swing_center);
    DESERIALIZE(self.m_gravity);
    DESERIALIZE(self.m_swing_velocity);
    DESERIALIZE(self.m_velocity);
    DESERIALIZE(self.m_jump_strength);
    DESERIALIZE(self.m_dash_duration);
    DESERIALIZE(self.m_mass);
    DESERIALIZE(self.m_radius);
    DESERIALIZE(self.m_height);
    DESERIALIZE(self.m_rope_length);
    DESERIALIZE(self.m_set_rope_length);
    DESERIALIZE(self.m_angle);
    DESERIALIZE(self.m_turn_speed);
    DESERIALIZE(self.m_base_turn_speed);
    DESERIALIZE(self.m_face_moving_direction);
    DESERIALIZE(self.m_on_ground);
    DESERIALIZE(self.m_start_swing);
    DESERIALIZE(self.m_end_swing);
    DESERIALIZE(self.m_is_swinging);
    DESERIALIZE(self.m_use_navmesh_collision);
    DESERIALIZE(self.m_is_flying);
    DESERIALIZE(self.m_up);
    DESERIALIZE(self.m_desired_direction);
    DESERIALIZE(self.m_facing_direction);
  }

 private:
  mutable glm::quat m_desired_direction = glm::quat(0, 0, 0, 1);
  mutable glm::quat m_facing_direction = glm::quat(0, 0, 0, 1);

  glm::vec3 m_gravity = glm::vec3(0.0f, 0.0f, -9.82f);
  glm::vec3 m_up = glm::vec3(0.0f, 0.0f, 1.0f);
  mutable glm::vec3 m_move_vector = glm::vec3(0.0f, 0.0f, 0.0f);
  glm::vec3 m_dash_vector = glm::vec3(0.0f, 0.0f, 0.0f);
  glm::vec3 m_swing_center = glm::vec3(0.0f, 0.0f, 0.0f);

  // Velocity to apply
  glm::vec3 m_swing_velocity = glm::vec3(0.0f, 0.0f, 0.0f);

  // Updated by PhysicsSystem
  glm::vec3 m_velocity = glm::vec3(0.0f, 0.0f, 0.0f);

  float m_jump_strength = 0.0f;
  float m_dash_duration = 0.0f;

  float m_mass = 1.0f;
  float m_radius = 1.0f;
  float m_height = 2.0f;

  // Length to swinging attatchment point
  float m_rope_length = 0.0f;

  // Nonzero if PhysicsSystem needs to set rope length
  float m_set_rope_length = 0.0f;

  // The angle to face if 'm_face_moving_direction' is false
  float m_angle = 0.0f;

  // The speed of turning (visual only, movement turning not affected)
  float m_turn_speed = 15.0f;

  float m_base_turn_speed = 15.0f;

  // If false, character will face toward 'm_angle' instead of moving direction
  bool m_face_moving_direction = true;

  // Updated by PhysicsSystem
  bool m_on_ground = false;

  // True if PhysicsSystem needs to start a swing
  bool m_start_swing = false;

  // True if PhysicsSystem needs to end a swing
  bool m_end_swing = false;

  // True if character is in swinging mode
  bool m_is_swinging = false;

  bool m_use_navmesh_collision = true;

  bool m_is_flying = false;
};

struct CharacterControllerComponent {
  CharacterControllerComponentContents contents;
};
}  // namespace gmp

UNBOUND_TYPE((gmp::CharacterControllerComponentContents), set_move_vector, set_turning_speed,
             get_turning_speed, set_facing_angle, set_face_moving_direction, reset_turn_speed,
             on_ground, get_velocity, get_speed, get_height, set_height, set_radius, set_mass,
             set_gravity, set_offset_transform, get_radius, get_mass, get_gravity,
             get_offset_transform, set_base_turning_speed, set_use_navmesh_collision,
             get_use_navmesh_collision, set_up)
UNBOUND_COMPONENT((gmp::CharacterControllerComponent), contents)

#endif  // GMP_CHARACTER_CONTROLLER_COMPONENT_HPP
