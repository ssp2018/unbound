#pragma once

#ifndef GMP_THREAT_COMPONENT_HPP
#define GMP_THREAT_COMPONENT_HPP

#include <cor/cor.hpp>
#include <cor/entity_handle.hpp>
#include <ext/ext.hpp>

namespace gmp {
// Component which contains about the threat level for other entities, all enemies that should be
// able to aggro other entities must have this component
struct ThreatComponent {
  std::unordered_map<cor::EntityHandle, float> threats;
  int max_number_of_threats = 5;
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::ThreatComponent), threats, max_number_of_threats)

#endif  // GMP_THREAT_COMPONENT_HPP