#pragma once
#ifndef GMP_DESTRUCTIBLE_COMPONENT_HPP
#define GMP_DESTRUCTIBLE_COMPONENT_HPP

#include <scr/object.hpp>

namespace gmp {
struct DestructibleComponent {
  int health_points;      // current HP of the entity
  int max_health_points;  // the "original" max HP of the entity

  scr::Object death_callback = scr::Object::NULL_OBJECT;  // Script callback on death
};
}  // namespace gmp
UNBOUND_COMPONENT((gmp::DestructibleComponent), health_points, max_health_points, death_callback)

#endif
