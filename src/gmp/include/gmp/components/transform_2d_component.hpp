#pragma once
#ifndef GMP_TEXTURE_COMPONENT_HPP
#define GMP_TEXTURE_COMPONENT_HPP

#include <cor/cor.hpp>
#include <ext/ext.hpp>
namespace gmp {
// Data for 2d transform
struct Transform2dComponent {
  glm::vec3 position;
  glm::vec2 size;
  float rotation;  // degrees

  enum class Centering : unsigned char { NONE, LEFT, CENTER, RIGHT };
  Centering centering = Centering::NONE;
};
}  // namespace gmp
UNBOUND_COMPONENT((gmp::Transform2dComponent), position, size, rotation);

#endif
