#pragma once
#ifndef GMP_FACTION_COMPONENT_HPP
#define GMP_FACTION_COMPONENT_HPP

#include "aud/sound_source.hpp"
#include <cor/cor.hpp>

namespace gmp {

enum Faction {
  PLAYER = 0,  // Player and NPCs could have this
  NATURE = 1,  // Wild animals and such
  HUMAN = 2,   // Enemies towards nature and player faction
  DEMON = 3    // Enemies with all other factions
};

struct FactionComponent {
  Faction type;
  aud::SoundType sound_category;
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::FactionComponent), type, sound_category)

#endif
