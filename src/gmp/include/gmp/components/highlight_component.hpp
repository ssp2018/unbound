#pragma once
#ifndef GMP_HIGHLIGHT_COMPONENT_HPP
#define GMP_HIGHLIGHT_COMPONENT_HPP

#include <cor/cor.hpp>
#include <ext/ext.hpp>
#include <scr/object.hpp>

namespace gmp {
// component for highlighting models
struct HighlightComponent {
  bool wireframe_only = false;
};
}  // namespace gmp
UNBOUND_COMPONENT((gmp::HighlightComponent), wireframe_only)

#endif
