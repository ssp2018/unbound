#pragma once
#ifndef SPAWN_COMPONENT_HPP
#define SPAWN_COMPONENT_HPP

#include <cor/cor.hpp>
#include <cor/entity_handle.hpp>
#include <ext/ext.hpp>
#include <gmp/components/ai_component.hpp>
#include <gmp/components/transform_component.hpp>
namespace gmp {

struct SpawnComponent {
  AIType type = AIType::FLYING_SKULL;
  int max_spawns;
  float delay;
  float timer = 0.f;
  float min_dist_to_player = 0.0f;
  float aggro_range_scale = 1.f;
  float cutoff_dist = -1.f;
  std::vector<cor::EntityHandle> spawned_enteties;
};
};  // namespace gmp

UNBOUND_COMPONENT((gmp::SpawnComponent), type, max_spawns, delay, timer, min_dist_to_player,
                  spawned_enteties);
#endif
