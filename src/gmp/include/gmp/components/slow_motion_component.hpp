#pragma once
#ifndef GMP_SLOW_MOTION_COMPONENT_HPP
#define GMP_SLOW_MOTION_COMPONENT_HPP
#include <cor/cor.hpp>

namespace cor {
struct FrameContext;
}

namespace gmp {

// Small helper class for keeping track of slow motion time
// Holds a float that is a factor (0-1) that represents how much of the global slow motion that
// should affect. 1=slow down as much as everything else, 0=no slow down at all, 0.5=slow down half
// as much as everything else
class SlowMotionFactor {
 public:
  float get_factor() const;
  void set_factor(const float factor);

  // Converts the global dt to the one specific to the current entity, using the internal slow
  // motion factor
  float get_converted_dt(const cor::FrameContext &context) const;

 private:
  float m_slow_motion_factor;
};

// Component for any entity that should have a custom slow motion factor (move faster than the rest
// of the world)
struct SlowMotionComponent {
  SlowMotionFactor factor;
};

};  // namespace gmp
UNBOUND_TYPE((gmp::SlowMotionFactor), set_factor, get_factor)
UNBOUND_COMPONENT((gmp::SlowMotionComponent), factor);

#endif  // GMP_SLOW_MOTION_COMPONENT_HPP
