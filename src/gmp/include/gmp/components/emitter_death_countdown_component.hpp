#pragma once

#ifndef GMP_EMITTER_DEATH_COUNTDOWN_HPP
#define GMP_EMITTER_DEATH_COUNTDOWN_HPP

#include <cor/cor.hpp>
#include <cor/entity_handle.hpp>
#include <ext/ext.hpp>

namespace gmp {
// component that represents lifetime left of an emitter
struct EmitterDeathCountdownComponent {
  float death_countdown = 2;
  bool handled = false;
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::EmitterDeathCountdownComponent), death_countdown, handled)

#endif  // GMP_EMITTER_DEATH_COUNTDOWN_HPP