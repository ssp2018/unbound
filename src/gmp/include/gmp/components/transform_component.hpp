#pragma once
#ifndef GMP_TRANSFORM_COMPONENT_HPP
#define GMP_TRANSFORM_COMPONENT_HPP

#include <cor/cor.hpp>
#include <ext/ext.hpp>
#include <gmp/transform.hpp>

namespace gmp {
// Transform component for handling transforms in a entity
struct TransformComponent {
  Transform transform;
};

};  // namespace gmp

UNBOUND_COMPONENT((gmp::TransformComponent), transform)

#endif
