#pragma once
#ifndef GMP_SATURATION_COMPONENT_HPP
#define GMP_SATURATION_COMPONENT_HPP
#include <cor/cor.hpp>

namespace gmp {

// component for handling saturation. Values below 1 gives lower saturation
struct SaturationComponent {
  float saturation = 0;
};

};  // namespace gmp
UNBOUND_COMPONENT((gmp::SaturationComponent), saturation)

#endif
