#pragma once
#ifndef STEERING_COMPONENT_HPP
#define STEERING_COMPONENT_HPP
#include <cor/cor.hpp>
#include <cor/entity_handle.hpp>
#include <cor/frame_context.hpp>
#include <ext/ext.hpp>
namespace gmp {
// Component containing data needed for steering.
struct SteeringComponent {
  std::variant<glm::vec3, cor::EntityHandle> goal;
  glm::vec3 avoid_vec = {0.0f, 0.0f, 0.0f};
  glm::vec3 vel_curr = {0.0f, 0.0f, 0.0f};
  float max_speed = 35.0f;
  float stop_dist = 5.0f;
  float accel_multiply = 25.0f;
  float max_see_ahead = 10.0f;
  float max_steer_force = 10.0f;
  float max_avoid_force = 15.0f;
  float avoid_fall_off_time = 0.5f;
  float time_left_major_threat = 0.0f;
  bool on_the_move = false;
};
// sets a glm::vec3 as a goal for SteeringComponent
void set_goal_vec(SteeringComponent& sc, glm::vec3& goal);
// sets a entity handle as goal for SteeringComponent
void set_goal_entity(SteeringComponent& sc, cor::EntityHandle& e_handle);
// extracts the goal position from the Steering component.
glm::vec3 get_goal_pos(SteeringComponent& sc, cor::FrameContext& context);
}  // namespace gmp

UNBOUND_COMPONENT((gmp::SteeringComponent), vel_curr, max_speed, on_the_move, stop_dist,
                  max_see_ahead, max_steer_force, max_avoid_force, avoid_fall_off_time,
                  time_left_major_threat, accel_multiply)
#endif
