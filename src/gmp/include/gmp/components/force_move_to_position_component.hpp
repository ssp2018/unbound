#ifndef FORCE_MOVE_TO_POSITION_COMPONENT_HPP
#define FORCE_MOVE_TO_POSITION_COMPONENT_HPP

#include <cor/cor.hpp>
#include <ext/ext.hpp>

namespace gmp {
// component used to force an entity to move to a position
struct ForceMoveToPositionComponent {
  glm::vec3 destination;
  float release_distance;
  float speed;
};
}  // namespace gmp
UNBOUND_COMPONENT((gmp::ForceMoveToPositionComponent), destination, release_distance, speed)
#endif
