#pragma once
#ifndef GMP_MATERIAL_COMPONENT_HPP
#define GMP_MATERIAL_COMPONENT_HPP

#include <gmp/material_type.hpp>

namespace gmp {

struct MaterialComponent {
  MaterialType material;
};

}  // namespace gmp

UNBOUND_COMPONENT((gmp::MaterialComponent), material)

#endif  // GMP_MATERIAL_COMPONENT_HPP