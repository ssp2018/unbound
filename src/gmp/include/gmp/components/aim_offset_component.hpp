#pragma once
#ifndef GMP_AIM_OFFSET_COMPONENT
#define GMP_AIM_OFFSET_COMPONENT

#include <cor/cor.hpp>
#include <ext/ext.hpp>

namespace gmp {
struct AimOffsetComponent {
  std::string m_joint_name;  // Joint to affect
  mutable float m_previous_angle = 0.0f;
  mutable float m_blend_ratio = 0.0f;
  mutable float m_blend_aim_blend_speed = 10.0f;
  mutable float m_blend_return_speed = 1.5f;
};

}  // namespace gmp

UNBOUND_COMPONENT((gmp::AimOffsetComponent), m_joint_name, m_previous_angle, m_blend_ratio,
                  m_blend_aim_blend_speed, m_blend_return_speed)

#endif