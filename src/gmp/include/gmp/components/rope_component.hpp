#pragma once
#ifndef GMP_ROPE_COMPONENT_HPP
#define GMP_ROPE_COMPONENT_HPP

#include <bse/asset.hpp>
#include <cor/cor.hpp>
#include <cor/entity_handle.hpp>
#include <ext/ext.hpp>
#include <gfx/texture.hpp>

namespace gmp {

// rope component for rendering a rope between this and another entity
struct RopeComponent {
  cor::EntityHandle other;
  bse::Asset<gfx::Texture> texture =
      bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/rope.png"_fp);
  float scale;
};

};  // namespace gmp
UNBOUND_COMPONENT((gmp::RopeComponent), other, texture, scale)
#endif
