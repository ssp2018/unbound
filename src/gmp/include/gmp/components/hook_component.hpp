#pragma once
#ifndef HOOK_COMPONENT_HPP
#define HOOK_COMPONENT_HPP

#include "cor/entity.hpp"
#include "cor/entity_handle.hpp"
#include <cor/cor.hpp>
#include <ext/ext.hpp>

namespace gmp {
struct HookComponent {
  cor::EntityHandle owner;
  cor::EntityHandle rope;
  glm::vec3 origin;
  float hook_pull_in_speed = 20;
  float release_distance = 10;

  // dont touch these:
  bool hooked = false;
  bool destination_reached = false;
};
}  // namespace gmp
UNBOUND_COMPONENT((gmp::HookComponent), owner, rope, origin, hook_pull_in_speed, release_distance,
                  hooked, destination_reached)
#endif
