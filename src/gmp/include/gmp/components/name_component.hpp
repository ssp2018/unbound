#pragma once
#ifndef GMP_NAME_COMPONENT_HPP
#define GMP_NAME_COMPONENT_HPP

#include "bse/reflect.hpp"
#include "cor/entity_handle.hpp"
#include <cor/cor.hpp>

namespace gmp {
struct NameComponent {
  std::string name;
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::NameComponent), name)
#endif
