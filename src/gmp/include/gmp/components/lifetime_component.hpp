#pragma once
#ifndef GMP_LIFETIME_COMPONENT_HPP
#define GMP_LIFETIME_COMPONENT_HPP
#include <cor/cor.hpp>

namespace gmp {

// Component for making an entity be destroyed after a certain amount of time
struct LifetimeComponent {
  float time_left = 1;  // Time left to live, in seconds
};

}  // namespace gmp
UNBOUND_COMPONENT((gmp::LifetimeComponent), time_left);

#endif  // GMP_LIFETIME_COMPONENT_HPP
