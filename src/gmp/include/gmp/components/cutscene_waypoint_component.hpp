#pragma once

#ifndef GMP_CUTSCENE_WAYPOINT_COMPONENT_HPP
#define GMP_CUTSCENE_WAYPOINT_COMPONENT_HPP

#include <cor/cor.hpp>

namespace gmp {
// Represents a waypoint in a camera-locked cutscene
struct CutsceneWaypointComponent {
  //
  enum Type { CONTINUOUS, INSTANT };
  Type type = Type::CONTINUOUS;
  int index = 0;
  int fov = -1;
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::CutsceneWaypointComponent), type, index, fov)

#endif  // GMP_CUTSCENE_WAYPOINT_COMPONENT_HPP
