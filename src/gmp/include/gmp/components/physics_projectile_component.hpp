#pragma once

#ifndef GMP_PHYSICS_PROJECTILE_COMPONENT_HPP
#define GMP_PHYSICS_PROJECTILE_COMPONENT_HPP

#include <cor/cor.hpp>
#include <ext/ext.hpp>
#include <gmp/physics_component_contents_base.hpp>
#include <phy/collision_groups.hpp>

namespace gmp {
// Handles the contents of a PhysicsProjectileComponent
class PhysicsProjectileComponentContents : public PhysicsComponentContentsBase {
  friend class gmp::PhysicsSystem;

 public:
  virtual ~PhysicsProjectileComponentContents() = default;
  glm::vec3 get_velocity() const;
  void set_velocity(glm::vec3 velocity);

  glm::vec3 get_gravity() const;
  void set_gravity(glm::vec3 gravity);

  float get_length() const;

  // Get length of projectile body. Note that the body is a capsule, and 'length' only affects the
  // cylindrical portion of the capsule. The total length will be 'length' + radius * 2
  void set_length(float length);

  float get_radius() const;

  // Set thickness of projectile
  void set_radius(float radius);

  float get_mass() const;

  // Set mass of projectile (currently unused)
  void set_mass(float mass);

  bool get_has_hit() const;

  void set_has_hit(bool hit);

  // Disables collision reporting for specified duration
  void disable_collision_reports_for(float duration);

  // Reenables collision reporting if it is disabled
  void enable_collision_reports();

  // Disables destruction on hit for specified duration
  void disable_destruction_for(float duration);

  // Reenables destruction on hit if it is disabled
  void enable_destruction();

  CLASS_SERFUN((PhysicsProjectileComponentContents)) {
    SERIALIZE_BASE_CLASS((PhysicsComponentContentsBase));
    SERIALIZE(self.m_velocity);
    SERIALIZE(self.m_gravity);
    SERIALIZE(self.m_disable_reporting_time);
    SERIALIZE(self.m_disable_destruction_time);
    SERIALIZE(self.m_length);
    SERIALIZE(self.m_radius);
    SERIALIZE(self.m_mass);
    SERIALIZE(self.m_has_hit);
  }
  CLASS_DESERFUN((PhysicsProjectileComponentContents)) {
    DESERIALIZE_BASE_CLASS((PhysicsComponentContentsBase));
    DESERIALIZE(self.m_velocity);
    DESERIALIZE(self.m_gravity);
    DESERIALIZE(self.m_disable_reporting_time);
    DESERIALIZE(self.m_disable_destruction_time);
    DESERIALIZE(self.m_length);
    DESERIALIZE(self.m_radius);
    DESERIALIZE(self.m_mass);
    DESERIALIZE(self.m_has_hit);
  }

 private:
  glm::vec3 m_velocity = glm::vec3(0, 0, 0);
  glm::vec3 m_gravity = glm::vec3(0, 0, -9.82f);

  // This value can't be read by users, mutable should be thread-safe
  // Remaining time collision should be disabled for
  mutable float m_disable_reporting_time = 0.0f;
  mutable float m_disable_destruction_time = 0.0f;

  float m_length = 0.3f;
  float m_radius = 0.1f;
  float m_mass = 1.0f;

  // True if the projectile has hit an object and needs to be removed
  bool m_has_hit = false;
};  // namespace gmp

struct PhysicsProjectileComponent {
  PhysicsProjectileComponentContents contents;

  // Set to true for projectiles that need to check collision against, and stick to, animated meshes
  bool stick_to_animated = false;

  // If not set to NONE, projectile will not collide with characters without these groups
  phy::CollisionGroup::Group character_group = phy::CollisionGroup::NONE;
};
}  // namespace gmp

UNBOUND_TYPE((gmp::PhysicsProjectileComponentContents), get_velocity, set_offset_transform,
             set_velocity, get_gravity, set_gravity, get_length, set_length, get_radius, set_radius,
             get_mass, set_mass, disable_collision_reports_for, enable_collision_reports,
             disable_destruction_for, enable_destruction, get_has_hit, set_has_hit)
UNBOUND_COMPONENT((gmp::PhysicsProjectileComponent), contents, stick_to_animated, character_group)
#endif  // GMP_PHYSICS_PROJECTILE_COMPONENT_HPP