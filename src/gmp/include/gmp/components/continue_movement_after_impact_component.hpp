#pragma once
#ifndef CONTINUE_MOVEMENT_AFTER_IMPACT_COMPONENT_HPP
#define CONTINUE_MOVEMENT_AFTER_IMPACT_COMPONENT_HPP

#include <cor/cor.hpp>

namespace gmp {
struct ContinueMovementAfterImpactComponent {
  // empty
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::ContinueMovementAfterImpactComponent))

#endif
