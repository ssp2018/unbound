#pragma once
#ifndef GMP_GRAPHIC_TEXT_COMPONENT_HPP
#define GMP_GRAPHIC_TEXT_COMPONENT_HPP
#include <cor/cor.hpp>

namespace gmp {

struct GraphicTextComponent {
  std::string font;
  std::string text;
  glm::u8vec3 color = {255, 255, 255};          // 0-255
  glm::u8vec3 outline_color = {255, 255, 255};  // 0-255
  float outline_thickness = 0.f;                // values between 0-0.15 make sense.
  float thickness = 0.f;                        // values between 0-0.15 make sense.
};

};  // namespace gmp
UNBOUND_COMPONENT((gmp::GraphicTextComponent), font, text, color, outline_color, outline_thickness,
                  thickness)
#endif
