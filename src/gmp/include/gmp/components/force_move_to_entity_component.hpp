#ifndef FORCE_MOVE_TO_ENTITY_COMPONENT_HPP
#define FORCE_MOVE_TO_ENTITY_COMPONENT_HPP

#include <cor/cor.hpp>
#include <cor/entity_handle.hpp>
#include <ext/ext.hpp>

namespace gmp {
// a component to force an entity to move towards another entity
struct ForceMoveToEntityComponent {
  cor::EntityHandle target;
  float release_distance;
  float speed;
};
}  // namespace gmp
UNBOUND_COMPONENT((gmp::ForceMoveToEntityComponent), target, release_distance, speed)
#endif