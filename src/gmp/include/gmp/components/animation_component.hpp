#pragma once
#ifndef GMP_ANIMATION_COMPONENT_HPP
#define GMP_ANIMATION_COMPONENT_HPP

#include <bse/task_handle.hpp>
#include <cor/cor.hpp>
#include <cor/entity.hpp>
#include <ext/ext.hpp>
#include <gmp/animation/animation_event_player.hpp>
#include <gmp/animation/animation_state_machine.hpp>
#include <gmp/systems/animation_system.hpp>
#include <phy/collision_groups.hpp>

namespace gmp {
struct AIComponent;
struct PlayerComponent;
struct CharacterControllerComponent;
class PhysicsSystem;
class AnimationSystem;

// Entity wrapper for ConstEntity for exposing the ECS API to script.
// TODO MH: Temporary. Replace with proper, generic script entity later on
struct ScriptEntity {
  cor::ConstEntity entity;

  // Get char controller component.
  // Very much copy-pasted from AiScriptEntityWrapper. Very temporary.
  const CharacterControllerComponent* get_char_ctrl_component() const;

  const AIComponent* get_ai_component() const;
  const PlayerComponent* get_player_component() const;
};

// Enables sharing of animated collision data. This is needed for collision checks against animated
// meshes. This object does not own the memory pointed to be the pointers. This data does not need
// to be serialized
class AnimatedData {
 public:
  // Mutable because they need to be set in read_update of AnimationSystem. They are not modified
  // after creation
  mutable glm::vec3* positions = nullptr;
  mutable glm::vec3* normals = nullptr;
  mutable btBvhTriangleMeshShape* mesh_shape = nullptr;
  mutable bse::TaskHandle* task;
  mutable size_t num_vertices = 0;

  AnimationSystem::AnimatedMeshData* mesh_data = nullptr;
};

struct AnimationComponent {
  AnimationStateMachine state_machine;
  AnimationStateMachine attack_state_machine;
  AnimatedData data;
  AnimationEventPlayer animation_event_player;

  // If not set to NONE, only projectiles with these groups set will collide with this animated mesh
  phy::CollisionGroup::Group character_group = phy::CollisionGroup::NONE;
};

}  // namespace gmp

UNBOUND_COMPONENT((gmp::AnimationComponent), data, state_machine, animation_event_player,
                  attack_state_machine)

#endif