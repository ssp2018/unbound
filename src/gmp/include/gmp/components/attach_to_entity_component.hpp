#pragma once

#ifndef GMP_ATTACH_TO_ENTITY_COMPONENT_HPP
#define GMP_ATTACH_TO_ENTITY_COMPONENT_HPP

#include "cor/entity_handle.hpp"
#include <cor/cor.hpp>
#include <ext/ext.hpp>
#include <gmp/transform.hpp>

namespace gmp {
// Attaches the current entity's transform onto another entity
struct AttachToEntityComponent {
  cor::EntityHandle entity_handle;
  Transform offset_transform;
  // Contains frame index of the last frame the transform was updated
  uint16_t m_frame_updated = 0;
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::AttachToEntityComponent), entity_handle, offset_transform);

#endif  // GMP_ATTACH_TO_JOINT_COMPONENT_HPP
