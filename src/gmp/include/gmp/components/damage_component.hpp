#pragma once
#ifndef GMP_DAMAGE_COMPONENT_HPP
#define GMP_DAMAGE_COMPONENT_HPP

#include <cor/cor.hpp>
#include <ext/ext.hpp>

namespace gmp {
// component for an entity's damage output
struct DamageComponent {
  int damage_value = 1;
  float impact_damage_radius = 0;
  bool friendly_fire = false;
};
}  // namespace gmp
UNBOUND_COMPONENT((gmp::DamageComponent), damage_value, impact_damage_radius, friendly_fire)
#endif
