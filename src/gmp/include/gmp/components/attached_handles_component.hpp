#pragma once
#ifndef GMP_ATTACHED_HANDLES_COMPONENT_HPP
#define GMP_ATTACHED_HANDLES_COMPONENT_HPP

#include "cor/entity_handle.hpp"
#include <cor/cor.hpp>
#include <ext/ext.hpp>

namespace gmp {

// Component for pickups. Makes them rotate and "float"
struct AttachedHandlesComponent {
  std::array<cor::EntityHandle, 5> handles;
};

}  // namespace gmp
UNBOUND_COMPONENT((gmp::AttachedHandlesComponent), handles)

#endif  // GMP_ATTACHED_HANDLES_COMPONENT_HPP
