#pragma once
#ifndef PLAYER_DAMAGE_VISUAL_COMPONENT_HPP
#define PLAYER_DAMAGE_VISUAL_COMPONENT_HPP

#include <cor/cor.hpp>
#include <ext/ext.hpp>

namespace gmp {

// radial blur component to adjust the level of blur applied to the scene
struct PlayerDamageVisualComponent {
  glm::vec4 color;
};

};  // namespace gmp
UNBOUND_COMPONENT((gmp::PlayerDamageVisualComponent), color)
#endif
