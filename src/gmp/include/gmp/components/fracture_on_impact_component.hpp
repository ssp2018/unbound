#pragma once
#ifndef GMP_FRACTURE_ON_IMPACT_COMPONENT_HPP
#define GMP_FRACTURE_ON_IMPACT_COMPONENT_HPP

#include <bse/asset.hpp>
#include <ext/ext.hpp>
#include <gfx/texture.hpp>

namespace gmp {
struct FractureOnImpactComponent {
  // bool deal_damage = false;  // if true, entity requires DamageComponent
  float radius = 0;         // radius of explosion
  int fragment_amount = 0;  // how many fragments the object will split into on impact
  float fragment_size = 0;  // size of the fragments

  bse::Asset<gfx::Texture> texture = bse::Asset<gfx::Texture>();  // texture of the fragments
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::FractureOnImpactComponent), radius, fragment_amount, fragment_size, texture)
#endif
