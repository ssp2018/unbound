#pragma once
#ifndef GMP_PARTICLES_COMPONENT_HPP
#define GMP_PARTICLES_COMPONENT_HPP

#include <bse/asset.hpp>
#include <cor/cor.hpp>
#include <ext/ext.hpp>
#include <gfx/particle_system_manager.hpp>
#include <gfx/shader.hpp>
#include <gfx/texture.hpp>

namespace gmp {

// All data needed for representing a particle system
struct ParticlesComponent {
  // internal struct stored for every particle
  struct Particle {
    glm::vec3 velocity;
    float life = -1;
    float start_life = -1;
    float camera_distance2 = 0;
    glm::vec3 position;
    glm::vec4 start_color;
    glm::vec4 end_color;
    glm::vec4 color;
    float size;
    float start_size;
    float end_size;
    float rotation;
    float rotation_speed;
  };

  // dont touch these variables
  std::vector<float> positions;
  std::vector<Particle> particles;
  int num_particles = 0;
  int last_unused_particle = 0;
  // unsigned int vbo = 999999;
  // unsigned int vao = 999999;
  gfx::ParticleSystemHandle handle;
  float time_since_particle = 0;
  glm::vec3 last_pos = glm::vec3(0, 0, 0);

  enum class SpawnShape { BOX, SPHERE, WAVE };
  SpawnShape spawn_shape = SpawnShape::BOX;

  float spawn_per_second = 10;
  float life_time = 2;
  int max_particles = 10;
  glm::vec3 offset_from_transform_component = glm::vec3(0, 0, 0);

  float sphere_min_speed = 1;
  float sphere_max_speed = 1;

  float wave_length = 1;
  float wave_radius = 10;
  float wave_time = 0;
  float wave_pos = 0;

  bse::Asset<gfx::Texture> texture =
      bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/swirl_particle01.png"_fp);

  // random factors
  glm::vec3 min_spawn_pos = glm::vec3(0, 0, 0);
  glm::vec3 max_spawn_pos = glm::vec3(1, 1, 0);
  glm::vec4 min_start_color = glm::vec4(0, 0, 0, 1);
  glm::vec4 max_start_color = glm::vec4(1, 1, 0, 1);
  glm::vec4 min_end_color = glm::vec4(0, 0, 0, 1);
  glm::vec4 max_end_color = glm::vec4(1, 1, 0, 1);

  enum class Velocity { CONSTANT, DECELERATE };
  Velocity velocity_mode = Velocity::CONSTANT;
  glm::vec3 min_start_velocity = glm::vec3(0, 0, 0);
  glm::vec3 max_start_velocity = glm::vec3(0, 0, 1);

  enum class Size { CONSTANT, CHANGING };
  Size size_mode = Size::CONSTANT;
  float min_start_size = 0;
  float max_start_size = 1;
  float min_end_size = 0;
  float max_end_size = 1;

  float min_life = 1;
  float max_life = 2;

  float min_rotation_speed = 3.f;
  float max_rotation_speed = 5.f;

  // Set to true if you want your entity to be destroyed when the particle
  // system has stopped emitting particles.
  bool do_destroy_entity = false;

  // How much longer to spawn particles. -1 = forever
  float spawn_duration_left = -1;

  bse::Asset<gfx::Shader> shader = bse::Asset<gfx::Shader>("assets/shader/particle.frag");
};
};  // namespace gmp

// UNBOUND_TYPE((gmp::Particle), velocity, life, camera_distance2, position, start_color, end_color,
//             color, size)
UNBOUND_COMPONENT((gmp::ParticlesComponent), spawn_shape, spawn_per_second, life_time,
                  max_particles, offset_from_transform_component, sphere_min_speed,
                  sphere_max_speed, wave_length, wave_radius, texture, min_spawn_pos, max_spawn_pos,
                  min_start_color, max_start_color, min_end_color, max_end_color, velocity_mode,
                  min_start_velocity, max_start_velocity, size_mode, min_start_size, max_start_size,
                  min_end_size, max_end_size, min_life, max_life, min_rotation_speed,
                  max_rotation_speed, spawn_duration_left, shader)

#endif
