#pragma once
#ifndef GMP_RADIAL_COMPONENT_HPP
#define GMP_RADIAL_COMPONENT_HPP

#include <bse/asset.hpp>
#include <cor/cor.hpp>
#include <ext/ext.hpp>
#include <gfx/texture.hpp>

namespace gmp {

// radial blur component to adjust the level of blur applied to the scene
struct RadialComponent {
  float blur_radius;
};

};  // namespace gmp
UNBOUND_COMPONENT((gmp::RadialComponent), blur_radius)
#endif
