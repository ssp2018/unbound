#ifndef COLE_COMPONENT_HPP
#define COLE_COMPONENT_HPP

#include <cor/cor.hpp>
#include <cor/entity.hpp>
#include <cor/entity_handle.hpp>
#include <ext/ext.hpp>

namespace gmp {
// component linking COLE with the smaller boxes
struct COLEComponent {
  std::array<cor::EntityHandle, 56> body;
  std::array<glm::vec3, 10> teleport_points;
  cor::EntityHandle death_beam;
  glm::vec3 beam_rotation_axis;
  float angle;
  glm::vec3 beam_dir;
  int last_tele_armor_amount = 56;
  int armor_left = 56;
  bool woundable = false;
  float rotation_percentage = 0.01f;
  float laser_rate_of_fire = 5.f;
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::COLEComponent), body, teleport_points, death_beam, beam_rotation_axis,
                  angle, beam_dir, last_tele_armor_amount, armor_left, woundable,
                  rotation_percentage, laser_rate_of_fire)
#endif