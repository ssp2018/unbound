#pragma once

#ifndef GMP_STATIC_COMPONENT_HPP
#define GMP_STATIC_COMPONENT_HPP

#include <cor/cor.hpp>
#include <gmp/static_component_contents.hpp>

namespace gmp {

struct StaticComponent {
  StaticComponentContents contents;
};
}  // namespace gmp
UNBOUND_COMPONENT((gmp::StaticComponent), contents)

#endif  // GMP_STATIC_COMPONENT_HPP