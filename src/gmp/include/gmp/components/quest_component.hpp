#pragma once
#ifndef GMP_QUEST_COMPONENT_HPP
#define GMP_QUEST_COMPONENT_HPP

#include <cor/cor.hpp>
#include <ext/ext.hpp>

namespace gmp {
// component for a quest
struct QuestComponent {
  int progression = 0;
  int goal = 1;
};
}  // namespace gmp
UNBOUND_COMPONENT((gmp::QuestComponent), progression, goal)

#endif
