#pragma once
#ifndef GMP_AI_COMPONENT_HPP
#define GMP_AI_COMPONENT_HPP

#include "bse/reflect.hpp"  // for IMPL_REFLECT_21, IMPL_REFLECT_5
#include "cor/entity_handle.hpp"
#include <aud/sound_buffer.hpp>
#include <aud/sound_source.hpp>
#include <bse/asset.hpp>
#include <cor/cor.hpp>  // for UNBOUND_TYPE
#include <scr/dynamic_object.hpp>
#include <scr/dynamic_table.hpp>  // for DynamicTable
namespace gmp {
struct TransformComponent;
}
namespace gmp {
struct AIPathfinding {
  // if the entity is moving to a location
  bool m_on_the_move;

  // bool if AI is in need of pathfinding
  bool m_need_path;

  // unit's destination
  glm::vec3 m_destination;

  // path to destination
  std::vector<glm::vec3> m_path;

  // current index in the path
  int m_curr_index;

  CLASS_SERFUN((AIPathfinding)) {
    SERIALIZE(self.m_on_the_move);
    SERIALIZE(self.m_need_path);
    SERIALIZE(self.m_destination);
    SERIALIZE(self.m_path);
    SERIALIZE(self.m_curr_index);
  }
  CLASS_DESERFUN((AIPathfinding)) {
    DESERIALIZE(self.m_on_the_move);
    DESERIALIZE(self.m_need_path);
    DESERIALIZE(self.m_destination);
    DESERIALIZE(self.m_path);
    DESERIALIZE(self.m_curr_index);
  }
};

enum class AIType { MELEE_GRUNT, RANGED_GRUNT, FLYING_SKULL, GOLEM, COLE_BOSS };

struct MoraleData {
  MoraleData(AIType type);
  MoraleData(){};
  std::string group_name;
  int morale = 100;  // smth smth that sounds good
  int morale_boost;
  int morale_loss_per_damage;
  int morale_regeneration_val;
  int morale_flee_threshold;
  int morale_reenter_combat_threshold;

  int morale_loss_ally_die;
  int morale_loss_ally_hit;
  int morale_loss_ally_flee;
  int morale_boost_from_group;
  int morale_boost_dealing_damage;
};

// class containing functions used in AIComponent
class AIComponentFunctions {
 public:
  // tells system to call pathfinding algorithm through infuence map
  void go_to(float x, float y, float z);

  // is moving
  bool is_moving();

  // calculates the distance between two entities
  float get_distance(TransformComponent& t_c_1, TransformComponent& t_c_2);

  // checks if a position is in the same cell as another position
  float distance(glm::vec2 pos, glm::vec2 other);

  // testing DynamicObject
  // scr::DynamicObject distance_2(scr::DynamicObject lhs, scr::DynamicObject rhs);

  // holds pathfinding variables
  AIPathfinding m_path_finding;
};

// component for AI behaviour
struct AIComponent {
  scr::DynamicTable lua_state;  // temp solution, containing lua state variables
  std::string type;             // type ("grunt", "bigBoss" or smthn)
  std::string state_name;       // current state of the entity in the AI state machine
  std::string previous_state;   // previous state, before state transition
  float state_time;             // time the entity has been in this state (can be changed by state)
  float total_state_time;  // the total time the entity has been in this state (can not be changed
                           // by state)
  float time_since_update = -FLT_MAX;  // time since last script update
  bool hard_cut = false;

  bse::FilePath death_sound_path;
  aud::SoundSettings death_sound_settings;
  glm::vec3 search_location;  // OBS! should be moved to lua state variables?
  glm::vec3 spawn_point;      // central location of unit camp
  float exploration_radius;   // the radius where ai-entity will walk around in.
  MoraleData morale_data;
  int aggressiveness;  // unit willingness to attack
  int speed_base;      // the base movement speed for the ai
  int movement_speed;  // movement speed of the ai
  int actions_taken;   // nr of "actions" issued in the same state (use depending on state)
  std::string misc;    // variable holding varying information (use depending on state)
  int damage_taken_previous_frame;  // signals entity took damage previous frame
  int frame_hit_count = 0;
  int base_attack_damage;        // damage of a "normal" attack from the unit
  bool start_attack_this_frame;  // signals the start of the state's attack
  int started_attack_damage;     // damage of the started attack
  float aggro_range;             // range at which the ai notices other entities
  bool has_target;               // if the entity has an active target
  cor::EntityHandle target;      // target entity handle
  float cutoff_dist = -1;        // hard cutoff distance of unit, -1 means usage of default

  float disengagement_range;  // the entity is removed from the threat list if it is futher away

  // this should rly be moved to lua_table of skulls
  float time_since_fireball = 0;  // Time since last fire ball attack

  glm::ivec2 old_pos = {0, 0};
  glm::ivec2 new_pos = {1, 1};

  AIComponentFunctions funcs;  // contains functions to the AI component
};

}  // namespace gmp
UNBOUND_TYPE((gmp::AIPathfinding), m_on_the_move, m_need_path, m_destination, m_path, m_curr_index)
UNBOUND_TYPE((gmp::AIComponentFunctions), go_to, m_path_finding, is_moving, get_distance, distance)
UNBOUND_TYPE((gmp::MoraleData), group_name, morale, morale_boost, morale_loss_per_damage,
             morale_regeneration_val, morale_flee_threshold, morale_reenter_combat_threshold,
             morale_loss_ally_die, morale_loss_ally_hit, morale_loss_ally_flee,
             morale_boost_from_group, morale_boost_dealing_damage);
UNBOUND_COMPONENT((gmp::AIComponent), lua_state, type, state_name, previous_state, state_time,
                  total_state_time, time_since_update, hard_cut, spawn_point, exploration_radius,
                  morale_data, aggressiveness, speed_base, movement_speed, actions_taken, misc,
                  aggro_range, has_target, target, funcs, search_location,
                  damage_taken_previous_frame, base_attack_damage, start_attack_this_frame,
                  started_attack_damage, time_since_fireball, frame_hit_count, disengagement_range,
                  old_pos, new_pos, death_sound_path, death_sound_settings)

#endif
