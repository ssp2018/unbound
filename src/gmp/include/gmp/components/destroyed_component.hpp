#pragma once
#ifndef GMP_DESTROYED_COMPONENT_HPP
#define GMP_DESTROYED_COMPONENT_HPP
#include <cor/cor.hpp>

namespace gmp {

// Component for makring an entity to be destroyed
struct DestroyedComponent {};

}  // namespace gmp
UNBOUND_COMPONENT((gmp::DestroyedComponent));

#endif  // GMP_DESTROYED_COMPONENT_HPP
