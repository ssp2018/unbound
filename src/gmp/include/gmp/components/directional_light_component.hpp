#pragma once
#ifndef GMP_DIRECTIONAL_LIGHT_COMPONENT_HPP
#define GMP_DIRECTIONAL_LIGHT_COMPONENT_HPP
#include <cor/cor.hpp>
#include <ext/ext.hpp>

namespace gmp {

// Directional light component for light data
struct DirectionalLightComponent {
  glm::vec3 direction;
  glm::vec3 color;
  bool has_shadow;
  bool is_cascade;
};

};  // namespace gmp

UNBOUND_COMPONENT((gmp::DirectionalLightComponent), direction, color, has_shadow, is_cascade)

#endif
