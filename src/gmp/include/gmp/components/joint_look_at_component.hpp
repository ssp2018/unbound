#pragma once
#ifndef GMP_JOINT_LOOK_AT_COMPONENT_HPP
#define GMP_JOINT_LOOK_AT_COMPONENT_HPP

#include <cor/cor.hpp>
#include <cor/entity_handle.hpp>
#include <ext/ext.hpp>

namespace gmp {
// Component used to make joints look at entities
struct JointLookAtComponent {
  std::string m_joint_name;               // Joint to affect
  cor::EntityHandle m_entity_to_look_at;  // Target
};

}  // namespace gmp

UNBOUND_COMPONENT((gmp::JointLookAtComponent), m_joint_name, m_entity_to_look_at)

#endif