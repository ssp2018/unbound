#pragma once

#ifndef GMP_ATTACH_TO_TRIANGLE_COMPONENT
#define GMP_ATTACH_TO_TRIANGLE_COMPONENT

#include <bse/reflect.hpp>
#include <cor/entity_handle.hpp>

namespace gmp {

// Component used to attach an object to a triangle of an animated mesh
struct AttachToTriangleComponent {
  // The entity with the AttachToTriangleComponent will follow a triangle on the entity pointed to
  // by 'attached_to'
  cor::EntityHandle attached_to;

  // The index of the triangle to attach to
  int triangle_index = 0;

  // Rotation from triangle normal to projectile direction
  glm::quat rotation;

  // Projectile position on the plane of the triangle
  glm::vec3 barycentric;

  // Projectile's distance from triangle's plane
  float distance_from_plane;
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::AttachToTriangleComponent), attached_to, triangle_index, rotation,
                  barycentric, distance_from_plane)

#endif  // GMP_ATTACH_TO_TRIANGLE_COMPONENT