#pragma once
#ifndef GMP_PICKUP_COMPONENT_HPP
#define GMP_PICKUP_COMPONENT_HPP

#include <cor/cor.hpp>
#include <ext/ext.hpp>

namespace gmp {

// Component for pickups. Makes them rotate and "float"
struct PickupComponent {
  float time_to_respawn = 0.f;  // Time until spawning. Set to -1 if it is a "custom" pickup
  float respawn_cooldown =
      30.f;  // How long after being picked up until it should respawn. -1=do not respawn
  float player_range = 250.f;  // Minimum range to player before respawning
  enum Type { HEALTH_ORB, EXPLOSIVE_ARROWS, NOTE, HORN };
  int type;              // What type of pickup it is. Can be ignored if time_to_respawn is -1
  glm::vec3 position;    // Position used for respawning (so should be the same as sent to
                         // TransformComponent)
  float vertical_value;  // Used internally to make the model "float". No reason to set manually
};

}  // namespace gmp
UNBOUND_COMPONENT((gmp::PickupComponent), time_to_respawn, respawn_cooldown, player_range, type,
                  position, vertical_value)

#endif
