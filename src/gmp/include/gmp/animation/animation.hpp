#pragma once
#ifndef GMP_ANIMATION_HPP
#define GMP_ANIMATION_HPP

namespace gmp {

// Holds the state of an animation
struct Animation {
  std::string name = "";
  uint32_t key_frame_index = 0;
  float time = 0.f;
};

}  // namespace gmp

#endif
