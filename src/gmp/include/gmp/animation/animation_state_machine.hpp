#pragma once

#include <bse/serialize.hpp>
#include <cor/cor.hpp>
#include <cor/entity_handle.hpp>
#include <cor/entity_manager.hpp>
#include <ext/ext.hpp>
#include <gmp/animation/animation_base_tree_node.hpp>
#include <gmp/animation/animation_node.hpp>
#include <gmp/animation/animation_state.hpp>
#include <gmp/animation/animation_state_machine_descriptions.hpp>
#include <gmp/animation/blend_node.hpp>

namespace gmp {

// A state machine handling switches between animations
class AnimationStateMachine {
 public:
  AnimationStateMachine();
  AnimationStateMachine(std::string machine_name, std::string start_state);
  AnimationStateMachine(const AnimationStateMachine& other);
  AnimationStateMachine(AnimationStateMachine&& other);
  ~AnimationStateMachine();

  AnimationStateMachine& operator=(const AnimationStateMachine& other);
  AnimationStateMachine& operator=(AnimationStateMachine&& other);

  // Set the active state machine description (defined in
  // 'animation_state_machine_descriptions.cpp') to the one specified by 'machine_name', in initial
  // state 'start_state'
  void set_machine_and_state(const std::string& machine_name, const std::string& start_state);

  // Set the active state machine description (defined in
  // 'animation_state_machine_descriptions.cpp') to the one specified by 'machine_name'
  void set_machine(const std::string& machine_name);

  // Updates tree based on an entity handle
  void update(cor::EntityHandle entity_handle, const cor::EntityManager& entity_manager,
              float dt) const;

  // Returns current machine name
  std::string get_machine() const;

  // Returns name of current state
  std::string get_state() const;

  // Returns index of start joint for current state
  int get_start_joint() const;

  // Return animation notify events
  std::vector<AnimationNotifyEvent> get_notify_events();

  const AnimationBaseTreeNode* get_tree() const;

  CLASS_SERFUN((AnimationStateMachine)) {
    SERIALIZE(self.m_machine_name);
    SERIALIZE(self.m_current_state);

    bool has_tree = (bool)m_tree_root;
    SERIALIZE(has_tree);

    if (has_tree) {
      SERIALIZE(*self.m_tree_root);
    }
  }
  CLASS_DESERFUN((AnimationStateMachine)) {
    DESERIALIZE(self.m_machine_name);
    DESERIALIZE(self.m_current_state);

    set_machine_deserialize(m_machine_name);

    bool has_tree;
    ::bse::deserialize(has_tree, data, offset);

    if (has_tree) {
      AnimationBaseTreeNode::NodeType type;
      ::bse::deserialize(type, data, offset);

      if (type == AnimationBaseTreeNode::NodeType::BLEND) {
        BlendNode* blend = new BlendNode();
        self.m_tree_root.reset(blend);
      } else if (type == AnimationBaseTreeNode::NodeType::ANIM) {
        AnimationNode* anim = new AnimationNode();
        self.m_tree_root.reset(anim);
      }

      DESERIALIZE(*self.m_tree_root);

      self.m_tree_root->restore_state(m_description);
    }
  }

 private:
  // Set the active state machine description (defined in
  // 'animation_state_machine_descriptions.cpp') to the one specified by 'machine_name' without
  // updating state
  // Used on deserialization
  void set_machine_deserialize(const std::string& machine_name);

  // Finds a state based on name, and returns it index in m_states, or -1 if not found
  int find_state(const std::string& state_name) const;

  void set_state(const std::string& state);

  // Move other into this
  void move_from(AnimationStateMachine&& other);

  // Copy from other into this
  void copy_from(const AnimationStateMachine& other);

  // Root of the animation tree. Mutable because it needs to be updated from read_update
  mutable std::unique_ptr<AnimationBaseTreeNode> m_tree_root;

  // Name of an AnimationStateMachineDescription in 'animation_state_machine_descriptions.cpp'
  std::string m_machine_name;

  // Pointer to vector of animation states
  const AnimationStateMachineDescription* m_description;

  // Index of current state in 'm_states'. Mutable because it needs to be updated from read_update
  mutable int m_current_state = 0;

  mutable std::vector<AnimationNotifyEvent> m_animation_notify_events;

  mutable float m_time_in_current_state;
};
}  // namespace gmp

UNBOUND_TYPE((gmp::AnimationStateMachine), set_machine_and_state, get_machine, get_state)