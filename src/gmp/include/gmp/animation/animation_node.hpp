#pragma once

#include "bse/mesh/animation_clip.hpp"
#include "gmp/animation/animation_base_tree_node.hpp"
#include <bse/mesh/animation_clip.hpp>
#include <bse/mesh/skeleton.hpp>
#include <ext/ext.hpp>

namespace gmp {

// Node containing a single animation to play
class AnimationNode : public AnimationBaseTreeNode {
 public:
  AnimationNode();
  virtual ~AnimationNode();

  void set_animation(const bse::AnimationClip* animation, const bse::Skeleton* skeleton,
                     std::string state_name);
  const bse::AnimationClip* get_animation() const;

  // Set the weight that the state machine should have during this animation
  void set_machine_weight(float weight);

  // Set whether the animation will loop
  void set_looping(bool loop);

  // Updates animation time. Returns a pointer to a node that should replace this node. If nothing
  // is returned, the node should stay
  virtual std::optional<std::unique_ptr<AnimationBaseTreeNode>> update(
      float dt, std::vector<AnimationNotifyEvent>& animation_notify_events) override;

  // Return SQT palette at current animation time
  virtual std::vector<bse::SQT>& get_sqt_palette() const override;

  // Returns the weight that the entire tree should have this frame
  virtual float get_machine_weight() const override;

  // Get current animation time
  float get_time() const;

  // Prints the tree structure to cout
  virtual void debug_print(int level = 0) override;

  // Returns a copy of the node
  virtual std::unique_ptr<AnimationBaseTreeNode> copy() const override;

  // Restores tree state based on input parameters
  virtual void restore_state(const AnimationStateMachineDescription* desc) override;

  float get_loop_count() const;

  VIRTUAL_CLASS_SERFUN((AnimationNode)) {
    AnimationBaseTreeNode::NodeType type = AnimationBaseTreeNode::NodeType::ANIM;
    SERIALIZE(type);
    SERIALIZE(self.m_state_name);
    SERIALIZE(self.m_notify_data);
    SERIALIZE(self.m_time);
    SERIALIZE(self.m_weight);
    SERIALIZE(self.m_key_frame_index);
    SERIALIZE(self.m_loop_count);
    SERIALIZE(self.m_result_SQT);
    SERIALIZE(self.m_loop);
    SERIALIZE(self.m_weight);
  }
  VIRTUAL_CLASS_DESERFUN((AnimationNode)) {
    DESERIALIZE(self.m_state_name);
    DESERIALIZE(self.m_notify_data);
    DESERIALIZE(self.m_time);
    DESERIALIZE(self.m_weight);
    DESERIALIZE(self.m_key_frame_index);
    DESERIALIZE(self.m_loop_count);
    DESERIALIZE(self.m_result_SQT);
    DESERIALIZE(self.m_loop);
    DESERIALIZE(self.m_weight);
  }

 private:
  // Struct containing current notify data
  struct NotifyData {
    int previous_frame = 0;
    int current_frame = 0;
    int next_notify = 0;
  };

  // Calculate the ratio used for interpolating between frames
  float calculate_interpolation_ratio(float current_time, float previous_time, float next_time);

  // Name of current state
  std::string m_state_name;

  NotifyData m_notify_data;
  const bse::AnimationClip* m_animation;
  const bse::Skeleton* m_skeleton;
  float m_time = 0.0f;
  float m_weight = 1.0f;
  int m_key_frame_index = 0;
  int m_loop_count = 0;
  mutable std::vector<bse::SQT> m_result_SQT;
  bool m_loop = true;
};

}  // namespace gmp