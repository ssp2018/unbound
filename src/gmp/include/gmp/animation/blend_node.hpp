#pragma once

#include "gmp/animation/animation_base_tree_node.hpp"
#include <ext/ext.hpp>
#include <gmp/animation/animation_node.hpp>

namespace gmp {
// Node capable of blending two animations
class BlendNode : public AnimationBaseTreeNode {
 public:
  BlendNode();
  virtual ~BlendNode();

  void set_children(std::unique_ptr<AnimationBaseTreeNode> &first_animation,
                    std::unique_ptr<AnimationBaseTreeNode> &second_animation);
  void set_blend_time(float blend_time);
  const std::pair<AnimationBaseTreeNode *, AnimationBaseTreeNode *> get_children();

  // Updates animation time
  virtual std::optional<std::unique_ptr<AnimationBaseTreeNode>> update(
      float dt, std::vector<AnimationNotifyEvent> &animation_notify_events) override;

  // Return SQT palette at current animation time
  virtual std::vector<bse::SQT> &get_sqt_palette() const override;

  // Returns the weight that the entire tree should have this frame
  virtual float get_machine_weight() const override;

  // Prints the tree structure to cout
  virtual void debug_print(int level = 0) override;

  // Returns a copy of the node
  virtual std::unique_ptr<AnimationBaseTreeNode> copy() const override;

  // Restores tree state based on input parameters
  virtual void restore_state(const AnimationStateMachineDescription *desc) override;

  VIRTUAL_CLASS_SERFUN((BlendNode)) {
    AnimationBaseTreeNode::NodeType type = AnimationBaseTreeNode::NodeType::BLEND;
    SERIALIZE(type);
    SERIALIZE(self.m_weight);
    SERIALIZE(self.m_blend_time);
    SERIALIZE(*self.m_children.first);
    SERIALIZE(*self.m_children.second);
  }
  VIRTUAL_CLASS_DESERFUN((BlendNode)) {
    DESERIALIZE(self.m_weight);
    DESERIALIZE(self.m_blend_time);

    AnimationBaseTreeNode::NodeType type;
    ::bse::deserialize(type, data, offset);

    if (type == AnimationBaseTreeNode::NodeType::BLEND) {
      BlendNode *blend = new BlendNode();
      m_children.first.reset(blend);
    } else if (type == AnimationBaseTreeNode::NodeType::ANIM) {
      AnimationNode *anim = new AnimationNode();
      m_children.first.reset(anim);
    } else {
      ASSERT(false) << "Error when deserializing BlendNode\n";
    }
    DESERIALIZE(*(self.m_children.first));

    ::bse::deserialize(type, data, offset);

    if (type == AnimationBaseTreeNode::NodeType::BLEND) {
      BlendNode *blend = new BlendNode();
      self.m_children.second.reset(blend);
    } else if (type == AnimationBaseTreeNode::NodeType::ANIM) {
      AnimationNode *anim = new AnimationNode();
      self.m_children.second.reset(anim);
    } else {
      ASSERT(false) << "Error when deserializing BlendNode\n";
    }
    DESERIALIZE(*(self.m_children.second));
  }

 private:
  // Blend the two animations together and return the result
  std::vector<bse::SQT> &blend_animations();

  float m_weight = 0.0f;
  float m_blend_time = 1.0f;
  std::pair<std::unique_ptr<AnimationBaseTreeNode>, std::unique_ptr<AnimationBaseTreeNode>>
      m_children;
};
}  // namespace gmp
