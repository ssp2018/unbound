#pragma once

#include <ext/ext.hpp>

namespace gmp {
struct SkinningTask : public bse::Task {
  struct AnimatedDataPointers {
    glm::vec3* positions;
    glm::vec3* normals;
  };

  SkinningTask(const glm::vec3* input_pos, const glm::vec3* input_nor, size_t start_index,
               size_t end_index, AnimationSystem::AnimatedMeshData* mesh)
      : input_pos{input_pos}, input_nor{input_nor}, start{start_index}, end{end_index}, mesh{mesh} {
    output_pos = (glm::vec3*)&mesh->positions[0];
    output_nor = (glm::vec3*)&mesh->normals[0];
  }

  const glm::vec3* input_pos;
  const glm::vec3* input_nor;
  glm::vec3* output_pos;
  glm::vec3* output_nor;

  // Start/end indices
  size_t start;
  size_t end;

  AnimationSystem::AnimatedMeshData* mesh;

  void kernel() override {
    // Save and use pointers here instead of operator[], the range check makes the code insanely
    // slow in debug
    const glm::mat4* joint_transforms = mesh->joint_transforms.data();
    const int* input_indices = mesh->parent_mesh_data->mesh.joint_indices.data();
    const float* input_weights = mesh->parent_mesh_data->mesh.joint_weights.data();

    for (size_t i = start; i < end; i++) {
      ///////////////////
      // GIMME DAT AVX //
      ///////////////////

      // Should technically multiply the normal vector by the transpose of the inverse of the
      // joint transform, but since we're not using (non-uniform) scaling it's fine

      // Grab matrices and weights
      glm::mat4 matrices[4]{
          joint_transforms[input_indices[i * 4 + 0]], joint_transforms[input_indices[i * 4 + 1]],
          joint_transforms[input_indices[i * 4 + 2]], joint_transforms[input_indices[i * 4 + 3]]};
      float weights[4]{input_weights[i * 4 + 0], input_weights[i * 4 + 1], input_weights[i * 4 + 2],
                       input_weights[i * 4 + 3]};

      // We want the combined matrix stored in two YMM registers, the first two rows in the first,
      // and the last two rows in the second
      __m256 matrix_row_0_1 = _mm256_set_ps(0, 0, 0, 0, 0, 0, 0, 0);  // Initialize to zero
      __m256 matrix_row_2_3 = matrix_row_0_1;

      // Weight of current matrix
      __m256 weight;

      // Linearly interpolate between matrices
      for (int i = 0; i < 4; i++) {
        // Store weight of matrix in all lanes
        weight = _mm256_broadcast_ss(&weights[i]);

        // Load matrix into two temporary registers
        __m256 temp_0_1 = _mm256_loadu_ps((float*)&matrices[i]);
        __m256 temp_2_3 = _mm256_loadu_ps((float*)&matrices[i] + 8);

        // Multiply matrix with weight
        temp_0_1 = _mm256_mul_ps(temp_0_1, weight);
        temp_2_3 = _mm256_mul_ps(temp_2_3, weight);

        // Add weighted matrix to final result matrix
        matrix_row_0_1 = _mm256_add_ps(matrix_row_0_1, temp_0_1);
        matrix_row_2_3 = _mm256_add_ps(matrix_row_2_3, temp_2_3);
      }

      // Spread each vector component into 4 SIMD positions
      // Note: _mm256_set_ps() is "reversed", the first argument will be at the highest memory
      // location
      // _mm256_set_ps(0, 1, 2, 3, 4, 5, 6, 7) results in this layout
      // in memory: 7, 6, 5, 4, 3, 2, 1, 0
      __m256 xy_pos = _mm256_set_ps(input_pos[i].y, input_pos[i].y, input_pos[i].y, input_pos[i].y,
                                    input_pos[i].x, input_pos[i].x, input_pos[i].x, input_pos[i].x);
      __m256 zw_pos = _mm256_set_ps(1.0f, 1.0f, 1.0f, 1.0f, input_pos[i].z, input_pos[i].z,
                                    input_pos[i].z, input_pos[i].z);

      __m256 xy_nor = _mm256_set_ps(input_nor[i].y, input_nor[i].y, input_nor[i].y, input_nor[i].y,
                                    input_nor[i].x, input_nor[i].x, input_nor[i].x, input_nor[i].x);
      __m256 zw_nor = _mm256_set_ps(0.0f, 0.0f, 0.0f, 0.0f, input_nor[i].z, input_nor[i].z,
                                    input_nor[i].z, input_nor[i].z);

      // Multiply the x, y, z, w components by a matrix row, then add x + z and y + w
      __m256 temp_pos = _mm256_add_ps(_mm256_mul_ps(xy_pos, matrix_row_0_1),
                                      _mm256_mul_ps(zw_pos, matrix_row_2_3));
      __m256 temp_nor = _mm256_add_ps(_mm256_mul_ps(xy_nor, matrix_row_0_1),
                                      _mm256_mul_ps(zw_nor, matrix_row_2_3));

      // Will store the result of both matrix multiplications; the position in the first 4 slots,
      // and the normal in the last 4
      __m256 result;

      const int xy_mask = 32;  // Mask for first permutation
      const int zw_mask = 49;  // Mask for second permutation

      // Now we need to add (x + y) to (z + w) to get the final vector
      // These two variables will hold temp_pos in the first 4 elements, and temp_nor in the last
      // 4 elements
      __m256 xy_pos_xy_nor = _mm256_permute2f128_ps(temp_pos, temp_nor, xy_mask);
      __m256 zw_pos_zw_nor = _mm256_permute2f128_ps(temp_pos, temp_nor, zw_mask);

      result = _mm256_add_ps(xy_pos_xy_nor, zw_pos_zw_nor);

      // Results in float form
      float res[8];
      _mm256_storeu_ps(res, result);

      // Store in output
      output_pos[i].x = res[0];
      output_pos[i].y = res[1];
      output_pos[i].z = res[2];

      output_nor[i].x = res[4];
      output_nor[i].y = res[5];
      output_nor[i].z = res[6];
    }
  }
};
}  // namespace gmp
