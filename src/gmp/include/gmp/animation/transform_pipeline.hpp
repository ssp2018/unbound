#pragma once

#ifndef GMP_TRANSFORM_PIPELINE_HPP
#define GMP_TRANSFORM_PIPELINE_HPP

namespace gmp {

// A transform tree for doing transformation calculations and blending for animations.
// TODO MH: Can be simplified by flattening? Depends on how blending math works.
struct TransformPipeline {
  // One node in the tree holding the actual data.
  struct Node {
    std::string animation;
    float weight;
    float time;
    std::vector<uint16_t> children;
  };

  uint16_t root_node;
  std::vector<Node> nodes;
};

}  // namespace gmp

#endif