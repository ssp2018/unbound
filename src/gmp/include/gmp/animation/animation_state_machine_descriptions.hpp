#pragma once

#include <bse/mesh/skeleton.hpp>
#include <gmp/animation/animation_state.hpp>

namespace gmp {
struct AnimationStateMachineDescription {
  AnimationStateMachineDescription(std::string description_name, const bse::Skeleton* skeleton,
                                   std::vector<AnimationState> states)
      : description_name(description_name), skeleton(skeleton), states(states) {}

  std::string description_name;
  const bse::Skeleton* skeleton;
  std::vector<AnimationState> states;
};

const AnimationStateMachineDescription* get_animation_state_machine_description(
    const std::string& description_name);
}  // namespace gmp