#pragma once

#include <bse/serialize.hpp>
#include <cor/event.hpp>
#include <ext/ext.hpp>
#include <gmp/animation/animation_state_machine_descriptions.hpp>

namespace gmp {
// Basic node tree class used as a base for other animation nodes
class AnimationBaseTreeNode {
  struct AnimationData {
    std::vector<glm::quat> m_quaternion_transforms;
    std::vector<char> m_blend_mask;
  };

 public:
  enum NodeType { ANIM, BLEND };

  AnimationBaseTreeNode();
  virtual ~AnimationBaseTreeNode();

  // Updates animation time. Returns a pointer to a node that should replace this node. If nothing
  // is returned, the node should stay. It also updates a shared notify vector used for triggering
  // events in other parts of the code.
  virtual std::optional<std::unique_ptr<AnimationBaseTreeNode>> update(
      float dt, std::vector<AnimationNotifyEvent> &animation_notify_events) = 0;

  // Return blended QT palette of this node and blend mask
  virtual std::vector<bse::SQT> &get_sqt_palette() const = 0;

  // Returns the weight that the entire tree should have this frame
  virtual float get_machine_weight() const = 0;

  // Prints the tree structure to cout
  virtual void debug_print(int level = 0) = 0;

  // Returns a copy of the node
  virtual std::unique_ptr<AnimationBaseTreeNode> copy() const = 0;

  // Restores tree state based on input parameters
  virtual void restore_state(const AnimationStateMachineDescription *desc) = 0;

  ABSTRACT_VIRTUAL_CLASS_SERFUN((AnimationBaseTreeNode)) {}
  ABSTRACT_VIRTUAL_CLASS_DESERFUN((AnimationBaseTreeNode)) {}

 private:
};

}  // namespace gmp

namespace bse {
inline void serialize(const gmp::AnimationBaseTreeNode::NodeType &value, SerializedData &data) {
  //
  memcpy(&data.bytes[data.head], &value, sizeof(gmp::AnimationBaseTreeNode::NodeType));
  data.head += sizeof(gmp::AnimationBaseTreeNode::NodeType);
  return;
}

inline void deserialize(gmp::AnimationBaseTreeNode::NodeType &value, const SerializedData &data,
                        int &offset) {
  //
  memcpy(&value, &data.bytes[offset], sizeof(gmp::AnimationBaseTreeNode::NodeType));
  offset += sizeof(gmp::AnimationBaseTreeNode::NodeType);
}
}  // namespace bse