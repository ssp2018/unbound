#pragma once

#include <bse/mesh/animation_clip.hpp>
#include <cor/entity_handle.hpp>
#include <cor/entity_manager.hpp>
#include <ext/ext.hpp>

namespace gmp {

// Struct used to define inter-state transition behavior
struct AnimationTransition {
  // Name of state to transition to
  std::string new_state_name;

  float blend_time = 1.0f;
};

// Defines a state that together with other instances form an animation state machine
class AnimationState {
 public:
  AnimationState();
  AnimationState(
      std::string state_name, const bse::AnimationClip* animation,
      std::function<std::optional<AnimationTransition>(
          cor::EntityHandle, const cor::EntityManager& entity_manager, float time_in_current_state)>
          animation_transition)
      : m_state_name(state_name),
        m_animation(animation),
        m_machine_weight(1.0f),
        m_animation_transition(animation_transition) {}
  AnimationState(
      std::string state_name, const bse::AnimationClip* animation, float machine_weight,
      bool loop_animation,
      std::function<std::optional<AnimationTransition>(
          cor::EntityHandle, const cor::EntityManager& entity_manager, float time_in_current_state)>
          animation_transition)
      : m_state_name(state_name),
        m_animation(animation),
        m_machine_weight(machine_weight),
        m_animation_transition(animation_transition),
        m_loop(loop_animation) {}
  ~AnimationState();

  // Function which is defined on a per instance basis which determines what triggers a state
  // transition
  std::function<std::optional<AnimationTransition>(
      cor::EntityHandle, const cor::EntityManager& entity_manager, float time_in_current_state)>
      m_animation_transition;

  // Name of this state
  std::string m_state_name;

  const bse::AnimationClip* m_animation;
  float m_machine_weight = 1.0f;  // Weight that the state machine should use during this state
  bool m_loop = true;

 private:
};

}  // namespace gmp