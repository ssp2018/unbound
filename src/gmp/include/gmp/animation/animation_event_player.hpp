#pragma once

#include "bse/asset.hpp"
#include "bse/serialize.hpp"
#include "gfx/model.hpp"
#include "gmp/animation/animation_base_tree_node.hpp"
#include "gmp/animation/animation_node.hpp"
namespace bse {
class Skeleton;
class AnimationClip;
struct SQT;
}  // namespace bse

namespace cor {
class EntityHandle;
}

namespace gmp {
class AnimationNode;

// Plays a single animation once based on an animation event
class AnimationEventPlayer {
 public:
  AnimationEventPlayer();
  AnimationEventPlayer(const AnimationEventPlayer& other);             // Copy constructor
  AnimationEventPlayer& operator=(const AnimationEventPlayer& other);  // Copy operator
  ~AnimationEventPlayer();
  // Updates current animation and returns the current frame's SQTs
  std::optional<std::vector<bse::SQT>> update(float dt,
                                              std::vector<AnimationNotifyEvent>& animation_notifies,
                                              cor::EntityHandle handle) const;

  // Play new animation
  void play_animation(bse::Asset<gfx::Model> model, const std::string& animation_name,
                      float blend_time) const;

  // Returns the weight this animation should have when blending with other animations
  float get_current_blend_factor() const;

  const AnimationNode* get_animation_node() const;

  CLASS_SERFUN((AnimationEventPlayer)) {
    SERIALIZE(self.m_animation_node == nullptr);
    if (self.m_animation_node != nullptr) {
      SERIALIZE(*self.m_animation_node);
      SERIALIZE(self.m_model);
      SERIALIZE(self.m_animation_name);
    }
    SERIALIZE(self.m_key_frame_index);
    SERIALIZE(self.m_blend_time);
  }
  CLASS_DESERFUN((AnimationEventPlayer)) {
    bool is_empty;
    DESERIALIZE(is_empty);

    if (!is_empty) {
      AnimationBaseTreeNode::NodeType type;
      ::bse::deserialize(type, data, offset);
      ASSERT(type == AnimationBaseTreeNode::NodeType::ANIM);
      m_animation_node = std::make_unique<AnimationNode>();

      DESERIALIZE(*self.m_animation_node);
      DESERIALIZE(self.m_model);
      DESERIALIZE(self.m_animation_name);
      if (self.m_model.is_valid() && self.m_animation_name != "") {
        self.m_animation_node->set_animation(
            &m_model->get_mesh()->get_skeleton().get_animations_map().at(m_animation_name),
            &m_model->get_mesh()->get_skeleton(), "");
      } else {
        self.m_animation_node->set_animation(nullptr, &m_model->get_mesh()->get_skeleton(), "");
      }
    }
    DESERIALIZE(self.m_key_frame_index);
    DESERIALIZE(self.m_blend_time);
  }

 private:
  // Node representing the current animation that will be played
  mutable std::unique_ptr<AnimationNode> m_animation_node;

  mutable bse::Asset<gfx::Model> m_model;
  mutable std::string m_animation_name;

  // The time during animation start and end which the event animation should blend with other
  // animations
  mutable float m_blend_time = 0.0f;

  int m_key_frame_index = 0;
};
}  // namespace gmp