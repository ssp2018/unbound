#pragma once
#ifndef AUDIO_SCRIPT_WRAPPER_HPP
#define AUDIO_SCRIPT_WRAPPER_HPP

#include <bse/asset.hpp>
#include <bse/log.hpp>
#include <cor/cor.hpp>
#include <cor/entity.hpp>
#include <cor/entity_handle.hpp>
#include <cor/entity_manager.hpp>
#include <cor/key.hpp>
#include <gmp/components/audio_component.hpp>
#include <gmp/systems/audio_system.hpp>

namespace gmp {
class AudioScriptWrapper {
 public:
  AudioScriptWrapper();
  ~AudioScriptWrapper();

  //
  void attach_audio_component(cor::Entity& entity);
  void remove_audio_component(cor::Entity& entity);

  // sets a collision sound effect to one of the material sounds
  void set_collision_sound(std::string asset_path, cor::Entity& entity, int type,
                           aud::SoundSettings settings);

  // set how often a sound in the periodic sound list should play
  void set_intervall_time_limit(cor::Entity& entity, std::string group, float val);

  // set a periodic sound group to start play its sounds
  void set_looping_sound_group_enabled(cor::Entity& entity, std::string group, bool val);

  // adds a new periodic sound group
  void add_periodic_sound_group(cor::Entity& entity, std::string group);

  //// Retrieves a random sound in the sound group
  // std::string get_random_sound_from_group(std::string group);

  //// Plays a sound from a sound group after a given period of time
  // void play_delayed_sound_from_group(cor::EntityHandle handle, std::string group, std::string
  // name,
  //                                   float delay_time);

 private:
  bool check_audio_component(cor::Entity& entity);
};
}  // namespace gmp

UNBOUND_TYPE(
    (gmp::AudioScriptWrapper), attach_audio_component, remove_audio_component, set_collision_sound,
    set_intervall_time_limit, set_looping_sound_group_enabled,
    add_periodic_sound_group /*, get_random_sound_from_group, play_delayed_sound_from_group*/)
#endif
