#pragma once

#ifndef GMP_PAUSE_STATE_HPP
#define GMP_PAUSE_STATE_HPP

#include <cor/state.hpp>

namespace gmp {

// State that pauses execution on any underlying state
class UnboundPauseState : public cor::State {
 public:
  UnboundPauseState(cor::Game& game, gfx::Window& window, aud::AudioManager& audio_mgr);
  virtual ~UnboundPauseState();

  void initialize() override;

  bool update(float dt) override;
  bool display() override;

 private:
  cor::EventManager::ListenID m_unpause_listener;
};
}  // namespace gmp
#endif  // GMP_PAUSE_STATE_HPP
