#pragma once
#ifndef AI_SCRIPT_ENTITY_WRAPPER_HPP
#define AI_SCRIPT_ENTITY_WRAPPER_HPP

#include <aud/sound_buffer.hpp>
#include <cor/cor.hpp>
#include <cor/entity.hpp>
#include <cor/entity_handle.hpp>
#include <cor/event.hpp>
#include <gfx/model.hpp>
#include <gfx/shader.hpp>
#include <gfx/texture.hpp>

namespace bse {
template <typename T>
class Asset;
class FilePath;
}  // namespace bse

namespace gmp {
struct AudioComponent;
struct AIComponent;
struct CharacterControllerComponent;
struct HealthComponent;
struct TransformComponent;
}  // namespace gmp

namespace cor {
class Entity;
struct FrameContext;
class EntityManager;
class EventManager;
}  // namespace cor

namespace phy {
class CollisionShape;
}

namespace gmp {

// struct conataining basic ai entity info
struct EntityInfo {
  float pos_x, pos_y, pos_z;
  int health;
  float health_percent;
};

struct TimeInfo {
  float dt;
  float speed_factor;
  float unslowed_factor;
};

// a class that wraps around an entity and lets us extract components we need in lua ai scripts
class AIScriptEntityWrapper {
 public:
  AIScriptEntityWrapper();
  ~AIScriptEntityWrapper();

  // Lua getter functions
  std::variant<cor::Entity *, cor::ConstEntity *> get_entity();
  // gmp::AIComponent *get_ai_component();
  std::variant<cor::FrameContext *, const cor::FrameContext *>
  get_frame_context();  // OBS! Only use as passthrough to functions
  cor::EntityManager *get_entity_manager();
  cor::EventManager *get_event_manager();
  gmp::TimeInfo get_time_info();
  bse::FilePath get_asset_path(std::string path = "");
  bse::Asset<gfx::Model> get_model_asset(std::string path = "");
  bse::Asset<gfx::Texture> get_texture_asset(std::string path = "");
  bse::Asset<aud::SoundBuffer> get_sound_buffer_asset(std::string path = "");
  bse::Asset<gfx::Shader> get_shader_asset(std::string path = "");

  // get shared pointer to collision shape
  std::shared_ptr<phy::CollisionShape> get_box_shared_pointer(float x, float y, float z);
  // std::shared_ptr<phy::SphereCollisionShape> get_sphere_shared_pointer(float radius);
  std::shared_ptr<phy::CollisionShape> get_capsule_shared_pointer(float x, float y);
  // std::shared_ptr<phy::CylinderCollisionShape> get_cylinder_shared_pointer(glm::vec3 dimensions);

  // identifies if there is a noise of interest in the vicinity
  bool listen_to_surroundings();

  AIScriptEntityWrapper(cor::Entity *e, cor::FrameContext &context);
  AIScriptEntityWrapper(cor::ConstEntity *e, const cor::FrameContext &context);

  // set rotation speed
  // void set_turning_speed_multiple(float multiple);

  // return rotation angle to target
  /*float get_target_angle();*/

  // turn towards the target coord
  void turn_towards(float x, float y, float z);
  void turn_towards_target();

  // identifies if there is an entity of interest in the vicinity
  bool look_at_surroundings();

  // plays a sound from script
  void send_play_sound_event(std::string path, float pitch, float gain, bool loop);

  // activates a periodic audio group, should be used in scripts
  void activate_repeating_audio_group(std::string group, bool val);

  // sets the repeat timer
  void set_repeat_intervall(std::string group, float val);

  // lists of information to be stored in next write update
  std::vector<AddThreatEvent> m_threat_event_list;
  std::vector<glm::vec3> m_search_location_list;

 private:
  std::variant<cor::FrameContext *, const cor::FrameContext *> m_context;
  std::variant<cor::Entity *, cor::ConstEntity *> m_entity;
};
}  // namespace gmp

UNBOUND_TYPE((gmp::AIScriptEntityWrapper), get_entity, get_frame_context, get_entity_manager,
             get_event_manager, get_time_info, get_asset_path, get_model_asset, get_texture_asset,
             get_sound_buffer_asset, get_shader_asset,
             /*set_turning_speed_multiple, */ /*get_target_angle,*/ listen_to_surroundings,
             look_at_surroundings, turn_towards, turn_towards_target, send_play_sound_event,
             activate_repeating_audio_group, set_repeat_intervall, get_capsule_shared_pointer,
             m_threat_event_list, m_search_location_list, get_box_shared_pointer)

UNBOUND_TYPE((gmp::TimeInfo), dt, speed_factor, unslowed_factor)

#endif
