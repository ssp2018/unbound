#pragma once
#ifndef GMP_ANIMATION_SYSTEM_HPP
#define GMP_ANIMATION_SYSTEM_HPP

#include <bse/asset.hpp>  // for Asset
#include <cor/event.hpp>
#include <cor/system.hpp>  // for System
#include <gfx/context.hpp>
#include <gfx/mesh.hpp>
namespace bse {
struct Joint;
}  // namespace bse
namespace bse {
struct KeyFrame;
}  // namespace bse
namespace bse {
struct SQT;
}
namespace cor {
struct FrameContext;
class ConstEntity;
}  // namespace cor
namespace gfx {
class Model;
}
namespace gmp {
struct Animation;
}  // namespace gmp

namespace bse {
struct Joint;
class Skeleton;
class AnimationClip;
struct KeyFrame;
}  // namespace bse

namespace gmp {

struct AnimationComponent;
struct ModelComponent;
struct Animation;
// Responsible for updating all animation components
class AnimationSystem : public cor::System {
 public:
  AnimationSystem(cor::FrameContext& context);
  ~AnimationSystem();
  // Function that listens for animation events in order to trigger animations
  void animation_event_listener(const AnimationEvent* e);
  virtual void read_update(const cor::FrameContext& context) override;
  virtual void write_update(cor::FrameContext& context) override;
  virtual void write_update_late(cor::FrameContext& context) override;

  struct AnimatedMeshData {
    ~AnimatedMeshData() {
      // Only delete the resources that are not shared between different instances of the same
      // mesh

      gfx::Mesh::MeshBuffers buffers_copy = buffers;
      glm::vec3* positions_copy = positions;
      glm::vec3* normals_copy = normals;
      btBvhTriangleMeshShape* mesh_shape_copy = mesh_shape;
      btTriangleIndexVertexArray* mesh_vertex_array_copy = mesh_vertex_array;
      gfx::Context::run_async([=]() {
        if (buffers_copy.m_vao) {
          gl::glDeleteVertexArrays(1, &buffers_copy.m_vao);
        }

        if (buffers_copy.m_vbo_position) {
          gl::glDeleteBuffers(1, &buffers_copy.m_vbo_position);
        }

        if (buffers_copy.m_vbo_normal) {
          gl::glDeleteBuffers(1, &buffers_copy.m_vbo_normal);
        }

        // Not needed, the memory is handled by OpenGL
        if (positions_copy) {
          delete positions_copy;
        }
        if (normals_copy) {
          delete normals_copy;
        }

        // Delete Bullet objects
        if (mesh_shape_copy) {
          delete mesh_shape_copy;
        }
        if (mesh_vertex_array_copy) {
          delete mesh_vertex_array_copy;
        }
      });
    }

    // OpenGL resources used to render mesh
    gfx::Mesh::MeshBuffers buffers;

    // Animated data for positions and normals
    // Pointers because they are allocated from OpenGL
    glm::vec3* positions;
    glm::vec3* normals;

    // Refers to position data
    btBvhTriangleMeshShape* mesh_shape;
    btTriangleIndexVertexArray* mesh_vertex_array;

    unsigned num_vertices;

    // Reference to mesh data of parent mesh
    const bse::ModelData* parent_mesh_data;

    // Index of last frame this object was updated
    uint16_t frame_updated;

    // Index of last frame the mesh acceleration structure was updated
    uint16_t acceleration_structure_frame_updated;

    // Index of last frame the mesh was skinned
    uint16_t skinning_frame_updated;

    // Task handle for acceleration structure recalculation task
    bse::TaskHandle task = bse::TaskHandle::NULL_HANDLE;

    // Handle to the entity which this data belongs to.
    cor::EntityHandle entity_handle;

    // Vector of joint transforms
    std::vector<glm::mat4> joint_transforms;
  };

  AnimatedMeshData* get_animated_mesh_data(const cor::EntityHandle& handle);

  VIRTUAL_CLASS_SERFUN((AnimationSystem)) { SERIALIZE(self.m_animation_events); }
  VIRTUAL_CLASS_DESERFUN((AnimationSystem)) {
    // Reset the animation system
    self.m_mesh_datas.clear();
    self.animation_notify_events.clear();
    DESERIALIZE(self.m_animation_events);
  }

  // Animates vertices of the mesh
  static void skin_mesh(AnimatedMeshData& mesh);

 private:
  struct RecalcAccelerationStructureTask : public bse::Task {
    RecalcAccelerationStructureTask(btBvhTriangleMeshShape* shape) : shape(shape) {}
    virtual void kernel() override {
      btVector3 min(0, 0, 0);
      btVector3 max(0, 0, 0);
      shape->getAabb(btTransform::getIdentity(), min, max);

      // Add some margin to avoid failing asserts
      min -= btVector3(10, 10, 10);
      max += btVector3(10, 10, 10);

      // Recalculate mesh shape acceleration structure
      shape->refitTree(min, max);
    }

    btBvhTriangleMeshShape* shape;
  };

  struct UpdateModelTask : public bse::Task {
    void kernel() override;

    std::shared_ptr<AnimatedMeshData> data;
    bse::Asset<gfx::Mesh> mesh;
    bool is_skinning_dirty = false;
    bool is_acceleration_structure_dirty = false;
    bool is_mesh_data_new = false;
    bse::TaskHandle handle;  // is only used by AnimationSystem, not the task itself
  };

  // CPU skin animated meshes. Returns vector of all tasks to wait for
  void skin_all_meshes(const cor::FrameContext& context);

  // Updates animation states of all animated components
  void update_animation_states(cor::FrameContext& context);

  // Update the transform of all entities attached to a skeleton
  void update_attachments(cor::FrameContext& context) const;

  // Updates AttachToTriangleComponents
  void update_triangle_attachments(cor::FrameContext& context);

  // Update all animated entities' animation transformations
  void update_animations(const cor::FrameContext& context) const;

  // Update aim offset of skeletal mesh
  void update_aim_offset(const cor::FrameContext& context, const cor::EntityHandle handle,
                         std::vector<bse::SQT>& quat_palette) const;

  // Quadratic easing for the aim offset ratio
  float quadratic_easing(float t) const;

  // Recursively multiply joint transforms by their parent transform and offset matrix
  // joint_transform should contain final interpolated transforms
  void create_final_joint_transform(std::vector<glm::mat4>& joint_transforms,
                                    glm::mat4 parent_transform, int current_id,
                                    const bse::Skeleton& skel) const;

  // Recursively blends two SQT vectors and places them in output vector, starting at 'current_id'.
  // Weight determines blend factor, 0.0 = input1, 1.0 = input2
  void AnimationSystem::blend_quat_transforms(std::vector<bse::SQT>& output,
                                              const std::vector<bse::SQT>& input1,
                                              const std::vector<bse::SQT>& input2, int current_id,
                                              float weight, const bse::Skeleton& skel) const;

  // Adds an AnimatedMeshData structure for an animated mesh
  void add_mesh_data(const cor::FrameContext& context, cor::EntityHandle& handle);

  // Removes entries in m_mesh_datas that have not been updated this frame
  void remove_stale_mesh_data(const cor::FrameContext& context);

  // Get update tasks that should be run this frame.
  std::vector<UpdateModelTask> get_update_model_tasks(const cor::FrameContext& context);

  // Map from entity handle ID to mesh data
  // Mutable because it needs to be updated in read_update(). It is not exposed to other systems,
  // so it should be fine
  mutable std::unordered_map<uint32_t, std::shared_ptr<AnimatedMeshData>> m_mesh_datas;

  mutable std::vector<AnimationNotifyEvent> animation_notify_events;
  // Send out all animation notify events found
  void send_animation_notifies(cor::FrameContext& context);

  // Update joint look at component
  void update_look_at(cor::ConstEntity origin, bse::Joint& origin_joint, bse::SQT& joint_transform,
                      cor::ConstEntity target) const;

  cor::EventManager::ListenID m_animation_event_lid;
  mutable std::vector<AnimationEvent> m_animation_events;
  mutable std::vector<AnimationNotifyEvent> m_animation_event_player_notify_events;
  std::vector<std::pair<cor::EntityHandle, bse::Asset<gfx::Model>>> m_new_buffers;

  // List of new entity handles that need their animation component updated in write_update
  std::vector<std::pair<cor::EntityHandle, AnimatedMeshData*>> m_new_mesh_datas;

  std::vector<UpdateModelTask> m_update_model_tasks;
  std::vector<UpdateModelTask> m_buffer_tasks;
};
};  // namespace gmp

#endif  // !GMP_ANIMATION_SYSTEM
