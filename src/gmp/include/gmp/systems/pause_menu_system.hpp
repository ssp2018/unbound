#pragma once
#ifndef PAUSE_MENU_SYSTEM_HPP
#define PAUSE_MENU_SYSTEM_HPP

#include "cor/entity_handle.hpp"  // for EntityHandle
#include "gmp/components/graphic_text_component.hpp"
#include <aud/sound_source.hpp>
#include <bse/serialize.hpp>
#include <cor/input.hpp>
#include <cor/system.hpp>  // for System
#include <gfx/texture.hpp>
#include <gfx/window.hpp>

namespace cor {
struct FrameContext;
}

namespace gmp {

// System for in game pause menu
class PauseMenuSystem : public cor::System {
 public:
  PauseMenuSystem(cor::FrameContext &context, gfx::Window &window);

  virtual ~PauseMenuSystem(){};

  // update with read only
  void read_update(const cor::FrameContext &context) override;

  // update with write only
  void write_update(cor::FrameContext &context) override;

  VIRTUAL_CLASS_SERFUN((PauseMenuSystem)) {
    SERIALIZE(m_paused);
    SERIALIZE(m_continue_handle);
    SERIALIZE(m_menu_handle);
    SERIALIZE(m_quit_handle);
    SERIALIZE(m_select_sound);
    SERIALIZE(m_click_sound);
    SERIALIZE(m_highlighted_entity_name);
    SERIALIZE(m_clicked_entity_name);
  }
  VIRTUAL_CLASS_DESERFUN((PauseMenuSystem)) {
    DESERIALIZE(m_paused);
    DESERIALIZE(m_continue_handle);
    DESERIALIZE(m_menu_handle);
    DESERIALIZE(m_quit_handle);
    DESERIALIZE(m_select_sound);
    DESERIALIZE(m_click_sound);
    DESERIALIZE(m_highlighted_entity_name);
    DESERIALIZE(m_clicked_entity_name);
  }

 private:
  // Show the pause menu
  void show_menu();

  // Hide the pause menu
  void hide_menu();

  // Game is paused
  bool m_paused = false;

  // Continue entity handle
  cor::EntityHandle m_continue_handle;
  // Quit entity handle
  cor::EntityHandle m_menu_handle;
  // Quit entity handle
  cor::EntityHandle m_quit_handle;

  bse::RuntimeAsset<aud::SoundSource> m_select_sound;
  bse::RuntimeAsset<aud::SoundSource> m_click_sound;

  std::string m_highlighted_entity_name = "";
  std::string m_clicked_entity_name = "";

  gfx::Window &m_window;

  // variable to hold the context
  cor::FrameContext &m_context;
};
}  // namespace gmp

#endif  // !PAUSE_SYSTEM_HPP
