#pragma once
#ifndef SPAWN_SYSTEM_HPP
#define SPAWN_SYSTEM_HPP

#include "bse/asset.hpp"
#include "gfx/model.hpp"
#include "gfx/texture.hpp"
#include <bse/serialize.hpp>
#include <cor/entity.hpp>
#include <cor/entity_handle.hpp>
#include <cor/system.hpp>
#include <ext/ext.hpp>
#include <scr/script.hpp>

namespace cor {
struct FrameContext;
}

namespace gmp {

// System that spawns enteties
class SpawnSystem : public cor::System {
 public:
  SpawnSystem(cor::FrameContext &context);
  virtual ~SpawnSystem(){};

  // update with read only
  void read_update(const cor::FrameContext &context) override;
  // update
  void write_update(cor::FrameContext &context) override;

  VIRTUAL_CLASS_SERFUN((SpawnSystem)) {}
  VIRTUAL_CLASS_DESERFUN((SpawnSystem)) {}

 private:
  cor::EntityHandle spawn_melee_grunt(cor::EntityHandle owner);

  cor::EntityHandle spawn_ranged_grunt(cor::EntityHandle owner);

  cor::EntityHandle spawn_skull(cor::EntityHandle owner);

  void spawn_golem_boss();

  void spawn_cole_boss();

  cor::FrameContext &m_context;

  bse::Asset<scr::Script> m_death_scripts;
  bse::Asset<gfx::Model> m_grunt_model;
  bse::Asset<gfx::Model> m_skull_model;
  bse::Asset<gfx::Model> m_golem_model;
  bse::Asset<gfx::Model> m_cole_model;
  bse::Asset<gfx::Model> m_cole_armor_model;
};
};  // namespace gmp
#endif