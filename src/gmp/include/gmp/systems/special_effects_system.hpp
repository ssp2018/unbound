#pragma once
#ifndef SPECIAL_EFFECTS_SYSTEM
#define SPECIAL_EFFECTS_SYSTEM

#include "cor/entity_handle.hpp"  // for EntityHandle
#include "cor/frame_context.hpp"
#include <bse/serialize.hpp>
#include <cor/system.hpp>  // for System

namespace cor {
class Entity;
}
namespace cor {
struct FrameContext;
}
struct AIAttackStartEvent;
struct CollisionEvent;

namespace gmp {
// info about particles
struct ParticleInfo {
  float death_countdown;
  cor::EntityHandle handle;
  bool handled;
};

class SpecialEffectsSystem : public cor::System {
 public:
  SpecialEffectsSystem(cor::FrameContext &context);

  virtual ~SpecialEffectsSystem(){};

  // update with read only
  void read_update(const cor::FrameContext &context) override;

  // update with write only
  void write_update(cor::FrameContext &context) override;

  VIRTUAL_CLASS_SERFUN((SpecialEffectsSystem)) {}
  VIRTUAL_CLASS_DESERFUN((SpecialEffectsSystem)) {}

 private:
  // handle FractureOnImpactComponent
  void trigger_fracture(cor::EntityHandle handle);

  // handle ExplodeOnImpactComponent
  void trigger_explosion(cor::EntityHandle handle);

  // destroy the given entity and spawn corresponding entities
  void destroy_destructible_entity(cor::EntityHandle handle);

  cor::FrameContext &m_context;
};
}  // namespace gmp

#endif