#pragma once
#ifndef PICKUP_SYSTEM_HPP
#define PICKUP_SYSTEM_HPP

#include "cor/entity_handle.hpp"  // for EntityHandle
#include <bse/serialize.hpp>
#include <cor/system.hpp>  // for System
#include <gfx/model.hpp>
#include <gfx/texture.hpp>

namespace cor {
struct FrameContext;
}

namespace gmp {

// System for handling pickups
class PickupSystem : public cor::System {
 public:
  PickupSystem(cor::FrameContext &context);

  virtual ~PickupSystem(){};

  // update with read only
  void read_update(const cor::FrameContext &context) override;

  // update with write only
  void write_update(cor::FrameContext &context) override;

  VIRTUAL_CLASS_SERFUN((PickupSystem)) {}
  VIRTUAL_CLASS_DESERFUN((PickupSystem)) {}

 private:
  bse::Asset<gfx::Model> m_health_orb_model;
  bse::Asset<gfx::Model> m_arrow_pickup_model;
  bse::Asset<gfx::Texture> m_swirl_particle;
};

}  // namespace gmp

#endif  // PICKUP_SYSTEM_HPP