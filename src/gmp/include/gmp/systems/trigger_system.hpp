#pragma once

#ifndef GMP_TRIGGER_SYSTEM_HPP
#define GMP_TRIGGER_SYSTEM_HPP

#include <cor/system.hpp>  // for System
#include <bse/serialize.hpp>  
namespace cor {
struct FrameContext;
}

namespace gmp {
// Handles collisions between entities and triggers of different kinds
class TriggerSystem : public cor::System {
 public:
  TriggerSystem(cor::FrameContext& context);
  virtual ~TriggerSystem();

  void read_update(const cor::FrameContext& context) override;
  void write_update(cor::FrameContext& context) override;

  VIRTUAL_CLASS_SERFUN((TriggerSystem)) {}
  VIRTUAL_CLASS_DESERFUN((TriggerSystem)) {}

 private:
};
}  // namespace gmp
#endif  // GMP_TRIGGER_SYSTEM_HPP
