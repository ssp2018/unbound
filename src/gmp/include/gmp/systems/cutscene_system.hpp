#pragma once

#ifndef GMP_CUTSCENE_SYSTEM_HPP
#define GMP_CUTSCENE_SYSTEM_HPP

#include "gmp/components/cutscene_waypoint_component.hpp"
#include <cor/entity_handle.hpp>
#include <cor/system.hpp>
#include <ext/ext.hpp>
#include <gmp/transform.hpp>

namespace gmp {
// System that takes control of the camera and moves it along a spline path
class CutsceneSystem : public cor::System {
 public:
  CutsceneSystem(cor::FrameContext& context);
  virtual ~CutsceneSystem();

  virtual void read_update(const cor::FrameContext& context) override;
  virtual void write_update(cor::FrameContext& context) override;

  VIRTUAL_CLASS_SERFUN((CutsceneSystem)) {
    //
  }
  VIRTUAL_CLASS_DESERFUN((CutsceneSystem)) {
    //
  }

 private:
  struct Waypoint {
    CutsceneWaypointComponent::Type type;
    Transform transform;
    int fov;
  };

 private:
  void end_cutscene(cor::FrameContext& context);

 private:
  std::vector<Waypoint> m_waypoints;
  bool m_cutscene_running = true;
  float m_cutscene_time = 0.f;
  cor::EntityHandle m_camera;
  int m_last_fov = 90;

  cor::EntityHandle m_letterbox_upper;
  cor::EntityHandle m_letterbox_lower;
};
}  // namespace gmp
#endif  // GMP_CUTSCENE_SYSTEM_HPP
