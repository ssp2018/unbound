#pragma once
#ifndef GMP_DEBUG_CAMERA_SYSTEM_HPP
#define GMP_DEBUG_CAMERA_SYSTEM_HPP
#include <cor/system.hpp>  // for System
namespace cor {
struct FrameContext;
}

namespace gmp {

// standalone camera for debug
class DebugCameraSystem : public cor::System {
 public:
  DebugCameraSystem(cor::FrameContext &context);
  
  virtual ~DebugCameraSystem(){};

  // update with read only
  void read_update(const cor::FrameContext &context) override;

  // update with write only
  void write_update(cor::FrameContext &context) override;

  VIRTUAL_CLASS_SERFUN((DebugCameraSystem)) {
    SERIALIZE(self.m_forward);
    SERIALIZE(self.m_right);
    SERIALIZE(self.m_rotation_x);
    SERIALIZE(self.m_rotation_z);
    SERIALIZE(self.m_debug_camera_active);
    SERIALIZE(self.m_debug_control_active);
  }
  VIRTUAL_CLASS_DESERFUN((DebugCameraSystem)) {
    DESERIALIZE(self.m_forward);
    DESERIALIZE(self.m_right);
    DESERIALIZE(self.m_rotation_x);
    DESERIALIZE(self.m_rotation_z);
    DESERIALIZE(self.m_debug_camera_active);
    DESERIALIZE(self.m_debug_control_active);
  }

 private:
  // Event list
  // std::vector<cor::EventManager::ListenID> m_lids;

  // Coordination variables
  float m_forward = 0.f;
  float m_right = 0.f;
  float m_rotation_x = 0.f;
  float m_rotation_z = 0.f;

  // Set active camera view and controls
  bool m_debug_camera_active = false;
  bool m_debug_control_active = false;
};
};  // namespace gmp

#endif
