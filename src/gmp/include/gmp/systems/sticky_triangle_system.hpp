#pragma once

#ifndef GMP_STICKY_TRIANGLE_SYSTEM_HPP
#define GMP_STICKY_TRIANGLE_SYSTEM_HPP

#include <cor/system.hpp>

namespace cor {
struct FrameContext;
}  // namespace cor

namespace gmp {

// Updates transforms of entities with StickyTriangleComponents
class StickyTriangleSystem : public cor::System {
 public:
  virtual ~StickyTriangleSystem(){};

  // read update
  void read_update(const cor::FrameContext& context) override;

  // write update
  void write_update(cor::FrameContext& context) override;

  VIRTUAL_CLASS_SERFUN((StickyTriangleSystem)) {}
  VIRTUAL_CLASS_DESERFUN((StickyTriangleSystem)) {}

 private:
  // Add the sticky entity to dirty queue if
  // any of its points have dirty transforms.
  void add_dirty(cor::EntityHandle sticky_entity, const cor::FrameContext& context);

  struct DirtySticky {
    cor::EntityHandle handle;
    Transform transform;
  };

  std::vector<DirtySticky> m_dirty_stickies;
};
}  // namespace gmp

#endif  // GMP_STICKY_TRIANGLE_SYSTEM_HPP