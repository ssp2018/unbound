#pragma once
#ifndef GMP_AI_SYSTEM_HPP
#define GMP_AI_SYSTEM_HPP

#include <ai/ai_base.hpp>
#include <bse/asset.hpp>
#include <cor/cor.hpp>
#include <cor/entity.hpp>
#include <cor/event_manager.hpp>
#include <cor/system.hpp>
#include <gmp/ai_script_entity_wrapper.hpp>
#include <gmp/components/ai_component.hpp>
#include <gmp/components/ai_force_state_change_component.hpp>
#include <gmp/components/static_component.hpp>
#include <scr/object.hpp>
#include <scr/script.hpp>

namespace gmp {
struct AIEntityUpdate {
  glm::vec3 swarm_vec;
};
// a system for updating all ai components
class AISystem : public cor::System {
 public:
  // Constructor also initializes the scripts
  AISystem(cor::FrameContext &context);
  virtual ~AISystem(){};

  // update with read only
  void read_update(const cor::FrameContext &context) override;

  // update with write only
  void write_update(cor::FrameContext &context) override;

  VIRTUAL_CLASS_SERFUN((AISystem)) {
    //
    SERIALIZE(self.m_transition_table);
    SERIALIZE(self.m_update_table);
    SERIALIZE(self.m_function_table);
    SERIALIZE(self.m_transition_table_script);
    SERIALIZE(self.m_update_table_script);
    SERIALIZE(self.m_function_table_script);  // not recreated correctly?
    SERIALIZE(self.m_active_ai);
    SERIALIZE(self.m_transitions);
    SERIALIZE(self.m_stun);
    SERIALIZE(self.m_entity_update);
    SERIALIZE(self.m_threat);
    SERIALIZE(self.m_search_location);
    SERIALIZE(self.m_ai_base);
  }
  VIRTUAL_CLASS_DESERFUN((AISystem)) {
    //
    DESERIALIZE(self.m_transition_table);
    DESERIALIZE(self.m_update_table);
    DESERIALIZE(self.m_function_table);
    DESERIALIZE(self.m_transition_table_script);
    DESERIALIZE(self.m_update_table_script);
    DESERIALIZE(self.m_function_table_script);  // not recreated correctly?
    DESERIALIZE(self.m_active_ai);
    DESERIALIZE(self.m_transitions);
    DESERIALIZE(self.m_stun);
    DESERIALIZE(self.m_entity_update);
    DESERIALIZE(self.m_threat);
    DESERIALIZE(self.m_search_location);
    DESERIALIZE(self.m_ai_base);
    initialize_tables();
  }

 private:
  // Here we create the tables required to store the AI functionality while also defining all
  // of the AI's actions.
  void initialize_tables();
  // Exposing AIScriptEntity type with its variables
  void expose_types();
  // event subscription
  void event_subscription(cor::FrameContext &context);

  // changes AI state in entity's AIComponent and dispatches event
  void handle_state_transition(cor::FrameContext &context, cor::Entity entity,
                               AIScriptEntityWrapper &script_entity, std::string str);

  // sends out area damage event and manupulates component
  void handle_attack_start(cor::FrameContext &context, AIComponent &ai_component,
                           cor::EntityHandle entity_handle, gmp::AIScriptEntityWrapper &aisew);

  // sets AI into "stunned" state
  void handle_stun_start_event(const StunEvent *event);
  // void handle_stun_end_event();

  // set ai path from current position, makes changes in AIPathfinding struct to set new path
  void make_path(AIPathfinding &pf, glm::vec3 curr_pos, cor::FrameContext &context);
  // follow path, returns a directional glm::vec2 following the path returns {0,0} if arrived att
  // goal location of path
  glm::vec2 follow_path(AIPathfinding &pf, glm::vec2 curr_pos, float dt);
  // calc next direction and returns {0,0} if points are close to the same.
  glm::vec2 calc_dir(glm::vec2 curr_pos, glm::vec2 next_pos, float epsi);
  // handle pathing
  void handle_pathing(cor::FrameContext &context);

  // takes care of the steering of the flying ai.
  void steering(cor::FrameContext &context);

  // calculates the steering vector
  glm::vec3 steering_vector(cor::FrameContext &context, cor::Entity &entity);

  // calculates the avoidance vector
  glm::vec3 avoidance_vector(cor::FrameContext &context, cor::Entity &entity);

  // calculates avoidance vector for entity away from a static component
  glm::vec3 avoidance_vector(cor::Entity &entity, gmp::StaticComponent &static_comp);

  // calculates the swarms vectors
  void swarm_update(const cor::FrameContext &context);
  // calculates the pursue swarms vectors
  void pursue_swarm_update(const cor::FrameContext &context,
                           const std::vector<cor::EntityHandle> &swarm,
                           cor::EntityHandle swarm_target_handle);
  // calculates the combat swarms vectors
  void combat_swarm_update(const cor::FrameContext &context,
                           const std::vector<cor::EntityHandle> &swarm,
                           cor::EntityHandle swarm_target_handle);

  // add entity threat found during listen/look/other
  bool add_threat(cor::EntityHandle entity_handle, cor::FrameContext &context);

  // add search location, aquired from sound
  void add_search_location(cor::EntityHandle entity_handle, cor::FrameContext &context);

  // regain hp of ai's far away from player
  void regain_hp(cor::FrameContext &context);

  bse::Timer m_frame_timer;
  float m_print_time;
  float m_frame_time;
  float m_read_time;
  float m_write_time;
  std::deque<float> m_frame_time_share_queue;
  float m_frame_time_share;
  float m_curr_run_chance;

#ifdef NDEBUG
  // float m_target_utilization = 0.12;
  float m_target_utilization = 0.16;
#else
  float m_target_utilization = 0.20;
#endif

  float m_correction_speed = 5;
  float m_hard_cutoff_distance = 400;  // default, can be defined per-unit
  float m_live_update_distance = 100;

  scr::Object m_transition_table;  // the root transition table is contained in this asset
  scr::Object m_update_table;      // the root update table is contained in this asset

  scr::Object m_function_table;  // runs a function given a table, type and state

  bse::Asset<scr::Script> m_transition_table_script;
  bse::Asset<scr::Script> m_update_table_script;
  bse::Asset<scr::Script> m_function_table_script;

  // stuff stored by read update to be executed next write update
  std::unordered_map<cor::EntityHandle, char> m_active_ai;                // AI active this frame
  std::unordered_map<cor::EntityHandle, char> m_dist_cutoff_ai;           // AI cut from hard cutoff
  std::unordered_map<cor::EntityHandle, std::string> m_transitions;       // transitions
  std::unordered_map<cor::EntityHandle, StunEvent> m_stun;                // catched stun events
  std::unordered_map<cor::EntityHandle, AIEntityUpdate> m_entity_update;  // swarm update
  std::unordered_map<cor::EntityHandle, AddThreatEvent> m_threat;      // cathed threatupdate event
  std::unordered_map<cor::EntityHandle, glm::vec3> m_search_location;  // change search pos

  ai::AIBase m_ai_base;

  // edit
  float m_swarm_pursue_seperation_factor = 800.f;
  float m_swarm_combat_seperation_factor = 1200.f;
  float m_target_seperation_factor = 1800.f;
  float m_target_cohesion_factor = 1.0f;
};
};  // namespace gmp

#endif
