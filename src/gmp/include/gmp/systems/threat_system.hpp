#pragma once

#ifndef GMP_THREAT_SYSTEM_HPP
#define GMP_THREAT_SYSTEM_HPP

#include <cor/event_manager.hpp>
#include <cor/frame_context.hpp>
#include <cor/system.hpp>

namespace gmp {

// system that manages threat for all threat components
class ThreatSystem : public cor::System {
 public:
  ThreatSystem(cor::FrameContext& context);

  virtual ~ThreatSystem(){};

  // read update
  virtual void read_update(const cor::FrameContext& context) override;

  // write update
  virtual void write_update(cor::FrameContext& context) override;

 private:
  float m_threat_loss = 0.5f;

  cor::FrameContext& m_context;

  // adds threat to a entity towards another entity
  void add_threat_event_reciever(const AddThreatEvent* event);

  // increases already existing threat level for a entity
  void increase_threat_event_reciever(const IncreaseThreatEvent* event);

  // std::vector<cor::EventManager::ListenID> m_add_threat_event_id;
};
}  // namespace gmp

#endif