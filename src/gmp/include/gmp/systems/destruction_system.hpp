#pragma once
#ifndef GMP_DESTRUCTION_SYSTEM_HPP
#define GMP_DESTRUCTION_SYSTEM_HPP

#include <cor/system.hpp>  // for System
namespace cor {
struct FrameContext;
}
namespace gmp {

// System for handling destruction/removal of entities
class DestructionSystem : public cor::System {
 public:
  virtual ~DestructionSystem(){};

  // Update with read only
  void read_update(const cor::FrameContext &context) override;

  // Update with write only
  void write_update(cor::FrameContext &context) override;

  VIRTUAL_CLASS_SERFUN((DestructionSystem)) {  // No-Op
  }
  VIRTUAL_CLASS_DESERFUN((DestructionSystem)) {  // No-Op
  }
};

}  // namespace gmp

#endif  // GMP_DESTRUCTION_SYSTEM_HPP
