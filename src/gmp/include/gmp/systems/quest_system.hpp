#pragma once
#ifndef QUEST_SYSTEM_HPP
#define QUEST_SYSTEM_HPP

#include <bse/serialize.hpp>
#include <cor/entity_handle.hpp>
#include <cor/event_manager.hpp>
#include <cor/system.hpp>  // for System
#include <scr/script.hpp>

namespace gmp {
// System handling all quests
class QuestSystem : public cor::System {
 public:
  QuestSystem(cor::FrameContext &context);

  virtual ~QuestSystem(){};

  // update with read only
  void read_update(const cor::FrameContext &context) override;

  // update with write only
  void write_update(cor::FrameContext &context) override;

  VIRTUAL_CLASS_SERFUN((QuestSystem)) {
    SERIALIZE(self.m_quest_line);
    SERIALIZE(self.m_horn_note_quest);
    SERIALIZE(self.m_horn_quest);
    SERIALIZE(self.m_golem_quest);
    SERIALIZE(self.m_skull_quest);
    SERIALIZE(self.m_cole_quest);
    SERIALIZE(self.m_death_scripts);
  }
  VIRTUAL_CLASS_DESERFUN((QuestSystem)) {
    DESERIALIZE(self.m_quest_line);
    DESERIALIZE(self.m_horn_quest);
    DESERIALIZE(self.m_horn_note_quest);
    DESERIALIZE(self.m_golem_quest);
    DESERIALIZE(self.m_skull_quest);
    DESERIALIZE(self.m_cole_quest);
    DESERIALIZE(self.m_death_scripts);
  }

 private:
  // Show the quests
  void show_first_quests();

  cor::EntityHandle m_quest_line;
  cor::EntityHandle m_horn_note_quest;
  cor::EntityHandle m_horn_quest;
  cor::EntityHandle m_golem_quest;
  cor::EntityHandle m_skull_quest;
  cor::EntityHandle m_cole_quest;

  bse::Asset<scr::Script> m_death_scripts;

  std::vector<cor::EntityHandle> m_start_descriptions;
  std::vector<cor::EntityHandle> m_note_descriptions;

  cor::FrameContext &m_context;
};
}  // namespace gmp

#endif  // !QUEST_SYSTEM_HPP
