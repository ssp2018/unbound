#pragma once
#ifndef GMP_ATTACH_UPDATE_SYSTEM_HPP
#define GMP_ATTACH_UPDATE_SYSTEM_HPP

#include <cor/system.hpp>  // for System

namespace gmp {

// Responsible for updating all attachment components
class AttachUpdateSystem : public cor::System {
 public:
  virtual ~AttachUpdateSystem(){};

  void read_update(const cor::FrameContext& context) override;
  void write_update(cor::FrameContext& context) override;

  VIRTUAL_CLASS_SERFUN((AttachUpdateSystem)) {
    // No-Op
  }
  VIRTUAL_CLASS_DESERFUN((AttachUpdateSystem)) {
    // No-Op
  }

 private:
  void update_entity(cor::FrameContext& context, cor::Entity& entity);
};
};  // namespace gmp

#endif
