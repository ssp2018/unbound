#pragma once
#ifndef GMP_FALL_DAMAGE_SYSTEM_HPP
#define GMP_FALL_DAMAGE_SYSTEM_HPP

#include <cor/system.hpp>

namespace gmp {

// System for handling fall damage
class FallDamageSystem : public cor::System {
 public:
 
  virtual ~FallDamageSystem(){};

  // Update with read only
  void read_update(const cor::FrameContext &context) override;

  // Update with write only
  void write_update(cor::FrameContext &context) override;

  VIRTUAL_CLASS_SERFUN((FallDamageSystem)) {
    SERIALIZE(self.m_time_since_update);  //
  }
  VIRTUAL_CLASS_DESERFUN((FallDamageSystem)) {
    DESERIALIZE(self.m_time_since_update);  //
  }

 private:
  // Timer to keep track of update
  float m_time_since_update = 0;
};

}  // namespace gmp

#endif  // GMP_FALL_DAMAGE_SYSTEM_HPP
