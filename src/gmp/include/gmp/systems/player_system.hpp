#pragma once
#ifndef GMP_PLAYER_SYSTEM_HPP
#define GMP_PLAYER_SYSTEM_HPP

#include "cor/entity_handle.hpp"  // for EntityHandle
#include "cor/frame_context.hpp"
#include "gfx/model.hpp"
#include "gmp/player/player_base_state.hpp"  // for StateAction, PlayerBaseS...
#include <bse/serialize.hpp>
#include <cor/system.hpp>  // for System
#include <gfx/texture.hpp>

namespace cor {
struct FrameContext;
}

namespace gmp {

class ParticlesComponent;

// ECS system for the player
// Internally runs a state machine
class PlayerSystem : public cor::System {
 public:
  PlayerSystem(cor::FrameContext &context);

  virtual ~PlayerSystem(){};

  // Update with read only
  void read_update(const cor::FrameContext &context) override;

  // Update with write only
  void write_update(cor::FrameContext &context) override;

  VIRTUAL_CLASS_SERFUN((PlayerSystem)) {
    // Serialize actions
    std::size_t num_actions = self.m_actions.size();
    SERIALIZE(num_actions);
    for (int i = 0; i < num_actions; i++) {
      SERIALIZE(self.m_actions[i].action);
      SERIALIZE(self.m_actions[i].new_state);
      SERIALIZE(self.m_actions[i].index);
    }

    // Serialize stack
    SERIALIZE(self.m_stack);

    SERIALIZE(self.m_current_checkpoint_area);
    SERIALIZE(self.m_action_index);
    SERIALIZE(self.m_keys_pressed);
    SERIALIZE(self.m_camera_rotation_x);
    SERIALIZE(self.m_camera_rotation_z);
    SERIALIZE(self.m_camera_distance);
    SERIALIZE(self.m_jumps_left);
    SERIALIZE(self.m_dashes_left);
    SERIALIZE(self.m_draw_time);
    SERIALIZE(self.m_shoot_cooldown);
    SERIALIZE(self.m_has_released_slow_motion);
    SERIALIZE(self.m_face_aiming_dir_states);
    SERIALIZE(self.m_camera_hit_point);
    SERIALIZE(self.m_rope_handle);
    SERIALIZE(self.m_rope_arrow_handle);
    SERIALIZE(self.m_flying_arrows);
    SERIALIZE(self.m_stuck_arrows);
    SERIALIZE(self.m_unused_arrows);
    SERIALIZE(self.m_next_stuck_arrow_update);
    SERIALIZE(self.m_selected_arrow);
    SERIALIZE(self.m_explosive_arrows_count);
    SERIALIZE(self.m_max_explosive_arrows_count);
    SERIALIZE(self.m_rope_arrow_hit_pos);
    SERIALIZE(self.m_time_left_of_attacking_anim);
    SERIALIZE(self.m_anchored_object_handle);
    SERIALIZE(self.m_launch_help_dir);
    SERIALIZE(self.m_launch_help_active);
    SERIALIZE(self.m_original_distance_to_swing_center);
    SERIALIZE(self.m_stamina_left);
    SERIALIZE(self.m_stamina_recharge_cooldown);
  }
  VIRTUAL_CLASS_DESERFUN((PlayerSystem)) {
    // Deserialize actions
    std::size_t num_actions;
    DESERIALIZE(num_actions);

    for (int i = 0; i < num_actions; i++) {
      if (i >= self.m_actions.size()) {
        self.m_actions.emplace_back(ActionType::PUSH, nullptr, -1);
      }
      DESERIALIZE(self.m_actions[i].action);
      bool was_null;
      DESERIALIZE(was_null);
      if (!was_null) {
        StateType state_type;
        DESERIALIZE(state_type);
        self.m_actions[i].new_state = self.m_exemplars[state_type]();
        DESERIALIZE(*self.m_actions[i].new_state);
      }
      DESERIALIZE(self.m_actions[i].index);
    }

    // Deserialize stack
    std::size_t num_states;
    DESERIALIZE(num_states);
    for (int i = 0; i < num_states; i++) {
      if (i >= self.m_stack.size()) {
        self.m_stack.emplace_back();
      }
      bool was_null;
      DESERIALIZE(was_null);
      if (!was_null) {
        StateType state_type;
        DESERIALIZE(state_type);
        self.m_stack[i] = self.m_exemplars[state_type]();
        DESERIALIZE(*self.m_stack[i]);
      }
    }
    self.m_stack.resize(num_states);

    DESERIALIZE(self.m_current_checkpoint_area);
    DESERIALIZE(self.m_action_index);
    DESERIALIZE(self.m_keys_pressed);
    DESERIALIZE(self.m_camera_rotation_x);
    DESERIALIZE(self.m_camera_rotation_z);
    DESERIALIZE(self.m_camera_distance);
    DESERIALIZE(self.m_jumps_left);
    DESERIALIZE(self.m_dashes_left);
    DESERIALIZE(self.m_draw_time);
    DESERIALIZE(self.m_shoot_cooldown);
    DESERIALIZE(self.m_has_released_slow_motion);
    DESERIALIZE(self.m_face_aiming_dir_states);
    DESERIALIZE(self.m_camera_hit_point);
    DESERIALIZE(self.m_rope_handle);
    DESERIALIZE(self.m_rope_arrow_handle);
    DESERIALIZE(self.m_flying_arrows);
    DESERIALIZE(self.m_stuck_arrows);
    DESERIALIZE(self.m_unused_arrows);
    DESERIALIZE(self.m_next_stuck_arrow_update);
    DESERIALIZE(self.m_selected_arrow);
    DESERIALIZE(self.m_explosive_arrows_count);
    DESERIALIZE(self.m_max_explosive_arrows_count);
    DESERIALIZE(self.m_rope_arrow_hit_pos);
    DESERIALIZE(self.m_time_left_of_attacking_anim);
    DESERIALIZE(self.m_anchored_object_handle);
    DESERIALIZE(self.m_launch_help_dir);
    DESERIALIZE(self.m_launch_help_active);
    DESERIALIZE(self.m_original_distance_to_swing_center);
    DESERIALIZE(self.m_stamina_left);
    DESERIALIZE(self.m_stamina_recharge_cooldown);
  }

  // Adds a state action to the internal actions vector
  void add_action(ActionType action, std::unique_ptr<PlayerBaseState> new_state = nullptr);

  // Returns if the given state is currently in the stack
  bool state_exists(const StateType state) const;

  // The basic player movement (used by the states)
  void base_move(const float right, const float forward);

  // Puts all arrows among unused_arrows
  void reset_arrows();

  // "Destroys" the arrow by removing the ModelComponent & PhysicsProjectileComponent
  // void destroy_arrow(cor::EntityHandle arrow_handle);

  // Returns if the given handle is active/valid (if it has a ModelComponent)
  bool is_arrow_active(cor::EntityHandle arrow_handle) const;

  // Returns a handle to a new arrow to use.
  // Takes from unused_arrows if possible, otherwise from stuck_arrows.
  cor::EntityHandle get_new_arrow();

  // Helper function to end the swing
  void end_swing();

  // Shoot a rope arrow instantly
  void shoot_rope_arrow();

  // Returns a pointer to the closest hit that is not the player. If non are found, nullptr is
  // returned. The input vector needs to be stored outside the function since a pointer is returned.
  cor::RayResult *get_closest_non_player_hit(std::vector<cor::RayResult> &hits,
                                             const bool filter_out_ai);

  cor::FrameContext &m_context;

  // Spawn positions
  struct Checkpoint {
    glm::vec3 position;
    float radius;
    glm::vec3 spawn_pos;
    float spawn_rot;
  };
  std::vector<Checkpoint> m_checkpoints;
  int m_current_checkpoint_area = -1;
  bool m_in_boss_fight = false;

  int m_action_index;
  std::vector<StateAction> m_actions;
  std::vector<std::unique_ptr<PlayerBaseState>> m_stack;

  // Should not be serialized!
  std::unordered_map<StateType, std::function<std::unique_ptr<PlayerBaseState>()>> m_exemplars;

  PlayerEvents m_keys_pressed;

  // Camera
  float m_camera_rotation_x = 0.f;
  float m_camera_rotation_z = 0.f;
  float m_camera_distance = 1.f;
  const float m_camera_zoom_speed = 24.f;

  // Uses left
  uint8_t m_jumps_left;
  uint8_t m_dashes_left;

  // Arrow shooting
  float m_draw_time = 0.f;
  const float m_max_draw_time = 0.5f;
  float m_shoot_cooldown = 0.f;
  const float m_max_shoot_cooldown = 0.3f;

  // Slow motion
  const float m_slowing_down_speed = 18.f;
  const float m_min_world_speed = 0.10f;
  bool m_has_released_slow_motion = true;

  // Aiming
  unsigned int m_face_aiming_dir_states = 0;
  glm::vec3 m_camera_hit_point;
  glm::vec3 m_arrow_aim_point;

  // Movement
  const float m_movement_speed = 9.f;
  const float m_air_movement_speed = 50.f;

  // Handles
  cor::EntityHandle m_player_handle;
  cor::EntityHandle m_camera_handle;
  cor::EntityHandle m_rope_handle;
  cor::EntityHandle m_rope_arrow_handle;

  // Flying arrows
  struct FlyingArrow {
    cor::EntityHandle handle;
    float time_to_direction_change;
    glm::vec3 new_direction;
    glm::vec3 dir_change_pos;
    float lifetime;
    bool to_be_destroyed = false;
  };
  std::vector<FlyingArrow> m_flying_arrows;
  // Stuck arrows
  struct StuckArrow {
    cor::EntityHandle handle;
    glm::vec3 pos;
    float time_left_particles = 2.f;
  };
  std::vector<StuckArrow> m_stuck_arrows;

  // Arrow related variables
  std::vector<cor::EntityHandle> m_unused_arrows;
  unsigned int m_next_stuck_arrow_update = 0;
  enum ArrowType { PIERCING, EXPLOSIVE };
  ArrowType m_selected_arrow = ArrowType::PIERCING;
  unsigned int m_explosive_arrows_count = 0;
  unsigned int m_max_explosive_arrows_count = 6;
  glm::vec3 m_rope_arrow_hit_pos;
  const float m_max_reach = 76.f;
  cor::EntityHandle
      m_arrow_pos_handle;  // Simple entity that will always be where the rope arrow should spawn.
  float m_time_left_of_attacking_anim = -1.f;

  // Swinging
  cor::EntityHandle m_anchored_object_handle;
  glm::vec3 m_launch_help_dir = glm::vec3(0, 0, 0);
  bool m_launch_help_active = false;
  float m_original_distance_to_swing_center;
  glm::vec3 m_desired_up = glm::vec3(0);

  // Stamina
  float m_stamina_left;
  const float m_max_stamina = 6.f;
  float m_stamina_recharge_cooldown = 0.f;
  const float m_stamina_max_cooldown = 0.6f;
  const float m_stamina_recharge_rate = 1.4f;
  const float m_stamina_dash_cost = 1.2f;

  // Assets
  bse::Asset<aud::SoundBuffer> m_hit_generic;
  bse::Asset<aud::SoundBuffer> m_hit_grass;
  bse::Asset<aud::SoundBuffer> m_hit_wood;
  bse::Asset<aud::SoundBuffer> m_hit_flesh;
  bse::Asset<aud::SoundBuffer> m_explosion_sound;
  bse::Asset<gfx::Texture> m_rope_texture;
  bse::Asset<gfx::Model> m_arrow_model;
  bse::Asset<gfx::Model> m_explosive_arrow_model;
  bse::Asset<gfx::Texture> m_swirl_particle;
  bse::Asset<gfx::Texture> m_explosion_particle;
};

};      // namespace gmp
#endif  // GMP_PLAYER_SYSTEM_HPP
