#pragma once
#ifndef GMP_CROWD_CONTROL_SYSTEM_HPP
#define GMP_CROWD_CONTROL_SYSTEM_HPP

#include <bse/asset.hpp>
#include <cor/cor.hpp>
#include <cor/entity.hpp>
#include <cor/event_manager.hpp>
#include <cor/system.hpp>
#include <ext/ext.hpp>
#include <gmp/components/knock_back_component.hpp>
#include <gmp/components/transform_component.hpp>
#include <scr/object.hpp>

namespace gmp {
// a system for handling stuns, pulls, knock backs...
class CrowdControlSystem : public cor::System {
 public:
  CrowdControlSystem(cor::FrameContext &context);

  virtual ~CrowdControlSystem(){};

  // listener to collision events
  void collision_event_listener(const CollisionEvent *event);

  void read_update(const cor::FrameContext &context) override;

  void write_update(cor::FrameContext &context) override;

  VIRTUAL_CLASS_SERFUN((CrowdControlSystem)) {
    //
    // SERIALIZE(self.);
  }
  VIRTUAL_CLASS_DESERFUN((CrowdControlSystem)) {
    //
    // DESERIALIZE(self.);
  }

 private:
  // called per-frame to handle different CC components & entities
  void handle_force_move(cor::FrameContext &context);
  void handle_hooks(cor::FrameContext &context);
  void handle_knock_back(cor::FrameContext &context);

  // vector holding knock-back fields created previous frame
  std::vector<std::pair<KnockBackComponent, TransformComponent>> area_knock_back_vector;

  cor::FrameContext &m_context;

  std::vector<cor::EntityHandle> m_pos_force_entities;

  std::vector<cor::EntityHandle> m_ent_force_entities;
  std::vector<cor::EntityHandle> m_hook_entities;

  std::vector<cor::EntityHandle> m_ccc_entities;
  std::vector<cor::EntityHandle> m_dc_entities;
  std::vector<cor::EntityHandle> m_knocked_back_stun_entities;
};
};  // namespace gmp

#endif
