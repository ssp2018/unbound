#pragma once
#ifndef GMP_PARTICLE_SYSTEM_HPP
#define GMP_PARTICLE_SYSTEM_HPP

#include <bse/serialize.hpp>
#include <cor/entity_handle.hpp>
#include <cor/system.hpp>  // for System
#include <gmp/components/particles_component.hpp>

namespace cor {
struct FrameContext;
}

namespace gmp {

// a system for updating particlesystems
class ParticleSystem : public cor::System {
 public:
  virtual ~ParticleSystem(){};

  // update with read only, not used
  void read_update(const cor::FrameContext &context) override;

  // update with read and write
  void write_update(cor::FrameContext &context) override;

  VIRTUAL_CLASS_SERFUN((ParticleSystem)) { /*No-Op*/
  }
  VIRTUAL_CLASS_DESERFUN((ParticleSystem)) { /*No-Op*/
  }

 private:
  // get the first unused particle
  int find_unused_particle(ParticlesComponent &pc);

  // get a random value between min and max
  glm::vec3 get_random_vec3(glm::vec3 min, glm::vec3 max);
  glm::vec4 get_random_vec4(glm::vec4 min, glm::vec4 max);
  float get_random_float(float min, float max);

  std::vector<std::pair<cor::EntityHandle, ParticlesComponent>> m_updated_systems;
};

};  // namespace gmp

#endif
