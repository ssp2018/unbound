#pragma once
#ifndef GMP_REPLAY_SYSTEM_HPP
#define GMP_REPLAY_SYSTEM_HPP

#include <bse/serialize.hpp>
#include <cor/entity_handle.hpp>
#include <cor/frame_context.hpp>
#include <cor/system.hpp>
#include <ext/ext.hpp>

namespace gmp {
// system handling replay camera motions and other effects
class ReplaySystem : public cor::System {
 public:
  ReplaySystem(cor::FrameContext &context);
  virtual ~ReplaySystem();

  void read_update(const cor::FrameContext &context);
  void write_update(cor::FrameContext &context);

  VIRTUAL_CLASS_SERFUN((ReplaySystem)) {
    // SERIALIZE(self.m_letterbox_upper);
    // SERIALIZE(self.m_letterbox_lower);
    // SERIALIZE(self.m_camera);
    // SERIALIZE(self.m_boss);
    // SERIALIZE(self.m_player);
    // SERIALIZE(self.m_arrow);
    // SERIALIZE(self.m_arrow_spawn_frame_num);
    // SERIALIZE(self.m_arrow_final_point);
    // SERIALIZE(self.m_arrow_start_point);
    SERIALIZE(self.m_time_since_last_replay);
    SERIALIZE(self.m_replay_type);
    SERIALIZE(self.m_phase);
  }
  VIRTUAL_CLASS_DESERFUN((ReplaySystem)) {
    // DESERIALIZE(self.m_letterbox_upper);
    // DESERIALIZE(self.m_letterbox_lower);
    // DESERIALIZE(self.m_camera);
    // DESERIALIZE(self.m_boss);
    // DESERIALIZE(self.m_player);
    // DESERIALIZE(self.m_arrow);
    // DESERIALIZE(self.m_arrow_spawn_frame_num);
    // DESERIALIZE(self.m_arrow_final_point);
    // DESERIALIZE(self.m_arrow_start_point);
    DESERIALIZE(self.m_time_since_last_replay);
    DESERIALIZE(self.m_replay_type);
    DESERIALIZE(self.m_phase);
  }

 private:
  void start_sequence(cor::FrameContext &context);
  void end_sequence(cor::FrameContext &context);

 private:
  cor::EntityHandle m_letterbox_upper = cor::EntityHandle::NULL_HANDLE;
  cor::EntityHandle m_letterbox_lower = cor::EntityHandle::NULL_HANDLE;

  cor::EntityHandle m_camera = cor::EntityHandle::NULL_HANDLE;

  cor::EntityHandle m_boss = cor::EntityHandle::NULL_HANDLE;
  cor::EntityHandle m_player = cor::EntityHandle::NULL_HANDLE;
  cor::EntityHandle m_arrow = cor::EntityHandle::NULL_HANDLE;

  uint16_t m_arrow_spawn_frame_num;
  glm::vec3 m_arrow_final_point;
  glm::vec3 m_arrow_start_point;

  enum class Type {
    BOSS_DEATH_REPLAY,
  };

  enum class Phase {  //
    PH0_BOSS_FOCUS,
    PH1_PLAYER_FOCUS,
    PH2_ARROW_FOCUS
  };

  constexpr static std::array<float, 3> PHASE_LENGTHS = {99.f, 6.f, 2.f};

  float m_time_since_last_replay = 5.f;
  Type m_replay_type;
  Phase m_phase = Phase::PH0_BOSS_FOCUS;
};
}  // namespace gmp

//
#endif
