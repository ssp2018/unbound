#pragma once

#ifndef GMP_CLOUD_SYSTEM_HPP
#define GMP_CLOUD_SYSTEM_HPP

#include <array>
#include <bse/asset.hpp>
#include <cor/entity_handle.hpp>
#include <cor/frame_context.hpp>
#include <cor/system.hpp>
#include <gfx/model.hpp>

namespace gmp {

// Is responsible for the cloud layer
class CloudSystem : public cor::System {
 public:
  CloudSystem(cor::FrameContext &context);
  virtual ~CloudSystem();

  // update with read only
  void read_update(const cor::FrameContext &context) override;

  // update with write only
  void write_update(cor::FrameContext &context) override;

  VIRTUAL_CLASS_SERFUN((CloudSystem)) {}
  VIRTUAL_CLASS_DESERFUN((CloudSystem)) {}

 private:
  static constexpr int LAYER_DIMENSIONS = 8;
  static constexpr int LAYER_INSTANCE_SCALE = 256;

  cor::EntityHandle m_camera_handle = cor::EntityHandle::NULL_HANDLE;
  bse::Asset<gfx::Model> m_cloud_layer_model;

  std::array<std::array<cor::EntityHandle, LAYER_DIMENSIONS>, LAYER_DIMENSIONS> m_handles;
};
}  // namespace gmp
#endif  // GMP_CLOUD_SYSTEM_HPP
