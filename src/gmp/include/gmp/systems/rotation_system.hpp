#pragma once
#ifndef GMP_ROTATION_SYSTEM_HPP
#define GMP_ROTATION_SYSTEM_HPP

#include <bse/serialize.hpp>
#include <cor/system.hpp>  // for System

namespace cor {
struct FrameContext;
}
namespace gmp {

// A simple system for rotating objects
class RotationSystem : public cor::System {
 public:

  virtual ~RotationSystem(){};

  // update with read only
  void read_update(const cor::FrameContext &context) override;
  // update
  void write_update(cor::FrameContext &context) override;

  VIRTUAL_CLASS_SERFUN((RotationSystem)) { /*No-Op*/
  }
  VIRTUAL_CLASS_DESERFUN((RotationSystem)) { /*No-Op*/
  }

 private:
};

};  // namespace gmp

#endif
