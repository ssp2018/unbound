#pragma once
#ifndef GMP_HOMING_SYSTEM_HPP
#define GMP_HOMING_SYSTEM_HPP

#include <cor/system.hpp>
#include <bse/serialize.hpp>

namespace gmp {

// System for handling homing of entities
class HomingSystem : public cor::System {
 public:
 
  virtual ~HomingSystem(){};

  // Update with read only
  void read_update(const cor::FrameContext &context) override;

  // Update with write only
  void write_update(cor::FrameContext &context) override;

  VIRTUAL_CLASS_SERFUN((HomingSystem)) {
    /*No-Op*/
  }
  VIRTUAL_CLASS_DESERFUN((HomingSystem)) {
    /*No-Op*/
  }
};

}  // namespace gmp

#endif  // GMP_HOMING_SYSTEM_HPP
