#pragma once
#ifndef GMP_HEALTH_SYSTEM_HPP
#define GMP_HEALTH_SYSTEM_HPP

#include <bse/asset.hpp>
#include <bse/serialize.hpp>
#include <cor/event_manager.hpp>
#include <cor/system.hpp>
#include <ext/ext.hpp>
#include <scr/object.hpp>
#include <scr/script.hpp>

namespace gmp {
// system handling health and damage components
class HealthSystem : public cor::System {
 public:
  HealthSystem(cor::FrameContext &context);

  virtual ~HealthSystem(){};

  void read_update(const cor::FrameContext &context);
  void write_update(cor::FrameContext &context);

  VIRTUAL_CLASS_SERFUN((HealthSystem)) { /*No-Op*/
  }
  VIRTUAL_CLASS_DESERFUN((HealthSystem)) { /*No-Op*/
  }

 private:
  // subscribe to events
  void event_subscription(cor::FrameContext &context);
};
}  // namespace gmp

#endif
