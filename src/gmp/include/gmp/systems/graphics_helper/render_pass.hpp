#pragma once
#ifndef GMP_RENDER_PASS_HPP
#define GMP_RENDER_PASS_HPP

#include <bse/allocators.hpp>

namespace gmp {

// a base class for all renderpasses, used for allowing the passes to be looped through
class RenderPass {
 public:
  RenderPass(float width, float height) {
    m_width = width;
    m_height = height;
  }

  virtual ~RenderPass() = default;

  // allocate what is needed from the stack
  virtual void prepare_for_frame() = 0;

  // sort the rendercalls
  virtual void sort() = 0;
  // submit the rendercalls
  virtual void submit() = 0;
  // reset the pass to make it ready for a new frame
  virtual void clear() = 0;
  // swap front and back buffer queues
  virtual void swap() { std::swap(m_base_front, m_base_back); }

 protected:
  float m_width;
  float m_height;

  struct BaseQueue {
    bool active = false;
  };

  BaseQueue m_base_front;
  BaseQueue m_base_back;
};

};  // namespace gmp

#endif
