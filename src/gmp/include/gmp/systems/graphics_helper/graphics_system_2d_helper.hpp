#pragma once
#ifndef GMP_GRAPHICS_SYSTEM_2D_HELPER_HPP
#define GMP_GRAPHICS_SYSTEM_2D_HELPER_HPP

namespace bse {
template <typename T>
class RuntimeAsset;
}
namespace cor {
struct FrameContext;
}
namespace gfx {
class CommandBucket;
}
namespace gfx {
class Key;
}  // namespace gfx
namespace gfx {
class TextCache;
}  // namespace gfx
namespace gmp {
struct GraphicTextComponent;
}
namespace gmp {
struct Transform2dComponent;
}

namespace gfx {
class Key;
class TextCache;
}  // namespace gfx

namespace gmp {

// add 2d transform to command key, return latest command
// append if previous_command is not null
void* add_2d_transform(const gfx::Key& key, void* previous_command,
                       const Transform2dComponent& transform, gfx::CommandBucket* gbuffer_bucket);

// add graphic text to command key, return latest command
// append if previous_command is not null
void* add_graphic_text(const gfx::Key& key, void* previous_command,
                       const Transform2dComponent& transform, const GraphicTextComponent& text,
                       gfx::CommandBucket* gbuffer_bucket,
                       bse::RuntimeAsset<gfx::TextCache>& text_cache);

// render 2d content
void add_2d(const cor::FrameContext& context, gfx::CommandBucket* gbuffer_bucket,
            bse::RuntimeAsset<gfx::TextCache>& text_cache);
};  // namespace gmp

#endif
