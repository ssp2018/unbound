#pragma once
#ifndef GMP_CSM_PASS_HPP
#define GMP_CSM_PASS_HPP

#include "gmp/systems/graphics_helper/render_pass.hpp"  // for RenderPass
#include <cor/entity.hpp>
#include <gfx/buckets/shadow_bucket.hpp>  // for ShadowBucket
#include <gfx/uniform_buffer.hpp>
#include <gmp/components/transform_component.hpp>
#include <gmp/systems/animation_system.hpp>
namespace bse {
class StackAllocator;
}
namespace cor {
struct FrameContext;
}

namespace gmp {

// a renderpass for rendering cascaded shadow maps
class CSMPass : public RenderPass {
 public:
  CSMPass(bse::StackAllocator* front, bse::StackAllocator* back, float size);
  virtual ~CSMPass() = default;

  // allocate what is needed from the stack
  void prepare_for_frame() override;
  // sort the rendercalls
  void sort() override;
  // submit the rendercalls
  void submit() override;
  // reset the pass to make it ready for a new frame
  void clear() override;
  // swap front and back buffer queues
  void swap() override;

  // populate this pass with framedata
  void populate_frame(const cor::FrameContext& context, const glm::mat4& p_matrix,
                      const glm::mat4& v_matrix, const glm::vec3& direction,
                      const glm::vec3& cam_pos);

  void set_splits(const std::vector<float>& splits);

  // return a pointer to the shadow_matrices
  float* get_shadow_matrices();

  float* get_shadow_depths();

  // get a single shadow_matrix
  const glm::mat4& get_matrix(int index) const;
  // return the shadow texture at index
  unsigned int get_texture(int index);

 private:
  struct EntityData {
    cor::ConstEntity entity;
    const gmp::TransformComponent* tc;
    const gmp::ModelComponent* mc;
    gfx::AABB aabb;
    std::vector<glm::mat4> matrices;
    int ubo_index;
  };

  struct Queue {
    std::vector<glm::mat4> matrices;
    std::vector<float> depths;
    std::vector<float> splits;
    bool do_render_world = true;
    std::array<std::map<int, EntityData>, 5> ubo_entities;
    std::array<gfx::UniformBuffer, 500> matrix_ubos;
  };

  std::vector<std::unique_ptr<gfx::ShadowBucket>> m_buckets;

  std::unique_ptr<Queue> m_front;
  std::unique_ptr<Queue> m_back;

  size_t m_world_index;
  bse::Timer m_world_timer;
  float m_time_since_world_render = 9999.f;
};

};  // namespace gmp

#endif
