#pragma once
#ifndef GMP_GOD_RAY_PASS_HPP
#define GMP_GOD_RAY_PASS_HPP

#include "bse/asset.hpp"                                // for Asset
#include "gmp/systems/graphics_helper/render_pass.hpp"  // for RenderPass
#include <gfx/framebuffer.hpp>                          // for FullFramePass
#include <gfx/full_frame_gaussian_pass.hpp>             // for FullFrameGaus...
#include <gfx/full_frame_pass.hpp>                      // for Framebuffer
namespace bse {
class StackAllocator;
}
namespace gfx {
class Shader;
}

namespace gmp {

// a pass for generating a ssao texture
class GodRayPass : public RenderPass {
 public:
  GodRayPass(bse::StackAllocator* front, bse::StackAllocator* back, float width, float height);
  virtual ~GodRayPass() = default;

  // allocate what is needed from the stack
  void prepare_for_frame() override;
  // sort the rendercalls
  void sort() override;
  // submit the rendercalls
  void submit() override;
  // reset the pass to make it ready for a new frame
  void clear() override;
  // swap front and back buffer queues
  void swap() override;

  // set the fbo to use for stencil testing
  void set_stencil_fbo(unsigned int stencil_fbo, float stencil_width, float stencil_height);

  gfx::FrameBuffer* get_fbo();

  // set the depth texture and return the resulting texture
  unsigned int populate_frame(unsigned int shadow_texture_0, unsigned int shadow_texture_1,
                              unsigned int shadow_texture_2, unsigned int depth_texture,
                              const float* shadow_matrices, const glm::mat4& vp_matrix,
                              const glm::vec3& light_color, const glm::vec3& cam_pos,
                              bool inside_godray, bool stencil_active);

 private:
  struct Queue {
    unsigned int stencil;
    float stencil_width;
    float stencil_height;
    bool inside_godray;
    bool stencil_active;
  };

  std::unique_ptr<gfx::FullFramePass> m_god_ray_pass;
  std::unique_ptr<gfx::FrameBuffer> m_god_ray_framebuffer;
  std::unique_ptr<gfx::FrameBuffer> m_gaussian_framebuffer;
  std::unique_ptr<gfx::FullFrameGaussianPass> m_gaussian_pass;
  unsigned int m_texture;
  unsigned int m_gaussian_texture;

  Queue m_front;
  Queue m_back;
};

};  // namespace gmp

#endif
