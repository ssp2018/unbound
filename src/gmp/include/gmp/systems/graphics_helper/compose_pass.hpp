#pragma once
#ifndef GMP_COMPOSE_PASS_HPP
#define GMP_COMPOSE_PASS_HPP
#include "bse/asset.hpp"                                // for Asset
#include "gmp/systems/graphics_helper/render_pass.hpp"  // for RenderPass
#include <gfx/framebuffer.hpp>                          // for Framebuffer
#include <gfx/full_frame_pass.hpp>                      // for FullFramePass
namespace bse {
class StackAllocator;
}
namespace gfx {
class Shader;
}
namespace cor {
struct FrameContext;
}

namespace gmp {

// a pass for applying radial blur on a texture
class ComposePass : public RenderPass {
 public:
  ComposePass(bse::StackAllocator* front, bse::StackAllocator* back, float width, float height);
  virtual ~ComposePass();

  // allocate what is needed from the stack
  void prepare_for_frame() override;
  // sort the rendercalls
  void sort() override;
  // submit the rendercalls
  void submit() override;
  // reset the pass to make it ready for a new frame
  void clear() override;
  // swap front and back buffer queues
  void swap() override;

  // populate frame. Checking for saturation value
  void populate_frame(const cor::FrameContext& context, const glm::vec3& directional_light_color);

  // set the texture to be modified and return the resulting texture
  void set_texture(unsigned int texture, std::string name);

  // get the texture
  unsigned int get_texture();

  // set the active framebuffer
  void set_framebuffer(gfx::FrameBuffer* framebuffer);

  // set radius for radial blur.
  void set_radial_blur_radius(float radius);

 private:
  struct Queue {
    std::unordered_map<std::string, unsigned int> textures;
    gfx::FrameBuffer* framebuffer = nullptr;
    float radial_blur_radius = 0.f;
  };

  Queue m_front;
  Queue m_back;

  std::unique_ptr<gfx::FullFramePass> m_full_frame_pass;

  bse::Asset<gfx::Shader> m_shader;
  std::unique_ptr<gfx::FrameBuffer> m_owned_framebuffer;
  unsigned int m_texture;
};

};  // namespace gmp

#endif
