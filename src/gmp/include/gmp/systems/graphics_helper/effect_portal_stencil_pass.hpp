#pragma once
#ifndef GMP_EFFECT_PORTAL_STENCIL_PASS_HPP
#define GMP_EFFECT_PORTAL_STENCIL_PASS_HPP
#include "gmp/systems/graphics_helper/effect_area_manager.hpp"  // for EffectAreaManager
#include "gmp/systems/graphics_helper/render_pass.hpp"          // for RenderPass
#include <cor/frame_context.hpp>                                // for FrameContext
#include <ext/ext.hpp>                                          // for Common includes
#include <gfx/command_bucket.hpp>                               // for CommandBucket
#include <gfx/framebuffer.hpp>                                  // for FrameBuffer
#include <gfx/model.hpp>                                        // for Model
#include <gmp/components/projection_component.hpp>              // for ProjectionComponent

namespace gmp {

// a pass for creating a stencilbuffer for effect portals
class EffectPortalStencilPass : public RenderPass {
 public:
  EffectPortalStencilPass(bse::StackAllocator* front, bse::StackAllocator* back, float width,
                          float height);
  virtual ~EffectPortalStencilPass() = default;

  // allocate what is needed from the stack
  void prepare_for_frame() override;
  // sort the rendercalls
  void sort() override;
  // submit the rendercalls
  void submit() override;
  // reset the pass to make it ready for a new frame
  void clear() override;
  // swap front and back buffer queues
  void swap() override;

  // return the fbo id
  unsigned int get_fbo();

  // set the fbo to use for writing
  void set_fbo(gfx::FrameBuffer* other);

  // populate this pass with framedata
  void populate_frame(const cor::FrameContext& context, const ProjectionComponent& camera,
                      const glm::mat4& v_matrix, const std::vector<Portal>& active_portals,
                      bse::Flags<LightEffects::COUNT>& active_effects);

 private:
  struct Queue {
    gfx::FrameBuffer* other_buffer = nullptr;
  };

  Queue m_front;
  Queue m_back;

  std::unique_ptr<gfx::CommandBucket> m_stencil_bucket;
  std::unique_ptr<gfx::FrameBuffer> m_framebuffer;

  // render vars
  unsigned int m_model_matrix_location;
  unsigned int m_view_projection_location;
  bse::Asset<gfx::Shader> m_shader;
  bse::Asset<gfx::Model> m_model;
};

};  // namespace gmp

#endif
