#pragma once
#ifndef GMP_EFFECT_AREA_HPP
#define GMP_EFFECT_AREA_HPP

#include "gmp/systems/graphics_helper/utilities.hpp"
#include <bse/serialize.hpp>
#include <ext/ext.hpp>
#include <gfx/aabb.hpp>
#include <gfx/frustum_shape.hpp>

namespace gmp {

// a struct for storing portal information
struct Portal {
  gfx::AABB aabb;
  bse::Flags<LightEffects::COUNT> flags;
  float rotation_z;

  CLASS_SERFUN((gmp::Portal)) {
    SERIALIZE(self.aabb);
    SERIALIZE(self.flags);
  }
  CLASS_DESERFUN((gmp::Portal)) {
    DESERIALIZE(self.aabb);
    DESERIALIZE(self.flags);
  }
};

// a struct for storing data of an area
struct EffectArea {
  gfx::AABB aabb;
  bse::Flags<LightEffects::COUNT> flags;
  std::vector<Portal> portals;

  glm::vec2 center;
  float radius;
  float low;
  float high;

  // get all portals in the camerafrustum
  std::vector<Portal> get_portal_effects(const gfx::FrustumShape &camera_frustum);

  CLASS_SERFUN((gmp::EffectArea)) {
    SERIALIZE(self.aabb);
    SERIALIZE(self.flags);
    SERIALIZE(self.portals);
  }
  CLASS_DESERFUN((gmp::EffectArea)) {
    DESERIALIZE(self.aabb);
    DESERIALIZE(self.flags);
    DESERIALIZE(self.portals);
  }
};

}  // namespace gmp

namespace gmp {

// A class for handling different effect areas and selecting between them.
class EffectAreaManager {
 public:
  EffectAreaManager();

  // get the area which position is inside
  const EffectArea get_effect_area(glm::vec3 position, glm::vec3 direction);
  // get all effects present in at least this area or a portal
  bse::Flags<LightEffects::COUNT> get_all_combined_effects(bse::Flags<LightEffects::COUNT> flags,
                                                           std::vector<Portal> active_portals);
  // get all effects that is only avaliable in a portal and not the current area
  bse::Flags<LightEffects::COUNT> needs_stencil_flags(bse::Flags<LightEffects::COUNT> flags,
                                                      std::vector<Portal> active_portals);

  CLASS_SERFUN((EffectAreaManager)) {
    SERIALIZE(self.m_effect_areas);
    SERIALIZE(self.m_default_area);
  }
  CLASS_DESERFUN((EffectAreaManager)) {
    DESERIALIZE(self.m_effect_areas);
    DESERIALIZE(self.m_default_area);
  }

 private:
  std::vector<EffectArea> m_effect_areas;
  EffectArea m_default_area;
  bse::Flags<LightEffects::COUNT> m_default_flags;
};

};  // namespace gmp

#endif
