#pragma once
#ifndef GMP_GRAPHICS_HELPER_UTILITIES_HPP
#define GMP_GRAPHICS_HELPER_UTILITIES_HPP

#include <cor/entity.hpp>
#include <ext/ext.hpp>
#include <gfx/mesh.hpp>

namespace gmp {
class AnimationSystem;
class ModelComponent;
}  // namespace gmp

namespace gmp {
// Get the correct VAO from a model, taking animation into account
unsigned int get_mesh_vao(cor::ConstEntity entity, const bse::Asset<gfx::Mesh>& mesh,
                          gfx::VertexFlags flags);

enum LightEffects { DIRECTIONAL_LIGHT, DIRECTIONAL_SHADOWS, GOD_RAYS, SSAO, GLOW, COUNT };

}  // namespace gmp

#endif  // GMP_GRAPHICS_HELPER_UTILITIES_HPP
