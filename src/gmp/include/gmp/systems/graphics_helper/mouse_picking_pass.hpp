#pragma once
#ifndef GMP_MOUSE_PICKING_PASS_HPP
#define GMP_MOUSE_PICKING_PASS_HPP
#include "cor/entity_handle.hpp"                        // for EntityHandle
#include "gmp/systems/graphics_helper/render_pass.hpp"  // for RenderPass
#include <bse/runtime_asset.hpp>                        // for RuntimeAsset
#include <gfx/command_bucket.hpp>                       // for CommandBucket
#include <gfx/framebuffer.hpp>                          // for Framebuffer
#include <gfx/text_cache.hpp>                           // for TextCache
#include <gfx/uniform_buffer.hpp>

namespace bse {
class StackAllocator;
}
namespace cor {
struct FrameContext;
}
namespace gfx {
class FrustumShape;
}
namespace gmp {
struct ProjectionComponent;
}

namespace gmp {

// the main render pass that renders data for mousepicking
class MousePickingPass : public RenderPass {
 public:
  MousePickingPass(bse::StackAllocator* front, bse::StackAllocator* back, float width,
                   float height);
  virtual ~MousePickingPass() = default;

  // allocate what is needed from the stack
  void prepare_for_frame() override;
  // sort the rendercalls
  void sort() override;
  // submit the rendercalls
  void submit() override;
  // reset the pass to make it ready for a new frame
  void clear() override;
  // swap front and back buffer queues
  void swap() override;

  // return the texture by name
  unsigned int get_texture(std::string name);
  // get the fbo handle
  unsigned int get_fbo();

  // populate this pass with framedata
  void populate_frame(const cor::FrameContext& context, const ProjectionComponent& camera,
                      const glm::mat4& v_matrix, const glm::vec3& cam_pos);

 private:
  struct UBOData {
    glm::mat4 view_matrix;
    glm::mat4 projection_matrix;
  } m_ubo_data;
  gfx::UniformBuffer m_uniform_buffer;

  std::unique_ptr<gfx::CommandBucket> m_command_bucket;
  std::unique_ptr<gfx::FrameBuffer> m_framebuffer;
  unsigned int m_entity_handle_texture;
  unsigned int m_world_pos_texture;
  unsigned int m_normal_texture;
  bse::Asset<gfx::Shader> m_shader;
  unsigned int m_model_matrix_location;
  unsigned int m_color_location;

  // private function for adding the models to renderer
  void add_models(const cor::FrameContext& context, gfx::FrustumShape& frustum_shape,
                  const glm::vec3& cam_pos);
};

};  // namespace gmp

#endif
