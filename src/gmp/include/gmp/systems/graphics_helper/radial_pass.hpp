#pragma once
#ifndef GMP_RADIAL_PASS_HPP
#define GMP_RADIAL_PASS_HPP
#include "bse/asset.hpp"                                // for Asset
#include "gmp/systems/graphics_helper/render_pass.hpp"  // for RenderPass
#include <gfx/full_frame_pass.hpp>                      // for FullFramePass
namespace bse {
class StackAllocator;
}
namespace gfx {
class Shader;
class FrameBuffer;
}  // namespace gfx
namespace gmp {

// a pass for applying radial blur on a texture
class RadialPass : public RenderPass {
 public:
  RadialPass(bse::StackAllocator* front, bse::StackAllocator* back, float width, float height);
  virtual ~RadialPass() = default;

  // allocate what is needed from the stack
  void prepare_for_frame() override;
  // sort the rendercalls
  void sort() override;
  // submit the rendercalls
  void submit() override;
  // reset the pass to make it ready for a new frame
  void clear() override;
  // swap front and back buffer queues
  void swap() override;

  // set the texture to be modified and return the resulting texture
  unsigned int set_texture(unsigned int texture);

  // set the blur radius
  void set_blur_radius(float blur);

  // set a fbo to write to
  void set_framebuffer(gfx::FrameBuffer* framebuffer);

 private:
  std::unique_ptr<gfx::FullFramePass> m_full_frame_pass;
  bse::Asset<gfx::Shader> m_shader;

  struct Queue {
    unsigned int in_texture;
    float blur = 0.005;
    gfx::FrameBuffer* framebuffer;
  };

  Queue m_front;
  Queue m_back;
};

};  // namespace gmp

#endif
