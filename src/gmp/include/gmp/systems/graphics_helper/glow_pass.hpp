#pragma once
#ifndef GMP_GLOW_PASS_HPP
#define GMP_GLOW_PASS_HPP

#include "bse/asset.hpp"                                // for Asset
#include "gmp/systems/graphics_helper/render_pass.hpp"  // for RenderPass
#include <gfx/framebuffer.hpp>                          // for FullFramePass
#include <gfx/full_frame_gaussian_pass.hpp>             // for FullFrameGaus...
#include <gfx/full_frame_pass.hpp>                      // for Framebuffer
namespace bse {
class StackAllocator;
}
namespace gfx {
class Shader;
}

namespace gmp {

// a pass for generating glow
class GlowPass : public RenderPass {
 public:
  GlowPass(bse::StackAllocator* front, bse::StackAllocator* back, float width, float height);
  virtual ~GlowPass() = default;

  // allocate what is needed from the stack
  void prepare_for_frame() override;
  // sort the rendercalls
  void sort() override;
  // submit the rendercalls
  void submit() override;
  // reset the pass to make it ready for a new frame
  void clear() override;
  // swap front and back buffer queues
  void swap() override;

  // set the texture to be blurred
  unsigned int set_texture(unsigned int in_texture, unsigned int m_stencil_buffer, bool inside_glow,
                           bool stencil_active, float stencil_width, float stencil_height);

 private:
  struct Queue {
    unsigned int stencil;
    unsigned int in_texture;
    float stencil_width;
    float stencil_height;
    bool inside_glow;
    bool stencil_active;
    float blur_radius = 0.001;
  };

  std::unique_ptr<gfx::FrameBuffer> m_internal_framebuffer;
  std::unique_ptr<gfx::FrameBuffer> m_gaussian_framebuffer;
  unsigned int m_internal_texture;
  unsigned int m_gaussian_texture;

  Queue m_front;
  Queue m_back;
};

};  // namespace gmp

#endif
