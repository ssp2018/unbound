#pragma once
#ifndef GMP_HUD_PASS_HPP
#define GMP_HUD_PASS_HPP
#include "gmp/systems/graphics_helper/render_pass.hpp"  // for RenderPass
#include <bse/runtime_asset.hpp>                        // for RuntimeAsset
#include <gfx/command_bucket.hpp>                       // for CommandBucket
#include <gfx/framebuffer.hpp>
#include <gfx/text_cache.hpp>  // for TextCache

namespace bse {
class StackAllocator;
}
namespace cor {
struct FrameContext;
}

namespace gmp {

// a pass for rendering the HUD
class HUDPass : public RenderPass {
 public:
  HUDPass(bse::StackAllocator* front, bse::StackAllocator* back, float width, float height);
  virtual ~HUDPass() = default;

  // allocate what is needed from the stack
  void prepare_for_frame() override;
  // sort the rendercalls
  void sort() override;
  // submit the rendercalls
  void submit() override;
  // reset the pass to make it ready for a new frame
  void clear() override;
  // swap front and back buffer queues
  void swap() override;

  // fill the frame with data
  void populate_frame(const cor::FrameContext& context);

  // set the active framebuffer
  void set_framebuffer(gfx::FrameBuffer* framebuffer);

 private:
  struct Queue {
    gfx::FrameBuffer* framebuffer = nullptr;
  };

  std::unique_ptr<gfx::CommandBucket> m_command_bucket;
  bse::RuntimeAsset<gfx::TextCache> m_text_cache;  // may cause problems if not double-buffered,
                                                   // could solve it internally in TextCache maybe

  Queue m_front;
  Queue m_back;
};

};  // namespace gmp

#endif
