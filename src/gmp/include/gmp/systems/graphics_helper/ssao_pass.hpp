#pragma once
#ifndef GMP_SSAO_PASS_HPP
#define GMP_SSAO_PASS_HPP

#include "bse/asset.hpp"  // for Asset
#include "gfx/full_frame_gaussian_pass.hpp"
#include "gfx/full_frame_ssao_pass.hpp"
#include "gmp/systems/graphics_helper/render_pass.hpp"  // for RenderPass
namespace bse {
class StackAllocator;
}
namespace gfx {
class Shader;
}

namespace gmp {

// a pass for generating a ssao texture
class SSAOPass : public RenderPass {
 public:
  SSAOPass(bse::StackAllocator* front, bse::StackAllocator* back, float width, float height);
  virtual ~SSAOPass() = default;

  // allocate what is needed from the stack
  void prepare_for_frame() override;
  // sort the rendercalls
  void sort() override;
  // submit the rendercalls
  void submit() override;
  // reset the pass to make it ready for a new frame
  void clear() override;
  // swap front and back buffer queues
  void swap() override;

  // set the depth texture and return the resulting texture
  unsigned int populate_frame(unsigned int depth_texture, const glm::mat4& p_matrix);

 private:
  std::unique_ptr<gfx::FullFrameSSAOPass> m_ssao_pass;
  std::unique_ptr<gfx::FullFrameGaussianPass> m_gaussian_pass;
};

};  // namespace gmp

#endif
