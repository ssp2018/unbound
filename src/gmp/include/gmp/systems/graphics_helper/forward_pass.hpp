#pragma once
#ifndef GMP_FORWARD_PASS_HPP
#define GMP_FORWARD_PASS_HPP
#include "cor/entity_handle.hpp"                        // for EntityHandle
#include "gmp/systems/graphics_helper/render_pass.hpp"  // for RenderPass
#include "gmp/systems/graphics_helper/utilities.hpp"    // for LightEffects
#include <bse/runtime_asset.hpp>                        // for RuntimeAsset
#include <gfx/buckets/gbuffer_bucket.hpp>               // for GBufferBucket
#include <gfx/framebuffer.hpp>                          // for FrameBuffer
#include <gfx/particle_system_manager.hpp>              // for UniformBuffer
#include <gfx/text_cache.hpp>                           // for TextCache
#include <gfx/uniform_buffer.hpp>                       // for UniformBuffer

namespace bse {
class StackAllocator;
}
namespace cor {
struct FrameContext;
}
namespace gfx {
class FrustumShape;
class Model;
class InstancedMesh;
}  // namespace gfx
namespace gmp {
struct ProjectionComponent;
class AnimationSystem;
class TransformComponent;
class ShieldEffectComponent;
}  // namespace gmp

namespace gmp {

struct ModelData {
  cor::ConstEntity entity;
  const gmp::TransformComponent* tc;
  const gmp::ModelComponent* mc;
  gfx::AABB aabb;
};

// the main render pass that writes to the gbuffers
class ForwardPass : public RenderPass {
 public:
  ForwardPass(bse::StackAllocator* front, bse::StackAllocator* back, float width, float height);
  virtual ~ForwardPass() = default;

  // allocate what is needed from the stack
  void prepare_for_frame() override;
  // sort the rendercalls
  void sort() override;
  // submit the rendercalls
  void submit() override;
  // reset the pass to make it ready for a new frame
  void clear() override;
  // swap front and back buffer queues
  void swap() override;

  // update particle buffers in readupdate
  void read_update(cor::FrameContext& context);

  // return the texture by name
  unsigned int get_texture(std::string name);

  gfx::FrameBuffer* get_fbo();

  // populate this pass with framedata
  void populate_frame(const cor::FrameContext& context, const ProjectionComponent& camera,
                      const glm::mat4& v_matrix, const glm::vec3& cam_pos,
                      std::vector<unsigned int>& shadow_textures, float* shadow_matrices,
                      float* shadow_depths, bse::Flags<LightEffects::COUNT> active_effects,
                      glm::vec3& dir_light_color, glm::vec3& dir_light_direction);

  // set the frustum splits used in the shadowmap
  void set_splits(float frustum_1, float frustum_2, float frustum_3);

  void set_texture(unsigned int texture, std::string name);

  std::unique_ptr<gfx::GBufferBucket> m_gbuffer_bucket;

 private:
  struct PointLightStruct {
    glm::vec3 position;
    glm::vec3 color;
    float radius;
  };

  struct UBOData {
    glm::mat4 view_matrix;
    glm::mat4 projection_matrix;
  };

  struct ModelMatricesUBOData {
    glm::mat4 matirces[350];
  };

  struct UBODataFragment {
    alignas(16) glm::vec3 frustum_limits;
    alignas(16) glm::vec3 bias;
    alignas(16) glm::vec3 dir_light_color;
    alignas(16) glm::vec3 dir_light_direction;
    alignas(16) glm::ivec2 bools;  // x = shadows, y = shadows
    alignas(16) glm::vec3 cam_pos;
    alignas(16) glm::vec2 windows_size;
    alignas(16) float time;
  };

  // a method for handling texture binds efficeently and decrease rebinds
  void* add_texture_to_bucket(unsigned int texture, unsigned int location, void* latest_command);

  // cull all models with frustum and return the models that are visible
  std::vector<ModelData> get_culled_models(const cor::FrameContext& context,
                                           gfx::FrustumShape& frustum_shape,
                                           const glm::vec3& cam_pos);

  // cull all point lights with frustum and return the pointlights that are visible
  std::vector<PointLightStruct> get_culled_point_lights(const cor::FrameContext& context,
                                                        gfx::FrustumShape& frustum_shape);

  // generate the commands for pointlights and add them to the models command queue
  void* add_pointlight_commands(std::vector<PointLightStruct>& point_lights_in_scene, ModelData& md,
                                const glm::vec3& cam_pos, void* previous_command, bool cull);

  // add wireframe rendering for model
  void ForwardPass::add_wireframe_to_bucket(ModelData& md, const glm::vec3& cam_pos);

  // add shield rendering for model
  void add_shield_to_bucket(const cor::FrameContext& context, ModelData& md,
                            const glm::vec3& cam_pos, const ShieldEffectComponent* sec);

  // private function for adding the models to renderer
  void add_models(const cor::FrameContext& context, gfx::FrustumShape& frustum_shape,
                  const glm::vec3& cam_pos, std::vector<unsigned int>& shadow_textures,
                  float* shadow_matrices, float* shadow_depths,
                  bse::Flags<LightEffects::COUNT> active_effects);

  // private function for adding the skybox to renderer
  void add_skybox(const cor::FrameContext& context, const ProjectionComponent& camera,
                  const glm::mat4& camera_transform);

  // private function for adding the ropes to renderer
  void add_ropes(const cor::FrameContext& context);

  // private function for adding the text to renderer
  void add_text(const cor::FrameContext& context, const glm::mat4& view_matrix,
                const glm::mat4& projection_matrix);

  // add particle systems for rendering
  void add_particle_systems(const cor::FrameContext& context, gfx::CommandBucket* gbuffer_bucket,
                            const glm::mat4& inv_camera_transform, const glm::vec3& cam_pos,
                            std::vector<cor::EntityHandle>& particle_systems_to_create);

  // framebuffer and textures
  std::unique_ptr<gfx::FrameBuffer> m_framebuffer;
  std::unique_ptr<gfx::FrameBuffer> m_framebuffer_small;
  unsigned int m_depth_texture;
  unsigned int m_depth_texture_small;
  unsigned int m_color_texture;
  unsigned int m_normal_texture;
  unsigned int m_position_texture;
  unsigned int m_glow_texture;
  unsigned int m_ssao_texture;

  float m_time = 0;

  std::vector<cor::EntityHandle> m_particle_systems_to_create;
  bse::RuntimeAsset<gfx::TextCache> m_text_cache;

  gfx::UniformBuffer m_uniform_buffer;
  gfx::UniformBuffer m_fragment_uniform_buffer;

  bse::Asset<gfx::Shader> m_skybox_shader;
  bse::Asset<gfx::Model> m_skybox_model;
  bse::Asset<gfx::Texture> m_skybox_texture;

  bse::Asset<gfx::Shader> m_billboard_shader;
  bse::Asset<gfx::Shader> m_text_shader;

  gfx::ParticleSystemManager m_particle_system_manager;
  std::unique_ptr<gfx::Texture> m_plane_texture;

  UBOData m_ubo_data;
  UBODataFragment m_ubo_fragment_data;

  struct Queue {
    bool depth_prepass_done = false;
    std::vector<glm::vec3> point_light_positions;
    std::vector<glm::vec3> point_light_colors;
    std::vector<float> point_light_radius;
    std::array<unsigned int, 20> bound_textures;
    std::array<int, 20> last_used_texture;
    std::array<gfx::UniformBuffer, 100> matrix_ubos;
    std::array<std::vector<glm::mat4>, 100> matrix_data;
    std::array<std::set<int>, 100> ubo_point_lights;

    int used_ubos = 0;
  };

  Queue m_front;
  Queue m_back;
  int m_last_used = 0;
};

};  // namespace gmp

#endif
