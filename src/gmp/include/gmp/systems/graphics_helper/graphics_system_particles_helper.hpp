#pragma once
#ifndef GMP_GRAPHICS_SYSTEM_PARTICLES_HELPER_HPP
#define GMP_GRAPHICS_SYSTEM_PARTICLES_HELPER_HPP
namespace cor {
struct EntityHandle;
}
namespace cor {
struct FrameContext;
}
namespace gfx {
class CommandBucket;
}

namespace gmp {
// add the particle systems to the bucket supplied
void add_particle_systems(const cor::FrameContext& context, gfx::CommandBucket* gbuffer_bucket,
                          const glm::mat4& inv_camera_transform, const glm::vec3& cam_pos,
                          std::vector<cor::EntityHandle>& particle_systems_to_create);
};  // namespace gmp

#endif
