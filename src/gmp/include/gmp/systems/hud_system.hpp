#pragma once
#ifndef HUD_SYSTEM_HPP
#define HUD_SYSTEM_HPP

#include "cor/entity_handle.hpp"  // for EntityHandle
#include "gmp/components/graphic_text_component.hpp"
#include <bse/serialize.hpp>
#include <cor/system.hpp>  // for System
#include <gfx/texture.hpp>

namespace cor {
struct FrameContext;
}

namespace gmp {

// System for in game hud
class HudSystem : public cor::System {
 public:
  HudSystem(cor::FrameContext &context);

  virtual ~HudSystem(){};

  // update with read only
  void read_update(const cor::FrameContext &context) override;

  // update with write only
  void write_update(cor::FrameContext &context) override;

  VIRTUAL_CLASS_SERFUN((HudSystem)) {
    SERIALIZE(self.m_heart_entities);
    SERIALIZE(self.m_enemy_heart_entities);
    SERIALIZE(self.m_crosshair_handle);
    SERIALIZE(self.m_container_handle);
    SERIALIZE(self.m_stamina_handle);
    SERIALIZE(self.m_stamina_container_handle);
    SERIALIZE(self.m_boss_container_handle);
    SERIALIZE(self.m_selected_arrow_handle);
    SERIALIZE(self.m_piercing_arrow_handle);
    SERIALIZE(self.m_explosive_arrow_handle);
    SERIALIZE(self.m_charge_handle);
    SERIALIZE(self.m_death_screen_handle);
    SERIALIZE(self.m_direction_indicator_handle);
    SERIALIZE(self.m_max_health);
    SERIALIZE(self.m_opponent_max_health);
  }
  VIRTUAL_CLASS_DESERFUN((HudSystem)) {
    DESERIALIZE(self.m_heart_entities);
    DESERIALIZE(self.m_enemy_heart_entities);
    DESERIALIZE(self.m_crosshair_handle);
    DESERIALIZE(self.m_container_handle);
    DESERIALIZE(self.m_stamina_handle);
    DESERIALIZE(self.m_stamina_container_handle);
    DESERIALIZE(self.m_boss_container_handle);
    DESERIALIZE(self.m_selected_arrow_handle);
    DESERIALIZE(self.m_piercing_arrow_handle);
    DESERIALIZE(self.m_explosive_arrow_handle);
    DESERIALIZE(self.m_charge_handle);
    DESERIALIZE(self.m_death_screen_handle);
    DESERIALIZE(self.m_direction_indicator_handle);
    DESERIALIZE(self.m_max_health);
    DESERIALIZE(self.m_opponent_max_health);
  }

 private:
  // Vector holding the hearts showing health
  std::vector<cor::EntityHandle> m_heart_entities;
  // Vector holding the hearts showing health
  std::vector<cor::EntityHandle> m_enemy_heart_entities;
  // Crosshair handle
  cor::EntityHandle m_crosshair_handle;
  // Container handle
  cor::EntityHandle m_container_handle;
  // Stamina handle
  cor::EntityHandle m_stamina_handle;
  // Stamina container handle
  cor::EntityHandle m_stamina_container_handle;
  // Boss container handle
  cor::EntityHandle m_boss_container_handle;
  // Boss container handle
  cor::EntityHandle m_boss_name_handle;
  // Selected arrow handle
  cor::EntityHandle m_selected_arrow_handle;
  // Piercing arrow handle
  cor::EntityHandle m_piercing_arrow_handle;
  // Explosive arrow handle
  cor::EntityHandle m_explosive_arrow_handle;
  // Normal explosive arrow texture
  bse::Asset<gfx::Texture> m_explosive_arrow_texture;
  // Grayed explosive arrow texture
  bse::Asset<gfx::Texture> m_grayed_explosive_arrow_texture;
  // Arrow count container
  cor::EntityHandle m_explosive_arrow_count_container_handle;
  cor::EntityHandle m_explosive_arrow_count_handle;
  // Charge circle handle
  cor::EntityHandle m_charge_handle;
  // Death screen handle
  cor::EntityHandle m_death_screen_handle;
  // Direction indicator handle
  cor::EntityHandle m_direction_indicator_handle;

  int m_max_health = 0;
  int m_opponent_max_health = 0;

  float m_stamina = 0;

  glm::vec2 m_charge = glm::vec2(0, 0);

  glm::vec2 m_golem_waypoint = glm::vec2(-1, -1);

  // Show hud after cutscene
  void show_hud();

  // Add hearts
  void add_hearts(int heartsToAdd);
  // Remove hearts
  void remove_hearts(int heartsToRemove);
  // Draw all hearts
  void update_hearts();

  // Update boss health
  void update_enemy_health();
  // Update direction indicator
  void update_direction();

  // Update stamina
  void update_stamina();
  // Update charge
  void update_charge();

  // Show death screen overlay
  void show_death_screen();

  // Show death screen overlay
  void remove_death_screen();

  // variable to hold the context
  cor::FrameContext &m_context;
};
}  // namespace gmp

#endif  // !HUD_SYSTEM_HPP
