#pragma once
#ifndef GMP_GRAPHICS_SYSTEM_HPP
#define GMP_GRAPHICS_SYSTEM_HPP

#include "bse/asset.hpp"       // for Asset
#include <bse/allocators.hpp>  // for StackAllocator
#include <bse/flags.hpp>
#include <cor/system.hpp>                                // for System
#include <gfx/debug_buffer.hpp>                          // for DebugBuffer
#include <gfx/full_frame_gaussian_pass.hpp>              // for FullFrameGau...
#include <gmp/systems/graphics_helper/compose_pass.hpp>  // for ComposePass
#include <gmp/systems/graphics_helper/csm_pass.hpp>      // for CSMPass
#include <gmp/systems/graphics_helper/effect_area_manager.hpp>
#include <gmp/systems/graphics_helper/effect_portal_stencil_pass.hpp>  // for EffectPortalStencilPass
#include <gmp/systems/graphics_helper/forward_pass.hpp>                // for ForwardPass
#include <gmp/systems/graphics_helper/gaussian_pass.hpp>               // for RadialPass
#include <gmp/systems/graphics_helper/glow_pass.hpp>
#include <gmp/systems/graphics_helper/god_ray_pass.hpp>        // for GodRayPass
#include <gmp/systems/graphics_helper/hud_pass.hpp>            // for HUDPass
#include <gmp/systems/graphics_helper/mouse_picking_pass.hpp>  // for MousePickingPass
#include <gmp/systems/graphics_helper/radial_pass.hpp>         // for RadialPass
#include <gmp/systems/graphics_helper/ssao_pass.hpp>           // for SSAOPass
#include <gmp/systems/graphics_helper/utilities.hpp>

namespace cor {
struct EntityHandle;
}
namespace cor {
struct FrameContext;
}
namespace gfx {
class Material;
}
namespace gmp {
class RenderPass;
}
namespace gmp {
struct DirectionalLightComponent;
}  // namespace gmp
namespace gmp {
struct ProjectionComponent;
}  // namespace gmp
struct ChangeCameraEvent;
struct DebugDrawLineEvent;
struct DebugDrawPointEvent;

namespace gfx {
class CommandBucket;
class ShadowBucket;
class GBufferBucket;
class DebugBuffer;
class TextCache;
class FullFramePass;
class FullFrameGaussianPass;
class FullFrameRadialPass;
class Window;
}  // namespace gfx
// class gfx::CommandBucket;

namespace gmp {
struct Transform2dComponent;
struct GraphicTextComponent;
struct DirectionalLightComponent;
struct ProjectionComponent;

// struct PickData {
// };

// a system for rendering all graphics components
class GraphicsSystem : public cor::System {
 public:
  GraphicsSystem(cor::FrameContext &context, gfx::Window *window);

  virtual ~GraphicsSystem();

  // update with read only - graphics is done here
  void read_update(const cor::FrameContext &context) override;
  // update
  void write_update(cor::FrameContext &context) override;

  void write_update_late(cor::FrameContext &context) override;

  void pick_location(glm::ivec2);

  // PickData &get_picked_data();

  VIRTUAL_CLASS_SERFUN((GraphicsSystem)) {
    SERIALIZE(self.m_active_camera);
    SERIALIZE(self.m_pick_location);
    SERIALIZE(self.m_generate_picking_texture);
    SERIALIZE(self.m_radial_active);
    SERIALIZE(self.m_effect_area_manager);
  }
  VIRTUAL_CLASS_DESERFUN((GraphicsSystem)) {
    DESERIALIZE(self.m_active_camera);
    DESERIALIZE(self.m_pick_location);
    DESERIALIZE(self.m_generate_picking_texture);
    DESERIALIZE(self.m_radial_active);
    DESERIALIZE(self.m_effect_area_manager);
  }

 private:
  void initialize();

  // Callback functions to draw debug points and lines
  void draw_debug_line(const DebugDrawLineEvent *line);
  void draw_debug_point(const DebugDrawPointEvent *point);

  // Change active camera
  void change_active_camera(const ChangeCameraEvent *event);

  // actually draw the line
  void draw_debug_line(const glm::vec3 &from, const glm::vec3 &to, const glm::vec3 &color);
  // draw a frustum based on a matrix
  void draw_debug_frustum(glm::mat4 matrix, glm::vec3 color);

  std::unique_ptr<bse::StackAllocator> m_stack_allocator_front;
  std::unique_ptr<bse::StackAllocator> m_stack_allocator_back;

  std::unique_ptr<CSMPass> m_csm_pass;
  std::unique_ptr<ForwardPass> m_forward_pass;
  std::unique_ptr<SSAOPass> m_ssao_pass;
  std::unique_ptr<RadialPass> m_radial_pass;
  std::unique_ptr<ComposePass> m_compose_pass;
  std::unique_ptr<HUDPass> m_hud_pass;
  std::unique_ptr<GodRayPass> m_god_ray_pass;
  std::unique_ptr<MousePickingPass> m_mouse_picking_pass;
  std::unique_ptr<GlowPass> m_glow_gaussian_blur;
  std::unique_ptr<EffectPortalStencilPass> m_portal_stencil_pass;

  // debug primitives
  std::unique_ptr<gfx::DebugBuffer> m_debug_buffer_line;
  std::unique_ptr<gfx::DebugBuffer> m_debug_buffer_point;

  cor::EntityHandle m_active_camera;
  cor::FrameContext &m_context;

  float m_frustum_splits[5];

  std::vector<RenderPass *> m_all_passes;

  gfx::Window *m_window;

  std::unique_ptr<gfx::FrameBuffer> m_active_fbo;
  std::unique_ptr<gfx::FrameBuffer> m_inactive_fbo;

  glm::ivec2 m_pick_location = glm::ivec2(-1, -1);
  bool m_generate_picking_texture = false;

  cor::EventManager::ListenID m_mouse_picking_listener;

  bool m_radial_active = true;

  EffectAreaManager m_effect_area_manager;
  bse::TaskHandle m_submit_task;
};

};  // namespace gmp

#endif
