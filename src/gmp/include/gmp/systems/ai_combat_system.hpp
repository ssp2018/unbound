#pragma once

#ifndef GMP_AI_COMBAT_SYSTEM_HPP
#define GMP_AI_COMBAT_SYSTEM_HPP

#include <cor/event_manager.hpp>
#include <cor/frame_context.hpp>
#include <cor/system.hpp>

namespace gmp {

struct EnemyParty {
  std::vector<cor::EntityHandle> members;  // assumes that each entity has a transform component
  unsigned short int original_group_size;
  glm::vec3 median_point;

  CLASS_SERFUN((EnemyParty)) {
    SERIALIZE(self.members);
    SERIALIZE(self.original_group_size);
    SERIALIZE(self.median_point);
  }
  CLASS_DESERFUN((EnemyParty)) {
    DESERIALIZE(self.members);
    DESERIALIZE(self.original_group_size);
    DESERIALIZE(self.median_point);
  }
};
// system that manages threat for all threat components
class AICombatSystem : public cor::System {
 public:
  AICombatSystem(cor::FrameContext& context);
  virtual ~AICombatSystem(){};

  // read update
  virtual void read_update(const cor::FrameContext& context) override;

  // write update
  virtual void write_update(cor::FrameContext& context) override;

  // adds a enemy to a party
  static void add_enemy_to_group(std::string name, cor::EntityHandle handle);

  VIRTUAL_CLASS_SERFUN((AICombatSystem)) {
    SERIALIZE(self.m_threat_loss);
    SERIALIZE(self.time_point);
    SERIALIZE(self.m_enemy_parties);
  }
  VIRTUAL_CLASS_DESERFUN((AICombatSystem)) {
    DESERIALIZE(self.m_threat_loss);
    DESERIALIZE(self.time_point);
    DESERIALIZE(self.m_enemy_parties);
  }

 private:
  float m_threat_loss = 0.5f;

  static std::unordered_map<std::string, EnemyParty> m_enemy_parties;

  // time point to know when to update the morale
  std::chrono::system_clock::time_point time_point = std::chrono::system_clock::now();

  cor::FrameContext& m_context;

  // adds threat to a entity towards another entity
  void add_threat_event_reciever(const AddThreatEvent* event);

  // increases already existing threat level for a entity
  void increase_threat_event_reciever(const IncreaseThreatEvent* event);

  // the event does not have a value of ho much to decrease the morale, instead the ai component has
  // that information, enabling grunts to have a "personality"
  void morale_loss_event_handler(const MoraleLossEvent* event);
  // std::vector<cor::EventManager::ListenID> m_add_threat_event_id;

  // deals morale damage to other groups if they're close enough to a horrifying event
  void morale_loss_for_other_parties(std::string group, int type);

  // deals morale dmg to the own group and all of its members
  void morale_loss_to_own_party(cor::EntityHandle self, std::string group, int type);

  // Checks if the points are close enough to eachother
  bool distance_check(glm::vec3& first, glm::vec3& second, float distance);

  // boosts the morale of the ai entity
  void morale_boost_event_handler(const MoraleBoostEvent* event);
};
}  // namespace gmp

#endif