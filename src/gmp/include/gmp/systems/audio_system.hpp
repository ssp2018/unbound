#pragma once
#ifndef GMP_AUDIO_SYSTEM_HPP
#define GMP_AUDIO_SYSTEM_HPP

#include <aud/audio_manager.hpp>
#include <cor/event.hpp>
#include <cor/system.hpp>
#include <ext/ext.hpp>
#include <gmp/components/audio_component.hpp>
namespace gmp {

// a system for updating all audio components
class AudioSystem : public cor::System {
 public:
  AudioSystem(cor::FrameContext &context);

  virtual ~AudioSystem();

  // update with read only
  void read_update(const cor::FrameContext &context) override;

  // update with write only
  void write_update(cor::FrameContext &context) override;

  // attaches a source to the entity and plays a given sound
  void audio_play_sound_event_handler(const PlaySoundEvent *event);

  // plays a sound after a while
  void audio_play_sound_delayed_event_handler(const PlaySoundDelayedEvent *event);

  // stops all sounds coming from a entity
  void audio_stop_sound_event_handler(const StopSoundEvent *event);

  // Changes the master volume with the + or - key on the numpad
  void audio_change_master_volume_event_handler(const ChangeMasterVolumeEvent *event);

  // Changes the master pitch
  void audio_change_master_pitch_event_handler(const ChangeMasterPitchEvent *event);

  // catch collision event to play hit sound
  void collision_event_handler(const CollisionEvent *event);

  VIRTUAL_CLASS_SERFUN((AudioSystem)) {
    // SERIALIZE_BASE_CLASS((System));
    // SERIALIZE(self.m_audio_manager);
  }
  VIRTUAL_CLASS_DESERFUN((AudioSystem)) {
    // DESERIALIZE_BASE_CLASS((System));
    // DESERIALIZE(self.m_audio_manager);
  }

 private:
  cor::FrameContext &m_context;
  float m_global_pitch = 1.f;

  std::vector<cor::EntityHandle> m_players;
  std::vector<cor::EntityHandle> m_cameras;
  std::vector<cor::EntityHandle> m_audios;
};
};  // namespace gmp
#endif
