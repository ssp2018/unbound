#pragma once

#ifndef GMP_TEST_SYSTEM_HPP
#define GMP_TEST_SYSTEM_HPP

#include <bse/serialize.hpp>
#include <cor/frame_context.hpp>
#include <cor/system.hpp>
#include <phy/physics_manager.hpp>

namespace gmp {
// Temporary system used for testing
class TestSystem : public cor::System {
 public:
  TestSystem(cor::FrameContext& context);

  virtual ~TestSystem(){};

  virtual void read_update(const cor::FrameContext& context) override;
  virtual void write_update(cor::FrameContext& context) override;

  VIRTUAL_CLASS_SERFUN((TestSystem)) { /*No-Op*/
  }
  VIRTUAL_CLASS_DESERFUN((TestSystem)) { /*No-Op*/
  }

 private:
};
}  // namespace gmp

#endif  // GMP_TEST_SYSTEM_HPP