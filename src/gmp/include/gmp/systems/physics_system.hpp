#pragma once

#ifndef GMP_PHYSICS_SYSTEM_HPP
#define GMP_PHYSICS_SYSTEM_HPP

#include <cor/frame_context.hpp>
#include <cor/system.hpp>
#include <phy/physics_manager.hpp>

namespace gmp {

// Handles physics components. Internal objects require that physics components are mutually
// exclusive, or they will not work properly. Entities with physics components additionally require
// a TransformComponent, or they will be removed as soon as the system is updated
class PhysicsSystem : public cor::System {
  friend class DebugDraw;
  // Debug drawer class inherited from Bullet interface. Fills m_debug_lines and m_debug_points with
  // data
  class DebugDraw : public btIDebugDraw {
   public:
    virtual ~DebugDraw() = default;
    DebugDraw(PhysicsSystem* system) : m_system(system) {}
    virtual void drawLine(const btVector3& from, const btVector3& to,
                          const btVector3& color) override;
    virtual void drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB,
                                  btScalar distance, int lifeTime, const btVector3& color) override;
    virtual void reportErrorWarning(const char* warningString) override;
    virtual void setDebugMode(int debugMode) override;
    virtual int getDebugMode() const override;
    virtual void draw3dText(const btVector3& location, const char* textString) override;

   private:
    PhysicsSystem* m_system;
  };

 public:
  PhysicsSystem(cor::FrameContext& context, float dt_multiplier = 1.f);
  virtual ~PhysicsSystem();

  virtual void read_update(const cor::FrameContext& context) override;
  virtual void write_update(cor::FrameContext& context) override;

  void set_debug_draw(bool draw);

  // Cast a ray into the world and return info on objects hit
  // 'direction' will be normalized
  // Currently only returns first object hit, could be modified to return all hits
  std::vector<cor::RayResult> raycast(glm::vec3 origin, glm::vec3 direction, float length);

  VIRTUAL_CLASS_SERFUN((PhysicsSystem)) { /*SERIALIZE_BASE_CLASS((System));*/
    self.m_pm.reset();
  }
  VIRTUAL_CLASS_DESERFUN((PhysicsSystem)) {
    /*DESERIALIZE_BASE_CLASS((System));*/
    // Reset the physics manager
    self.m_pm.reset();

    self.m_debug_lines.clear();
    self.m_debug_points.clear();
  }

 private:
  // Read update for static objects
  void read_static(const cor::FrameContext& context);
  // Read update for dynamic objects
  void read_dynamic(const cor::FrameContext& context);
  // Read update for character controllers
  void read_character_controller(const cor::FrameContext& context);
  // Read update for projectiles
  void read_projectile(const cor::FrameContext& context);

  // Write update for static objects
  void write_static(cor::FrameContext& context);
  // Write update for dynamic objects
  void write_dynamic(cor::FrameContext& context);
  // Write update for character controllers
  void write_character_controller(cor::FrameContext& context);
  // Write update for projectiles
  void write_projectile(cor::FrameContext& context);

  // Adds a static object
  void add_static_object(uint32_t id, glm::mat4 transform_no_scale, glm::vec3 scale,
                         std::shared_ptr<phy::CollisionShape> shape, bool is_trigger);
  // Adds a dynamic object
  void add_dynamic_object(uint32_t id, glm::mat4 transform,
                          std::shared_ptr<phy::CollisionShape> shape, float mass);
  // Adds a character controller
  void add_character_controller(uint32_t id,                  //
                                glm::vec3 position,           //
                                float radius,                 //
                                float height,                 //
                                float turning_speed,          //
                                float angle,                  //
                                bool face_moving_direction,   //
                                float mass,                   //
                                glm::vec3 gravity,            //
                                glm::vec3 velocity,           //
                                bool navmesh_collision,       //
                                bool is_flying,               //
                                glm::vec3 up,                 //
                                glm::quat desired_direction,  //
                                glm::quat facing_direction,   //
                                bool on_ground,               //
                                bool swinging,                //
                                glm::vec3 swing_velocity,     //
                                glm::vec3 swing_center = glm::vec3(0, 0, 0));
  // Adds a projectile
  void add_projectile(uint32_t id, glm::vec3 position, float radius, float length,
                      glm::vec3 velocity, glm::vec3 gravity, float mass);

  // Activates a static object, resuming simulation if it is asleep
  void activate_static_object(uint32_t id);

  // Activates a dynamic object, resuming simulation if it is asleep
  void activate_dynamic_object(uint32_t id);

  // Activates a character controller, resuming simulation if it is asleep
  void activate_character_controller(uint32_t id);

  // Send collision events
  void send_collision_events(cor::FrameContext& context);

  // Helper function for remove_component_if_projectile. Removes DynamicComponent from projectile if
  // trigger is not a trigger object
  void remove_projectile_if_not_trigger(cor::FrameContext& context, cor::EntityHandle projectile,
                                        cor::EntityHandle trigger);

  // Helper function for send_collision_events. Removes DynamicComponent if the object is a
  // projectile and the other object is not a trigger
  void remove_component_if_projectile(cor::FrameContext& context, cor::EntityHandle object,
                                      cor::EntityHandle object2);

  // Helper function for remove_component_if_projectile
  void remove_projectile(cor::FrameContext& context, cor::Entity& projectile);

  // Creates collision shapes from navmeshes specified in context
  void create_navmesh_shapes(const cor::FrameContext& context);

  // Shapes and mesh datas for navmeshes
  std::vector<std::unique_ptr<btBvhTriangleMeshShape>> m_navmesh_shapes;
  std::vector<std::unique_ptr<btTriangleIndexVertexArray>> m_navmesh_mesh_datas;

  phy::PhysicsManager m_pm;

  DebugDraw m_debug_drawer;
  bool m_debug_drawing_enabled;
  float m_dt_multiplier;

  // Debug lines and points to be sent this frame
  std::vector<glm::vec3> m_debug_lines;
  std::vector<glm::vec3> m_debug_points;

  std::vector<cor::EntityHandle> m_dirty_transforms;

  // Contains entity handles of characters that jumped
  std::vector<cor::EntityHandle> m_character_controllers_jumped;
};

}  // namespace gmp
#endif  // GMP_PHYSICS_SYSTEM_HPP
