#pragma once
#ifndef DEATH_CALLBACKS_HPP
#define DEATH_CALLBACKS_HPP
#include <cor/entity.hpp>
#include <cor/entity_manager.hpp>
#include <gmp/ai_script_entity_wrapper.hpp>
namespace cor {
class EventManager;
}

// AI death callback function where it removes components that is not needed when the ai dies.
void ai_death_callback(cor::EntityHandle entity_handle, cor::EventManager& event_manager,
                       cor::EntityManager& entity_mgr);
// skull death callback
void skull_death_callback(gmp::AIScriptEntityWrapper e, cor::EntityHandle entity_handle,
                          cor::EventManager& event_manager, cor::EntityManager& entity_mgr,
                          cor::FrameContext& context);
// exposing death callback functions to lua so they can be called from scripts.
void expose_death_callbacks();
#endif  // !
