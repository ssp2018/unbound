#pragma once

#ifndef GMP_STATIC_COMPONENT_CONTENTS_HPP
#define GMP_STATIC_COMPONENT_CONTENTS_HPP

#include <cor/cor.hpp>
#include <ext/ext.hpp>
#include <gfx/aabb.hpp>
#include <gmp/physics_component_contents_base.hpp>
#include <phy/collision_shapes.hpp>

namespace cor {
struct FrameContext;
}

namespace gmp {
class PhysicsSystem;
}

namespace phy {
class CollisionShape;
}

// extern template class std::shared_ptr<phy::CollisionShape>;

namespace gmp {

// Handles the contents of a StaticComponent
class StaticComponentContents : public PhysicsComponentContentsBase {
  friend class gmp::PhysicsSystem;

 public:
  virtual ~StaticComponentContents() = default;
  // Set collision shape of the object
  void set_collission_shape(cor::FrameContext& context,
                            const std::shared_ptr<phy::CollisionShape>& shape);

  bool get_is_trigger() const;

  // If set to true, object will be considered a trigger. It will not collide with regular objects,
  // but will still report collision events
  void set_is_trigger(bool is_trigger);

  const phy::CollisionShape* get_collision_shape() const;

  CLASS_SERFUN((StaticComponentContents)) {
    SERIALIZE_BASE_CLASS((PhysicsComponentContentsBase));
    SERIALIZE(self.m_is_trigger);

    SERIALIZE(*m_shape);
  }
  CLASS_DESERFUN((StaticComponentContents)) {
    DESERIALIZE_BASE_CLASS((PhysicsComponentContentsBase));
    DESERIALIZE(self.m_is_trigger);

    phy::CollisionShapeType shape_type;
    ::bse::deserialize(shape_type, data, offset);

    if (shape_type & phy::CollisionShapeType::COMPOUND) {
      m_shape = std::make_shared<phy::CompoundCollisionShape>();
    } else if (shape_type & phy::CollisionShapeType::BOX) {
      m_shape = std::make_shared<phy::BoxCollisionShape>();
    } else if (shape_type & phy::CollisionShapeType::SPHERE) {
      m_shape = std::make_shared<phy::SphereCollisionShape>();
    } else if (shape_type & phy::CollisionShapeType::CAPSULE) {
      m_shape = std::make_shared<phy::CapsuleCollisionShape>();
    } else if (shape_type & phy::CollisionShapeType::CYLINDER) {
      m_shape = std::make_shared<phy::CylinderCollisionShape>();
    } else if (shape_type & phy::CollisionShapeType::CONVEX_HULL) {
      m_shape = std::make_shared<phy::ConvexHullCollisionShape>();
    } else if (shape_type & phy::CollisionShapeType::TRIANGLE_MESH) {
      m_shape = std::make_shared<phy::TriangleMeshCollisionShape>();
    } else {
      ASSERT(false) << "Unknown child type when deserializing CompoundCollisionShape. Type: "
                    << shape_type;
    }

    DESERIALIZE(*m_shape);
  }

 protected:
  // Shape is shared between Component and PhysicsManager. Kind of ugly, but both are going to need
  // it
  std::shared_ptr<phy::CollisionShape> m_shape;

  bool m_is_trigger = false;
};
}  // namespace gmp

// DONT expose the classes here, expose them in the classes that inherit from them

UNBOUND_TYPE((gmp::StaticComponentContents), set_collission_shape, get_is_trigger, set_is_trigger)

#endif