#pragma once

#ifndef GMP_UNBOUND_LOADING_STATE_HPP
#define GMP_UNBOUND_LOADING_STATE_HPP

#include <cor/state.hpp>
#include <gmp/transform.hpp>
#include <gmp/unbound_game_state.hpp>

namespace gmp {

// State for the game's start screen
class UnboundLoadingState : public cor::State {
 public:
  UnboundLoadingState(cor::Game& game, gfx::Window& window, aud::AudioManager& audio_mgr,
                      const std::string& scene_path);
  virtual ~UnboundLoadingState();

  void initialize() override;

  bool update(float dt) override;
  bool display() override;

 private:
  // Used to delay loading for one frame so that the screen has time to be displayed
  int m_phase = 0;

  std::unique_ptr<gmp::UnboundGameState> m_loaded_state;
  bse::TaskHandle m_loading_task;

  cor::FrameContext m_loaded_context;
  // sol::table m_loaded_scene;

  cor::EntityHandle m_swirl;
  std::string m_scene_path;
};
}  // namespace gmp
#endif  // GMP_UNBOUND_LOADING_STATE_HPP
