#pragma once

#ifndef GMP_GAME_STATE_HPP
#define GMP_GAME_STATE_HPP

#include <bse/serialize.hpp>
#include <cor/state.hpp>
#include <gmp/event_recorder.hpp>

// namespace cor {
//   class EventRecorder;
// }

namespace gmp {

// The main state for Unbound's game logic
class UnboundGameState : public cor::State {
 public:
  UnboundGameState(cor::Game& game, gfx::Window& window, aud::AudioManager& audio_mgr);
  UnboundGameState(cor::Game& game, gfx::Window& window, aud::AudioManager& audio_mgr,
                   cor::FrameContext&& context);
  virtual ~UnboundGameState();

  void initialize() override;

  bool update(float dt) override;
  bool display() override;

 private:
  cor::EventManager::ListenID m_toggle_camera_switch_listener;
  cor::EventManager::ListenID m_collision_listener;
  cor::EventManager::ListenID m_close_listener;
  cor::EventManager::ListenID m_pause_listener;
  cor::EventManager::ListenID m_editor_listener;
  cor::EventManager::ListenID m_start_listener;
  cor::EventManager::ListenID m_bse_edit_listener;
  cor::EventManager::ListenID m_quick_save_listener;
  cor::EventManager::ListenID m_quick_load_listener;
  cor::EventManager::ListenID m_start_event_recording_listener;
  cor::EventManager::ListenID m_stop_event_recording_listener;
  cor::EventManager::ListenID m_start_event_playback_listener;
  cor::EventManager::ListenID m_save_checkpoint_listener;
  cor::EventManager::ListenID m_load_checkpoint_listener;

  bse::SerializedData m_saved_state;

  gmp::EventRecorder m_event_recorder;

  bool m_paused = false;
  cor::SystemManager::ActiveFingerprint m_pause_fingerprint;
  bse::RuntimeAsset<aud::SoundSource> m_ambient;
};
}  // namespace gmp
#endif  // GMP_GAME_STATE_HPP
