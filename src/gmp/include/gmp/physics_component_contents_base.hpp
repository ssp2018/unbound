#pragma once

#ifndef GMP_PHYSICS_COMPONENT_CONTENTS_BASE_HPP
#define GMP_PHYSICS_COMPONENT_CONTENTS_BASE_HPP

#include <bse/serialize.hpp>
#include <gfx/aabb.hpp>

namespace gmp {
class PhysicsSystem;
}

namespace gmp {
// Base for classes contained in physics components
class PhysicsComponentContentsBase {
  friend class gmp::PhysicsSystem;

 public:
  virtual ~PhysicsComponentContentsBase() = default;
  const gfx::AABB& get_aabb() const;

  void set_offset_transform(const glm::mat4& offset);
  glm::mat4 get_offset_transform() const;

  CLASS_SERFUN((PhysicsComponentContentsBase)) {
    SERIALIZE(self.m_offset);
    SERIALIZE(self.m_aabb);
    SERIALIZE(self.m_dirty);
  }
  CLASS_DESERFUN((PhysicsComponentContentsBase)) {
    DESERIALIZE(self.m_offset);
    DESERIALIZE(self.m_aabb);
    DESERIALIZE(self.m_dirty);
  }

 protected:
  // Returns true if the object has changed
  bool is_dirty() const;

  // Set dirty flag
  // This is const because it is used by PhysicsSystem during a read update and is not exposed to
  // other systems
  void set_dirty(bool dirty = true) const;

  // Contains an offset transform to be applied to the transform component before the transformation
  // from the physics system
  glm::mat4 m_offset = glm::mat4(1.0f);

  gfx::AABB m_aabb;

  mutable bool m_dirty = false;
};

}  // namespace gmp

// DONT expose the classes here, expose them in the classes that inherit from them

#endif  // GMP_PHYSICS_COMPONENT_CONTENTS_HPP