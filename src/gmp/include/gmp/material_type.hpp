#pragma once
#ifndef GMP_MATERIAL_TYPE_HPP
#define GMP_MATERIAL_TYPE_HPP

namespace gmp {

// Types of materials. Add more if required, but also add more parameters to
// AudioClass::set_collision_sound() in AudioComponent
// IMPORTANT!!!! DO NOT CHANGE THE MATERIAL NUMBERS WHEN ADDING OR REMOVING SOMETHING!!!
enum class MaterialType {
  INVALID = 0,
  GENERIC = 1,
  WOOD = 2,
  GRASS = 3,
  METAL = 4,
  STONE = 5,
  FLESH = 6,
  MAGICAL = 7
};

}  // namespace gmp

#endif  // GMP_MATERIAL_TYPE_HPP