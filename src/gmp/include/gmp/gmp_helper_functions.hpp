#ifndef GMP_HELPER_FUNCTIONS_HPP
#define GMP_HELPER_FUNCTIONS_HPP

#include <cor/entity.hpp>

namespace cor {
struct FrameContext;
}

namespace gmp {
// function that checks if projectile entity e1's collision with entity e2 should count.
bool validate_projectile_collision(cor::Entity& e1, cor::Entity& e2);

// Creates a particle system entity at the given position using the given ParticlesComponent
cor::EntityHandle create_particle_burst(glm::vec3 pos, cor::FrameContext& context);

// Creates a particles explosion at the position of the entity and using the explode_component of it
// to set the explosion parameters.
// The newly created particle entity is returned and can be modified if need be.
cor::EntityHandle create_particle_explosion(cor::Entity& exploded_entity,
                                            cor::FrameContext& context);

// "Destroys" the arrow by removing the ModelComponent & PhysicsProjectileComponent
void destroy_arrow(cor::EntityHandle arrow_handle, cor::FrameContext& context);

}  // namespace gmp
#endif
