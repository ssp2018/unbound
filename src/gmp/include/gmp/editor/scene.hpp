#pragma once
#ifndef GMP_SCENE_HPP
#define GMP_SCENE_HPP
#include <ai/navmesh.hpp>
#include <bse/asset.hpp>
#include <bse/file_path.hpp>
#include <bse/result.hpp>
namespace scr {
class Object;
}

namespace cor {
struct FrameContext;
}

namespace bse {
template <typename T>
class Asset;
}

extern template class std::unique_ptr<scr::Object>;

namespace gmp {
class Scene {
 public:
  Scene(const std::string& path);

  // Save scene to file at path.
  // If path is not specified, the scene is
  // saved to the same path as it was loaded.
  void save(const std::string& path = "") const;

  // Save scene to file at path.
  static void save(const sol::table& scene, const std::string& path);

  // Load scene from file at path.
  static sol::table load(const std::string& path);

  std::unordered_map<std::string, ai::Navmesh> get_navmeshes() const;
  static std::unordered_map<std::string, ai::Navmesh> get_navmeshes(sol::table scene);
  // Scene(Scene&& other) = default;
  // Scene(const Scene& other) = delete;

  // Scene& operator=(Scene&& other) = default;
  // Scene& operator=(Scene& other) = delete;

  // Load Scene from file
  // static bse::Result<Scene> load(bse::FilePath path);

  // Get scene data structure
  sol::table get_scene() const;

  // Upload scene into frame context.
  static void upload(const sol::table& scene, cor::FrameContext& context);

 private:
  // Scene(const scr::Object& root);

  std::unique_ptr<sol::table> m_root;
  std::string m_path;
};

}  // namespace gmp

extern template class bse::Asset<gmp::Scene>;

#endif
