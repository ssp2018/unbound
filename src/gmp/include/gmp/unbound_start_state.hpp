#pragma once

#ifndef GMP_UNBOUND_START_STATE_HPP
#define GMP_UNBOUND_START_STATE_HPP

#include <cor/state.hpp>
#include <gmp/event_recorder.hpp>
#include <gmp/transform.hpp>

namespace gmp {

// State for the game's start screen
class UnboundStartState : public cor::State {
 public:
  UnboundStartState(cor::Game& game, gfx::Window& window, aud::AudioManager& audio_mgr);
  virtual ~UnboundStartState();

  void initialize() override;

  bool update(float dt) override;
  bool display() override;

 private:
  enum class Transition {
    RECORDING,
    FADE_IN,  //
    WAIT,     //
    FADE_OUT
  };

  cor::EventManager::ListenID m_confirm_listener;
  cor::EventManager::ListenID m_restart_listener;
  cor::EventManager::ListenID m_close_listener;
  cor::EventManager::ListenID m_start_event_recording_listener;
  cor::EventManager::ListenID m_stop_event_recording_listener;
  cor::EventManager::ListenID m_start_event_playback_listener;
  cor::EntityHandle m_confirm_text_handle;
  cor::EntityHandle m_play_handle;
  cor::EntityHandle m_tutorial_handle;
  cor::EntityHandle m_controls_handle;
  cor::EntityHandle m_options_handle;
  cor::EntityHandle m_exit_handle;
  cor::EntityHandle m_title_handle;
  cor::EntityHandle m_camera_handle;

  bool m_options_visible = false;
  cor::EntityHandle m_fullscreen_handle;
  cor::EntityHandle m_1440_handle;
  cor::EntityHandle m_1080_handle;
  cor::EntityHandle m_720_handle;
  cor::EntityHandle m_restart_handle;
  int m_fullscreen;
  int m_res_width;
  int m_res_height;

  gmp::Transform m_transform_from;
  gmp::Transform m_transform_to;

  Transition m_transition = Transition::FADE_IN;
  float m_current_time = 0.f;
  float m_duration = 3.f;

  bse::SerializedData m_initial_state;
  gmp::EventRecorder m_event_recorder;

  std::string m_highlighted_entity_name;
  std::string m_clicked_entity_name;
  bse::RuntimeAsset<aud::SoundSource> m_music;
  bse::RuntimeAsset<aud::SoundSource> m_ambient;
  bse::RuntimeAsset<aud::SoundSource> m_select_sound;
  bse::RuntimeAsset<aud::SoundSource> m_click_sound;

  gfx::Window& m_window;

  std::string m_scene;

  // Show and hide options
  void toggle_options();
  // Toggle fullscreen
  void toggle_fullscreen();
  // Set resolution
  void set_resolution(int width, int height);
  // Save config
  void save_config();
};
}  // namespace gmp
#endif  // GMP_UNBOUND_START_STATE_HPP
