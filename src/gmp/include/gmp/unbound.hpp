#pragma once

#ifndef GMP_UNBOUND_HPP
#define GMP_UNBOUND_HPP

#include <cor/game.hpp>  // for Game
namespace gfx {
class Window;
}

namespace gmp {

void initialize_unbound_types_and_components();
void initialize_unbound_types_and_components1();
void initialize_unbound_types_and_components2();
void initialize_unbound_types_and_components3();
void initialize_unbound_types_and_components4();
void initialize_unbound_types_and_components5();
void initialize_unbound_types_and_components6();
void initialize_unbound_types_and_components7();
void initialize_unbound_types_and_components8();
void initialize_unbound_types_and_components9();
// See comments for cor::Game
class Unbound final : public cor::Game {
 public:
  Unbound(gfx::Window& window, unsigned int max_frame_count = 0);
  virtual ~Unbound();

 protected:
  // Override for initializing all Unbound related components and systems
  void initialize() override;

 private:
};
}  // namespace gmp
#endif  // GMP_UNBOUND_HPP
