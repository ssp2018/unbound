#pragma once
#ifndef GMP_PLAYER_BASE_STATE_HPP
#define GMP_PLAYER_BASE_STATE_HPP

#include <bse/serialize.hpp>

namespace cor {
struct FrameContext;
}
namespace gmp {
class PlayerBaseState;
}  // namespace gmp
namespace gmp {
class PlayerSystem;
}  // namespace gmp

namespace gmp {

class PlayerSystem;
class PlayerBaseState;

// Enum for the type of action that was returned from a state update
enum class ActionType { PUSH, REPLACE, REMOVE };

// All possible player states
enum class StateType {
  COMMON,
  STANDING,
  RUNNING,
  IN_AIR,
  SWINGING,
  DRAWING,
  AIMING,
  DASHING,
  SLOW_MOTION,
  STUNNED,
  DEAD,
  SPAWNING
};

// Contains the type of action as well as a pointer if necessary
struct StateAction {
  ActionType action;
  std::unique_ptr<PlayerBaseState> new_state;
  int index;

  StateAction(ActionType action, std::unique_ptr<PlayerBaseState> new_state, int index)
      : action{action}, new_state{std::move(new_state)}, index{index} {}
};

// A collection of tightly packed bools representing the key-events that have happened
struct PlayerEvents {
  bool moved : 1;
  bool jumped : 1;
  bool dashed : 1;
  bool draw_started : 1;
  bool draw_ended : 1;
  bool aim_started : 1;
  bool aim_ended : 1;
  bool slow_motion : 1;
  bool action_canceled : 1;
  bool rope_action : 1;
  bool rope_hit : 1;
  bool normal_arrow : 1;
  bool explosive_arrow : 1;

  float move_forward;
  float move_right;

  // Reset all values to false
  void reset() {
    moved = false;
    jumped = false;
    dashed = false;
    draw_started = false;
    draw_ended = false;
    aim_started = false;
    aim_ended = false;
    slow_motion = false;
    action_canceled = false;
    rope_action = false;
    rope_hit = false;
    normal_arrow = false;
    explosive_arrow = false;

    move_forward = 0.f;
    move_right = 0.f;
  }
};

// Base class for player states
class PlayerBaseState {
 public:
  PlayerBaseState(PlayerSystem* player_system);
  virtual ~PlayerBaseState() = default;

  // Abstract initialize-function for the states
  virtual void enter(cor::FrameContext& context) = 0;

  // Abstract update function for each state that gets called by update
  virtual void update(cor::FrameContext& context, const PlayerEvents& keys_pressed) = 0;

  VIRTUAL_CLASS_SERFUN((PlayerBaseState)) { SERIALIZE(m_state_type); }
  VIRTUAL_CLASS_DESERFUN((PlayerBaseState)) { /*No-Op*/
  }

  StateType m_state_type;

 protected:
  PlayerSystem* m_player_system;
};

};  // namespace gmp

#endif
