#pragma once
#ifndef GMP_PLAYER_DASHING_HPP
#define GMP_PLAYER_DASHING_HPP

#include "gmp/player/player_base_state.hpp"  // for WriteData (ptr only)
namespace cor {
struct FrameContext;
}
namespace gmp {
class PlayerSystem;
}

namespace gmp {

// State representing player doing a dash
class PlayerDashing : public PlayerBaseState {
 public:
  PlayerDashing(PlayerSystem* player_system, const glm::vec3 direction);

  virtual ~PlayerDashing() = default;
  // Run code that should always happen when entering this state
  void enter(cor::FrameContext& context) override;

  // Update the state
  void update(cor::FrameContext& context, const PlayerEvents& keys_pressed) override;

  VIRTUAL_CLASS_SERFUN((PlayerDashing)) {
    SERIALIZE_BASE_CLASS((PlayerBaseState));
    SERIALIZE(self.m_delay_left);
    SERIALIZE(self.m_time_left);
    SERIALIZE(self.m_direction);
  }
  VIRTUAL_CLASS_DESERFUN((PlayerDashing)) {
    DESERIALIZE_BASE_CLASS((PlayerBaseState));
    DESERIALIZE(self.m_delay_left);
    DESERIALIZE(self.m_time_left);
    DESERIALIZE(self.m_direction);
  }

 private:
  const float m_initial_delay = 0.10f;
  float m_delay_left;
  const float m_speed = 100.f;
  const float m_duration = 0.21f;
  float m_time_left;
  glm::vec3 m_direction;
};

};  // namespace gmp

#endif  // GMP_PLAYER_DASHING_HPP
