#pragma once
#ifndef GMP_PLAYER_IN_AIR_HPP
#define GMP_PLAYER_IN_AIR_HPP

#include "gmp/player/player_base_state.hpp"  // for WriteData (ptr only)
namespace cor {
struct FrameContext;
}
namespace gmp {
class PlayerSystem;
}
namespace gmp {

// State representing player being in the air
class PlayerInAir : public PlayerBaseState {
 public:
  PlayerInAir(PlayerSystem* player_system, bool just_falling = false,
              glm::vec2 horizontal_vel = glm::vec2(0, 0));

  virtual ~PlayerInAir() = default;

  // Run code that should always happen when entering this state
  void enter(cor::FrameContext& context) override;

  // Update the state
  void update(cor::FrameContext& context, const PlayerEvents& keys_pressed) override;

  VIRTUAL_CLASS_SERFUN((PlayerInAir)) {
    SERIALIZE_BASE_CLASS((PlayerBaseState));
    SERIALIZE(self.m_just_falling);
    SERIALIZE(self.m_frames_in_state);
    SERIALIZE(self.m_horizontal_velocity);
    SERIALIZE(self.m_max_total_velocity);
    SERIALIZE(self.m_history_index);
    SERIALIZE(self.m_z_velocity_history);
  }
  VIRTUAL_CLASS_DESERFUN((PlayerInAir)) {
    DESERIALIZE_BASE_CLASS((PlayerBaseState));
    DESERIALIZE(self.m_just_falling);
    DESERIALIZE(self.m_frames_in_state);
    DESERIALIZE(self.m_horizontal_velocity);
    DESERIALIZE(self.m_max_total_velocity);
    DESERIALIZE(self.m_history_index);
    DESERIALIZE(self.m_z_velocity_history);
  }

 private:
  bool m_just_falling;

  unsigned int m_frames_in_state;
  glm::vec2 m_horizontal_velocity;
  float m_max_total_velocity;

  unsigned char m_history_index;
  std::vector<float> m_z_velocity_history;
};

};  // namespace gmp

#endif  // GMP_PLAYER_IN_AIR_HPP
