#pragma once
#ifndef GMP_PLAYER_DEAD_HPP
#define GMP_PLAYER_DEAD_HPP

#include "gmp/player/player_base_state.hpp"  // for WriteData (ptr only)
namespace cor {
struct FrameContext;
}
namespace gmp {
class PlayerSystem;
}

namespace gmp {

// State representing player being dead
class PlayerDead : public PlayerBaseState {
 public:
  PlayerDead(PlayerSystem* player_system);

  virtual ~PlayerDead() = default;
  // Run code that should always happen when entering this state
  void enter(cor::FrameContext& context) override;

  // Update the state
  void update(cor::FrameContext& context, const PlayerEvents& keys_pressed) override;

  VIRTUAL_CLASS_SERFUN((PlayerDead)) {
    SERIALIZE_BASE_CLASS((PlayerBaseState));
    SERIALIZE(m_time_before_exiting);
  }
  VIRTUAL_CLASS_DESERFUN((PlayerDead)) {
    DESERIALIZE_BASE_CLASS((PlayerBaseState));  //
    DESERIALIZE(m_time_before_exiting);
  }

 private:
  float m_time_before_exiting = 10.f;
};

};  // namespace gmp

#endif  // GMP_PLAYER_DEAD_HPP
