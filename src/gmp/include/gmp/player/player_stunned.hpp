#pragma once
#ifndef GMP_PLAYER_STUNNED_HPP
#define GMP_PLAYER_STUNNED_HPP

#include "gmp/player/player_base_state.hpp"  // for WriteData (ptr only)
namespace cor {
struct FrameContext;
}
namespace gmp {
class PlayerSystem;
}

namespace gmp {

// State representing player stunned
class PlayerStunned : public PlayerBaseState {
 public:
  PlayerStunned(PlayerSystem* player_system, float duration);

  virtual ~PlayerStunned() = default;

  // Run code that should always happen when entering this state
  void enter(cor::FrameContext& context) override;

  // Update the state
  void update(cor::FrameContext& context, const PlayerEvents& keys_pressed) override;

  VIRTUAL_CLASS_SERFUN((PlayerStunned)) {
    SERIALIZE_BASE_CLASS((PlayerBaseState));
    SERIALIZE(self.m_duration_left);
  }
  VIRTUAL_CLASS_DESERFUN((PlayerStunned)) {
    DESERIALIZE_BASE_CLASS((PlayerBaseState));
    DESERIALIZE(self.m_duration_left);
  }

 private:
  float m_duration_left;
};

};  // namespace gmp

#endif  // GMP_PLAYER_STUNNED_HPP
