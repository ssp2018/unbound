#pragma once
#ifndef GMP_PLAYER_AIMING_HPP
#define GMP_PLAYER_AIMING_HPP

#include "gmp/player/player_base_state.hpp"  // for WriteData (ptr only)
namespace cor {
struct FrameContext;
}
namespace gmp {
class PlayerSystem;
}
namespace gmp {

// State representing player aiming (precision aiming)
class PlayerAiming : public PlayerBaseState {
 public:
  PlayerAiming(PlayerSystem* player_system);

  virtual ~PlayerAiming() = default;
  // Run code that should always happen when entering this state
  void enter(cor::FrameContext& context) override;

  // Update the state
  void update(cor::FrameContext& context, const PlayerEvents& keys_pressed) override;

  VIRTUAL_CLASS_SERFUN((PlayerAiming)) {
    SERIALIZE_BASE_CLASS((PlayerBaseState));  //
  }
  VIRTUAL_CLASS_DESERFUN((PlayerAiming)) {
    DESERIALIZE_BASE_CLASS((PlayerBaseState));  //
  }

 private:
  const float m_closest_camera_percent = 0.01f;
};

};  // namespace gmp

#endif  // GMP_PLAYER_AIMING_HPP
