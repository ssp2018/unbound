#pragma once
#ifndef GMP_PLAYER_SPAWNING_HPP
#define GMP_PLAYER_SPAWNING_HPP

#include "cor/entity_handle.hpp"
#include "gmp/player/player_base_state.hpp"  // for WriteData (ptr only)

namespace cor {
struct FrameContext;
}
namespace gmp {
class PlayerSystem;
}

namespace gmp {

// State representing player spawning
class PlayerSpawning : public PlayerBaseState {
 public:
  PlayerSpawning(PlayerSystem* player_system);

  virtual ~PlayerSpawning() = default;

  // Run code that should always happen when entering this state
  void enter(cor::FrameContext& context) override;

  // Update the state
  void update(cor::FrameContext& context, const PlayerEvents& keys_pressed) override;

  VIRTUAL_CLASS_SERFUN((PlayerSpawning)) {
    SERIALIZE_BASE_CLASS((PlayerBaseState));
    SERIALIZE(self.m_frames_in_state);
    SERIALIZE(self.m_rotation);
    SERIALIZE(self.m_player_offset_matrix);
    SERIALIZE(self.m_black_screen_handle);
  }
  VIRTUAL_CLASS_DESERFUN((PlayerSpawning)) {
    DESERIALIZE_BASE_CLASS((PlayerBaseState));
    DESERIALIZE(self.m_frames_in_state);
    DESERIALIZE(self.m_rotation);
    DESERIALIZE(self.m_player_offset_matrix);
    DESERIALIZE(self.m_black_screen_handle);
  }

 private:
  unsigned int m_frames_in_state = 0;
  float m_time_in_state = 0.f;
  float m_rotation;
  glm::mat4 m_player_offset_matrix;

  cor::EntityHandle m_black_screen_handle;
};

};  // namespace gmp

#endif  // GMP_PLAYER_SPAWNING_HPP
