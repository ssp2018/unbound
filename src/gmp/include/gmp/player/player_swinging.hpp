#pragma once
#ifndef GMP_PLAYER_SWINGING_HPP
#define GMP_PLAYER_SWINGING_HPP

#include "gmp/player/player_base_state.hpp"  // for WriteData (ptr only)
namespace cor {
struct FrameContext;
}
namespace gmp {
class PlayerSystem;
}

namespace gmp {

// State representing the player in a swing
class PlayerSwinging : public PlayerBaseState {
 public:
  PlayerSwinging(PlayerSystem* player_system, cor::FrameContext& context);
  virtual ~PlayerSwinging() = default;

  // Run code that should always happen when entering this state
  void enter(cor::FrameContext& context) override;

  // Update the state
  void update(cor::FrameContext& context, const PlayerEvents& keys_pressed) override;

  VIRTUAL_CLASS_SERFUN((PlayerSwinging)) {
    SERIALIZE_BASE_CLASS((PlayerBaseState));
    SERIALIZE(self.m_time_in_state);
    SERIALIZE(self.m_start_dir_to_swing_center);
    SERIALIZE(self.m_frames_on_ground);
    SERIALIZE(self.m_internal_state);
    SERIALIZE(self.m_exit_speed_xy);
    SERIALIZE(self.m_exit_speed_z);
    SERIALIZE(self.m_history_index);
    SERIALIZE(self.m_velocity_history);
  }
  VIRTUAL_CLASS_DESERFUN((PlayerSwinging)) {
    DESERIALIZE_BASE_CLASS((PlayerBaseState));
    DESERIALIZE(self.m_time_in_state);
    DESERIALIZE(self.m_start_dir_to_swing_center);
    DESERIALIZE(self.m_frames_on_ground);
    DESERIALIZE(self.m_internal_state);
    DESERIALIZE(self.m_exit_speed_xy);
    DESERIALIZE(self.m_exit_speed_z);
    DESERIALIZE(self.m_history_index);
    DESERIALIZE(self.m_velocity_history);
  }

 private:
  // Helper to exit swinging and the update function quickly
#define LEAVE_SWING()                        \
  m_player_system->end_swing();              \
  m_internal_state = InternalState::LEAVING; \
  return

  float m_time_in_state = 0.f;
  glm::vec3 m_start_dir_to_swing_center;
  unsigned int m_frames_on_ground = 0;

  enum class InternalState { STAYING, LEAVING, LANDING };
  InternalState m_internal_state = InternalState::STAYING;
  glm::vec2 m_exit_speed_xy = glm::vec2(0, 0);
  float m_exit_speed_z = 0.f;

  unsigned char m_history_index;
  std::vector<float> m_velocity_history;
};
};  // namespace gmp

#endif  // GMP_PLAYER_SWINGING_HPP
