#pragma once
#ifndef GMP_PLAYER_RUNNING_HPP
#define GMP_PLAYER_RUNNING_HPP

#include "gmp/player/player_base_state.hpp"  // for WriteData (ptr only)
namespace cor {
struct FrameContext;
}
namespace gmp {
class PlayerSystem;
}

namespace gmp {

// State representing player running on the ground
class PlayerRunning : public PlayerBaseState {
 public:
  PlayerRunning(PlayerSystem* player_system);

  virtual ~PlayerRunning() = default;

  // Run code that should always happen when entering this state
  void enter(cor::FrameContext& context) override;

  // Update the state
  void update(cor::FrameContext& context, const PlayerEvents& keys_pressed) override;

  VIRTUAL_CLASS_SERFUN((PlayerRunning)) { SERIALIZE_BASE_CLASS((PlayerBaseState)); }
  VIRTUAL_CLASS_DESERFUN((PlayerRunning)) {
    DESERIALIZE_BASE_CLASS((PlayerBaseState));  //
  }

 private:
};

};  // namespace gmp

#endif  // GMP_PLAYER_RUNNING_HPP
