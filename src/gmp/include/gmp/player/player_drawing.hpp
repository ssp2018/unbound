#pragma once
#ifndef GMP_PLAYER_DRAWING_HPP
#define GMP_PLAYER_DRAWING_HPP

#include "cor/entity_handle.hpp"             // for EntityHandle
#include "gmp/player/player_base_state.hpp"  // for WriteData (ptr only)
namespace cor {
struct FrameContext;
}
namespace gmp {
class PlayerSystem;
}
namespace gmp {

// State representing player drawing/charging the bow
class PlayerDrawing : public PlayerBaseState {
 public:
  PlayerDrawing(PlayerSystem* player_system);

  virtual ~PlayerDrawing();

  // Run code that should always happen when entering this state
  void enter(cor::FrameContext& context) override;

  // Update the state
  void update(cor::FrameContext& context, const PlayerEvents& keys_pressed) override;

  VIRTUAL_CLASS_SERFUN((PlayerDrawing)) {
    SERIALIZE_BASE_CLASS((PlayerBaseState));
    SERIALIZE(self.m_arrow_handle);
    SERIALIZE(self.m_type_when_entering);
  }
  VIRTUAL_CLASS_DESERFUN((PlayerDrawing)) {
    DESERIALIZE_BASE_CLASS((PlayerBaseState));
    DESERIALIZE(self.m_arrow_handle);
    DESERIALIZE(self.m_type_when_entering);
  }

 private:
  cor::EntityHandle m_arrow_handle;
  unsigned int m_frames_charged = 0;
  int m_type_when_entering;  // Which arrow type when started drawing
};

};  // namespace gmp

#endif  // GMP_PLAYER_DRAWING_HPP
