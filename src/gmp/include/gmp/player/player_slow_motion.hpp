#pragma once
#ifndef GMP_PLAYER_SLOW_MOTION_HPP
#define GMP_PLAYER_SLOW_MOTION_HPP

#include "gmp/player/player_base_state.hpp"  // for WriteData (ptr only)
namespace cor {
struct FrameContext;
}
namespace gmp {
class PlayerSystem;
}

namespace gmp {

// State representing player in slow motion mode
class PlayerSlowMotion : public PlayerBaseState {
 public:
  PlayerSlowMotion(PlayerSystem* player_system);

  virtual ~PlayerSlowMotion() = default;
  // Run code that should always happen when entering this state
  void enter(cor::FrameContext& context) override;

  // Update the state
  void update(cor::FrameContext& context, const PlayerEvents& keys_pressed) override;

  VIRTUAL_CLASS_SERFUN((PlayerSlowMotion)) { SERIALIZE_BASE_CLASS((PlayerBaseState)); }
  VIRTUAL_CLASS_DESERFUN((PlayerSlowMotion)) {
    DESERIALIZE_BASE_CLASS((PlayerBaseState));  //
  }

 private:
};

};  // namespace gmp

#endif  // GMP_PLAYER_SLOW_MOTION_HPP
