#pragma once
#ifndef GMP_PLAYER_STANDING_HPP
#define GMP_PLAYER_STANDING_HPP

#include "gmp/player/player_base_state.hpp"  // for WriteData (ptr only)
namespace cor {
struct FrameContext;
}
namespace gmp {
class PlayerSystem;
}

namespace gmp {

// State representing player standing still
class PlayerStanding : public PlayerBaseState {
 public:
  PlayerStanding(PlayerSystem* player_system);

  virtual ~PlayerStanding() = default;
  // Run code that should always happen when entering this state
  void enter(cor::FrameContext& context) override;

  // Update the state
  void update(cor::FrameContext& context, const PlayerEvents& keys_pressed) override;

  VIRTUAL_CLASS_SERFUN((PlayerStanding)) { SERIALIZE_BASE_CLASS((PlayerBaseState)); }
  VIRTUAL_CLASS_DESERFUN((PlayerStanding)) {
    DESERIALIZE_BASE_CLASS((PlayerBaseState));  //
  }

 private:
};

};  // namespace gmp

#endif  // GMP_PLAYER_STANDING_HPP
