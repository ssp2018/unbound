#pragma once
#ifndef GMP_PLAYER_COMMON_STATE_HPP
#define GMP_PLAYER_COMMON_STATE_HPP

#include "gmp/player/player_base_state.hpp"  // for WriteData (ptr only)

namespace cor {
struct FrameContext;
}
namespace gmp {
class PlayerSystem;
}

namespace gmp {

// State for all things that are common to all (living) states
class PlayerCommonState : public PlayerBaseState {
 public:
  PlayerCommonState(PlayerSystem* player_system);

  virtual ~PlayerCommonState() = default;

  // Run code that should always happen when entering this state
  void enter(cor::FrameContext& context) override;

  // Update the state
  void update(cor::FrameContext& context, const PlayerEvents& keys_pressed) override;

  VIRTUAL_CLASS_SERFUN((PlayerCommonState)) {
    SERIALIZE_BASE_CLASS((PlayerBaseState));
    SERIALIZE(self.m_can_reach);
    SERIALIZE(self.m_fly_by_sound_cooldown);
  }
  VIRTUAL_CLASS_DESERFUN((PlayerCommonState)) {
    DESERIALIZE_BASE_CLASS((PlayerBaseState));
    DESERIALIZE(self.m_can_reach);
    DESERIALIZE(self.m_fly_by_sound_cooldown);
  }

 private:
  bool m_can_reach = false;

  float m_fly_by_sound_cooldown = -1.f;
  const float m_antenna_length = 2.f;
  const float m_antenna_start = 3.0f;
};

};  // namespace gmp

#endif  // GMP_PLAYER_COMMON_STATE_HPP
