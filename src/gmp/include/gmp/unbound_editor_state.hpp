#pragma once

#ifndef GMP_UNBOUND_EDITOR_STATE_HPP
#define GMP_UNBOUND_EDITOR_STATE_HPP

#include <cor/editor_state.hpp>
#include <ext/ext.hpp>
#include <gmp/components/transform_component.hpp>
#include <scr/scr.hpp>

namespace gmp {

// Helper struct containing plane information
struct Plane {
  glm::vec3 normal;
  float distance;
};

// Helper struct containing ray information
struct Ray {
  glm::vec3 origin;
  glm::vec3 direction;
};

// Concrete implementation of the Unbound editor
class UnboundEditorState : public cor::EditorState {
 public:
  UnboundEditorState(cor::Game& game, gfx::Window& window, aud::AudioManager& audio_mgr);
  virtual ~UnboundEditorState();

  void initialize() override;

  bool update(float dt) override;
  bool display() override;

  void select(cor::EntityHandle handle);
  void deselect();
  std::vector<cor::Entity> get_selected_entities();

 private:
  // attach an entity on another entity
  void attach_entity();

  // Updates the transforms on the current selection if any transform mode is active
  void update_transform_mode();

  // Updates the individual transformation modes
  void update_translation();
  void update_rotation();
  void update_scale();
  void update_attach();

  // Draws lines between waypoint entities
  void draw_waypoint_path();

  // Creates an entity through a script (gives it a meta component, etc)
  cor::Entity create_entity(std::string name = "");

  // Creates a preset from the current selection
  void create_preset(std::string name);

  // Switches to (or toggles) the given transform mode
  void switch_to_transform_mode(EditorTransform::Flags target_mode);

  // Reverts to last recorded transform
  void revert_transforms();

  // Get camera entity
  cor::EntityHandle get_camera() const;

  // Returns the screen position of the given entity
  glm::vec2 get_screen_position(cor::EntityHandle handle);

  // Project screen coordinates onto a world plane
  glm::vec3 project_screen_position(glm::vec2 screen_position, Plane world_plane);

  // Project screen coordinates along a ray
  glm::vec3 project_screen_position(glm::vec2 screen_position, Ray world_ray);

  // Calculates the view projection matrix for the active camera
  glm::mat4 get_view_projection() const;

  // Returns the view matrix
  glm::mat4 get_view_matrix() const;

  // Returns the coordinate of the first intersection between the given ray and plane
  glm::vec3 ray_plane_intersection(Ray ray, Plane plane);

  // Converts world positions to NDC
  glm::vec3 world_to_ndc(glm::vec3 world);

  // Converts NDC positions to world
  glm::vec3 ndc_to_world(glm::vec3 ndc);

  // Converts NDC positions to screen
  glm::vec2 ndc_to_screen(glm::vec3 ndc);

  // Draws a plane using debug lines
  void draw_plane(Plane world_plane, glm::vec3 origin);

  // Draws a ray using debug lines
  void draw_ray(Ray world_ray, glm::vec3 color = {0, 0, 0});

  // Draws a line
  void draw_line(glm::vec3 start, glm::vec3 end, glm::vec3 color = {0, 0, 0});

  // Load scene from file.
  void load_scene(const std::string& path);

 private:
  struct TransformMode {
    EditorTransform::Flags mode;
    std::unordered_map<cor::EntityHandle, gmp::Transform> last_transform;
    gmp::Transform widget_transform;
    gmp::Transform last_widget_transform;
    float unit_length = 1.f;
    glm::vec2 mouse_entry_pos;
    glm::vec2 mouse_offset_from_widget;
    Axis axis = Axis::NONE;
    bool is_global_axis = true;

  } m_current_mode;

  cor::EntityHandle m_camera_handle;

  glm::vec2 m_current_mouse_pos;

  scr::Object m_test_preset;

  bool m_multiselect_active = false;
  bool m_attach_active = false;
  bool m_ignore_input = false;
  sol::table m_context_table;

  std::function<sol::table(cor::EntityManager&, cor::FrameContext&, std::vector<std::string_view>)>
      m_context_creator;
  std::function<void(sol::table&, const sol::table&)> m_event_handler;
};
}  // namespace gmp
#endif  // GMP_UNBOUND_EDITOR_STATE_HPP
