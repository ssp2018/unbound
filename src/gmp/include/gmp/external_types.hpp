#pragma once
#ifndef GMP_EXTERNAL_TYPES_HPP
#define GMP_EXTERNAL_TYPES_HPP

#include <cor/cor.hpp>
#include <ext/ext.hpp>

UNBOUND_TYPE((glm::vec4), x, y, z, w)
UNBOUND_TYPE((glm::vec3), x, y, z)
UNBOUND_TYPE((glm::vec2), x, y)
UNBOUND_TYPE((glm::u8vec3), x, y, z)
UNBOUND_TYPE((glm::quat), x, y, z, w)
UNBOUND_TYPE((glm::mat4))
#endif
