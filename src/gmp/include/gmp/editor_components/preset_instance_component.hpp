#pragma once

#ifndef GMP_INSTANCE_DUMMY_COMPONENT_HPP
#define GMP_INSTANCE_DUMMY_COMPONENT_HPP

#include <cor/cor.hpp>
#include <cor/entity_handle.hpp>
#include <string>

namespace gmp {

// Component which represents an instance of a preset
struct PresetInstanceComponent {
  std::string name;
  std::vector<cor::EntityHandle> dummies;
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::PresetInstanceComponent), name, dummies)

#endif  // GMP_INSTANCE_DUMMY_COMPONENT_HPP
