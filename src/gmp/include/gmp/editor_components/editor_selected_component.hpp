#pragma once

#ifndef GMP_EDITOR_SELECTED_COMPONENT_HPP
#define GMP_EDITOR_SELECTED_COMPONENT_HPP

#include <cor/cor.hpp>
#include <cor/entity_handle.hpp>

namespace gmp {

// Component which signifies that an entity is selected in the editor
struct EditorSelectedComponent {};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::EditorSelectedComponent))

#endif  // GMP_EDITOR_SELECTED_COMPONENT_HPP
