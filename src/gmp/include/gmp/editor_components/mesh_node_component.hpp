#pragma once
#ifndef GMP_MESH_NODE_COMPONENT_HPP
#define GMP_MESH_NODE_COMPONENT_HPP

#include <cor/cor.hpp>

namespace gmp {

struct MeshNodeComponent {
  std::string mesh;
  std::vector<cor::EntityHandle> neighbors;
};

}  // namespace gmp

UNBOUND_COMPONENT((gmp::MeshNodeComponent), mesh, neighbors)

#endif
