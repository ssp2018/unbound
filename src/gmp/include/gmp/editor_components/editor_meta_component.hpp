#pragma once
#ifndef GMP_EDITOR_META_COMPONENT_HPP
#define GMP_EDITOR_META_COMPONENT_HPP

#include <cor/cor.hpp>

namespace gmp {

struct EditorMetaComponent {
  std::string name;
};

}  // namespace gmp

UNBOUND_COMPONENT((gmp::EditorMetaComponent), name)

#endif
