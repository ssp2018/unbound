#pragma once

#ifndef GMP_PRESET_DUMMY_COMPONENT_HPP
#define GMP_PRESET_DUMMY_COMPONENT_HPP

#include <cor/cor.hpp>
#include <cor/entity_handle.hpp>

namespace gmp {

// Component which points to the real preset entity
struct PresetDummyComponent {
  cor::EntityHandle parent;
};
}  // namespace gmp

UNBOUND_COMPONENT((gmp::PresetDummyComponent), parent)

#endif  // GMP_PRESET_DUMMY_COMPONENT_HPP
