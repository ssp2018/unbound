#pragma once
#ifndef GMP_TRANSFORM_HPP
#define GMP_TRANSFORM_HPP

#include <cor/cor.hpp>
#include <cor/frame_context.hpp>
#include <ext/ext.hpp>

namespace gmp {

// The contents of a TransformComponent
class Transform {
 public:
  glm::mat4 get_model_matrix() const;
  glm::vec3 get_position() const;
  glm::quat get_rotation() const;
  float get_scale() const;
  glm::vec3 get_scale_vector() const;

  // Returns the index of the last frame this transform was updated
  uint16_t get_updated_index() const;

  glm::vec3 get_forward() const;
  glm::vec3 get_right() const;
  glm::vec3 get_up() const;

  bool get_physics_dirty() const;

  void set_model_matrix(glm::mat4 matrix, const cor::FrameContext& context);

  // Should only be used by the physics system
  // Updates model matrix without updating frame index
  void set_model_matrix_no_update(glm::mat4 matrix, const cor::FrameContext& context);

  void set_position(glm::vec3 position, const cor::FrameContext& context);
  void set_rotation(glm::quat rotation, const cor::FrameContext& context);
  void set_rotation_vec(glm::vec3 axis, float radians, const cor::FrameContext& context);
  void set_scale(float scale, const cor::FrameContext& context);
  void set_scale_vector(glm::vec3 scale, const cor::FrameContext& context);

  // Moves an object
  void translate(glm::vec3 move_vector, const cor::FrameContext& context);

  // Rotate with quaternion
  void rotate(glm::quat rotation, const cor::FrameContext& context);

  // Rotate with vector and angle (in radians)
  void rotate(glm::vec3 axis, float radians, const cor::FrameContext& context);

  // Multiply current scale with 'scale'
  void scale(float scale, const cor::FrameContext& context);

  uint16_t get_frame_updated() const;

  // Forces the transform to update next time it is accessed
  // (Used for special occasions where transforms must be reverted)
  void force_dirty(const cor::FrameContext& context);

  // sets the m_physics_dirty variable to false
  void clean_physics() const;

  CLASS_SERFUN((Transform)) {
    SERIALIZE(self.m_position);
    SERIALIZE(self.m_rotation);
    SERIALIZE(self.m_scale);
    SERIALIZE(self.m_dirty);
    SERIALIZE(self.m_physics_dirty);
    SERIALIZE(self.m_frame_updated);
  }
  CLASS_DESERFUN((Transform)) {
    DESERIALIZE(self.m_position);
    DESERIALIZE(self.m_rotation);
    DESERIALIZE(self.m_scale);
    DESERIALIZE(self.m_dirty);
    DESERIALIZE(self.m_physics_dirty);
    DESERIALIZE(self.m_frame_updated);
  }

 private:
  mutable glm::mat4 m_model_matrix = glm::mat4(1.0f);
  glm::vec3 m_position = glm::vec3(0, 0, 0);
  glm::quat m_rotation = glm::quat(1, 0, 0, 0);
  glm::vec3 m_scale = glm::vec3(1, 1, 1);

  // Indicates that model matrix need to be recalculated
  mutable bool m_dirty = false;
  mutable bool m_physics_dirty = true;

  // Contains frame index of the last frame the transform was updated
  uint16_t m_frame_updated = 0;
};

}  // namespace gmp
UNBOUND_TYPE((gmp::Transform), get_model_matrix, get_position, get_rotation, get_scale,
             set_model_matrix, set_model_matrix_no_update, set_position, set_scale,
             set_scale_vector, translate, set_rotation_vec, set_rotation, scale, get_forward,
             get_up, get_right)

#endif
