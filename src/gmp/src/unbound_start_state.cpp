#include "gmp/unbound_start_state.hpp"

#include <bse/edit.hpp>
#include <bse/file.hpp>
#include <cor/entity.tcc>
#include <cor/entity_manager.tcc>
#include <cor/event_variant.hpp>
#include <gmp/components/animation_component.hpp>
#include <gmp/components/audio_component.hpp>
#include <gmp/components/character_controller_component.hpp>
#include <gmp/components/debug_camera_component.hpp>
#include <gmp/components/directional_light_component.hpp>
#include <gmp/components/graphic_text_component.hpp>
#include <gmp/components/health_component.hpp>
#include <gmp/components/material_component.hpp>
#include <gmp/components/model_component.hpp>
#include <gmp/components/name_component.hpp>
#include <gmp/components/particles_component.hpp>
#include <gmp/components/player_component.hpp>
#include <gmp/components/player_damage_visual_component.hpp>
#include <gmp/components/projection_component.hpp>
#include <gmp/components/radial_component.hpp>
#include <gmp/components/saturation_component.hpp>
#include <gmp/components/slow_motion_component.hpp>
#include <gmp/components/texture_component.hpp>
#include <gmp/components/transform_2d_component.hpp>
#include <gmp/components/transform_component.hpp>
#include <gmp/editor/scene.hpp>
#include <gmp/systems/audio_system.hpp>
#include <gmp/systems/cloud_system.hpp>
#include <gmp/systems/debug_camera_system.hpp>
#include <gmp/systems/graphics_system.hpp>
#include <gmp/systems/physics_system.hpp>
#include <gmp/systems/player_system.hpp>
#include <gmp/unbound.hpp>
#include <gmp/unbound_game_state.hpp>
#include <gmp/unbound_loading_state.hpp>

namespace gmp {

static inline const glm::vec3 G_TEXT_COLOR =
    glm::vec3{0.8, 0.54509803921568627450980392156863, 0.27058823529411764705882352941176} * 255.f;
static inline const glm::vec3 G_TEXT_OUTLINE_COLOR =
    glm::vec3{0.56862745098039215686274509803922, 0.16470588235294117647058823529412,
              0.2039215686274509803921568627451} *
    255.f;
gmp::Transform linear_interpolation(const gmp::Transform& from, const gmp::Transform& to,
                                    float frac, cor::FrameContext& context) {
  gmp::Transform result;
  result.set_model_matrix(from.get_model_matrix() * (1.f - frac) + to.get_model_matrix() * (frac),
                          context);
  return result;
}

gmp::Transform cos_interpolation(const gmp::Transform& from, const gmp::Transform& to, float frac,
                                 cor::FrameContext& context) {
  return linear_interpolation(from, to, -std::cos(glm::pi<float>() * frac) / 2 + 0.5f, context);
}

glm::vec2 linear_interpolation(const glm::vec2& from, const glm::vec2& to, float frac,
                               cor::FrameContext& context) {
  glm::vec2 result;
  result = from * (1.f - frac) + to * (frac);
  return result;
}

glm::vec2 cos_interpolation(const glm::vec2& from, const glm::vec2& to, float frac,
                            cor::FrameContext& context) {
  return linear_interpolation(from, to, -std::cos(glm::pi<float>() * frac) / 2 + 0.5f, context);
}

UnboundStartState::UnboundStartState(cor::Game& game, gfx::Window& window,
                                     aud::AudioManager& audio_mgr)
    : State(game, window, audio_mgr), m_event_recorder{m_context}, m_window(window) {
  //
  m_fullscreen = window.is_fullscreen();
  m_res_width = window.get_width();
  m_res_height = window.get_height();
  initialize();
  bse::Edit::disable();
}

UnboundStartState::~UnboundStartState() {
  //
}

void UnboundStartState::initialize() {
  //
  // LOG(NOTICE) << "I'm doing it!";

  // auto result = bse::load_file_raw(bse::ASSETS_ROOT / "saved_states/main_menu_events.bin"_fp);
  // if (result) {
  //   m_initial_state.bytes = *result;
  //   int offset = 0;
  //   bse::deserialize(m_event_recorder, m_initial_state, offset);
  // }

  // Listeners
  m_confirm_listener =
      m_context.event_mgr.register_handler<MenuConfirm>([this](const MenuConfirm* event) {
        if (m_transition == Transition::WAIT) {
          m_transition = Transition::FADE_OUT;
          m_current_time = 0.f;
          m_duration = 3.f;

          TransformComponent& tc = m_context.entity_mgr.get_entity(m_camera_handle);

          // m_game.replace_state(std::make_unique<gmp::UnboundGameState>(m_game, m_window));

          m_transform_from = tc.transform;

          m_transform_to.set_position({-12.0863f, -15.9123f, 54.6709f}, m_context);
          m_transform_to.set_rotation({0.66574f, 0.587526f, -0.304379f, -0.3449f}, m_context);
          m_transform_to.set_scale(1, m_context);

          // m_event_recorder.play(m_context);
        }
      });

  m_start_event_recording_listener = m_context.event_mgr.register_handler<StartEventRecording>(
      [this](const StartEventRecording* event) {
        //
        // m_event_recorder.start_recording();
      });
  m_stop_event_recording_listener = m_context.event_mgr.register_handler<StopEventRecording>(
      [this](const StopEventRecording* event) {
        //
        // m_event_recorder.stop_recording();
        // m_initial_state = bse::serialize(m_event_recorder);

        // bse::write_file(bse::ASSETS_ROOT / "saved_states/main_menu_events.bin"_fp,
        //                 m_initial_state.bytes);
      });
  m_start_event_playback_listener = m_context.event_mgr.register_handler<StartEventPlayback>(
      [this](const StartEventPlayback* event) {
        //
        // m_event_recorder.play(m_context);
      });

  m_close_listener =
      m_context.event_mgr.register_handler<CloseApplication>([this](const CloseApplication* event) {
        std::cout << "Closing" << std::endl;
        m_window.close();
      });

  // m_restart_listener = m_context.event_mgr.register_handler<ExplosiveArrowModeEvent>(
  //     [this](const ExplosiveArrowModeEvent* event) {
  //       bse::Loader<scr::Script>::refresh();
  //       m_game.replace_state(std::make_unique<gmp::UnboundStartState>(m_game, m_window)); //
  //     });

  auto scene_table = Scene::load("assets/script/scene/main_menu_scene.lua");
  Scene::upload(scene_table, m_context);

  m_transform_from.set_position({-24.2959f, -29.5586f, 50.4536f}, m_context);
  m_transform_from.set_rotation({0.632369f, 0.580979f, -0.346677f, -0.377343f}, m_context);
  m_transform_from.set_scale(1, m_context);

  m_transform_to.set_position({-25.8585f, -31.2239f, 53.3625f}, m_context);
  m_transform_to.set_rotation({0.590665f, 0.782819f, -0.156242f, -0.11789f}, m_context);
  m_transform_to.set_scale(1, m_context);

  GraphicTextComponent standard_text;
  standard_text.outline_color = {54, 13, 37};
  // standard_text.color = {189, 41, 72};
  standard_text.color = G_TEXT_COLOR;
  standard_text.outline_color = G_TEXT_OUTLINE_COLOR;
  standard_text.outline_thickness = 0.05f;
  standard_text.font = "arial";

  const float scale = 0.025f;
  const float aspect = (float)m_window.get_width() / m_window.get_height();

  const glm::vec2 text_size{0.2f, 0.2f};

  // Entities
  m_confirm_text_handle = m_context.entity_mgr.create_entity();
  {
    //
    cor::Entity confirm_text = m_context.entity_mgr.get_entity(m_confirm_text_handle);

    gmp::Transform2dComponent t2dc;
    t2dc.position = glm::vec3(0.5, 2.0f, 0);
    t2dc.size = glm::vec2(20.25f * scale / aspect, scale);
    t2dc.rotation = 0.f;

    confirm_text = t2dc;

    gmp::TextureComponent texc;
    texc.texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/press_to_play.png"_fp);

    confirm_text = texc;

    gmp::NameComponent nc;
    nc.name = "Confirm";

    confirm_text = nc;
  }

  m_play_handle = m_context.entity_mgr.create_entity();
  {
    //
    cor::Entity play_text = m_context.entity_mgr.get_entity(m_play_handle);

    gmp::Transform2dComponent t2dc;
    t2dc.position = glm::vec3(0.5f, 2.0f, 0);
    t2dc.size = text_size;
    t2dc.rotation = 0.f;
    t2dc.centering = Transform2dComponent::Centering::CENTER;

    play_text = t2dc;

    gmp::GraphicTextComponent gtc = standard_text;
    gtc.text = "Play";
    play_text = gtc;

    /*
gmp::TextureComponent texc;
texc.texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/play.png"_fp);

play_text = texc;
    */

    gmp::NameComponent nc;
    nc.name = "Play";

    play_text = nc;
  }

  m_tutorial_handle = m_context.entity_mgr.create_entity();
  {
    //
    cor::Entity tutorial_text = m_context.entity_mgr.get_entity(m_tutorial_handle);

    gmp::Transform2dComponent t2dc;
    t2dc.position = glm::vec3(0.5f, 2.0f, 0);
    t2dc.size = text_size;
    t2dc.rotation = 0.f;
    t2dc.centering = Transform2dComponent::Centering::CENTER;

    tutorial_text = t2dc;

    gmp::GraphicTextComponent gtc = standard_text;
    gtc.text = "Tutorial";
    tutorial_text = gtc;

    /*
gmp::TextureComponent texc;
texc.texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/play.png"_fp);

play_text = texc;
    */

    gmp::NameComponent nc;
    nc.name = "Tutorial";

    tutorial_text = nc;
  }

  m_controls_handle = m_context.entity_mgr.create_entity();
  {
    //
    cor::Entity text = m_context.entity_mgr.get_entity(m_controls_handle);

    gmp::Transform2dComponent t2dc;
    t2dc.position = glm::vec3(0.5f, 2.0f, 0);
    t2dc.size = text_size;
    t2dc.rotation = 0.f;
    t2dc.centering = Transform2dComponent::Centering::CENTER;

    text = t2dc;

    gmp::GraphicTextComponent gtc = standard_text;
    gtc.text = "Controls";
    text = gtc;

    /*
gmp::TextureComponent texc;
texc.texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/play.png"_fp);

play_text = texc;
    */

    gmp::NameComponent nc;
    nc.name = "Controls";

    text = nc;
  }

  m_options_handle = m_context.entity_mgr.create_entity();
  {
    //
    cor::Entity text = m_context.entity_mgr.get_entity(m_options_handle);

    gmp::Transform2dComponent t2dc;
    t2dc.position = glm::vec3(0.5f, 2.0f, 0);
    t2dc.size = text_size;
    t2dc.rotation = 0.f;
    t2dc.centering = Transform2dComponent::Centering::CENTER;

    text = t2dc;

    gmp::GraphicTextComponent gtc = standard_text;
    gtc.text = "Options";
    text = gtc;

    /*
gmp::TextureComponent texc;
texc.texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/play.png"_fp);

play_text = texc;
    */

    gmp::NameComponent nc;
    nc.name = "Options";

    text = nc;
  }

  m_exit_handle = m_context.entity_mgr.create_entity();
  {
    //
    cor::Entity exit_text = m_context.entity_mgr.get_entity(m_exit_handle);

    gmp::Transform2dComponent t2dc;
    t2dc.position = glm::vec3(0.5f, 2.40f, 0);
    t2dc.size = text_size;
    t2dc.rotation = 0.f;
    t2dc.centering = Transform2dComponent::Centering::CENTER;

    exit_text = t2dc;

    gmp::GraphicTextComponent gtc = standard_text;
    gtc.text = "Exit";
    exit_text = gtc;

    gmp::NameComponent nc;
    nc.name = "Exit";

    exit_text = nc;
  }

  m_fullscreen_handle = m_context.entity_mgr.create_entity();
  {
    //
    cor::Entity entity = m_context.entity_mgr.get_entity(m_fullscreen_handle);

    gmp::Transform2dComponent t2dc;
    t2dc.position = glm::vec3(1.5f, .4f, 0);
    t2dc.size = glm::vec2(0.1f, 0.1f);
    t2dc.rotation = 0.f;
    t2dc.centering = Transform2dComponent::Centering::CENTER;

    entity = t2dc;

    gmp::GraphicTextComponent gtc = standard_text;
    gtc.text = "Fullscreen";
    if (m_fullscreen == 1) {
      gtc.color = glm::vec3(255, 255, 255);
    } else {
      gtc.color = standard_text.color;
    }
    entity = gtc;

    gmp::NameComponent nc;
    nc.name = "Fullscreen";

    entity = nc;
  }

  m_1440_handle = m_context.entity_mgr.create_entity();
  {
    //
    cor::Entity entity = m_context.entity_mgr.get_entity(m_1440_handle);

    gmp::Transform2dComponent t2dc;
    t2dc.position = glm::vec3(1.5f, .5f, 0);
    t2dc.size = glm::vec2(0.1f, 0.1f);
    t2dc.rotation = 0.f;
    t2dc.centering = Transform2dComponent::Centering::CENTER;

    entity = t2dc;

    gmp::GraphicTextComponent gtc = standard_text;
    gtc.text = "1440";
    if (m_res_height == 1440) {
      gtc.color = glm::vec3(255, 255, 255);
    }
    entity = gtc;

    gmp::NameComponent nc;
    nc.name = "1440";

    entity = nc;
  }

  m_1080_handle = m_context.entity_mgr.create_entity();
  {
    //
    cor::Entity entity = m_context.entity_mgr.get_entity(m_1080_handle);

    gmp::Transform2dComponent t2dc;
    t2dc.position = glm::vec3(1.5f, .55f, 0);
    t2dc.size = glm::vec2(0.1f, 0.1f);
    t2dc.rotation = 0.f;
    t2dc.centering = Transform2dComponent::Centering::CENTER;

    entity = t2dc;

    gmp::GraphicTextComponent gtc = standard_text;
    gtc.text = "1080";
    if (m_res_height == 1080) {
      gtc.color = glm::vec3(255, 255, 255);
    }
    entity = gtc;

    gmp::NameComponent nc;
    nc.name = "1080";

    entity = nc;
  }

  m_720_handle = m_context.entity_mgr.create_entity();
  {
    //
    cor::Entity entity = m_context.entity_mgr.get_entity(m_720_handle);

    gmp::Transform2dComponent t2dc;
    t2dc.position = glm::vec3(1.5f, .6f, 0);
    t2dc.size = glm::vec2(0.1f, 0.1f);
    t2dc.rotation = 0.f;
    t2dc.centering = Transform2dComponent::Centering::CENTER;

    entity = t2dc;

    gmp::GraphicTextComponent gtc = standard_text;
    gtc.text = "720";
    if (m_res_height == 720) {
      gtc.color = glm::vec3(255, 255, 255);
    }
    entity = gtc;

    gmp::NameComponent nc;
    nc.name = "720";

    entity = nc;
  }

  m_restart_handle = m_context.entity_mgr.create_entity();
  {
    //
    cor::Entity entity = m_context.entity_mgr.get_entity(m_restart_handle);

    gmp::Transform2dComponent t2dc;
    t2dc.position = glm::vec3(1.5f, .7f, 0);
    t2dc.size = glm::vec2(0.05f, 0.05f);
    t2dc.rotation = 0.f;
    t2dc.centering = Transform2dComponent::Centering::CENTER;

    entity = t2dc;

    gmp::GraphicTextComponent gtc = standard_text;
    gtc.text = "Restart to take effect";
    entity = gtc;

    gmp::NameComponent nc;
    nc.name = "Restart";

    entity = nc;
  }

  m_title_handle = m_context.entity_mgr.create_entity();
  {
    //
    cor::Entity title_text = m_context.entity_mgr.get_entity(m_title_handle);

    // gmp::Transform2dComponent t2dc;
    // t2dc.position = glm::vec2(0.28, -1.0f);
    // t2dc.size = glm::vec2(5.09284f * scale / aspect, scale);
    // t2dc.rotation = 0.f;

    // title_text = t2dc;

    gmp::TransformComponent tc;
    tc.transform.set_position({-21.452060699463, -26.250562667847, 52.78099822998}, m_context);

    title_text = tc;

    gmp::TextureComponent texc;
    texc.texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/title.png"_fp);

    title_text = texc;
  }

  {
    std::vector<cor::EntityHandle> cameras =
        m_context.entity_mgr.get_entities(cor::Key::create<ProjectionComponent>());
    for (cor::EntityHandle camera : cameras) {
      m_context.entity_mgr.destroy_entity(camera);
    }
  }

  m_camera_handle = m_context.entity_mgr.create_entity();
  {
    //
    cor::Entity camera = m_context.entity_mgr.get_entity(m_camera_handle);

    // gmp::TransformComponent tc;
    // tc.transform.set_position({0, 0, 0}, m_context);
    // camera = tc;

    /*
    -25.8585, -31.2239, 53.3625
    0.782819, -0.156242, -0.11789, 0.590665,
    1

    */

    gmp::TransformComponent tc;
    tc.transform = m_transform_from;
    camera = tc;

    gmp::ProjectionComponent pc;
    pc.aspectRatio = (float)m_window.get_width() / m_window.get_height();
    pc.near = 0.1f;
    pc.far = 1500.f;
    pc.field_of_view = 80.f;
    pc.projection_matrix =
        glm::perspective(glm::radians(pc.field_of_view), pc.aspectRatio, pc.near, pc.far);
    camera = pc;

    gmp::DebugCameraComponent dbgc;
    camera = dbgc;
  }

  {
    //
    cor::EntityHandle handle = m_context.entity_mgr.create_entity();
    cor::Entity player_camera = m_context.entity_mgr.get_entity(handle);

    // gmp::TransformComponent tc;
    // tc.transform.set_position({0, 0, 0}, m_context);
    // player_camera = tc;

    /*
    -25.8585, -31.2239, 53.3625
    0.782819, -0.156242, -0.11789, 0.590665,
    1

    */

    // gmp::TransformComponent tc;
    // tc.transform = m_transform_from;
    // player_camera = tc;

    // gmp::ProjectionComponent pc;
    // pc.aspectRatio = (float)m_window.get_width() / m_window.get_height();
    // pc.near = 0.1f;
    // pc.far = 1500.f;
    // pc.field_of_view = 80.f;
    // pc.projection_matrix =
    //    glm::perspective(glm::radians(pc.field_of_view), pc.aspectRatio, pc.near, pc.far);
    // player_camera = pc;

    // gmp::NameComponent nc;
    // nc.name = "main_camera";
    // player_camera = nc;
  }

  cor::EntityHandle light_handle = m_context.entity_mgr.create_entity();
  {
    cor::Entity light = m_context.entity_mgr.get_entity(light_handle);

    DirectionalLightComponent dlc;
    dlc.direction = glm::normalize(glm::vec3(-0.5, -1, -1));
    dlc.color = glm::vec3(1.f, 1.f, 1.f);
    dlc.has_shadow = true;
    dlc.is_cascade = true;
    light = dlc;
  }

  {
    auto entity_handle = m_context.entity_mgr.create_entity();
    auto entity = m_context.entity_mgr.get_entity(entity_handle);
    SaturationComponent comp = SaturationComponent();
    comp.saturation = 1;
    entity = comp;
  }

  // Player
  {
    cor::EntityHandle player_handle = m_context.entity_mgr.create_entity();
    cor::Entity player = m_context.entity_mgr.get_entity(player_handle);
    TransformComponent tc3;
    tc3.transform.translate(glm::vec3(-21.452060699463f, -27.417137146, 62.78099822998f),
                            m_context);
    tc3.transform.rotate(glm::quat(0.0f, 0.0f, 0.98239195346832f, -0.18683165311813f), m_context);
    tc3.transform.set_scale(2.f, m_context);
    player = tc3;
    ModelComponent mc3;
    mc3.asset = bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/player_mesh.fbx"_fp);
    player = mc3;
    player = PlayerComponent();
    SlowMotionComponent smc;
    smc.factor.set_factor(0.8f);
    player = smc;
    HealthComponent hcp;
    hcp.max_health_points = 10;
    hcp.health_points = hcp.max_health_points;
    ParticlesComponent ppc;
    ppc.spawn_per_second = 0;
    ppc.texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/particle.png"_fp);
    ppc.min_life = 0.1f;
    ppc.max_life = 0.2f;
    player = ppc;
    RadialComponent rbc;
    rbc.blur_radius = 0.f;
    player = rbc;
    NameComponent name;
    name.name = "player";
    player = name;
    PlayerDamageVisualComponent pdvc;
    pdvc.color = glm::vec4(1, 0, 0, 0);
    player = pdvc;

    // highlight player
    // player = HighlightComponent();

    // hcp.death_callback = death_scripts->return_value()["player_death_callback"];

    player = hcp;
    if (1) {
      AnimationComponent& ac1 = player.attach<AnimationComponent>();
      ac1.state_machine.set_machine_and_state("player", "idle");
    }

    // Attachment onto skeleton
    // auto attaching_entity_handle = m_context.entity_mgr.create_entity();
    // auto attaching_entity = m_context.entity_mgr.get_entity(attaching_entity_handle);
    // attaching_entity = TransformComponent();
    // auto& atjoint_c = attaching_entity.attach<AttachToJointComponent>();
    // atjoint_c.entity_handle = entity1_handle;
    // atjoint_c.joint_name = "head";
    // auto& atmodel_c = attaching_entity.attach<ModelComponent>();
    // atmodel_c.asset = bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/hat.fbx"_fp);

    auto cc = CharacterControllerComponent();
    cc.contents.set_height(m_context, 0.8f);
    cc.contents.set_radius(m_context, 0.3f);
    cc.contents.set_mass(m_context, 80.0f);
    cc.contents.set_gravity(glm::vec3(0, 0, 0.f));
    cc.contents.set_turning_speed(10);
    cc.contents.set_offset_transform(
        glm::translate(
            glm::mat4(1.0f),
            glm::vec3(0, 0, -cc.contents.get_height() * 0.5f - cc.contents.get_radius())) *
        glm::rotate(glm::mat4(1), glm::radians(180.f), glm::vec3(0.f, 0.f, 1.f)));
    player = cc;

    AudioComponent pac;
    pac.audio.add_periodic_sound_group("ambient");
    pac.audio.set_intervall_time_limit("ambient", 0.05f);
    pac.audio.set_looping_sound_group_enabled("ambient", true);
    player = std::move(pac);

    MaterialComponent mc = MaterialComponent();
    mc.material = MaterialType::FLESH;
    player = mc;
  }

  //   // Systems
  m_system_mgr.add_system<gmp::GraphicsSystem>(m_context, &m_window);
  m_system_mgr.add_system<gmp::AnimationSystem>(m_context);
  m_system_mgr.add_system<gmp::PlayerSystem>(m_context);
  m_system_mgr.add_system<gmp::PhysicsSystem>(m_context);
  m_system_mgr.add_system<gmp::CloudSystem>(m_context);
  // m_system_mgr.add_system<gmp::DebugCameraSystem>(m_context);

  // Unlock mouse
  m_input_mgr.unlock_mouse();

  aud::SoundSettings settings;
  settings.loop = true;
  settings.gain = 0.45f;
  settings.rolloff_factor = 0.f;
  m_music = std::move(bse::RuntimeAsset<aud::SoundSource>(
      bse::Asset<aud::SoundBuffer>("assets/audio/music/toot_toot.ogg"), settings));
  m_music->play(true);

  settings.loop = true;
  settings.gain = 0.05f;
  m_ambient = std::move(bse::RuntimeAsset<aud::SoundSource>(
      bse::Asset<aud::SoundBuffer>("assets/audio/effects/ambient/ambient_forest.ogg"), settings));
  m_ambient->play(true);

  settings.loop = false;
  settings.gain = 0.75f;
  m_select_sound = std::move(bse::RuntimeAsset<aud::SoundSource>(
      bse::Asset<aud::SoundBuffer>("assets/audio/effects/bow_and_arrow/hit/post/hit2.ogg"),
      settings));
  m_click_sound = std::move(bse::RuntimeAsset<aud::SoundSource>(
      bse::Asset<aud::SoundBuffer>("assets/audio/effects/bow_and_arrow/hit/post/hit1.ogg"),
      settings));
}

bool UnboundStartState::update(float dt) {
  //

  TransformComponent& tc = m_context.entity_mgr.get_entity(m_camera_handle);
  Transform2dComponent& text_tc = m_context.entity_mgr.get_entity(m_confirm_text_handle);
  Transform2dComponent& play_tc = m_context.entity_mgr.get_entity(m_play_handle);
  Transform2dComponent& tutorial_tc = m_context.entity_mgr.get_entity(m_tutorial_handle);
  Transform2dComponent& controls_tc = m_context.entity_mgr.get_entity(m_controls_handle);
  Transform2dComponent& options_tc = m_context.entity_mgr.get_entity(m_options_handle);
  Transform2dComponent& exit_tc = m_context.entity_mgr.get_entity(m_exit_handle);
  // Transform2dComponent& title_tc = m_context.entity_mgr.get_entity(m_title_handle);

  const float button_positions_start_x = 0.2f;
  const float button_positions_start_y = 0.4f;
  const float button_positions_step_y = 0.12f;
  static const glm::vec2 button_positions[5] = {
      {button_positions_start_x, button_positions_start_y + button_positions_step_y * 0.f},
      {button_positions_start_x, button_positions_start_y + button_positions_step_y * 1.f},
      {button_positions_start_x, button_positions_start_y + button_positions_step_y * 2.f},
      {button_positions_start_x, button_positions_start_y + button_positions_step_y * 3.f},
      {button_positions_start_x, button_positions_start_y + button_positions_step_y * 4.f}};

  switch (m_transition) {
    case Transition::RECORDING: {
      m_input_mgr.poll_input();
      // dt = m_event_recorder.update(m_context, dt);
    } break;

    case Transition::FADE_IN: {
      if (m_current_time > m_duration) {
        // dt = m_event_recorder.update(m_context, dt);
        m_current_time = 0.f;
        m_duration = 4.f;
        m_transition = Transition::WAIT;
        m_transform_from = tc.transform;
        gmp::Transform new_to = tc.transform;
        new_to.translate({0, 0, 0.2f}, m_context);
        m_transform_to = new_to;
      } else {
        m_current_time += dt;
        tc.transform = cos_interpolation(m_transform_from, m_transform_to,
                                         (m_current_time / m_duration), m_context);

        // text_tc.position =
        //    cos_interpolation({0.5, 1.2f}, {0.5, 0.7f}, (m_current_time / m_duration), m_context);

        play_tc.position =
            glm::vec3(cos_interpolation({0.5, 1.4f + 1.2f * 0.f}, button_positions[0],
                                        (m_current_time / m_duration), m_context),
                      play_tc.position.z);

        tutorial_tc.position =
            glm::vec3(cos_interpolation({0.5, 1.4f + 1.2f * 1.f}, button_positions[1],
                                        (m_current_time / m_duration), m_context),
                      tutorial_tc.position.z);

        controls_tc.position =
            glm::vec3(cos_interpolation({0.5, 1.4f + 1.2f * 2.f}, button_positions[2],
                                        (m_current_time / m_duration), m_context),
                      play_tc.position.z);

        options_tc.position =
            glm::vec3(cos_interpolation({0.5, 1.4f + 1.2f * 3.f}, button_positions[3],
                                        (m_current_time / m_duration), m_context),
                      play_tc.position.z);

        exit_tc.position =
            glm::vec3(cos_interpolation({0.5, 1.4f + 1.2f * 4.f}, button_positions[4],
                                        (m_current_time / m_duration), m_context),
                      play_tc.position.z);

        // title_tc.position =
        //     cos_interpolation({title_tc.position.x, 0.12f}, {title_tc.position.x, 0.12f},
        //                       (m_current_time / m_duration), m_context);
      }

      //
    } break;

    case Transition::WAIT: {
      int button = glfwGetMouseButton(m_window.get_window(), GLFW_MOUSE_BUTTON_LEFT);

      double xPos, yPos, width, height;
      width = m_window.get_width();
      height = m_window.get_height();

      glfwGetCursorPos(m_window.get_window(), &xPos, &yPos);

      std::string name = "";

      cor::Key key = cor::Key::create<Transform2dComponent, GraphicTextComponent, NameComponent>();
      auto entities = m_context.entity_mgr.get_entities(key);
      for (auto handle : entities) {
        cor::Entity entity = m_context.entity_mgr.get_entity(handle);
        Transform2dComponent& tc = entity;
        GraphicTextComponent& text = entity;
        text.outline_thickness = 0.05f;
        NameComponent& nc = entity;

        /*bse::Asset<gfx::Font> font("assets/font/" + text.font + ".ttf");

        glm::vec2 min;
        glm::vec2 max;
        std::tie(min, max) = font->get_bounds(text.text);

        glm::vec2 size = max - min;

        glm::vec2 center = min + size / 2.f;
        float max_size = std::max(tc.size.x, tc.size.y);
        glm::vec2 position = min - center;
        position.x *= max_size * 1080.f / 1920.f;
        position.y *= max_size;
        size.x *= max_size * 1080.f / 1920.f;
        size.y *= max_size;
        position += tc.position;
*/

        glm::vec2 position = tc.position;
        glm::vec2 size = tc.size;  //{0.3f, 0.1f};
        size.y *= .33;
        position.x -= size.x / 2.f;
        position.y -= size.y * 0.75f;

        if (xPos > position.x * width && xPos < (position.x + size.x) * width) {
          if (yPos > position.y * height && yPos < (position.y + size.y) * height) {
            name = nc.name;
            text.outline_thickness = 0.08f;
          }
        }
      }

      if (m_highlighted_entity_name == "" || m_highlighted_entity_name != name) {
        if (name != "") {
          m_select_sound->play(false);
          m_select_sound->play(true);
        }

        m_highlighted_entity_name = name;
      }

      if (button == GLFW_PRESS) {
        if (name != "") {
          if (m_clicked_entity_name != name) {
            m_click_sound->play(true);
          }
          m_clicked_entity_name = name;
        }
      } else if (button == GLFW_RELEASE) {
        if (name != "" && m_clicked_entity_name == name) {
          if (name == "Play") {
            m_context.event_mgr.send_event(MenuConfirm());
            m_scene = "default_editor_scene";
          } else if (name == "Tutorial") {
            m_context.event_mgr.send_event(MenuConfirm());
            m_scene = "tutorial";
          } else if (name == "Options") {
            toggle_options();
          } else if (name == "Controls") {
          } else if (name == "Exit") {
            m_context.event_mgr.send_event(CloseApplication());
          } else if (name == "Fullscreen") {
            toggle_fullscreen();
          } else if (name == "1440") {
            set_resolution(2560, 1440);
          } else if (name == "1080") {
            set_resolution(1920, 1080);
          } else if (name == "720") {
            set_resolution(1280, 720);
          } else if (name == "Restart") {
            m_context.event_mgr.send_event(CloseApplication());
          }
        }
        m_clicked_entity_name = "";
      }

      m_input_mgr.poll_menu_input();
      // dt = m_event_recorder.update(m_context, dt);
      if (m_current_time > m_duration) {
        m_current_time -= m_duration;
      }
      m_current_time += dt;

      if (m_current_time < m_duration / 2.f) {
        tc.transform = cos_interpolation(m_transform_from, m_transform_to,
                                         (m_current_time / (m_duration / 2.f)), m_context);

        /*text_tc.position = cos_interpolation({0.5, 0.7f}, {0.5, 0.68f},
                                             (m_current_time / (m_duration / 2.f)), m_context);*/

      } else {
        tc.transform = cos_interpolation(m_transform_to, m_transform_from,
                                         (m_current_time / (m_duration / 2.f) - 1.f), m_context);

        /*text_tc.position = cos_interpolation(
            {0.5, 0.68f}, {0.5, 0.7f}, (m_current_time / (m_duration / 2.f) - 1.f), m_context);*/
      }

      //
    } break;
    case Transition::FADE_OUT: {
      // dt = m_event_recorder.update(m_context, dt);
      //

      if (m_current_time > m_duration) {
        m_game.replace_state(std::make_unique<gmp::UnboundLoadingState>(
            m_game, m_window, m_context.audio_mgr,
            "assets/script/scene/" + m_scene + ".lua"));  //
      }
      m_current_time += dt;
      tc.transform = cos_interpolation(m_transform_from, m_transform_to,
                                       (m_current_time / m_duration), m_context);

      // text_tc.position =
      //    cos_interpolation({0.5, 0.7f}, {0.5, 2.f}, (m_current_time / m_duration), m_context);

      play_tc.position = glm::vec3(cos_interpolation(button_positions[0], {0.5, 3.4f + 0.2f * 0.f},
                                                     (m_current_time / m_duration), m_context),
                                   play_tc.position.z);

      tutorial_tc.position =
          glm::vec3(cos_interpolation(button_positions[1], {0.5, 3.4f + 0.2f * 1.f},
                                      (m_current_time / m_duration), m_context),
                    play_tc.position.z);

      controls_tc.position =
          glm::vec3(cos_interpolation(button_positions[2], {0.5, 3.4f + 0.2f * 2.f},
                                      (m_current_time / m_duration), m_context),
                    play_tc.position.z);

      options_tc.position =
          glm::vec3(cos_interpolation(button_positions[3], {0.5, 3.4f + 0.2f * 3.f},
                                      (m_current_time / m_duration), m_context),
                    play_tc.position.z);

      exit_tc.position = glm::vec3(cos_interpolation(button_positions[4], {0.5, 3.4f + 0.2f * 4.f},
                                                     (m_current_time / m_duration), m_context),
                                   play_tc.position.z);

      // title_tc.position =
      //     cos_interpolation({title_tc.position.x, 0.12f}, {title_tc.position.x, -0.2f},
      //                       (m_current_time / m_duration), m_context);

    } break;
  }

  State::update(dt);

  // LOG(NOTICE) << "\n"                                   //
  //             << tc.transform.get_position().x << ", "  //
  //             << tc.transform.get_position().y << ", "  //
  //             << tc.transform.get_position().z          //

  //             << "\n"
  //             << tc.transform.get_rotation().w << ", "  //
  //             << tc.transform.get_rotation().x << ", "  //
  //             << tc.transform.get_rotation().y << ", "  //
  //             << tc.transform.get_rotation().z          //

  //             << "\n"                       //
  //             << tc.transform.get_scale();  //

  return true;
}

bool UnboundStartState::display() {
  //
  State::display();

  return true;
}

void UnboundStartState::toggle_options() {
  auto fullscreen_entity = m_context.entity_mgr.get_entity(m_fullscreen_handle);
  auto resolution_1440_entity = m_context.entity_mgr.get_entity(m_1440_handle);
  auto resolution_1080_entity = m_context.entity_mgr.get_entity(m_1080_handle);
  auto resolution_720_entity = m_context.entity_mgr.get_entity(m_720_handle);
  auto restart_entity = m_context.entity_mgr.get_entity(m_restart_handle);

  Transform2dComponent& fullscren_tc = fullscreen_entity;
  Transform2dComponent& resolution_1440_tc = resolution_1440_entity;
  Transform2dComponent& resolution_1080_tc = resolution_1080_entity;
  Transform2dComponent& resolution_720_tc = resolution_720_entity;
  Transform2dComponent& restart_tc = restart_entity;

  if (m_options_visible) {
    fullscren_tc.position = glm::vec3(1.5, fullscren_tc.position.y, fullscren_tc.position.z);
    resolution_1440_tc.position =
        glm::vec3(1.5, resolution_1440_tc.position.y, resolution_1440_tc.position.z);
    resolution_1080_tc.position =
        glm::vec3(1.5, resolution_1080_tc.position.y, resolution_1080_tc.position.z);
    resolution_720_tc.position =
        glm::vec3(1.5, resolution_720_tc.position.y, resolution_720_tc.position.z);
    restart_tc.position = glm::vec3(1.5, restart_tc.position.y, restart_tc.position.z);
  } else {
    fullscren_tc.position = glm::vec3(0.9, fullscren_tc.position.y, fullscren_tc.position.z);
    resolution_1440_tc.position =
        glm::vec3(.9, resolution_1440_tc.position.y, resolution_1440_tc.position.z);
    resolution_1080_tc.position =
        glm::vec3(.9, resolution_1080_tc.position.y, resolution_1080_tc.position.z);
    resolution_720_tc.position =
        glm::vec3(.9, resolution_720_tc.position.y, resolution_720_tc.position.z);
    restart_tc.position = glm::vec3(.9, restart_tc.position.y, restart_tc.position.z);
  }
  m_options_visible = !m_options_visible;
}

void UnboundStartState::toggle_fullscreen() {
  auto fullscreen_entity = m_context.entity_mgr.get_entity(m_fullscreen_handle);

  GraphicTextComponent& fullscren_gtc = fullscreen_entity;

  if (m_fullscreen == 1) {
    m_fullscreen = 0;
    fullscren_gtc.color = G_TEXT_COLOR;
  } else {
    m_fullscreen = 1;
    fullscren_gtc.color = glm::vec3(255, 255, 255);
  }

  save_config();
}

void UnboundStartState::set_resolution(int width, int height) {
  auto resolution_1440_entity = m_context.entity_mgr.get_entity(m_1440_handle);
  auto resolution_1080_entity = m_context.entity_mgr.get_entity(m_1080_handle);
  auto resolution_720_entity = m_context.entity_mgr.get_entity(m_720_handle);

  GraphicTextComponent& resolution_1440_gtc = resolution_1440_entity;
  GraphicTextComponent& resolution_1080_gtc = resolution_1080_entity;
  GraphicTextComponent& resolution_720_gtc = resolution_720_entity;

  resolution_720_gtc.color = G_TEXT_COLOR;
  resolution_1080_gtc.color = G_TEXT_COLOR;
  resolution_1440_gtc.color = G_TEXT_COLOR;

  switch (height) {
    case 720:
      resolution_720_gtc.color = glm::vec3(255, 255, 255);
      break;
    case 1080:
      resolution_1080_gtc.color = glm::vec3(255, 255, 255);
      break;
    case 1440:
      resolution_1440_gtc.color = glm::vec3(255, 255, 255);
      break;
  }

  m_res_height = height;
  m_res_width = width;

  save_config();
}

void UnboundStartState::save_config() {
  std::ofstream settings("config");
  settings.is_open();
  settings << m_fullscreen << std::endl;
  settings << m_res_width << std::endl;
  settings << m_res_height << std::endl;
  settings.close();
}

}  // namespace gmp