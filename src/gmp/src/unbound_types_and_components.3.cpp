#define UNBOUND_DO_EXPOSE_TYPES
// Do NOT include anything above this line

#include <cor/cor.hpp>
#include <gmp/components/frame_tag_component.hpp>
#include <gmp/components/lifetime_component.hpp>
#include <gmp/components/model_component.hpp>
#include <gmp/components/player_damage_visual_component.hpp>
#include <gmp/components/point_light_component.hpp>
#include <gmp/components/radial_component.hpp>
#include <gmp/components/rope_component.hpp>
#include <scr/scr.tcc>

namespace gmp {
void initialize_unbound_types_and_components3() {}
}  // namespace gmp