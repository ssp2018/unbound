#include "gmp/gmp_helper_functions.hpp"

#include "cor/entity.tcc"
#include "cor/entity_handle.hpp"
#include "cor/entity_manager.tcc"
#include "gmp/components/attach_to_joint_component.hpp"
#include "gmp/components/attach_to_triangle_component.hpp"
#include "gmp/components/model_component.hpp"
#include <gmp/components/explode_component.hpp>
#include <gmp/components/lifetime_component.hpp>
#include <gmp/components/particles_component.hpp>
#include <gmp/components/physics_projectile_component.hpp>
#include <gmp/components/transform_component.hpp>

namespace gmp {

bool validate_projectile_collision(cor::Entity& e1, cor::Entity& e2) {
  if (e1.has<PhysicsProjectileComponent>()) {
    auto& first_ppc = e1.get<PhysicsProjectileComponent>();
    if (e2.has<PhysicsProjectileComponent>()) {
      auto& second_ppc = e2.get<PhysicsProjectileComponent>();
      if (second_ppc.contents.get_mass() / first_ppc.contents.get_mass() > 0.2f) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  } else {
    return true;
  }
}

cor::EntityHandle create_particle_burst(glm::vec3 pos, cor::FrameContext& context) {
  //
  cor::EntityHandle emitter_handle = context.entity_mgr.create_entity();
  cor::Entity emitter_entity = context.entity_mgr.get_entity(emitter_handle);

  ParticlesComponent pc;
  pc.spawn_duration_left = 0.05f;
  pc.spawn_per_second = 80;
  pc.min_life = 0.3f;
  pc.max_life = 0.5f;
  pc.min_start_size = 0.3f;
  pc.max_start_size = 0.4f;
  pc.min_end_size = 0.4f;
  pc.max_end_size = 0.5f;
  pc.size_mode = ParticlesComponent::Size::CHANGING;
  pc.min_spawn_pos = glm::vec3(0);
  pc.max_spawn_pos = glm::vec3(0);
  pc.velocity_mode = ParticlesComponent::Velocity::DECELERATE;

  TransformComponent tc;
  tc.transform.set_position(pos, context);

  LifetimeComponent ltc;
  ltc.time_left = 1.5f;

  emitter_entity = pc;
  emitter_entity = tc;
  emitter_entity = ltc;

  return emitter_handle;
}

cor::EntityHandle create_particle_explosion(cor::Entity& exploded_entity,
                                            cor::FrameContext& context) {
  const TransformComponent& origin_tc = exploded_entity;
  const ExplodeComponent& exc = exploded_entity;

  cor::EntityHandle emitter_handle = context.entity_mgr.create_entity();
  cor::Entity emitter_entity = context.entity_mgr.get_entity(emitter_handle);
  ParticlesComponent ppc;
  ppc.spawn_per_second = exc.particle_amount;
  ppc.min_start_color = exc.min_start_color;
  ppc.max_start_color = exc.max_start_color;
  ppc.min_end_color = exc.min_end_color;
  ppc.max_end_color = exc.max_end_color;
  ppc.min_start_velocity = glm::vec3(-exc.particle_speed);
  ppc.max_start_velocity = glm::vec3(exc.particle_speed);
  ppc.min_start_size = exc.particle_start_size * 0.9;
  ppc.max_start_size = exc.particle_start_size * 1.1;
  ppc.min_end_size = exc.particle_end_size * 0.9;
  ppc.max_end_size = exc.particle_end_size * 1.1;
  ppc.size_mode = ParticlesComponent::Size::CHANGING;
  ppc.min_life = exc.time * 0.5f;
  ppc.max_life = exc.time;
  ppc.min_spawn_pos = glm::vec3(0, 0, 0);
  ppc.max_spawn_pos = glm::vec3(0, 0, 0);
  ppc.spawn_shape = ParticlesComponent::SpawnShape::SPHERE;
  ppc.sphere_min_speed = exc.particle_speed;
  ppc.sphere_max_speed = exc.particle_speed;
  ppc.texture = exc.texture;
  ppc.velocity_mode = ParticlesComponent::Velocity::DECELERATE;
  ppc.spawn_duration_left = 0.05f;
  ppc.min_rotation_speed = -3.f;
  ppc.max_rotation_speed = 3.f;

  TransformComponent tc;
  tc.transform.set_position(origin_tc.transform.get_position(), context);

  LifetimeComponent ltc;
  ltc.time_left = exc.time + 0.2f;

  emitter_entity.attach(ppc);
  emitter_entity.attach(tc);
  emitter_entity.attach(ltc);

  return emitter_handle;
}

void destroy_arrow(cor::EntityHandle arrow_handle, cor::FrameContext& context) {
  //
  cor::Entity arrow = context.entity_mgr.get_entity(arrow_handle);
  arrow.detach<ModelComponent>();
  arrow.detach<PhysicsProjectileComponent>();
  arrow.detach<AttachToJointComponent>();
  arrow.detach<AttachToTriangleComponent>();
  arrow.get<TransformComponent>().transform.set_scale(0.8f, context);
  arrow.get<TransformComponent>().transform.set_position(glm::vec3(0, 0, -1000), context);
}

}  // namespace gmp