#include <gmp/static_component_contents.hpp>
#include <phy/collision_shapes.hpp>
// template class std::shared_ptr<phy::CollisionShape>;
namespace gmp {
const gfx::AABB& PhysicsComponentContentsBase::get_aabb() const { return m_aabb; }
void PhysicsComponentContentsBase::set_offset_transform(const glm::mat4& offset) {
  m_offset = offset;
}
glm::mat4 PhysicsComponentContentsBase::get_offset_transform() const { return m_offset; }
bool PhysicsComponentContentsBase::is_dirty() const { return m_dirty; }
void PhysicsComponentContentsBase::set_dirty(bool dirty) const { m_dirty = dirty; }

void StaticComponentContents::set_collission_shape(
    cor::FrameContext& context, const std::shared_ptr<phy::CollisionShape>& shape) {
  m_shape = shape;

  set_dirty();
}

bool StaticComponentContents::get_is_trigger() const { return m_is_trigger; }

void StaticComponentContents::set_is_trigger(bool is_trigger) {
  if (m_is_trigger != is_trigger) {
    m_is_trigger = is_trigger;
    set_dirty();
  }
}

const phy::CollisionShape* StaticComponentContents::get_collision_shape() const {
  //
  return m_shape.get();
}

}  // namespace gmp
