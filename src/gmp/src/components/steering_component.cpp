#include <cor/entity.tcc>
#include <gmp/components/steering_component.hpp>
#include <gmp/components/transform_component.hpp>
namespace gmp {
void set_goal_vec(SteeringComponent& sc, glm::vec3& goal) { sc.goal = goal; }
void set_goal_entity(SteeringComponent& sc, cor::EntityHandle& e_handle) { sc.goal = e_handle; }
glm::vec3 get_goal_pos(SteeringComponent& sc, cor::FrameContext& context) {
  glm::vec3 goal;
  if (std::holds_alternative<glm::vec3>(sc.goal)) {
    goal = std::get<glm::vec3>(sc.goal);
  } else if (std::holds_alternative<cor::EntityHandle>(sc.goal)) {
    auto& goal_handle = std::get<cor::EntityHandle>(sc.goal);
    auto goal_entity = context.entity_mgr.get_entity(goal_handle);
    if (goal_entity.is_valid()) {
      auto& goal_tc = goal_entity.get<TransformComponent>();
      goal = goal_tc.transform.get_position();
    } else {
      LOG(WARNING) << "Invalid goal entity.\n";
      goal = glm::vec3(0.0f);
    }
  } else {
    LOG(WARNING) << "Unkown goal type.\n";
    goal = glm::vec3(0.0f);
  }
  return goal;
};
}  // namespace gmp