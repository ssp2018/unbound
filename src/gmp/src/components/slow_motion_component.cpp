#include <cor/frame_context.hpp>
#include <gmp/components/slow_motion_component.hpp>

namespace gmp {

float SlowMotionFactor::get_factor() const { return m_slow_motion_factor; }
void SlowMotionFactor::set_factor(const float factor) {
  ASSERT(factor >= 0 && factor <= 1.f);
  m_slow_motion_factor = factor;
}
float SlowMotionFactor::get_converted_dt(const cor::FrameContext &context) const {
  return context.unslowed_dt * (1 - ((1 - context.speed_factor) * m_slow_motion_factor));
}

};  // namespace gmp
