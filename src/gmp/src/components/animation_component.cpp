#include <cor/entity.tcc>
#include <gmp/components/ai_component.hpp>
#include <gmp/components/animation_component.hpp>
#include <gmp/components/character_controller_component.hpp>
#include <gmp/components/player_component.hpp>

namespace gmp {
const CharacterControllerComponent* ScriptEntity::get_char_ctrl_component() const {
  return entity.get_if<CharacterControllerComponent>();
}

const AIComponent* ScriptEntity::get_ai_component() const { return entity.get_if<AIComponent>(); }
const PlayerComponent* ScriptEntity::get_player_component() const {
  return entity.get_if<PlayerComponent>();
}
// Holds animations

}  // namespace gmp
