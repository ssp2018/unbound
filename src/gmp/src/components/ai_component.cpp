#include "gmp/components/ai_component.hpp"

#include "gmp/components/transform_component.hpp"  // for Transform, Transfo...
#include <gmp/components/transform_component.hpp>
void gmp::AIComponentFunctions::go_to(float x, float y, float z) {
  m_path_finding.m_destination = {x, y, z};
  m_path_finding.m_need_path = true;
  m_path_finding.m_on_the_move = true;
}

bool gmp::AIComponentFunctions::is_moving() { return m_path_finding.m_on_the_move; }

float gmp::AIComponentFunctions::get_distance(gmp::TransformComponent& t_c_1,
                                              gmp::TransformComponent& t_c_2) {
  glm::vec3 tmp = (t_c_1.transform.get_position() - t_c_2.transform.get_position());
  return std::sqrt(tmp.x * tmp.x + tmp.y * tmp.y + tmp.z * tmp.z);
}

float gmp::AIComponentFunctions::distance(glm::vec2 pos, glm::vec2 other) {
  glm::vec2 tmp = pos - other;
  float sqrt = std::sqrt(tmp.x * tmp.x + tmp.y * tmp.y);
  return sqrt;
}

gmp::MoraleData::MoraleData(AIType type) {
  if (gmp::AIType::MELEE_GRUNT == type) {
    morale_boost = 0;
    morale_loss_per_damage = 20;
    morale_regeneration_val = 5;
    morale_flee_threshold = 30;
    morale_reenter_combat_threshold = 70;

    morale_loss_ally_die = 30;
    morale_loss_ally_hit = 10;
    morale_loss_ally_flee = 20;
    morale_boost_from_group = 10;
    morale_boost_dealing_damage = 30;

  } else if (gmp::AIType::GOLEM == type) {
    morale_boost = 0;
    morale_loss_per_damage = 0;
    morale_regeneration_val = 100;
    morale_flee_threshold = 0;
    morale_reenter_combat_threshold = 100;

    morale_loss_ally_die = 0;
    morale_loss_ally_hit = 0;
    morale_loss_ally_flee = 0;
    morale_boost_from_group = 100;
    morale_boost_dealing_damage = 100;
  } else {
  }
}

// this was apparently a bad idea...
// scr::DynamicObject gmp::AIComponentFunctions::distance_2(scr::DynamicObject lhs,
//                                                         scr::DynamicObject rhs) {
//  /*DynamicObject can be:
//   * std::nullptr_t
//   * double
//   * std::string
//   * Userdata
//   * OwningUserdata
//   * bool
//   * DynamicTable
//   */
//
//  if (!lhs.is_userdata() || !rhs.is_userdata()) {
//    return nullptr;
//  }
//
//  scr::DynamicObject::Userdata lhs_data = lhs;
//  scr::DynamicObject::Userdata rhs_data = rhs;
//  glm::vec3 pos[2];
//
//  for (int i = 0; i < 2; ++i) {
//    scr::DynamicObject::Userdata curr_data = (i == 0 ? lhs : rhs);
//    scr::DynamicObject curr_obj = (i == 0 ? lhs : rhs);
//
//    if (curr_data.type_name == "vec3") {
//      pos[i] = curr_obj;
//    } else if (curr_data.type_name == "vec2") {
//      pos[i] = glm::vec3({(glm::vec2)curr_obj, 0});
//    } else if (curr_data.type_name == "TransposeComponent") {
//      gmp::TransformComponent& tc = (gmp::TransformComponent)curr_obj;
//    } else {
//      return nullptr;
//    }
//  }
//
//  glm::vec3 tmp = pos[0] - pos[1];
//  return std::sqrt(tmp.x * tmp.x + tmp.y * tmp.y);
//}