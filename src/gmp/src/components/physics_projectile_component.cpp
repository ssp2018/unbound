#include <gmp/components/physics_projectile_component.hpp>

namespace gmp {
glm::vec3 PhysicsProjectileComponentContents::get_velocity() const { return m_velocity; }
void PhysicsProjectileComponentContents::set_velocity(glm::vec3 velocity) {
  m_velocity = velocity;
  set_dirty();
}

glm::vec3 PhysicsProjectileComponentContents::get_gravity() const { return m_gravity; }
void PhysicsProjectileComponentContents::set_gravity(glm::vec3 gravity) {
  m_gravity = gravity;
  set_dirty();
}

float PhysicsProjectileComponentContents::get_length() const { return m_length; }
void PhysicsProjectileComponentContents::set_length(float length) {
  m_length = length;
  set_dirty();
}

float PhysicsProjectileComponentContents::get_radius() const { return m_radius; }

void PhysicsProjectileComponentContents::set_radius(float radius) {
  m_radius = radius;
  set_dirty();
}

float PhysicsProjectileComponentContents::get_mass() const { return m_mass; }

void PhysicsProjectileComponentContents::set_mass(float mass) {
  m_mass = mass;
  set_dirty();
}

bool PhysicsProjectileComponentContents::get_has_hit() const { return m_has_hit; }

void PhysicsProjectileComponentContents::set_has_hit(bool hit) { m_has_hit = hit; }

void PhysicsProjectileComponentContents::disable_collision_reports_for(float duration) {
  if (duration > m_disable_reporting_time) {
    m_disable_reporting_time = duration;
  }
}

void PhysicsProjectileComponentContents::enable_collision_reports() {
  m_disable_reporting_time = 0.0f;
}

void PhysicsProjectileComponentContents::disable_destruction_for(float duration) {
  if (duration > m_disable_destruction_time) {
    m_disable_destruction_time = duration;
  }
}

void PhysicsProjectileComponentContents::enable_destruction() { m_disable_destruction_time = 0.0f; }

}  // namespace gmp
