#include <gfx/aabb.hpp>
#include <gmp/components/sticky_triangle_component.hpp>
#include <gmp/transform.hpp>

namespace gmp {

gfx::AABB StickyTriangleComponent::get_aabb(const Transform& transform) {
  glm::mat4 model = transform.get_model_matrix();

  std::array<glm::vec3, 3> points = {glm::vec3(model[0]), glm::vec3(model[1]), glm::vec3(model[2])};

  glm::vec3 average = (points[0] + points[1] + points[2]) / 3.f;

  float max_distance_sqrd = 0.f;
  for (const glm::vec3& p : points) {
    max_distance_sqrd = std::max(max_distance_sqrd, glm::length2(p - average));
  }

  gfx::AABB aabb;
  aabb.position = average;
  float max_distance = std::sqrt(max_distance_sqrd);
  aabb.size = {max_distance, max_distance, max_distance};
  return aabb;
}

}  // namespace gmp
