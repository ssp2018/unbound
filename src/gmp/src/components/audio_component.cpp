#include "gmp/components/audio_component.hpp"

#include "aud/sound_buffer.hpp"    // for SoundBuffer
#include "aud/sound_source.hpp"    // for SoundSource, SoundSettings
#include "bse/asset.hpp"           // for Asset
#include "bse/directory_path.hpp"  // for ASSETS_ROOT, DirectoryPath
#include "bse/log.hpp"             // for WARNING, Log, LOG
namespace gmp {
AudioClass::AudioClass() {
  auto err = bse::Asset<aud::SoundBuffer>(bse::ASSETS_ROOT / "audio/errors/1.ogg"_fp);
  m_collision_sound_effects[MaterialType::INVALID] = SoundWithSettings(err, aud::SoundSettings());
}

AudioClass::~AudioClass() { sources.clear(); }

AudioClass::AudioClass(AudioClass&& other) {
  sources = std::move(other.sources);
  m_collision_sound_effects = std::move(other.m_collision_sound_effects);
  m_periodic_sound_groups = std::move(other.m_periodic_sound_groups);
  // m_looping_sound_groups = std::move(other.m_looping_sound_groups);
  // m_sound_groups = std::move(other.m_sound_groups);
  // m_delay_start_sound_vector = std::move(other.m_delay_start_sound_vector);
}

AudioClass& AudioClass::operator=(AudioClass&& other) {
  if (&other != this) {
    sources = std::move(other.sources);
    m_collision_sound_effects = std::move(other.m_collision_sound_effects);
    m_periodic_sound_groups = std::move(other.m_periodic_sound_groups);
    // m_looping_sound_groups = std::move(other.m_looping_sound_groups);
    // m_sound_groups = std::move(other.m_sound_groups);
    // m_delay_start_sound_vector = std::move(other.m_delay_start_sound_vector);
  }

  return *this;
}

void AudioClass::set_collision_sound(
    bse::Asset<aud::SoundBuffer> asset,
    const std::vector<std::pair<MaterialType, aud::SoundSettings>>& args) {
  for (auto arg : args) {
    m_collision_sound_effects[arg.first] = SoundWithSettings(asset, arg.second);
  }

  /* m_collision_sound_effects[hit_type_1] = SoundWithSettings(asset, settings);
   if (hit_type_2 != MaterialSound::INVALID)
     m_collision_sound_effects[hit_type_2] = SoundWithSettings(asset, settings);
   if (hit_type_3 != MaterialSound::INVALID)
     m_collision_sound_effects[hit_type_3] = SoundWithSettings(asset, settings);
   if (hit_type_4 != MaterialSound::INVALID)
     m_collision_sound_effects[hit_type_4] = SoundWithSettings(asset, settings);
   if (hit_type_5 != MaterialSound::INVALID)
     m_collision_sound_effects[hit_type_5] = SoundWithSettings(asset, settings);
   if (hit_type_6 != MaterialSound::INVALID)
     m_collision_sound_effects[hit_type_6] = SoundWithSettings(asset, settings);
   if (hit_type_7 != MaterialSound::INVALID)
     m_collision_sound_effects[hit_type_7] = SoundWithSettings(asset, settings);
   if (hit_type_8 != MaterialSound::INVALID)
     m_collision_sound_effects[hit_type_8] = SoundWithSettings(asset, settings);*/
}

void AudioClass::play_collision_sound(MaterialType hit_type, glm::vec3 pos) {
  bse::RuntimeAsset<aud::SoundSource> effect;

  if (hit_type == MaterialType::INVALID ||
      m_collision_sound_effects.find(hit_type) == m_collision_sound_effects.end()) {
    if (m_collision_sound_effects.find(MaterialType::GENERIC) == m_collision_sound_effects.end()) {
      return;
    } else {
      effect->attach_buffer(m_collision_sound_effects[MaterialType::GENERIC].buffer);
      effect->apply_sound_settings(m_collision_sound_effects[MaterialType::GENERIC].settings);
    }
  } else {
    effect->attach_buffer(m_collision_sound_effects[hit_type].buffer);
    effect->apply_sound_settings(m_collision_sound_effects[hit_type].settings);
  }

  effect->play(true);
  sources.push_back(std::move(effect));
}

void AudioClass::set_intervall_time_limit(std::string group, float val) {
  // If the sound group exists
  if (m_periodic_sound_groups.find(group) != m_periodic_sound_groups.end()) {
    m_periodic_sound_groups[group].silent_intervall_time = val;
  } else {
    LOG(WARNING) << "There does not exist a looping sound group with the name: " << group << "\n";
  }
}

void AudioClass::set_looping_sound_group_enabled(std::string group, bool val) {
  // If the sound group exists
  if (m_periodic_sound_groups.find(group) != m_periodic_sound_groups.end()) {
    m_periodic_sound_groups[group].active = val;
  } else {
    LOG(WARNING) << "There does not exist a looping sound group with the name: " << group << "\n";
  }
}

void AudioClass::add_periodic_sound_group(std::string group) {
  // check if the periodic sound group already exists
  if (m_periodic_sound_groups.find(group) == m_periodic_sound_groups.end()) {
    // add a empty periodic sound group
    m_periodic_sound_groups[group] = std::move(LoopingSoundSettings());
  } else {
    LOG(WARNING) << "There already exists a looping sound group with the name: " << group << "\n";
  }
}

void AudioClass::set_ss_periodic_sound(std::string group,
                                       aud::SoundSettings settings) {  // If the sound group exists
  if (m_periodic_sound_groups.find(group) != m_periodic_sound_groups.end()) {
    // add the sound data to the sound group
    m_periodic_sound_groups[group].settings = settings;

  } else {
    LOG(WARNING) << "There does not exist a sound group with the name: " << group << "\n";
  }
}

void AudioClass::play_sound_delayed(DelayedSoundInfo info) { m_play_sound_vector.push_back(info); }

std::unordered_map<MaterialType, SoundWithSettings> AudioClass::get_collission_sound_effects() {
  return std::move(m_collision_sound_effects);
}

void AudioClass::set_collission_sound_effects(
    std::unordered_map<MaterialType, SoundWithSettings> col_set) {
  m_collision_sound_effects = std::move(col_set);
}

std::unordered_map<std::string, LoopingSoundSettings> AudioClass::get_periodic_sound_groups() {
  return std::move(m_periodic_sound_groups);
}

void AudioClass::set_periodic_sound_groups(
    std::unordered_map<std::string, LoopingSoundSettings> per_snd_grps) {
  m_periodic_sound_groups = std::move(per_snd_grps);
}

std::vector<DelayedSoundInfo> AudioClass::get_delayed_sounds() {
  return std::move(m_play_sound_vector);
}

void AudioClass::set_delayed_sounds(std::vector<DelayedSoundInfo> delayed_snds) {
  m_play_sound_vector = std::move(delayed_snds);
}

}  // namespace gmp