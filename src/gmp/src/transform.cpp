#include "cor/frame_context.hpp"  // for FrameContext
#include <gmp/transform.hpp>      // for Transform
namespace gmp {
glm::mat4 Transform::get_model_matrix() const {
  if (m_dirty) {
    m_model_matrix = glm::translate(glm::mat4(1.0f), m_position) * glm::toMat4(m_rotation) *
                     glm::scale(glm::mat4(1.0f), m_scale);
    m_dirty = false;
  }
  return m_model_matrix;
}
glm::vec3 Transform::get_position() const { return m_position; }
glm::quat Transform::get_rotation() const { return m_rotation; }
float Transform::get_scale() const { return m_scale.x; }
glm::vec3 Transform::get_scale_vector() const { return m_scale; }

glm::vec3 Transform::get_forward() const { return get_model_matrix() * glm::vec4(0, 1, 0, 0); }

glm::vec3 Transform::get_right() const { return get_model_matrix() * glm::vec4(1, 0, 0, 0); }

glm::vec3 Transform::get_up() const { return get_model_matrix() * glm::vec4(0, 0, 1, 0); }

uint16_t Transform::get_updated_index() const { return m_frame_updated; }

bool Transform::get_physics_dirty() const { return m_physics_dirty; }

void Transform::set_model_matrix(glm::mat4 matrix, const cor::FrameContext& context) {
  set_model_matrix_no_update(matrix, context);
  m_physics_dirty = true;
  m_frame_updated = context.frame_num;
}

void Transform::set_model_matrix_no_update(glm::mat4 matrix, const cor::FrameContext& context) {
  m_model_matrix = matrix;

  glm::quat orientation;
  glm::vec3 skew;
  glm::vec4 persp;

  glm::decompose(m_model_matrix, m_scale, m_rotation, m_position, skew, persp);
  m_frame_updated = context.frame_num - 1;
}

void Transform::set_position(glm::vec3 position, const cor::FrameContext& context) {
  m_position = position;
  m_dirty = true;
  m_physics_dirty = true;
  m_frame_updated = context.frame_num;
}
void Transform::set_rotation(glm::quat rotation, const cor::FrameContext& context) {
  m_rotation = rotation;
  m_dirty = true;
  m_physics_dirty = true;
  m_frame_updated = context.frame_num;
}
void Transform::set_rotation_vec(glm::vec3 axis, float radians, const cor::FrameContext& context) {
  m_rotation = glm::angleAxis(radians, axis);
  m_dirty = true;
  m_physics_dirty = true;
  m_frame_updated = context.frame_num;
}
void Transform::set_scale(float scale, const cor::FrameContext& context) {
  m_scale = glm::vec3(scale, scale, scale);
  m_dirty = true;
  m_physics_dirty = true;
  m_frame_updated = context.frame_num;
}
void Transform::set_scale_vector(glm::vec3 scale, const cor::FrameContext& context) {
  m_scale = scale;
  m_dirty = true;
  m_physics_dirty = true;
  m_frame_updated = context.frame_num;
}

void Transform::translate(glm::vec3 move_vector, const cor::FrameContext& context) {
  m_position += move_vector;
  m_dirty = true;
  m_physics_dirty = true;
  m_frame_updated = context.frame_num;
}

void Transform::rotate(glm::quat rotation, const cor::FrameContext& context) {
  m_rotation = m_rotation * rotation;
  m_dirty = true;
  m_physics_dirty = true;
  m_frame_updated = context.frame_num;
}

void Transform::rotate(glm::vec3 axis, float radians, const cor::FrameContext& context) {
  m_rotation = glm::rotate(m_rotation, radians, axis);
  m_dirty = true;
  m_physics_dirty = true;
  m_frame_updated = context.frame_num;
}

void Transform::scale(float scale, const cor::FrameContext& context) {
  m_scale *= scale;
  m_dirty = true;
  m_physics_dirty = true;
  m_frame_updated = context.frame_num;
}

uint16_t Transform::get_frame_updated() const { return m_frame_updated; }

void Transform::force_dirty(const cor::FrameContext& context) {
  m_dirty = true;
  m_physics_dirty = true;
  m_frame_updated = context.frame_num;
}

void Transform::clean_physics() const { m_physics_dirty = false; }

}  // namespace gmp