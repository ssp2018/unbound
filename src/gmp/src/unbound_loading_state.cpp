#include "gmp/unbound_loading_state.hpp"

#include <bse/edit.hpp>
#include <bse/task_scheduler.hpp>
#include <cor/entity.tcc>
#include <cor/entity_manager.tcc>
#include <gmp/components/graphic_text_component.hpp>
#include <gmp/components/name_component.hpp>
#include <gmp/components/projection_component.hpp>
#include <gmp/components/saturation_component.hpp>
#include <gmp/components/texture_component.hpp>
#include <gmp/components/transform_2d_component.hpp>
#include <gmp/components/transform_component.hpp>
#include <gmp/editor/scene.hpp>
#include <gmp/systems/graphics_system.hpp>
#include <gmp/unbound.hpp>
#include <gmp/unbound_game_state.hpp>

namespace gmp {

UnboundLoadingState::UnboundLoadingState(cor::Game& game, gfx::Window& window,
                                         aud::AudioManager& audio_mgr,
                                         const std::string& scene_path)
    : State(game, window, audio_mgr), m_loaded_context{audio_mgr}, m_scene_path(scene_path) {
  //
  initialize();
  bse::Edit::disable();
}

UnboundLoadingState::~UnboundLoadingState() {
  //
}

void UnboundLoadingState::initialize() {
  //
  {
    //
    cor::Entity background = m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());

    const float scale = 0.025f;
    const float aspect = (float)m_window.get_width() / m_window.get_height();

    gmp::Transform2dComponent t2dc;
    t2dc.position = glm::vec3(0.5f, 0.5f, 0);
    t2dc.size = glm::vec2(1.f, 1.f);
    t2dc.rotation = 0.f;

    background = t2dc;

    gmp::TextureComponent texc;
    texc.texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/SSPposter_CROPPED.png"_fp);

    background = texc;
  }

  // {
  //   //
  //   cor::Entity title = m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());

  //   const float scale = 0.025f;
  //   const float aspect = (float)m_window.get_width() / m_window.get_height();

  //   gmp::Transform2dComponent t2dc;
  //   t2dc.position = glm::vec3(0.25, 0.1f, 1);
  //   t2dc.size = glm::vec2(0.13f * 1.817f * aspect, 0.13f);
  //   t2dc.rotation = 0.f;

  //   title = t2dc;

  //   gmp::TextureComponent texc;
  //   texc.texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/title.png"_fp);

  //   title = texc;
  // }

  // {
  //   cor::Entity loading_text =
  //       m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());

  //   const float aspect = (float)m_window.get_width() / m_window.get_height();

  //   gmp::Transform2dComponent t2dc;
  //   t2dc.position = glm::vec3(0.5, 0.8, 1);
  //   t2dc.size = glm::vec2(.3 / aspect, .1);
  //   t2dc.rotation = 0.f;

  //   loading_text = t2dc;

  //   gmp::GraphicTextComponent gtc;
  //   gtc.text = "Loading...";
  //   gtc.font = "arial";
  //   gtc.color = glm::vec3(255, 255, 255);

  //   loading_text = gtc;
  // }

  m_swirl = m_context.entity_mgr.create_entity();
  {
    cor::Entity swirl = m_context.entity_mgr.get_entity(m_swirl);
    //

    const float aspect = (float)m_window.get_width() / m_window.get_height();

    gmp::Transform2dComponent t2dc;
    t2dc.position = glm::vec3(0.9, 0.8, 1);
    t2dc.size = glm::vec2(0.2f / aspect, 0.2f);
    t2dc.rotation = 2.f;

    swirl = t2dc;

    gmp::TextureComponent texc;
    texc.texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/swirl.png"_fp);

    swirl = texc;
  }

  {
    //
    cor::Entity camera = m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());

    gmp::TransformComponent tc;
    // tc.transform = m_transform_from;
    camera = tc;

    gmp::ProjectionComponent pc;
    pc.aspectRatio = (float)m_window.get_width() / m_window.get_height();
    pc.near = 0.1f;
    pc.far = 1500.f;
    pc.field_of_view = 80.f;
    pc.projection_matrix =
        glm::perspective(glm::radians(pc.field_of_view), pc.aspectRatio, pc.near, pc.far);
    camera = pc;
  }

  m_system_mgr.add_system<gmp::GraphicsSystem>(m_context, &m_window);

  m_input_mgr.unlock_mouse();
}

bool UnboundLoadingState::update(float dt) {
  //
  cor::Entity swirl = m_context.entity_mgr.get_entity(m_swirl);
  Transform2dComponent& t2dc = swirl;
  t2dc.rotation += -360.0f * dt;

  State::update(dt);

  if (m_phase == 3) {
    struct LoadingTask : bse::Task {
      LoadingTask(std::function<void(void)> actual_kernel) : m_actual_kernel{actual_kernel} {};

      void kernel() override { m_actual_kernel(); }

      std::function<void(void)> m_actual_kernel;
    };

    LoadingTask task{[this]() {
      {
        sol::table loaded_scene = gmp::Scene::load(m_scene_path);
        gmp::Scene::upload(loaded_scene, m_loaded_context);

        cor::Entity camera = m_context.entity_mgr.get_entity(m_context.entity_mgr.get_entities(
            cor::Key::create<ProjectionComponent, TransformComponent>())[0]);
        cor::Entity camera_copy =
            m_loaded_context.entity_mgr.get_entity(m_loaded_context.entity_mgr.create_entity());
        camera_copy = camera.get<ProjectionComponent>();
        ProjectionComponent& pc = camera_copy;
        pc.aspectRatio = 1280.f / 720.f;
        pc.field_of_view = 90;
        pc.near = 0.1;
        pc.far = 1000;
        camera_copy = camera.get<TransformComponent>();

        NameComponent& nc = camera_copy.attach<NameComponent>();
        nc.name = "main_camera";
      }

      {
        cor::Entity saturation_entity =
            m_loaded_context.entity_mgr.get_entity(m_loaded_context.entity_mgr.create_entity());
        SaturationComponent comp;
        comp.saturation = 1;
        saturation_entity = comp;
      }

      m_loaded_state = std::make_unique<gmp::UnboundGameState>(
          m_game, m_window, m_context.audio_mgr, std::move(m_loaded_context));
    }};

    m_loading_task = bse::TaskScheduler::get()->add_task(task);
  }

  if (m_phase > 3 && bse::TaskScheduler::get()->is_finished(m_loading_task)) {
    m_game.replace_state(std::move(m_loaded_state));
  }

  m_phase += 1;

  return true;
}  // namespace gmp

bool UnboundLoadingState::display() {
  //
  State::display();
  return true;
}

}  // namespace gmp