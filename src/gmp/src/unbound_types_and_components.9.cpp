#define UNBOUND_DO_EXPOSE_TYPES
// Do NOT include anything above this line

#include <cor/cor.hpp>
#include <cor/event.hpp>
#include <cor/event_manager.hpp>
#include <cor/event_variant.hpp>
#include <gmp/components/shield_effect_component.hpp>
#include <gmp/components/texture_component.hpp>
#include <gmp/components/threat_component.hpp>
#include <gmp/components/transforms_component.hpp>
UNBOUND_TYPE((TestEvent), num, str)
UNBOUND_TYPE((PlaySoundEvent), id, buffer_path, settings)
UNBOUND_TYPE((PlaySoundDelayedEvent), id, buffer_path, settings, delay_time)
UNBOUND_TYPE((PlayerZeroHealth))
UNBOUND_TYPE((KilledSkullEvent))
UNBOUND_TYPE((KilledColeEvent))
UNBOUND_TYPE((KilledGolemEvent), golem_handle, killing_arrow_handle, arrow_frame_num,
             arrow_final_point)
UNBOUND_TYPE((TookHornNoteEvent))
UNBOUND_TYPE((TookHornEvent))
UNBOUND_TYPE((WeakSpotHit), target_entity, hit_point, hit_point_normal)
UNBOUND_TYPE((CollisionEvent), entity_1, entity_2, hit_point, hit_point_normal)
#include <scr/scr.tcc>
UNBOUND_TYPE((AddExplosiveArrowsEvent))
UNBOUND_TYPE((SpawnHealthOrb), position, delay)
UNBOUND_TYPE((AnimationEvent), animation_name, handle, blend_time)

namespace gmp {
void initialize_unbound_types_and_components9() {
  scr::expose_type<cor::EventManager>(
      "EventManager", scr::Constructors<cor::EventManager()>(), "send_event",
      [](cor::EventManager& manager, const decltype(Event::data)& e) { manager.send_event(e); });
}
}  // namespace gmp