#define UNBOUND_DO_EXPOSE_TYPES
// Do NOT include anything above this line

#include <cor/cor.hpp>
#include <gmp/components/continue_movement_after_impact_component.hpp>
#include <gmp/components/debug_camera_component.hpp>
#include <gmp/components/destroyed_component.hpp>
#include <gmp/components/directional_light_component.hpp>
#include <gmp/components/particles_component.hpp>
#include <gmp/components/physics_projectile_component.hpp>
#include <gmp/components/quest_component.hpp>
#include <scr/scr.tcc>

namespace gmp {
void initialize_unbound_types_and_components2() {}
}  // namespace gmp