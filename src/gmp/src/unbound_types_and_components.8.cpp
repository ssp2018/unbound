#define UNBOUND_DO_EXPOSE_TYPES
// Do NOT include anything above this line

//#pragma warning(push, 0)
#include <aud/sound_buffer.hpp>
#include <bse/asset.hpp>
#include <cor/cor.hpp>
#include <gfx/font.hpp>
#include <gfx/material.hpp>
#include <gfx/mesh.hpp>
#include <gfx/model.hpp>
#include <gfx/shader.hpp>
#include <gfx/texture.hpp>
#include <gmp/audio_script_wrapper.hpp>
#include <gmp/components/fracture_on_impact_component.hpp>
#include <gmp/components/transform_2d_component.hpp>
#include <gmp/components/trigger_script_component.hpp>
#include <gmp/external_types.hpp>
#include <scr/scr.tcc>

UNBOUND_TYPE((bse::FilePath), get_string)

template <typename T>
void expose_asset(std::string_view name) {
  scr::expose_type<T>(name, scr::Constructors<T(), T(bse::FilePath)>(), "get_path", &T::get_path);
}

namespace gmp {
void initialize_unbound_types_and_components8() {
  scr::expose_type<bse::FilePath>("FilePath",
                                  scr::Constructors<bse::FilePath(), bse::FilePath(std::string)>(),
                                  "get_string", &bse::FilePath::get_string);

  expose_asset<bse::Asset<gfx::Model>>("ModelAsset");
  expose_asset<bse::Asset<gfx::Texture>>("TextureAsset");
  expose_asset<bse::Asset<gfx::Material>>("MaterialAsset");
  expose_asset<bse::Asset<gfx::Mesh>>("MeshAsset");
  expose_asset<bse::Asset<gfx::Font>>("FontAsset");
  expose_asset<bse::Asset<gfx::Shader>>("ShaderAsset");
  expose_asset<bse::Asset<aud::SoundBuffer>>("SoundBufferAsset");
}
}  // namespace gmp
   //#pragma warning(pop)