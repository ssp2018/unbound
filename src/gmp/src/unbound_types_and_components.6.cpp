#define UNBOUND_DO_EXPOSE_TYPES
// Do NOT include anything above this line

#include <cor/cor.hpp>
#include <gmp/components/force_move_to_entity_component.hpp>
#include <gmp/components/force_move_to_position_component.hpp>
#include <gmp/components/graphic_text_component.hpp>
#include <gmp/components/hook_component.hpp>
#include <gmp/components/material_component.hpp>
#include <gmp/components/name_component.hpp>
#include <gmp/components/saturation_component.hpp>
#include <gmp/editor_components/mesh_node_component.hpp>
#include <scr/scr.tcc>
namespace gmp {
void initialize_unbound_types_and_components6() { ; }
}  // namespace gmp