
#define UNBOUND_DO_EXPOSE_TYPES
// Do NOT include anything above this line

#include <cor/cor.hpp>
#include <cor/frame_context.hpp>
#include <gmp/ai_script_entity_wrapper.hpp>
#include <gmp/components/ai_force_state_change_component.hpp>
#include <gmp/components/cole_component.hpp>
#include <gmp/components/destructible_component.hpp>
#include <gmp/components/faction_component.hpp>
#include <gmp/components/health_component.hpp>
#include <gmp/components/homing_component.hpp>
#include <gmp/components/hook_hit_component.hpp>
#include <scr/dynamic_object.hpp>
#include <scr/scr.tcc>

namespace gmp {
void initialize_unbound_types_and_components1() {}
}  // namespace gmp