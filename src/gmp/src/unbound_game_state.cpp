#include "gmp/unbound_game_state.hpp"

#include "gmp/components/animation_component.hpp"
#include "gmp/components/attached_handles_component.hpp"
#include "gmp/components/debug_camera_component.hpp"
#include "gmp/components/destroyed_component.hpp"
#include "gmp/components/destructible_component.hpp"
#include "gmp/components/directional_light_component.hpp"
#include "gmp/components/explode_component.hpp"
#include "gmp/components/highlight_component.hpp"
#include "gmp/components/lifetime_component.hpp"
#include "gmp/components/material_component.hpp"
#include "gmp/components/model_component.hpp"
#include "gmp/components/particles_component.hpp"
#include "gmp/components/pickup_component.hpp"
#include "gmp/components/player_component.hpp"
#include "gmp/components/projection_component.hpp"
#include "gmp/components/radial_component.hpp"
#include "gmp/components/rotate_z_component.hpp"
#include "gmp/components/slow_motion_component.hpp"
#include "gmp/components/transform_component.hpp"
#include "gmp/gmp_helper_functions.hpp"
#include "gmp/systems/attach_update_system.hpp"
#include "gmp/systems/cloud_system.hpp"
#include "gmp/systems/debug_camera_system.hpp"
#include "gmp/systems/destruction_system.hpp"
#include "gmp/systems/fall_damage_system.hpp"
#include "gmp/systems/graphics_system.hpp"
#include "gmp/systems/particle_system.hpp"
#include "gmp/systems/pause_menu_system.hpp"
#include "gmp/systems/physics_system.hpp"
#include "gmp/systems/pickup_system.hpp"
#include "gmp/systems/player_system.hpp"
#include "gmp/systems/quest_system.hpp"
#include "gmp/systems/rotation_system.hpp"
#include "gmp/systems/trigger_system.hpp"
#include <bse/asset.hpp>
#include <bse/edit.hpp>
#include <bse/log.hpp>
#include <cor/cor.hpp>
#include <cor/entity.tcc>
#include <cor/entity_manager.tcc>
#include <cor/event_variant.hpp>
#include <cor/game.hpp>
#include <cor/script_type.hpp>
#include <ext/ext.hpp>
#include <gmp/audio_script_wrapper.hpp>
#include <gmp/components/ai_component.hpp>
#include <gmp/components/ai_force_state_change_component.hpp>
#include <gmp/components/apply_damage_over_time_component.hpp>
#include <gmp/components/attach_to_entity_component.hpp>
#include <gmp/components/attach_to_joint_component.hpp>
#include <gmp/components/audio_component.hpp>
#include <gmp/components/character_controller_component.hpp>
#include <gmp/components/cole_component.hpp>
#include <gmp/components/continue_movement_after_impact_component.hpp>
#include <gmp/components/damage_component.hpp>
#include <gmp/components/dynamic_component.hpp>
#include <gmp/components/faction_component.hpp>
#include <gmp/components/force_move_to_entity_component.hpp>
#include <gmp/components/force_move_to_position_component.hpp>
#include <gmp/components/graphic_text_component.hpp>
#include <gmp/components/health_component.hpp>
#include <gmp/components/hook_component.hpp>
#include <gmp/components/name_component.hpp>
#include <gmp/components/physics_projectile_component.hpp>
#include <gmp/components/player_damage_visual_component.hpp>
#include <gmp/components/point_light_component.hpp>
#include <gmp/components/rope_component.hpp>
#include <gmp/components/saturation_component.hpp>
#include <gmp/components/static_component.hpp>
#include <gmp/components/steering_component.hpp>
#include <gmp/components/take_damage_over_time_component.hpp>
#include <gmp/components/texture_component.hpp>
#include <gmp/components/threat_component.hpp>
#include <gmp/components/transform_2d_component.hpp>
#include <gmp/components/transforms_component.hpp>
#include <gmp/components/trigger_script_component.hpp>
#include <gmp/death_callbacks.hpp>
#include <gmp/event_recorder.hpp>
#include <gmp/external_types.hpp>
#include <gmp/systems/ai_combat_system.hpp>
#include <gmp/systems/ai_system.hpp>
#include <gmp/systems/attach_update_system.hpp>
#include <gmp/systems/audio_system.hpp>
#include <gmp/systems/crowd_control_system.hpp>
#include <gmp/systems/cutscene_system.hpp>
#include <gmp/systems/health_system.hpp>
#include <gmp/systems/homing_system.hpp>
#include <gmp/systems/hud_system.hpp>
#include <gmp/systems/replay_system.hpp>
#include <gmp/systems/spawn_system.hpp>
#include <gmp/systems/special_effects_system.hpp>
#include <gmp/systems/test_system.hpp>
#include <gmp/unbound_editor_state.hpp>
#include <gmp/unbound_pause_state.hpp>
#include <gmp/unbound_start_state.hpp>
#include <phy/collision_mesh.hpp>
#include <scr/dynamic_object.hpp>
#include <scr/object.hpp>
#include <scr/script.hpp>
#ifdef UNBOUND_CUSTOM_ALLOCATION
#include <bse/memory_debug.hpp>
#endif

namespace gmp {

UnboundGameState::UnboundGameState(cor::Game& game, gfx::Window& window,
                                   aud::AudioManager& audio_mgr)
    : State(game, window, audio_mgr), m_event_recorder{m_context} {
  //
  m_context.audio_mgr = audio_mgr;

  initialize();
  bse::Edit::disable();
}

UnboundGameState::UnboundGameState(cor::Game& game, gfx::Window& window,
                                   aud::AudioManager& audio_mgr, cor::FrameContext&& context)
    : State(game, window, audio_mgr), m_event_recorder{m_context} {
  //
  m_context.entity_mgr = std::move(context.entity_mgr);
  m_context.navmeshes = std::move(context.navmeshes);
  initialize();
  bse::Edit::disable();
}

UnboundGameState::~UnboundGameState() {
  //
}

void UnboundGameState::initialize() {
  aud::SoundSettings settings;
  settings.loop = true;
  settings.gain = 0.1f;
  settings.rolloff_factor = 0.f;
  m_ambient = std::move(bse::RuntimeAsset<aud::SoundSource>(
      bse::Asset<aud::SoundBuffer>("assets/audio/effects/ambient/ambient_forest.ogg"), settings));
  m_ambient->play(false);
  m_ambient->play(true);

  //
  GraphicTextComponent standard_text;
  standard_text.outline_color = {54, 13, 37};
  standard_text.color = {189, 41, 72};
  standard_text.outline_thickness = 0.05f;
  standard_text.font = "arial";

  // Listeners
  m_bse_edit_listener = m_context.event_mgr.register_handler<ToggleBseEditEvent>(
      [](const ToggleBseEditEvent* event) { bse::Edit::toggle(); });

  m_close_listener =
      m_context.event_mgr.register_handler<CloseApplication>([this](const CloseApplication* event) {
        std::cout << "Closing" << std::endl;
        m_window.close();
      });

  m_editor_listener =
      m_context.event_mgr.register_handler<EnterEditorEvent>([this](const EnterEditorEvent* event) {
        if (dynamic_cast<const UnboundEditorState*>(m_game.peek_below(this))) {
          m_game.pop_state();
        } else {
          m_game.replace_state(
              std::make_unique<UnboundEditorState>(m_game, m_window, m_context.audio_mgr));
        }
      });

  m_start_listener =
      m_context.event_mgr.register_handler<EnterStartEvent>([this](const EnterStartEvent* event) {
        if (dynamic_cast<const UnboundEditorState*>(m_game.peek_below(this))) {
          m_game.pop_state();
        } else {
          m_game.replace_state(
              std::make_unique<UnboundStartState>(m_game, m_window, m_context.audio_mgr));
        }
      });

  m_toggle_camera_switch_listener =
      m_context.event_mgr.register_handler<ToggleDebugCameraMovementEvent>(
          [this](const ToggleDebugCameraMovementEvent* event) {
            ToggleSystem toggle_debug{m_context.system_id_proxy.get_name<DebugCameraSystem>()};
            ToggleSystem toggle_player{m_context.system_id_proxy.get_name<PlayerSystem>()};

            m_context.event_mgr.send_event(toggle_debug);
            m_context.event_mgr.send_event(toggle_player);
          });

  /* Handle player arrow hits */
  m_collision_listener =
      m_context.event_mgr.register_handler<CollisionEvent>([this](const CollisionEvent* event) {
        //
        cor::Entity arrow;
        cor::Entity other;
        // Make sure both entities are valid
        if (m_context.entity_mgr.is_valid(event->entity_1) &&
            m_context.entity_mgr.is_valid(event->entity_2)) {
          arrow = m_context.entity_mgr.get_entity(event->entity_1);
          // Check if entity_1 is an arrow
          if (arrow.has<NameComponent>() && arrow.get<NameComponent>().name == "player_arrow")
            other = m_context.entity_mgr.get_entity(event->entity_2);
          else {
            arrow = m_context.entity_mgr.get_entity(event->entity_2);
            // Check if entity_2 is an arrow, otherwise exit function
            if (arrow.has<NameComponent>() && arrow.get<NameComponent>().name == "player_arrow")
              other = m_context.entity_mgr.get_entity(event->entity_1);
            else
              return;
          }
        } else
          return;

        if (arrow.has<ParticlesComponent>()) {
          ParticlesComponent& pc = arrow;
          pc.min_start_size = 0.0f;
          pc.max_start_size = 0.0f;
        }

        if (other.has<PhysicsProjectileComponent>() || other.has<ExplodeComponent>() ||
            other.has<DynamicComponent>()) {
          destroy_arrow(arrow.get_handle(), m_context);
        }

        // Spawn particles at impact point based on surface material
        if (!arrow.has<ExplodeComponent>()) {
          //
          auto particle_holder_handle = m_context.entity_mgr.create_entity();
          auto particle_holder = m_context.entity_mgr.get_entity(particle_holder_handle);

          TransformComponent tc;
          tc.transform.set_position(event->hit_point, m_context);
          particle_holder = tc;

          LifetimeComponent ltc;
          ltc.time_left = 1.0f;
          particle_holder = ltc;

          ParticlesComponent pc;
          pc.spawn_per_second = 400;
          pc.min_life = 0.3f;
          pc.max_life = 0.35f;
          pc.min_start_size = 0.4f;
          pc.max_start_size = 0.6f;
          pc.size_mode = ParticlesComponent::Size::CONSTANT;
          pc.min_rotation_speed = -4.f;
          pc.max_rotation_speed = 4.f;
          const glm::vec3 vel = glm::vec3(40.f);
          pc.min_start_velocity = glm::vec3(-vel);
          pc.max_start_velocity = glm::vec3(vel);
          pc.velocity_mode = ParticlesComponent::Velocity::DECELERATE;
          pc.min_spawn_pos = glm::vec3(-0.0f);
          pc.max_spawn_pos = glm::vec3(0.0f);
          pc.spawn_duration_left = 0.05f;

          if (other.has<MaterialComponent>()) {
            MaterialComponent& mc = other;
            switch (mc.material) {
              case MaterialType::GRASS:
                pc.min_start_color = glm::vec4(.1, .2, 0, 1);
                pc.max_start_color = glm::vec4(.2, .3, 0, 1);
                pc.min_end_color = pc.min_start_color;
                pc.max_end_color = pc.max_start_color;
                pc.texture =
                    bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/explosion.png"_fp);
                break;
              case MaterialType::FLESH:
                pc.min_start_color = glm::vec4(0.2, 0, 0, 1);
                pc.max_start_color = glm::vec4(0.5, 0, 0, 1);
                pc.min_end_color = glm::vec4(0.2, 0, 0, 1);
                pc.max_end_color = glm::vec4(0.5, 0, 0, 1);
                pc.min_rotation_speed = 4.f;
                pc.max_rotation_speed = 7.f;
                pc.min_life = 0.1f;
                pc.min_start_size = 0.15f;
                pc.max_start_size = 0.3f;
                pc.spawn_per_second = 400;
                pc.texture =
                    bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/swirl_particle01.png"_fp);
                break;
              case MaterialType::STONE:
                pc.min_start_color = glm::vec4(1);
                pc.max_start_color = glm::vec4(1);
                pc.min_end_color = pc.min_start_color;
                pc.max_end_color = pc.max_start_color;
                pc.texture =
                    bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/rocks/rock_01.png"_fp);
                break;
              case MaterialType::MAGICAL:
                pc.min_start_color = glm::vec4(0.8, 0, 0.7, 1);
                pc.max_start_color = glm::vec4(0.9, 0, 0.9, 1);
                pc.min_end_color = glm::vec4(0.2, 0, 0.2, 1);
                pc.max_end_color = glm::vec4(0.3, 0, 0.3, 1);
                pc.texture =
                    bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/swirl_particle01.png"_fp);
              default:
                pc.min_start_color = glm::vec4(0.3, 0.3, 0.3, 1);
                pc.max_start_color = glm::vec4(0.3, 0.3, 0.3, 1);
                pc.min_end_color = pc.min_start_color;
                pc.max_end_color = pc.max_start_color;
                pc.texture =
                    bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/explosion.png"_fp);
                break;
            }
          }
          // No material attached
          else {
            pc.min_start_color = glm::vec4(0.3, 0.3, 0.3, 1);
            pc.max_start_color = glm::vec4(0.3, 0.3, 0.3, 1);
            pc.min_end_color = pc.min_start_color;
            pc.max_end_color = pc.max_start_color;
            pc.texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/explosion.png"_fp);
          }
          particle_holder = pc;
        }
      });

  m_pause_listener =
      m_context.event_mgr.register_handler<PauseGameEvent>([this](const PauseGameEvent* event) {
        m_paused = !m_paused;
        if (m_paused) {
          m_pause_fingerprint = m_system_mgr.get_active_fingerprint();
          m_system_mgr.disable_all_systems();
          m_input_mgr.unlock_mouse();
          m_system_mgr.enable_system(m_context.system_id_proxy.get_name<PauseMenuSystem>());
          m_system_mgr.enable_system(m_context.system_id_proxy.get_name<GraphicsSystem>());
        } else {
          m_input_mgr.lock_mouse();
          m_system_mgr.set_active_fingerprint(m_pause_fingerprint);
        }
      });

  m_quick_load_listener = m_context.event_mgr.register_handler<StartEventPlayback>(
      [this](const StartEventPlayback* event) {
        //
        m_event_recorder.play(m_context, m_system_mgr);
      });

  m_save_checkpoint_listener =
      m_context.event_mgr.register_handler<SaveCheckpoint>([this](const SaveCheckpoint* event) {
        //
        m_saved_state.reset();

        bse::serialize(m_context.entity_mgr, m_saved_state);
        bse::serialize(m_system_mgr, m_saved_state);
        bse::serialize(m_context.event_mgr, m_saved_state);
      });

  m_load_checkpoint_listener =
      m_context.event_mgr.register_handler<LoadCheckpoint>([this](const LoadCheckpoint* event) {
        //
        if (m_saved_state.bytes.size() > 0 && m_saved_state.head > 0) {
          int offset = 0;
          m_context.entity_mgr.clear();
          bse::deserialize(m_context.entity_mgr, m_saved_state, offset);
          bse::deserialize(m_system_mgr, m_saved_state, offset);
          bse::deserialize(m_context.event_mgr, m_saved_state, offset);

          m_context.event_mgr.send_event(CheckpointLoaded());

          m_context.is_replay = false;
          m_context.replay_time_left = 0.f;
          m_context.replay_time_factor = 1.f;
        }
      });

  // register systems
  m_system_mgr.add_system<PhysicsSystem>(m_context);
  m_system_mgr.add_system<GraphicsSystem>(m_context, &m_window);
  m_system_mgr.add_system<AnimationSystem>(m_context);
  m_system_mgr.add_system<FallDamageSystem>();
  m_system_mgr.add_system<HomingSystem>();
  m_system_mgr.add_system<QuestSystem>(m_context);
  m_system_mgr.add_system<TriggerSystem>(m_context);
  m_system_mgr.add_system<AISystem>(m_context);
  m_system_mgr.add_system<ParticleSystem>();
  m_system_mgr.add_system<AudioSystem>(m_context);
  m_system_mgr.add_system<CutsceneSystem>(m_context);
  m_system_mgr.add_system<RotationSystem>();
  m_system_mgr.add_system<AICombatSystem>(m_context);
  m_system_mgr.add_system<HealthSystem>(m_context);
  m_system_mgr.add_system<HudSystem>(m_context);
  m_system_mgr.add_system<CloudSystem>(m_context);
  m_system_mgr.add_system<SpecialEffectsSystem>(m_context);
  m_system_mgr.add_disabled_system<DebugCameraSystem>(m_context);
  m_system_mgr.add_system<PlayerSystem>(m_context);
  m_system_mgr.add_system<PickupSystem>(m_context);
  m_system_mgr.add_system<AttachUpdateSystem>();
  m_system_mgr.add_system<CrowdControlSystem>(m_context);
  m_system_mgr.add_system<SpawnSystem>(m_context);
  m_system_mgr.add_system<ReplaySystem>(m_context);
  m_system_mgr.add_system<PauseMenuSystem>(m_context, m_window);
  // This system HAS to be added last
  m_system_mgr.add_system<DestructionSystem>();

  m_input_mgr.lock_mouse();
  return;
}

bool UnboundGameState::update(float dt) {
  //
  if (!m_event_recorder.is_playing()) {
    m_input_mgr.poll_input();
  }
  dt = m_event_recorder.update(m_context, m_system_mgr, dt, m_context.replay_time_factor);
  State::update(dt);

  return false;
}

bool UnboundGameState::display() {
  //
  State::display();

  return false;
}

}  // namespace gmp
