#include <ai/navmesh.hpp>
#include <bse/asset.hpp>
#include <bse/file.hpp>
#include <bse/mesh_builder.hpp>
#include <bse/utility.hpp>
#include <cor/frame_context.hpp>
#include <gmp/editor/scene.hpp>
#include <scr/detail/utility.hpp>
#include <scr/object.hpp>
#include <scr/script.hpp>

template class std::unique_ptr<scr::Object>;

namespace gmp {

Scene::Scene(const std::string &path) {
  bse::Result<std::string> str = bse::load_file_str(path);
  m_root.reset(new sol::table(scr::detail::State::load_script(str.is_valid() ? *str : "", path)));
}

void Scene::save(const std::string &path) const {
  const std::string *out_path = (path == "") ? &m_path : &path;
  save(*m_root, *out_path);
}

std::unordered_map<std::string, ai::Navmesh> Scene::get_navmeshes() const {
  std::unordered_map<std::string, ai::Navmesh> navmeshes;

  sol::table scene = get_scene();
  if (!scene.valid()) {
    return navmeshes;
  }

  navmeshes = get_navmeshes(scene);

  return navmeshes;
}

std::unordered_map<std::string, ai::Navmesh> Scene::get_navmeshes(sol::table scene) {
  std::unordered_map<std::string, ai::Navmesh> navmeshes;

  bse::Asset<scr::Script> script("assets/script/editor/mesh.lua");
  using Neighbors = std::vector<uint32_t>;
  using Position = std::array<float, 3>;
  std::function<std::unordered_map<std::string, sol::object>(const sol::table &)> getter =
      script->return_value()["get_meshes"];

  std::unordered_map<std::string, sol::object> meshes = getter(scene);

  for (auto &[mesh_name, mesh] : meshes) {
    sol::table mesh_table = mesh.as<sol::table>();
    std::vector<Position> positions_vec =
        sol::object(mesh_table["positions"]).as<std::vector<Position>>();
    const glm::vec3 *positions = (const glm::vec3 *)positions_vec.data();
    std::vector<Neighbors> nodes = sol::object(mesh_table["nodes"]).as<std::vector<Neighbors>>();

    ai::Navmesh &navmesh = navmeshes[mesh_name];
    bse::MeshBuilder builder;
    for (size_t i = 0; i < positions_vec.size(); i++) {
      uint16_t builder_vertex = builder.create_vertex();
      ASSERT(builder_vertex == i);
    }

    std::vector<int> triangle_indices;
    for (size_t i = 0; i < nodes.size(); i++) {
      const Neighbors &neighbors = nodes[i];
      for (uint32_t neighbor : neighbors) {
        std::vector<bse::MeshBuilder::FaceId> new_face_ids =
            builder.connect_vertices(i, neighbor - 1);

        for (bse::MeshBuilder::FaceId id : new_face_ids) {
          ASSERT(id == triangle_indices.size());
          const bse::MeshBuilder::Face &face = builder.get_face(id);
          triangle_indices.push_back(
              navmesh.add_triangle(positions[face[0]], positions[face[1]], positions[face[2]]));
        }
      }
    }

    for (bse::MeshBuilder::FaceId id = 0; id < triangle_indices.size(); id++) {
      std::vector<bse::MeshBuilder::FaceId> neighbor_ids = builder.get_face_neighbors(id);
      for (bse::MeshBuilder::FaceId neighbor_id : neighbor_ids) {
        navmesh.add_neighbour(triangle_indices[id], triangle_indices[neighbor_id]);
      }
    }
  }

  return navmeshes;
}

void to_string(const sol::object &object, std::stringstream &stream, std::string &indentation,
               bool do_sort) {
  if (!object.valid()) {
    stream << "nil";
  } else if (object.is<glm::mat4>()) {
    glm::mat4 matrix = object.as<glm::mat4>();
    stream << "{";
    for (int x = 0; x < 4; x++) {
      for (int y = 0; y < 4; y++) {
        stream << matrix[y][x];
        if (x == 3 && y == 3) {
          stream << " ";
        } else {
          stream << ", ";
        }
      }
    }
    stream << "}";
  } else if (object.is<sol::table>()) {
    sol::table table = object.as<sol::table>();
    if (table[sol::metatable_key].valid()) {
      stream << sol::object(object.as<sol::table>()[sol::metatable_key]["__name"]).as<std::string>()
             << "_userdata";
    } else {
      std::vector<std::pair<int, sol::object>> index_children;
      std::vector<std::pair<std::string, sol::object>> key_children;
      scr::detail::for_each(
          object.as<sol::table>(),
          [&index_children, &key_children](const sol::object &key, const sol::object &value) {
            if (value.is<sol::table>()) {
              sol::table table = value.as<sol::table>();
              if (table[sol::metatable_key].valid()) {
                if (table.is<glm::mat4>()) {
                  // glm::mat4 matrix = table.get<glm::mat4>();
                  // stream << "{" << matrix[0] << "}"
                } else {
                  return;
                }
              }
            }
            if (key.get_type() == sol::type::number) {
              index_children.emplace_back(key.as<int>(), value);
            } else {
              if (key.as<std::string>().find('/') != std::string::npos) {
                key_children.emplace_back("[\"" + key.as<std::string>() + "\"] = ", value);
              } else {
                key_children.emplace_back(key.as<std::string>() + " = ", value);
              }
            }
          });

      if (do_sort) {
        std::sort(index_children.begin(), index_children.end(),
                  [](const auto &lhs, const auto &rhs) { return lhs.first < rhs.first; });
        std::sort(key_children.begin(), key_children.end(),
                  [](const auto &lhs, const auto &rhs) { return lhs.first < rhs.first; });
      }

      if (do_sort && key_children.empty()) {
        bool is_contiguous = true;
        int previous = 0;
        for (const auto &[i, value] : index_children) {
          if (i != previous + 1) {
            is_contiguous = false;
            break;
          }
          previous++;
        }

        if (is_contiguous) {
          for (const auto &[i, value] : index_children) {
            key_children.emplace_back("", value);
          }
        } else {
          for (const auto &[i, value] : index_children) {
            key_children.emplace_back("[" + std::to_string(i) + "] = ", value);
          }
        }

      } else {
        for (const auto &[i, value] : index_children) {
          key_children.emplace_back("[" + std::to_string(i) + "] = ", value);
        }
      }

      if (key_children.empty()) {
        stream << "{}";
      } else {
        stream << "{" << std::endl;
        indentation += "  ";
        for (size_t i = 0; i < key_children.size(); i++) {
          const auto &[key, value] = key_children[i];

          stream << indentation << key;
          to_string(value, stream, indentation, do_sort);
          if (i < key_children.size() - 1) {
            stream << ",";
          }
          stream << std::endl;
        }
        indentation.pop_back();
        indentation.pop_back();
        stream << indentation << "}";
      }
      index_children.clear();
      key_children.clear();
    }

  } else if (object.is<bool>()) {
    if (object.as<bool>()) {
      stream << "true";
    } else {
      stream << "false";
    }
  } /*else if (object.is<sol::function>()) {
    stream << "func_" << object.pointer() << std::endl;

  }*/

  else {
    if (object.get_type() == sol::type::string) {
      stream << "\"" << object.as<std::string>() << "\"";
    } else {
      stream << object.as<std::string>();
    }
  }
}

std::string to_string(const sol::object &object, bool do_sort) {
  std::string string;

  std::stringstream stream;
  std::string indentation;
  to_string(object, stream, indentation, do_sort);
  string = stream.str();
  bse::remove(string, '\r');
  while (!string.empty() && (string.back() == '\r' || string.back() == '\n')) {
    string.pop_back();
  }
  string += '\n';
  return string;
}
void Scene::save(const sol::table &scene, const std::string &path) {
  // std::cout << to_string(scene, true) << std::endl;
  bse::write_file(path, "scene = " + to_string(scene, true));
}

sol::table Scene::load(const std::string &path) {
  Scene scene(path);
  return scene.get_scene();
}

void Scene::upload(const sol::table &scene, cor::FrameContext &context) {
  bse::Asset<scr::Script> script("assets/script/editor/scene.lua");
  std::function<void(const sol::table &, cor::FrameContext &, cor::EntityManager &)> uploader =
      script->return_value()["upload"];

  context.navmeshes = get_navmeshes(scene);

  uploader(scene, context, context.entity_mgr);
}

/*Scene::Scene(const scr::Object& root) : m_root(new sol::table(root.get<sol::table>())) {}

bse::Result<Scene> Scene::load(bse::FilePath path) {
  bse::Asset<scr::Script> script(path);
  if (!script) {
    return bse::Error() << "Failed to load scene script.";
  }

  scr::Object root = script->return_value();
  return std::move(Scene(root));
}*/

sol::table Scene::get_scene() const {
  if (!m_root->valid()) {
    return sol::nil;
  }

  sol::object scene = (*m_root)["scene"];
  if (!scene.valid() || !scene.is<sol::table>()) {
    return sol::nil;
  }
  return scene;
}

}  // namespace gmp