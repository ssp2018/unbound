#include "gmp/animation/animation_node.hpp"

namespace gmp {
AnimationNode::AnimationNode() : m_animation(nullptr), m_skeleton(nullptr) {}
AnimationNode::~AnimationNode() {}

void AnimationNode::set_animation(const bse::AnimationClip* animation,
                                  const bse::Skeleton* skeleton, std::string state_name) {
  m_animation = animation;
  m_skeleton = skeleton;
  m_state_name = state_name;

  ASSERT(skeleton) << "Skeleton was nullptr";

  if (m_animation) {
    // Make space for quaternions
    ASSERT(m_animation->get_keyframes().size() > 0) << "Animation has no keyframes\n";

    m_result_SQT.resize(m_skeleton->num_joints());
  }
}

const bse::AnimationClip* AnimationNode::get_animation() const {
  // ASSERT(m_animation != nullptr) << "Animation was null!";
  return m_animation;
}

float AnimationNode::get_time() const { return m_time; }

std::optional<std::unique_ptr<AnimationBaseTreeNode>> AnimationNode::update(
    float dt, std::vector<AnimationNotifyEvent>& animation_notify_events) {
  if (!(m_skeleton && m_animation)) {
    return std::nullopt;
  }

  // Find the current keyframe
  auto& kfs = m_animation->get_keyframes();

  ASSERT(m_key_frame_index >= 0) << "Keyframe index out of range\n";

  if (kfs.size()) {
    ASSERT(m_key_frame_index < kfs.size()) << "Keyframe index out of range\n";
  }

  // Only animate if there are two or more keyframes
  if (kfs.size() >= 2) {
    m_time += dt;

    while (kfs[m_key_frame_index + 1].m_time <= m_time) {
      if (m_loop || m_key_frame_index < kfs.size() - 2) {
        m_key_frame_index++;
      }

      const std::vector<int>& anim_notifies = m_animation->get_notifies();
      // if (anim_notifies.size() > 0) {
      //  if (m_animation->m_name == "boss_anm_groundslam") {
      //    std::cout << m_animation->m_name << " notifies found" << std::endl;
      //  }
      //}

      if (anim_notifies.size() > 0) {
        // if (anim_notifies[m_notify_data.next_notify] - m_key_frame_index <= 0 &&
        //    m_notify_data.previous_frame < anim_notifies[m_notify_data.next_notify]) {
        if (anim_notifies[m_notify_data.next_notify] == m_key_frame_index) {
          AnimationNotifyEvent anim_notify;
          anim_notify.animation_name = m_animation->m_name;
          // anim_notify.handle = handle;
          animation_notify_events.push_back(anim_notify);
          if (m_notify_data.next_notify + 1 >= anim_notifies.size()) {
            m_notify_data.next_notify = 0;
          } else {
            m_notify_data.next_notify++;
          }
        } else {
          // break;
        }
      }

      // If on the last keyframe, loop animation
      if (m_key_frame_index == kfs.size() - 1) {
        float duration = kfs[kfs.size() - 1].m_time;

        m_key_frame_index = 0;
        m_loop_count++;

        // Loop animation
        while (m_time >= duration) {
          m_time -= duration;
        }
      } else if (!m_loop && m_key_frame_index == kfs.size() - 2) {
        m_time = kfs[kfs.size() - 1].m_time;
        break;
      }
    }
  }

  float interpolation_ratio = calculate_interpolation_ratio(m_time, kfs[m_key_frame_index].m_time,
                                                            kfs[m_key_frame_index + 1].m_time);

  // Interpolate between keyframes
  for (int i = 0; i < m_skeleton->num_joints(); i++) {
    // Only update if joint with id 'i' is found in joint poses map
    if (kfs[m_key_frame_index].m_joint_poses.find(i) !=
        kfs[m_key_frame_index].m_joint_poses.end()) {
      m_result_SQT[i].m_rotation = glm::slerp(
          kfs[m_key_frame_index].m_joint_poses.at(i).m_rotation,
          kfs[m_key_frame_index + 1].m_joint_poses.at(i).m_rotation, interpolation_ratio);

      const glm::vec3& start = kfs[m_key_frame_index].m_joint_poses.at(i).m_translation;
      const glm::vec3& end = kfs[m_key_frame_index + 1].m_joint_poses.at(i).m_translation;

      m_result_SQT[i].m_translation = start + (end - start) * interpolation_ratio;
    } else {
      // m_skeleton->get_joint_map().at(m_skeleton
      //    ->get_joint_name(i))->

      m_result_SQT[i].m_rotation = {0, 0, 0, 0};
      m_result_SQT[i].m_translation = {0, 0, 0};
    }
  }

  return std::nullopt;
}

std::vector<bse::SQT>& AnimationNode::get_sqt_palette() const { return m_result_SQT; }

void AnimationNode::set_looping(bool loop) { m_loop = loop; }

float AnimationNode::get_loop_count() const { return m_loop_count; }

float AnimationNode::get_machine_weight() const { return m_weight; }

void AnimationNode::set_machine_weight(float weight) { m_weight = weight; }

float AnimationNode::calculate_interpolation_ratio(float current_time, float previous_time,
                                                   float next_time) {
  return (current_time - previous_time) / (next_time - previous_time);
}

void AnimationNode::debug_print(int level) {
  for (int i = 0; i < level; i++) {
    std::cout << "  ";
  }

  std::cout << m_animation->m_name << " : " << m_time << "s\n";
}

std::unique_ptr<AnimationBaseTreeNode> AnimationNode::copy() const {
  AnimationNode* anim = new AnimationNode();
  anim->m_animation = m_animation;
  anim->m_skeleton = m_skeleton;
  anim->m_time = m_time;
  anim->m_weight = m_weight;
  anim->m_key_frame_index = m_key_frame_index;
  anim->m_result_SQT = m_result_SQT;
  anim->m_loop = m_loop;
  anim->m_loop_count = m_loop_count;
  anim->m_state_name = m_state_name;
  anim->m_notify_data = m_notify_data;

  std::unique_ptr<AnimationBaseTreeNode> unp;
  unp.reset(anim);
  return unp;
}

void AnimationNode::restore_state(const AnimationStateMachineDescription* desc) {
  m_skeleton = desc->skeleton;

  for (auto& state : desc->states) {
    if (state.m_state_name == m_state_name) {
      set_animation(state.m_animation, desc->skeleton, state.m_state_name);
      return;
    }
  }

  ASSERT(false) << "Could not find correct animation '" << m_state_name
                << "' when restoring AnimationNode state\n";
}

}  // namespace gmp