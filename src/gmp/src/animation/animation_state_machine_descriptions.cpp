#include <bse/asset.hpp>
#include <bse/mesh/skeleton.hpp>
#include <cor/entity.hpp>
#include <cor/entity.tcc>
#include <cor/entity_manager.tcc>
#include <gfx/model.hpp>
#include <gmp/animation/animation_state_machine_descriptions.hpp>
#include <gmp/components/ai_component.hpp>
#include <gmp/components/character_controller_component.hpp>
#include <gmp/components/player_component.hpp>

namespace gmp {

struct AnimationStateMachineDescriptionCollection {
  std::vector<AnimationStateMachineDescription> state_machine_descriptions;
  AnimationStateMachineDescriptionCollection() {
    {
      const auto player_skel =
          &bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/player_mesh.fbx"_fp)
               ->get_mesh()
               ->get_skeleton();
      const auto& player_anim_map = player_skel->get_animations_map();

      state_machine_descriptions.push_back(AnimationStateMachineDescription(
          "player", player_skel,
          {AnimationState("idle", &player_anim_map.at("player_mesh_anm_idle"),
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity player = entity_manager.get_entity(handle);

                            if (player.has<CharacterControllerComponent, PlayerComponent>()) {
                              const CharacterControllerComponent& ccc = player;
                              const PlayerComponent& pc = player;

                              if (pc.is_dead) {
                                return AnimationTransition{"death", 0.3f};
                              }
                              if (glm::length(ccc.contents.get_velocity()) > 0.5f &&
                                  ccc.contents.on_ground() && !pc.is_swinging) {
                                return AnimationTransition{"run", 0.3f};
                              }
                              if (!ccc.contents.on_ground() && !pc.is_swinging) {
                                return AnimationTransition{"jump", 0.3f};
                              }
                              if (pc.is_swinging) {
                                return AnimationTransition{"swing", 0.3f};
                              }
                              if (pc.is_dashing && ccc.contents.on_ground()) {
                                return AnimationTransition{"dash", 0.1f};
                              }
                            }
                            return std::nullopt;
                          }),
           AnimationState("run", &player_anim_map.at("player_mesh_anm_run"),
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity player = entity_manager.get_entity(handle);

                            if (player.has<CharacterControllerComponent, PlayerComponent>()) {
                              const CharacterControllerComponent& ccc = player;
                              const PlayerComponent& pc = player;

                              if (pc.is_dead) {
                                return AnimationTransition{"death", 0.3f};
                              }
                              if (glm::length(ccc.contents.get_velocity()) < 0.5f &&
                                  ccc.contents.on_ground() && !pc.is_swinging) {
                                return AnimationTransition{"idle", 0.3f};
                              }
                              if (!ccc.contents.on_ground() && !pc.is_swinging) {
                                return AnimationTransition{"jump", 0.3f};
                              }
                              if (pc.is_swinging) {
                                return AnimationTransition{"swing", 0.3f};
                              }
                              if (pc.is_dashing && ccc.contents.on_ground()) {
                                return AnimationTransition{"dash", 0.1f};
                              }
                            }
                            return std::nullopt;
                          }),
           AnimationState("jump", &player_anim_map.at("player_mesh_anm_jump"),
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity player = entity_manager.get_entity(handle);

                            if (player.has<CharacterControllerComponent, PlayerComponent>()) {
                              const CharacterControllerComponent& ccc = player;
                              const PlayerComponent& pc = player;

                              if (pc.is_dead) {
                                return AnimationTransition{"death", 0.3f};
                              }
                              if (pc.is_dashing) {
                                return AnimationTransition{"dash", 0.1f};
                              }
                              if (glm::length(ccc.contents.get_velocity()) < 0.5f &&
                                  ccc.contents.on_ground() && !pc.is_swinging) {
                                return AnimationTransition{"idle", 0.3f};
                              }
                              if (glm::length(ccc.contents.get_velocity()) >= 0.5f &&
                                  ccc.contents.on_ground() && !pc.is_swinging) {
                                return AnimationTransition{"run", 0.3f};
                              }
                              if (!ccc.contents.on_ground() && pc.is_swinging) {
                                return AnimationTransition{"swing", 0.3f};
                              }
                            }
                            return std::nullopt;
                          }),
           AnimationState(
               "swing", &player_anim_map.at("player_mesh_anm_swing"),
               [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                  float time_in_current_state) -> std::optional<AnimationTransition> {
                 cor::ConstEntity player = entity_manager.get_entity(handle);

                 if (player.has<CharacterControllerComponent, PlayerComponent>()) {
                   const CharacterControllerComponent& ccc = player;
                   const PlayerComponent& pc = player;

                   if (pc.is_dead) {
                     return AnimationTransition{"death", 0.3f};
                   }
                   if (!ccc.contents.on_ground() && !pc.is_swinging) {
                     return AnimationTransition{"jump", 0.3f};
                   }
                   if (!pc.is_swinging && glm::length(ccc.contents.get_velocity()) < 0.5f) {
                     return AnimationTransition{"idle", 0.3f};
                   }
                   if (!pc.is_swinging && glm::length(ccc.contents.get_velocity()) >= 0.5f) {
                     return AnimationTransition{"run", 0.3f};
                   }
                 }
                 return std::nullopt;
               }),
           AnimationState("dash", &player_anim_map.at("player_mesh_anm_dash"),
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity player = entity_manager.get_entity(handle);

                            if (player.has<CharacterControllerComponent, PlayerComponent>()) {
                              const CharacterControllerComponent& ccc = player;
                              const PlayerComponent& pc = player;

                              if (pc.is_dead) {
                                return AnimationTransition{"death", 0.3f};
                              }
                              if (ccc.contents.on_ground() && !pc.is_dashing &&
                                  glm::length(ccc.contents.get_velocity()) < 0.5f) {
                                return AnimationTransition{"idle", 0.3f};
                              }
                              if (ccc.contents.on_ground() && !pc.is_dashing &&
                                  glm::length(ccc.contents.get_velocity()) >= 0.5f) {
                                return AnimationTransition{"run", 0.3f};
                              }
                              if (!ccc.contents.on_ground() && !pc.is_dashing) {
                                return AnimationTransition{"jump", 0.3f};
                              }
                            }
                            return std::nullopt;
                          }),
           AnimationState("death", &player_anim_map.at("player_mesh_anm_death"), 1.0f, false,
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity player = entity_manager.get_entity(handle);
                            if (player.has<CharacterControllerComponent, PlayerComponent>()) {
                              const PlayerComponent& pc = player;

                              if (!pc.is_dead) {
                                return AnimationTransition{"idle", 0.0f};
                              }
                            }
                            return std::nullopt;
                          })}));

      // Attack state machine
      state_machine_descriptions.push_back(AnimationStateMachineDescription(
          "player_attack", player_skel,
          {AnimationState("draw", &player_anim_map.at("player_mesh_anm_bowdraw"),
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity player = entity_manager.get_entity(handle);

                            if (player.has<CharacterControllerComponent, PlayerComponent>()) {
                              const CharacterControllerComponent& ccc = player;
                              const PlayerComponent& pc = player;

                              if (pc.is_dead) {
                                return AnimationTransition{"none", 0.1f};
                              }
                              return AnimationTransition{"hold", 0.1f};
                            }
                            return std::nullopt;
                          }),
           AnimationState("hold", &player_anim_map.at("player_mesh_anm_bowdrawloop"),
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity player = entity_manager.get_entity(handle);

                            if (player.has<CharacterControllerComponent, PlayerComponent>()) {
                              const CharacterControllerComponent& ccc = player;
                              const PlayerComponent& pc = player;

                              if (pc.is_dead) {
                                return AnimationTransition{"none", 0.1f};
                              }
                              if (!pc.is_attacking) {
                                return AnimationTransition{"release", 0.1f};
                              }
                            }
                            return std::nullopt;
                          }),
           AnimationState("release", &player_anim_map.at("player_mesh_anm_bowshoot"),
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity player = entity_manager.get_entity(handle);

                            if (player.has<CharacterControllerComponent, PlayerComponent>() &&
                                time_in_current_state > 0.333f) {
                              return AnimationTransition{"return", 0.0f};
                            }
                            return std::nullopt;
                          }),
           AnimationState("return", &player_anim_map.at("player_mesh_anm_bowreturn"),
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity player = entity_manager.get_entity(handle);

                            if (player.has<CharacterControllerComponent, PlayerComponent>() &&
                                time_in_current_state > 0.333f) {
                              return AnimationTransition{"none", 0.1f};
                            }
                            return std::nullopt;
                          }),
           AnimationState("none", &player_anim_map.at("player_mesh_anm_bowdrawloop"), 0.0f, true,
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity player = entity_manager.get_entity(handle);

                            if (player.has<CharacterControllerComponent, PlayerComponent>()) {
                              const CharacterControllerComponent& ccc = player;
                              const PlayerComponent& pc = player;

                              if (pc.is_attacking && !pc.is_dead) {
                                return AnimationTransition{"draw", 0.1f};
                              }
                            }
                            return std::nullopt;
                          })}));
    }
    {
      const auto boss_skel = &bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/boss.fbx"_fp)
                                  ->get_mesh()
                                  ->get_skeleton();
      const auto& boss_anim_map = boss_skel->get_animations_map();
      state_machine_descriptions.push_back(AnimationStateMachineDescription(
          "golem", boss_skel,
          {AnimationState("idle", &boss_anim_map.at("boss_anm_idle"),
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity boss = entity_manager.get_entity(handle);

                            if (boss.has<AIComponent>()) {
                              const AIComponent& aic = boss;

                              if (aic.state_name == "battle_stance") {
                                return AnimationTransition{"battle_stance", 0.3f};
                              } else if (boss.has<CharacterControllerComponent>()) {
                                auto& ccc = boss.get<CharacterControllerComponent>();
                                float speed = glm::length(ccc.contents.get_velocity());
                                if (speed > 0.5f) {
                                  return AnimationTransition{"idle_walk", 0.3f};
                                }
                              }
                            } else {
                              return AnimationTransition{"dead", 0.3f};
                            }
                            return std::nullopt;
                          }),
           AnimationState("idle_walk", &boss_anim_map.at("boss_anm_walk"),
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity boss = entity_manager.get_entity(handle);

                            if (boss.has<AIComponent>()) {
                              const AIComponent& aic = boss;

                              if (aic.state_name == "battle_stance") {
                                return AnimationTransition{"battle_stance", 0.3f};
                              } else if (boss.has<CharacterControllerComponent>()) {
                                auto& ccc = boss.get<CharacterControllerComponent>();
                                float speed = glm::length(ccc.contents.get_velocity());
                                if (speed < 0.5f) {
                                  return AnimationTransition{"idle", 0.3f};
                                }
                              } else {
                                return AnimationTransition{"idle", 0.3f};
                              }
                            } else {
                              return AnimationTransition{"dead", 0.3f};
                            }
                            return std::nullopt;
                          }),
           AnimationState("battle_stance", &boss_anim_map.at("boss_anm_walk"),
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity boss = entity_manager.get_entity(handle);

                            if (boss.has<AIComponent>()) {
                              const AIComponent& aic = boss;

                              if (aic.state_name != "battle_stance") {
                                return AnimationTransition{"idle", 0.3f};
                              }
                            } else {
                              return AnimationTransition{"dead", 0.3f};
                            }
                            return std::nullopt;
                          }),
           AnimationState("dead", &boss_anim_map.at("boss_anm_death"), 1.0f, false,
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            // there is no return from death >:|
                            cor::ConstEntity boss = entity_manager.get_entity(handle);

                            if (boss.has<AIComponent>()) {
                              // std::cout << "Boss returned from the dead!" << std::endl;
                              return AnimationTransition{"idle", 0.0f};
                            }

                            return std::nullopt;
                          })}));
    }

    {
      const auto grunt_skel = &bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/grunt.fbx"_fp)
                                   ->get_mesh()
                                   ->get_skeleton();
      const auto& grunt_anim_map = grunt_skel->get_animations_map();
      state_machine_descriptions.push_back(AnimationStateMachineDescription(
          "grunt", grunt_skel,
          {AnimationState("idle", &grunt_anim_map.at("grunt_anm_idle"),
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity grunt = entity_manager.get_entity(handle);

                            if (!grunt.has<AIComponent>()) {
                              return AnimationTransition{"dead", 0.3f};
                            } else {
                              const AIComponent& aic = grunt;

                              if (aic.state_name == "melee_attack" ||
                                  aic.state_name == "shield_sword_poke") {
                                return AnimationTransition{"attack", 0.3f};
                              }

                              if (grunt.has<CharacterControllerComponent>()) {
                                const CharacterControllerComponent& ccc = grunt;

                                if (ccc.contents.get_speed() > 0.5f) {
                                  return AnimationTransition{"run", 0.3f};
                                }
                              }
                            }

                            return std::nullopt;
                          }),
           AnimationState("run", &grunt_anim_map.at("grunt_anm_run"),
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity grunt = entity_manager.get_entity(handle);

                            if (!grunt.has<AIComponent>()) {
                              return AnimationTransition{"dead", 0.3f};
                            } else {
                              const AIComponent& aic = grunt;

                              if (aic.state_name == "melee_attack" ||
                                  aic.state_name == "shield_sword_poke") {
                                return AnimationTransition{"attack", 0.3f};
                              }

                              if (grunt.has<CharacterControllerComponent>()) {
                                const CharacterControllerComponent& ccc = grunt;

                                if (ccc.contents.get_speed() < 0.5f) {
                                  return AnimationTransition{"idle", 0.3f};
                                }
                              }
                            }

                            return std::nullopt;
                          }),
           AnimationState("attack", &grunt_anim_map.at("grunt_anm_attack"),
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity grunt = entity_manager.get_entity(handle);

                            if (!grunt.has<AIComponent>()) {
                              return AnimationTransition{"dead", 0.3f};
                            } else {
                              const AIComponent& aic = grunt;

                              if (grunt.has<CharacterControllerComponent>()) {
                                const CharacterControllerComponent& ccc = grunt;

                                if (!(aic.state_name == "melee_attack" ||
                                      aic.state_name == "shield_sword_poke")) {
                                  if (ccc.contents.get_speed() > 0.5f) {
                                    return AnimationTransition{"run", 0.3f};
                                  } else {
                                    return AnimationTransition{"idle", 0.3f};
                                  }
                                }
                              }
                            }

                            return std::nullopt;
                          }),
           AnimationState("dead", &grunt_anim_map.at("grunt_anm_death"), 1.0f, false,
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity grunt = entity_manager.get_entity(handle);

                            if (grunt.has<AIComponent>()) {
                              return AnimationTransition{"idle", 0.0f};
                            }

                            return std::nullopt;
                          })}));
    }

    {
      const auto skull_skel = &bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/skull.fbx"_fp)
                                   ->get_mesh()
                                   ->get_skeleton();
      const auto& skull_anim_map = skull_skel->get_animations_map();
      state_machine_descriptions.push_back(AnimationStateMachineDescription(
          "skull", skull_skel,
          {AnimationState("idle", &skull_anim_map.at("skull_anm_idle"),
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity skull = entity_manager.get_entity(handle);

                            if (skull.has<AIComponent>()) {
                              const AIComponent& aic = skull;
                              if (aic.state_name == "pursue") {
                                return AnimationTransition{"aggro", 0.3f};
                              }
                            }

                            return std::nullopt;
                          }),
           AnimationState("aggro", &skull_anim_map.at("skull_anm_aggro"),
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity skull = entity_manager.get_entity(handle);

                            if (skull.has<AIComponent>()) {
                              const AIComponent& aic = skull;
                              if (aic.state_name == "return_home") {
                                return AnimationTransition{"idle", 0.3f};
                              }
                            }

                            return std::nullopt;
                          })}));
    }

    {
      const auto cole_skel = &bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/cole.fbx"_fp)
                                  ->get_mesh()
                                  ->get_skeleton();
      const auto& cole_anim_map = cole_skel->get_animations_map();

      state_machine_descriptions.push_back(AnimationStateMachineDescription(
          "COLE", cole_skel,
          {AnimationState("idle", &cole_anim_map.at("cole_anm_idle"),
                          [](cor::EntityHandle handle, const cor::EntityManager& entity_manager,
                             float time_in_current_state) -> std::optional<AnimationTransition> {
                            cor::ConstEntity player = entity_manager.get_entity(handle);

                            return std::nullopt;
                          })}));
    }
  }
};

static const AnimationStateMachineDescriptionCollection& get_collection() {
  static AnimationStateMachineDescriptionCollection collection;
  return collection;
}

// Finds and returns an animation state machine description by name, or nullptr if not found
const AnimationStateMachineDescription* get_animation_state_machine_description(
    const std::string& description_name) {
  for (int i = 0; i < get_collection().state_machine_descriptions.size(); ++i) {
    if (description_name == get_collection().state_machine_descriptions[i].description_name)
      return &get_collection().state_machine_descriptions[i];
  }

  return nullptr;
}

}  // namespace gmp