#include "gmp/animation/blend_node.hpp"

#include "bse/assert.hpp"

namespace gmp {
BlendNode::BlendNode() {}
BlendNode::~BlendNode() {}

void BlendNode::set_children(std::unique_ptr<AnimationBaseTreeNode> &first_animation,
                             std::unique_ptr<AnimationBaseTreeNode> &second_animation) {
  m_children.first = std::move(first_animation);
  m_children.second = std::move(second_animation);
}

void BlendNode::set_blend_time(float blend_time) { m_blend_time = blend_time; }

const std::pair<AnimationBaseTreeNode *, AnimationBaseTreeNode *> BlendNode::get_children() {
  std::pair<AnimationBaseTreeNode *, AnimationBaseTreeNode *> nodes;
  nodes.first = m_children.first.get();
  nodes.second = m_children.second.get();
  return nodes;
}

std::optional<std::unique_ptr<AnimationBaseTreeNode>> BlendNode::update(
    float dt, std::vector<AnimationNotifyEvent> &animation_notify_events) {
  // Update children. If they need to be replaced, replace them with the returned value
  auto opt_replacement = m_children.first->update(dt, animation_notify_events);
  if (opt_replacement.has_value()) {
    m_children.first = std::move(*opt_replacement);
  }

  opt_replacement = m_children.second->update(dt, animation_notify_events);
  if (opt_replacement.has_value()) {
    m_children.second = std::move(*opt_replacement);
  }
  m_weight += dt / m_blend_time;

  blend_animations();

  // If weight is 1.0 then that means that the second animation has taken over and thus the blend
  // node is obsolete. Remove first animation and blend node and return the second animation node.
  if (m_weight >= 1.0f) {
    return std::move(m_children.second);
  } else {
    return std::nullopt;
  }
}

std::vector<bse::SQT> &BlendNode::get_sqt_palette() const {
  // Result of blend stored in second animation
  return m_children.second->get_sqt_palette();
}

float BlendNode::get_machine_weight() const {
  float w1 = m_children.first->get_machine_weight();
  float w2 = m_children.second->get_machine_weight();

  return w1 * (1.0f - m_weight) + w2 * m_weight;
}

std::vector<bse::SQT> &BlendNode::blend_animations() {
  std::vector<bse::SQT> &first_animation = m_children.first.get()->get_sqt_palette();
  std::vector<bse::SQT> &second_animation = m_children.second.get()->get_sqt_palette();
  ASSERT(first_animation.size() == second_animation.size())
      << "Animation blend node error: The two animations differ in number of joints!\n";

  for (int i = 0; i < second_animation.size(); i++) {
    second_animation[i].m_rotation =
        glm::slerp(first_animation[i].m_rotation, second_animation[i].m_rotation, m_weight);

    second_animation[i].m_translation =
        first_animation[i].m_translation +
        (second_animation[i].m_translation - first_animation[i].m_translation) * m_weight;
  }

  return second_animation;
}

void BlendNode::debug_print(int level) {
  for (int i = 0; i < level; i++) {
    std::cout << "  ";
  }

  std::cout << "Blending over " << m_blend_time << "s : " << m_weight << "\n";

  m_children.first->debug_print(level + 1);
  m_children.second->debug_print(level + 1);
}

std::unique_ptr<AnimationBaseTreeNode> BlendNode::copy() const {
  std::unique_ptr<AnimationBaseTreeNode> child1 = m_children.first->copy();
  std::unique_ptr<AnimationBaseTreeNode> child2 = m_children.second->copy();

  BlendNode *blend = new BlendNode();
  blend->m_blend_time = m_blend_time;
  blend->m_weight = m_weight;
  blend->set_children(child1, child2);

  std::unique_ptr<AnimationBaseTreeNode> unp;
  unp.reset(blend);
  return unp;
}

void BlendNode::restore_state(const AnimationStateMachineDescription *desc) {
  m_children.first->restore_state(desc);
  m_children.second->restore_state(desc);
}

}  // namespace gmp