#include "gmp/animation/animation_event_player.hpp"

#include "bse/mesh/skeleton.hpp"
#include "cor/entity_handle.hpp"
#include "gmp/animation/animation_node.hpp"

namespace gmp {
AnimationEventPlayer::AnimationEventPlayer() {}
AnimationEventPlayer::AnimationEventPlayer(const AnimationEventPlayer& other) {
  if (other.m_animation_node) {
    AnimationBaseTreeNode* base_node = other.m_animation_node->copy().release();
    AnimationNode* new_animation_node = dynamic_cast<AnimationNode*>(base_node);
    ASSERT(new_animation_node);
    m_animation_node.reset(new_animation_node);
  }

  m_key_frame_index = other.m_key_frame_index;
  m_blend_time = other.m_blend_time;
}
AnimationEventPlayer& AnimationEventPlayer::operator=(const AnimationEventPlayer& other) {
  if (other.m_animation_node) {
    AnimationBaseTreeNode* base_node = other.m_animation_node->copy().release();
    AnimationNode* new_animation_node = dynamic_cast<AnimationNode*>(base_node);
    ASSERT(new_animation_node);
    m_animation_node.reset(new_animation_node);
  }

  m_key_frame_index = other.m_key_frame_index;
  m_blend_time = other.m_blend_time;

  return *this;
}
AnimationEventPlayer::~AnimationEventPlayer() {}

float AnimationEventPlayer::get_current_blend_factor() const {
  if (!(m_animation_node && m_animation_node->get_animation())) {
    return 1.0f;
  }

  float time = m_animation_node->get_time();

  if (time < m_blend_time) {
    return time / m_blend_time;
  } else if (time > m_animation_node->get_animation()->get_duration() - m_blend_time) {
    return (m_animation_node->get_animation()->get_duration() - time) / m_blend_time;
  } else {
    return 1.0f;
  }
}

std::optional<std::vector<bse::SQT>> AnimationEventPlayer::update(
    float dt, std::vector<AnimationNotifyEvent>& animation_notifies,
    cor::EntityHandle handle) const {
  std::vector<bse::SQT> result_quaternions;

  // Only play the animation once
  if (m_animation_node->get_loop_count() == 0) {
    m_animation_node->update(dt, animation_notifies);

    // Bit of a hack to attach entity handle to the notify events
    for (auto& anim_event : animation_notifies) {
      anim_event.handle = handle;
    }
    result_quaternions = m_animation_node->get_sqt_palette();
    return result_quaternions;
  }

  m_animation_node.reset();
  return std::nullopt;
}

void AnimationEventPlayer::play_animation(bse::Asset<gfx::Model> model,
                                          const std::string& animation_name,
                                          float blend_time) const {
  m_animation_node = std::make_unique<AnimationNode>();
  m_animation_node->set_animation(
      &model->get_mesh()->get_skeleton().get_animations_map().at(animation_name),
      &model->get_mesh()->get_skeleton(), animation_name);
  m_blend_time = blend_time;
  m_model = model;
  m_animation_name = animation_name;
}

const AnimationNode* AnimationEventPlayer::get_animation_node() const {
  return m_animation_node.get();
}

}  // namespace gmp
