#include <bse/assert.hpp>
#include <gmp/animation/animation_state_machine.hpp>
#include <gmp/animation/animation_state_machine_descriptions.hpp>
#include <gmp/animation/blend_node.hpp>

namespace gmp {

AnimationStateMachine::AnimationStateMachine() {
  m_machine_name = "";
  m_description = nullptr;
  m_tree_root.reset();
  m_time_in_current_state = 0.0f;
}

AnimationStateMachine::AnimationStateMachine(std::string machine_name, std::string start_state) {
  set_machine_and_state(machine_name, start_state);
  m_time_in_current_state = 0.0f;
}
AnimationStateMachine::~AnimationStateMachine() {}

AnimationStateMachine::AnimationStateMachine(AnimationStateMachine&& other) {
  move_from(std::move(other));
}

AnimationStateMachine& AnimationStateMachine::operator=(AnimationStateMachine&& other) {
  if (this != &other) {
    move_from(std::move(other));
  }

  return *this;
}

std::string AnimationStateMachine::get_machine() const { return m_machine_name; }

std::string AnimationStateMachine::get_state() const {
  if (m_description) {
    return m_description->states[m_current_state].m_state_name;
  } else {
    return "";
  }
}

std::vector<AnimationNotifyEvent> AnimationStateMachine::get_notify_events() {
  return m_animation_notify_events;
}

void AnimationStateMachine::update(cor::EntityHandle entity_handle,
                                   const cor::EntityManager& entity_manager, float dt) const {
  m_time_in_current_state += dt;

  if (m_tree_root && m_current_state >= 0) {
    // Execute the transition function
    auto t = m_description->states[m_current_state].m_animation_transition(
        entity_handle, entity_manager, m_time_in_current_state);
    // Run additional transition tests to avoid adding incorrect animations
    for (int i = 0; i < 4; i++) {
      int new_state = find_state(t->new_state_name);

      if (new_state >= 0 && new_state < m_description->states.size()) {
        auto additional_transition = m_description->states[new_state].m_animation_transition(
            entity_handle, entity_manager, m_time_in_current_state);

        // Replace transition
        if (additional_transition.has_value()) {
          t = additional_transition;
        } else {
          break;
        }
      } else {
        break;
      }
    }

    // If a transition was returned, add a new blend node at the top of the animation tree
    if (t.has_value()) {
      m_time_in_current_state = 0.0f;
      int i = find_state(t->new_state_name);

      if (i >= 0) {
        ASSERT(i < m_description->states.size()) << "Animation state index out of range\n";
        m_current_state = i;

        // Create animation node
        AnimationNode* anim = new AnimationNode();
        anim->set_animation(m_description->states[i].m_animation, m_description->skeleton,
                            m_description->states[i].m_state_name);
        anim->set_machine_weight(m_description->states[i].m_machine_weight);
        anim->set_looping(m_description->states[i].m_loop);

        std::unique_ptr<AnimationBaseTreeNode> new_anim_node;
        new_anim_node.reset(anim);

        // Create blend node blending between previous root and new animation
        BlendNode* blend = new BlendNode();
        blend->set_children(m_tree_root, new_anim_node);
        blend->set_blend_time(t->blend_time);

        std::unique_ptr<AnimationBaseTreeNode> new_blend_node;
        new_blend_node.reset(blend);

        // Set new blend node to root node
        m_tree_root = std::move(new_blend_node);
      } else {
        LOG(WARNING) << "Attempted to transition into invalid animation state '"
                     << t->new_state_name << "'\n";
      }
    }

    // Update animation tree
    auto optional_node = m_tree_root->update(dt, m_animation_notify_events);
    // Bit of a hack to attach entity handle to the notify events
    for (auto& anim_event : m_animation_notify_events) {
      anim_event.handle = entity_handle;
    }

    // If optional_node contains a value, that value should become the new root node
    if (optional_node.has_value()) {
      m_tree_root = std::move(*optional_node);
    }

    // m_tree_root->debug_print();
  }
}

int AnimationStateMachine::get_start_joint() const {
  if (m_current_state >= 0) {
    return m_description->states[m_current_state].m_animation->get_start_joint();
  } else {
    return 0;
  }
}

const AnimationBaseTreeNode* AnimationStateMachine::get_tree() const { return m_tree_root.get(); }

int AnimationStateMachine::find_state(const std::string& state_name) const {
  if (m_description) {
    for (int i = 0; i < m_description->states.size(); i++) {
      if (m_description->states[i].m_state_name == state_name) {
        return i;
      }
    }
  }
  return -1;
}

void AnimationStateMachine::move_from(AnimationStateMachine&& other) {
  m_machine_name = std::move(other.m_machine_name);
  m_description = other.m_description;
  m_current_state = other.m_current_state;

  m_tree_root = std::move(other.m_tree_root);
}

void AnimationStateMachine::set_machine_and_state(const std::string& machine_name,
                                                  const std::string& start_state) {
  const AnimationStateMachineDescription* desc =
      get_animation_state_machine_description(machine_name);

  if (desc && desc->states.size()) {
    m_machine_name = machine_name;
    m_description = desc;

    set_state(start_state);
  } else {
    LOG(WARNING) << "Could not find animation state machine '" << machine_name << "'";
  }
}

void AnimationStateMachine::set_machine_deserialize(const std::string& machine_name) {
  const AnimationStateMachineDescription* desc =
      get_animation_state_machine_description(machine_name);

  if (desc && desc->states.size()) {
    m_machine_name = machine_name;
    m_description = desc;
  } else {
    LOG(WARNING) << "Could not find animation state machine '" << machine_name << "'";
  }
}

void AnimationStateMachine::set_machine(const std::string& machine_name) {
  const AnimationStateMachineDescription* desc =
      get_animation_state_machine_description(machine_name);

  if (desc && desc->states.size()) {
    m_machine_name = machine_name;
    m_description = desc;

    set_state(desc->states[0].m_state_name);
  } else {
    LOG(WARNING) << "Could not find animation state machine '" << machine_name << "'";
  }
}

void AnimationStateMachine::set_state(const std::string& state) {
  int i = find_state(state);
  if (i >= 0) {
    m_current_state = i;

    // Replace tree with new animation node
    AnimationNode* an = new AnimationNode();
    an->set_animation(m_description->states[i].m_animation, m_description->skeleton,
                      m_description->states[i].m_state_name);
    an->set_machine_weight(m_description->states[i].m_machine_weight);
    an->set_looping(m_description->states[i].m_loop);
    m_tree_root.reset(an);
  } else {
    m_current_state = -1;
  }
}

void AnimationStateMachine::copy_from(const AnimationStateMachine& other) {
  if (other.m_tree_root) {
    m_tree_root = other.m_tree_root->copy();
  }
  m_machine_name = other.m_machine_name;
  m_description = other.m_description;
  m_current_state = other.m_current_state;
}

AnimationStateMachine::AnimationStateMachine(const AnimationStateMachine& other) {
  copy_from(other);
}
AnimationStateMachine& AnimationStateMachine::operator=(const AnimationStateMachine& other) {
  copy_from(other);

  return *this;
}
}  // namespace gmp