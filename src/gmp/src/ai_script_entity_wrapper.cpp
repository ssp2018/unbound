#include "gmp/ai_script_entity_wrapper.hpp"  // for AIScrip...

#include "aud/sound_source.hpp"    // for SoundSo..
#include "bse/assert.hpp"          // for ASSERT
#include "bse/file_path.hpp"       // for FilePath
#include "cor/entity.tcc"          // for Entity
#include "cor/entity_handle.hpp"   // for EntityH...
#include "cor/entity_manager.tcc"  // for EntityM...
#include "cor/frame_context.hpp"   // for FrameCo...
#include "cor/key.hpp"             // for Key
#include <aud/sound_buffer.hpp>
#include <bse/asset.hpp>
#include <cor/event.hpp>          // for PlaySou...
#include <cor/event_manager.hpp>  // for EventMa...
#include <gfx/model.hpp>
#include <gfx/shader.hpp>
#include <gfx/texture.hpp>
#include <gmp/components/ai_component.hpp>
#include <gmp/components/audio_component.hpp>
#include <gmp/components/character_controller_component.hpp>  // for Charact...
#include <gmp/components/damage_component.hpp>
#include <gmp/components/faction_component.hpp>  // for Faction...
#include <gmp/components/health_component.hpp>
#include <gmp/components/physics_projectile_component.hpp>
#include <gmp/components/transform_component.hpp>

gmp::AIScriptEntityWrapper::AIScriptEntityWrapper(cor::Entity* e, cor::FrameContext& context) {
  m_entity = e;
  m_context = &context;
}

gmp::AIScriptEntityWrapper::AIScriptEntityWrapper(cor::ConstEntity* e,
                                                  const cor::FrameContext& context) {
  m_entity = e;
  m_context = &context;
}

gmp::AIScriptEntityWrapper::AIScriptEntityWrapper() {}

gmp::AIScriptEntityWrapper::~AIScriptEntityWrapper() {}

std::variant<cor::Entity*, cor::ConstEntity*> gmp::AIScriptEntityWrapper::get_entity() {
  return m_entity;
}

std::variant<cor::FrameContext*, const cor::FrameContext*>
gmp::AIScriptEntityWrapper::get_frame_context() {
  return m_context;
}

cor::EntityManager* gmp::AIScriptEntityWrapper::get_entity_manager() {
  if (auto fc = std::get_if<cor::FrameContext*>(&m_context)) {
    return &(*fc)->entity_mgr;
  } else {
    auto cfc = std::get_if<const cor::FrameContext*>(&m_context);
    return const_cast<cor::EntityManager*>(&(*cfc)->entity_mgr);
  }
}

cor::EventManager* gmp::AIScriptEntityWrapper::get_event_manager() {
  if (auto fc = std::get_if<cor::FrameContext*>(&m_context)) {
    return &(*fc)->event_mgr;
  } else {
    auto cfc = std::get_if<const cor::FrameContext*>(&m_context);
    return const_cast<cor::EventManager*>(&(*cfc)->event_mgr);
  }
}

gmp::TimeInfo gmp::AIScriptEntityWrapper::get_time_info() {
  TimeInfo ti;

  if (auto fc = std::get_if<cor::FrameContext*>(&m_context)) {
    ti.dt = (*fc)->dt;
    ti.speed_factor = (*fc)->speed_factor;
    ti.unslowed_factor = (*fc)->unslowed_dt;
  } else {
    auto cfc = std::get_if<const cor::FrameContext*>(&m_context);
    ti.dt = (*cfc)->dt;
    ti.speed_factor = (*cfc)->speed_factor;
    ti.unslowed_factor = (*cfc)->unslowed_dt;
  }

  return ti;
}

bse::FilePath gmp::AIScriptEntityWrapper::get_asset_path(std::string path) {
  return (bse::ASSETS_ROOT / bse::FilePath(path));
}

bse::Asset<gfx::Model> gmp::AIScriptEntityWrapper::get_model_asset(std::string path) {
  return bse::Asset<gfx::Model>(bse::ASSETS_ROOT / bse::FilePath(path));
}

bse::Asset<gfx::Texture> gmp::AIScriptEntityWrapper::get_texture_asset(std::string path) {
  return bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / bse::FilePath(path));
}

bse::Asset<aud::SoundBuffer> gmp::AIScriptEntityWrapper::get_sound_buffer_asset(std::string path) {
  return bse::Asset<aud::SoundBuffer>(bse::ASSETS_ROOT / bse::FilePath(path));
}

bse::Asset<gfx::Shader> gmp::AIScriptEntityWrapper::get_shader_asset(std::string path) {
  return bse::Asset<gfx::Shader>(bse::ASSETS_ROOT / bse::FilePath(path));
}

std::shared_ptr<phy::CollisionShape> gmp::AIScriptEntityWrapper::get_box_shared_pointer(float x,
                                                                                        float y,
                                                                                        float z) {
  return std::make_shared<phy::BoxCollisionShape>(glm::vec3(x, y, z));
}

std::shared_ptr<phy::CollisionShape> gmp::AIScriptEntityWrapper::get_capsule_shared_pointer(
    float x, float y) {
  return std::make_shared<phy::CapsuleCollisionShape>(x, y);
}

void gmp::AIScriptEntityWrapper::turn_towards(float x, float y, float z) {
  cor::Entity* ent;
  if (auto en = std::get_if<cor::Entity*>(&m_entity)) {
    ent = *en;
  } else {
    LOG(WARNING) << "attempted to write to component in read update";
    return;
  }

  glm::vec3 look_at(x, y, z);
  auto cc = ent->get_if<CharacterControllerComponent>();
  ASSERT(cc != nullptr) << "Entity does not contain a CharacterControllerComponent";
  auto tc = ent->get_if<TransformComponent>();
  ASSERT(tc != nullptr) << "Entity does not contain a TransformComponent";
  glm::vec3 look_dir = look_at - tc->transform.get_position();
  look_dir = glm::normalize(look_dir);
  cc->contents.set_facing_direction(look_dir);
}

void gmp::AIScriptEntityWrapper::turn_towards_target() {
  cor::Entity* ent;
  cor::FrameContext* fc;

  if (auto en = std::get_if<cor::Entity*>(&m_entity)) {
    ent = *en;
    auto frame = std::get_if<cor::FrameContext*>(&m_context);
    fc = *frame;
  } else {
    LOG(WARNING) << "attempted to write to component in read update";
    return;
  }

  AIComponent* aic = ent->get_if<AIComponent>();
  TransformComponent* target_tc =
      fc->entity_mgr.get_entity(aic->target).get_if<TransformComponent>();
  auto cc = ent->get_if<CharacterControllerComponent>();
  ASSERT(cc != nullptr) << "Entity does not contain a CharacterControllerComponent";
  auto tc = ent->get_if<TransformComponent>();
  ASSERT(tc != nullptr) << "Entity does not contain a TransformComponent";
  glm::vec3 look_dir = target_tc->transform.get_position() - tc->transform.get_position();
  look_dir = glm::normalize(look_dir);
  cc->contents.set_facing_direction(look_dir);
}

bool gmp::AIScriptEntityWrapper::listen_to_surroundings() {
  cor::ConstEntity* ent;
  const cor::FrameContext* fr;

  if (auto cen = std::get_if<cor::ConstEntity*>(&m_entity)) {
    ent = *cen;
    auto cframe = std::get_if<const cor::FrameContext*>(&m_context);
    fr = *cframe;
  } else {
    auto en = std::get_if<cor::Entity*>(&m_entity);
    auto frame = std::get_if<cor::FrameContext*>(&m_context);
    ent = (cor::ConstEntity*)en;
    fr = *frame;
  }

  const TransformComponent& ai_tc = *ent;
  const FactionComponent& fc = *ent;
  // float hearing_dist = 120;  // this should be unique to each AI, but its not

  cor::Key sound_key = cor::Key::create<AudioComponent, TransformComponent>();
  auto sound_enteties = fr->entity_mgr.get_entities(sound_key);

  for (auto sound_entity_handle : sound_enteties) {
    cor::ConstEntity sound_entity = fr->entity_mgr.get_entity(sound_entity_handle);
    const AudioComponent& ac = sound_entity;

    // if the entity has sound playing
    if (!ac.audio.sources.empty()) {
      const TransformComponent& tc = sound_entity;
      glm::vec3 closest_sound = {0, 0, 0};
      float dist = FLT_MAX;

      // for each sound source
      for (const bse::RuntimeAsset<aud::SoundSource>& source : ac.audio.sources) {
        // if the faction components does not match
        if (source->get_type() != fc.sound_category) {
          glm::vec3 tmp = (ai_tc.transform.get_position() - tc.transform.get_position());
          float curr_dist = std::sqrt(tmp.x * tmp.x + tmp.y * tmp.y + tmp.z * tmp.z);
          // if the sound source is within hearing distance for the ai
          if (curr_dist < source->get_hearing_distance()) {
            if (curr_dist < dist) {
              // save the position if it is the closest sound so far
              closest_sound = tc.transform.get_position();
              dist = curr_dist;
            }
          }
        }
      }

      // Check if there was a sound source close enough to the ai and set the closest sound
      // position
      if (dist != FLT_MAX) {
        auto aic = ent->get_if<AIComponent>();
        // aic->search_location = glm::vec3(closest_sound.x, closest_sound.y, closest_sound.z);
        m_search_location_list.push_back(
            glm::vec3(closest_sound.x, closest_sound.y, closest_sound.z));
        return true;
      }
    }
  }
  return false;
}

bool gmp::AIScriptEntityWrapper::look_at_surroundings() {
  cor::ConstEntity* ent;
  const cor::FrameContext* fr;

  if (auto cen = std::get_if<cor::ConstEntity*>(&m_entity)) {
    ent = *cen;
    auto cframe = std::get_if<const cor::FrameContext*>(&m_context);
    fr = *cframe;
  } else {
    auto en = std::get_if<cor::Entity*>(&m_entity);
    auto frame = std::get_if<cor::FrameContext*>(&m_context);
    ent = (cor::ConstEntity*)en;
    fr = *frame;
  }

  const FactionComponent& fc = *ent->get_if<FactionComponent>();
  const AIComponent& aic = *ent->get_if<AIComponent>();
  const TransformComponent& tc = *ent->get_if<TransformComponent>();

  cor::Key key = cor::Key::create<FactionComponent, TransformComponent>();
  auto entities = fr->entity_mgr.get_entities(key);

  for (auto entity_handle : entities) {
    auto entity = fr->entity_mgr.get_entity(entity_handle);
    if (!entity.has<PhysicsProjectileComponent>() && !entity.has<DamageComponent>()) {
      const TransformComponent& new_tc = entity;
      const FactionComponent& new_fc = entity;

      if (fc.type != new_fc.type) {
        glm::vec3 tmp = (tc.transform.get_position() - new_tc.transform.get_position());
        float dist = std::sqrt(tmp.x * tmp.x + tmp.y * tmp.y + tmp.z * tmp.z);
        if (dist < aic.aggro_range) {
          auto te = AddThreatEvent();
          te.self = ent->get_handle();
          te.other = entity_handle;
          te.threat_level = 30;
          // fr->event_mgr.send_event(te);
          m_threat_event_list.push_back(te);
          return true;
        }
      }
    }
  }

  return false;
}

void gmp::AIScriptEntityWrapper::send_play_sound_event(std::string path, float pitch, float gain,
                                                       bool loop) {
  cor::ConstEntity* ce;
  cor::FrameContext* fc;

  if (auto frame = std::get_if<cor::FrameContext*>(&m_context)) {
    fc = *frame;
  } else {
    LOG(WARNING) << "attempted to play sound in read update";
    return;
  }

  if (auto ent = std::get_if<cor::ConstEntity*>(&m_entity)) {
    ce = *ent;
  } else {
    auto cfc = std::get_if<cor::Entity*>(&m_entity);
    ce = (cor::ConstEntity*)(*cfc);
  }

  PlaySoundEvent e;
  e.buffer_path = path.c_str();
  e.id = ce->get_handle();
  e.settings.gain = gain;
  e.settings.pitch = pitch;
  e.settings.loop = loop;
  fc->event_mgr.send_event(e);
}

void gmp::AIScriptEntityWrapper::activate_repeating_audio_group(std::string group, bool val) {
  cor::Entity* ent;
  if (auto en = std::get_if<cor::Entity*>(&m_entity)) {
    ent = *en;
  } else {
    LOG(WARNING) << "attempted to write to AudioComponent in read update";
    return;
  }

  AudioComponent& ac = *ent;
  ac.audio.set_looping_sound_group_enabled(group, val);
}

void gmp::AIScriptEntityWrapper::set_repeat_intervall(std::string group, float val) {
  cor::Entity* ent;
  if (auto en = std::get_if<cor::Entity*>(&m_entity)) {
    ent = *en;
  } else {
    LOG(WARNING) << "attempted to write to AudioComponent in read update";
    return;
  }

  AudioComponent& ac = *ent;
  ac.audio.set_intervall_time_limit(group, val);
}
