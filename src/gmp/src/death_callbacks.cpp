#include "cor/entity.hpp"          // for Entity
#include "cor/entity_handle.hpp"   // for EntityHandle
#include "cor/entity_manager.hpp"  // for EntityManager
#include "cor/event_manager.hpp"   // for EntityManager
//#include "gmp/animation/animation_state.hpp"  // for AnimationState
#include <cor/entity.tcc>
#include <cor/entity_manager.tcc>
#include <gmp/ai_script_entity_wrapper.hpp>
#include <gmp/components/ai_component.hpp>                    // for AnimationComponent
#include <gmp/components/animation_component.hpp>             // for AnimationComponent
#include <gmp/components/audio_component.hpp>                 // for AnimationComponent
#include <gmp/components/character_controller_component.hpp>  // for AnimationComponent
#include <gmp/components/destroyed_component.hpp>
#include <gmp/components/explode_component.hpp>
#include <gmp/components/health_component.hpp>  // for AnimationComponent
#include <gmp/components/lifetime_component.hpp>
#include <gmp/components/particles_component.hpp>
#include <gmp/components/transform_component.hpp>
#include <gmp/death_callbacks.hpp>  // for ai_death_callback
#include <gmp/gmp_helper_functions.hpp>
#include <scr/scr.tcc>

namespace gmp {
struct AIComponent;
}
namespace gmp {
struct AudioComponent;
}
namespace gmp {
struct CharacterControllerComponent;
}
namespace gmp {
struct HealthComponent;
}
void ai_death_callback(cor::EntityHandle entity_handle, cor::EventManager& event_manager,
                       cor::EntityManager& entity_mgr) {
  cor::Entity e = entity_mgr.get_entity(entity_handle);
  if (e.is_valid()) {
    // Detach unwanted components
    gmp::AIComponent& aic = e;
    event_manager.send_event(MoraleLossEvent(0, aic.morale_data.group_name, entity_handle));
    if (aic.death_sound_path.get_string() != "") {
      cor::EntityHandle sound_entity_h = entity_mgr.create_entity();
      cor::Entity sound_entity = entity_mgr.get_entity(sound_entity_h);

      gmp::AudioComponent auc;
      sound_entity = std::move(auc);
      // play death sound here
      PlaySoundEvent pse;
      pse.buffer_path = aic.death_sound_path;
      pse.id = sound_entity_h;

      event_manager.send_event(pse);

      bse::Asset<aud::SoundBuffer> asset = bse::Asset<aud::SoundBuffer>(aic.death_sound_path);
      gmp::LifetimeComponent lfc;
      lfc.time_left = asset->get_buffer_time_length();
      sound_entity = lfc;
    }
    e.detach<gmp::AIComponent>();
    e.detach<gmp::CharacterControllerComponent>();
    e.detach<gmp::AudioComponent>();
    e.detach<gmp::HealthComponent>();
    // Do pause on animation component so it stops after death animation is done.
    auto& ac = e.get<gmp::AnimationComponent>();
    // ac.state.pause();
  }
}

void skull_death_callback(gmp::AIScriptEntityWrapper e, cor::EntityHandle entity_handle,
                          cor::EventManager& event_manager, cor::EntityManager& entity_mgr,
                          cor::FrameContext& context) {
  cor::Entity entity = entity_mgr.get_entity(entity_handle);
  gmp::AIComponent& aic = entity;
  entity.detach<gmp::HealthComponent>();
  entity.attach<gmp::DestroyedComponent>();
  if (aic.death_sound_path.get_string() != "") {
    cor::EntityHandle sound_entity_h = entity_mgr.create_entity();
    cor::Entity sound_entity = entity_mgr.get_entity(sound_entity_h);

    gmp::AudioComponent auc;
    sound_entity = std::move(auc);
    // play death sound here PlaySoundEvent pse;

    PlaySoundEvent pse;
    pse.buffer_path = aic.death_sound_path;
    pse.settings = aic.death_sound_settings;
    pse.id = sound_entity_h;

    event_manager.send_event(pse);
  }

  gmp::create_particle_explosion(context.entity_mgr.get_entity(entity_handle), context);

  entity_mgr.destroy_entity(entity_handle);
}

void expose_death_callbacks() {
  scr::expose_function("ai_death_callback", ai_death_callback);
  scr::expose_function("skull_death_callback", skull_death_callback);
}