#include "gmp/player/player_drawing.hpp"

#include "aud/sound_buffer.hpp"    // for SoundBu...
#include "bse/asset.hpp"           // for Asset
#include "bse/directory_path.hpp"  // for ASSETS_...
#include "bse/file_path.hpp"       // for operator/
#include "cor/entity_manager.tcc"  // for EntityM...
#include "cor/event.hpp"           // for PlaySou...
#include "cor/event_manager.hpp"   // for EventMa...
#include "cor/frame_context.hpp"   // for FrameCo...
#include "gfx/model.hpp"           // for Model
#include "gfx/texture.hpp"         // for Texture
#include "gmp/components/attach_to_joint_component.hpp"
#include "gmp/components/audio_component.hpp"                 // for AudioClass
#include "gmp/components/character_controller_component.hpp"  // for Charact...
#include "gmp/components/damage_component.hpp"                // for DamageC...
#include "gmp/components/destroyed_component.hpp"             // for Destroy...
#include "gmp/components/destructible_component.hpp"
#include "gmp/components/explode_component.hpp"
#include "gmp/components/lifetime_component.hpp"     // for Lifetim...
#include "gmp/components/model_component.hpp"        // for ModelCo...
#include "gmp/components/particles_component.hpp"    // for Particl...
#include "gmp/components/player_component.hpp"       // for PlayerC...
#include "gmp/components/slow_motion_component.hpp"  // for SlowMot...
#include "gmp/components/transform_component.hpp"    // for Transfo...
#include "gmp/gmp_helper_functions.hpp"
#include "gmp/systems/player_system.hpp"  // for PlayerS...
#include <cor/entity.tcc>                 // for Entity
#include <gmp/components/name_component.hpp>
#include <gmp/components/physics_projectile_component.hpp>  // for Physics...
#include <gmp/components/rope_component.hpp>                // for RopeCom...

namespace gmp {

PlayerDrawing::PlayerDrawing(PlayerSystem* player_system) : PlayerBaseState(player_system) {
  m_state_type = StateType::DRAWING;
}

void PlayerDrawing::enter(cor::FrameContext& context) {
  m_player_system->m_face_aiming_dir_states++;

  m_arrow_handle = m_player_system->get_new_arrow();
  auto arrow = context.entity_mgr.get_entity(m_arrow_handle);

  auto player = context.entity_mgr.get_entity(m_player_system->m_player_handle);
  PlayerComponent& player_component = player;
  player_component.is_attacking = true;
  m_player_system->m_time_left_of_attacking_anim = 0.15f;

  AttachToJointComponent atjc;
  atjc.entity_handle = m_player_system->m_player_handle;
  atjc.joint_name = "R_hand";
  atjc.offset_matrix = glm::rotate(3.1415f * 0.5f, glm::vec3(0, 0, 1)) *
                       glm::rotate(3.1415f * -0.072f, glm::vec3(1, 0, 0)) *  // Vertical
                       glm::rotate(3.1415f * -0.092f, glm::vec3(0, 0, 1)) *  // Horizontal
                       glm::translate(glm::vec3(-1.f, 57.f, 0.f));  // Left/right, Back/front,
  arrow = atjc;

  // Particles
  ParticlesComponent pc;
  pc.min_start_size = 0.04f;
  pc.max_start_size = 0.08f;
  pc.min_life = 0.1f;
  pc.max_life = 0.23f;
  pc.spawn_per_second = 0;
  const float grey_scale = 0.05f;
  pc.min_start_color = glm::vec4(grey_scale, grey_scale, grey_scale, 1);
  pc.max_start_color = glm::vec4(grey_scale, grey_scale, grey_scale, 1);
  pc.min_end_color = glm::vec4(grey_scale, grey_scale, grey_scale, 1);
  pc.max_end_color = glm::vec4(grey_scale, grey_scale, grey_scale, 1);
  pc.min_start_velocity = glm::vec3(0);
  pc.max_start_velocity = glm::vec3(0);
  const float var = 0.05f;
  pc.min_spawn_pos = glm::vec3(-var);
  pc.max_spawn_pos = glm::vec3(var);
  pc.min_rotation_speed = 2.f;
  pc.max_rotation_speed = 4.f;
  arrow = pc;

  m_type_when_entering = m_player_system->m_selected_arrow;

  // Drawing bow sound effect
  aud::SoundSettings s2;
  s2.gain = 1.0f;
  context.event_mgr.send_event(PlaySoundEvent(
      m_player_system->m_player_handle,
      bse::ASSETS_ROOT / "audio/effects/bow_and_arrow/tighten_bow/post/tighten2.ogg"_fp, s2));
}

void PlayerDrawing::update(cor::FrameContext& context, const PlayerEvents& keys_pressed) {
  //
  auto player = context.entity_mgr.get_entity(m_player_system->m_player_handle);
  TransformComponent& ptc = player;
  CharacterControllerComponent& cc = player;
  PlayerComponent& player_component = player;

  auto camera = context.entity_mgr.get_entity(m_player_system->m_camera_handle);
  TransformComponent& ctc = camera;

  auto arrow = context.entity_mgr.get_entity(m_arrow_handle);

  const SlowMotionComponent& smc = player;
  m_player_system->m_draw_time +=
      fmaxf(smc.factor.get_converted_dt(context), context.unslowed_dt * 0.5f);

  // Position and rotation
  TransformComponent& tc = arrow;

  ParticlesComponent& pc = arrow;
  pc.min_spawn_pos = glm::vec3(tc.transform.get_rotation() * glm::vec4(-0.05, 0.45, -0.05, 0));
  pc.max_spawn_pos = glm::vec3(tc.transform.get_rotation() * glm::vec4(0.05, 0.55, 0.05, 0));

  // Full charge done
  if (m_player_system->m_draw_time > m_player_system->m_max_draw_time) {
    ++m_frames_charged;
  }
  // Particle burst on full charge
  if (m_frames_charged == 1) {
    pc.spawn_per_second = 200.f;
    pc.max_life = 0.35f;
    const float vel = 9.f;
    pc.min_start_velocity = glm::vec3(-vel);
    pc.max_start_velocity = glm::vec3(vel);
    pc.velocity_mode = ParticlesComponent::Velocity::DECELERATE;
    pc.min_rotation_speed = 5.f;
    pc.max_rotation_speed = 7.f;

    // Arrow type specific things
    switch (m_type_when_entering) {
      case PlayerSystem::ArrowType::PIERCING: {
        // Particles
        pc.min_start_color = glm::vec4(0.2, 0.2, 0.6, 1);
        pc.max_start_color = glm::vec4(0.2, 0.2, 0.6, 1);
        pc.min_end_color = glm::vec4(0, 0, .1, 1);
        pc.max_end_color = glm::vec4(0, 0, .3, 1);
        break;
      }
      case PlayerSystem::ArrowType::EXPLOSIVE: {
        // Particles
        pc.min_start_color = glm::vec4(0, 0, 0, 1);
        pc.max_start_color = glm::vec4(.3, .02, 0, 1);
        pc.min_end_color = glm::vec4(0, 0, 0, 1);
        pc.max_end_color = glm::vec4(.2, 0, 0, 1);

        ExplodeComponent ec;
        ec.time = 0.9f;
        ec.damage_radius = 10;
        ec.deal_damage = true;
        ec.max_damage = 5;
        ec.particle_amount = 8000;
        ec.particle_speed = 60;
        ec.particle_start_size = 0.6f;
        ec.particle_end_size = 1.3f;
        ec.explode_on_impact = true;
        ec.texture = m_player_system->m_explosion_particle;
        arrow.attach(ec);

        arrow.get<ModelComponent>().asset = m_player_system->m_explosive_arrow_model;

        break;
      }
      default:
        break;
    }
  }
  // Make particles normal again
  else if (m_frames_charged == 2) {
    pc.spawn_per_second = 80.f;
    pc.max_life = 0.4f;
    const float var = 2.0f;
    pc.min_start_velocity = glm::vec3(-var);
    pc.max_start_velocity = glm::vec3(var);
  }

  // Shoot arrow
  if (keys_pressed.draw_ended && m_player_system->m_shoot_cooldown <= 0.f) {
    context.event_mgr.send_event(StopSoundEvent{m_player_system->m_player_handle});

    // Rotate arrow right when firing to make to hit the correct spot
    const glm::vec3 arrow_pos = tc.transform.get_position();
    tc.transform.set_model_matrix(
        glm::translate(glm::mat4(1), arrow_pos) *
            glm::toMat4(
                glm::rotation(glm::vec3(0, 1, 0),
                              glm::normalize(m_player_system->m_arrow_aim_point - arrow_pos))),
        context);

    arrow.detach<AttachToJointComponent>();

    // Hitbox and physics
    PhysicsProjectileComponent ppc;
    ppc.contents.set_velocity(tc.transform.get_model_matrix() * glm::vec4(0, 200, 0, 0));
    ppc.stick_to_animated = true;
    const float scale = 0.8f;
    ppc.contents.set_offset_transform(glm::scale(glm::vec3(scale, scale, scale)));
    ppc.contents.set_radius(0.07f);
    ppc.contents.set_length(0.07f);
    ppc.character_group = phy::CollisionGroup::ENEMY;
    arrow = ppc;

    // Particles
    pc.max_life = .8f;
    pc.spawn_per_second = 250;
    const float var = 0.05f;
    pc.min_spawn_pos = glm::vec3(-var);
    pc.max_spawn_pos = glm::vec3(var);

    aud::SoundSettings s2;
    s2.gain = 0.1f;
    // play arrow flying sound
    context.event_mgr.send_event(PlaySoundEvent(
        m_player_system->m_player_handle,
        bse::ASSETS_ROOT / "audio/effects/bow_and_arrow/flying_arrow/post/flying1.ogg"_fp, s2));

    // Full draw
    aud::SoundSettings s3;
    s3.gain = 0.4f;
    if (m_player_system->m_draw_time > m_player_system->m_max_draw_time) {
      const float var = 0.8f;
      pc.min_start_velocity = glm::vec3(-var);
      pc.max_start_velocity = glm::vec3(var);

      if (m_type_when_entering == PlayerSystem::ArrowType::PIERCING) {
        arrow.get<DamageComponent>().damage_value = 4;
      } else if (m_type_when_entering == PlayerSystem::ArrowType::EXPLOSIVE) {
        arrow.get<AudioComponent>().audio.set_collision_sound(
            m_player_system->m_explosion_sound,
            {std::make_pair(MaterialType::GENERIC, aud::SoundSettings())});
        m_player_system->m_explosive_arrows_count--;
        context.event_mgr.send_event(
            ExplosiveArrowCountChanged{m_player_system->m_explosive_arrows_count});

        if (m_player_system->m_explosive_arrows_count == 0) {
          m_player_system->m_selected_arrow = PlayerSystem::ArrowType::PIERCING;
          context.event_mgr.send_event(NormalArrowModeEvent());

          // play bow release sound effect
          context.event_mgr.send_event(PlaySoundEvent(
              m_player_system->m_player_handle,
              bse::ASSETS_ROOT /
                  "audio/effects/bow_and_arrow/arrows_out/post/explosives_out.ogg"_fp,
              aud::SoundSettings()));
        }
      }
      s3.gain = 0.8f;
    } else {
      // Be destructible
      DestructibleComponent destructible;
      destructible.max_health_points = 1;
      destructible.health_points = 1;
      arrow = destructible;
    }

    // play bow release sound effect
    context.event_mgr.send_event(PlaySoundEvent(
        m_player_system->m_player_handle,
        bse::ASSETS_ROOT / "audio/effects/bow_and_arrow/release_arrow/post/release1.ogg"_fp, s3));

    m_player_system->m_shoot_cooldown = m_player_system->m_max_shoot_cooldown;
    m_player_system->m_face_aiming_dir_states--;

    m_player_system->add_action(ActionType::REMOVE);

    player_component.is_attacking = false;
    m_player_system->m_time_left_of_attacking_anim = -1;
    m_player_system->m_draw_time = 0.f;

    m_player_system->m_flying_arrows.push_back(
        {m_arrow_handle, 0.13f,
         glm::normalize(m_player_system->m_camera_hit_point - ctc.transform.get_position()),
         m_player_system->m_arrow_aim_point, 5.f});

    m_arrow_handle = cor::EntityHandle::NULL_HANDLE;
  }
  // Cancel drawing
  else if (keys_pressed.action_canceled ||
           (keys_pressed.draw_ended && m_player_system->m_shoot_cooldown > 0.f)) {
    m_player_system->m_face_aiming_dir_states--;

    destroy_arrow(m_arrow_handle, context);
    m_player_system->m_unused_arrows.push_back(m_arrow_handle);
    m_arrow_handle = cor::EntityHandle::NULL_HANDLE;

    m_player_system->add_action(ActionType::REMOVE);

    player_component.is_attacking = false;
    m_player_system->m_time_left_of_attacking_anim = -1;
    m_player_system->m_draw_time = 0.f;
  }
}

PlayerDrawing::~PlayerDrawing() {
  //
  if (m_arrow_handle != cor::EntityHandle::NULL_HANDLE) {
    destroy_arrow(m_arrow_handle, m_player_system->m_context);
    m_player_system->m_unused_arrows.push_back(m_arrow_handle);

    m_player_system->m_time_left_of_attacking_anim = -1;
    m_player_system->m_draw_time = 0.f;
  }
}

};  // namespace gmp