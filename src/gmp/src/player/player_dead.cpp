#include "gmp/player/player_dead.hpp"

#include "bse/log.hpp"              // for NOTICE, LOG
#include "cor/entity.tcc"           // for Entity
#include "cor/entity_manager.tcc"   // for EntityManager
#include "cor/event.hpp"            // for EnableSystem
#include "cor/event_manager.hpp"    // for EventManager
#include "cor/frame_context.hpp"    // for FrameContext
#include "cor/system_id_proxy.hpp"  // for SystemIDProxy
#include "gmp/components/character_controller_component.hpp"
#include "gmp/components/health_component.hpp"  // for HealthComponent
#include "gmp/components/player_component.hpp"
#include "gmp/components/player_damage_visual_component.hpp"
#include "gmp/components/saturation_component.hpp"
#include "gmp/components/transform_component.hpp"  // for Transform, Transfo...
#include "gmp/player/player_common_state.hpp"
#include "gmp/player/player_in_air.hpp"    // for PlayerInAir
#include "gmp/player/player_standing.hpp"  // for PlayerStanding
#include "gmp/systems/player_system.hpp"   // for PlayerSystem

namespace gmp {
class AISystem;
}
namespace gmp {
class AnimationSystem;
}
namespace gmp {
class PhysicsSystem;
}
namespace gmp {

PlayerDead::PlayerDead(PlayerSystem* player_system) : PlayerBaseState(player_system) {
  m_state_type = StateType::DEAD;
}

void PlayerDead::enter(cor::FrameContext& context) {
  LOG(NOTICE) << "Player dieded\n";

  auto player = context.entity_mgr.get_entity(m_player_system->m_player_handle);
  PlayerComponent& pc = player;
  pc.is_dead = true;
}

void PlayerDead::update(cor::FrameContext& context, const PlayerEvents& keys_pressed) {
  // Saturation
  cor::Key saturation_key = cor::Key::create<SaturationComponent>();
  auto saturation_entity =
      context.entity_mgr.get_entity(context.entity_mgr.get_entities(saturation_key)[0]);
  SaturationComponent& satc = saturation_entity;
  satc.saturation -= 0.1 * context.dt;
  if (satc.saturation < 0.05f) satc.saturation = 0.05f;

  // Camera
  auto camera = context.entity_mgr.get_entity(m_player_system->m_camera_handle);
  auto player = context.entity_mgr.get_entity(m_player_system->m_player_handle);
  TransformComponent& ctc = camera;
  const TransformComponent& ptc = player;

  ctc.transform.set_model_matrix(
      glm::inverse(glm::lookAt(ctc.transform.get_position() + glm::vec3(0, 0, 7) * context.dt,
                               ptc.transform.get_position(), glm::vec3(0, 0, 1))),
      context);

  m_time_before_exiting -= context.dt;

  if (keys_pressed.jumped || m_time_before_exiting < 0.f) {
    context.event_mgr.send_event(LoadCheckpoint());
  }
}
};  // namespace gmp