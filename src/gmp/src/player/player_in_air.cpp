#include "gmp/player/player_in_air.hpp"

#include "bse/directory_path.hpp"                             // for ASSETS_...
#include "bse/file_path.hpp"                                  // for operator/
#include "cor/entity_manager.tcc"                             // for EntityM...
#include "cor/event.hpp"                                      // for PlaySou...
#include "cor/event_manager.hpp"                              // for EventMa...
#include "cor/frame_context.hpp"                              // for FrameCo...
#include "gmp/components/character_controller_component.hpp"  // for Charact...
#include "gmp/components/particles_component.hpp"             // for Particl...
#include "gmp/components/slow_motion_component.hpp"           // for SlowMot...
#include "gmp/components/transform_component.hpp"             // for Transform
#include "gmp/gmp_helper_functions.hpp"
#include "gmp/player/player_aiming.hpp"       // for PlayerA...
#include "gmp/player/player_dashing.hpp"      // for PlayerD...
#include "gmp/player/player_drawing.hpp"      // for PlayerD...
#include "gmp/player/player_slow_motion.hpp"  // for PlayerS...
#include "gmp/player/player_standing.hpp"     // for PlayerS...
#include "gmp/player/player_swinging.hpp"     // for PlayerS...
#include "gmp/systems/player_system.hpp"      // for PlayerS...
#include <cor/entity.tcc>                     // for Entity

namespace gmp {

  PlayerInAir::PlayerInAir(PlayerSystem* player_system, bool just_falling, glm::vec2 horizontal_vel)
    : PlayerBaseState(player_system),
    m_just_falling(just_falling),
    m_frames_in_state(0),
    m_horizontal_velocity(horizontal_vel),
    m_history_index(0) {
    m_state_type = StateType::IN_AIR;
  }

  void PlayerInAir::enter(cor::FrameContext& context) {
    //
    auto player = context.entity_mgr.get_entity(m_player_system->m_player_handle);
    CharacterControllerComponent& cc = player;

    // Check if it was an actual jump or just started falling
    if (!m_just_falling) {
      m_player_system->m_jumps_left--;

      if (cc.contents.on_ground()) {
        context.event_mgr.send_event(PlaySoundEvent(
          m_player_system->m_player_handle,
          bse::ASSETS_ROOT / "audio/effects/landing/post/thud.ogg"_fp, aud::SoundSettings()));
      }
      else {
        context.event_mgr.send_event(
          PlaySoundEvent(player.get_handle(), bse::ASSETS_ROOT / "audio/effects/dash/dash.ogg"_fp,
            aud::SoundSettings()));
        context.event_mgr.send_event(
          AnimationEvent("player_mesh_anm_frontflip", m_player_system->m_player_handle, 0.2f));
      }

      // Doing the actual jumping
      cc.contents.jump(50.f);

      // Particles
      TransformComponent& ptc = player;
      cor::Entity burst =
        context.entity_mgr.get_entity(create_particle_burst(ptc.transform.get_position(), context));
      ParticlesComponent& pc = burst;
      pc.spawn_per_second = 260;
      pc.min_start_color = glm::vec4(1, 1, 1, 1);
      pc.max_start_color = glm::vec4(1, 1, 1, 1);
      pc.min_end_color = glm::vec4(0.7, 0.7, 0.7, 1);
      pc.max_end_color = glm::vec4(0.7, 0.7, 0.7, 1);
      pc.min_start_velocity = glm::vec3(-12, -12, -2);
      pc.max_start_velocity = glm::vec3(12, 12, 3);
      pc.min_spawn_pos = glm::vec3(0, 0, 0.1);
      pc.max_spawn_pos = glm::vec3(0, 0, 0.2);
      pc.size_mode = ParticlesComponent::Size::CONSTANT;
      pc.texture = m_player_system->m_swirl_particle;
    }

    // Save horizontal speed
    m_horizontal_velocity += glm::vec2(m_player_system->m_launch_help_dir);

    // if (glm::length(m_horizontal_velocity) < 0.001) {
    // const TransformComponent& ptc = player;
    // cc.contents.set_facing_direction(ptc.transform.get_model_matrix() * glm::vec4(0, -1, 0, 0));
    // } else {
    // cc.contents.set_facing_direction(
    //    glm::vec3(m_horizontal_velocity.x, m_horizontal_velocity.y, 0));
    // }

    m_max_total_velocity = fmaxf(25.f, fminf(glm::length(m_horizontal_velocity * 1.2f), 100.f));

    m_z_velocity_history.resize(2);
  }

  void PlayerInAir::update(cor::FrameContext& context, const PlayerEvents& keys_pressed) {
    // Get if on ground
    auto player = context.entity_mgr.get_entity(m_player_system->m_player_handle);

    CharacterControllerComponent& cc = player;
    bool on_ground = cc.contents.on_ground();

    SlowMotionComponent& smc = player;

    // Falling
    if (cc.contents.get_velocity().z < -5.f)
      cc.contents.set_gravity(glm::vec3(0, 0, -90.f));
    else
      cc.contents.set_gravity(glm::vec3(0, 0, -80.f));

    // Save z velocity
    m_z_velocity_history[m_history_index] =
      fmaxf(fminf(-cc.contents.get_velocity().z / 100.f, 1.0f), 0.1f);

    m_history_index = (m_history_index + 1) % 2;

    // Swing
    if (keys_pressed.rope_hit) {
      m_player_system->add_action(ActionType::REPLACE,
        std::make_unique<PlayerSwinging>(m_player_system, context));
    }
    // Jump
    else if (keys_pressed.jumped && m_player_system->m_jumps_left > 0) {
      m_player_system->add_action(
        ActionType::REPLACE,
        std::make_unique<PlayerInAir>(m_player_system, false, m_horizontal_velocity * 0.85f));
    }
    else {
      // Move
      m_horizontal_velocity += glm::vec2(
        glm::rotate(glm::mat4(1), m_player_system->m_camera_rotation_x, glm::vec3(0, 0, 1)) *
        glm::vec4(keys_pressed.move_right, keys_pressed.move_forward, 0, 0) *
        m_player_system->m_air_movement_speed * smc.factor.get_converted_dt(context));

      m_horizontal_velocity -= glm::abs(m_horizontal_velocity) * (m_horizontal_velocity * 0.3f) *
        0.04f * smc.factor.get_converted_dt(context);

      cc.contents.set_move_vector(glm::vec3(m_horizontal_velocity.x, m_horizontal_velocity.y, 0) *
        smc.factor.get_converted_dt(context));
      // Dash
      if (keys_pressed.dashed && m_player_system->m_dashes_left >= 1 &&
        m_player_system->m_stamina_left > m_player_system->m_stamina_dash_cost) {
        // Calculate direction
        glm::vec3 direction;
        if (keys_pressed.moved) {
          TransformComponent& tc = player;
          const glm::mat4 rot_z =
            glm::rotate(glm::mat4(1), m_player_system->m_camera_rotation_x, glm::vec3(0, 0, 1));
          direction = glm::normalize(
            glm::vec3(rot_z * glm::vec4(keys_pressed.move_right, keys_pressed.move_forward, 0, 0)));
        }
        else {
          TransformComponent& tc = player;
          direction = glm::vec3(tc.transform.get_model_matrix() * glm::vec4(0, -1, 0, 0));
        }
        m_player_system->add_action(ActionType::REPLACE,
          std::make_unique<PlayerDashing>(m_player_system, direction));
      }
      // Land on something
      else if (on_ground && m_frames_in_state >= 3) {
        m_player_system->add_action(ActionType::REPLACE,
          std::make_unique<PlayerStanding>(m_player_system));

        // Particles
        TransformComponent& ptc = player;
        cor::Entity burst = context.entity_mgr.get_entity(
          create_particle_burst(ptc.transform.get_position(), context));
        ParticlesComponent& pc = burst;
        const float factor =
          *std::max_element(m_z_velocity_history.begin(), m_z_velocity_history.end());
        pc.spawn_per_second = 400 * factor;
        const float grey_scale = 0.3f;
        pc.min_start_color = glm::vec4(grey_scale, grey_scale, grey_scale, 1);
        pc.max_start_color = glm::vec4(grey_scale, grey_scale, grey_scale, 1);
        pc.min_end_color = glm::vec4(grey_scale, grey_scale, grey_scale, 1);
        pc.max_end_color = glm::vec4(grey_scale, grey_scale, grey_scale, 1);
        const float spread = 30.f * factor;
        pc.min_start_velocity = glm::vec3(-spread, -spread, 0.5);
        pc.max_start_velocity = glm::vec3(spread, spread, 1.5);
        pc.min_rotation_speed = -3.f;
        pc.max_rotation_speed = 3.f;
        pc.min_spawn_pos.z = 0.1;
        pc.max_spawn_pos.z = 0.2;
        pc.texture = m_player_system->m_explosion_particle;

        aud::SoundSettings settings;
        settings.gain = fminf(factor, 0.2f);
        // Landing sound
        context.event_mgr.send_event(
          PlaySoundEvent(m_player_system->m_player_handle,
            bse::ASSETS_ROOT / "audio/effects/landing/post/thud.ogg"_fp, settings));
      }
    }

    // Draw
    if (keys_pressed.draw_started)
      m_player_system->add_action(ActionType::PUSH, std::make_unique<PlayerDrawing>(m_player_system));

    // Rope arrow
    if (keys_pressed.rope_action) {
      m_player_system->shoot_rope_arrow();
    }

    // Aim
    if (keys_pressed.aim_started)
      m_player_system->add_action(ActionType::PUSH, std::make_unique<PlayerAiming>(m_player_system));

    // Slow motion
    if (keys_pressed.slow_motion && !m_player_system->state_exists(StateType::SLOW_MOTION) &&
      m_player_system->m_stamina_left > 0.f && m_player_system->m_has_released_slow_motion)
      m_player_system->add_action(ActionType::PUSH,
        std::make_unique<PlayerSlowMotion>(m_player_system));

    m_frames_in_state++;
  }

};  // namespace gmp