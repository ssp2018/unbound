#include "gmp/player/player_common_state.hpp"

#include "cor/entity.tcc"                                     // for Entity
#include "cor/entity_manager.tcc"                             // for EntityM...
#include "cor/frame_context.hpp"                              // for FrameCo...
#include "gmp/components/character_controller_component.hpp"  // for Charact...
#include "gmp/components/lifetime_component.hpp"              // for SlowMot...
#include "gmp/components/particles_component.hpp"             // for Particl...
#include "gmp/components/player_component.hpp"                // for PlayerC...
#include "gmp/components/projection_component.hpp"            // for Project...
#include "gmp/components/radial_component.hpp"                // for RadialC...
#include "gmp/components/saturation_component.hpp"
#include "gmp/components/slow_motion_component.hpp"  // for SlowMot...
#include "gmp/components/transform_component.hpp"    // for Transform
#include "gmp/systems/player_system.hpp"             // for PlayerS...
#include "phy/physics_manager.hpp"                   // for RayResult
#include <bse/edit.hpp>

namespace gmp {
struct LifetimeComponent;
}

namespace gmp {

PlayerCommonState::PlayerCommonState(PlayerSystem* player_system) : PlayerBaseState(player_system) {
  m_state_type = StateType::COMMON;
}

void PlayerCommonState::enter(cor::FrameContext& context) {
  m_player_system->m_face_aiming_dir_states = 0;
  m_player_system->m_launch_help_dir = glm::vec3(0);
  m_player_system->m_launch_help_active = false;
  m_player_system->m_jumps_left = 2;
  m_player_system->m_dashes_left = 2;
  m_player_system->m_time_left_of_attacking_anim = -1.f;
  m_player_system->m_desired_up = glm::vec3(0, 0, 1);

  auto player = context.entity_mgr.get_entity(m_player_system->m_player_handle);
  PlayerComponent& pc = player;
  pc.is_in_air = false;
  pc.is_attacking = false;
  pc.is_swinging = false;
  pc.is_dashing = false;
}

void PlayerCommonState::update(cor::FrameContext& context, const PlayerEvents& keys_pressed) {
  //
  auto player = context.entity_mgr.get_entity(m_player_system->m_player_handle);
  auto camera = context.entity_mgr.get_entity(m_player_system->m_camera_handle);

  // ------ Camera position ------
  TransformComponent& ctc = camera;
  TransformComponent& ptc = player;
  // Prepare
  const glm::mat4 rot_z =
      glm::rotate(glm::mat4(1), m_player_system->m_camera_rotation_x, glm::vec3(0, 0, 1));
  const glm::mat4 rot_x =
      glm::rotate(glm::mat4(1), m_player_system->m_camera_rotation_z, glm::vec3(1, 0, 0));
  const glm::mat4 rot = rot_z * rot_x;
  const float angle_z = abs(cosf(m_player_system->m_camera_rotation_z));
  const glm::vec3 player_pos = ptc.transform.get_position();
  const glm::vec3 front_dir = glm::vec3(rot * glm::vec4(0, 1, 0, 0));
  glm::vec3 cam_pos;
  const glm::vec3 offset = glm::vec3(rot_z * glm::vec4(0.25f, 0, 1.7f, 0));

  // Sweep backwards
  const glm::vec3 desired_pos = player_pos + offset + glm::vec3(rot * glm::vec4(1, -2.7, 0, 0));
  glm::vec3 diag_dir = glm::vec3(rot * glm::vec4(1, -2.7, 0, 0));
  const float diag_distance = glm::length(diag_dir);
  diag_dir = glm::normalize(diag_dir);
  const cor::SweepResult res =
      context.utilities.sphere_sweep(player_pos + offset, diag_dir, diag_distance, 0.15);
  if (res.hit)
    cam_pos = player_pos + offset + diag_dir * diag_distance * res.fraction;
  else
    cam_pos = desired_pos;

  ctc.transform.set_model_matrix(
      glm::inverse(glm::lookAt(cam_pos, cam_pos + front_dir, glm::vec3(0, 0, 1))), context);
  // ---------------

  // FOV
  ProjectionComponent& pc = camera;
  pc.field_of_view = 35 + m_player_system->m_camera_distance * 35.f;
  pc.projection_matrix =
      glm::perspective(glm::radians(pc.field_of_view), pc.aspectRatio, pc.near, pc.far);

  // Player character/model
  CharacterControllerComponent& cc = player;
  player.get<PlayerComponent>().is_in_air = !cc.contents.on_ground();

  // Raytrace camera hit point forward
  float hit_distance = m_player_system->m_max_reach + 3.f;
  std::vector<cor::RayResult> hits =
      context.utilities.raycast(ctc.transform.get_position() + front_dir, front_dir, hit_distance);
  cor::RayResult* closest_hit = m_player_system->get_closest_non_player_hit(hits, false);
  glm::vec3 hit_point;
  glm::vec3 arrow_aim_point;
  const float t = 27.f;  // The distance from the camera at which point the arrow turns
  if (closest_hit == nullptr) {
    hit_point = glm::vec3(ctc.transform.get_model_matrix() * glm::vec4(0, 0, -hit_distance, 1));
    arrow_aim_point = ctc.transform.get_position() + front_dir * t;
  } else {
    hit_point = closest_hit->hit_point;

    if (closest_hit->fraction > t / hit_distance) {
      arrow_aim_point = ctc.transform.get_position() + front_dir * t;
    } else {
      arrow_aim_point = hit_point;
    }

    hit_distance = closest_hit->fraction * hit_distance;
  }
  m_player_system->m_camera_hit_point = hit_point;
  m_player_system->m_arrow_aim_point = arrow_aim_point;

  // When aiming or drawing
  if (!m_player_system->state_exists(StateType::DASHING)) {
    if (m_player_system->m_face_aiming_dir_states > 0) {
      cc.contents.set_face_moving_direction(false);

      // Horizontal rotation
      cc.contents.set_facing_direction(
          glm::vec3(glm::toMat4(glm::rotation(
                        glm::vec3(0, 1, 0),
                        glm::normalize(m_player_system->m_arrow_aim_point - player_pos))) *
                    glm::vec4(0, 1, 0, 0)));

      // Vertical rotation
      const glm::vec3 dir =
          glm::normalize(m_player_system->m_arrow_aim_point - player_pos - offset);
      player.get<PlayerComponent>().aim_angle = asinf(dir.z);

    } else {
      cc.contents.set_face_moving_direction(true);
      player.get<PlayerComponent>().aim_angle = 0.f;
    }
  }

  // Draw
  const SlowMotionComponent& smc = player;
  m_player_system->m_shoot_cooldown -=
      fmaxf(smc.factor.get_converted_dt(context), context.unslowed_dt * 0.7f);

  // Stamina (no need to use SlowMotionComponent for dt)
  m_player_system->m_stamina_recharge_cooldown -= context.dt;
  if (m_player_system->m_stamina_recharge_cooldown < 0.f) {
    m_player_system->m_stamina_left += m_player_system->m_stamina_recharge_rate * context.dt;
    if (m_player_system->m_stamina_left > m_player_system->m_max_stamina)
      m_player_system->m_stamina_left = m_player_system->m_max_stamina;
  }

  // Saturation
  cor::Key saturation_key = cor::Key::create<SaturationComponent>();
  auto saturation_entity =
      context.entity_mgr.get_entity(context.entity_mgr.get_entities(saturation_key)[0]);
  SaturationComponent& satc = saturation_entity;
  satc.saturation = context.speed_factor * 0.6f + 0.4f;

  // Arrow selection
  if (keys_pressed.normal_arrow) {
    m_player_system->m_selected_arrow = PlayerSystem::ArrowType::PIERCING;
  } else if (keys_pressed.explosive_arrow) {
    m_player_system->m_selected_arrow = PlayerSystem::ArrowType::EXPLOSIVE;
  }

  // Rope arrow reach
  if (hit_distance <= m_player_system->m_max_reach && hit_point.z < 860.f &&
      !m_player_system->state_exists(StateType::DASHING)) {
    context.event_mgr.send_event(RopeReachEvent({true}));
  } else if ((hit_distance > m_player_system->m_max_reach || hit_point.z > 860.f ||
              m_player_system->state_exists(StateType::DASHING))) {
    context.event_mgr.send_event(RopeReachEvent({false}));
  }
  m_can_reach = (hit_distance <= m_player_system->m_max_reach);

  // Blur
  const float speed_factor = fminf(glm::length(cc.contents.get_velocity()) / 70.f, 1.f);
  if (!m_player_system->state_exists(StateType::DASHING)) {
    RadialComponent& rbc = player;
    rbc.blur_radius = speed_factor * speed_factor * 0.009f;
  }

  // Max height
  if (player_pos.z > 880.f)
    ptc.transform.set_position(glm::vec3(player_pos.x, player_pos.y, 880.f), context);

  // Attacking/drawing animation (after a rope arrow has been shot)
  if (m_player_system->m_time_left_of_attacking_anim > 0.f) {
    m_player_system->m_time_left_of_attacking_anim -= smc.factor.get_converted_dt(context);
    if (m_player_system->m_time_left_of_attacking_anim <= 0.f &&
        !m_player_system->state_exists(StateType::DRAWING)) {
      player.get<PlayerComponent>().is_attacking = false;
      m_player_system->m_face_aiming_dir_states--;
    }
  }

  // Checkpoint areas
  if (!m_player_system->m_in_boss_fight) {
    for (unsigned int i = 0; i < m_player_system->m_checkpoints.size(); ++i) {
      if (m_player_system->m_current_checkpoint_area != i &&
          glm::distance(m_player_system->m_checkpoints[i].position, ptc.transform.get_position()) <
              m_player_system->m_checkpoints[i].radius) {
        m_player_system->m_current_checkpoint_area = i;
        context.event_mgr.send_event(SaveCheckpoint());
        break;
      }
    }
  }

  // Up dir
  const glm::vec3 current_up = cc.contents.get_up();
  const glm::vec3 on_ground_up = (m_player_system->state_exists(StateType::IN_AIR) ||
                                  m_player_system->state_exists(StateType::SWINGING))
                                     ? glm::vec3(0)
                                     : glm::vec3(0, 0, 6);
  cc.contents.set_up(glm::normalize(current_up * 300.f * smc.factor.get_converted_dt(context) +
                                    speed_factor * m_player_system->m_desired_up + on_ground_up));
}

};  // namespace gmp