#include "gmp/player/player_slow_motion.hpp"

#include "gmp/systems/player_system.hpp"
#include <cor/frame_context.hpp>
namespace gmp {

PlayerSlowMotion::PlayerSlowMotion(PlayerSystem* player_system) : PlayerBaseState(player_system) {
  m_state_type = StateType::SLOW_MOTION;
}

void PlayerSlowMotion::enter(cor::FrameContext& context) {
  m_player_system->m_has_released_slow_motion = false;
  ChangeMasterPitchEvent cmpe;
  cmpe.new_pitch_val = 0.35f;
  context.event_mgr.send_event(cmpe);
}

void PlayerSlowMotion::update(cor::FrameContext& context, const PlayerEvents& keys_pressed) {
  //
  context.speed_factor *= 1 - m_player_system->m_slowing_down_speed * context.unslowed_dt;
  if (context.speed_factor < m_player_system->m_min_world_speed)
    context.speed_factor = m_player_system->m_min_world_speed;

  m_player_system->m_stamina_recharge_cooldown = m_player_system->m_stamina_max_cooldown;
  m_player_system->m_stamina_left -= context.unslowed_dt;

  if (!keys_pressed.slow_motion || m_player_system->m_stamina_left < 0.f) {
    m_player_system->add_action(ActionType::REMOVE);
    ChangeMasterPitchEvent cmpe;
    cmpe.new_pitch_val = 1.f;
    context.event_mgr.send_event(cmpe);
  }
}

};  // namespace gmp