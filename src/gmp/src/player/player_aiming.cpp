#include "gmp/player/player_aiming.hpp"

#include "cor/entity.tcc"                            // for Entity
#include "cor/entity_manager.tcc"                    // for EntityManager
#include "gmp/components/slow_motion_component.hpp"  // for SlowMotionComponent
#include "gmp/systems/player_system.hpp"             // for PlayerSystem
#include <cor/frame_context.hpp>                     // for FrameContext

namespace gmp {

PlayerAiming::PlayerAiming(PlayerSystem* player_system) : PlayerBaseState(player_system) {
  m_state_type = StateType::AIMING;
}

void PlayerAiming::enter(cor::FrameContext& context) {
  m_player_system->m_face_aiming_dir_states++;
}

void PlayerAiming::update(cor::FrameContext& context, const PlayerEvents& keys_pressed) {
  //
  auto player = context.entity_mgr.get_entity(m_player_system->m_player_handle);
  SlowMotionComponent& smc = player;

  m_player_system->m_camera_distance -= m_player_system->m_camera_zoom_speed * context.unslowed_dt;
  if (m_player_system->m_camera_distance < m_closest_camera_percent)
    m_player_system->m_camera_distance = m_closest_camera_percent;

  if (keys_pressed.aim_ended) {
    m_player_system->m_face_aiming_dir_states--;
    m_player_system->add_action(ActionType::REMOVE);
  }
}

};  // namespace gmp