#include "gmp/player/player_dashing.hpp"

#include "bse/directory_path.hpp"                             // for ASSETS_...
#include "bse/file_path.hpp"                                  // for operator/
#include "cor/entity_manager.tcc"                             // for EntityM...
#include "cor/event.hpp"                                      // for PlaySou...
#include "cor/event_manager.hpp"                              // for EventMa...
#include "cor/frame_context.hpp"                              // for FrameCo...
#include "cor/key.hpp"                                        // for Key
#include "gmp/components/character_controller_component.hpp"  // for Charact...
#include "gmp/components/particles_component.hpp"
#include "gmp/components/player_component.hpp"  // for SlowMot...
#include "gmp/components/radial_component.hpp"
#include "gmp/components/slow_motion_component.hpp"  // for SlowMot...
#include "gmp/components/transform_component.hpp"
#include "gmp/gmp_helper_functions.hpp"
#include "gmp/player/player_in_air.hpp"   // for PlayerI...
#include "gmp/player/player_running.hpp"  // for PlayerR...
#include "gmp/player/player_slow_motion.hpp"
#include "gmp/player/player_standing.hpp"  // for PlayerS...
#include "gmp/systems/player_system.hpp"   // for PlayerS...
#include <cor/entity.tcc>                  // for Entity

namespace gmp {
struct PlayerComponent;
}

namespace gmp {

PlayerDashing::PlayerDashing(PlayerSystem* player_system, const glm::vec3 direction)
    : PlayerBaseState(player_system), m_direction(direction), m_time_left(m_duration) {
  m_state_type = StateType::DASHING;
}

void PlayerDashing::enter(cor::FrameContext& context) {
  //
  auto player = context.entity_mgr.get_entity(m_player_system->m_player_handle);

  context.event_mgr.send_event(PlaySoundEvent(player.get_handle(),
                                              bse::ASSETS_ROOT / "audio/effects/dash/dash.ogg"_fp,
                                              aud::SoundSettings()));

  m_player_system->m_stamina_left -= m_player_system->m_stamina_dash_cost;
  m_player_system->m_stamina_recharge_cooldown = m_player_system->m_stamina_max_cooldown;

  m_delay_left = m_initial_delay;

  CharacterControllerComponent& cc = player;
  cc.contents.set_face_moving_direction(false);
  cc.contents.set_facing_direction(m_direction);

  m_player_system->m_dashes_left--;

  // Destroy rope arrow and rope
  if (m_player_system->m_rope_arrow_handle != cor::EntityHandle::NULL_HANDLE &&
      m_player_system->is_arrow_active(m_player_system->m_rope_arrow_handle))
    destroy_arrow(m_player_system->m_rope_arrow_handle, context);
  if (context.entity_mgr.is_valid(m_player_system->m_rope_handle))
    context.entity_mgr.destroy_entity(m_player_system->m_rope_handle);
}

void PlayerDashing::update(cor::FrameContext& context, const PlayerEvents& keys_pressed) {
  auto player = context.entity_mgr.get_entity(m_player_system->m_player_handle);
  CharacterControllerComponent& cc = player;
  const SlowMotionComponent& smc = player;

  if (m_delay_left > 0.f) {
    if (keys_pressed.moved) {
      const glm::mat4 rot_z =
          glm::rotate(glm::mat4(1), m_player_system->m_camera_rotation_x, glm::vec3(0, 0, 1));
      m_direction =
          glm::vec3(rot_z * glm::vec4(keys_pressed.move_right, keys_pressed.move_forward, 0, 0));
    }
    cc.contents.set_facing_direction(m_direction);
    cc.contents.set_gravity(glm::vec3(0.f));

    m_delay_left -= smc.factor.get_converted_dt(context);

    // Delay done
    if (m_delay_left <= 0.f) {
      // Particle burst
      TransformComponent& ptc = player;
      cor::Entity burst = context.entity_mgr.get_entity(
          create_particle_burst(ptc.transform.get_position(), context));
      ParticlesComponent& pc = burst;
      pc.spawn_per_second = 350;
      pc.spawn_duration_left = 0.05f;
      pc.min_start_color = glm::vec4(0.5, 0.15, 0, 1);
      pc.max_start_color = glm::vec4(0.6, 0.25, 0, 1);
      const float t = 0.5f;
      pc.min_end_color = glm::vec4(t, t, t, 1);
      pc.max_end_color = glm::vec4(t, t, t, 1);
      pc.min_start_velocity = glm::normalize(m_direction) * (m_speed) + glm::vec3(-10);
      pc.max_start_velocity = glm::normalize(m_direction) * (m_speed) + glm::vec3(10);
      pc.min_spawn_pos = glm::vec3(-0, -0, 0.9);
      pc.max_spawn_pos = glm::vec3(0, 0, 0.9);
      pc.min_start_size = 0.3f;
      pc.max_start_size = 0.4f;
      pc.min_end_size = 0.7f;
      pc.max_end_size = 0.8f;
      pc.min_life = 0.3f;
      pc.max_life = 0.5f;

      player.get<PlayerComponent>().is_dashing = true;
    }
  }
  // Actual dashing
  else {
    m_time_left -= smc.factor.get_converted_dt(context);
    cc.contents.set_face_moving_direction(true);
    cc.contents.dash(m_direction * m_speed, m_time_left);

    RadialComponent& rbc = player;
    rbc.blur_radius = 0.009f;

    // Time to leave dashing state
    if (m_time_left < 0.f) {
      auto player = context.entity_mgr.get_entity(m_player_system->m_player_handle);
      cc.contents.set_gravity(glm::vec3(0, 0, -80.f));

      // Falling
      if (!cc.contents.on_ground())
        m_player_system->add_action(
            ActionType::REPLACE,
            std::make_unique<PlayerInAir>(m_player_system, true, cc.contents.get_velocity()));
      // Running
      else if (keys_pressed.moved)
        m_player_system->add_action(ActionType::REPLACE,
                                    std::make_unique<PlayerRunning>(m_player_system));
      // Standing
      else
        m_player_system->add_action(ActionType::REPLACE,
                                    std::make_unique<PlayerStanding>(m_player_system));

      player.get<PlayerComponent>().is_dashing = false;
    }
  }

  // Stop swing from happening
  if (keys_pressed.rope_hit) {
    m_player_system->end_swing();
  }

  // Slow motion
  if (keys_pressed.slow_motion && !m_player_system->state_exists(StateType::SLOW_MOTION) &&
      m_player_system->m_stamina_left > 0.f && m_player_system->m_has_released_slow_motion)
    m_player_system->add_action(ActionType::PUSH,
                                std::make_unique<PlayerSlowMotion>(m_player_system));
}

};  // namespace gmp