#include "gmp/player/player_swinging.hpp"

#include "bse/directory_path.hpp"  // for ASSETS_...
#include "bse/file_path.hpp"       // for operator/
#include "cor/entity.tcc"          // for Entity
#include "cor/entity_manager.tcc"  // for EntityM...
#include "cor/event.hpp"           // for PlaySou...
#include "cor/event_manager.hpp"   // for EventMa...
#include "cor/frame_context.hpp"   // for FrameCo...
#include "cor/system_id_proxy.hpp"
#include "gmp/components/character_controller_component.hpp"  // for Charact...
#include "gmp/components/particles_component.hpp"
#include "gmp/components/player_component.hpp"
#include "gmp/components/slow_motion_component.hpp"  // for SlowMot...
#include "gmp/components/transform_component.hpp"    // for SlowMot...
#include "gmp/gmp_helper_functions.hpp"
#include "gmp/player/player_aiming.hpp"       // for PlayerA...
#include "gmp/player/player_drawing.hpp"      // for PlayerD...
#include "gmp/player/player_in_air.hpp"       // for PlayerI...
#include "gmp/player/player_running.hpp"      // for PlayerI...
#include "gmp/player/player_slow_motion.hpp"  // for PlayerI...
#include "gmp/systems/player_system.hpp"      // for PlayerS...
#include <cor/system.hpp>

namespace gmp {

PlayerSwinging::PlayerSwinging(PlayerSystem* player_system, cor::FrameContext& context)
    : PlayerBaseState(player_system) {
  m_history_index = 0;
  m_velocity_history.resize(2);
  m_state_type = StateType::SWINGING;
  // Crash into something
  // m_player_system->add_listener(context.event_mgr.register_handler<CollisionEvent>(
  //    [this, &context, player_system](const CollisionEvent* event) {
  //      //
  //      if ((event->entity_1 == player_system->m_player_handle ||
  //           event->entity_2 == player_system->m_player_handle) &&
  //          m_internal_state != InternalState::LEAVING) {
  //        player_system->end_swing(context);
  //        m_internal_state = InternalState::LEAVING;

  //        auto player = context.entity_mgr.get_entity(player_system->m_player_handle);
  //        CharacterControllerComponent& cc = player;
  //        cc.contents.set_move_vector(-cc.contents.get_move_vector() * 0.7f);

  //        // Play crash sound
  //        aud::SoundSettings settings;
  //        settings.gain = .6f;
  //        context.event_mgr.send_event(PlaySoundEvent(
  //            player_system->m_player_handle,
  //            bse::ASSETS_ROOT / "audio/effects/swinging/crash/post/hit_post.ogg"_fp, settings));
  //      }
  //    }));
}

void PlayerSwinging::enter(cor::FrameContext& context) {
  m_player_system->m_jumps_left = 1;
  m_player_system->m_dashes_left = 2;

  auto player = context.entity_mgr.get_entity(m_player_system->m_player_handle);
  CharacterControllerComponent& cc = player;

  auto arrow = context.entity_mgr.get_entity(m_player_system->m_rope_arrow_handle);

  const TransformComponent& ptc = player;
  m_start_dir_to_swing_center =
      glm::normalize(m_player_system->m_rope_arrow_hit_pos - ptc.transform.get_position());
  player.get<PlayerComponent>().is_swinging = true;
}

void PlayerSwinging::update(cor::FrameContext& context, const PlayerEvents& keys_pressed) {
  //
  auto player = context.entity_mgr.get_entity(m_player_system->m_player_handle);
  CharacterControllerComponent& cc = player;
  const SlowMotionComponent& smc = player;
  const TransformComponent& ptc = player;

  // Actually leaves swinging (needs 1 frame delay for swinging to disable properly)
  if (m_internal_state != InternalState::STAYING) {
    //
    cc.contents.jump(m_exit_speed_z);
    m_player_system->m_desired_up = glm::vec3(0, 0, 1);

    if (m_internal_state == InternalState::LANDING) {
      // Particles
      TransformComponent& ptc = player;
      cor::Entity burst = context.entity_mgr.get_entity(
          create_particle_burst(ptc.transform.get_position(), context));
      ParticlesComponent& pc = burst;
      const float factor = *std::max_element(m_velocity_history.begin(), m_velocity_history.end());
      pc.spawn_per_second = 40.f * factor * factor;
      const float grey_scale = 0.3f;
      pc.min_start_color = glm::vec4(grey_scale, grey_scale, grey_scale, 1);
      pc.max_start_color = glm::vec4(grey_scale, grey_scale, grey_scale, 1);
      pc.min_end_color = glm::vec4(grey_scale, grey_scale, grey_scale, 0.6);
      pc.max_end_color = glm::vec4(grey_scale, grey_scale, grey_scale, 0.6);
      const float spread = 7.f * factor;
      pc.min_start_velocity = glm::vec3(-spread, -spread, 0);
      pc.max_start_velocity = glm::vec3(spread, spread, spread * 0.15f);
      pc.min_spawn_pos = cc.contents.get_velocity() * 0.05f;
      pc.min_spawn_pos.z = -1;
      pc.max_spawn_pos = cc.contents.get_velocity() * 0.05f;
      pc.max_spawn_pos.z = -1;

      // Landing sound
      aud::SoundSettings settings;
      settings.gain = fminf(factor, 0.3f);
      context.event_mgr.send_event(
          PlaySoundEvent(m_player_system->m_player_handle,
                         bse::ASSETS_ROOT / "audio/effects/landing/post/thud.ogg"_fp, settings));

      m_player_system->add_action(ActionType::REPLACE,
                                  std::make_unique<PlayerRunning>(m_player_system));
    } else {
      // Using rand to determine if we should flip in the air
      int random_flip = std::rand() % 10;
      if (cc.contents.get_speed() >= 65.f && random_flip <= 5) {
        context.event_mgr.send_event(
            AnimationEvent("player_mesh_anm_flip", m_player_system->m_player_handle, 0.2f));
      }
      m_player_system->add_action(ActionType::REPLACE, std::make_unique<PlayerInAir>(
                                                           m_player_system, true, m_exit_speed_xy));
    }

    m_player_system->m_launch_help_active = false;
    m_player_system->m_launch_help_dir = glm::vec3(0, 0, 0);
  }
  // Do not exit swinging state this frame
  else {
    // Save velocity
    m_velocity_history[m_history_index] =
        fmaxf(fminf(glm::length(cc.contents.get_velocity()) / 70.f, 1.0f), 0.3f);

    m_history_index = (m_history_index + 1) % 2;

    // Hit ground
    if (cc.contents.on_ground()) {
      ++m_frames_on_ground;
      if (m_frames_on_ground >= 5) {
        m_internal_state = InternalState::LANDING;
        m_player_system->end_swing();
        return;
      }
    } else {
      m_frames_on_ground = 0;
    }

    const glm::vec3 player_to_swing_center =
        glm::normalize(cc.contents.get_swing_center() - ptc.transform.get_position());
    const float angle = sinf(glm::angle(player_to_swing_center, glm::vec3(0, 0, 1)));

    if (m_player_system->is_arrow_active(m_player_system->m_rope_arrow_handle)) {
      // Jump out
      if (keys_pressed.jumped) {
        //
        if (cc.contents.get_velocity().z > 0.f) {
          m_exit_speed_xy = cc.contents.get_velocity() * 1.06f;
          m_exit_speed_z += angle * 100.f;
        } else {
          m_exit_speed_xy = glm::vec2(cc.contents.get_velocity());
          m_exit_speed_z = cc.contents.get_velocity().z + 90.f;
        }

        // Play jump sound
        context.event_mgr.send_event(
            PlaySoundEvent(player.get_handle(), bse::ASSETS_ROOT / "audio/effects/dash/dash.ogg"_fp,
                           aud::SoundSettings()));

        LEAVE_SWING();
      }
      // Cancel swing
      else if (keys_pressed.action_canceled || keys_pressed.rope_action) {
        m_exit_speed_xy = glm::vec2(cc.contents.get_velocity());
        m_exit_speed_z = cc.contents.get_velocity().z;
        LEAVE_SWING();
      }
    } else {
      m_exit_speed_xy = glm::vec2(cc.contents.get_velocity());
      m_exit_speed_z = cc.contents.get_velocity().z;

      LEAVE_SWING();
    }

    const glm::mat4 rot_z =
        glm::rotate(glm::mat4(1), m_player_system->m_camera_rotation_x, glm::vec3(0, 0, 1));

    // Move
    const float side_speed = 30.f + angle * 55.f;
    const float tangental_speed = 30.f + angle * 55.f;
    const glm::mat4 rot_camera_tangent =
        glm::toMat4(glm::rotation(glm::vec3(0, 0, 1), player_to_swing_center));
    const glm::vec4 move_dir = glm::vec4(keys_pressed.move_right * side_speed,
                                         keys_pressed.move_forward * tangental_speed, 0, 0);
    const glm::vec3 dir = m_player_system->m_launch_help_dir +
                          glm::vec3(rot_camera_tangent * rot_z * move_dir) +
                          glm::vec3(rot_z * glm::vec4(-3, 0, 0, 0));
    cc.contents.add_swing_velocity(dir * smc.factor.get_converted_dt(context));

    // Rope length
    const float distance_to_swing_center =
        glm::length(ptc.transform.get_position() - cc.contents.get_swing_center());
    if (distance_to_swing_center < cc.contents.get_rope_length() - 1.f &&
        cc.contents.get_rope_length() > 5.f)
      cc.contents.set_rope_length(distance_to_swing_center);

    // Shorten rope on launch help
    if (m_player_system->m_launch_help_active &&
        cc.contents.get_rope_length() >
            m_player_system->m_original_distance_to_swing_center * 0.5f &&
        cc.contents.get_rope_length() > 15.f) {
      //
      cc.contents.set_rope_length(cc.contents.get_rope_length() -
                                  cc.contents.get_rope_length() * 0.8f *
                                      smc.factor.get_converted_dt(context));
    }
    // Cut the rope if it is too short
    // else if (cc.contents.get_rope_length() < 5.f) {
    //  // Rope break sound
    //  context.event_mgr.send_event(PlaySoundEvent(
    //      m_player_system->m_player_handle,
    //      bse::ASSETS_ROOT / "audio/effects/bow_and_arrow/rope_broke/post/rope_broke.ogg"_fp,
    //      aud::SoundSettings()));

    //  LEAVE_SWING();
    //}

    // Up dir
    cor::Entity arrow = context.entity_mgr.get_entity(m_player_system->m_rope_arrow_handle);
    const TransformComponent& atc = arrow;
    m_player_system->m_desired_up =
        glm::normalize(atc.transform.get_position() - ptc.transform.get_position());

    // Draw
    if (keys_pressed.draw_started)
      m_player_system->add_action(ActionType::PUSH,
                                  std::make_unique<PlayerDrawing>(m_player_system));

    // Aim
    if (keys_pressed.aim_started)
      m_player_system->add_action(ActionType::PUSH,
                                  std::make_unique<PlayerAiming>(m_player_system));

    // Slow motion
    if (keys_pressed.slow_motion && !m_player_system->state_exists(StateType::SLOW_MOTION) &&
        m_player_system->m_stamina_left > 0.f && m_player_system->m_has_released_slow_motion)
      m_player_system->add_action(ActionType::PUSH,
                                  std::make_unique<PlayerSlowMotion>(m_player_system));
  }
}

};  // namespace gmp