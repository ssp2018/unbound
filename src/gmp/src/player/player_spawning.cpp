#include "gmp/player/player_spawning.hpp"

#include "cor/entity_manager.tcc"
#include "gmp/components/character_controller_component.hpp"
#include "gmp/components/destroyed_component.hpp"
#include "gmp/components/physics_projectile_component.hpp"
#include "gmp/components/player_component.hpp"
#include "gmp/components/projection_component.hpp"
#include "gmp/components/transform_component.hpp"
#include "gmp/gmp_helper_functions.hpp"
#include "gmp/player/player_common_state.hpp"
#include "gmp/player/player_standing.hpp"  // for PlayerStanding
#include "gmp/player/player_standing.hpp"
#include "gmp/systems/player_system.hpp"  // for PlayerSystem
#include <cor/entity.tcc>
#include <cor/frame_context.hpp>  // for FrameContext
#include <gmp/components/homing_component.hpp>
#include <gmp/components/name_component.hpp>
#include <gmp/components/texture_component.hpp>
#include <gmp/components/transform_2d_component.hpp>

namespace gmp {

PlayerSpawning::PlayerSpawning(PlayerSystem* player_system) : PlayerBaseState(player_system) {
  m_state_type = StateType::SPAWNING;
}

void PlayerSpawning::enter(cor::FrameContext& context) {
  //
  // Only add black screen when not starting the game
  if (m_player_system->m_current_checkpoint_area != -1) {
    m_black_screen_handle = context.entity_mgr.create_entity();
    cor::Entity black_screen = context.entity_mgr.get_entity(m_black_screen_handle);

    Transform2dComponent t2dc;
    t2dc.size = glm::vec2(1.f, 1.f);
    t2dc.position = glm::vec3(0.5f, 0.5f, 0.f);

    TextureComponent texc;
    texc.texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/black_pixel.png"_fp);

    black_screen = t2dc;
    black_screen = texc;
  }

  auto player = context.entity_mgr.get_entity(m_player_system->m_player_handle);
  PlayerComponent& pc = player;
  pc.stamina = m_player_system->m_max_stamina;
  pc.charge_time = 0.f;

  aud::SoundSettings ambient_ss;
  ambient_ss.gain = 0.015;
  ambient_ss.loop = true;
  PlaySoundEvent snd_ev(m_player_system->m_player_handle,
                        bse::ASSETS_ROOT / "audio/effects/ambient/ambient_forest.ogg"_fp,
                        ambient_ss);

  context.event_mgr.send_event(snd_ev);

  // Override current character controller
  CharacterControllerComponent& pcc = player;  // Previous character controller
  m_player_offset_matrix = pcc.contents.get_offset_transform();
  player.detach<CharacterControllerComponent>();

  // Destroy rope
  if (m_player_system->m_rope_arrow_handle != cor::EntityHandle::NULL_HANDLE) {
    destroy_arrow(m_player_system->m_rope_arrow_handle, context);
  }
  if (context.entity_mgr.is_valid(m_player_system->m_rope_handle))
    context.entity_mgr.destroy_entity(m_player_system->m_rope_handle);

  // Destroy all skull shots
  auto& projectiles =
      context.entity_mgr
          .get_entities_with_components<PhysicsProjectileComponent, HomingComponent>();
  for (auto& p : projectiles) {
    cor::Entity projectile = context.entity_mgr.get_entity(p.handle);
    projectile = DestroyedComponent();
  }
}

void PlayerSpawning::update(cor::FrameContext& context, const PlayerEvents& keys_pressed) {
  //
  auto player = context.entity_mgr.get_entity(m_player_system->m_player_handle);
  auto camera = context.entity_mgr.get_entity(m_player_system->m_camera_handle);

  if (m_frames_in_state == 2) {
    CharacterControllerComponent cc;
    cc.contents.set_height(context, 0.25f);
    cc.contents.set_mass(context, 80.f);
    cc.contents.set_offset_transform(m_player_offset_matrix);
    cc.contents.set_radius(context, 0.6f);
    cc.contents.set_turning_speed(10.f);
    cc.contents.set_face_moving_direction(false);
    cc.contents.set_turning_speed(20.f);
    cc.contents.set_use_navmesh_collision(false);
    player = cc;

    // Move and rotate player
    TransformComponent& ptc = player;
    // Starting game
    if (m_player_system->m_current_checkpoint_area == -1) {
      m_rotation = glm::eulerAngles(ptc.transform.get_rotation()).z;
    }
    // Respawning
    else {
      m_rotation =
          m_player_system->m_checkpoints[m_player_system->m_current_checkpoint_area].spawn_rot;
      ptc.transform.set_position(
          m_player_system->m_checkpoints[m_player_system->m_current_checkpoint_area].spawn_pos,
          context);
    }
    m_player_system->m_camera_rotation_z = 0.f;
    m_player_system->m_camera_rotation_x = m_rotation;
  }
  if (m_frames_in_state >= 2) {
    CharacterControllerComponent& cc = player;
    cc.contents.set_up(glm::vec3(0, 0, 1));
    cc.contents.set_facing_direction(
        glm::vec3(glm::rotate(m_rotation, glm::vec3(0, 0, 1)) * glm::vec4(0, 1, 0, 0)));

    // ------ Camera stuff ------
    TransformComponent& ptc = player;

    const glm::mat4 rot_z =
        glm::rotate(glm::mat4(1), m_player_system->m_camera_rotation_x, glm::vec3(0, 0, 1));
    const glm::mat4 rot_x =
        glm::rotate(glm::mat4(1), m_player_system->m_camera_rotation_z, glm::vec3(1, 0, 0));
    const glm::mat4 rot = rot_z * rot_x;
    const float angle_z = abs(cosf(m_player_system->m_camera_rotation_z));
    const glm::vec3 player_pos = ptc.transform.get_position();
    const glm::vec3 front_dir = glm::vec3(rot * glm::vec4(0, 1, 0, 0));
    glm::vec3 cam_pos;
    const glm::vec3 offset = glm::vec3(rot_z * glm::vec4(0.25f, 0, 1.7f, 0));

    const glm::vec3 desired_pos = player_pos + offset + glm::vec3(rot * glm::vec4(1, -2.7, 0, 0));
    glm::vec3 diag_dir = glm::vec3(rot * glm::vec4(1, -2.7, 0, 0));
    const float diag_distance = glm::length(diag_dir);
    diag_dir = glm::normalize(diag_dir);
    const cor::SweepResult res =
        context.utilities.sphere_sweep(player_pos + offset, diag_dir, diag_distance, 0.15);
    if (res.hit)
      cam_pos = player_pos + offset + diag_dir * diag_distance * res.fraction;
    else
      cam_pos = desired_pos;

    if (camera.is_valid()) {
      TransformComponent& ctc = camera;

      ctc.transform.set_model_matrix(
          glm::inverse(glm::lookAt(cam_pos, cam_pos + front_dir, glm::vec3(0, 0, 1))), context);

      // FOV
      ProjectionComponent& pc = camera;
      pc.field_of_view = 35 + m_player_system->m_camera_distance * 35.f;
      pc.projection_matrix =
          glm::perspective(glm::radians(pc.field_of_view), pc.aspectRatio, pc.near, pc.far);
    }
    // ---------------

    // Done spawning
    if (cc.contents.on_ground() && m_frames_in_state >= 10 && m_time_in_state >= 0.8f) {
      if (context.entity_mgr.is_valid(m_black_screen_handle)) {
        context.entity_mgr.destroy_entity(m_black_screen_handle);
      }
      cc.contents.set_turning_speed(10.f);

      m_player_system->add_action(ActionType::REPLACE,
                                  std::make_unique<PlayerCommonState>(m_player_system));
      m_player_system->add_action(ActionType::PUSH,
                                  std::make_unique<PlayerStanding>(m_player_system));
    }
  }
  m_frames_in_state++;
  m_time_in_state += context.dt;
}

};  // namespace gmp