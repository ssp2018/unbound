#include "gmp/player/player_stunned.hpp"

#include "gmp/player/player_standing.hpp"  // for PlayerStanding
#include "gmp/systems/player_system.hpp"   // for PlayerSystem
#include <cor/frame_context.hpp>           // for FrameContext

namespace gmp {

PlayerStunned::PlayerStunned(PlayerSystem* player_system, float duration)
    : PlayerBaseState(player_system), m_duration_left(duration) {
  m_state_type = StateType::STUNNED;
}

void PlayerStunned::enter(cor::FrameContext& context) {}

void PlayerStunned::update(cor::FrameContext& context, const PlayerEvents& keys_pressed) {
  //
  m_duration_left -= context.dt;

  if (m_duration_left < 0.f) {
    m_player_system->add_action(ActionType::REPLACE,
                                std::make_unique<PlayerStanding>(m_player_system));
  }
}

};  // namespace gmp