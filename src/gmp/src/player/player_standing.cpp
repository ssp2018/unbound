#include "gmp/player/player_standing.hpp"

#include "cor/entity_manager.tcc"  // for EntityManager
#include "cor/frame_context.hpp"   // for FrameContext
#include "gmp/components/character_controller_component.hpp"
#include "gmp/components/transform_component.hpp"  // for Transform, Transfo...
#include "gmp/player/player_aiming.hpp"            // for PlayerAiming
#include "gmp/player/player_dashing.hpp"           // for PlayerDashing
#include "gmp/player/player_drawing.hpp"           // for PlayerDrawing
#include "gmp/player/player_in_air.hpp"            // for PlayerInAir
#include "gmp/player/player_running.hpp"           // for PlayerRunning
#include "gmp/player/player_slow_motion.hpp"       // for PlayerSlowMotion
#include "gmp/systems/player_system.hpp"           // for PlayerSystem
#include <cor/entity.tcc>                          // for Entity

namespace gmp {

PlayerStanding::PlayerStanding(PlayerSystem* player_system) : PlayerBaseState(player_system) {
  m_state_type = StateType::STANDING;
}

void PlayerStanding::enter(cor::FrameContext& context) {
  m_player_system->m_jumps_left = 2;
  m_player_system->m_dashes_left = 2;
}

void PlayerStanding::update(cor::FrameContext& context, const PlayerEvents& keys_pressed) {
  auto player = context.entity_mgr.get_entity(m_player_system->m_player_handle);
  CharacterControllerComponent& cc = player;

  // Jumped
  if (keys_pressed.jumped)
    m_player_system->add_action(ActionType::REPLACE,
                                std::make_unique<PlayerInAir>(m_player_system));
  // Dashed
  else if (keys_pressed.dashed && m_player_system->m_dashes_left >= 1 &&
           m_player_system->m_stamina_left > m_player_system->m_stamina_dash_cost) {
    m_player_system->m_jumps_left--;
    // Calculate direction
    auto player = context.entity_mgr.get_entity(m_player_system->m_player_handle);
    TransformComponent& tc = player;
    glm::vec3 direction = glm::vec3(tc.transform.get_model_matrix() * glm::vec4(0, -1, 0, 0));
    m_player_system->add_action(ActionType::REPLACE,
                                std::make_unique<PlayerDashing>(m_player_system, direction));
  }
  // Running
  else if (keys_pressed.moved) {
    m_player_system->base_move(keys_pressed.move_right * m_player_system->m_movement_speed,
                               keys_pressed.move_forward * m_player_system->m_movement_speed);
    m_player_system->add_action(ActionType::REPLACE,
                                std::make_unique<PlayerRunning>(m_player_system));
  }

  // Falling
  if (!cc.contents.on_ground()) {
    m_player_system->base_move(keys_pressed.move_right * m_player_system->m_movement_speed,
                               keys_pressed.move_forward * m_player_system->m_movement_speed);
    m_player_system->m_jumps_left = 1;
    m_player_system->add_action(ActionType::REPLACE,
                                std::make_unique<PlayerInAir>(m_player_system, true));
  }

  // Stop swing
  if (keys_pressed.rope_hit) {
    m_player_system->end_swing();
  }

  // Draw
  if (keys_pressed.draw_started)
    m_player_system->add_action(ActionType::PUSH, std::make_unique<PlayerDrawing>(m_player_system));

  // Rope arrow
  if (keys_pressed.rope_action) {
    // m_player_system->shoot_rope_arrow(context);
  }

  // Aim
  if (keys_pressed.aim_started)
    m_player_system->add_action(ActionType::PUSH, std::make_unique<PlayerAiming>(m_player_system));

  // Slow motion
  if (keys_pressed.slow_motion && !m_player_system->state_exists(StateType::SLOW_MOTION) &&
      m_player_system->m_stamina_left > 0.f && m_player_system->m_has_released_slow_motion)
    m_player_system->add_action(ActionType::PUSH,
                                std::make_unique<PlayerSlowMotion>(m_player_system));
}

};  // namespace gmp