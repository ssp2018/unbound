#include "gmp/unbound_editor_state.hpp"

#include "gmp/unbound_game_state.hpp"
#include "gmp/unbound_loading_state.hpp"
#include <bse/asset.hpp>
#include <bse/edit.hpp>
#include <bse/file.hpp>
#include <bse/mesh_builder.hpp>
#include <bse/utility.hpp>
#include <cmath>
#include <cor/entity.tcc>
#include <cor/entity_manager.tcc>
#include <cor/event_manager.hpp>
#include <cor/game.hpp>
#include <cor/unbound_type_explicit.hpp>
#include <gfx/model.hpp>
#include <gmp/components/attach_to_entity_component.hpp>
#include <gmp/components/cutscene_waypoint_component.hpp>
#include <gmp/components/debug_camera_component.hpp>
#include <gmp/components/directional_light_component.hpp>
#include <gmp/components/graphic_text_component.hpp>
#include <gmp/components/highlight_component.hpp>
#include <gmp/components/model_component.hpp>
#include <gmp/components/name_component.hpp>
#include <gmp/components/projection_component.hpp>
#include <gmp/components/transform_2d_component.hpp>
#include <gmp/components/transform_component.hpp>
#include <gmp/editor/scene.hpp>
#include <gmp/editor_components/editor_meta_component.hpp>
#include <gmp/editor_components/editor_selected_component.hpp>
#include <gmp/editor_components/preset_dummy_component.hpp>
#include <gmp/editor_components/preset_instance_component.hpp>
#include <gmp/external_types.hpp>
#include <gmp/systems/attach_update_system.hpp>
#include <gmp/systems/cloud_system.hpp>
#include <gmp/systems/debug_camera_system.hpp>
#include <gmp/systems/graphics_system.hpp>
#include <gmp/systems/particle_system.hpp>
#include <gmp/systems/physics_system.hpp>
#include <gmp/systems/sticky_triangle_system.hpp>
#include <scr/detail/utility.hpp>
#include <scr/dynamic_object.hpp>
#include <scr/dynamic_table.hpp>
#include <scr/scr.tcc>
#include <scr/script.hpp>
//
#include <gmp/components/character_controller_component.hpp>
#include <gmp/components/particles_component.hpp>
#include <gmp/components/player_component.hpp>
#include <gmp/components/player_damage_visual_component.hpp>
#include <gmp/components/radial_component.hpp>
#include <gmp/components/saturation_component.hpp>
#include <gmp/components/slow_motion_component.hpp>
#include <gmp/components/sticky_triangle_component.hpp>
UNBOUND_TYPE_EXPLICIT((bse::MeshBuilder), create_vertex, destroy_vertex, get_face, connect_vertices,
                      disconnect_vertices)

namespace gmp {

UnboundEditorState::UnboundEditorState(cor::Game& game, gfx::Window& window,
                                       aud::AudioManager& audio_mgr)
    : EditorState(game, window, audio_mgr) {
  //
  initialize();
}

UnboundEditorState::~UnboundEditorState() {
  //
}

scr::DynamicObject edit(std::string path, scr::DynamicObject object) {
  if (object.is_number()) {
    double old_val = object;
    double new_val = old_val;
    BSE_EDIT_NAMED(new_val, path.c_str());
    if (old_val != new_val) {
      return new_val;
    } else {
      return nullptr;
    }
  } else if (object.is_userdata()) {
    std::vector<float> floats = object;
    int flerp = 0;
    return nullptr;
  } else if (object.is_boolean()) {
    bool old_val = object;
    bool new_val = old_val;
    BSE_EDIT_NAMED(new_val, path.c_str());
    if (old_val != new_val) {
      return new_val;
    } else {
      return nullptr;
    }
  } else if (object.is_string()) {
    std::string old_val = object;
    std::string new_val = old_val;
    BSE_EDIT_NAMED(new_val, path.c_str());
    if (old_val != new_val) {
      return new_val;
    } else {
      return nullptr;
    }
  }
  return nullptr;
}

template <typename KeyT, typename ValueT>
std::vector<std::pair<const KeyT*, const ValueT*>> get_sorted_vector(
    const std::unordered_map<KeyT, ValueT>& map) {
  std::vector<std::pair<const KeyT*, const ValueT*>> vector;
  vector.reserve(map.size());
  for (const auto& [key, value] : map) {
    vector.emplace_back(&key, &value);
  }

  std::sort(vector.begin(), vector.end(),
            [](const auto& lhs, const auto& rhs) { return *lhs.first < *rhs.first; });
  return vector;
}

void UnboundEditorState::initialize() {
  // Expose script functions
  scr::expose_function("edit", edit);
  scr::expose_function("remove_entry", bse::Edit::remove_entry);
  scr::expose_function("load_model",
                       [](const std::string& path) { return bse::Asset<gfx::Model>(path); });
  scr::expose_function("load_scene", &Scene::load);
  scr::expose_function("save_scene", [](const sol::table& scene, const std::string& path) {
    Scene::save(scene, path);
  });

  bse::Asset<scr::Script> editor("assets/script/editor/editor.lua");
  m_context_creator = editor->return_value()["create_context"].get<decltype(m_context_creator)>();
  m_event_handler = editor->return_value()["handle_event"].get<decltype(m_event_handler)>();

  // Create editor script context
  std::vector<std::string_view> component_names = cor::ComponentName::get_registered_names();
  std::sort(component_names.begin(), component_names.end(),
            [](const auto& lhs, const auto& rhs) { return lhs < rhs; });
  m_context_table = m_context_creator(m_context.entity_mgr, m_context, component_names);

  std::string scene_path;
  {
    bse::Result<std::string> config_file = bse::load_file_str("config.cfg"_fp);
    if (config_file.is_valid()) {
      std::istringstream stream(*config_file);

      std::string line;
      while (std::getline(stream, line)) {
        static const std::string key = "scene: ";
        if (line.find(key) != line.npos && line.size() > key.size()) {
          scene_path = &line[key.size()];
        }
      }
    }
  }

  scene_path = bse::trim_whitespace(scene_path);

  if (scene_path != "") {
    m_context_table["path"] = scene_path;
  }

  if (bse::file_exists(m_context_table["path"].get<std::string>())) {
    // Load default scene at startup, for now
    ScriptEvent event = scr::detail::State::get().create_table_with();
    event["type"] = "load_scene";
    m_event_handler(m_context_table, event);
  }

  // Listeners
  m_lids.push_back(
      m_context.event_mgr.register_handler<EditorTransform>([this](const EditorTransform* event) {
        //
        switch (event->flags) {
          case EditorTransform::TRANSLATE:
            switch_to_transform_mode(EditorTransform::TRANSLATE);
            break;

          case EditorTransform::ROTATE:
            switch_to_transform_mode(EditorTransform::ROTATE);
            break;

          case EditorTransform::SCALE:
            switch_to_transform_mode(EditorTransform::SCALE);
            break;

          case EditorTransform::ATTACH:
            switch_to_transform_mode(EditorTransform::ATTACH);
            break;

          case EditorTransform::NONE:
            if (m_current_mode.mode != EditorTransform::NONE) {
              revert_transforms();
            }
            m_current_mode.mode = EditorTransform::NONE;
            m_current_mode.axis = Axis::NONE;
            break;

          default:
            ASSERT(false) << "Could not assign a mixed transform mode";
        }
      }));

  m_lids.push_back(m_context.event_mgr.register_handler<EditorTransformSetAxis>(
      [this](const EditorTransformSetAxis* event) {
        //
        if (m_current_mode.mode != EditorTransform::NONE) {
          if (m_current_mode.axis == event->axis) {
            m_current_mode.axis = Axis::NONE;
          } else {
            m_current_mode.axis = event->axis;
          }
        }
      }));

  m_lids.push_back(m_context.event_mgr.register_handler<EditorTransformAddAxis>(
      [this](const EditorTransformAddAxis* event) {
        //
        if (m_current_mode.mode != EditorTransform::NONE) {
          m_current_mode.axis = (Axis)((int)m_current_mode.axis ^ (int)event->axis);
        }
      }));

  m_lids.push_back(m_context.event_mgr.register_handler<EditorMouseMovementEvent>(
      [this](const EditorMouseMovementEvent* event) {
        //
        m_current_mouse_pos = glm::vec2((float)event->posx / m_window.get_width(),
                                        (float)event->posy / m_window.get_height());
        m_current_mouse_pos = glm::vec2(m_current_mouse_pos.x, 1.f - m_current_mouse_pos.y);
      }));

  m_lids.push_back(m_context.event_mgr.register_handler<EditorToggleGlobalTransform>(
      [this](const EditorToggleGlobalTransform* event) {
        if (m_current_mode.mode != EditorTransform::NONE) {
          m_current_mode.is_global_axis = !m_current_mode.is_global_axis;
        }
      }));

  m_lids.push_back(m_context.event_mgr.register_handler<EditorConfirmTransform>(
      [this](const EditorConfirmTransform* event) {
        if (m_current_mode.mode != EditorTransform::NONE) {
          // Causes the current mode to toggle off and apply the current settings
          switch_to_transform_mode(m_current_mode.mode);
        }
      }));

  m_lids.push_back(m_context.event_mgr.register_handler<EditorCancelTransform>(
      [this](const EditorCancelTransform* event) {
        if (m_current_mode.mode != EditorTransform::NONE) {
          m_current_mode.mode = EditorTransform::NONE;
          m_current_mode.axis = Axis::NONE;
          revert_transforms();
        }
      }));

  m_lids.push_back(
      m_context.event_mgr.register_handler<CloseApplication>([this](const CloseApplication* event) {
        //
        m_game.replace_state(
            std::make_unique<UnboundGameState>(m_game, m_window, m_context.audio_mgr));
      }));

  m_lids.push_back(
      m_context.event_mgr.register_handler<EditorPlayEvent>([this](const EditorPlayEvent* event) {
        //
        if (m_ignore_input) {
          m_ignore_input = false;
          return;
        }

        if (!dynamic_cast<const UnboundGameState*>(m_game.peek_above(this))) {
          m_ignore_input = true;
          m_game.push_state(std::make_unique<UnboundLoadingState>(
              m_game, m_window, m_context.audio_mgr, m_context_table["path"].get<std::string>()));
        }
      }));

  m_lids.push_back(
      m_context.event_mgr.register_handler<ScriptEvent>([this](const ScriptEvent* event) {
        //
        m_event_handler(m_context_table, *event);
      }));

  // mouse click listener
  m_lids.push_back(m_context.event_mgr.register_handler<StartLeftMousePress>(
      [this](const StartLeftMousePress* event) {
        if (m_current_mode.mode == EditorTransform::NONE) {
          m_context.event_mgr.send_event(BeginMousePicking{event->position});
        }
      }));

  m_lids.push_back(m_context.event_mgr.register_handler<EditorStartMultiselect>(
      [this](const EditorStartMultiselect* event) {
        //
        m_multiselect_active = true;
      }));

  m_lids.push_back(m_context.event_mgr.register_handler<EditorStopMultiselect>(
      [this](const EditorStopMultiselect* event) {
        //
        m_multiselect_active = false;
      }));

  m_lids.push_back(m_context.event_mgr.register_handler<EditorStartAttach>(
      [this](const EditorStartAttach* event) {
        //
        m_attach_active = true;
      }));

  m_lids.push_back(
      m_context.event_mgr.register_handler<EditorCtrlClick>([this](const EditorCtrlClick* event) {
        if (m_current_mode.mode == EditorTransform::ATTACH) {
          // actually attach the object.
          attach_entity();
          m_current_mode.mode = EditorTransform::NONE;
        } else {
          m_context.event_mgr.send_event(EditorTransform{EditorTransform::ATTACH});
        }
      }));

  m_lids.push_back(
      m_context.event_mgr.register_handler<MousePickData>([this](const MousePickData* event) {
        if (m_current_mode.mode == EditorTransform::NONE) {
          if (m_context.entity_mgr.is_valid(cor::EntityHandle(event->entity_handle))) {
            select(event->entity_handle);
          }
        }
      }));
  m_lids.push_back(m_context.event_mgr.register_handler<ToggleBseEditEvent>(
      [](const ToggleBseEditEvent* event) { bse::Edit::toggle_gui(); }));

  // Systems

  m_system_mgr.add_system<GraphicsSystem>(m_context, &m_window);
  m_system_mgr.add_system<DebugCameraSystem>(m_context);
  m_system_mgr.add_system<AttachUpdateSystem>();
  m_system_mgr.add_system<CloudSystem>(m_context);
  m_system_mgr.add_system<ParticleSystem>();
  m_system_mgr.add_system<PhysicsSystem>(m_context, 0.f);
  m_system_mgr.add_system<StickyTriangleSystem>();
}

bool UnboundEditorState::update(float dt) {
  //
  bse::Edit::enable();
  {
    // Make them refreshable. Hobo fix.
    bse::Asset<scr::Script> editor("assets/script/editor/editor.lua");
    /*m_context_creator = editor->return_value()["create_context"]
                            .get<sol::object>()
                            .as<decltype(m_context_creator)>();
    m_event_handler =
        editor->return_value()["handle_event"].get<sol::object>().as<decltype(m_event_handler)>();*/
    m_context_creator = editor->return_value()["create_context"].get<decltype(m_context_creator)>();
    m_event_handler = editor->return_value()["handle_event"].get<decltype(m_event_handler)>();
  }

  for (const std::string& path : bse::Edit::get_updated_paths()) {
    ScriptEvent event = scr::detail::State::get().create_table_with();
    event["type"] = "gui_edit";
    std::vector<std::string_view> exploded = bse::explode(path, '/');
    sol::table path_table = scr::detail::State::get().create_table_with();
    for (size_t i = 0; i < exploded.size(); i++) {
      path_table[i + 1] = exploded[i];
    }
    event["path"] = path_table;
    m_event_handler(m_context_table, event);
    LOG(NOTICE) << "bse edit: " << path;
  }

  EditorState::update(dt);

  update_transform_mode();

  std::string preset_name;
  BSE_EDIT_NAMED(preset_name, "editor/Preset name:");

  static bool previous_press = false;
  bool create_preset_press = false;
  BSE_EDIT_NAMED(create_preset_press, "editor/Create preset");
  if (create_preset_press && previous_press == false) {
    LOG(NOTICE) << "Pressed";
    create_preset(preset_name);
  }
  previous_press = create_preset_press;

  draw_waypoint_path();

  return false;
}

bool UnboundEditorState::display() {
  //
  EditorState::display();
  return false;
}

void UnboundEditorState::select(cor::EntityHandle handle) {
  cor::Entity entity = m_context.entity_mgr.get_entity(handle);
  if (entity.is_valid()) {
    if (PresetDummyComponent* pdc = entity) {
      handle = pdc->parent;
      entity = m_context.entity_mgr.get_entity(handle);
    }

    if (!m_multiselect_active) {
      deselect();
    }
    entity = HighlightComponent();
    entity = EditorSelectedComponent();

    if (PresetInstanceComponent* pic = entity) {
      for (auto& dummy_handle : pic->dummies) {
        cor::Entity dummy_entity = m_context.entity_mgr.get_entity(dummy_handle);
        dummy_entity = HighlightComponent();
      }
    }

    ScriptEvent event = scr::detail::State::get().create_table_with();
    event["type"] = "select";
    event["entity_id"] = entity.get_handle();
    m_event_handler(m_context_table, event);

    switch_to_transform_mode(m_current_mode.mode);
    switch_to_transform_mode(m_current_mode.mode);
  }
}

void UnboundEditorState::attach_entity() {
  glm::ivec2 mouse_pos = glm::ivec2(m_current_mouse_pos.x * m_window.get_width(),
                                    (-m_current_mouse_pos.y + 1) * m_window.get_height());
  MousePickData pick_data = m_context.utilities.get_picking_data(
      mouse_pos, glm::ivec2(m_window.get_width(), m_window.get_height()));
  if (m_context.entity_mgr.is_valid(pick_data.entity_handle)) {
    cor::Entity clicked_entity =
        m_context.entity_mgr.get_entity(cor::EntityHandle(pick_data.entity_handle));
    TransformComponent& clicked_tc = clicked_entity;

    // transform clicked worldpos to clicked_entity_model space
    glm::mat4 inv_model_matrix = glm::inverse(clicked_tc.transform.get_model_matrix());

    glm::vec4 model_space_pos =
        inv_model_matrix * glm::vec4(pick_data.world_position.x, pick_data.world_position.y,
                                     pick_data.world_position.z, 1);
    glm::vec3 normal = glm::normalize(pick_data.normal);
    glm::vec3 forward = glm::cross(normal, glm::vec3(1.0f, 0.0f, 0.0f));
    float dot = glm::dot(normal, glm::vec3(1.0f, 0.0f, 0.0f));
    if (glm::length(forward) < 1) {
      forward = glm::cross(normal, glm::vec3(0.0f, 1.0f, 0.0f));
    }

    glm::vec3 right = glm::cross(forward, normal);
    glm::vec3 up = normal;  // trans[1];
    forward = glm::normalize(forward);
    right = glm::normalize(right);
    up = glm::normalize(up);
    glm::mat4 rotation_matrix = glm::mat4(glm::vec4(right, 0), glm::vec4(forward, 0),
                                          glm::vec4(up, 0), glm::vec4(0, 0, 0, 1));

    cor::Entity selection = get_selected_entities()[0];
    // prevent self attachment (i.e. narcissism)
    if (selection.get_handle().full_handle == pick_data.entity_handle) {
      return;
    }

    // attach the selected entity to mousepicked location
    if (selection.is_valid()) {
      // If the clicked entity is already attached to the selected entity,
      // Then do NOTHING
      // (This avoids a cycle of attachments!)
      if (AttachToEntityComponent* clicked_atec = clicked_entity) {
        //
        if (clicked_atec->entity_handle == selection.get_handle()) {
          return;
        }
      }
      // }

      AttachToEntityComponent atec;

      TransformComponent& tc = selection;
      atec.offset_transform.set_model_matrix(glm::mat4(1.f), m_context);
      atec.offset_transform.set_position(glm::vec3(model_space_pos), m_context);
      atec.offset_transform.set_scale(tc.transform.get_scale() / clicked_tc.transform.get_scale(),
                                      m_context);

      atec.entity_handle = cor::EntityHandle(pick_data.entity_handle);
      selection = atec;

      // update transform
      tc.transform.set_model_matrix(
          clicked_tc.transform.get_model_matrix() * atec.offset_transform.get_model_matrix(),
          m_context);

      ScriptEvent event = scr::detail::State::get().create_table_with();
      event["type"] = "attach";
      event["target"] = atec.entity_handle;
      m_event_handler(m_context_table, event);
    } else {
      /*  auto attached_object_handle = m_context.entity_mgr.create_entity();
        auto entity = m_context.entity_mgr.get_entity(attached_object_handle);
        TransformComponent tc;
        tc.transform.set_model_matrix(
            clicked_tc.transform.get_model_matrix() *
                glm::translate(glm::mat4(1),
                               glm::vec3(model_space_pos.x, model_space_pos.y, model_space_pos.z))
        * rotation_matrix, m_context); entity = tc;

        AttachToEntityComponent atec;
        atec.offset_transform.set_model_matrix(
            glm::translate(glm::mat4(1),
                           glm::vec3(model_space_pos.x, model_space_pos.y, model_space_pos.z)) *
                rotation_matrix,
            m_context);
        atec.entity_handle = cor::EntityHandle(pick_data.entity_handle);
        entity = atec;
        entity = HighlightComponent();*/
    }
  } else {
    cor::Entity selection = get_selected_entities()[0];
    if (selection.is_valid()) {
      ScriptEvent event = scr::detail::State::get().create_table_with();
      event["type"] = "detach";
      m_event_handler(m_context_table, event);
    }
  }
}

void UnboundEditorState::deselect() {
  auto selected_entities = get_selected_entities();
  for (auto& entity : selected_entities) {
    if (entity.is_valid()) {
      if (!entity.has<StickyTriangleComponent>()) {
        entity.detach<HighlightComponent>();
      }
      entity.detach<EditorSelectedComponent>();

      ScriptEvent event = scr::detail::State::get().create_table_with();
      event["type"] = "deselect";
      event["entity_id"] = entity.get_handle();
      m_event_handler(m_context_table, event);
    }
  }

  // Just in case, clear all highlights
  auto highlighted_entities =
      m_context.entity_mgr.get_entities(cor::Key::create<HighlightComponent>());
  for (auto& highlight_handle : highlighted_entities) {
    cor::Entity highlight_entity = m_context.entity_mgr.get_entity(highlight_handle);
    if (!highlight_entity.has<StickyTriangleComponent>()) {
      highlight_entity.detach<HighlightComponent>();
    }
  }
}

std::vector<cor::Entity> UnboundEditorState::get_selected_entities() {
  auto handles = m_context.entity_mgr.get_entities(cor::Key::create<EditorSelectedComponent>());
  if (handles.size() == 0) {
    return {m_context.entity_mgr.get_entity(cor::EntityHandle::NULL_HANDLE)};
  }

  std::vector<cor::Entity> entities;
  entities.reserve(handles.size());
  for (auto handle : handles) {
    entities.push_back(m_context.entity_mgr.get_entity(handle));
  }
  return entities;
}

void UnboundEditorState::update_transform_mode() {
  switch (m_current_mode.mode) {
    case EditorTransform::TRANSLATE: {
      update_translation();
    } break;

    case EditorTransform::ROTATE: {
      update_rotation();
    } break;

    case EditorTransform::SCALE: {
      update_scale();
    } break;

    case EditorTransform::ATTACH: {
      update_attach();
    } break;
  }
}

void UnboundEditorState::update_translation() {
  //
  // Movement along all axes in 3D does not make sense (usage-wise)
  // So we revert to using no axes.
  if (m_current_mode.axis == (Axis::X | Axis::Y | Axis::Z)) {
    m_current_mode.axis = Axis::NONE;
  }

  auto selected_entities = get_selected_entities();
  if (selected_entities[0].is_valid()) {
    // TransformComponent& tc = selection;

    // Helper function
    auto get_plane_from_axes = [this](glm::vec3 origin, const Axis axes) -> Plane {
      Plane plane;
      plane.normal = glm::vec3((axes & Axis::X ? 0 : 1),  //
                               (axes & Axis::Y ? 0 : 1),  //
                               (axes & Axis::Z ? 0 : 1));
      plane.distance = glm::dot(origin, plane.normal);
      return plane;
    };

    // Helper function
    auto get_ray_from_axis = [this](glm::vec3 origin, const Axis axis) -> Ray {
      glm::vec3 axis_vector = glm::normalize(glm::vec3((axis & Axis::X ? 1 : 0),  //
                                                       (axis & Axis::Y ? 1 : 0),  //
                                                       (axis & Axis::Z ? 1 : 0)));

      return {origin, axis_vector};
    };

    glm::vec3 selection_world_position = m_current_mode.widget_transform.get_position();

    // Retrieve a plane or a ray depending on the number of restricted axes
    bool is_plane = true;
    Plane plane{};
    Ray ray{};
    if (m_current_mode.axis == Axis::NONE) {
      //
      plane.normal = glm::vec3(glm::inverse(get_view_matrix()) * glm::vec4(0.f, 0.f, 1.f, 0.f));
      plane.distance = glm::dot(selection_world_position, plane.normal);
    } else if (!(m_current_mode.axis == Axis::X ||  //
                 m_current_mode.axis == Axis::Y ||  //
                 m_current_mode.axis == Axis::Z)) {
      //
      plane = get_plane_from_axes(selection_world_position, m_current_mode.axis);
    } else {
      //
      is_plane = false;
      ray = get_ray_from_axis(selection_world_position, m_current_mode.axis);
    }

    glm::vec3 projected;

    // Calculate the projected position based on the given plane or ray
    if (is_plane) {
      draw_plane(plane, selection_world_position);
      projected = project_screen_position(
          m_current_mouse_pos + m_current_mode.mouse_offset_from_widget, plane);
    } else {
      draw_ray(ray);
      projected = project_screen_position(
          m_current_mouse_pos + m_current_mode.mouse_offset_from_widget, ray);
    }

    glm::mat4 model_before = m_current_mode.widget_transform.get_model_matrix();
    m_current_mode.widget_transform.set_position(projected, m_context);
    glm::mat4 model_after = m_current_mode.widget_transform.get_model_matrix();

    for (auto entity : selected_entities) {
      if (AttachToEntityComponent* selected_atec = entity) {
        selected_atec->offset_transform.set_model_matrix(
            model_after * glm::inverse(model_before) *
                selected_atec->offset_transform.get_model_matrix(),
            m_context);
      }
      TransformComponent& tc = entity;
      tc.transform.set_model_matrix(
          model_after * glm::inverse(model_before) * tc.transform.get_model_matrix(), m_context);
    }
  }
}

void UnboundEditorState::update_rotation() {
  //
  if (!(m_current_mode.axis == Axis::X ||  //
        m_current_mode.axis == Axis::Y ||  //
        m_current_mode.axis == Axis::Z) &&
      m_current_mode.axis != Axis::NONE) {
    //
    if (m_current_mode.axis == Axis::X | Axis::Y | Axis::Z) {
      m_current_mode.axis = Axis::NONE;
    } else {
      m_current_mode.axis = (Axis)((int)m_current_mode.axis ^ (int)m_current_mode.axis);
    }
  }

  auto selected_entities = get_selected_entities();
  if (selected_entities[0].is_valid()) {
    glm::vec2 selection_screen_pos =
        ndc_to_screen(world_to_ndc(m_current_mode.widget_transform.get_position()));
    glm::vec3 selection_world_pos = m_current_mode.widget_transform.get_position();

    // Screen space direction vectors used to derive the angle of the given vectors
    glm::vec2 normalized_entry =
        glm::normalize(m_current_mode.mouse_entry_pos - selection_screen_pos);
    glm::vec2 normalized_mouse = glm::normalize(m_current_mouse_pos - selection_screen_pos);

    float entry_angle = std::atan2(normalized_entry.y, normalized_entry.x);
    float delta_angle = std::atan2(normalized_mouse.y, normalized_mouse.x) - entry_angle;

    glm::vec3 axis_vector{};
    glm::vec3 view_direction =
        glm::normalize(glm::vec3(glm::inverse(get_view_matrix()) * glm::vec4(0.f, 0.f, 1.f, 0.f)));

    {
      // Draw helper lines showing where the mouse started vs. its current location.

      Plane world_screen_plane;
      world_screen_plane.normal = -view_direction;
      world_screen_plane.distance = glm::dot(selection_world_pos, world_screen_plane.normal);

      glm::vec3 ray_entry_direction = glm::normalize(
          project_screen_position(m_current_mode.mouse_entry_pos, world_screen_plane) -
          selection_world_pos);

      glm::vec3 ray_mouse_direction = glm::normalize(
          project_screen_position(m_current_mouse_pos, world_screen_plane) - selection_world_pos);

      draw_ray(Ray{selection_world_pos, ray_entry_direction}, {1.0, 1.0, 1.0});
      draw_ray(Ray{selection_world_pos, ray_mouse_direction}, {0, 0.5, 0.5});
    }

    // Represents the actual axis vector in world space (To be used in future calculations)
    glm::vec3 adjusted_axis_vector;

    if (m_current_mode.axis == Axis::NONE) {
      // Make the vector from the camera to the selected object the rotation axis

      cor::Entity camera = m_context.entity_mgr.get_entity(get_camera());
      axis_vector = glm::normalize(selection_world_pos -
                                   camera.get<TransformComponent>().transform.get_position());

      adjusted_axis_vector = axis_vector;
    } else {
      // Make one of the cardinal axes into the rotation axis

      axis_vector = glm::normalize(glm::vec3((m_current_mode.axis & Axis::X ? 1 : 0),  //
                                             (m_current_mode.axis & Axis::Y ? 1 : 0),  //
                                             (m_current_mode.axis & Axis::Z ? 1 : 0)));

      // Draw the ray differently depending on whether our transformation is local or global.
      if (m_current_mode.is_global_axis) {
        adjusted_axis_vector = axis_vector;
        draw_ray(Ray{selection_world_pos, axis_vector});
      } else {
        adjusted_axis_vector = glm::vec3(m_current_mode.widget_transform.get_model_matrix() *
                                         glm::vec4(axis_vector, 0.f));

        draw_ray(Ray{selection_world_pos, adjusted_axis_vector}, {0.9f, 0.3f, 0.9f});
      }
    }

    // Reverse the rotation axis depending on view direction to always make rotations clockwise in
    // relation to the user.
    if (glm::dot(view_direction, adjusted_axis_vector) < 0.f) {
      axis_vector *= -1.f;
    }

    glm::mat4 model_before;
    glm::mat4 model_after;

    // Order of operations matter depending on whether the transforms are global or local.
    // We also don't want to apply local transformations if the transform is along the view
    // direction
    if (m_current_mode.is_global_axis || m_current_mode.axis == Axis::NONE) {
      model_before = m_current_mode.widget_transform.get_model_matrix();

      m_current_mode.widget_transform.set_rotation_vec(axis_vector, delta_angle, m_context);
      m_current_mode.widget_transform.rotate(m_current_mode.last_widget_transform.get_rotation(),
                                             m_context);

      model_after = m_current_mode.widget_transform.get_model_matrix();

    } else {
      model_before = m_current_mode.widget_transform.get_model_matrix();

      m_current_mode.widget_transform.set_rotation(
          m_current_mode.last_widget_transform.get_rotation(), m_context);
      m_current_mode.widget_transform.rotate(axis_vector, delta_angle, m_context);

      model_after = m_current_mode.widget_transform.get_model_matrix();
    }

    for (auto entity : selected_entities) {
      TransformComponent& tc = entity;
      tc.transform.set_model_matrix(
          model_after * glm::inverse(model_before) * tc.transform.get_model_matrix(), m_context);
    }
  }

  //
}

void UnboundEditorState::update_scale() {
  //

  auto selected_entities = get_selected_entities();
  if (selected_entities[0].is_valid()) {
    glm::vec2 selection_screen_pos =
        ndc_to_screen(world_to_ndc(m_current_mode.widget_transform.get_position()));
    glm::vec3 selection_world_pos = m_current_mode.widget_transform.get_position();

    float entry_distance = m_current_mode.unit_length;
    float delta_distance = glm::length(m_current_mouse_pos - selection_screen_pos) / entry_distance;

    {
      // Draw helper lines showing where the mouse started vs. its current location.

      glm::vec3 view_direction = glm::normalize(
          glm::vec3(glm::inverse(get_view_matrix()) * glm::vec4(0.f, 0.f, 1.f, 0.f)));

      Plane world_screen_plane;
      world_screen_plane.normal = -view_direction;
      world_screen_plane.distance = glm::dot(selection_world_pos, world_screen_plane.normal);

      draw_plane(world_screen_plane, selection_world_pos);

      glm::vec3 current_world_mouse =
          project_screen_position(m_current_mouse_pos, world_screen_plane);

      glm::vec3 normalized_world_mouse = glm::normalize(current_world_mouse - selection_world_pos);

      glm::vec3 ortho_world_mouse =
          glm::normalize(glm::cross(view_direction, normalized_world_mouse));

      glm::vec3 entry_world_mouse =
          selection_world_pos + normalized_world_mouse *
                                    glm::length(current_world_mouse - selection_world_pos) /
                                    delta_distance;

      draw_ray(Ray{entry_world_mouse, ortho_world_mouse}, {1.0, 1.0, 1.0});
      draw_ray(Ray{current_world_mouse, ortho_world_mouse}, {0, 0.5, 0.5});
    }

    glm::mat4 model_before = m_current_mode.widget_transform.get_model_matrix();

    m_current_mode.widget_transform.set_scale(m_current_mode.last_widget_transform.get_scale(),
                                              m_context);
    m_current_mode.widget_transform.scale(delta_distance, m_context);

    glm::mat4 model_after = m_current_mode.widget_transform.get_model_matrix();

    for (auto entity : selected_entities) {
      TransformComponent& tc = entity;
      tc.transform.set_model_matrix(
          model_after * glm::inverse(model_before) * tc.transform.get_model_matrix(), m_context);
    }
  }
}

void UnboundEditorState::draw_waypoint_path() {
  //
  auto waypoints = m_context.entity_mgr.get_entities(
      cor::Key::create<TransformComponent, CutsceneWaypointComponent>());

  std::vector<std::pair<CutsceneWaypointComponent::Type, glm::vec3>> points;
  points.resize(waypoints.size());

  for (auto& wp_handle : waypoints) {
    cor::Entity waypoint = m_context.entity_mgr.get_entity(wp_handle);
    glm::vec3 position = waypoint.get<TransformComponent>().transform.get_position();

    points[waypoint.get<CutsceneWaypointComponent>().index] = {
        waypoint.get<CutsceneWaypointComponent>().type, position};
  }

  for (int i = 0; i < (int)points.size() - 1; i++) {
    auto& [type, pos] = points[0];
    glm::vec3 color = {0.3f, 0.f, 0.f};
    switch (type) {
      case CutsceneWaypointComponent::Type::CONTINUOUS:
        color = {0.3f, 0.f, 0.f};
        break;
      case CutsceneWaypointComponent::Type::INSTANT:
        color = {0.9f, 0.9f, 0.9f};
        break;
    }

    draw_line(points[i].second, points[i + 1].second, color);
  }
}

void UnboundEditorState::update_attach() {
  glm::ivec2 mouse_pos = glm::ivec2(m_current_mouse_pos.x * m_window.get_width(),
                                    (-m_current_mouse_pos.y + 1) * m_window.get_height());
  MousePickData pick_data;
  gfx::Context::run([this, &pick_data, &mouse_pos]() {
    pick_data = m_context.utilities.get_picking_data(
        mouse_pos, glm::ivec2(m_window.get_width(), m_window.get_height()));
  });
  if (m_context.entity_mgr.is_valid(pick_data.entity_handle)) {
    cor::Entity clicked_entity =
        m_context.entity_mgr.get_entity(cor::EntityHandle(pick_data.entity_handle));
    TransformComponent& clicked_tc = clicked_entity;

    // transform clicked worldpos to clicked_entity_model space
    glm::mat4 inv_model_matrix = glm::inverse(clicked_tc.transform.get_model_matrix());

    glm::vec4 model_space_pos =
        inv_model_matrix * glm::vec4(pick_data.world_position.x, pick_data.world_position.y,
                                     pick_data.world_position.z, 1);
    glm::vec3 normal = glm::normalize(pick_data.normal);
    glm::vec3 forward = glm::cross(normal, glm::vec3(1.0f, 0.0f, 0.0f));
    float dot = glm::dot(normal, glm::vec3(1.0f, 0.0f, 0.0f));
    if (glm::length(forward) < 1) {
      forward = glm::cross(normal, glm::vec3(0.0f, 1.0f, 0.0f));
    }

    glm::vec3 right = glm::cross(forward, normal);
    glm::vec3 up = normal;  // trans[1];
    forward = glm::normalize(forward);
    right = glm::normalize(right);
    up = glm::normalize(up);
    glm::mat4 rotation_matrix = glm::mat4(glm::vec4(right, 0), glm::vec4(forward, 0),
                                          glm::vec4(up, 0), glm::vec4(0, 0, 0, 1));

    cor::Entity selection = get_selected_entities()[0];
    // update the selected entity to mousepicked location
    if (selection.is_valid()) {
      // update transform
      TransformComponent& tc = selection;

      Transform offset_transform;
      offset_transform.set_model_matrix(glm::mat4(1.f), m_context);
      offset_transform.set_position(glm::vec3(model_space_pos), m_context);
      offset_transform.set_scale(tc.transform.get_scale() / clicked_tc.transform.get_scale(),
                                 m_context);
      tc.transform.set_model_matrix(
          clicked_tc.transform.get_model_matrix() * offset_transform.get_model_matrix(), m_context);
    }
  }
}

cor::Entity UnboundEditorState::create_entity(std::string name) {
  //

  bse::Asset<scr::Script> script("assets/script/editor/create_entity.lua");
  std::function<cor::Entity(const sol::table&, std::string)> script_create_entity =
      script->return_value()["create_entity"];

  cor::Entity entity = script_create_entity(m_context_table, name);
  return entity;
}

void UnboundEditorState::create_preset(std::string name) {
  //
  auto selected_entities = get_selected_entities();
  if (selected_entities[0].is_valid()) {
    //
    m_context_table["presets"][name] = scr::detail::State::get().create_table_with();
    auto preset_desc = m_context_table["presets"][name];

    glm::vec3 average_position(0.f);
    for (auto entity : selected_entities) {
      if (TransformComponent* tc = entity) {
        average_position += tc->transform.get_position();
      } else {
        LOG(WARNING) << "Cannot create a preset with this entity! (It did not have a transform "
                        "component)";
      }
    }
    average_position /= selected_entities.size();
    glm::mat4 preset_space;
    {
      Transform temp_transform;
      temp_transform.set_position(average_position, m_context);
      preset_space = glm::inverse(temp_transform.get_model_matrix());
    }

    for (auto entity : selected_entities) {
      if (EditorMetaComponent* emc = entity) {
        TransformComponent& tc = entity;
        preset_desc[emc->name] = m_context_table["entities"][emc->name];
        preset_desc[emc->name]["relative_transform"] =
            preset_space * tc.transform.get_model_matrix();

      } else {
        LOG(WARNING)
            << "Cannot create a preset with this entity! (It did not have a meta component)";
      }
    }

    LOG(NOTICE) << scr::detail::to_string(sol::object(m_context_table["presets"]));
  }
}

void UnboundEditorState::switch_to_transform_mode(EditorTransform::Flags target_mode) {
  //
  bool was_entity_transformed = false;
  if (m_current_mode.mode == target_mode) {
    m_current_mode.mode = EditorTransform::NONE;
    m_current_mode.axis = Axis::NONE;

    auto selected_entities = get_selected_entities();
    for (auto& entity : selected_entities) {
      if (entity.is_valid()) {
        TransformComponent& tc = entity;
        auto it = m_current_mode.last_transform.find(entity.get_handle());
        if (it == m_current_mode.last_transform.end()) {
          m_current_mode.last_transform[entity.get_handle()] = tc.transform;
        } else {
          glm::mat4 before = it->second.get_model_matrix();
          glm::mat4 after = tc.transform.get_model_matrix();
          if (memcmp(&before, &after, sizeof(glm::mat4)) != 0) {
            was_entity_transformed = true;
            it->second = tc.transform;
          }
        }
      }
    }
  } else {
    m_current_mode.axis = Axis::NONE;

    auto selected_entities = get_selected_entities();

    if (selected_entities[0].is_valid()) {
      m_current_mode.mode = target_mode;
      m_current_mode.mouse_entry_pos = m_current_mouse_pos;
      m_current_mode.widget_transform = Transform();

      glm::vec3 average_position = glm::vec3(0, 0, 0);

      for (auto& entity : selected_entities) {
        if (entity.is_valid()) {
          TransformComponent& tc = entity;
          average_position += tc.transform.get_position();
          auto it = m_current_mode.last_transform.find(entity.get_handle());
          if (it == m_current_mode.last_transform.end()) {
            m_current_mode.last_transform[entity.get_handle()] = tc.transform;
          } else {
            glm::mat4 before = it->second.get_model_matrix();
            glm::mat4 after = tc.transform.get_model_matrix();
            if (memcmp(&before, &after, sizeof(glm::mat4)) != 0) {
              // was_entity_transformed = true;
              it->second = tc.transform;
            }
          }
        }
      }

      average_position /= selected_entities.size();

      m_current_mode.mouse_offset_from_widget =
          ndc_to_screen(world_to_ndc(average_position)) - m_current_mode.mouse_entry_pos;

      m_current_mode.widget_transform.set_position(average_position, m_context);

      // If we only selected one entity, then we want to keep the orientation.
      if (selected_entities.size() == 1) {
        TransformComponent& tc = selected_entities[0];
        m_current_mode.widget_transform.set_rotation(tc.transform.get_rotation(), m_context);
      }

      m_current_mode.last_widget_transform = m_current_mode.widget_transform;
      m_current_mode.unit_length =
          glm::length(m_current_mouse_pos - ndc_to_screen(world_to_ndc(average_position)));
    }
    // was_entity_transformed = was_entity_transformed && target_mode ==
    // EditorTransform::Flags::NONE;
  }
  if (was_entity_transformed) {
    ScriptEvent event = scr::detail::State::get().create_table_with();
    event["type"] = "transform";
    m_event_handler(m_context_table, event);
  }
}

void UnboundEditorState::revert_transforms() {
  //
  auto selected_entities = get_selected_entities();
  for (auto& entity : selected_entities) {
    if (entity.is_valid()) {
      TransformComponent& tc = entity;
      auto last_it = m_current_mode.last_transform.find(entity.get_handle());
      if (last_it != m_current_mode.last_transform.end()) {
        tc.transform = last_it->second;
        tc.transform.force_dirty(m_context);
      }
    }
  }
}

cor::EntityHandle UnboundEditorState::get_camera() const {
  cor::Key key = cor::Key::create<ProjectionComponent>();
  std::vector<cor::EntityHandle> cameras = m_context.entity_mgr.get_entities(key);
  if (cameras.empty()) {
    return cor::EntityHandle::NULL_HANDLE;
  } else {
    return cameras[0];
  }
}

glm::vec2 UnboundEditorState::get_screen_position(cor::EntityHandle handle) {
  //
  cor::Entity entity = m_context.entity_mgr.get_entity(handle);
  cor::Entity camera = m_context.entity_mgr.get_entity(get_camera());

  //
  glm::vec4 entity_3d_position =
      glm::vec4(entity.get<TransformComponent>().transform.get_position(), 1.f);  //

  glm::vec4 clip_coordinates = get_view_projection() * entity_3d_position;

  glm::vec2 screen_coordinates = glm::vec2(clip_coordinates / clip_coordinates.w);
  screen_coordinates = (screen_coordinates)*0.5f + glm::vec2(0.5f, 0.5f);

  return screen_coordinates;
}

glm::vec3 UnboundEditorState::project_screen_position(glm::vec2 screen_position,
                                                      Plane world_plane) {
  //
  const glm::mat4 VIEW_MATRIX = get_view_matrix();

  glm::vec3 ray_ndc_origin = glm::vec3((screen_position - glm::vec2(0.5f, 0.5f)) / 0.5f, 0.f);
  glm::vec3 ray_view_origin =
      glm::vec3(VIEW_MATRIX * glm::vec4(ndc_to_world(ray_ndc_origin), 1.0f));

  Ray view_ray{ray_view_origin, glm::normalize(ray_view_origin)};

  glm::vec3 view_plane_origin =
      VIEW_MATRIX * glm::vec4(world_plane.normal * world_plane.distance, 1.0f);

  Plane view_plane;
  view_plane.normal = glm::vec3(VIEW_MATRIX * glm::vec4(world_plane.normal, 0.f));
  view_plane.distance = -glm::dot(view_plane_origin, view_plane.normal);

  glm::vec3 world_intersection =
      glm::inverse(VIEW_MATRIX) * glm::vec4(ray_plane_intersection(view_ray, view_plane), 1.0f);

  return world_intersection;
}

glm::vec3 UnboundEditorState::project_screen_position(glm::vec2 screen_position, Ray world_ray) {
  //
  const glm::mat4 VIEW_MATRIX = get_view_matrix();

  // The ray to project onto
  Ray proj_view_ray{glm::vec3(VIEW_MATRIX * glm::vec4(world_ray.origin, 1.f)),
                    glm::vec3(VIEW_MATRIX * glm::vec4(world_ray.direction, 0.f))};

  // The orthogonal vector from the ray to the camera
  // Formula:
  //    Find u
  //    Ray origin is p
  //    Ray direction is v
  //    u dot v = 0
  //    u = p + t * v
  //
  //    u dot v = p dot v + t * v dot v
  //    0 = p dot v + t|v|^2
  //
  //    Which gives u = p + v(-p dot v)
  //    if |v| == 1

  glm::vec3 orto_vec =
      proj_view_ray.origin +
      proj_view_ray.direction * -glm::dot(proj_view_ray.origin, proj_view_ray.direction);

  Plane view_plane;
  view_plane.normal = -glm::normalize(orto_vec);
  view_plane.distance = glm::dot(proj_view_ray.origin, view_plane.normal);

  Plane world_plane;
  world_plane.normal = glm::vec3(glm::inverse(VIEW_MATRIX) * glm::vec4(view_plane.normal, 0.f));
  world_plane.distance =
      glm::dot(glm::vec3(glm::inverse(VIEW_MATRIX) *
                         glm::vec4(view_plane.normal * view_plane.distance, 1.f)),
               world_plane.normal);

  glm::vec3 plane_coordinates = project_screen_position(screen_position, world_plane);

  draw_plane(world_plane, world_ray.origin);

  glm::vec3 ray_coordinates =
      world_ray.origin +
      world_ray.direction * glm::dot(plane_coordinates - world_ray.origin, world_ray.direction);

  return ray_coordinates;
}

glm::mat4 UnboundEditorState::get_view_projection() const {
  //
  cor::ConstEntity camera = m_context.entity_mgr.get_entity(get_camera());

  glm::mat4 view_transform = get_view_matrix();                                          //
  glm::mat4 projection_transform = camera.get<ProjectionComponent>().projection_matrix;  //

  return projection_transform * view_transform;
}

glm::mat4 UnboundEditorState::get_view_matrix() const {
  //
  cor::ConstEntity camera = m_context.entity_mgr.get_entity(get_camera());

  glm::mat4 view_transform = glm::inverse(                           //
      camera.get<TransformComponent>().transform.get_model_matrix()  //
  );                                                                 //

  return view_transform;
}

glm::vec3 UnboundEditorState::ray_plane_intersection(Ray ray, Plane plane) {
  //
  // Formula:
  // Ray    is P = P0 + tV
  // If P is in the plane (N,d), then P dot N + d = 0
  //
  // Solution:
  // t = -(P0 dot N + d) / (V dot N)
  // P = P0 + tV
  //

  float t = -(glm::dot(ray.origin, plane.normal) + plane.distance) /
            glm::dot(ray.direction, plane.normal);
  return ray.origin + t * ray.direction;
}

glm::vec3 UnboundEditorState::world_to_ndc(glm::vec3 world) {
  //
  glm::vec4 clip = get_view_projection() * glm::vec4(world, 1.f);
  glm::vec3 ndc = glm::vec3(clip / clip.w);
  return ndc;
}

glm::vec3 UnboundEditorState::ndc_to_world(glm::vec3 ndc) {
  //
  glm::vec4 clip = inverse(get_view_projection()) * glm::vec4(ndc, 1.f);
  glm::vec3 world = glm::vec3(clip / clip.w);
  return world;
}

glm::vec2 UnboundEditorState::ndc_to_screen(glm::vec3 ndc) {
  return glm::vec2(ndc.x / 2 + 0.5f, ndc.y / 2 + 0.5f);
}

void UnboundEditorState::draw_plane(Plane world_plane, glm::vec3 origin) {
  //
  glm::vec3 right;
  glm::vec3 forward;

  if (world_plane.normal != glm::vec3(0, 0, 1)) {
    right = glm::cross(world_plane.normal, glm::vec3(0, 0, 1));
    forward = glm::cross(world_plane.normal, right);
  } else {
    right = glm::cross(world_plane.normal, glm::vec3(1, 0, 0));
    forward = glm::cross(world_plane.normal, right);
  }

  DebugDrawLineEvent ev;
  ev.from = origin + right * -32000.f;
  ev.to = origin + right * 32000.f;
  ev.color = glm::vec3(0, 0, 0);
  //
  m_context.event_mgr.send_event(ev);

  ev.from = origin + forward * -32000.f;
  ev.to = origin + forward * 32000.f;
  ev.color = glm::vec3(0, 0, 0);
  //
  m_context.event_mgr.send_event(ev);
}

void UnboundEditorState::draw_ray(Ray world_ray, glm::vec3 color) {
  //
  DebugDrawLineEvent ev;
  ev.from = world_ray.origin + world_ray.direction * -32000.f;
  ev.to = world_ray.origin + world_ray.direction * 32000.f;
  ev.color = color;
  //
  m_context.event_mgr.send_event(ev);
}

void UnboundEditorState::draw_line(glm::vec3 start, glm::vec3 end, glm::vec3 color) {
  DebugDrawLineEvent ev;
  ev.from = start;
  ev.to = end;
  ev.color = color;

  m_context.event_mgr.send_event(ev);
}

}  // namespace gmp
