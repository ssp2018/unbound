#include "gmp/unbound_pause_state.hpp"

#include <bse/asset.hpp>
#include <cor/entity.tcc>
#include <cor/entity_manager.tcc>
#include <cor/event_manager.hpp>
#include <cor/game.hpp>
#include <gfx/model.hpp>
#include <gmp/components/graphic_text_component.hpp>
#include <gmp/components/model_component.hpp>
#include <gmp/components/projection_component.hpp>
#include <gmp/components/transform_2d_component.hpp>
#include <gmp/components/transform_component.hpp>
#include <gmp/systems/graphics_system.hpp>

namespace gmp {

UnboundPauseState::UnboundPauseState(cor::Game& game, gfx::Window& window,
                                     aud::AudioManager& audio_mgr)
    : State(game, window, audio_mgr) {
  //
  initialize();
}

UnboundPauseState::~UnboundPauseState() {
  //
}

void UnboundPauseState::initialize() {
  //
  m_unpause_listener =
      m_context.event_mgr.register_handler<JumpEvent>([this](const JumpEvent* event) {
        //
        m_game.pop_state();
      });

  m_system_mgr.add_system<GraphicsSystem>(m_context, &m_window);

  auto handle = m_context.entity_mgr.create_entity();
  {
    auto entity = m_context.entity_mgr.get_entity(handle);
    TransformComponent tc;
    tc.transform.set_position(glm::vec3(0, 0, 0), m_context);
    entity = tc;

    ModelComponent mc;
    mc.asset = bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/axis.fbx"_fp);
    entity = mc;
  }

  auto handle2 = m_context.entity_mgr.create_entity();
  {
    auto entity = m_context.entity_mgr.get_entity(handle2);
    TransformComponent tc;
    tc.transform.set_model_matrix(
        glm::inverse(glm::lookAt(glm::vec3(8, 8, 8), glm::vec3(0, 0, 0), glm::vec3(0, 0, 1))),
        m_context);
    entity = tc;

    ProjectionComponent pc;
    pc.aspectRatio = (float)1280 / (float)720;
    pc.field_of_view = 90.f;
    pc.near = 0.1f;
    pc.far = 1000.0f;
    pc.projection_matrix =
        glm::perspective(glm::radians(pc.field_of_view), pc.aspectRatio, pc.near, pc.far);
    entity = pc;
  }

  auto handle3 = m_context.entity_mgr.create_entity();
  {
    auto entity = m_context.entity_mgr.get_entity(handle3);
    TransformComponent tc;
    tc.transform.set_position(glm::vec3(0, 0, 0), m_context);
    entity = tc;

    // Transform2dComponent t2dc;
    // t2dc.position = {0.5, 0.5};
    // t2dc.size = {100, 100};
    // entity = t2dc;

    GraphicTextComponent gtc;
    gtc.font = "arial";
    gtc.text = "Paused";
    gtc.color = {255, 255, 255};
    entity = gtc;
  }
}

bool UnboundPauseState::update(float dt) {
  //
  m_input_mgr.poll_input();
  State::update(dt);
  return false;
}

bool UnboundPauseState::display() {
  //
  State::display();
  return false;
}

}  // namespace gmp
