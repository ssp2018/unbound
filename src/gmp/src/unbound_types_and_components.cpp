#define UNBOUND_DO_EXPOSE_TYPES
// Do NOT include anything above this line

#include "gmp/components/highlight_component.hpp"
#include <cor/cor.hpp>
#include <cor/entity_handle.hpp>
#include <cor/entity_manager.hpp>
#include <ext/ext.hpp>
#include <gmp/components/attach_to_entity_component.hpp>
#include <gmp/components/attach_to_triangle_component.hpp>
#include <gmp/components/emitter_death_countdown_component.hpp>
#include <gmp/components/explode_component.hpp>
#include <gmp/components/knock_back_component.hpp>
#include <gmp/components/knocked_back_stun_component.hpp>
#include <gmp/unbound_types_and_components.hpp>
#include <scr/scr.tcc>

glm::mat4 create_mat4() { return glm::mat4(1.f); }
glm::mat4 translate(const glm::mat4& mat, const glm::vec3& vec) { return glm::translate(mat, vec); }
glm::mat4 scale(const glm::mat4& mat, const glm::vec3& vec) { return glm::scale(mat, vec); }
float angle_vec4(glm::vec4 a, glm::vec4 b) { return glm::angle(a, b); }
float angle_vec3(glm::vec3 a, glm::vec3 b) { return glm::angle(a, b); }
glm::vec2 vec2_fill(const float x, const float y) { return glm::vec2(x, y); }
glm::vec3 vec3_fill(const float x, const float y, const float z) {
  return glm::vec3(x, y, z);
}  // this mother fucker
glm::vec4 vec4_fill(const float x, const float y, const float z, const float w) {
  return glm::vec4(x, y, z, w);
}
glm::mat4 rotate(const glm::mat4& mat, const glm::vec3& axis, const float angle) {
  return glm::rotate(angle, axis) * mat;
}

UNBOUND_TYPE((cor::EntityManager))
UNBOUND_TYPE((cor::EntityHandle))

namespace gmp {
void initialize_unbound_types_and_components() {
  scr::expose_function("normalize_vec3", glm::normalize<3, float, glm::qualifier::highp>);
  scr::expose_function("normalize_vec4", glm::normalize<4, float, glm::qualifier::highp>);
  scr::expose_function("normalize_vec2", glm::normalize<2, float, glm::qualifier::highp>);
  scr::expose_function("distance_vec3", glm::distance<3, float, glm::qualifier::highp>);
  scr::expose_function("create_mat4", create_mat4);
  scr::expose_function("mat4_translate", translate);
  scr::expose_function("mat4_scale", scale);
  scr::expose_function("mat4_inverse", [](const glm::mat4& mat) { return glm::inverse(mat); });
  scr::expose_function("mat4_multiply",
                       [](const glm::mat4& lhs, const glm::mat4& rhs) { return lhs * rhs; });
  scr::expose_function("mat4_vec4_mul",
                       [](const glm::mat4& lhs, const glm::vec4& rhs) { return lhs * rhs; });
  scr::expose_function("angle_vec4", angle_vec4);
  scr::expose_function("angle_vec3", angle_vec3);
  scr::expose_function("vec2_fill", vec2_fill);
  scr::expose_function("vec3_fill", vec3_fill);
  scr::expose_function("vec4_fill", vec4_fill);
  scr::expose_function("get_mat4_rot", rotate);
  scr::expose_function("perspective", glm::perspective<float>);
  scr::expose_function("radians", glm::radians<float>);
  scr::expose_function("vec3_add", [](glm::vec3& target, const glm::vec3& source) {
    target += source;
    return target;
  });
  scr::expose_function("vec3_subtract", [](glm::vec3& target, const glm::vec3& source) {
    target -= source;
    return target;
  });
  scr::expose_function("vec3_divide", [](glm::vec3& target, float divisor) {
    target /= divisor;
    return target;
  });
  scr::expose_function("vec3_scalar_multiplication", [](glm::vec3& target, float scalar) {
    target *= scalar;
    return target;
  });
  scr::expose_function("vec4_add", [](glm::vec4& target, const glm::vec4& source) {
    target += source;
    return target;
  });
  scr::expose_function("vec4_subtract", [](glm::vec4& target, const glm::vec4& source) {
    target -= source;
    return target;
  });
  scr::expose_function("cross",
                       [](glm::vec3& v1, const glm::vec3& v2) { return glm::cross(v1, v2); });
  scr::expose_function("rotate_vec_to_vec",
                       [](glm::vec3 from, glm::vec3 to) { return glm::rotation(from, to); });
  scr::expose_function("quat_slerp", [](glm::quat q1, glm::quat q2, float percentage) {
    return glm::slerp(q1, q2, percentage);
  });
  scr::expose_function("normalize_quat", [](glm::quat q) { return glm::normalize(q); });
  scr::expose_function("dot", [](glm::vec3 a, glm::vec3 b) { return glm::dot(a, b); });
  scr::expose_function("mul_quats", [](glm::quat a, glm::quat b) { return a * b; });
  initialize_unbound_types_and_components1();
  initialize_unbound_types_and_components2();
  initialize_unbound_types_and_components3();
  initialize_unbound_types_and_components4();
  initialize_unbound_types_and_components5();
  initialize_unbound_types_and_components6();
  initialize_unbound_types_and_components7();
  initialize_unbound_types_and_components8();
  initialize_unbound_types_and_components9();
}
}  // namespace gmp