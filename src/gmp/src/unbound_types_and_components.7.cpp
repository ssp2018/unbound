#define UNBOUND_DO_EXPOSE_TYPES
// Do NOT include anything above this line

#include <cor/cor.hpp>
#include <gmp/components/cutscene_waypoint_component.hpp>
#include <gmp/components/pickup_component.hpp>
#include <gmp/components/projection_component.hpp>
#include <gmp/components/rotate_z_component.hpp>
#include <gmp/components/slow_motion_component.hpp>
#include <gmp/components/sticky_triangle_component.hpp>
#include <phy/collision_shapes.hpp>
#include <scr/scr.tcc>

std::shared_ptr<phy::CollisionShape> make_shared_CCS(float x, float y) {
  return std::make_shared<phy::CapsuleCollisionShape>(x, y);
}

namespace gmp {
void initialize_unbound_types_and_components7() {
  scr::expose_type<phy::CapsuleCollisionShape>(
      "CapsuleCollisionShape",
      scr::Constructors<phy::CapsuleCollisionShape(), phy::CapsuleCollisionShape(float, float)>());
  scr::expose_function("make_shared_CCS", make_shared_CCS);
}
}  // namespace gmp