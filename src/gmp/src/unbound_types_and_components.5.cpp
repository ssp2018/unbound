#define UNBOUND_DO_EXPOSE_TYPES
// Do NOT include anything above this line

#include <cor/cor.hpp>
#include <gmp/components/apply_damage_over_time_component.hpp>
#include <gmp/components/attach_to_joint_component.hpp>
#include <gmp/components/attached_handles_component.hpp>
#include <gmp/components/audio_component.hpp>
#include <gmp/components/damage_component.hpp>
#include <gmp/components/spawn_component.hpp>
#include <gmp/editor_components/editor_meta_component.hpp>
#include <gmp/editor_components/editor_selected_component.hpp>
#include <gmp/editor_components/preset_dummy_component.hpp>
#include <gmp/editor_components/preset_instance_component.hpp>
#include <scr/detail/utility.hpp>
#include <scr/scr.tcc>

void write_audio_class(sol::table& desc, gmp::AudioComponent& audio_component);

void create_audio_class(sol::table desc, gmp::AudioComponent& audio_component) {
  //

  std::unordered_map<std::string, gmp::LoopingSoundSettings> periodic_sound_groups;

  sol::table root = desc["audio_desc"];
  if (root != sol::nil) {
    scr::detail::for_each(root, [&](const sol::object& group_key, const sol::object& group_obj) {
      //
      sol::table group = group_obj;

      ASSERT(group["name"] != sol::nil) << "What in tarnation???";
      ASSERT(sol::object(group["name"]) != sol::nil)
          << "Aww naruuuu, mahyte! That gits me right in the peckah!";
      ASSERT(group["settings"] != sol::nil) << "U WOT mATE!???";

      std::string name = group["name"];
      sol::table settings = group["settings"];

      periodic_sound_groups[name] = scr::from_table_sol<gmp::LoopingSoundSettings>(settings);
    });

    audio_component.audio.set_periodic_sound_groups(periodic_sound_groups);
  }
}

template <typename T>
sol::table helper_to_table(T& item) {
  sol::table new_table = scr::detail::State::get().create_table_with();
  bse::for_each_field(item, [&](auto& field) {
    using field_type = std::remove_reference_t<decltype(field)>;
    if constexpr (bse::meta<field_type>::is_defined) {
      new_table[field.name] = helper_to_table(field.value);  //
    } else {
      new_table[field.name] = field.value;  //
    }
  });
  return new_table;
}

void write_audio_class(sol::table& desc, gmp::AudioComponent& audio_component) {
  //

  std::unordered_map<std::string, gmp::LoopingSoundSettings> periodic_sound_groups =
      audio_component.audio.get_periodic_sound_groups();

  sol::table audio_desc = scr::detail::State::get().create_table_with();
  for (auto& [name, settings] : periodic_sound_groups) {
    sol::table group = scr::detail::State::get().create_table_with();
    group["name"] = name;
    group["settings"] = helper_to_table(settings);
    audio_desc[audio_desc.size() + 1] = group;
  }
  desc["audio_desc"] = audio_desc;
}

namespace gmp {
void initialize_unbound_types_and_components5() {
  //
  scr::expose_function("create_audio_class", create_audio_class);
  scr::expose_function("write_audio_class", write_audio_class);
}
}  // namespace gmp