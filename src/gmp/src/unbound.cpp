#include "gmp/unbound.hpp"

#include <bse/asset.hpp>
#include <bse/edit.hpp>
#include <bse/file.hpp>
#include <bse/utility.hpp>
#include <cor/cor.hpp>
#include <gmp/death_callbacks.hpp>
#include <gmp/external_types.hpp>
#include <gmp/systems/graphics_system.hpp>
#include <gmp/unbound_editor_state.hpp>
#include <gmp/unbound_game_state.hpp>
#include <gmp/unbound_loading_state.hpp>
#include <gmp/unbound_start_state.hpp>
#include <phy/collision_mesh.hpp>
#include <string>
namespace gfx {
class Window;
}

namespace gmp {

Unbound::Unbound(gfx::Window& window, unsigned int max_frame_count)
    : Game(window, max_frame_count) {
  bse::Edit::new_frame();

  initialize_unbound_types_and_components();

  initialize();

  // cor::FrameContext context(m_audio_mgr);

  auto config_file = bse::load_file_str("config.cfg"_fp);
  if (!config_file.is_valid()) {
    push_state(std::make_unique<UnboundStartState>(*this, window, m_audio_mgr));

  } else {
    std::istringstream stream(*config_file);

    std::string state;
    std::string line;
    while (std::getline(stream, line)) {
      static const std::string key = "run: ";
      if (line.find(key) != line.npos && line.size() > key.size()) {
        state = &line[key.size()];
      }
    }

    state = bse::trim_whitespace(state);

    // I expect that whoever needs a more detailed parser will make it :)
    if (state == "Editor") {
      push_state(std::make_unique<UnboundEditorState>(*this, window, m_audio_mgr));
    } else if (state == "Start") {
      push_state(std::make_unique<UnboundStartState>(*this, window, m_audio_mgr));
    } else if (state == "Game") {
      push_state(std::make_unique<UnboundLoadingState>(
          *this, window, m_audio_mgr, "assets/script/scene/default_editor_scene.lua"));
    } else {
      push_state(std::make_unique<UnboundStartState>(*this, window, m_audio_mgr));
    }
  }

  /* No more handle arrow hits */
  bse::Edit::end_frame();
}

Unbound::~Unbound() {
  //
}

void Unbound::initialize() {
  // expose cpp death_callback functions
  expose_death_callbacks();
  m_audio_mgr.init_device();

  bse::Asset<gfx::Shader>(bse::ASSETS_ROOT / "shader/shader.frag"_fp).pre_load();
  // bse::Asset<gfx::Shader>(bse::ASSETS_ROOT / "shader/shader.frag"_fp);
}
}  // namespace gmp
