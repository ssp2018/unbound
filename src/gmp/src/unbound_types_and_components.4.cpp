#define UNBOUND_DO_EXPOSE_TYPES
// Do NOT include anything above this line

#include <cor/cor.hpp>
#include <cor/frame_context.hpp>
#include <gmp/components/ai_component.hpp>
#include <gmp/components/aim_offset_component.hpp>
#include <gmp/components/animation_component.hpp>
#include <gmp/components/character_controller_component.hpp>
#include <gmp/components/dynamic_component.hpp>
#include <gmp/components/joint_look_at_component.hpp>
#include <gmp/components/player_component.hpp>
#include <gmp/components/static_component.hpp>
#include <gmp/components/steering_component.hpp>
#include <gmp/components/take_damage_over_time_component.hpp>
#include <gmp/components/transform_component.hpp>
#include <gmp/static_component_contents.hpp>
#include <scr/scr.tcc>
UNBOUND_TYPE((gmp::ScriptEntity), get_char_ctrl_component, get_ai_component, get_player_component)

gmp::StaticComponentContents load_static_component_contents(cor::FrameContext& context,
                                                            const std::string& type,
                                                            const std::string& path) {
  gmp::StaticComponentContents contents;
  if (type == "Triangle" && !path.empty()) {
    contents.set_collission_shape(context, std::make_shared<phy::TriangleMeshCollisionShape>(
                                               bse::Asset<phy::CollisionMesh>(path)));
  } else if (type == "Box") {
    contents.set_collission_shape(context,
                                  std::make_shared<phy::BoxCollisionShape>(glm::vec3(100, 100, 1)));
  } else if (type == "Convex") {
    contents.set_collission_shape(context, std::make_shared<phy::ConvexHullCollisionShape>(
                                               bse::Asset<phy::CollisionMesh>(path)));
  }
  return contents;
}
sol::table write_static_component_contents(const gmp::StaticComponentContents& contents) {
  sol::table table = scr::detail::State::get().create_table_with();
  if (contents.get_collision_shape() != nullptr) {
    switch (contents.get_collision_shape()->get_type()) {
      case phy::CollisionShapeType::TRIANGLE_MESH: {
        table["type"] = "Triangle";
        const phy::TriangleMeshCollisionShape* triangle_mesh =
            static_cast<const phy::TriangleMeshCollisionShape*>(contents.get_collision_shape());
        table["path"] = triangle_mesh->get_mesh_asset().get_path().get_string();

      } break;
      case phy::CollisionShapeType::CONVEX_HULL: {
        table["type"] = "Convex";
        const phy::ConvexHullCollisionShape* triangle_mesh =
            static_cast<const phy::ConvexHullCollisionShape*>(contents.get_collision_shape());
        table["path"] = triangle_mesh->get_mesh_asset().get_path().get_string();

      } break;
      case phy::CollisionShapeType::BOX: {
        table["type"] = "Box";
      } break;
    }
  } else {
    table["type"] = "UNDEFINED";
    table["path"] = "";
  }
  return table;
}

namespace gmp {
void initialize_unbound_types_and_components4() {
  scr::expose_function("load_static_component_contents", load_static_component_contents);
  scr::expose_function("write_static_component_contents", write_static_component_contents);

  scr::expose_function("set_goal_vec", gmp::set_goal_vec);
  scr::expose_function("set_goal_entity", gmp::set_goal_entity);

  scr::expose_function("set_transform_rotation",
                       (void (gmp::Transform::*)(glm::quat, const cor::FrameContext&)) &
                           gmp::Transform::set_rotation);
}
}  // namespace gmp