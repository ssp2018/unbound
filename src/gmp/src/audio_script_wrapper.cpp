#include <gmp/audio_script_wrapper.hpp>

namespace gmp {
AudioScriptWrapper::AudioScriptWrapper() {}

AudioScriptWrapper::~AudioScriptWrapper() {}

void AudioScriptWrapper::attach_audio_component(cor::Entity& entity) {
  if (check_audio_component(entity)) {
    LOG(WARNING) << "Entity already has a audio component attached";
  } else {
    entity.attach<AudioComponent>();
  }
}

void AudioScriptWrapper::remove_audio_component(cor::Entity& entity) {
  if (check_audio_component(entity)) {
    entity.detach<AudioComponent>();
  }
}

void AudioScriptWrapper::set_collision_sound(std::string asset_path, cor::Entity& entity, int type,
                                             aud::SoundSettings settings) {
  if (check_audio_component(entity)) {
    bse::FilePath file_path = bse::FilePath(bse::ASSETS_ROOT / bse::FilePath(asset_path.c_str()));
    AudioComponent& ac = entity;
    ac.audio.set_collision_sound(file_path,
                                 {std::make_pair(MaterialType(type), std::move(settings))});
  }
}

void AudioScriptWrapper::set_intervall_time_limit(cor::Entity& entity, std::string group,
                                                  float val) {
  if (check_audio_component(entity)) {
    AudioComponent& ac = entity;
    ac.audio.set_intervall_time_limit(std::move(group), val);
  }
}

void AudioScriptWrapper::set_looping_sound_group_enabled(cor::Entity& entity, std::string group,
                                                         bool val) {
  if (check_audio_component(entity)) {
    AudioComponent& ac = entity;
    ac.audio.set_looping_sound_group_enabled(group, val);
  }
}

void AudioScriptWrapper::add_periodic_sound_group(cor::Entity& entity, std::string group) {
  if (check_audio_component(entity)) {
    AudioComponent& ac = entity;
    ac.audio.add_periodic_sound_group(group);
  }
}

// std::string AudioScriptWrapper::get_random_sound_from_group(std::string group) {
//  return AudioSystem::get_random_sound_from_group(group);
//}

// void AudioScriptWrapper::play_delayed_sound_from_group(cor::EntityHandle handle, std::string
// group,
//                                                       std::string name, float delay_time) {
//  SoundToBePlayed* sound = AudioSystem::get_sound_from_group(group, name);
//  AudioSystem::play_sound_delayed(DelayedSoundInfo(handle, *sound, delay_time));
//}

bool AudioScriptWrapper::check_audio_component(cor::Entity& entity) {
  if (entity.has(cor::Key().create<AudioComponent>())) {
    return true;
  } else {
    return false;
    LOG(WARNING) << "Entity does not have a audio component attached";
  }
}
}  // namespace gmp