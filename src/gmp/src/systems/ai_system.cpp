#include "ai/ai_base.hpp"                                      // for AIBase
#include "bse/asset.hpp"                                       // for Asset
#include "bse/directory_path.hpp"                              // for ASSETS...
#include "bse/file_path.hpp"                                   // for operator/
#include "cor/entity.tcc"                                      // for Entity
#include "cor/entity_handle.hpp"                               // for Entity...
#include "cor/entity_manager.tcc"                              // for Entity...
#include "cor/frame_context.hpp"                               // for FrameC...
#include "cor/key.hpp"                                         // for Key
#include "gmp/ai_script_entity_wrapper.hpp"                    // for AIScri...
#include "gmp/components/ai_force_state_change_component.hpp"  // for AIForc...
#include "scr/dynamic_table.hpp"                               // for Dynami...
#include <aud/sound_buffer.hpp>
#include <bse/edit.hpp>
#include <cor/event.hpp>          // for StunEvent
#include <cor/event_manager.hpp>  // for EventM...
#include <gfx/model.hpp>
#include <gfx/texture.hpp>
#include <gmp/components/ai_component.hpp>                    // for AIComp...
#include <gmp/components/animation_component.hpp>             // for Animat...
#include <gmp/components/character_controller_component.hpp>  // for Charac...
#include <gmp/components/cole_component.hpp>
#include <gmp/components/health_component.hpp>
#include <gmp/components/player_component.hpp>
#include <gmp/components/shield_effect_component.hpp>
#include <gmp/components/static_component.hpp>
#include <gmp/components/steering_component.hpp>
#include <gmp/components/threat_component.hpp>     // for Threat...
#include <gmp/components/transform_component.hpp>  // for Transform
#include <gmp/systems/ai_system.hpp>               // for AISystem
#include <scr/dynamic_object.hpp>
#include <scr/object.hpp>  // for Object
#include <scr/script.hpp>  // for Script

// debug
#include <gmp/components/name_component.hpp>
#define EPSILON 1e-3

gmp::AISystem::AISystem(cor::FrameContext& context) {
  initialize_tables();
  expose_types();
  event_subscription(context);
  m_ai_base.initialize();

  for (int i = 0; i < 10; ++i) m_frame_time_share_queue.push_back(0.1);
  m_read_time = 0;
  m_write_time = 0;
  m_frame_timer.start();
  m_curr_run_chance = 100;
  m_print_time = 0;

  // preloading models
  bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/boulder-boss.fbx"_fp).pre_load();
  bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/bump.fbx"_fp).pre_load();
  bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/hook.fbx"_fp).pre_load();
  bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/gravel_mesh_2.fbx"_fp).pre_load();
  bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/rope.fbx"_fp).pre_load();

  // preloading textures
  bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/rocks/rock_01.png"_fp).pre_load();
  bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/dirt_particles/2_cropped.png"_fp).pre_load();

  // preloading scripts
  bse::Asset<scr::Script>(bse::ASSETS_ROOT /
                          "script/ai/boss_golem/boss_head_weakspot_callback.lua"_fp)
      .pre_load();

  // preloading sound buffers
  bse::Asset<aud::SoundBuffer>(bse::ASSETS_ROOT /
                               "audio/effects/boss1/rock_collission_sound.ogg"_fp)
      .pre_load();
  bse::Asset<aud::SoundBuffer>(bse::ASSETS_ROOT / "audio/effects/boss1/rock_throwing_sound.ogg"_fp)
      .pre_load();
  bse::Asset<aud::SoundBuffer>(bse::ASSETS_ROOT /
                               "audio/effects/boss1/chain_throw_hit_sound1.ogg"_fp)
      .pre_load();
  bse::Asset<aud::SoundBuffer>(bse::ASSETS_ROOT / "audio/effects/boss1/chain_throw_sound1.ogg"_fp)
      .pre_load();
  bse::Asset<aud::SoundBuffer>(bse::ASSETS_ROOT / "audio/effects/boss1/chain_throw_sound2.ogg"_fp)
      .pre_load();
  bse::Asset<aud::SoundBuffer>(bse::ASSETS_ROOT / "audio/effects/boss1/ground_slam.ogg"_fp)
      .pre_load();
  bse::Asset<aud::SoundBuffer>(bse::ASSETS_ROOT / "audio/effects/boss1/gravel_combined.ogg"_fp)
      .pre_load();
  bse::Asset<aud::SoundBuffer>(bse::ASSETS_ROOT / "audio/effects/boss1/sweep.ogg"_fp).pre_load();
}

void gmp::AISystem::initialize_tables() {
  m_function_table_script = bse::Asset<scr::Script>("assets/script/ai/function_accessor.lua");
  m_function_table = m_function_table_script->return_value();
}

void gmp::AISystem::expose_types() {
  // Exposing a non-component object
}

void gmp::AISystem::event_subscription(cor::FrameContext& context) {
  add_listener(
      EVENT_REGISTER_MEMBER_FUNCTION_I(StunEvent, context.event_mgr, handle_stun_start_event));
}

void gmp::AISystem::read_update(const cor::FrameContext& context) {
  bse::Timer read_timer;
  read_timer.start();

  m_frame_time = m_frame_timer.stop().value_or(0);
  m_print_time += m_frame_time;
  m_frame_timer.start();

  m_frame_time_share_queue.pop_front();
  m_frame_time_share_queue.push_back((m_read_time + m_write_time) / m_frame_time);

  m_frame_time_share = 0;
  for (int i = 0; i < 10; ++i) m_frame_time_share += m_frame_time_share_queue[i];
  m_frame_time_share /= 10.f;

  // change chance of running AI depending on AI system utilization
  // float change = m_target_utilization - m_frame_time_share;
  float change = 1;
  m_curr_run_chance += (change * m_correction_speed);
  m_curr_run_chance = glm::min(100.f, glm::max(5.f, m_curr_run_chance));

  cor::EntityHandle player_handle;
  cor::Key p_key = cor::Key::create<PlayerComponent, TransformComponent>();
  auto p_entities = context.entity_mgr.get_entities(p_key);
  for (auto& entity_handle : p_entities) player_handle = entity_handle;

  if (!context.entity_mgr.is_valid(player_handle)) {
    return;
  }

  cor::ConstEntity player_entity = context.entity_mgr.get_entity(player_handle);
  const TransformComponent& player_tc = player_entity;

  int ai_exec = 0;
  cor::Key key = cor::Key::create<AIComponent, TransformComponent>();
  auto entities = context.entity_mgr.get_entities(key);
  for (auto& entity_handle : entities) {
    cor::ConstEntity ai_entity = context.entity_mgr.get_entity(entity_handle);
    const TransformComponent& ai_tc = ai_entity;
    const AIComponent& ai_ai = ai_entity;

    float dist = glm::distance(ai_tc.transform.get_position(), player_tc.transform.get_position());

    if (ai_entity.has<gmp::NameComponent>()) {
      const NameComponent& name = ai_entity;
      if (name.name == "COLE") {
        goto run_ai;
      }
    }

    float hard_cutoff = (ai_ai.cutoff_dist == -1) ? m_hard_cutoff_distance : ai_ai.cutoff_dist;

    // determine who gets a lua update this frame
    if (dist > hard_cutoff) {
      if (!ai_ai.hard_cut) m_dist_cutoff_ai.insert({entity_handle, ' '});
    } else {
      bool skip_rand = false;
      if (dist < m_live_update_distance) {
        m_active_ai.insert({entity_handle, ' '});
        skip_rand = true;
      }
      int random = rand() % 101;
      float nominator = dist - m_live_update_distance;
      float denominator = (hard_cutoff - m_live_update_distance);
      float chance = m_curr_run_chance - (nominator / denominator) * 20;
      if (skip_rand || chance > random) {
      run_ai:
        ai_exec++;
        m_active_ai.insert({entity_handle, ' '});
        cor::ConstEntity entity = context.entity_mgr.get_entity(entity_handle);
        AIScriptEntityWrapper ai_script_entity(&entity, context);
        const AIComponent& AIc = entity;

        //---------TRANSITIONS---------//
        // call lua function function_table_script_transition
        std::function<std::string(std::string, std::string, std::string, AIScriptEntityWrapper&)>
            function_table_script_transition = m_function_table["access_function_table"];
        std::string transition_str = function_table_script_transition(
            AIc.type, "transition", AIc.state_name, ai_script_entity);

        // Add stuff to be done in next write update
        if (transition_str != "") m_transitions.insert({entity_handle, transition_str});
        for (int i = 0; i < ai_script_entity.m_threat_event_list.size(); ++i) {
          m_threat.insert({entity_handle, ai_script_entity.m_threat_event_list.at(i)});
        }
        for (int i = 0; i < ai_script_entity.m_search_location_list.size(); ++i) {
          m_search_location.insert({entity_handle, ai_script_entity.m_search_location_list.at(i)});
        }
        //-------END TRANSITIONS-------//
      }
    }
  }

  if (m_print_time > 5) {
    std::cout << "frame time: <" << m_frame_time_share * 100 << "%>, run_chance: <"
              << m_curr_run_chance << "%>, executed ai: <" << ai_exec << ">\n";
    m_print_time -= 5;
  }

  swarm_update(context);
  m_read_time = read_timer.stop().value_or(0);
}

void gmp::AISystem::write_update(cor::FrameContext& context) {
  bse::Timer write_timer;
  write_timer.start();

  cor::Key key = cor::Key::create<AIComponent, TransformComponent>();
  auto entities = context.entity_mgr.get_entities(key);

  for (auto& entity_handle : entities) {
    cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
    AIScriptEntityWrapper ai_script_entity(&entity, context);
    AIComponent& AIc = entity;
    TransformComponent& tc = entity;
    // auto& cc = entity.get<CharacterControllerComponent>();
    AIc.state_time += context.dt;
    AIc.total_state_time += context.dt;
    AIc.time_since_fireball += context.dt;

    if (entity.has<ThreatComponent>()) {
      ThreatComponent& threat_c = entity;
      if (threat_c.threats.size() > 0) {
        // find the entity with the most threat
        float tmp = -FLT_MAX;
        cor::EntityHandle h;
        for (std::unordered_map<cor::EntityHandle, float>::iterator it = threat_c.threats.begin();
             it != threat_c.threats.end(); ++it) {
          if (it->second > tmp) {
            tmp = it->second;
            h = it->first;
          }
        }
        // and give it to the entity wrapper
        AIc.target = h;
        AIc.has_target = true;
      } else {
        // ai_script_entity.m_curr_target_handle = nullptr; //cant do dat no more
        AIc.has_target = false;
      }
    }
    //---------HANDLE EVENTS---------//

    // handle eventual stun
    if ((m_stun.find(entity_handle)) != m_stun.end()) {
      handle_state_transition(context, entity, ai_script_entity, "stunned");
      AIc.misc = std::to_string(m_stun.at(entity_handle).duration_time);
    }

    //-------END EVENT HANDLING-------//

    // change state
    if (m_transitions.find(entity_handle) != m_transitions.end())
      handle_state_transition(context, entity, ai_script_entity, m_transitions.at(entity_handle));

    // handle eventual state change force
    if (entity.has<AIForceStateChangeComponent>()) {
      AIForceStateChangeComponent force = entity;
      handle_state_transition(context, entity, ai_script_entity, force.new_state);
      entity.detach<AIForceStateChangeComponent>();
    }

    for (int i = 0; i < ai_script_entity.m_threat_event_list.size(); ++i) {
      m_threat.insert({entity_handle, ai_script_entity.m_threat_event_list.at(i)});
    }
    for (int i = 0; i < ai_script_entity.m_search_location_list.size(); ++i) {
      m_search_location.insert({entity_handle, ai_script_entity.m_search_location_list.at(i)});
    }

    bool run_update = add_threat(entity_handle, context);
    add_search_location(entity_handle, context);

    if (m_active_ai.find(entity_handle) != m_active_ai.end() ||
        context.entity_mgr.get_entity(entity_handle).has<COLEComponent>()) {
      //-----------UPDATES-----------//
      // call lua function function_table_script_update
      // "create" function function_table_script_update
      std::function<void(std::string, std::string, std::string, AIScriptEntityWrapper&)>
          function_table_script_update = m_function_table["access_function_table"];
      if (run_update) {
        function_table_script_update(AIc.type, "update", AIc.state_name, ai_script_entity);
      }

      //---------END UPDATES---------//

      if (AIc.start_attack_this_frame == true) {
        handle_attack_start(context, AIc, entity_handle, ai_script_entity);
      }
    }
    AIc.damage_taken_previous_frame = 0;
  }
  // Steering behaviour
  steering(context);
  // Handle pathing
  handle_pathing(context);
  // regain hp if far from player
  regain_hp(context);
  // clear frame vectors
  m_active_ai.clear();
  m_dist_cutoff_ai.clear();
  m_transitions.clear();
  m_stun.clear();
  m_entity_update.clear();
  m_threat.clear();
  m_search_location.clear();

  // imgui edit variables
  BSE_EDIT_NAMED(m_swarm_pursue_seperation_factor, "AISystem/m_swarm_pursue_seperation_factor");
  BSE_EDIT_NAMED(m_swarm_combat_seperation_factor, "AISystem/m_swarm_combat_seperation_factor");
  BSE_EDIT_NAMED(m_target_cohesion_factor, "AISystem/m_target_cohesion_factor");
  BSE_EDIT_NAMED(m_target_seperation_factor, "AISystem/m_target_seperation_factor");

  cor::Key name_key = cor::Key::create<NameComponent, ShieldEffectComponent>();
  auto& handles = context.entity_mgr.get_entities(name_key);
  for (auto& handle : handles) {
    auto& e = context.entity_mgr.get_entity(handle);
    auto& name_c = e.get<NameComponent>();
    if (name_c.name.compare("COLE") == 0) {
      auto& shield = e.get<ShieldEffectComponent>();
      BSE_EDIT_NAMED(shield.color.r, "AISystem/cole_shield_color_r");
      BSE_EDIT_NAMED(shield.color.g, "AISystem/cole_shield_color_g");
      BSE_EDIT_NAMED(shield.color.b, "AISystem/cole_shield_color_b");
      BSE_EDIT_NAMED(shield.color.a, "AISystem/cole_shield_color_a");
      BSE_EDIT_NAMED(shield.scale_offset, "AISystem/cole_shield_radius");
    }
  }

  m_write_time = write_timer.stop().value_or(0);
}

void gmp::AISystem::handle_state_transition(cor::FrameContext& context, cor::Entity entity,
                                            AIScriptEntityWrapper& script_entity, std::string str) {
  AIComponent& component = entity;
  AIEntityStateChangeEvent ev;

  // run exit block function from the new state
  std::function<void(std::string, std::string, std::string, AIScriptEntityWrapper&)>
      state_script_exit_table = m_function_table["access_function_table"];
  state_script_exit_table(component.type, "exit_block", component.state_name, script_entity);

  // fill event data
  ev.id = entity.get_handle();
  ev.previous_state = component.state_name;
  ev.new_state = str;

  // change component data
  component.previous_state = component.state_name;
  component.state_name = str;
  component.state_time = 0;
  component.total_state_time = 0.0f;
  component.actions_taken = 0;
  component.funcs.m_path_finding.m_on_the_move = false;
  component.frame_hit_count = 0;
  // reset next state by setting its state table to nil
  component.lua_state = scr::DynamicTable();

  // run enter transition function from the new state
  std::function<void(std::string, std::string, std::string, AIScriptEntityWrapper&)>
      state_script_enter_table = m_function_table["access_function_table"];
  state_script_enter_table(component.type, "enter_block", component.state_name, script_entity);

  // trigger animation of new state
  AnimationComponent* animation = entity.get_if<AnimationComponent>();
  if (animation) {
    AnimationEvent event;
    event.handle = entity.get_handle();
    event.blend_time = 0.0f;

    // TODO: GET RID O DIS SHIT MON
    if (str == "ground_slam") {
      event.animation_name = "boss_anm_groundslam";
    } else if (str == "melee_attack_front") {
      event.animation_name = "boss_anm_attackfront";
    } else if (str == "melee_attack_behind") {
      event.animation_name = "boss_anm_attackbehind";
    } else if (str == "rock_throw") {
      event.animation_name = "boss_anm_rockthrow";
    } else if (str == "rock_dropped") {
      event.animation_name = "boss_anm_wounded";
    } else if (str == "chain_hook_throw") {
      event.animation_name = "boss_anm_hook";
    } else if (str == "chain_hook_pull") {
      event.animation_name = "boss_anm_hookpull";
    } else if (str == "gravel_fling") {
      event.animation_name = "boss_anm_gravelfling";
    } else {
      event.animation_name = "";
    }

    context.event_mgr.send_event(event);
  }

  context.event_mgr.send_event(ev);

  if (str == "battle_stance") {
    int a = 5;
  }

  /*std::cout << "AI <" << component.type << "> state transition: (" << ev.previous_state << ") -->
     ("
            << str << ")\n";*/
}

void gmp::AISystem::handle_attack_start(cor::FrameContext& context, AIComponent& ai_component,
                                        cor::EntityHandle entity_handle,
                                        gmp::AIScriptEntityWrapper& aisew) {
  // ai_component.target = aisew.curr_target_handle;

  // AIAttackStartEvent ev;
  // ev.handle = entity_handle;
  // ev.ai_state = ai_component.state_name;
  // context.event_mgr.send_event(ev);

  // change component status
  ai_component.start_attack_this_frame = false;
}

void gmp::AISystem::handle_stun_start_event(const StunEvent* event) {
  m_stun.insert({event->target, *event});
}

void gmp::AISystem::make_path(AIPathfinding& pf, glm::vec3 curr_pos, cor::FrameContext& context) {
  // pf.m_path = m_ai_base.get_path(curr_pos, pf.m_destination);
  glm::vec3 curr_pos_modified = curr_pos + glm::vec3(0.0f, 0.0f, 1.0f);
  glm::vec3 dest_modified = pf.m_destination + glm::vec3(0.0f, 0.0f, 1.0f);
  pf.m_path = context.navmeshes["navmesh"].closest_path(curr_pos_modified, dest_modified);
  pf.m_on_the_move = true;
  pf.m_need_path = false;
  pf.m_curr_index = 0;
}

glm::vec2 gmp::AISystem::follow_path(AIPathfinding& pf, glm::vec2 curr_pos, float dt) {
  glm::vec2 dir;
  unsigned int path_size = pf.m_path.size();
  if (pf.m_curr_index < path_size) {
    dir = calc_dir(curr_pos, pf.m_path[pf.m_curr_index], dt);
    if ((glm::length(dir) < dt)) {
      pf.m_curr_index++;
      if (pf.m_curr_index < path_size) {
        dir = calc_dir(curr_pos, pf.m_path[pf.m_curr_index], dt);
      } else {
        pf.m_on_the_move = false;
      }
    }
  } else {
    dir = {0, 0};
  }
  return dir;
}

glm::vec2 gmp::AISystem::calc_dir(glm::vec2 curr_pos, glm::vec2 next_pos, float epsi) {
  glm::vec2 dir = next_pos - curr_pos;
  if (glm::length(dir) < epsi) {
    return {0, 0};
  } else {
    dir = glm::normalize(dir);
  }
  return dir;
}

void gmp::AISystem::handle_pathing(cor::FrameContext& context) {
  cor::Key key = cor::Key::create<AIComponent, TransformComponent, CharacterControllerComponent>();
  auto entities = context.entity_mgr.get_entities(key);
  for (auto& entity_handle : entities) {
    cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
    auto& aic = entity.get<AIComponent>();
    auto& tc = entity.get<TransformComponent>();
    auto& cc = entity.get<CharacterControllerComponent>();
    glm::vec3 char_pos = tc.transform.get_position();

    // if the ai wants to go to a new grid position, calculate a new path
    if (aic.funcs.m_path_finding.m_need_path) {
      make_path(aic.funcs.m_path_finding, char_pos, context);
    }
    // If AI component is on the move(pathing), update the direction according to the path.
    if (aic.funcs.m_path_finding.m_on_the_move) {
      // Calc direction
      glm::vec2 dir =
          follow_path(aic.funcs.m_path_finding, char_pos, aic.movement_speed * context.dt);

      dir *= aic.movement_speed * context.dt;

      // Update the position by adding the direction vector(should be fixed later to update
      // the character component of the ai entity).
      cc.contents.set_move_vector(glm::vec3(dir.x, dir.y, 0.0f));
    } else {
      cc.contents.set_move_vector(glm::vec3(0.0f, 0.0f, 0.0f));
    }
  }
}

glm::vec3 trunc_vec3(const glm::vec3& vec, float length) {
  return (glm::length(vec) > length) ? glm::normalize(vec) * length : vec;
}

void gmp::AISystem::steering(cor::FrameContext& context) {
  cor::Key key =
      cor::Key::create<CharacterControllerComponent, TransformComponent, SteeringComponent>();
  auto entities = context.entity_mgr.get_entities(key);
  for (auto& entity_handle : entities) {
    auto entity = context.entity_mgr.get_entity(entity_handle);
    auto& cc = entity.get<CharacterControllerComponent>();
    auto& sc = entity.get<SteeringComponent>();
    auto& tc = entity.get<TransformComponent>();
    if (sc.on_the_move) {
      // Check if location is reached
      glm::vec3 dist_left = get_goal_pos(sc, context) - tc.transform.get_position();
      if (glm::length(dist_left) < sc.stop_dist) {
        // update_move_vec = dist_left;
        sc.on_the_move = false;
        // makes skull stop
        sc.vel_curr = {0.0f, 0.0f, 0.0f};
        cc.contents.set_move_vector(sc.vel_curr);
        return;
      }
      glm::vec3 steer_vec = steering_vector(context, entity);
      glm::vec3 avoid_vec = avoidance_vector(context, entity);
      glm::vec3 seperation_vec;
      if (m_entity_update.find(entity_handle) != m_entity_update.end()) {
        seperation_vec = m_entity_update[entity_handle].swarm_vec;
      } else {
        seperation_vec = {0.0f, 0.0f, 0.0f};
      }
      glm::vec3 accel =
          sc.accel_multiply * (steer_vec + avoid_vec + seperation_vec) / cc.contents.get_mass();
      // calculate final move vector
      sc.vel_curr = trunc_vec3(sc.vel_curr + accel, sc.max_speed);
      // add swarm seperation
      // update move vector
      glm::vec3 update_move_vec = sc.vel_curr * context.dt;

      cc.contents.set_move_vector(update_move_vec);
    }
  }
}

glm::vec3 gmp::AISystem::steering_vector(cor::FrameContext& context, cor::Entity& entity) {
  auto& tc = entity.get<TransformComponent>();
  auto& cc = entity.get<CharacterControllerComponent>();
  auto& sc = entity.get<SteeringComponent>();
  // Extract goal position
  glm::vec3 goal = get_goal_pos(sc, context);
  glm::vec3 vel_curr = sc.vel_curr;
  glm::vec3 vel_desired = glm::normalize(goal - tc.transform.get_position()) * sc.max_speed;
  glm::vec3 steering_vec = vel_desired - vel_curr;
  // steering_vec = glm::clamp(steering_vec, 0.0f, MAX_STEER_FORCE);
  return trunc_vec3(steering_vec, sc.max_steer_force);
}

glm::vec3 gmp::AISystem::avoidance_vector(cor::FrameContext& context, cor::Entity& entity) {
  auto& tc = entity.get<TransformComponent>();
  auto& cc = entity.get<CharacterControllerComponent>();
  auto& sc = entity.get<SteeringComponent>();
  glm::vec3 vel_curr = sc.vel_curr;
  glm::vec3 pos = tc.transform.get_position();
  if (glm::length(vel_curr) > EPSILON) {
    if (sc.time_left_major_threat > 0.0f) {
      sc.time_left_major_threat -= context.dt;
      return sc.avoid_vec;
    }
    std::vector<cor::RayResult> ahead_result =
        context.utilities.raycast(pos, vel_curr, std::max(glm::length(vel_curr), sc.max_see_ahead));

    for (auto& res : ahead_result) {
      auto potential_threat = context.entity_mgr.get_entity(res.handle);
      auto static_comp = potential_threat.get_if<StaticComponent>();
      if (static_comp != nullptr) {
        sc.time_left_major_threat = sc.avoid_fall_off_time;
        sc.avoid_vec = avoidance_vector(entity, *static_comp);
        return sc.avoid_vec;
      }
    }
  }
  return glm::vec3({0.f, 0.f, 0.f});
}

glm::vec3 gmp::AISystem::avoidance_vector(cor::Entity& entity, gmp::StaticComponent& static_comp) {
  gfx::AABB aabb = static_comp.contents.get_aabb();
  auto& tc = entity.get<TransformComponent>();
  auto& cc = entity.get<CharacterControllerComponent>();
  auto& sc = entity.get<SteeringComponent>();
  // calculates the closest point to the object the entity will pass through if it continues at
  // current move direction.
  // Temp avoid solution (just turns to one side to try and go around it.)

  glm::vec3 avoid_vec = glm::normalize(tc.transform.get_right()) * sc.max_avoid_force;

  // TODO: make them avoid shortest way according to aabb(when the map is made out of multiple
  return avoid_vec;
}

glm::vec3 point_plane_projection(const glm::vec3& point, const glm::vec3& norm) {
  float dist = glm::dot(norm, point);
  return point - norm * dist;
}

void gmp::AISystem::swarm_update(const cor::FrameContext& context) {
  m_entity_update.clear();
  cor::Key key = cor::Key::create<SteeringComponent, AIComponent, TransformComponent>();
  auto& entities = context.entity_mgr.get_entities(key);
  std::vector<cor::EntityHandle> pursue_swarm;
  std::vector<cor::EntityHandle> combat_swarm;
  cor::EntityHandle swarm_target_handle;
  // find all ai�s in the swarm
  for (auto& entity_handle : entities) {
    auto& entity = context.entity_mgr.get_entity(entity_handle);
    auto& aic = entity.get<AIComponent>();

    if (aic.has_target) {
      if (aic.state_name.compare("pursue") == 0) {
        pursue_swarm.push_back(entity_handle);
      } else {
        combat_swarm.push_back(entity_handle);
      }
      swarm_target_handle = aic.target;
    }
  }
  pursue_swarm_update(context, pursue_swarm, swarm_target_handle);
  combat_swarm_update(context, combat_swarm, swarm_target_handle);
}
void gmp::AISystem::pursue_swarm_update(const cor::FrameContext& context,
                                        const std::vector<cor::EntityHandle>& swarm,
                                        cor::EntityHandle swarm_target_handle) {
  glm::vec3 swarm_center(0.0f);
  // calculate the center of swarm
  for (auto& entity_handle : swarm) {
    auto& entity = context.entity_mgr.get_entity(entity_handle);
    auto& tc = entity.get<TransformComponent>();
    swarm_center += tc.transform.get_position();
  }
  swarm_center = swarm_center / ((float)swarm.size());
  auto& swarm_target = context.entity_mgr.get_entity(swarm_target_handle);
  if (!swarm_target.is_valid()) {
    return;
  }
  if (!swarm_target.has<TransformComponent>()) {
    return;
  }
  glm::vec3 plane_norm =
      swarm_target.get<TransformComponent>().transform.get_position() - swarm_center;
  plane_norm = glm::normalize(plane_norm);
  std::unordered_map<cor::EntityHandle, glm::vec3> projected_pos;
  // calculate projected positions
  for (auto& entity_handle : swarm) {
    auto& entity = context.entity_mgr.get_entity(entity_handle);
    auto& tc = entity.get<TransformComponent>();
    projected_pos[entity.get_handle()] =
        point_plane_projection(tc.transform.get_position(), plane_norm);
  }
  // calculate seperation of swarm
  for (unsigned int i = 0; i < swarm.size(); i++) {
    glm::vec3 direction(0.0f);
    // calculate seperation vector by looping through all the others in swarm.
    for (unsigned int j = 0; j < swarm.size(); j++) {
      // if not self
      if (i != j) {
        glm::vec3 tmp;
        tmp = projected_pos[swarm[i]] - projected_pos[swarm[j]];
        float dist = glm::length(tmp);
        tmp = glm::normalize(tmp) / dist;
        direction += tmp;
      }
    }
    direction *= m_swarm_pursue_seperation_factor;
    if (swarm.size() > 1) {
      m_entity_update[swarm[i]].swarm_vec = direction / ((float)swarm.size() - 1);
    }
  }
}

void gmp::AISystem::combat_swarm_update(const cor::FrameContext& context,
                                        const std::vector<cor::EntityHandle>& swarm,
                                        cor::EntityHandle swarm_target_handle) {
  auto& swarm_target = context.entity_mgr.get_entity(swarm_target_handle);
  if (!swarm_target.is_valid()) {
    return;
  }
  if (!swarm_target.has<TransformComponent>()) {
    return;
  }
  auto swarm_target_tc = swarm_target.get<TransformComponent>();

  // calculate seperation of swarm
  for (unsigned int i = 0; i < swarm.size(); i++) {
    glm::vec3 direction(0.0f);
    auto& entity_i = context.entity_mgr.get_entity(swarm[i]);
    auto& tc_i = entity_i.get<TransformComponent>();
    // calculate seperation vector by looping through all the others in swarm.
    for (unsigned int j = 0; j < swarm.size(); j++) {
      // if not self
      if (i != j) {
        glm::vec3 tmp;
        auto& entity_j = context.entity_mgr.get_entity(swarm[j]);
        auto& tc_j = entity_j.get<TransformComponent>();
        tmp = tc_i.transform.get_position() - tc_j.transform.get_position();
        float dist = glm::length(tmp);
        tmp = glm::normalize(tmp) / dist;
        direction += tmp;
      }
    }
    direction *= m_swarm_combat_seperation_factor;
    if (swarm.size() > 1) {
      direction = direction / ((float)swarm.size() - 1);
    }
    // calculate cohesion to target
    glm::vec3 target_cohesion_vec =
        swarm_target_tc.transform.get_position() - tc_i.transform.get_position();
    // calculate seperation from target
    glm::vec3 target_seperation_vec =
        tc_i.transform.get_position() - swarm_target_tc.transform.get_position();
    float dist = glm::length(target_seperation_vec);
    target_seperation_vec = glm::normalize(target_seperation_vec) / dist;
    direction = direction + target_cohesion_vec * m_target_cohesion_factor +
                target_seperation_vec * m_target_seperation_factor;
    m_entity_update[swarm[i]].swarm_vec = direction;
  }
}

// glm::vec3 gmp::AISystem::swarming_vector(const cor::FrameContext& context) {}

bool gmp::AISystem::add_threat(cor::EntityHandle entity_handle, cor::FrameContext& context) {
  bool ret = true;
  if ((m_threat.find(entity_handle)) != m_threat.end()) {
    ret = false;
    cor::Entity& ent = context.entity_mgr.get_entity(entity_handle);
    context.event_mgr.send_event(m_threat.at(entity_handle));
  }
  return ret;
}

void gmp::AISystem::add_search_location(cor::EntityHandle entity_handle,
                                        cor::FrameContext& context) {
  if ((m_search_location.find(entity_handle)) != m_search_location.end()) {
    cor::Entity& ent = context.entity_mgr.get_entity(entity_handle);
    AIComponent& aic = ent;
    aic.search_location = m_search_location.at(entity_handle);
  }
}

void gmp::AISystem::regain_hp(cor::FrameContext& context) {
  cor::Key key = cor::Key::create<AIComponent, TransformComponent, HealthComponent>();
  auto entities = context.entity_mgr.get_entities(key);
  cor::Key player_key = cor::Key::create<PlayerComponent, TransformComponent>();
  auto players = context.entity_mgr.get_entities(player_key);
  if (players.size() > 0) {
    auto& player = context.entity_mgr.get_entity(players[0]);
    auto& player_tc = player.get<TransformComponent>();
    glm::vec3 player_pos = player_tc.transform.get_position();
    for (auto& entity_handle : entities) {
      cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
      auto& hc = entity.get<HealthComponent>();
      if (hc.health_points != hc.max_health_points) {
        auto& aic = entity.get<AIComponent>();
        auto& tc = entity.get<TransformComponent>();
        glm::vec3 ai_pos = tc.transform.get_position();
        if (glm::distance(ai_pos, player_pos) > aic.disengagement_range) {
          hc.health_points = hc.max_health_points;
        }
      }
    }
  } else {
    for (auto& entity_handle : entities) {
      cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
      auto& hc = entity.get<HealthComponent>();
      hc.health_points = hc.max_health_points;
    }
  }
}