#include "gmp/systems/replay_system.hpp"

#include "cor/entity.tcc"
#include "cor/entity_manager.tcc"
#include "gmp/components/transform_component.hpp"
#include <ext/ext.hpp>
#include <gmp/components/ai_component.hpp>
#include <gmp/components/frame_tag_component.hpp>
#include <gmp/components/name_component.hpp>
#include <gmp/components/physics_projectile_component.hpp>
#include <gmp/components/player_component.hpp>
#include <gmp/components/projection_component.hpp>
#include <gmp/components/saturation_component.hpp>
#include <gmp/components/texture_component.hpp>
#include <gmp/components/transform_2d_component.hpp>
#include <gmp/components/transform_component.hpp>

namespace gmp {

ReplaySystem::ReplaySystem(cor::FrameContext &context) {
  //

  {
    m_camera = context.entity_mgr.create_entity();
    cor::Entity camera = context.entity_mgr.get_entity(m_camera);

    gmp::TransformComponent tc;
    // tc.transform = m_transform_from;
    camera = tc;
  }

  {
    m_letterbox_upper = context.entity_mgr.create_entity();
    cor::Entity letterbox_upper = context.entity_mgr.get_entity(m_letterbox_upper);

    m_letterbox_lower = context.entity_mgr.create_entity();
    cor::Entity letterbox_lower = context.entity_mgr.get_entity(m_letterbox_lower);

    gmp::Transform2dComponent t2dc;
    t2dc.size = glm::vec2(1.f, 0.25f);
    t2dc.rotation = 0.f;
    {
      t2dc.position = glm::vec3(0.5, -4.25f, 0);
      letterbox_upper = t2dc;
    }
    {
      t2dc.position = glm::vec3(0.5, 4.25f, 0);
      letterbox_lower = t2dc;
    }

    gmp::TextureComponent texc;
    texc.texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/black_pixel.png"_fp);
    letterbox_upper = texc;
    letterbox_lower = texc;
  }

  add_listener(
      context.event_mgr.register_handler<KilledGolemEvent>([&, this](const KilledGolemEvent *ev) {
        //
        // We don't want to accidentally trigger a replay while replaying
        if (!context.is_replay && m_time_since_last_replay > 4.f) {
          context.event_mgr.send_event(StartEventPlayback{});  //
          m_replay_type = Type::BOSS_DEATH_REPLAY;
          m_boss = ev->golem_handle;

          {
            auto entity_handles =
                context.entity_mgr.get_entities(cor::Key::create<PlayerComponent>());
            if (entity_handles.size() == 0) {
              LOG(WARNING) << "No player entity was found";
              return;
            }
            m_player = entity_handles[0];
          }

          m_arrow = ev->killing_arrow_handle;
          m_arrow_spawn_frame_num = ev->arrow_frame_num;
          m_arrow_final_point = ev->arrow_final_point;
        }
      }));

  add_listener(
      context.event_mgr.register_handler<ReplayStarted>([&, this](const ReplayStarted *ev) {
        start_sequence(context);  //
      }));

  add_listener(context.event_mgr.register_handler<ReplayEnded>([&, this](const ReplayEnded *ev) {
    end_sequence(context);  //
  }));

  // add_listener(
  //     context.event_mgr.register_handler<LoadCheckpoint>([&, this](const LoadCheckpoint *ev) {
  //       end_sequence(context);  //
  //     }));
}

ReplaySystem::~ReplaySystem() {
  //
}

void ReplaySystem::start_sequence(cor::FrameContext &context) {
  //
  m_time_since_last_replay = 0.f;

  ChangeCameraEvent cam_ev;
  cam_ev.entity_handle = m_camera;
  context.event_mgr.send_event(cam_ev);
  {
    cor::Entity letterbox_upper = context.entity_mgr.get_entity(m_letterbox_upper);
    cor::Entity letterbox_lower = context.entity_mgr.get_entity(m_letterbox_lower);

    {
      gmp::Transform2dComponent &t2dc = letterbox_upper;
      t2dc.size = glm::vec2(1.f, 0.25f);
      t2dc.rotation = 0.f;
      t2dc.position = glm::vec3(0.5, 0.0f, 0);
    }
    {
      gmp::Transform2dComponent &t2dc = letterbox_lower;
      t2dc.size = glm::vec2(1.f, 0.25f);
      t2dc.rotation = 0.f;
      t2dc.position = glm::vec3(0.5, 1.0f, 0);
    }
  }

  {
    cor::Entity camera = context.entity_mgr.get_entity(m_camera);

    gmp::ProjectionComponent pc = gmp::ProjectionComponent();

    {
      // Copy the settings from the main camera into our replay camera
      auto entity_handles =
          context.entity_mgr.get_entities(cor::Key::create<ProjectionComponent, NameComponent>());
      for (auto &handle : entity_handles) {
        cor::Entity entity = context.entity_mgr.get_entity(handle);
        NameComponent &nc = entity;
        if (nc.name == "main_camera") {
          ProjectionComponent &main_pc = entity;
          pc = main_pc;
        }
      }
    }

    // Custom FOV for cinematic feel
    pc.field_of_view = 60.f;
    pc.near = 0.05f;
    pc.projection_matrix =
        glm::perspective(glm::radians(pc.field_of_view), pc.aspectRatio, pc.near, pc.far);
    camera = pc;
  }
}

void ReplaySystem::end_sequence(cor::FrameContext &context) {
  //
  // ChangeCameraEvent cam_ev;
  // cam_ev.entity_handle = cor::EntityHandle::NULL_HANDLE;
  // context.event_mgr.send_event(cam_ev);

  {
    cor::Entity letterbox_upper = context.entity_mgr.get_entity(m_letterbox_upper);
    cor::Entity letterbox_lower = context.entity_mgr.get_entity(m_letterbox_lower);

    {
      gmp::Transform2dComponent &t2dc = letterbox_upper;
      t2dc.size = glm::vec2(1.f, -4.25f);
      t2dc.rotation = 0.f;
      t2dc.position = glm::vec3(0.5, 0.0f, 0);
    }
    {
      gmp::Transform2dComponent &t2dc = letterbox_lower;
      t2dc.size = glm::vec2(1.f, 4.25f);
      t2dc.rotation = 0.f;
      t2dc.position = glm::vec3(0.5, 1.0f, 0);
    }
  }

  cor::Entity camera = context.entity_mgr.get_entity(m_camera);
  camera.detach<ProjectionComponent>();

  m_boss = cor::EntityHandle::NULL_HANDLE;
  m_arrow = cor::EntityHandle::NULL_HANDLE;
  m_player = cor::EntityHandle::NULL_HANDLE;
}

void ReplaySystem::read_update(const cor::FrameContext &context) {
  //
  if (!context.is_replay) {
    m_time_since_last_replay += context.dt;
    m_phase = Phase::PH0_BOSS_FOCUS;
  }
}

void ReplaySystem::write_update(cor::FrameContext &context) {
  //
  if (context.is_replay) {
    m_time_since_last_replay = 0.f;

    cor::Entity player = context.entity_mgr.get_entity(m_player);
    if (!player.is_valid()) {
      LOG(WARNING) << "Replay: Player entity is invalid!\n";
      return;
    }

    cor::Entity boss = context.entity_mgr.get_entity(m_boss);
    if (!boss.is_valid()) {
      LOG(WARNING) << "Replay: Boss entity is invalid!\n";
      return;
    }

    if (!boss.has<AIComponent>()) {
      LOG(WARNING) << "Replay: Boss does not have AI component :O!\n";
    }

    cor::Entity camera = context.entity_mgr.get_entity(m_camera);
    if (camera.is_valid()) {
      TransformComponent &tc = player;
      TransformComponent &ctc = camera;
      TransformComponent &btc = boss;

      cor::Entity arrow = context.entity_mgr.get_entity(m_arrow);

      /*
        enum class Phase {  //
          PH0_BOSS_FOCUS,
          PH1_PLAYER_FOCUS,
          PH2_ARROW_FOCUS
        };

        constexpr static float PHASE_LENGTHS = {99.f, 6.f, 2.f};
      */

      // if (m_arrow == cor::EntityHandle::NULL_HANDLE) {
      //   auto arrow_entities = context.entity_mgr.get_entities(
      //       cor::Key::create<TransformComponent, FrameTagComponent>());

      //   for (auto handle : arrow_entities) {
      //     cor::Entity entity = context.entity_mgr.get_entity(handle);
      //     if (entity.get<FrameTagComponent>().frame_num == m_arrow_spawn_frame_num &&
      //         entity.get<NameComponent>().name == "player_arrow") {
      //       m_arrow = handle;
      //       arrow = entity;
      //     }
      //   }
      // }

      if (m_phase != Phase::PH2_ARROW_FOCUS) {
        if (arrow.is_valid() && arrow.has<PhysicsProjectileComponent>()) {
          m_phase = Phase::PH2_ARROW_FOCUS;
          m_arrow_start_point = arrow.get<TransformComponent>().transform.get_position();
        } else {
          float total_length = 0.f;
          for (int i = (int)Phase::PH1_PLAYER_FOCUS; i >= (int)Phase::PH0_BOSS_FOCUS; i--) {
            //
            total_length += PHASE_LENGTHS[i];

            if (context.replay_time_left < total_length) {
              m_phase = static_cast<Phase>(i);
              break;
            }
          }
          // std::cout << "State: " << static_cast<int>(m_phase) << ", " << context.replay_time_left
          //           << "\n";
        }
      }

      float phase_progress = context.replay_time_left;
      for (int p = (int)m_phase + 1; p < (int)Phase::PH2_ARROW_FOCUS; p++) {
        //
        phase_progress -= PHASE_LENGTHS[(int)p];
      }
      phase_progress /= PHASE_LENGTHS[(int)m_phase];
      phase_progress = 1.f - phase_progress;

      switch (m_phase) {
        //
        case Phase::PH0_BOSS_FOCUS: {
          ctc.transform.set_model_matrix(
              glm::inverse(glm::lookAt(tc.transform.get_position() + glm::vec3(0, 0, 3),
                                       btc.transform.get_position() + glm::vec3(0, 0, 25),
                                       {0, 0, 1})),
              context);

          context.replay_time_factor = 1.f;
        } break;

        //
        case Phase::PH1_PLAYER_FOCUS: {
          ctc.transform.set_model_matrix(
              glm::inverse(glm::lookAt(btc.transform.get_position() + glm::vec3(0, 0, 55),
                                       tc.transform.get_position() + glm::vec3(0, 0, 1.8f),
                                       {0, 0, 1})),
              context);

          // const float max_fov = 120.f;
          // const float min_fov = 20.f;
          const float fov_progress = (glm::clamp(phase_progress, 0.4f, 0.6f) - 0.4f) / 0.2f;
          const float target_fov = (60.f) * (1.f - fov_progress) + (10.f) * (fov_progress);

          ProjectionComponent &cpc = camera;
          cpc.field_of_view = cpc.field_of_view * 0.80f + target_fov * 0.20f;
          cpc.projection_matrix =
              glm::perspective(glm::radians(cpc.field_of_view), cpc.aspectRatio, cpc.near, cpc.far);

          context.replay_time_factor = 1.f;
        } break;

        //
        case Phase::PH2_ARROW_FOCUS: {
          if (!arrow.is_valid()) {
            break;
          }
          TransformComponent &atc = arrow;
          // ctc.transform.set_model_matrix(
          //     glm::inverse(glm::lookAt(atc.transform.get_position() + glm::vec3{10, 10, 10},
          //                              atc.transform.get_position(), {0, 0, 1})),
          //     context);
          glm::vec3 mid = (m_arrow_start_point + m_arrow_final_point) / 2.f;
          glm::vec3 track_dir = glm::normalize(m_arrow_final_point - m_arrow_start_point);
          glm::vec3 offshoot =
              glm::cross(glm::normalize(atc.transform.get_position() - m_arrow_start_point),
                         glm::vec3(0, 0, 1));
          float distance_along_track = glm::distance(m_arrow_start_point, m_arrow_final_point);
          float arrow_progress_unnormalized =
              glm::dot(atc.transform.get_position() - m_arrow_start_point, track_dir);
          float arrow_progress = arrow_progress_unnormalized / distance_along_track;

          // glm::vec3 diff = (m_arrow_start_point + track_dir * distance_along_track / 2.f) -
          // mid; std::cout << diff.x << " " << diff.y << " " << diff.z << "\n";

          // if (arrow_progress > 0.50f) {
          //   ctc.transform.set_model_matrix(
          //       // ctc.transform.get_model_matrix() * 0.70f +
          //       glm::inverse(glm::lookAt(
          //           m_arrow_start_point + track_dir * distance_along_track / 2.f + 0.1f *
          //           offshoot, atc.transform.get_position(), {0, 0, 1})),
          //       context);
          // } else {
          ctc.transform.set_model_matrix(
              glm::inverse(
                  glm::lookAt(mid + 0.1f * offshoot, atc.transform.get_position(), {0, 0, 1})),
              context);
          // }

          // float slow_motion_factor =
          //     glm::clamp(glm::distance(atc.transform.get_position(), mid) /
          //                    glm::distance(m_arrow_start_point, m_arrow_final_point) / 2.f,
          //                0.01f, 0.8f);

          context.replay_time_factor = glm::clamp(std::abs(arrow_progress - 0.5f), 0.003f, 0.8f);
          // std::cout << "Replay time factor: " << slow_motion_factor << "\n";

          const float max_fov = 120.f;
          const float min_fov = 20.f;

          ProjectionComponent &cpc = camera;
          cpc.field_of_view = max_fov * glm::clamp(1.f - std::abs(arrow_progress - 0.5f) * 2.f,
                                                   min_fov / max_fov, 1.0f);
          cpc.projection_matrix =
              glm::perspective(glm::radians(cpc.field_of_view), cpc.aspectRatio, cpc.near, cpc.far);
        } break;
      }

      if (m_phase != Phase::PH2_ARROW_FOCUS && context.frame_num > m_arrow_spawn_frame_num - 5) {
        context.replay_time_factor = 0.8f;
      }

      // ctc.transform.set_model_matrix(
      //     glm::inverse(glm::lookAt(btc.transform.get_position() + glm::vec3(0, 0, 20),
      //                              tc.transform.get_position(), {0, 0, 1})),
      //     context);
      // ctc.transform = tc.transform;
    }
  }
}

}  // namespace gmp
