#include "gmp/systems/graphics_system.hpp"

#include "bse/allocators.hpp"                              // for StackAlloc...
#include "bse/assert.hpp"                                  // for ASSERT
#include "cor/entity.tcc"                                  // for ConstEntity
#include "cor/entity_handle.hpp"                           // for EntityHandle
#include "cor/entity_manager.tcc"                          // for EntityManager
#include "cor/event.hpp"                                   // for DebugDrawP...
#include "cor/event_manager.hpp"                           // for EVENT_REGI...
#include "cor/frame_context.hpp"                           // for FrameContext
#include "cor/key.hpp"                                     // for Key
#include "gfx/key.hpp"                                     // for Key, Key::...
#include "gmp/components/directional_light_component.hpp"  // for Directiona...
#include "gmp/components/model_component.hpp"              // for RadialComp...
#include "gmp/components/projection_component.hpp"         // for Projection...
#include "gmp/components/radial_component.hpp"             // for RadialComp...
#include "gmp/components/transform_component.hpp"          // for Transform
#include "gmp/systems/graphics_helper/compose_pass.hpp"    // for ComposePass
#include "gmp/systems/graphics_helper/csm_pass.hpp"        // for CSMPass
#include "gmp/systems/graphics_helper/forward_pass.hpp"    // for ForwardPass
#include "gmp/systems/graphics_helper/god_ray_pass.hpp"    // for GodRayPass
#include "gmp/systems/graphics_helper/hud_pass.hpp"        // for HUDPass
#include "gmp/systems/graphics_helper/radial_pass.hpp"     // for RadialPass
#include "gmp/systems/graphics_helper/render_pass.hpp"     // for RenderPass
#include "gmp/systems/graphics_helper/ssao_pass.hpp"       // for SSAOPass
#include <bse/edit.hpp>                                    // for BSE_EDIT_N...
#include <gfx/buckets/gbuffer_bucket.hpp>                  // for GBufferBucket
#include <gfx/command.hpp>                                 // for DrawPoints
#include <gfx/context.hpp>
#include <gfx/debug_buffer.hpp>   // for DebugBuffer
#include <gfx/framebuffer.hpp>    // for Framebuffer
#include <gfx/frustum_shape.hpp>  // for FrustumShape
#include <gfx/helper.hpp>
#include <gfx/window.hpp>  // for Window
namespace gmp {
struct ModelComponent;
}

namespace gmp {

void GraphicsSystem::initialize() {
  gfx::Context::run([this]() {
    float ssao_quality = 0.2f;

    m_frustum_splits[0] = 0.1;
    m_frustum_splits[1] = 8.f;
    m_frustum_splits[2] = 50.f;
    m_frustum_splits[3] = 150.f;
    m_frustum_splits[4] = 600.0f;  // camera far.

#ifndef UNBOUND_PROFILE
    glm::ivec2 debug_size = glm::ivec2(1920, 1080);
#else
    glm::ivec2 debug_size = glm::ivec2(1920, 1080);
#endif

    float god_ray_resolution = 0.25f;
    float glow_resolution = 0.2f;

    // create buckets and allocate a stack allocator.
    m_stack_allocator_front = std::make_unique<bse::StackAllocator>(1 << 22);
    m_stack_allocator_back = std::make_unique<bse::StackAllocator>(1 << 22);

    // for (int i = 0; i < 1000; i++) {
    //   m_stack_allocator->clear();
    //   // gmp::GraphicsSystem(context, &window);
    //   // gfx::FrameBuffer fbo(1920, 1080, true, true);
    //   // fbo.add_texture(gfx::TextureType::RGBA16F);
    //   // fbo.create();
    // }
    m_csm_pass = std::make_unique<CSMPass>(m_stack_allocator_front.get(),
                                           m_stack_allocator_back.get(), 2048);

    m_portal_stencil_pass = std::make_unique<EffectPortalStencilPass>(
        m_stack_allocator_front.get(), m_stack_allocator_back.get(), debug_size.x, debug_size.y);
    m_forward_pass = std::make_unique<ForwardPass>(
        m_stack_allocator_front.get(), m_stack_allocator_back.get(), debug_size.x, debug_size.y);
    m_ssao_pass = std::make_unique<SSAOPass>(
        m_stack_allocator_front.get(), m_stack_allocator_back.get(), debug_size.x, debug_size.y);
    m_god_ray_pass = std::make_unique<GodRayPass>(
        m_stack_allocator_front.get(), m_stack_allocator_back.get(),
        debug_size.x * god_ray_resolution, debug_size.y * god_ray_resolution);
    m_radial_pass =
        std::make_unique<RadialPass>(m_stack_allocator_front.get(), m_stack_allocator_back.get(),
                                     m_window->get_width(), m_window->get_height());
    m_compose_pass =
        std::make_unique<ComposePass>(m_stack_allocator_front.get(), m_stack_allocator_back.get(),
                                      m_window->get_width(), m_window->get_height());
    m_glow_gaussian_blur = std::make_unique<GlowPass>(
        m_stack_allocator_front.get(), m_stack_allocator_back.get(),
        m_window->get_width() * glow_resolution, m_window->get_height() * glow_resolution);
    m_mouse_picking_pass = std::make_unique<MousePickingPass>(
        m_stack_allocator_front.get(), m_stack_allocator_back.get(), m_window->get_width(),
        m_window->get_height());
    m_context.utilities.set_mouse_picking_data(m_mouse_picking_pass->get_fbo(),
                                               m_mouse_picking_pass->get_texture("pos"),
                                               m_mouse_picking_pass->get_texture("normal"));
    m_hud_pass =
        std::make_unique<HUDPass>(m_stack_allocator_front.get(), m_stack_allocator_back.get(),
                                  m_window->get_width(), m_window->get_height());

    // add to array order will reflect rendering order
    m_all_passes.push_back(m_forward_pass.get());         // depth prepass
    m_all_passes.push_back(m_ssao_pass.get());            // ssao pass
    m_all_passes.push_back(m_portal_stencil_pass.get());  // create portal map
    m_all_passes.push_back(m_csm_pass.get());
    m_all_passes.push_back(m_mouse_picking_pass.get());
    m_all_passes.push_back(m_forward_pass.get());  // color forward pass
    m_all_passes.push_back(m_god_ray_pass.get());
    m_all_passes.push_back(m_glow_gaussian_blur.get());
    m_all_passes.push_back(m_compose_pass.get());
    m_all_passes.push_back(m_radial_pass.get());
    m_all_passes.push_back(m_hud_pass.get());

    // setup double buffers:
    m_active_fbo = std::make_unique<gfx::FrameBuffer>();
    // m_active_fbo = std::make_unique<gfx::FrameBuffer>(m_window->get_width(),
    // m_window->get_height(),
    //                                                  false, false);
    // m_active_fbo->add_texture(gfx::TextureType::RGBA);
    // m_active_fbo->create();
    m_inactive_fbo = std::make_unique<gfx::FrameBuffer>(m_window->get_width(),
                                                        m_window->get_height(), false, false);
    m_inactive_fbo->add_texture(gfx::TextureType::RGBA);
    m_inactive_fbo->create();

    // create debug buffers:
    m_debug_buffer_line = std::make_unique<gfx::DebugBuffer>();
    m_debug_buffer_point = std::make_unique<gfx::DebugBuffer>();

    // Register to listen to debug drawing events
    add_listener(
        EVENT_REGISTER_MEMBER_FUNCTION_I(DebugDrawLineEvent, m_context.event_mgr, draw_debug_line));
    add_listener(EVENT_REGISTER_MEMBER_FUNCTION_I(DebugDrawPointEvent, m_context.event_mgr,
                                                  draw_debug_point));
    add_listener(EVENT_REGISTER_MEMBER_FUNCTION_I(ChangeCameraEvent, m_context.event_mgr,
                                                  change_active_camera));

    m_mouse_picking_listener = m_context.event_mgr.register_handler<BeginMousePicking>(
        [this](const BeginMousePicking* event) {
          m_generate_picking_texture = true;
          m_pick_location = event->position;
        });
  });
}
GraphicsSystem::GraphicsSystem(cor::FrameContext& context, gfx::Window* window)
    : m_context(context), m_window(window) {
  initialize();
}

GraphicsSystem::~GraphicsSystem() {
  bse::TaskScheduler::get()->wait_on_task(m_submit_task);
  gfx::Context::run([this]() {
    m_debug_buffer_point.reset();
    m_debug_buffer_line.reset();
    m_csm_pass.reset();
    m_portal_stencil_pass.reset();
    m_forward_pass.reset();
    m_ssao_pass.reset();
    m_god_ray_pass.reset();
    m_radial_pass.reset();
    m_compose_pass.reset();
    m_glow_gaussian_blur.reset();
    m_mouse_picking_pass.reset();
    m_hud_pass.reset();
    m_stack_allocator_front.reset();
    m_stack_allocator_back.reset();
    m_active_fbo.reset();
    m_inactive_fbo.reset();
  });
}

void GraphicsSystem::read_update(const cor::FrameContext& context) {
  // m_generate_picking_texture = true;

  // stencil concept:
  // for
  //   auto portal : current_effect_area->protals { render portal to stencilpass with
  //   effects key}
  // use stencil when rendering godray pass, glow pass and ssao pass.

  // if it hasn't swapped, render this frame to the third buffer.

  // render to active fbo.

  // update all bse_edit
  // BSE_EDIT_NAMED(m_god_rays_active, "God rays on");
  // BSE_EDIT_NAMED(m_shadows_active, "shadows on");
  BSE_EDIT_NAMED(m_radial_active, "radial on");
  // BSE_EDIT_NAMED(m_ssao_active, "ssao on");

  BSE_EDIT_NAMED(m_frustum_splits[1], "shadow_frusutm_limit1");
  BSE_EDIT_NAMED(m_frustum_splits[2], "shadow_frusutm_limit2");
  BSE_EDIT_NAMED(m_frustum_splits[3], "shadow_frusutm_limit3");
  BSE_EDIT_NAMED(m_frustum_splits[4], "shadow_frusutm_limit4");

  m_forward_pass->set_splits(m_frustum_splits[1], m_frustum_splits[2], m_frustum_splits[3]);
  m_csm_pass->set_splits(
      {m_frustum_splits[1], m_frustum_splits[2], m_frustum_splits[3], m_frustum_splits[4]});

  // prepare for new frame
  for (auto pass : m_all_passes) {
    pass->prepare_for_frame();
  }

  // models
  cor::Key model_key = cor::Key::create<TransformComponent, ModelComponent>();
  std::vector<cor::EntityHandle> models = context.entity_mgr.get_entities(model_key);
  // cameras
  cor::Key camera_key = cor::Key::create<TransformComponent, ProjectionComponent>();
  std::vector<cor::EntityHandle> cameras = context.entity_mgr.get_entities(camera_key);
  // directional lights
  cor::Key light_key = cor::Key::create<DirectionalLightComponent>();
  std::vector<cor::EntityHandle> directional_lights = context.entity_mgr.get_entities(light_key);

  unsigned int color_texture;

  // get camera
  cor::ConstEntity active_camera;
  active_camera = context.entity_mgr.get_entity(m_active_camera);
  if (!active_camera.is_valid() || !active_camera.has<ProjectionComponent>()) {
    // cameras
    cor::Key camera_key = cor::Key::create<TransformComponent, ProjectionComponent>();
    std::vector<cor::EntityHandle> cameras = context.entity_mgr.get_entities(camera_key);
    // ASSERT(cameras.size() > 0) << "Failed to find a camera. GraphicsSystem fatal error!";
    if (cameras.size() == 0) {
      return;
    }
    m_active_camera = cameras[0];
    active_camera = context.entity_mgr.get_entity(m_active_camera);
  }

  const TransformComponent& camera_tc = active_camera;
  const ProjectionComponent& camera_pc = active_camera;

  glm::mat4 inv_camera_transform = glm::inverse(camera_tc.transform.get_model_matrix());

  glm::mat4 vp_matrix = (camera_pc.projection_matrix * inv_camera_transform);
  // test frustum shape
  gfx::FrustumShape f_shape(camera_pc.projection_matrix, inv_camera_transform);

  bool debug_camera = false;
  if (debug_camera) {
    glm::mat4 debug_view_matrix = inv_camera_transform;
    glm::mat4 debug_projection_matrix = camera_pc.projection_matrix;
    debug_projection_matrix =
        glm::perspective(glm::radians(90.0f), (float)1280 / (float)720, 0.1f, 100.0f);
    debug_view_matrix = glm::lookAt(glm::vec3(10, 10, 60), glm::vec3(0, 0, 10), glm::vec3(0, 0, 1));
    draw_debug_frustum(vp_matrix, glm::vec3(1, 0, 0));
  }

  glm::vec3 cam_pos = camera_tc.transform.get_position();
  std::swap(cam_pos.y, cam_pos.z);
  cam_pos.x *= -1;
  cam_pos.z *= -1;

  // check which area the player is in...

  EffectArea effect_area =
      m_effect_area_manager.get_effect_area(camera_tc.transform.get_position(), glm::vec3(0, 0, 1));
  bse::Flags<LightEffects::COUNT> active_effects = effect_area.flags;

  std::vector<Portal> portals = effect_area.get_portal_effects(f_shape);

  bse::Flags<LightEffects::COUNT> m_all_effects =
      m_effect_area_manager.get_all_combined_effects(active_effects, portals);

  // populate stencilpass
  bse::Flags<LightEffects::COUNT> needs_stencil_flags =
      m_effect_area_manager.needs_stencil_flags(active_effects, portals);
  bool need_stencil = false;
  if (needs_stencil_flags.test(LightEffects::GLOW) ||
      needs_stencil_flags.test(LightEffects::GOD_RAYS)) {
    m_portal_stencil_pass->populate_frame(context, camera_pc, inv_camera_transform, portals,
                                          active_effects);
    m_portal_stencil_pass->set_fbo(m_forward_pass->get_fbo());
    need_stencil = true;
  }

  // find main light
  cor::ConstEntity main_dir_light;
  glm::vec3 dir_light_direction;
  glm::vec3 dir_light_color;
  bool has_shadowmap = false;
  std::vector<unsigned int> shadow_textures;
  shadow_textures.reserve(4);
  float* shadow_matrices = nullptr;
  for (cor::EntityHandle& dir_light_handle : directional_lights) {
    cor::ConstEntity dir_light = context.entity_mgr.get_entity(dir_light_handle);
    const DirectionalLightComponent& dc = dir_light;
    if (dc.has_shadow) {
      main_dir_light = dir_light;
      dir_light_direction = dc.direction;
      dir_light_color = dc.color;

      if (m_all_effects.test(LightEffects::DIRECTIONAL_SHADOWS)) {
        has_shadowmap = true;
        m_csm_pass->populate_frame(context, camera_pc.projection_matrix, inv_camera_transform,
                                   dir_light_direction, cam_pos);

        // get shadow_textures
        shadow_textures.push_back(m_csm_pass->get_texture(0));
        shadow_textures.push_back(m_csm_pass->get_texture(1));
        shadow_textures.push_back(m_csm_pass->get_texture(2));
        shadow_textures.push_back(m_csm_pass->get_texture(3));
        shadow_matrices = m_csm_pass->get_shadow_matrices();

        // add god rays if there are any light

        if (m_all_effects.test(LightEffects::GOD_RAYS)) {
          glm::vec3 cam_pos_2 = camera_tc.transform.get_position();
          unsigned int god_ray_texture = m_god_ray_pass->populate_frame(
              m_csm_pass->get_texture(0), m_csm_pass->get_texture(1), m_csm_pass->get_texture(4),
              m_forward_pass->get_texture("depth"), shadow_matrices, vp_matrix, dir_light_color,
              cam_pos_2, active_effects.test(LightEffects::GOD_RAYS),
              needs_stencil_flags.test(LightEffects::GOD_RAYS));
          // m_forward_pass->set_texture(god_ray_texture, "god_ray");
          m_compose_pass->set_texture(god_ray_texture, "god_ray");
          m_god_ray_pass->set_stencil_fbo(m_forward_pass->get_fbo()->get_id(),
                                          m_forward_pass->get_fbo()->get_width(),
                                          m_forward_pass->get_fbo()->get_height());
        }
      }
    }
  }

  //  static size_t num_init_csm_debug_updates = 0;
  //  bool update_csm_debug_frusta = false;
  //  bool draw_csm_debug_frusta = true;
  //  BSE_EDIT_NAMED(update_csm_debug_frusta, "csm/update_debug_frusta");
  //  BSE_EDIT_NAMED(draw_csm_debug_frusta, "csm/draw_debug_frusta");
  //
  //#if defined(UNBOUND_PROFILE)
  //  // update_csm_debug_frusta = false;
  //  // draw_csm_debug_frusta = false;
  //
  //#endif
  //  static glm::mat4 csm_debug_proj;
  //  static glm::mat4 csm_debug_view;
  //  static std::vector<glm::mat4> cascades;
  //  static std::vector<glm::mat4> cascade_culls;
  //  if (num_init_csm_debug_updates < 60 || update_csm_debug_frusta) {
  //    num_init_csm_debug_updates++;
  //    csm_debug_proj = camera_pc.projection_matrix;
  //    csm_debug_view = inv_camera_transform;
  //
  //    cascades = {
  //
  //            m_csm_pass->get_matrix(0),
  //            m_csm_pass->get_matrix(1),
  //            m_csm_pass->get_matrix(2),
  //            m_csm_pass->get_matrix(3),
  //    /*
  //            gfx::calcuate_shadow_frustum(m_frustum_splits[0], m_frustum_splits[1],
  //                                         camera_pc.projection_matrix, inv_camera_transform,
  //                                         dir_light_direction),
  //            gfx::calcuate_shadow_frustum(m_frustum_splits[1], m_frustum_splits[2],
  //                                         camera_pc.projection_matrix, inv_camera_transform,
  //                                         dir_light_direction),
  //            gfx::calcuate_shadow_frustum(m_frustum_splits[2], m_frustum_splits[3],
  //                                         camera_pc.projection_matrix, inv_camera_transform,
  //                                         dir_light_direction),*/
  //            /*gfx::calcuate_shadow_frustum(m_frustum_splits[3], m_frustum_splits[4],
  //                                         camera_pc.projection_matrix, inv_camera_transform,
  //                                         dir_light_direction)*/};
  //
  //    cascade_culls = {m_csm_pass->get_matrix(-1), m_csm_pass->get_matrix(-2),
  //                     m_csm_pass->get_matrix(-3), m_csm_pass->get_matrix(-4)};
  //  }
  //
  //  if (draw_csm_debug_frusta) {
  //    glm::mat4 vp = csm_debug_proj * csm_debug_view;
  //    draw_debug_frustum(vp, glm::vec3(0, 1, 0));
  //    for (const glm::mat4& cascade : cascades) {
  //      draw_debug_frustum(cascade, glm::vec3(1, 0, 0));
  //    }
  //
  //    for (const glm::mat4& cascade : cascade_culls) {
  //      draw_debug_frustum(cascade, glm::vec3(1, 1, 0));
  //    }
  //
  //    glm::mat4 inv_proj = glm::inverse(csm_debug_proj);
  //    glm::mat4 inv_view = glm::inverse(csm_debug_view);
  //    for (size_t i = 1; i < 5; i++) {
  //      std::array<glm::vec4, 4> split = {glm::vec4(-1, 1, 0, 1),    // top-left
  //                                        glm::vec4(1, 1, 0, 1),     // top-right
  //                                        glm::vec4(1, -1, 0, 1),    // bot-right
  //                                        glm::vec4(-1, -1, 0, 1)};  // bot-left
  //
  //      for (glm::vec4& p : split) {
  //        glm::vec4 split_z = csm_debug_proj * glm::vec4(0, 0, -m_frustum_splits[i], 1);
  //        p.z = split_z.z / split_z.w;
  //
  //        p = inv_proj * p;
  //        p /= p.w;
  //        p = inv_view * p;
  //      }
  //
  //      for (size_t i = 0; i < split.size(); i++) {
  //        draw_debug_line(split[i], split[(i + 1) % split.size()], glm::vec3(0, 1, 0));
  //      }
  //    }
  //  }

  m_forward_pass->populate_frame(context, camera_pc, inv_camera_transform, cam_pos, shadow_textures,
                                 shadow_matrices, m_csm_pass->get_shadow_depths(), m_all_effects,
                                 dir_light_color, dir_light_direction);

  if (m_generate_picking_texture) {
    m_mouse_picking_pass->populate_frame(context, camera_pc, inv_camera_transform, cam_pos);
    // m_generate_picking_texture = false;
  }
  color_texture = m_forward_pass->get_texture("color");

  // add ssao pass
  if (active_effects.test(LightEffects::SSAO)) {
    unsigned int ssao_texture = m_ssao_pass->populate_frame(m_forward_pass->get_texture("depth"),
                                                            camera_pc.projection_matrix);
    m_forward_pass->set_texture(ssao_texture, "ssao");
  }

  if (m_all_effects.test(LightEffects::GLOW)) {
    m_compose_pass->set_texture(
        m_glow_gaussian_blur->set_texture(
            m_forward_pass->get_texture("glow"), m_forward_pass->get_fbo()->get_id(),
            active_effects.test(LightEffects::GLOW), needs_stencil_flags.test(LightEffects::GLOW),
            m_forward_pass->get_fbo()->get_width(), m_forward_pass->get_fbo()->get_height()),
        "glow");
  }

  m_compose_pass->set_texture(color_texture, "color");
  m_compose_pass->populate_frame(context, dir_light_color);

  static gfx::FrameBuffer default_framebuffer;

  // check if radial is needed
  static bool previous_radial = false;
  bool has_radial = false;
  float radial_blur_radius = 0.f;
  if (m_radial_active) {
    cor::Key radial_key = cor::Key::create<RadialComponent>();
    std::vector<cor::EntityHandle> radial_components = context.entity_mgr.get_entities(radial_key);
    if (radial_components.size() > 0) {
      cor::ConstEntity radial_entity = context.entity_mgr.get_entity(radial_components[0]);
      const RadialComponent& rc = radial_entity;
      if (rc.blur_radius > 0.00001) {
        // m_radial_pass->set_blur_radius(rc.blur_radius);
        // has_radial = true;
        radial_blur_radius = rc.blur_radius;
      }
    }
  }
  m_compose_pass->set_radial_blur_radius(radial_blur_radius);
  if (!has_radial) {
    m_compose_pass->set_framebuffer(m_active_fbo.get());
  } else {
    // m_compose_pass->set_framebuffer(m_active_fbo.get());
    m_radial_pass->set_framebuffer(m_active_fbo.get());
    m_radial_pass->set_texture(m_compose_pass->get_texture());
  }

  m_hud_pass->populate_frame(context);
  m_hud_pass->set_framebuffer(m_active_fbo.get());

  // if debug () {

  gfx::Key command_key;
  command_key.shader = m_debug_buffer_line->get_shader();
  command_key.shader_index = command_key.shader.get_id();
  command_key.blend_type = 0;

  // add debug lines
  if (m_debug_buffer_line->get_vertex_count()) {
    gfx::commands::DrawLines* debug_line =
        m_forward_pass->m_gbuffer_bucket->add_command<gfx::commands::DrawLines>(command_key, 0);
    debug_line->vertex_count = m_debug_buffer_line->get_vertex_count();
    debug_line->vao = m_debug_buffer_line->get_vao();
  }

  // add debug points
  if (m_debug_buffer_point->get_vertex_count()) {
    gfx::commands::DrawPoints* debug_point =
        m_forward_pass->m_gbuffer_bucket->add_command<gfx::commands::DrawPoints>(command_key, 0);
    debug_point->vertex_count = m_debug_buffer_point->get_vertex_count();
    debug_point->vao = m_debug_buffer_point->get_vao();
    debug_point->point_size = 5;
  }

  // dont touch below here!!!!!!!!!!!!!!!

  // sort render commands
  for (auto pass : m_all_passes) {
    pass->sort();
  }

  bse::TaskScheduler::get()->wait_on_task(m_submit_task);

  // clear frame
  std::swap(m_stack_allocator_front, m_stack_allocator_back);
  m_stack_allocator_back->clear();
  for (auto pass : m_all_passes) {
    pass->swap();
    pass->clear();
  }

  m_submit_task = gfx::Context::run_async([this]() {
    m_debug_buffer_line->update_buffer();
    m_debug_buffer_point->update_buffer();
    // submit for rendering
    for (auto pass : m_all_passes) {
      pass->submit();
    }

    // blit active framebuffer to backbuffer
    /*gl::glBindFramebuffer(gl::GL_READ_FRAMEBUFFER, m_active_fbo->get_id());
    gl::glBindFramebuffer(gl::GL_DRAW_FRAMEBUFFER, 0);
    gl::glBlitFramebuffer(0, 0, m_window->get_width(), m_window->get_height(), 0, 0,
                          m_window->get_width(), m_window->get_height(), gl::GL_COLOR_BUFFER_BIT,
                          gl::GL_NEAREST);*/
    // m_window->swap();
    // std::swap(m_active_fbo, m_inactive_fbo);
    m_debug_buffer_line->clear();
    m_debug_buffer_point->clear();
  });
}

void GraphicsSystem::write_update(cor::FrameContext& context) {
  m_forward_pass->read_update(context);

  if (m_generate_picking_texture) {
    gfx::Context::run([this, &context]() {
      context.event_mgr.send_event(context.utilities.get_picking_data(
          m_pick_location, glm::ivec2(m_window->get_width(), m_window->get_height())));
      m_generate_picking_texture = false;
    });
  }
}

void GraphicsSystem::write_update_late(cor::FrameContext& context) {}

void GraphicsSystem::draw_debug_line(const DebugDrawLineEvent* line) {
  draw_debug_line(line->from, line->to, line->color);
}

void GraphicsSystem::draw_debug_point(const DebugDrawPointEvent* point) {
  // m_renderer.add_debug_point(point->position, point->color);
  std::vector<float> vertices(12);
  vertices.push_back(point->position.x);
  vertices.push_back(point->position.y);
  vertices.push_back(point->position.z);
  vertices.push_back(point->color.x);
  vertices.push_back(point->color.y);
  vertices.push_back(point->color.z);
  m_debug_buffer_point->add_vertices(vertices);
}

void GraphicsSystem::draw_debug_frustum(glm::mat4 matrix, glm::vec3 color) {
  glm::mat4 vp_matrix = glm::inverse(matrix);

  glm::vec4 points[8];
  points[0] = vp_matrix * glm::vec4(-1, -1, -1, 1);
  points[1] = vp_matrix * glm::vec4(1, -1, -1, 1);
  points[2] = vp_matrix * glm::vec4(-1, -1, 1, 1);
  points[3] = vp_matrix * glm::vec4(1, -1, 1, 1);
  points[4] = vp_matrix * glm::vec4(-1, 1, -1, 1);
  points[5] = vp_matrix * glm::vec4(1, 1, -1, 1);
  points[6] = vp_matrix * glm::vec4(-1, 1, 1, 1);
  points[7] = vp_matrix * glm::vec4(1, 1, 1, 1);

  for (int i = 0; i < 8; i++) {
    points[i] /= points[i].w;
  }

  draw_debug_line(glm::vec3(points[0]), glm::vec3(points[1]), color);
  draw_debug_line(glm::vec3(points[2]), glm::vec3(points[3]), color);
  draw_debug_line(glm::vec3(points[0]), glm::vec3(points[2]), color);
  draw_debug_line(glm::vec3(points[1]), glm::vec3(points[3]), color);

  draw_debug_line(glm::vec3(points[4]), glm::vec3(points[5]), color);
  draw_debug_line(glm::vec3(points[6]), glm::vec3(points[7]), color);
  draw_debug_line(glm::vec3(points[4]), glm::vec3(points[6]), color);
  draw_debug_line(glm::vec3(points[5]), glm::vec3(points[7]), color);

  draw_debug_line(glm::vec3(points[0]), glm::vec3(points[4]), color);
  draw_debug_line(glm::vec3(points[1]), glm::vec3(points[5]), color);
  draw_debug_line(glm::vec3(points[2]), glm::vec3(points[6]), color);
  draw_debug_line(glm::vec3(points[3]), glm::vec3(points[7]), color);
}

void GraphicsSystem::draw_debug_line(const glm::vec3& from, const glm::vec3& to,
                                     const glm::vec3& color) {
  std::vector<float> vertices(12);
  vertices.push_back(from.x);
  vertices.push_back(from.y);
  vertices.push_back(from.z);
  vertices.push_back(color.x);
  vertices.push_back(color.y);
  vertices.push_back(color.z);
  vertices.push_back(to.x);
  vertices.push_back(to.y);
  vertices.push_back(to.z);
  vertices.push_back(color.x);
  vertices.push_back(color.y);
  vertices.push_back(color.z);
  m_debug_buffer_line->add_vertices(vertices);
}

void GraphicsSystem::change_active_camera(const ChangeCameraEvent* event) {
  // cor::Key camera_key = cor::Key::create<TransformComponent, ProjectionComponent>();
  // std::vector<cor::EntityHandle> cameras = m_context.entity_mgr.get_entities(camera_key);
  // m_active_camera = (m_active_camera + 1) % cameras.size();

  cor::Entity entity = m_context.entity_mgr.get_entity(event->entity_handle);
  if (entity.is_valid() && entity.has<TransformComponent>() && entity.has<ProjectionComponent>()) {
    m_active_camera = event->entity_handle;
  } else {
    m_active_camera = cor::EntityHandle::NULL_HANDLE;
    LOG(WARNING) << "Tried to change to an invalid camera entity!";
  }
}

}  // namespace gmp