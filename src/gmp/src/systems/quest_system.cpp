#include "gmp/systems/quest_system.hpp"

#include "cor/key.hpp"
#include "gmp/components/pickup_component.hpp"
#include <cor/entity.tcc>
#include <cor/entity_manager.tcc>
#include <cor/frame_context.hpp>
#include <ext/ext.hpp>
#include <gmp/components/audio_component.hpp>
#include <gmp/components/graphic_text_component.hpp>
#include <gmp/components/name_component.hpp>
#include <gmp/components/particles_component.hpp>
#include <gmp/components/quest_component.hpp>
#include <gmp/components/transform_2d_component.hpp>
#include <gmp/components/transform_component.hpp>
#include <gmp/systems/audio_system.hpp>

namespace gmp {
QuestSystem::QuestSystem(cor::FrameContext &context) : m_context(context) {
  // Find quest descriptions in the game and move them down 2000 meters
  auto handles = context.entity_mgr.get_entities(
      cor::Key::create<NameComponent, TransformComponent, GraphicTextComponent>());
  for (auto h : handles) {
    cor::Entity entity = context.entity_mgr.get_entity(h);
    if (entity.is_valid()) {
      if (entity.get<NameComponent>().name == "start_quest_desc") {
        m_start_descriptions.push_back(h);
        TransformComponent &tc = entity;
        tc.transform.set_position(tc.transform.get_position() - glm::vec3(0, 0, 2000), context);
      } else if (entity.get<NameComponent>().name == "note_pickup_desc") {
        m_note_descriptions.push_back(h);
        TransformComponent &tc = entity;
        tc.transform.set_position(tc.transform.get_position() - glm::vec3(0, 0, 2000), context);
      }
    }
  }

  // Create quests
  m_quest_line = m_context.entity_mgr.create_entity();
  {
    cor::Entity entity = m_context.entity_mgr.get_entity(m_quest_line);

    QuestComponent qc;
    qc.progression = 0;
    qc.goal = 5;
    entity = qc;

    GraphicTextComponent gtc;
    gtc.text = "Protect the trees";
    gtc.font = "arial";
    gtc.color = glm::vec3(255, 215, 0);
    gtc.outline_color = glm::vec3(0, 0, 0);
    gtc.thickness = 0.0f;
    gtc.outline_thickness = 0.1f;
    entity = gtc;
  }

  // Find horn note quest
  m_horn_note_quest = m_context.entity_mgr.create_entity();
  {
    cor::Entity entity = m_context.entity_mgr.get_entity(m_horn_note_quest);

    QuestComponent qc;
    qc.progression = 0;
    qc.goal = 1;
    entity = qc;

    GraphicTextComponent gtc;
    gtc.text = "Get the horn inside the big tree stump";
    gtc.font = "arial";
    gtc.color = glm::vec3(255, 215, 0);
    gtc.outline_color = glm::vec3(0, 0, 0);
    gtc.thickness = 0.0f;
    gtc.outline_thickness = 0.1f;
    entity = gtc;
  }

  // Spawn the note
  {
    cor::EntityHandle horn_pickup_handle = m_context.entity_mgr.create_entity();
    cor::Entity horn_pickup = m_context.entity_mgr.get_entity(horn_pickup_handle);
    PickupComponent puc;
    puc.type = PickupComponent::NOTE;

    // Find position
    auto entities =
        m_context.entity_mgr.get_entities_with_components<NameComponent, TransformComponent>();
    for (auto entity : entities) {
      if (entity.comp1.name == "note_spawn") {
        puc.position = entity.comp2.transform.get_position();
        break;
      }
    }

    puc.respawn_cooldown = -1;
    horn_pickup = puc;
    TransformComponent tc;
    tc.transform.set_position(puc.position, m_context);
    tc.transform.set_scale(0.4f, m_context);
    horn_pickup = tc;
    ParticlesComponent pc;
    pc.spawn_per_second = 0.f;
    pc.min_start_color = glm::vec4(0, 0.5, 0, 1);
    pc.max_start_color = glm::vec4(0, 0.7, 0, 1);
    pc.min_end_color = glm::vec4(0, 0, 0, 1);
    pc.max_end_color = glm::vec4(0, 0.2, 0, 1);
    horn_pickup = pc;

    AudioComponent ac;
    horn_pickup = std::move(ac);
  }

  // Find horn quest
  m_horn_quest = m_context.entity_mgr.create_entity();
  {
    cor::Entity entity = m_context.entity_mgr.get_entity(m_horn_quest);

    QuestComponent qc;
    qc.progression = 0;
    qc.goal = 1;
    entity = qc;

    GraphicTextComponent gtc;
    gtc.text = "Search the 3 camps for the horn";
    gtc.font = "arial";
    gtc.color = glm::vec3(255, 215, 0);
    gtc.outline_color = glm::vec3(0, 0, 0);
    gtc.thickness = 0.0f;
    gtc.outline_thickness = 0.1f;
    entity = gtc;
  }

  // Golem quest
  m_golem_quest = m_context.entity_mgr.create_entity();
  {
    cor::Entity entity = m_context.entity_mgr.get_entity(m_golem_quest);

    QuestComponent qc;
    qc.progression = 0;
    qc.goal = 1;
    entity = qc;

    GraphicTextComponent gtc;
    gtc.text = "Kill Golem (!)";
    gtc.font = "arial";
    gtc.color = glm::vec3(255, 215, 0);
    gtc.outline_color = glm::vec3(0, 0, 0);
    gtc.thickness = 0.0f;
    gtc.outline_thickness = 0.1f;
    entity = gtc;
  }

  // Kill skulls quest
  m_skull_quest = m_context.entity_mgr.create_entity();
  {
    cor::Entity entity = m_context.entity_mgr.get_entity(m_skull_quest);

    GraphicTextComponent gtc;
    gtc.text = "Kill skulls by the treetops 0 / 5";
    gtc.font = "arial";
    gtc.color = glm::vec3(255, 215, 0);
    gtc.outline_color = glm::vec3(0, 0, 0);
    gtc.thickness = 0.0f;
    gtc.outline_thickness = 0.1f;
    entity = gtc;
  }

  // Kill COLE quest
  m_cole_quest = m_context.entity_mgr.create_entity();
  {
    cor::Entity entity = m_context.entity_mgr.get_entity(m_cole_quest);

    GraphicTextComponent gtc;
    gtc.text = "Kill C.O.L.E. (!)";
    gtc.font = "arial";
    gtc.color = glm::vec3(255, 215, 0);
    gtc.outline_color = glm::vec3(0, 0, 0);
    gtc.thickness = 0.0f;
    gtc.outline_thickness = 0.1f;
    entity = gtc;
  }

  // Register event listeners

  // Cutscene finished
  add_listener(context.event_mgr.register_handler<CutsceneFinished>(
      [this, &context](const CutsceneFinished *event) {
        show_first_quests();

        // Move the description up
        for (auto h : m_start_descriptions) {
          cor::Entity entity = context.entity_mgr.get_entity(h);
          TransformComponent &tc = entity;
          tc.transform.set_position(tc.transform.get_position() + glm::vec3(0, 0, 2000), context);
        }
      }));

  // Picked up the note
  add_listener(m_context.event_mgr.register_handler<TookHornNoteEvent>(
      [this, &context](const TookHornNoteEvent *event) {
        cor::Entity entity = m_context.entity_mgr.get_entity(m_horn_note_quest);
        if (entity.is_valid() && entity.has<QuestComponent, GraphicTextComponent>()) {
          // Active next quest
          QuestComponent &qc = entity;
          GraphicTextComponent &qgtc = entity;
          qgtc.color = glm::vec3(128, 107, 0);

          cor::Entity horn_entity = m_context.entity_mgr.get_entity(m_horn_quest);

          Transform2dComponent tc;
          tc.size = glm::vec2(0.05f, 0.05f);
          tc.position = glm::vec3(.99f, 0.62f, 0);
          tc.centering = Transform2dComponent::Centering::RIGHT;
          horn_entity = tc;

          GraphicTextComponent &gtc = horn_entity;
          context.event_mgr.send_event(SaveCheckpoint());

          // Move the description up
          for (auto h : m_note_descriptions) {
            cor::Entity entity = context.entity_mgr.get_entity(h);
            TransformComponent &tc = entity;
            tc.transform.set_position(tc.transform.get_position() + glm::vec3(0, 0, 2000), context);
          }

          // Spawn the Horn
          {
            cor::EntityHandle horn_pickup_handle = m_context.entity_mgr.create_entity();
            cor::Entity horn_pickup = m_context.entity_mgr.get_entity(horn_pickup_handle);
            PickupComponent puc;
            puc.type = PickupComponent::HORN;

            // Find position
            int r = rand() % 3;
            int index = 0;
            auto entities = m_context.entity_mgr
                                .get_entities_with_components<NameComponent, TransformComponent>();
            for (auto entity : entities) {
              if (entity.comp1.name == "horn_spawn") {
                if (index == r) {
                  puc.position = entity.comp2.transform.get_position();
                  break;
                }
                ++index;
              }
            }

            puc.respawn_cooldown = -1;
            horn_pickup = puc;
            TransformComponent tc;
            tc.transform.set_position(puc.position, m_context);
            tc.transform.set_scale(1.f, m_context);
            horn_pickup = tc;
            ParticlesComponent pc;
            pc.spawn_per_second = 0.f;
            pc.min_start_color = glm::vec4(0, 0, 0.5, 1);
            pc.max_start_color = glm::vec4(0, 0, 0.7, 1);
            pc.min_end_color = glm::vec4(0, 0, 0, 1);
            pc.max_end_color = glm::vec4(0, 0, 0.2, 1);
            horn_pickup = pc;

            AudioComponent ac;
            horn_pickup = std::move(ac);

            context.event_mgr.send_event(SaveCheckpoint());
          }
        }
      }));

  // Picked up the horn
  add_listener(m_context.event_mgr.register_handler<TookHornEvent>(
      [this, &context](const TookHornEvent *event) {
        cor::Entity entity = m_context.entity_mgr.get_entity(m_horn_quest);
        if (entity.is_valid() && entity.has<QuestComponent, GraphicTextComponent>()) {
          QuestComponent &qc = entity;
          GraphicTextComponent &qgtc = entity;
          qgtc.color = glm::vec3(128, 107, 0);

          cor::Entity golem_entity = m_context.entity_mgr.get_entity(m_golem_quest);

          Transform2dComponent tc;
          tc.size = glm::vec2(0.06f, 0.06f);
          tc.position = glm::vec3(.99f, 0.65, 0);
          tc.centering = Transform2dComponent::Centering::RIGHT;
          golem_entity = tc;

          GraphicTextComponent &gtc = golem_entity;

          context.event_mgr.send_event(SaveCheckpoint());
        }
      }));

  // Killed Golem
  add_listener(m_context.event_mgr.register_handler<KilledGolemEvent>(
      [this, &context](const KilledGolemEvent *event) {
        cor::Entity entity = m_context.entity_mgr.get_entity(m_golem_quest);
        if (entity.is_valid() && entity.has<QuestComponent, GraphicTextComponent>() &&
            !context.is_replay) {
          entity.destroy();

          cor::Entity horn_note_entity = m_context.entity_mgr.get_entity(m_horn_note_quest);
          horn_note_entity.destroy();

          cor::Entity horn_entity = m_context.entity_mgr.get_entity(m_horn_quest);
          horn_entity.destroy();

          cor::Entity quest_line_entity = m_context.entity_mgr.get_entity(m_quest_line);

          GraphicTextComponent &qgtc = quest_line_entity;
          qgtc.text = "Forest plague";

          cor::Entity skull_entity = m_context.entity_mgr.get_entity(m_skull_quest);

          GraphicTextComponent &gtc = skull_entity;

          Transform2dComponent tc;
          tc.size = glm::vec2(0.05f, 0.05f);
          tc.position = glm::vec3(.99f, 0.59, 0);
          tc.centering = Transform2dComponent::Centering::RIGHT;
          skull_entity = tc;

          QuestComponent qc;
          qc.progression = 0;
          qc.goal = 5;
          skull_entity = qc;

          context.event_mgr.send_event(SaveCheckpoint());
        }
      }));

  // Killed a normal (non-COLE) skull
  add_listener(m_context.event_mgr.register_handler<KilledSkullEvent>(
      [this, &context](const KilledSkullEvent *event) {
        cor::Entity entity = m_context.entity_mgr.get_entity(m_skull_quest);
        if (entity.is_valid() && entity.has<QuestComponent, GraphicTextComponent>()) {
          QuestComponent &qc = entity;
          GraphicTextComponent &gtc = entity;
          if (qc.progression < qc.goal) {
            qc.progression++;
            GraphicTextComponent &gtc = entity;
            gtc.text = "Kill skulls by the treetops " + std::to_string(qc.progression) + " / " +
                       std::to_string(qc.goal);
          }
          // Enough skulls killed
          if (qc.progression == qc.goal) {
            context.event_mgr.send_event(SpawnColeEvent());

            gtc.color = glm::vec3(128, 107, 0);

            cor::Entity cole_entity = m_context.entity_mgr.get_entity(m_cole_quest);

            Transform2dComponent tc;
            tc.size = glm::vec2(0.05f, 0.05f);
            tc.position = glm::vec3(.99f, 0.62, 0);
            tc.centering = Transform2dComponent::Centering::RIGHT;
            cole_entity = tc;
          }
        }
      }));

  // Killed COLE
  add_listener(m_context.event_mgr.register_handler<KilledColeEvent>(
      [this, &context](const KilledColeEvent *event) {
        cor::Entity entity = m_context.entity_mgr.get_entity(m_cole_quest);
        entity.destroy();
        cor::Entity skull_quest_entity = m_context.entity_mgr.get_entity(m_skull_quest);
        skull_quest_entity.destroy();
        cor::Entity quest_line_entity = m_context.entity_mgr.get_entity(m_quest_line);
        quest_line_entity.destroy();

        context.event_mgr.send_event(SaveCheckpoint());
      }));
}

void QuestSystem::read_update(const cor::FrameContext &context) {}

void QuestSystem::write_update(cor::FrameContext &context) {}

void QuestSystem::show_first_quests() {
  cor::Entity quest_entity = m_context.entity_mgr.get_entity(m_quest_line);
  cor::Entity note_entity = m_context.entity_mgr.get_entity(m_horn_note_quest);

  Transform2dComponent qtc;
  qtc.size = glm::vec2(0.07f, 0.07f);
  qtc.position = glm::vec3(.99f, 0.55, 0);
  qtc.rotation = 0;
  qtc.centering = Transform2dComponent::Centering::RIGHT;
  quest_entity = qtc;

  Transform2dComponent htc;
  htc.size = glm::vec2(0.05f, 0.05f);
  htc.position = glm::vec3(.99f, 0.59, 0);
  htc.rotation = 0;
  htc.centering = Transform2dComponent::Centering::RIGHT;
  note_entity = htc;
}

}  // namespace gmp