#include "gmp/systems/cloud_system.hpp"

#include "gmp/components/model_component.hpp"
#include "gmp/components/name_component.hpp"
#include "gmp/components/projection_component.hpp"
#include "gmp/components/transform_component.hpp"
#include <bse/log.hpp>
#include <cor/entity.hpp>
#include <cstdlib>

namespace gmp {

CloudSystem::CloudSystem(cor::FrameContext& context) {
  //
  for (int i = -LAYER_DIMENSIONS / 2; i < LAYER_DIMENSIONS / 2; i++) {
    for (int j = -LAYER_DIMENSIONS / 2; j < LAYER_DIMENSIONS / 2; j++) {
      //
      cor::EntityHandle handle = context.entity_mgr.create_entity();
      cor::Entity entity = context.entity_mgr.get_entity(handle);

      TransformComponent& tc = entity.attach<TransformComponent>();
      tc.transform.set_position({i * LAYER_INSTANCE_SCALE, j * LAYER_INSTANCE_SCALE, 0}, context);
      tc.transform.set_scale(LAYER_INSTANCE_SCALE, context);
      tc.transform.set_rotation_vec({0, 0, 1}, (rand() % 360) / 360.f * glm::pi<float>(), context);

      ModelComponent& mc = entity.attach<ModelComponent>();
      mc.asset = bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/cloud.fbx"_fp);

      m_handles[i + LAYER_DIMENSIONS / 2][j + LAYER_DIMENSIONS / 2] = handle;
    }
  }
}

CloudSystem::~CloudSystem() {
  //
}

void CloudSystem::read_update(const cor::FrameContext& context) {
  //
}

void CloudSystem::write_update(cor::FrameContext& context) {
  //
  if (m_camera_handle == cor::EntityHandle::NULL_HANDLE) {
    auto entities = context.entity_mgr.get_entities(cor::Key::create<NameComponent>());
    for (auto handle : entities) {
      cor::Entity ent = context.entity_mgr.get_entity(handle);
      NameComponent& nc = ent;
      if (nc.name == "main_camera") {
        m_camera_handle = handle;
        LOG(NOTICE) << "Found a camera named: \"main_camera\"";
      }
    }

    if (m_camera_handle == cor::EntityHandle::NULL_HANDLE) {
      auto camera_entities =
          context.entity_mgr.get_entities(cor::Key::create<ProjectionComponent>());
      if (!camera_entities.empty()) {
        m_camera_handle = camera_entities[0];
        LOG(NOTICE) << "Didn't find a camera named: \"main_camera\"";
      }
    }
  }

  if (m_camera_handle != cor::EntityHandle::NULL_HANDLE) {
    cor::Entity camera = context.entity_mgr.get_entity(m_camera_handle);
    Transform cam_transform = camera.get<TransformComponent>().transform;
    cam_transform.set_rotation({}, context);
    cam_transform.set_position(cam_transform.get_position() * (1.f / LAYER_INSTANCE_SCALE),
                               context);

    glm::mat4 to_cam_world = cam_transform.get_model_matrix();
    glm::mat4 to_cam_local = glm::inverse(to_cam_world);

    for (auto& row : m_handles) {
      for (auto& handle : row) {
        //
        TransformComponent& tc = context.entity_mgr.get_entity(handle);

        Transform new_cloud_transform;
        new_cloud_transform.set_model_matrix(to_cam_local * tc.transform.get_model_matrix(),
                                             context);

        // tc.transform.translate({2 * context.dt, 0, 0}, context);
        glm::vec3 pos = new_cloud_transform.get_position();
        pos.x += 20.f * context.dt;

        if (pos.x / LAYER_INSTANCE_SCALE > LAYER_DIMENSIONS / 2.f) {
          pos.x = -LAYER_DIMENSIONS / 2.f * LAYER_INSTANCE_SCALE;
        } else if (pos.x / LAYER_INSTANCE_SCALE < -LAYER_DIMENSIONS / 2.f) {
          pos.x = LAYER_DIMENSIONS / 2.f * LAYER_INSTANCE_SCALE;
        }

        new_cloud_transform.set_position(pos, context);
        tc.transform.set_model_matrix(to_cam_world * new_cloud_transform.get_model_matrix(),
                                      context);
      }
    }
  }
}

}  // namespace gmp
