#include "gmp/systems/graphics_helper/mouse_picking_pass.hpp"

#include "bse/asset.hpp"
#include "bse/directory_path.hpp"
#include "bse/file_path.hpp"
#include "bse/runtime_asset.hpp"
#include "cor/entity.tcc"
#include "cor/entity_manager.tcc"
#include "cor/frame_context.hpp"
#include "cor/key.hpp"
#include "gfx/command.hpp"
#include "gfx/command_bucket.hpp"
#include "gfx/key.hpp"
#include "gfx/material.hpp"
#include "gfx/mesh.hpp"
#include "gfx/shader.hpp"
#include "gfx/text_cache.hpp"
#include "gmp/components/animation_component.hpp"
#include "gmp/components/character_controller_component.hpp"
#include "gmp/components/directional_light_component.hpp"
#include "gmp/components/dynamic_component.hpp"
#include "gmp/components/graphic_text_component.hpp"
#include "gmp/components/model_component.hpp"
#include "gmp/components/particles_component.hpp"
#include "gmp/components/point_light_component.hpp"
#include "gmp/components/projection_component.hpp"
#include "gmp/components/rope_component.hpp"
#include "gmp/components/static_component.hpp"
#include "gmp/components/transform_component.hpp"
#include "gmp/components/transforms_component.hpp"
#include "gmp/systems/graphics_helper/graphics_system_particles_helper.hpp"
#include <bse/edit.hpp>
#include <gfx/context.hpp>
#include <gfx/framebuffer.hpp>
#include <gfx/frustum_shape.hpp>
#include <gfx/model.hpp>
// #include <gfx/particle_system_helper.hpp>
#include <gfx/context.hpp>
#include <gfx/text.hpp>
#include <gfx/texture.hpp>

namespace bse {
class StackAllocator;
}
namespace gmp {
struct AnimationComponent;
}

namespace gmp {

MousePickingPass::MousePickingPass(bse::StackAllocator* front, bse::StackAllocator* back,
                                   float width, float height)
    : RenderPass(width, height) {
  m_command_bucket = std::make_unique<gfx::CommandBucket>(front, back, m_width, m_height);

  m_framebuffer = std::make_unique<gfx::FrameBuffer>(m_width, m_height, true, true);
  m_entity_handle_texture = m_framebuffer->add_texture(gfx::TextureType::RGBA16F);
  m_world_pos_texture = m_framebuffer->add_texture(gfx::TextureType::RGBA16F);
  m_normal_texture = m_framebuffer->add_texture(gfx::TextureType::RGBA16F);
  m_framebuffer->create();

  m_shader = bse::Asset<gfx::Shader>(bse::ASSETS_ROOT / "shader/picking.vert"_fp);
  m_model_matrix_location = m_shader->get_uniform_location("MODEL_MATRIX");
  m_color_location = m_shader->get_uniform_location("color");

  // ubo data
  m_uniform_buffer.create(sizeof(UBOData), &m_ubo_data);
}

void MousePickingPass::swap() {
  RenderPass::swap();
  m_command_bucket->swap();
}
void MousePickingPass::prepare_for_frame() {
  m_base_back.active = false;
  m_command_bucket->prepare_for_new_frame();
}

void MousePickingPass::sort() {
  if (m_base_back.active) {
    m_command_bucket->sort();
  }
}
void MousePickingPass::submit() {
  if (m_base_front.active) {
    m_framebuffer->bind();
    gl::glClearColor(0.f, 0.f, 0.f, 1.f);
    gl::glClear(gl::GL_DEPTH_BUFFER_BIT | gl::GL_STENCIL_BUFFER_BIT | gl::GL_COLOR_BUFFER_BIT);
    gl::glDepthFunc(gl::GL_LEQUAL);
    // gl::glDisable(gl::GL_DEPTH_TEST);
    gl::glDisable(gl::GL_STENCIL_TEST);
    gl::glDepthMask(gl::GL_TRUE);
    gl::glColorMask(gl::GL_TRUE, gl::GL_TRUE, gl::GL_TRUE, gl::GL_TRUE);
    gl::glDisable(gl::GL_BLEND);
    m_uniform_buffer.bind(0);
    m_command_bucket->submit();
  }
}
void MousePickingPass::clear() { m_command_bucket->clear(); }

unsigned int MousePickingPass::get_texture(std::string name) {
  m_base_back.active = true;
  if (name == "pos") {
    return m_world_pos_texture;

  } else if (name == "normal") {
    return m_normal_texture;
  }
  return m_entity_handle_texture;
}

unsigned int MousePickingPass::get_fbo() { return m_framebuffer->get_id(); }

void MousePickingPass::populate_frame(const cor::FrameContext& context,
                                      const ProjectionComponent& camera_pc,
                                      const glm::mat4& v_matrix, const glm::vec3& cam_pos) {
  m_base_back.active = true;

  gfx::FrustumShape frustum_shape(camera_pc.projection_matrix, v_matrix);

  add_models(context, frustum_shape, cam_pos);

  m_ubo_data.view_matrix = v_matrix;
  m_ubo_data.projection_matrix = camera_pc.projection_matrix;
  gfx::Context::run(
      [this, data = m_ubo_data]() mutable { m_uniform_buffer.update(sizeof(data), &data); });
}

void MousePickingPass::add_models(const cor::FrameContext& context,
                                  gfx::FrustumShape& frustum_shape, const glm::vec3& cam_pos) {
  cor::Key model_key = cor::Key::create<TransformComponent, ModelComponent>();
  std::vector<cor::EntityHandle> models = context.entity_mgr.get_entities(model_key);

  for (cor::EntityHandle& entity_handle : models) {
    cor::ConstEntity entity = context.entity_mgr.get_entity(entity_handle);
    const TransformComponent& tc = entity;
    const ModelComponent& mc = entity;

    // save some fetching
    bse::Asset<gfx::Mesh> mesh = mc.asset->get_mesh();

    // do culling check...
    gfx::AABB aabb;
    if (entity.has<StaticComponent>()) {
      const StaticComponent& sc = entity;
      aabb = sc.contents.get_aabb();
    } else if (entity.has<DynamicComponent>()) {
      const DynamicComponent& dc = entity;
      aabb = dc.contents.get_aabb();
    } else if (entity.has<CharacterControllerComponent>()) {
      const CharacterControllerComponent& ccc = entity;
      aabb = ccc.contents.get_aabb();
    } else {
      aabb = mesh->get_aabb();
      aabb.size *= tc.transform.get_scale();
      aabb.position += tc.transform.get_position();
    }
    bool visible = true;
    visible = frustum_shape.aabb_intersect(aabb);

    // populate gbuffer
    if (visible) {
      gfx::Key command_key;
      command_key.shader = m_shader;
      command_key.shader_index = m_shader->get_shader_handle();
      command_key.depth = glm::distance(tc.transform.get_position(), cam_pos);
      command_key.blend_type = 0;

      // add modelmatrix
      gfx::commands::BindUniformMat4* bind_model_matrix_cp =
          m_command_bucket->add_command<gfx::commands::BindUniformMat4>(command_key, 0);
      bind_model_matrix_cp->location = m_model_matrix_location;
      bind_model_matrix_cp->matrix = tc.transform.get_model_matrix();

      // add color
      gfx::commands::BindUniformVec4* bind_color_cp =
          m_command_bucket->append_command<gfx::commands::BindUniformVec4>(bind_model_matrix_cp, 0);
      bind_color_cp->location = m_color_location;
      bind_color_cp->vector = glm::vec4((entity_handle.full_handle & 0x000000FF) >> 0,
                                        (entity_handle.full_handle & 0x0000FF00) >> 8,
                                        (entity_handle.full_handle & 0x00FF0000) >> 16,
                                        (entity_handle.full_handle & 0xFF000000) >> 24);
      bind_color_cp->vector = bind_color_cp->vector / 255.0f;

      // render mesh
      gfx::commands::DrawIndexed* draw_indexed =
          m_command_bucket->append_command<gfx::commands::DrawIndexed>(bind_color_cp, 0);
      draw_indexed->vertex_count = mesh->get_vertex_count();

      gfx::VertexFlags flags;
      flags.set(bse::VertexTypes::POSITION);
      flags.set(bse::VertexTypes::NORMAL);
      draw_indexed->vao = mesh->get_buffers(flags).m_vao;
      draw_indexed->triangle_mode = gl::GL_TRIANGLES;
    }
  }
}

}  // namespace gmp