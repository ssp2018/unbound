#include "gmp/systems/graphics_helper/gaussian_pass.hpp"

#include "gfx/command.hpp"                   // for BindTexture, BindUniform...
#include "gfx/full_frame_gaussian_pass.hpp"  // for FullFrameGaussianPass
#include "gfx/full_frame_pass.hpp"           // for FullFramePass
#include "gfx/shader.hpp"                    // for Shader
#include <bse/edit.hpp>                      // for BSE_EDIT
namespace bse {
class StackAllocator;
}

namespace gmp {

GaussianPass::GaussianPass(bse::StackAllocator* front, bse::StackAllocator* back, float width,
                           float height)
    : RenderPass(width, height) {
  m_full_frame_pass = std::make_unique<gfx::FullFrameGaussianPass>(
      front, back, bse::Asset<gfx::Shader>("assets/shader/gaussian.frag"), width, height);
}

void GaussianPass::swap() {
  RenderPass::swap();
  m_full_frame_pass->swap();
}
void GaussianPass::prepare_for_frame() { m_base_back.active = false; }

void GaussianPass::sort() {}
void GaussianPass::submit() {
  if (m_base_front.active) {
    m_full_frame_pass->submit();
  }
}
void GaussianPass::clear() { m_full_frame_pass->clear(); }

unsigned int GaussianPass::set_texture(unsigned int in_texture) {
  m_full_frame_pass->set_texture(in_texture);

  BSE_EDIT(m_blur_radius);
  m_full_frame_pass->set_radius(m_blur_radius);

  m_base_back.active = true;
  return m_full_frame_pass->get_texture(0);
}

}  // namespace gmp