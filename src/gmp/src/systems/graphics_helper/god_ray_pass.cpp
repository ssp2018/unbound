#include "gmp/systems/graphics_helper/god_ray_pass.hpp"

#include "gfx/command.hpp"                   // for BindTexture, BindUniform...
#include "gfx/full_frame_gaussian_pass.hpp"  // for FullFrameGaussianPass
#include "gfx/full_frame_pass.hpp"           // for FullFramePass
#include "gfx/shader.hpp"                    // for Shader
#include <bse/edit.hpp>                      // for BSE_EDIT
namespace bse {
class StackAllocator;
}

namespace gmp {

GodRayPass::GodRayPass(bse::StackAllocator* front, bse::StackAllocator* back, float width,
                       float height)
    : RenderPass(width, height) {
  m_god_ray_pass = std::make_unique<gfx::FullFramePass>(
      front, back, bse::Asset<gfx::Shader>("assets/shader/god_ray.frag"), width, height, false);
  m_god_ray_framebuffer = std::make_unique<gfx::FrameBuffer>(width, height, true, true);
  m_texture = m_god_ray_framebuffer->add_texture(gfx::TextureType::RED);
  m_god_ray_framebuffer->create();

  m_gaussian_framebuffer = std::make_unique<gfx::FrameBuffer>(width, height, true, true);
  m_gaussian_texture = m_gaussian_framebuffer->add_texture(gfx::TextureType::RED);
  m_gaussian_framebuffer->create();

  m_gaussian_pass = std::make_unique<gfx::FullFrameGaussianPass>(
      front, back, bse::Asset<gfx::Shader>("assets/shader/gaussian.frag"), width, height);
}

void GodRayPass::swap() {
  RenderPass::swap();
  m_god_ray_pass->swap();
  m_gaussian_pass->swap();
  std::swap(m_front, m_back);
}
void GodRayPass::prepare_for_frame() {
  m_base_back.active = false;
  m_back.stencil_active = false;
}

void GodRayPass::sort() {}
void GodRayPass::submit() {
  if (m_base_front.active) {
    float radius = 0.001;
    BSE_EDIT(radius);
    // blit stencil:
    m_god_ray_framebuffer->bind();
    gl::glClear(gl::GL_COLOR_BUFFER_BIT);
    if (m_front.stencil_active) {
      gl::glEnable(gl::GL_STENCIL_TEST);
      gl::glBindFramebuffer(gl::GL_READ_FRAMEBUFFER, m_front.stencil);
      gl::glBlitFramebuffer(0, 0, m_front.stencil_width, m_front.stencil_height, 0, 0, m_width,
                            m_height, gl::GL_STENCIL_BUFFER_BIT, gl::GL_NEAREST);

      //   gl::glNamedFramebufferReadBuffer(m_stencil, gl::GL_STEN);
      gl::glStencilOp(gl::GL_KEEP, gl::GL_KEEP, gl::GL_KEEP);
      if (m_front.inside_godray) {
        gl::glStencilFunc(gl::GL_EQUAL, 1, 0b00000010);  // Pass test if stencil value is 2
      } else {
        gl::glStencilFunc(gl::GL_NOTEQUAL, 1, 0b00000010);  // Pass test if stencil value is not 2
      }
      gl::glStencilMask(0x00);  // Don't write anything to stencil buffer
      // m_god_ray_framebuffer->bind();
      // gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, m_god_ray_framebuffer->get_id());
    } else {
      gl::glDisable(gl::GL_STENCIL_TEST);
    }
    gl::glDepthMask(gl::GL_FALSE);   // Don't write to depth buffer
    gl::glDepthFunc(gl::GL_ALWAYS);  // Pass all depthtests
    gl::glViewport(0, 0, m_width, m_height);
    m_god_ray_pass->submit();

    gl::glDisable(gl::GL_STENCIL_TEST);
    gl::glDepthMask(gl::GL_TRUE);
    gl::glDepthFunc(gl::GL_LEQUAL);
    // m_gaussian_pass->submit();
  }
}
void GodRayPass::clear() {
  m_god_ray_pass->clear();
  m_gaussian_pass->clear();
}

void GodRayPass::set_stencil_fbo(unsigned int stencil_fbo, float stencil_width,
                                 float stencil_height) {
  m_back.stencil = stencil_fbo;
  m_back.stencil_width = stencil_width;
  m_back.stencil_height = stencil_height;
}

gfx::FrameBuffer* GodRayPass::get_fbo() { return m_god_ray_framebuffer.get(); }

unsigned int GodRayPass::populate_frame(unsigned int shadow_texture_0,
                                        unsigned int shadow_texture_1,
                                        unsigned int shadow_texture_2, unsigned int depth_texture,
                                        const float* shadow_matrices, const glm::mat4& vp_matrix,
                                        const glm::vec3& light_color, const glm::vec3& cam_pos,
                                        bool inside_godray, bool stencil_active) {
  m_back.inside_godray = inside_godray;
  m_back.stencil_active = stencil_active;

  glm::mat4 inv_vp_matrix = glm::inverse(vp_matrix);

  gfx::commands::BindTexture* bind_texture_depth =
      m_god_ray_pass->add_command<gfx::commands::BindTexture>(0);
  bind_texture_depth->location =
      m_god_ray_pass->get_shader()->get_uniform_location("depth_sampler");
  bind_texture_depth->texture = depth_texture;
  bind_texture_depth->index = 0;

  gfx::commands::BindTexture* bind_texture_shadow_0 =
      m_god_ray_pass->add_command<gfx::commands::BindTexture>(0);
  bind_texture_shadow_0->location =
      m_god_ray_pass->get_shader()->get_uniform_location("shadow_sampler_0");
  bind_texture_shadow_0->texture = shadow_texture_0;
  bind_texture_shadow_0->index = 1;

  gfx::commands::BindTexture* bind_texture_shadow_1 =
      m_god_ray_pass->add_command<gfx::commands::BindTexture>(0);
  bind_texture_shadow_1->location =
      m_god_ray_pass->get_shader()->get_uniform_location("shadow_sampler_1");
  bind_texture_shadow_1->texture = shadow_texture_1;
  bind_texture_shadow_1->index = 2;

  gfx::commands::BindTexture* bind_texture_shadow_2 =
      m_god_ray_pass->add_command<gfx::commands::BindTexture>(0);
  bind_texture_shadow_2->location =
      m_god_ray_pass->get_shader()->get_uniform_location("shadow_sampler_2");
  bind_texture_shadow_2->texture = shadow_texture_2;
  bind_texture_shadow_2->index = 3;

  gfx::commands::BindUniformMat4* bind_inv_vp_matrix =
      m_god_ray_pass->add_command<gfx::commands::BindUniformMat4>(0);
  bind_inv_vp_matrix->location =
      m_god_ray_pass->get_shader()->get_uniform_location("inv_vp_matrix");
  bind_inv_vp_matrix->matrix = inv_vp_matrix;

  gfx::commands::BindUniformMat4List* bind_shadow_matrices =
      m_god_ray_pass->add_command<gfx::commands::BindUniformMat4List>(0);
  bind_shadow_matrices->location =
      m_god_ray_pass->get_shader()->get_uniform_location("shadow_matrices");
  bind_shadow_matrices->matrices = shadow_matrices;
  bind_shadow_matrices->count = 5;

  // bind light color
  gfx::commands::BindUniformVec3* bind_light_color =
      m_god_ray_pass->add_command<gfx::commands::BindUniformVec3>(0);
  bind_light_color->location = m_god_ray_pass->get_shader()->get_uniform_location("light_color");
  bind_light_color->vector = light_color;

  gfx::commands::BindUniformVec3* bind_cam_pos =
      m_god_ray_pass->add_command<gfx::commands::BindUniformVec3>(0);
  bind_cam_pos->location = m_god_ray_pass->get_shader()->get_uniform_location("cam_world_pos");
  bind_cam_pos->vector = cam_pos;

  m_base_back.active = true;
  return m_texture;
  //   return m_gaussian_pass->get_texture(0);
}

}  // namespace gmp