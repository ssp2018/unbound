#include "gmp/systems/graphics_helper/compose_pass.hpp"

#include "cor/entity.tcc"           // for ConstEn...
#include "cor/entity_handle.hpp"    // for EntityH...
#include "cor/frame_context.hpp"    // for EntityH...
#include "cor/key.hpp"              // for Key
#include "gfx/command.hpp"          // for BindTexture, BindUniformBool
#include "gfx/full_frame_pass.hpp"  // for FullFramePass
#include "gfx/shader.hpp"           // for Shader
#include <cor/entity_manager.tcc>
#include <gmp/components/player_damage_visual_component.hpp>  // for PlayerDamageVisualComponent
#include <gmp/components/saturation_component.hpp>            // for SaturationComponent

namespace bse {
class StackAllocator;
}
namespace gmp {

ComposePass::ComposePass(bse::StackAllocator* front, bse::StackAllocator* back, float width,
                         float height)
    : RenderPass(width, height) {
  m_shader = bse::Asset<gfx::Shader>("assets/shader/compose.frag");
  m_full_frame_pass =
      std::make_unique<gfx::FullFramePass>(front, back, m_shader, width, height, false);

  m_owned_framebuffer = std::make_unique<gfx::FrameBuffer>(m_width, m_height, true);
  m_texture = m_owned_framebuffer->add_texture(gfx::TextureType::RGBA);
  m_owned_framebuffer->create();
}

void ComposePass::prepare_for_frame() {
  m_base_back.active = false;
  m_back.framebuffer = nullptr;
}

void ComposePass::sort() {}

void ComposePass::swap() {
  RenderPass::swap();
  m_full_frame_pass->swap();
  std::swap(m_front, m_back);
}

void ComposePass::set_radial_blur_radius(float radius) { m_back.radial_blur_radius = radius; }

void ComposePass::populate_frame(const cor::FrameContext& context,
                                 const glm::vec3& directional_light_color) {
  if (m_base_back.active) {
    cor::Key key = cor::Key::create<SaturationComponent>();
    std::vector<cor::EntityHandle> saturations = context.entity_mgr.get_entities(key);
    float saturation = 1;
    if (saturations.size() > 0) {
      saturation =
          context.entity_mgr.get_entity(saturations[0]).get<SaturationComponent>().saturation;
    }
    gfx::commands::BindUniformFloat* command =
        m_full_frame_pass->add_command<gfx::commands::BindUniformFloat>(0);
    command->location = m_shader->get_uniform_location("saturation");
    command->value = saturation;

    key = cor::Key::create<PlayerDamageVisualComponent>();
    std::vector<cor::EntityHandle> damages = context.entity_mgr.get_entities(key);
    glm::vec4 damage_color = glm::vec4(0, 0, 0, 1);
    if (damages.size() > 0) {
      damage_color =
          context.entity_mgr.get_entity(damages[0]).get<PlayerDamageVisualComponent>().color;
    }
    gfx::commands::BindUniformVec4* bind_damage_color =
        m_full_frame_pass->add_command<gfx::commands::BindUniformVec4>(0);
    bind_damage_color->location = m_shader->get_uniform_location("damage_color");
    bind_damage_color->vector = damage_color;

    gfx::commands::BindTexture* bind_texture_color =
        m_full_frame_pass->add_command<gfx::commands::BindTexture>(0);
    bind_texture_color->location = m_shader->get_uniform_location("color_sampler");
    bind_texture_color->texture = m_back.textures["color"];
    bind_texture_color->index = 0;

    // if (m_back.textures.find("ssao") != m_back.textures.end()) {
    //  gfx::commands::BindTexture* bind_texture_ssao =
    //      m_full_frame_pass->add_command<gfx::commands::BindTexture>(0);
    //  bind_texture_ssao->location = m_shader->get_uniform_location("ssao_sampler");
    //  bind_texture_ssao->texture = m_back.textures["ssao"];
    //  bind_texture_ssao->index = 1;
    //}
    bool god_ray_active = m_back.textures.find("god_ray") != m_back.textures.end();
    if (god_ray_active) {
      gfx::commands::BindTexture* bind_texture_god_ray =
          m_full_frame_pass->add_command<gfx::commands::BindTexture>(0);
      bind_texture_god_ray->location = m_shader->get_uniform_location("god_ray_sampler");
      bind_texture_god_ray->texture = m_back.textures["god_ray"];
      bind_texture_god_ray->index = 1;

      gfx::commands::BindUniformVec3* bind_god_ray_color =
          m_full_frame_pass->add_command<gfx::commands::BindUniformVec3>(0);
      bind_god_ray_color->location = m_shader->get_uniform_location("god_ray_color");
      bind_god_ray_color->vector = directional_light_color;
    }
    // bind bool uniform
    gfx::commands::BindUniformBool* bind_bool =
        m_full_frame_pass->add_command<gfx::commands::BindUniformBool>(0);
    bind_bool->location = m_shader->get_uniform_location("god_ray_active");
    bind_bool->value = god_ray_active;

    // glow texture
    bool glow_active = m_back.textures.find("glow") != m_back.textures.end();
    if (glow_active) {
      gfx::commands::BindTexture* bind_texture_glow =
          m_full_frame_pass->add_command<gfx::commands::BindTexture>(0);
      bind_texture_glow->location = m_shader->get_uniform_location("glow_sampler");
      bind_texture_glow->texture = m_back.textures["glow"];
      bind_texture_glow->index = 2;
    }
    // bind bool uniform
    bind_bool = m_full_frame_pass->add_command<gfx::commands::BindUniformBool>(0);
    bind_bool->location = m_shader->get_uniform_location("glow_active");
    bind_bool->value = glow_active;

    gfx::commands::BindUniformFloat* bind_blur_radius =
        m_full_frame_pass->add_command<gfx::commands::BindUniformFloat>(0);
    bind_blur_radius->location = m_shader->get_uniform_location("radial_blur_radius");
    bind_blur_radius->value = m_back.radial_blur_radius;
  }
}

void ComposePass::submit() {
  if (m_base_front.active) {
    if (m_front.framebuffer != nullptr) {
      m_front.framebuffer->bind();
    } else {
      // m_framebuffer->unbind();
      m_owned_framebuffer->bind();
    }
    gl::glClear(gl::GL_COLOR_BUFFER_BIT);
    gl::glDisable(gl::GL_DEPTH_TEST);
    gl::glColorMask(gl::GL_TRUE, gl::GL_TRUE, gl::GL_TRUE, gl::GL_TRUE);
    gl::glDisable(gl::GL_STENCIL_TEST);
    gl::glDisable(gl::GL_BLEND);
    m_full_frame_pass->submit();
    m_owned_framebuffer->unbind();
  }
}
void ComposePass::clear() {
  m_back.textures.clear();
  m_full_frame_pass->clear();
}

void ComposePass::set_texture(unsigned int texture, std::string name) {
  m_base_back.active = true;
  m_back.textures[name] = texture;
}

unsigned int ComposePass::get_texture() {
  if (m_back.framebuffer == nullptr) {
    return m_texture;
  } else {
    return m_full_frame_pass->get_fbo();
  }
}

void ComposePass::set_framebuffer(gfx::FrameBuffer* framebuffer) {
  m_back.framebuffer = framebuffer;
}

ComposePass::~ComposePass() {
  for (auto& tex : m_back.textures) {
    gl::glDeleteTextures(1, &tex.second);
  }
}

}  // namespace gmp