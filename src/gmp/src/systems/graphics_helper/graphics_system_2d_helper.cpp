#include "gmp/systems/graphics_helper/graphics_system_2d_helper.hpp"

#include "bse/assert.hpp"                             // for ASSERT
#include "bse/asset.hpp"                              // for Asset
#include "bse/file_path.hpp"                          // for FilePath
#include "bse/runtime_asset.hpp"                      // for RuntimeAsset
#include "cor/entity.tcc"                             // for ConstEntity
#include "cor/entity_manager.tcc"                     // for EntityManager
#include "cor/frame_context.hpp"                      // for FrameContext
#include "cor/key.hpp"                                // for Key
#include "gfx/command_bucket.hpp"                     // for CommandBucket
#include "gfx/key.hpp"                                // for Key, Key::(anon...
#include "gfx/material.hpp"                           // for Material
#include "gfx/shader.hpp"                             // for Shader
#include "gmp/components/graphic_text_component.hpp"  // for GraphicTextComp...
#include "gmp/components/texture_component.hpp"       // for TextureComponent
#include "gmp/components/transform_2d_component.hpp"  // for Transform2dComp...
#include <bse/edit.hpp>                               // for BSE_EDIT, BSE_E...
#include <gfx/command.hpp>                            // for BindUniformMat4
#include <gfx/mesh.hpp>                               // for Mesh
#include <gfx/text.hpp>
#include <gfx/text_cache.hpp>  // for TextCache
#include <gfx/texture.hpp>     // for Texture
namespace cor {
struct EntityHandle;
}

namespace gmp {

void* add_2d_transform(const gfx::Key& key, void* previous_command,
                       const Transform2dComponent& transform, gfx::CommandBucket* gbuffer_bucket) {
  static constexpr size_t viewport_width = 1920;   // TODO MH: hard coded
  static constexpr size_t viewport_height = 1080;  // TODO MH: hard coded

  gfx::commands::BindUniformMat4* bind_model_matrix =
      previous_command
          ? gbuffer_bucket->append_command<gfx::commands::BindUniformMat4>(previous_command, 0)
          : gbuffer_bucket->add_command<gfx::commands::BindUniformMat4>(key, 0);
  bind_model_matrix->location = key.shader->get_uniform_location("MODEL_MATRIX");
  bind_model_matrix->matrix =
      glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(1.f, 0.f, 0.f));

  glm::mat4 trans = glm::translate(
      glm::mat4(1.f),
      glm::vec3(transform.position.x * 2.f - 1.f, (1.f - transform.position.y) * 2.f - 1.f, 0.5f));
  glm::mat4 scale = glm::scale(glm::mat4(1.f), glm::vec3(transform.size.x, transform.size.y, 1));
  glm::mat4 rot =
      glm::rotate(glm::mat4(1.f), glm::radians(transform.rotation), glm::vec3(0.f, 0.f, -1.f));

  bind_model_matrix->matrix = trans * scale * rot * bind_model_matrix->matrix;

  return bind_model_matrix;
}

void* add_graphic_text(const gfx::Key& key, void* previous_command,
                       const Transform2dComponent& transform, const GraphicTextComponent& text,
                       gfx::CommandBucket* gbuffer_bucket,
                       bse::RuntimeAsset<gfx::TextCache>& text_cache) {
  gfx::TextCache::Entry text_cache_entry = text_cache->get_text({text.font, text.text});

  Transform2dComponent mod_transform = transform;
  float max_size = std::max(mod_transform.size.x, mod_transform.size.y);
  mod_transform.size = {max_size * 1080.f / 1920.f, max_size};

  gfx::commands::BindUniformMat4* bind_model_matrix =
      (gfx::commands::BindUniformMat4*)add_2d_transform(key, previous_command, mod_transform,
                                                        gbuffer_bucket);

  if (transform.centering == Transform2dComponent::Centering::CENTER) {
    glm::vec2 center = text_cache_entry.top_left + text_cache_entry.bounds / 2.f;
    bind_model_matrix->matrix = bind_model_matrix->matrix *
                                glm::translate(glm::mat4(1.f), glm::vec3(-center.x, 0, -center.y));
  } else if (transform.centering == Transform2dComponent::Centering::RIGHT) {
    bind_model_matrix->matrix =
        bind_model_matrix->matrix *
        glm::translate(glm::mat4(1.f),
                       glm::vec3(-(text_cache_entry.top_left.x + text_cache_entry.bounds.x), 0, 0));
  } else if (transform.centering == Transform2dComponent::Centering::LEFT) {
    bind_model_matrix->matrix =
        bind_model_matrix->matrix *
        glm::translate(glm::mat4(1.f), glm::vec3(-text_cache_entry.top_left.x, 0, 0));
  }

  gfx::commands::BindUniformVec3* bind_color =
      gbuffer_bucket->append_command<gfx::commands::BindUniformVec3>(bind_model_matrix, 0);
  bind_color->location = key.shader->get_uniform_location("COLOR");
  bind_color->vector = glm::vec3(text.color) / 255.f;

  bind_color = gbuffer_bucket->append_command<gfx::commands::BindUniformVec3>(bind_color, 0);
  bind_color->location = key.shader->get_uniform_location("OUTLINE_COLOR");
  bind_color->vector =
      glm::vec3((text.outline_thickness < 0.00001f) ? text.color : text.outline_color) / 255.f;

  gfx::commands::BindUniformFloat* bind_thickness =
      gbuffer_bucket->append_command<gfx::commands::BindUniformFloat>(bind_color, 0);
  bind_thickness->location = key.shader->get_uniform_location("THICKNESS");
  bind_thickness->value = text.thickness;

  bind_thickness =
      gbuffer_bucket->append_command<gfx::commands::BindUniformFloat>(bind_thickness, 0);
  bind_thickness->location = key.shader->get_uniform_location("OUTLINE_THICKNESS");
  bind_thickness->value = text.outline_thickness;

  gfx::commands::BindTexture* bind_texture =
      gbuffer_bucket->append_command<gfx::commands::BindTexture>(bind_thickness, 0);
  bind_texture->location = key.shader->get_uniform_location("TEXTURE");
  bind_texture->texture = text_cache_entry.texture;
  bind_texture->index = 0;

  gfx::commands::DrawArrays* draw_arrays =
      gbuffer_bucket->append_command<gfx::commands::DrawArrays>(bind_texture, 0);
  draw_arrays->vertex_count = text_cache_entry.vertex_count;
  draw_arrays->vao = text_cache_entry.vao;
  draw_arrays->triangle_mode = text_cache_entry.MODE;

  return draw_arrays;
}

void add_2d(const cor::FrameContext& context, gfx::CommandBucket* gbuffer_bucket,
            bse::RuntimeAsset<gfx::TextCache>& text_cache) {
  gfx::Key command_key;
  command_key.shader = bse::Asset<gfx::Shader>("assets/shader/2d_text_shader.frag"_fp);
  command_key.shader_index = command_key.shader.get_id();
  command_key.blend_type = true;

  cor::Key query_key = cor::Key::create<Transform2dComponent, GraphicTextComponent>();
  for (const cor::EntityHandle& entity_handle : context.entity_mgr.get_entities(query_key)) {
    cor::ConstEntity entity = context.entity_mgr.get_entity(entity_handle);

    add_graphic_text(command_key, nullptr, entity.get<Transform2dComponent>(),
                     entity.get<GraphicTextComponent>(), gbuffer_bucket, text_cache);
  }

  command_key = gfx::Key();
  command_key.shader = bse::Asset<gfx::Shader>("assets/shader/2d_textured_shader.frag"_fp);
  command_key.shader_index = command_key.shader.get_id();
  command_key.blend_type = true;

  // static std::vector<gfx::Texture::Rgba> icon_pixels = {
  //    {255, 0, 255, 255},
  //    {0, 0, 0, 255},
  //    {0, 0, 0, 255},
  //    {255, 0, 255, 255},
  //};
  // static gfx::Texture icon(icon_pixels.data(), 2, 2);

  query_key = cor::Key::create<Transform2dComponent, TextureComponent>();
  for (const cor::EntityHandle& entity_handle : context.entity_mgr.get_entities(query_key)) {
    cor::ConstEntity entity = context.entity_mgr.get_entity(entity_handle);

    Transform2dComponent transform = entity.get<Transform2dComponent>();
    command_key.depth = transform.position.z;

    void* command = nullptr;
    command = add_2d_transform(command_key, command, transform, gbuffer_bucket);

    bse::Asset<gfx::Texture> texture = entity.get<TextureComponent>().texture;

    if (!texture) {
      texture = bse::Asset<gfx::Texture>("assets/texture/fallback.png");
      ASSERT(texture) << "Failed to load fallback texture.";
    }

    gfx::commands::BindTexture* bind_texture =
        gbuffer_bucket->append_command<gfx::commands::BindTexture>(command, 0);
    bind_texture->location = command_key.shader->get_uniform_location("textureSampler");
    bind_texture->texture = texture->get_id();
    bind_texture->index = 0;

    command = bind_texture;

    gfx::commands::DrawIndexed* draw_indexed =
        gbuffer_bucket->append_command<gfx::commands::DrawIndexed>(command, 0);
    draw_indexed->vertex_count = gfx::Mesh::texture_quad().get_vertex_count();
    gfx::VertexFlags flags;
    flags.set(bse::VertexTypes::POSITION);
    flags.set(bse::VertexTypes::UV);
    draw_indexed->vao = gfx::Mesh::texture_quad().get_buffers(flags).m_vao;
    draw_indexed->triangle_mode = gl::GL_TRIANGLES;
  }
}

}  // namespace gmp