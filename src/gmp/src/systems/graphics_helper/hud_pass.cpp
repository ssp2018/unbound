#include "gmp/systems/graphics_helper/hud_pass.hpp"

#include "gfx/command_bucket.hpp"
#include "gfx/text_cache.hpp"
#include "gmp/systems/graphics_helper/graphics_system_2d_helper.hpp"
namespace bse {
class StackAllocator;
}
namespace cor {
struct FrameContext;
}

namespace gmp {

HUDPass::HUDPass(bse::StackAllocator* front, bse::StackAllocator* back, float width, float height)
    : RenderPass(width, height) {
  m_command_bucket = std::make_unique<gfx::CommandBucket>(front, back, width, height);
}

void HUDPass::swap() {
  RenderPass::swap();
  m_command_bucket->swap();
}
void HUDPass::prepare_for_frame() { m_command_bucket->prepare_for_new_frame(); }

void HUDPass::sort() { m_command_bucket->sort(); }
void HUDPass::submit() {
  if (m_front.framebuffer != nullptr) {
    m_front.framebuffer->bind();
    gl::glEnable(gl::GL_BLEND);
    m_command_bucket->submit();
    m_front.framebuffer->unbind();
  } else {
    m_command_bucket->unbind_fbo();
    gl::glEnable(gl::GL_BLEND);
    m_command_bucket->submit();
  }
  gl::glDisable(gl::GL_BLEND);
}
void HUDPass::clear() { m_command_bucket->clear(); }

void HUDPass::populate_frame(const cor::FrameContext& context) {
  add_2d(context, m_command_bucket.get(), m_text_cache);
}

void HUDPass::set_framebuffer(gfx::FrameBuffer* framebuffer) { m_back.framebuffer = framebuffer; }

}  // namespace gmp