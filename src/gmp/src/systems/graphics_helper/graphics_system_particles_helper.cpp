#include "gmp/systems/graphics_helper/graphics_system_particles_helper.hpp"

#include "bse/asset.hpp"                           // for Asset
#include "cor/entity.tcc"                          // for ConstEntity
#include "cor/entity_handle.hpp"                   // for EntityHandle
#include "cor/entity_manager.tcc"                  // for EntityManager
#include "cor/frame_context.hpp"                   // for FrameContext
#include "cor/key.hpp"                             // for Key
#include "gfx/command_bucket.hpp"                  // for CommandBucket
#include "gfx/key.hpp"                             // for Key, Key::(anonymo...
#include "gfx/shader.hpp"                          // for Shader
#include "gfx/texture.hpp"                         // for Texture
#include "gmp/components/particles_component.hpp"  // for ParticlesComponent
#include "gmp/components/transform_component.hpp"  // for Transform, Transfo...
#include <gfx/command.hpp>                         // for BindTexture, DrawI...
// #include <gfx/particle_system_helper.hpp>          // for update_buffer
namespace gmp {}  // namespace gmp