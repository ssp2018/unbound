#include <cor/entity.tcc>
#include <gmp/components/animation_component.hpp>
#include <gmp/components/model_component.hpp>
#include <gmp/components/transform_component.hpp>
#include <gmp/systems/animation_system.hpp>
#include <gmp/systems/graphics_helper/utilities.hpp>

namespace gmp {

unsigned int get_mesh_vao(cor::ConstEntity entity, const bse::Asset<gfx::Mesh>& mesh,
                          gfx::VertexFlags flags) {
  // ASSERT(entity.has<gmp::ModelComponent>())
  //     << "Tried to get the VAO of an entity without a ModelComponent\n";

  // const gmp::ModelComponent& mc = entity;

  // If entity has skeletal animation, get its custom VAO
  if (entity.has<AnimationComponent>()) {
    const AnimationComponent& ac = entity;

    const AnimationSystem::AnimatedMeshData* mesh_data = ac.data.mesh_data;

    if (mesh_data && mesh_data->buffers.m_vao >= 0) {
      return mesh_data->buffers.m_vao;
    } else {
      return mesh->get_buffers(flags).m_vao;
    }
  }
  // Else, get VAO from mesh
  else {
    return mesh->get_buffers(flags).m_vao;
  }
}
}  // namespace gmp