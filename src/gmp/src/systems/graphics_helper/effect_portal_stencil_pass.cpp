
#include "gmp/systems/graphics_helper/effect_portal_stencil_pass.hpp"

#include <bse/mesh/mesh_loader.hpp>
#include <gfx/mesh.hpp>
#include <gfx/model.hpp>

namespace gmp {

EffectPortalStencilPass::EffectPortalStencilPass(bse::StackAllocator* front,
                                                 bse::StackAllocator* back, float width,
                                                 float height)
    : RenderPass(width, height) {
  m_stencil_bucket = std::make_unique<gfx::CommandBucket>(front, back, m_width, m_height);
  m_framebuffer = std::make_unique<gfx::FrameBuffer>(m_width, m_height, true, true);
  m_framebuffer->create();

  // setup rendervars
  m_shader = bse::Asset<gfx::Shader>(bse::ASSETS_ROOT / "shader/stencil.shd"_fp);
  m_model = bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/box.fbx"_fp);
  m_model_matrix_location = m_shader->get_uniform_location("MODEL_MATRIX");
  m_view_projection_location = m_shader->get_uniform_location("VIEW_PROJECTION_MATRIX");
}

// allocate what is needed from the stack
void EffectPortalStencilPass::prepare_for_frame() {
  m_stencil_bucket->prepare_for_new_frame();
  m_base_back.active = false;
}

void EffectPortalStencilPass::swap() {
  RenderPass::swap();
  m_stencil_bucket->swap();
  std::swap(m_front, m_back);
}
// sort the rendercalls
void EffectPortalStencilPass::sort() { m_stencil_bucket->sort(); }
// submit the rendercalls
void EffectPortalStencilPass::submit() {
  if (m_base_front.active) {
    if (m_front.other_buffer != nullptr) {
      m_front.other_buffer->bind();
    } else {
      m_framebuffer->bind();
    }
    // gl::glViewport(0, 0, 1280 * 0.5, 720 * 0.5);
    gl::glEnable(gl::GL_STENCIL_TEST);
    gl::glStencilFunc(gl::GL_ALWAYS, 1, 0xFF);  // Always pass stencil test
    gl::glStencilOp(
        gl::GL_KEEP, gl::GL_KEEP,
        gl::GL_REPLACE);      // increment tencil value only if both stencil and depth test succeeds
    gl::glStencilMask(0xFF);  // Write to stencil buffer
    gl::glDepthMask(gl::GL_FALSE);  // Don't write to depth buffer
    gl::glClear(gl::GL_STENCIL_BUFFER_BIT);
    gl::glDepthFunc(gl::GL_LEQUAL);
    m_stencil_bucket->submit();
    m_framebuffer->unbind();
    gl::glDisable(gl::GL_STENCIL_TEST);
    gl::glDepthMask(gl::GL_TRUE);
  }
}
// reset the pass to make it ready for a new frame
void EffectPortalStencilPass::clear() { m_stencil_bucket->clear(); }

// return the texture by name
unsigned int EffectPortalStencilPass::get_fbo() { return m_framebuffer->get_id(); }

void EffectPortalStencilPass::set_fbo(gfx::FrameBuffer* other) { m_back.other_buffer = other; }

// populate this pass with framedata
void EffectPortalStencilPass::populate_frame(const cor::FrameContext& context,
                                             const ProjectionComponent& camera,
                                             const glm::mat4& v_matrix,
                                             const std::vector<Portal>& active_portals,
                                             bse::Flags<LightEffects::COUNT>& active_effects) {
  m_base_back.active = true;
  for (auto& portal : active_portals) {
    // calculate stencilvalue
    bse::Flags<LightEffects::COUNT> active = portal.flags ^ active_effects;
    int stencil_value = 0;
    if (active.test(LightEffects::GOD_RAYS)) {
      stencil_value += 0b10;
    }
    if (active.test(LightEffects::GLOW)) {
      stencil_value += 0b01;
    }
    // create key // TODO: add distance
    gfx::Key shadow_key;
    shadow_key.shader = m_shader;
    shadow_key.shader_index = m_shader.get_id();
    // shadow_key.depth = glm::distance2(aabb.position, cam_pos);
    shadow_key.blend_type = false;
    // add modelmatrix
    gfx::commands::BindUniformMat4* bind_model_matrix_cp =
        m_stencil_bucket->add_command<gfx::commands::BindUniformMat4>(shadow_key, 0);
    bind_model_matrix_cp->location = m_model_matrix_location;
    bind_model_matrix_cp->matrix =
        glm::scale(glm::rotate(glm::translate(glm::mat4(1), portal.aabb.position),
                               portal.rotation_z, glm::vec3(0, 0, 1)),
                   portal.aabb.size);
    void* latest_command = bind_model_matrix_cp;

    gfx::commands::BindUniformMat4* bind_vp_matrix_cp =
        m_stencil_bucket->append_command<gfx::commands::BindUniformMat4>(latest_command, 0);
    bind_vp_matrix_cp->location = m_view_projection_location;
    bind_vp_matrix_cp->matrix = camera.projection_matrix * v_matrix;
    latest_command = bind_vp_matrix_cp;

    gfx::commands::SetStencilRef* set_stencil_ref =
        m_stencil_bucket->append_command<gfx::commands::SetStencilRef>(latest_command, 0);
    set_stencil_ref->value = stencil_value;
    latest_command = set_stencil_ref;

    gfx::VertexFlags shadow_flags;
    shadow_flags.set(bse::VertexTypes::POSITION);

    gfx::commands::DrawIndexed* draw_indexed =
        m_stencil_bucket->append_command<gfx::commands::DrawIndexed>(latest_command, 0);
    draw_indexed->vertex_count = m_model->get_mesh()->get_vertex_count();
    draw_indexed->triangle_mode = gl::GL_TRIANGLES;

    draw_indexed->vao = m_model->get_mesh()->get_buffers(shadow_flags).m_vao;
  }
}

};  // namespace gmp