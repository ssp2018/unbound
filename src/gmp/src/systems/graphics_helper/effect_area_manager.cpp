#include "gmp/systems/graphics_helper/effect_area_manager.hpp"

#include <bse/edit.hpp>
#include <bse/reflect.hpp>

REFLECT((glm::vec3), x, y, z)

namespace gmp {

std::vector<Portal> EffectArea::get_portal_effects(const gfx::FrustumShape &camera_frustum) {
  std::vector<Portal> res;
  for (auto portal : portals) {
    if (camera_frustum.aabb_intersect(portal.aabb)) {
      res.push_back(portal);
    }
  }
  return res;
}

EffectAreaManager::EffectAreaManager() {
  m_default_area.flags.set(LightEffects::DIRECTIONAL_LIGHT);
  m_default_area.flags.set(LightEffects::DIRECTIONAL_SHADOWS);
  m_default_area.flags.set(LightEffects::GOD_RAYS);
  m_default_area.flags.set(LightEffects::SSAO);
  m_default_area.aabb.size = glm::vec3(999999);
  m_default_area.aabb.position = glm::vec3(0);
  m_default_area.radius = 9999999;
  m_default_area.low = -9999;
  m_default_area.high = 9999;
  Portal portal;
  portal.flags.set(LightEffects::GLOW);
  portal.flags.set(LightEffects::SSAO);
  portal.aabb.position = glm::vec3(-555, -408, 125);
  portal.aabb.size = glm::vec3(70, 0, 70);
  portal.rotation_z = 0.6f;
  m_default_area.portals.push_back(portal);

  // add area
  EffectArea area;
  area.aabb.position = glm::vec3(-81.5, -2.5, 100);
  area.aabb.size = glm::vec3(44, 42.5, 50);
  area.flags.set(LightEffects::GLOW);
  area.flags.set(LightEffects::SSAO);
  area.radius = 84.2;
  area.low = 90;
  area.high = 170;
  area.center = glm::vec2(-506.9, -478.2);
  portal = Portal();
  portal.flags = m_default_area.flags;
  portal.aabb.position = glm::vec3(-555, -408, 125);
  portal.aabb.size = glm::vec3(70, 0, 70);
  portal.rotation_z = 0.6f;
  area.portals.push_back(portal);
  m_effect_areas.push_back(area);
}

const EffectArea EffectAreaManager::get_effect_area(glm::vec3 position, glm::vec3 direction) {
  BSE_EDIT_NAMED(m_effect_areas[0].radius, "radius");
  BSE_EDIT_NAMED(m_effect_areas[0].center.x, "cx");
  BSE_EDIT_NAMED(m_effect_areas[0].center.y, "cy");
  for (EffectArea &area : m_effect_areas) {
    //  aabb test
    // if (area.aabb.is_point_inside(position)) {
    //   return area;
    // }
    if (area.low < position.z && position.z < area.high) {
      if (glm::distance(glm::vec2(position.x, position.y), area.center) < area.radius) {
        return area;
      }
    }
  }
  return m_default_area;
}
bse::Flags<LightEffects::COUNT> EffectAreaManager::get_all_combined_effects(
    bse::Flags<LightEffects::COUNT> flags, std::vector<Portal> active_portals) {
  bse::Flags<LightEffects::COUNT> res = flags;
  for (auto portal : active_portals) {
    res |= portal.flags;
  }
  return res;
}
bse::Flags<LightEffects::COUNT> EffectAreaManager::needs_stencil_flags(
    bse::Flags<LightEffects::COUNT> flags, std::vector<Portal> active_portals) {
  bse::Flags<LightEffects::COUNT> res;
  for (auto portal : active_portals) {
    res |= (portal.flags ^ flags);
  }
  return res;
}

}  // namespace gmp