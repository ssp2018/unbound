#include "gmp/systems/graphics_helper/csm_pass.hpp"

#include "bse/asset.hpp"                  // for Asset
#include "cor/entity.tcc"                 // for ConstEn...
#include "cor/entity_handle.hpp"          // for EntityH...
#include "cor/entity_manager.tcc"         // for EntityM...
#include "cor/frame_context.hpp"          // for FrameCo...
#include "cor/key.hpp"                    // for Key
#include "gfx/buckets/shadow_bucket.hpp"  // for ShadowB...
#include "gfx/command.hpp"                // for BindUni...
#include "gfx/helper.hpp"                 // for calcuat...
#include "gfx/key.hpp"                    // for Key
#include "gfx/material.hpp"               // for Material
#include "gfx/mesh.hpp"                   // for AABB, Mesh
#include "gfx/model.hpp"                  // for Model
#include "gmp/components/animation_component.hpp"
#include "gmp/components/character_controller_component.hpp"  // for Charact...
#include "gmp/components/dynamic_component.hpp"               // for Dynamic...
#include "gmp/components/model_component.hpp"                 // for ModelCo...
#include "gmp/components/static_component.hpp"                // for StaticC...
#include "gmp/components/transform_component.hpp"             // for Transform
#include "gmp/components/transforms_component.hpp"            // for Transfo...
#include <gfx/frustum_shape.hpp>                              // for Frustum...
#include <gmp/components/name_component.hpp>
#include <gmp/systems/animation_system.hpp>
#include <gmp/systems/graphics_helper/utilities.hpp>

namespace bse {
class StackAllocator;
}
namespace gmp {
struct AnimationComponent;
}

namespace gmp {

CSMPass::CSMPass(bse::StackAllocator* front, bse::StackAllocator* back, float size)
    : RenderPass(size, size), m_front(new Queue()), m_back(new Queue()) {
  m_world_timer.start();
  m_buckets.resize(5);
  // m_buckets[0].reset(new gfx::ShadowBucket(front, back, size));
  // m_buckets[1].reset(new gfx::ShadowBucket(front, back, size));
  // m_buckets[2].reset(new gfx::ShadowBucket(front, back, size));
  // m_buckets[3].reset(new gfx::ShadowBucket(front, back, size));
  // m_buckets[4].reset(new gfx::ShadowBucket(front, back, size / 2));
  m_buckets[0].reset(new gfx::ShadowBucket(front, back, 2048));
  m_buckets[1].reset(new gfx::ShadowBucket(front, back, 2048));
  m_buckets[2].reset(new gfx::ShadowBucket(front, back, 1024));
  m_buckets[3].reset(new gfx::ShadowBucket(front, back, 256));
  m_buckets[4].reset(new gfx::ShadowBucket(front, back, 256));
  m_world_index = 4;
  m_back->matrices.resize(m_buckets.size());
  m_back->depths.resize(m_buckets.size());
  set_splits({15, 50, 300, 1000});
  // *m_front = *m_back;
  m_front->matrices.resize(m_buckets.size());
  m_front->depths.resize(m_buckets.size());

  for (int i = 0; i < 25; i++) {
    m_back->matrix_ubos[i].create(sizeof(glm::mat4) * 200, nullptr);
    m_front->matrix_ubos[i].create(sizeof(glm::mat4) * 200, nullptr);
  }

}  // namespace gmp

void CSMPass::prepare_for_frame() {
  m_time_since_world_render += m_world_timer.reset();

  if (m_time_since_world_render > 2.f) {
    m_back->do_render_world = true;
    m_time_since_world_render = 0.f;
  } else {
    m_back->do_render_world = false;
  }
  for (std::unique_ptr<gfx::ShadowBucket>& bucket : m_buckets) {
    bucket->prepare_for_new_frame();
  }

  m_base_back.active = false;
}

void CSMPass::sort() {
  if (m_base_back.active) {
    for (std::unique_ptr<gfx::ShadowBucket>& bucket : m_buckets) {
      bucket->sort();
    }
  }
}

void CSMPass::swap() {
  RenderPass::swap();
  std::swap(m_front, m_back);
  for (std::unique_ptr<gfx::ShadowBucket>& bucket : m_buckets) {
    bucket->swap();
  }
}
void CSMPass::submit() {
  if (m_base_front.active) {
    size_t end = m_front->do_render_world ? m_buckets.size() : m_world_index;
    for (size_t i = 0; i < end; i++) {
      gfx::ShadowBucket& bucket = *m_buckets[i];
      for (auto& entity_data_it : m_front->ubo_entities[i]) {
        if (!entity_data_it.second.matrices.empty()) {
          m_front->matrix_ubos[entity_data_it.second.ubo_index].update(
              entity_data_it.second.matrices.size() * sizeof(glm::mat4),
              &entity_data_it.second.matrices[0]);
        }
      }
      bucket.submit();
    }

    // for (std::unique_ptr<gfx::ShadowBucket>& bucket : m_buckets) {
    //   bucket->submit();
    // }
  }
}
void CSMPass::clear() {
  if (m_base_back.active) {
    int j = 0;
    for (int j = 0; j < 5; j++) {
      for (auto& [key, data] : m_back->ubo_entities[j]) {
        data.matrices.clear();
      }
    }
    for (std::unique_ptr<gfx::ShadowBucket>& bucket : m_buckets) {
      bucket->clear();
    }
  }
}

float* CSMPass::get_shadow_matrices() { return &m_back->matrices[0][0][0]; }

const glm::mat4& CSMPass::get_matrix(int index) const {
  ASSERT(index >= 0 && index < m_back->matrices.size());
  return m_back->matrices[index];
}
unsigned int CSMPass::get_texture(int index) {
  ASSERT(index >= 0 && index < m_buckets.size());
  return m_buckets[index]->get_texture(0);
}

void CSMPass::populate_frame(const cor::FrameContext& context, const glm::mat4& p_matrix,
                             const glm::mat4& v_matrix, const glm::vec3& direction,
                             const glm::vec3& cam_pos) {
  m_base_back.active = true;

  int used_ubos = 0;
  std::array<std::map<int, int>, 5> map_model_id_to_ubo;

  for (size_t i = 0; i < 4; i++) {
    m_buckets[i]->fit_camera(p_matrix, v_matrix, direction, m_back->splits[i],
                             m_back->splits[i + 1]);
  }

  cor::Key rim_key = cor::Key::create<TransformComponent, NameComponent>();
  std::vector<glm::vec3> world_rim;
  for (const cor::EntityHandle& handle : context.entity_mgr.get_entities(rim_key)) {
    cor::ConstEntity entity = context.entity_mgr.get_entity(handle);
    if (entity.get<NameComponent>().name == "world_rim") {
      world_rim.push_back(entity.get<TransformComponent>().transform.get_position());
    }
  }

  for (size_t i = 4; i < 5; i++) {
    m_buckets[i]->fit_points(world_rim, direction);
  }

  std::vector<gfx::FrustumShape> shadow_shapes(m_buckets.size());
  for (size_t i = 0; i < m_buckets.size(); i++) {
    m_back->matrices[i] = m_buckets[i]->get_shadow_matrix();
    m_back->depths[i] = m_buckets[i]->get_shadow_depth();
    shadow_shapes[i] = gfx::FrustumShape(m_buckets[i]->get_cull_matrix());
  }

  std::vector<cor::EntityManager::EntityHandleWithComponent<TransformComponent, ModelComponent>>
      models =
          context.entity_mgr.get_entities_with_components<TransformComponent, ModelComponent>();
  size_t bucket_end = m_back->do_render_world ? m_buckets.size() : m_world_index;

  for (auto& entity_handle : models) {
    cor::ConstEntity entity = context.entity_mgr.get_entity(entity_handle.handle);
    const ModelComponent& mc = entity_handle.comp2;
    if (!mc.asset->get_shadow_material().is_valid()) {
      continue;
    }
    const TransformComponent& tc = entity_handle.comp1;

    // do culling check...
    gfx::AABB aabb;
    if (entity.has<StaticComponent>()) {
      const StaticComponent& sc = entity;
      aabb = sc.contents.get_aabb();
    } else if (entity.has<DynamicComponent>()) {
      const DynamicComponent& dc = entity;
      aabb = dc.contents.get_aabb();
    } else if (entity.has<CharacterControllerComponent>()) {
      const CharacterControllerComponent& ccc = entity;
      aabb = ccc.contents.get_aabb();
    } else {
      aabb = mc.asset->get_mesh()->get_aabb();
      aabb.size *= tc.transform.get_scale();
      aabb.position += tc.transform.get_position();
    }

    for (size_t i = 0; i < bucket_end; i++) {
      gfx::ShadowBucket& bucket = *m_buckets[i];
      if (!shadow_shapes[i].aabb_intersect(aabb)) {
        continue;
      }

      int model_id = mc.asset.get_id();

      EntityData& data = m_back->ubo_entities[i][model_id];
      if (data.matrices.empty()) {
        data.entity = entity;
        data.tc = &tc;
        data.mc = &mc;
        data.aabb = aabb;
      }
      data.matrices.push_back(tc.transform.get_model_matrix());
    }
  }
  int ubo_index = 0;
  for (size_t i = 0; i < bucket_end; i++) {
    gfx::ShadowBucket& bucket = *m_buckets[i];
    for (auto& entity_data_it : m_back->ubo_entities[i]) {
      auto& entity_data = entity_data_it.second;
      if (entity_data.matrices.empty()) {
        continue;
      }

      gfx::Key shadow_key;
      shadow_key.shader = entity_data.mc->asset->get_shadow_material()->get_shader();
      shadow_key.shader_index = shadow_key.shader.get_id();
      shadow_key.depth = glm::distance2(entity_data.aabb.position, cam_pos);
      shadow_key.blend_type = false;

      // add modelmatrix
      entity_data.ubo_index = ubo_index;
      gfx::commands::BindUniformBuffer* bind_model_matrix_ubo =
          bucket.add_command<gfx::commands::BindUniformBuffer>(shadow_key, 0);
      bind_model_matrix_ubo->location =
          entity_data.mc->asset->get_shadow_material()->model_matrix_location;
      bind_model_matrix_ubo->id = m_back->matrix_ubos[ubo_index].get_handle();
      bind_model_matrix_ubo->program = shadow_key.shader->get_shader_handle();
      void* latest_command = bind_model_matrix_ubo;

      // gfx::commands::BindUniformMat4* bind_model_matrix_cp =
      //     bucket.add_command<gfx::commands::BindUniformMat4>(shadow_key, 0);
      // bind_model_matrix_cp->location =
      //     entity_data.mc->asset->get_shadow_material()->model_matrix_location;
      // bind_model_matrix_cp->matrix = entity_data.tc->transform.get_model_matrix();
      // void* latest_command = bind_model_matrix_cp;

      bse::Flags<bse::VertexTypes::COUNT> shadow_flags =
          entity_data.mc->asset->get_shadow_material()->get_required_vertex_data();

      if (entity_data.matrices.size() >= 5) {
        gfx::commands::DrawInstancedIndexed* draw_instanced_indexed =
            bucket.append_command<gfx::commands::DrawInstancedIndexed>(latest_command, 0);
        draw_instanced_indexed->vertex_count =
            entity_data.mc->asset->get_mesh()->get_vertex_count();
        draw_instanced_indexed->vao =
            get_mesh_vao(entity_data.entity, entity_data.mc->asset->get_mesh(), shadow_flags);
        draw_instanced_indexed->instance_count = entity_data.matrices.size();
      } else {
        gfx::commands::DrawIndexed* draw_indexed =
            bucket.append_command<gfx::commands::DrawIndexed>(latest_command, 0);
        draw_indexed->vertex_count = entity_data.mc->asset->get_mesh()->get_vertex_count();
        draw_indexed->triangle_mode = gl::GL_TRIANGLES;

        draw_indexed->vao =
            get_mesh_vao(entity_data.entity, entity_data.mc->asset->get_mesh(), shadow_flags);
      }
      ubo_index++;
    }
  }
}

void CSMPass::set_splits(const std::vector<float>& splits) {
  m_back->splits = {0.1f};
  m_back->splits.insert(m_back->splits.end(), splits.begin(), splits.end());

  ASSERT(m_back->splits.size() == m_buckets.size());
}

float* CSMPass::get_shadow_depths() { return &m_back->depths[0]; }

}  // namespace gmp