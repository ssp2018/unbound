#include "gmp/systems/graphics_helper/glow_pass.hpp"

#include "gfx/command.hpp"                   // for BindTexture, BindUniform...
#include "gfx/full_frame_gaussian_pass.hpp"  // for FullFrameGaussianPass
#include "gfx/full_frame_pass.hpp"           // for FullFramePass
#include "gfx/shader.hpp"                    // for Shader
#include <bse/edit.hpp>                      // for BSE_EDIT

namespace gmp {

GlowPass::GlowPass(bse::StackAllocator* front, bse::StackAllocator* back, float width, float height)
    : RenderPass(width, height) {
  m_internal_framebuffer = std::make_unique<gfx::FrameBuffer>(width, height, true, true);
  m_internal_texture = m_internal_framebuffer->add_texture(gfx::TextureType::RGBA);
  m_internal_framebuffer->create();

  m_gaussian_framebuffer = std::make_unique<gfx::FrameBuffer>(width, height, true, true);
  m_gaussian_texture = m_gaussian_framebuffer->add_texture(gfx::TextureType::RGBA);
  m_gaussian_framebuffer->create();
}

void GlowPass::swap() {
  RenderPass::swap();
  std::swap(m_front, m_back);
}
void GlowPass::prepare_for_frame() { m_base_back.active = false; }

void GlowPass::sort() {}
void GlowPass::submit() {
  if (m_base_front.active) {
    // do gaussian pass
    bse::Asset<gfx::Shader> gaussian_shader =
        bse::Asset<gfx::Shader>("assets/shader/gaussian.frag");
    m_internal_framebuffer->bind();
    gl::glDepthFunc(gl::GL_ALWAYS);
    if (m_front.stencil_active) {
      gl::glEnable(gl::GL_STENCIL_TEST);
      gl::glBindFramebuffer(gl::GL_READ_FRAMEBUFFER, m_front.stencil);
      // setup gaussian framebuffer setencil
      gl::glBlitFramebuffer(0, 0, m_front.stencil_width, m_front.stencil_height, 0, 0, m_width,
                            m_height, gl::GL_STENCIL_BUFFER_BIT, gl::GL_NEAREST);
      gl::glStencilMask(0x00);        // Don't write anything to stencil buffer
      gl::glDepthMask(gl::GL_FALSE);  // Don't write to depth buffer
      gl::glStencilOp(gl::GL_KEEP, gl::GL_KEEP, gl::GL_KEEP);
      if (m_front.inside_glow) {
        gl::glStencilFunc(gl::GL_NOTEQUAL, 1, 0b00000001);  // Pass test if stencil value is not 1
      } else {
        gl::glStencilFunc(gl::GL_EQUAL, 1, 0b00000001);  // Pass test if stencil value is 1
      }
    } else {
      gl::glDisable(gl::GL_STENCIL_TEST);

      gl::glDepthFunc(gl::GL_ALWAYS);
    }

    gl::glViewport(0, 0, m_width, m_height);
    gl::glClear(gl::GL_COLOR_BUFFER_BIT);

    // render first gaussian
    gl::glUseProgram(gaussian_shader->get_shader_handle());

    // bind input_texture
    gl::glUniform1i(gaussian_shader->get_uniform_location("texture_sampler"), 0);
    gl::glActiveTexture(gl::GL_TEXTURE0);
    gl::glBindTexture(gl::GL_TEXTURE_2D, m_front.in_texture);

    gl::glUniform1f(gaussian_shader->get_uniform_location("radius"), m_front.blur_radius);
    gl::glUniform2f(gaussian_shader->get_uniform_location("direction"), 1, 0);

    // render quad
    gfx::VertexFlags flags;
    flags.set(bse::VertexTypes::POSITION);
    flags.set(bse::VertexTypes::UV);
    gl::glBindVertexArray(gfx::Mesh::fullscreen_quad().get_buffers(flags).m_vao);

    // Draw the triangles !
    glDrawElements(gl::GL_TRIANGLES,                                              // mode
                   (gl::GLsizei)gfx::Mesh::fullscreen_quad().get_vertex_count(),  // count
                   gl::GL_UNSIGNED_INT,                                           // type
                   (void*)0  // element array buffer offset
    );

    // render second gaussian
    m_gaussian_framebuffer->bind();
    if (m_front.stencil_active) {
      // setup gaussian framebuffer setencil
      gl::glBindFramebuffer(gl::GL_READ_FRAMEBUFFER, m_front.stencil);
      gl::glBlitFramebuffer(0, 0, m_front.stencil_width, m_front.stencil_height, 0, 0, m_width,
                            m_height, gl::GL_STENCIL_BUFFER_BIT, gl::GL_NEAREST);
      gl::glStencilMask(0x00);        // Don't write anything to stencil buffer
      gl::glDepthMask(gl::GL_FALSE);  // Don't write to depth buffer
      gl::glStencilOp(gl::GL_KEEP, gl::GL_KEEP, gl::GL_KEEP);
      if (m_front.inside_glow) {
        gl::glStencilFunc(gl::GL_NOTEQUAL, 1, 0b00000001);  // Pass test if stencil value is not 1
      } else {
        gl::glStencilFunc(gl::GL_EQUAL, 1, 0b00000001);  // Pass test if stencil value is 1
      }
    }

    gl::glClear(gl::GL_COLOR_BUFFER_BIT);

    gl::glUseProgram(gaussian_shader->get_shader_handle());

    // bind input_texture
    gl::glUniform1i(gaussian_shader->get_uniform_location("texture_sampler"), 0);
    gl::glActiveTexture(gl::GL_TEXTURE0);
    gl::glBindTexture(gl::GL_TEXTURE_2D, m_internal_texture);

    gl::glUniform1f(gaussian_shader->get_uniform_location("radius"), m_front.blur_radius);
    gl::glUniform2f(gaussian_shader->get_uniform_location("direction"), 0, 1);

    // render quad
    gl::glBindVertexArray(gfx::Mesh::fullscreen_quad().get_buffers(flags).m_vao);

    // Draw the triangles !
    glDrawElements(gl::GL_TRIANGLES,                                              // mode
                   (gl::GLsizei)gfx::Mesh::fullscreen_quad().get_vertex_count(),  // count
                   gl::GL_UNSIGNED_INT,                                           // type
                   (void*)0  // element array buffer offset
    );

    gl::glDisable(gl::GL_STENCIL_TEST);
    gl::glDepthMask(gl::GL_TRUE);
    gl::glDepthFunc(gl::GL_LEQUAL);
    // m_gaussian_pass->submit();
  }
}
void GlowPass::clear() {
  // m_god_ray_pass->clear();
  // m_gaussian_pass->clear();
}

unsigned int GlowPass::set_texture(unsigned int in_texture, unsigned int stencil_fbo,
                                   bool inside_glow, bool stencil_active, float stencil_width,
                                   float stencil_height) {
  m_back.in_texture = in_texture;
  m_back.stencil = stencil_fbo;
  m_base_back.active = true;
  m_back.inside_glow = inside_glow;
  m_back.stencil_active = stencil_active;
  m_back.stencil_width = stencil_width;
  m_back.stencil_height = stencil_height;
  return m_gaussian_texture;
}
}  // namespace gmp