#include "gmp/systems/graphics_helper/forward_pass.hpp"

#include "bse/asset.hpp"
#include "bse/directory_path.hpp"
#include "bse/file_path.hpp"
#include "bse/runtime_asset.hpp"
#include "cor/entity.tcc"
#include "cor/entity_manager.tcc"
#include "cor/frame_context.hpp"
#include "cor/key.hpp"
#include "gfx/buckets/gbuffer_bucket.hpp"
#include "gfx/command.hpp"
#include "gfx/key.hpp"
#include "gfx/material.hpp"
#include "gfx/mesh.hpp"
#include "gfx/shader.hpp"
#include "gfx/text_cache.hpp"
#include "gmp/components/animation_component.hpp"
#include "gmp/components/character_controller_component.hpp"
#include "gmp/components/directional_light_component.hpp"
#include "gmp/components/dynamic_component.hpp"
#include "gmp/components/graphic_text_component.hpp"
#include "gmp/components/highlight_component.hpp"
#include "gmp/components/model_component.hpp"
#include "gmp/components/particles_component.hpp"
#include "gmp/components/point_light_component.hpp"
#include "gmp/components/projection_component.hpp"
#include "gmp/components/rope_component.hpp"
#include "gmp/components/shield_effect_component.hpp"
#include "gmp/components/static_component.hpp"
#include "gmp/components/transform_component.hpp"
#include "gmp/components/transforms_component.hpp"
#include "gmp/systems/graphics_helper/graphics_system_particles_helper.hpp"
#include <bse/edit.hpp>
#include <gfx/context.hpp>
#include <gfx/frustum_shape.hpp>
#include <gfx/instanced_mesh.hpp>
#include <gfx/model.hpp>
#include <gfx/particle_system_manager.hpp>
#include <gfx/text.hpp>
#include <gfx/texture.hpp>
#include <gmp/components/player_component.hpp>
#include <gmp/components/sticky_triangle_component.hpp>
#include <gmp/systems/animation_system.hpp>
#include <gmp/systems/graphics_helper/utilities.hpp>

namespace bse {
class StackAllocator;
}
namespace gmp {
struct AnimationComponent;
}  // namespace gmp

namespace gmp {

ForwardPass::ForwardPass(bse::StackAllocator* front, bse::StackAllocator* back, float width,
                         float height)
    : RenderPass(width, height) {
  m_gbuffer_bucket = std::make_unique<gfx::GBufferBucket>(front, back, m_width, m_height, true);

  // ubo data
  m_uniform_buffer.create(sizeof(UBOData), &m_ubo_data);
  m_ubo_fragment_data.bias.x = 0.0002f;
  m_ubo_fragment_data.bias.y = 0.0005f;
  m_ubo_fragment_data.bias.z = 0.003f;
  m_ubo_fragment_data.frustum_limits.x = 15;
  m_ubo_fragment_data.frustum_limits.y = 50;
  m_ubo_fragment_data.frustum_limits.z = 300;
  m_ubo_fragment_data.windows_size = glm::vec2(m_width, m_height);
  m_fragment_uniform_buffer.create(sizeof(UBODataFragment), &m_ubo_fragment_data);

  // Fetch skybox assets
  m_skybox_shader = bse::Asset<gfx::Shader>(bse::ASSETS_ROOT / "shader/texture.frag"_fp);
  m_skybox_model = bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/dome.fbx"_fp);
  m_skybox_texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/sky.png"_fp);

  // Fetch billboard shader
  m_billboard_shader =
      bse::Asset<gfx::Shader>(bse::ASSETS_ROOT / "shader/grayscale_texture.frag"_fp);
  m_text_shader = bse::Asset<gfx::Shader>(bse::ASSETS_ROOT / "shader/2d_text_shader.frag"_fp);

  // setup framebuffer
  m_framebuffer = std::make_unique<gfx::FrameBuffer>(width, height, true, true);
  m_color_texture = m_framebuffer->add_texture(gfx::TextureType::RGBA);
  // m_position_texture = m_framebuffer->add_texture(gfx::TextureType::RGB16F);
  // m_normal_texture = m_framebuffer->add_texture(gfx::TextureType::RGB16F);
  m_glow_texture = m_framebuffer->add_texture(gfx::TextureType::RGBA);
  m_depth_texture = m_framebuffer->create();

  /*m_framebuffer_small = std::make_unique<gfx::FrameBuffer>(width / 4, height / 4, true, true);
  m_depth_texture_small = m_framebuffer_small->create();
*/
  m_back.point_light_positions.reserve(20);
  m_back.point_light_colors.reserve(20);
  m_back.point_light_radius.reserve(20);
  m_back.depth_prepass_done = true;
  m_front.point_light_positions.reserve(20);
  m_front.point_light_colors.reserve(20);
  m_front.point_light_radius.reserve(20);
  for (int i = 0; i < m_back.bound_textures.size(); i++) {
    m_back.bound_textures[i] = 0;
    m_back.last_used_texture[i] = 0;
    m_front.bound_textures[i] = 0;
    m_front.last_used_texture[i] = 0;
  }

  for (int i = 0; i < m_back.bound_textures.size(); i++) {
    m_back.bound_textures[i] = -1;
    m_back.last_used_texture[i] = -1;
  }
  for (int i = 0; i < 100; i++) {
    m_back.matrix_ubos[i].create(sizeof(ForwardPass::ModelMatricesUBOData), nullptr);
    m_front.matrix_ubos[i].create(sizeof(ForwardPass::ModelMatricesUBOData), nullptr);
  }
}

void ForwardPass::prepare_for_frame() {
  m_gbuffer_bucket->prepare_for_new_frame();
  m_back.depth_prepass_done = false;
}

void ForwardPass::sort() { m_gbuffer_bucket->sort(); }
void ForwardPass::submit() {
  if (!m_front.depth_prepass_done) {
    for (int i = 0; i < m_front.used_ubos; i++) {
      ASSERT(m_front.matrix_data[i].size() < 350)
          << "Trying to allocate more matrices to ubo than exists";
      m_front.matrix_ubos[i].update(m_front.matrix_data[i].size() * sizeof(glm::mat4),
                                    &m_front.matrix_data[i][0]);
    }
    m_framebuffer->bind();
    m_uniform_buffer.bind(0);
    // m_fragment_uniform_buffer.bind(1);
    gl::glColorMask(gl::GL_FALSE, gl::GL_FALSE, gl::GL_FALSE, gl::GL_FALSE);
    gl::glDepthMask(gl::GL_TRUE);
    gl::glEnable(gl::GL_DEPTH_TEST);
    gl::glClear(gl::GL_DEPTH_BUFFER_BIT | gl::GL_STENCIL_BUFFER_BIT);
    gl::glDepthFunc(gl::GL_LEQUAL);  // bind textures needed
    m_gbuffer_bucket->submit();
    gl::glColorMask(gl::GL_TRUE, gl::GL_TRUE, gl::GL_TRUE, gl::GL_TRUE);
    m_front.depth_prepass_done = true;
  } else {
    gl::glDisable(gl::GL_STENCIL_TEST);
    m_framebuffer->bind();
    m_uniform_buffer.bind(0);
    m_fragment_uniform_buffer.bind(1);
    gl::glDepthFunc(gl::GL_EQUAL);
    gl::glEnable(gl::GL_DEPTH_TEST);
    gl::glDisable(gl::GL_BLEND);
    gl::glClear(gl::GL_COLOR_BUFFER_BIT);

    // bind textures needed
    for (int i = 0; i < m_front.bound_textures.size(); i++) {
      if (m_front.bound_textures[i] == 0) {
        break;
      }
      gl::glActiveTexture(gl::GL_TEXTURE0 + i);
      gl::glBindTexture(gl::GL_TEXTURE_2D, m_front.bound_textures[i]);
    }

    m_gbuffer_bucket->submit();
    gl::glDepthFunc(gl::GL_LEQUAL);
  }
}
void ForwardPass::clear() {
  m_gbuffer_bucket->clear();
  for (int i = 0; i < m_back.bound_textures.size(); i++) {
    m_back.bound_textures[i] = -1;
    m_back.last_used_texture[i] = -1;
  }
  for (int i = 0; i < m_back.matrix_data.size(); i++) {
    m_back.matrix_data[i].clear();
    m_back.ubo_point_lights[i].clear();
  }
}

void ForwardPass::read_update(cor::FrameContext& context) {
  for (auto particle_handle : m_particle_systems_to_create) {
    cor::Entity particle = context.entity_mgr.get_entity(particle_handle);
    if (particle.is_valid() && particle.has<ParticlesComponent>()) {
      ParticlesComponent& pc = particle;
      pc.handle = m_particle_system_manager.get_particle_system();
      // pc.vbo = gfx::create_vbo_bufffer(pc.max_particles);
      // pc.vao = gfx::create_vao_buffer(pc.vbo);
    }
  }
  m_particle_systems_to_create.clear();
  m_particle_system_manager.update_age();
}

unsigned int ForwardPass::get_texture(std::string name) {
  if (name == "depth") {
    return m_depth_texture;
  } else if (name == "glow") {
    return m_glow_texture;
  } else if (name == "position") {
    return m_position_texture;
  } else if (name == "normal") {
    return m_normal_texture;
  } else if (name == "depth_small") {
    return m_depth_texture_small;
  }
  return m_color_texture;
}

gfx::FrameBuffer* ForwardPass::get_fbo() { return m_framebuffer.get(); }

void ForwardPass::populate_frame(const cor::FrameContext& context,
                                 const ProjectionComponent& camera, const glm::mat4& v_matrix,
                                 const glm::vec3& cam_pos,
                                 std::vector<unsigned int>& shadow_textures, float* shadow_matrices,
                                 float* shadow_depths,
                                 bse::Flags<LightEffects::COUNT> active_effects,
                                 glm::vec3& dir_light_color, glm::vec3& dir_light_direction) {
  gfx::FrustumShape frustum_shape(camera.projection_matrix, v_matrix);

  // get dir light
  m_ubo_fragment_data.bools.x = 0;
  m_ubo_fragment_data.bools.y = 0;
  if (active_effects.test(LightEffects::DIRECTIONAL_LIGHT)) {
    m_ubo_fragment_data.dir_light_direction = dir_light_direction;
    m_ubo_fragment_data.dir_light_color = dir_light_color;
    m_ubo_fragment_data.bools.x = 1;

    // add shadows
    if (active_effects.test(LightEffects::DIRECTIONAL_SHADOWS)) {
      m_ubo_fragment_data.bools.y = 1;
    }
  }

  add_models(context, frustum_shape, cam_pos, shadow_textures, shadow_matrices, shadow_depths,
             active_effects);
  add_skybox(context, camera, v_matrix);

  add_ropes(context);

  add_particle_systems(context, m_gbuffer_bucket.get(), v_matrix, cam_pos,
                       m_particle_systems_to_create);

  add_text(context, v_matrix, camera.projection_matrix);

  BSE_EDIT(m_ubo_fragment_data.bias.x);
  BSE_EDIT(m_ubo_fragment_data.bias.y);
  BSE_EDIT(m_ubo_fragment_data.bias.z);
  glm::vec3 c_pos = cam_pos;
  std::swap(c_pos.y, c_pos.z);
  m_ubo_fragment_data.cam_pos = c_pos;
  size_t size = sizeof(m_ubo_fragment_data);

  m_ubo_data.view_matrix = v_matrix;
  m_ubo_data.projection_matrix = camera.projection_matrix;
  m_ubo_fragment_data.time += context.dt;

  gfx::Context::run_async(
      [this, size, data = m_ubo_data, fragment_data = m_ubo_fragment_data]() mutable {
        m_fragment_uniform_buffer.update(size, &fragment_data);
        m_uniform_buffer.update(sizeof(data), &data);
      });
}

void ForwardPass::swap() {
  if (m_front.depth_prepass_done) {
    RenderPass::swap();
    std::swap(std::move(m_front), std::move(m_back));
    m_gbuffer_bucket->swap();
  }
}

void ForwardPass::set_splits(float frustum_1, float frustum_2, float frustum_3) {
  m_ubo_fragment_data.frustum_limits.x = frustum_1;
  m_ubo_fragment_data.frustum_limits.y = frustum_2;
  m_ubo_fragment_data.frustum_limits.z = frustum_3;
}

void ForwardPass::set_texture(unsigned int texture, std::string name) { m_ssao_texture = texture; }

void* ForwardPass::add_texture_to_bucket(unsigned int texture, unsigned int location,
                                         void* prev_command) {
  int index = -1;
  auto loc = std::find(m_back.bound_textures.begin(), m_back.bound_textures.end(), texture);
  int idx =
      std::distance(m_back.bound_textures.begin(),
                    std::find(m_back.bound_textures.begin(), m_back.bound_textures.end(), texture));
  if (loc != m_back.bound_textures.end()) {
    index = loc - m_back.bound_textures.begin();
    m_back.last_used_texture[index] = m_last_used++;
  } else {
    index = 0;
    for (int i = 1; i < m_back.last_used_texture.size(); i++) {
      if (m_back.last_used_texture[i] < m_back.last_used_texture[index]) {
        index = i;
      }
    }
    m_back.bound_textures[index] = texture;
    m_back.last_used_texture[index] = m_last_used++;
  }

  gfx::commands::BindUniformInt* bind_texture_index =
      m_gbuffer_bucket->append_command<gfx::commands::BindUniformInt>(prev_command, 0);
  bind_texture_index->location = location;
  bind_texture_index->value = index;
  return bind_texture_index;
}

std::vector<ModelData> ForwardPass::get_culled_models(const cor::FrameContext& context,
                                                      gfx::FrustumShape& frustum_shape,
                                                      const glm::vec3& cam_pos) {
  std::vector<cor::EntityManager::EntityHandleWithComponent<TransformComponent, ModelComponent>>
      models =
          context.entity_mgr.get_entities_with_components<TransformComponent, ModelComponent>();
  std::vector<ModelData> results;
  results.reserve(models.size());

  for (auto& entity_handle : models) {
    cor::ConstEntity entity = context.entity_mgr.get_entity(entity_handle.handle);
    const TransformComponent& tc = entity_handle.comp1;
    const ModelComponent& mc = entity_handle.comp2;

    // do culling check...
    gfx::AABB aabb;
    const StaticComponent* sc = entity.get_if<StaticComponent>();
    if (sc != nullptr) {
      aabb = sc->contents.get_aabb();
    } else if (entity.has<DynamicComponent>()) {
      const DynamicComponent& dc = entity;
      aabb = dc.contents.get_aabb();
    } else if (entity.has<CharacterControllerComponent>()) {
      const CharacterControllerComponent& ccc = entity;
      aabb = ccc.contents.get_aabb();

      // If the entity is the player, increase size of AABB to avoid clipping due to frame delays
      if (entity.has<PlayerComponent>()) {
        aabb.size *= 5000;
      }
    } else if (entity.has<StickyTriangleComponent>()) {
      aabb = StickyTriangleComponent::get_aabb(tc.transform);
    } else {
      aabb = mc.asset->get_mesh()->get_aabb();
      aabb.size *= tc.transform.get_scale();
      aabb.position += tc.transform.get_position();
    }

    if (frustum_shape.aabb_intersect(aabb)) {
      // visible, add to results
      // results.push_back(entity_handle);
      ModelData md = {entity, &entity_handle.comp1, &entity_handle.comp2, aabb};
      results.push_back(md);
    }
  }
  return results;
}

std::vector<ForwardPass::PointLightStruct> ForwardPass::get_culled_point_lights(
    const cor::FrameContext& context, gfx::FrustumShape& frustum_shape) {
  std::vector<
      cor::EntityManager::EntityHandleWithComponent<TransformComponent, PointLightComponent>>
      point_lights = context.entity_mgr
                         .get_entities_with_components<TransformComponent, PointLightComponent>();

  // cull point lights and add the result into an array
  std::vector<PointLightStruct> point_lights_in_scene;
  point_lights_in_scene.reserve(point_lights.size());
  for (auto p_light_handle : point_lights) {
    // cor::ConstEntity entity = context.entity_mgr.get_entity(p_light_handle);
    const TransformComponent& tc = p_light_handle.comp1;
    const PointLightComponent& pc = p_light_handle.comp2;
    gfx::AABB aabb;
    aabb.position = tc.transform.get_position();
    aabb.size = glm::vec3(pc.radius * 0.2);
    if (frustum_shape.aabb_intersect(aabb)) {
      point_lights_in_scene.push_back({tc.transform.get_position(), pc.color, pc.radius});
    }
  }
  return point_lights_in_scene;
}

void* ForwardPass::add_pointlight_commands(std::vector<PointLightStruct>& point_lights_in_scene,
                                           ModelData& md, const glm::vec3& cam_pos,
                                           void* previous_command, bool cull = true) {
  void* latest_command = previous_command;
  // add point lights
  m_back.point_light_positions.clear();
  m_back.point_light_colors.clear();
  m_back.point_light_radius.clear();
  for (auto light : point_lights_in_scene) {
    if (glm::distance(md.tc->transform.get_position(), light.position) <=
            light.radius + std::max(md.aabb.size.x, std::max(md.aabb.size.y, md.aabb.size.z)) ||
        !cull) {
      m_back.point_light_positions.push_back(light.position);
      m_back.point_light_colors.push_back(light.color);
      m_back.point_light_radius.push_back(light.radius);
    }
    if (m_back.point_light_positions.size() > 10) {
      int farthest = 0;
      float farthest_distance = glm::distance2(cam_pos, m_back.point_light_positions[0]);
      int i = 0;
      for (auto& position : m_back.point_light_positions) {
        float dist = glm::distance2(cam_pos, position);
        if (dist > farthest_distance) {
          farthest = i;
          farthest_distance = dist;
        }
        i += 1;
      }
      // remove farthest
      m_back.point_light_positions.erase(m_back.point_light_positions.begin() + farthest);
      m_back.point_light_colors.erase(m_back.point_light_colors.begin() + farthest);
      m_back.point_light_radius.erase(m_back.point_light_radius.begin() + farthest);
    }
  }
  if (m_back.point_light_positions.size() > 0) {
    gfx::commands::BindUniformVec3List* bind_light_position =
        m_gbuffer_bucket->append_command<gfx::commands::BindUniformVec3List>(
            previous_command, sizeof(glm::vec3) * m_back.point_light_positions.size());
    bind_light_position->location = md.mc->asset->get_material()->point_light_position_location;
    char* temp_array = gfx::command_packet::get_auxiliary_memory(bind_light_position);
    memcpy(temp_array, &m_back.point_light_positions[0][0],
           sizeof(glm::vec3) * m_back.point_light_positions.size());
    bind_light_position->vertices = (float*)temp_array;
    bind_light_position->count = m_back.point_light_positions.size();

    gfx::commands::BindUniformVec3List* bind_light_color =
        m_gbuffer_bucket->append_command<gfx::commands::BindUniformVec3List>(
            bind_light_position, sizeof(glm::vec3) * m_back.point_light_colors.size());
    bind_light_color->location = md.mc->asset->get_material()->point_light_color_location;
    temp_array = gfx::command_packet::get_auxiliary_memory(bind_light_color);
    memcpy(temp_array, &m_back.point_light_colors[0][0],
           sizeof(glm::vec3) * m_back.point_light_colors.size());
    bind_light_color->vertices = (float*)temp_array;
    bind_light_color->count = m_back.point_light_colors.size();

    gfx::commands::BindUniformFloatList* bind_light_radius =
        m_gbuffer_bucket->append_command<gfx::commands::BindUniformFloatList>(
            bind_light_color, sizeof(float) * m_back.point_light_radius.size());
    bind_light_radius->location = md.mc->asset->get_material()->point_light_radius_location;
    temp_array = gfx::command_packet::get_auxiliary_memory(bind_light_radius);
    memcpy(temp_array, &m_back.point_light_radius[0],
           sizeof(float) * m_back.point_light_radius.size());
    bind_light_radius->floats = (float*)temp_array;
    bind_light_radius->count = m_back.point_light_radius.size();

    latest_command = bind_light_radius;
  }

  gfx::commands::BindUniformInt* bind_light_count =
      m_gbuffer_bucket->append_command<gfx::commands::BindUniformInt>(latest_command, 0);
  bind_light_count->location = md.mc->asset->get_material()->point_light_count_location;
  bind_light_count->value = m_back.point_light_positions.size();
  return bind_light_count;
}

void ForwardPass::add_wireframe_to_bucket(ModelData& md, const glm::vec3& cam_pos) {
  gfx::Key command_key;
  command_key.shader = bse::Asset<gfx::Shader>(bse::ASSETS_ROOT / "shader/highlight.frag"_fp);
  command_key.shader_index = command_key.shader.get_id();
  command_key.depth = glm::distance(md.tc->transform.get_position(), cam_pos);
  command_key.blend_type = 0;

  // add modelmatrix
  gfx::commands::BindUniformMat4* bind_model_matrix_cp =
      m_gbuffer_bucket->add_command<gfx::commands::BindUniformMat4>(command_key, 0);

  bind_model_matrix_cp->location = command_key.shader->get_uniform_location("MODEL_MATRIX");
  bind_model_matrix_cp->matrix = md.tc->transform.get_model_matrix();

  gfx::commands::DrawIndexed* draw_indexed =
      m_gbuffer_bucket->append_command<gfx::commands::DrawIndexed>(bind_model_matrix_cp, 0);
  draw_indexed->triangle_mode = gl::GL_LINES;
  draw_indexed->vertex_count = md.mc->asset->get_mesh()->get_vertex_count();

  gfx::VertexFlags flags;
  flags.set(bse::VertexTypes::POSITION);

  draw_indexed->vao = get_mesh_vao(md.entity, md.mc->asset->get_mesh(), flags);
}

void ForwardPass::add_shield_to_bucket(const cor::FrameContext& context, ModelData& md,
                                       const glm::vec3& cam_pos, const ShieldEffectComponent* sec) {
  gfx::Key command_key;
  command_key.shader = bse::Asset<gfx::Shader>(bse::ASSETS_ROOT / "shader/shield.frag"_fp);
  command_key.shader_index = command_key.shader.get_id();
  command_key.depth = glm::distance(md.tc->transform.get_position(), cam_pos);
  command_key.blend_type = 1;

  // add modelmatrix
  gfx::commands::BindUniformMat4* bind_model_matrix_cp =
      m_gbuffer_bucket->add_command<gfx::commands::BindUniformMat4>(command_key, 0);

  bind_model_matrix_cp->location = command_key.shader->get_uniform_location("MODEL_MATRIX");
  Transform t = md.tc->transform;
  t.set_scale(md.tc->transform.get_scale() * sec->scale_offset, context);
  bind_model_matrix_cp->matrix = t.get_model_matrix();

  gfx::commands::BindTexture* bind_texture =
      m_gbuffer_bucket->append_command<gfx::commands::BindTexture>(bind_model_matrix_cp, 0);
  bind_texture->location = command_key.shader->get_uniform_location("TEXTURE_SAMPLER");
  bind_texture->texture =
      bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/shield.png"_fp)->get_id();
  bind_texture->index = 0;

  gfx::commands::BindUniformFloat* bind_time =
      m_gbuffer_bucket->append_command<gfx::commands::BindUniformFloat>(bind_texture, 0);
  bind_time->value = m_time;
  bind_time->location = command_key.shader->get_uniform_location("time_passed");

  gfx::commands::BindUniformVec4* bind_color =
      m_gbuffer_bucket->append_command<gfx::commands::BindUniformVec4>(bind_time, 0);
  bind_color->vector = sec->color;
  bind_color->location = command_key.shader->get_uniform_location("color");

  gfx::commands::DrawIndexed* draw_indexed =
      m_gbuffer_bucket->append_command<gfx::commands::DrawIndexed>(bind_color, 0);
  draw_indexed->triangle_mode = gl::GL_TRIANGLES;
  draw_indexed->vertex_count = md.mc->asset->get_mesh()->get_vertex_count();

  gfx::VertexFlags flags;
  flags.set(bse::VertexTypes::POSITION);
  flags.set(bse::VertexTypes::NORMAL);
  flags.set(bse::VertexTypes::UV);

  draw_indexed->vao = get_mesh_vao(md.entity, md.mc->asset->get_mesh(), flags);
}

void ForwardPass::add_models(const cor::FrameContext& context, gfx::FrustumShape& frustum_shape,
                             const glm::vec3& cam_pos, std::vector<unsigned int>& shadow_textures,
                             float* shadow_matrices, float* shadow_depths,

                             bse::Flags<LightEffects::COUNT> active_effects) {
  //
  std::vector<ModelData> models = get_culled_models(context, frustum_shape, cam_pos);
  std::vector<PointLightStruct> point_lights = get_culled_point_lights(context, frustum_shape);

  std::map<int, int> instance_map;
  std::map<int, int> map_model_id_to_index;

  glm::vec3 adjusted_cam_pos = glm::vec3(-cam_pos.x, -cam_pos.z, cam_pos.y);

  for (auto& model_data : models) {
    if (!model_data.entity.has<AnimationComponent>()) {
      int as = model_data.mc->asset.get_id();
      instance_map[as] += 1;
    }
  }

  for (auto& item = instance_map.begin(); item != instance_map.end();) {
    if (item->second <= 0) {
      instance_map.erase(item++);
    } else {
      ++item;
    }
  }

  m_back.used_ubos = 0;
  // m_back.matrix_data.clear();
  int ubo_offset = 2;
  for (auto& model_data : models) {
    const TransformComponent* tc = model_data.tc;
    const ModelComponent* mc = model_data.mc;

    bse::Asset<gfx::Material> material = mc->asset->get_material();
    bse::Asset<gfx::Shader> shader = material->get_shader();
    bool highlight = false;
    bool wireframe_only = false;

    const HighlightComponent* hc = model_data.entity.get_if<HighlightComponent>();
    if (hc != nullptr) {
      highlight = true;
      wireframe_only = hc->wireframe_only;
    }
    if (!wireframe_only) {
      bool instanced = false;
      int model_id = model_data.mc->asset.get_id();
      auto it = instance_map.find(model_id);
      int ubo_index = 0;
      if (it != instance_map.end()) {
        // if already given id
        auto it = map_model_id_to_index.find(model_id);
        if (it != map_model_id_to_index.end()) {
          ubo_index = map_model_id_to_index[model_id];
          m_back.matrix_data[ubo_index].push_back(model_data.tc->transform.get_model_matrix());
          for (int i = 0; i < point_lights.size(); i++) {
            if (glm::distance(model_data.tc->transform.get_position(), point_lights[i].position) <=
                point_lights[i].radius +
                    std::max(model_data.aabb.size.x,
                             std::max(model_data.aabb.size.y, model_data.aabb.size.z))) {
              m_back.ubo_point_lights[ubo_index].insert(i);
            }
          }
          if (m_back.matrix_data[ubo_index].size() == instance_map[model_id]) {
            // all instances accounted for, render object as instanced
            // give the object an index.
            instanced = true;
          } else {
            continue;
          }
        } else {
          map_model_id_to_index[model_id] = m_back.used_ubos;
          ubo_index = m_back.used_ubos;
          m_back.matrix_data[m_back.used_ubos].push_back(
              model_data.tc->transform.get_model_matrix());
          m_back.used_ubos += 1;
          // if (m_back.matrix_data[ubo_index].size() == instance_map[model_id]) {
          //   // all instances accounted for, render object as instanced
          //   // give the object an index.
          //   instanced = true;
          // } else {
          //   continue;
          // }
        }
      } else {
        // not instanced, add
        auto itt = map_model_id_to_index.find(model_id);
        if (itt != map_model_id_to_index.end()) {
          ubo_index = map_model_id_to_index[model_id];
        } else {
          if (model_data.entity.has<AnimationComponent>()) {
            ubo_index = m_back.used_ubos;
            m_back.used_ubos += 1;

          } else {
            map_model_id_to_index[model_id] = m_back.used_ubos;
            ubo_index = m_back.used_ubos;
            m_back.used_ubos += 1;
          }
        }
        m_back.matrix_data[ubo_index].push_back(model_data.tc->transform.get_model_matrix());
      }
      void* latest_command;

      gfx::Key command_key;
      command_key.shader = shader;
      command_key.shader_index = shader.get_id();
      command_key.depth = glm::distance(tc->transform.get_position(), cam_pos);
      command_key.blend_type = 0;

      // add modelmatrix
      // gfx::commands::BindUniformMat4* bind_model_matrix_cp =
      //     m_gbuffer_bucket->add_command<gfx::commands::BindUniformMat4>(command_key, 0);
      // bind_model_matrix_cp->location = material->model_matrix_location;
      // bind_model_matrix_cp->matrix = tc->transform.get_model_matrix();
      // latest_command = bind_model_matrix_cp;

      // bind model_matrix ubo
      // bind index + offset as model_matrix.
      gfx::commands::BindUniformBuffer* bind_model_matrix_ubo =
          m_gbuffer_bucket->add_command<gfx::commands::BindUniformBuffer>(command_key, 0);
      bind_model_matrix_ubo->location = material->model_matrix_ubo_location;

      // get handle

      bind_model_matrix_ubo->id = m_back.matrix_ubos[ubo_index].get_handle();

      bind_model_matrix_ubo->program = command_key.shader->get_shader_handle();
      latest_command = bind_model_matrix_ubo;

      latest_command = add_texture_to_bucket(
          m_ssao_texture, shader->get_uniform_location("ssao_sampler"), latest_command);

      if (instanced) {
        std::vector<PointLightStruct> pl;
        pl.reserve(20);
        for (const int p : m_back.ubo_point_lights[ubo_index]) {
          pl.push_back(point_lights[p]);
        }
        latest_command =
            add_pointlight_commands(pl, model_data, adjusted_cam_pos, latest_command, false);
      } else {
        latest_command =
            add_pointlight_commands(point_lights, model_data, adjusted_cam_pos, latest_command);
      }

      if (shadow_matrices != nullptr && !shadow_textures.empty()) {
        // add commands for shadowmapping.
        gfx::commands::BindUniformMat4List* bind_shadow_matrices =
            m_gbuffer_bucket->append_command<gfx::commands::BindUniformMat4List>(latest_command, 0);
        bind_shadow_matrices->location = material->light_space_matrix_location;
        bind_shadow_matrices->matrices = shadow_matrices;
        bind_shadow_matrices->count = 4;

        gfx::commands::BindUniformFloatList* bind_shadow_depths =
            m_gbuffer_bucket->append_command<gfx::commands::BindUniformFloatList>(
                bind_shadow_matrices, 0);
        bind_shadow_depths->location = material->get_uniform_location("shadow_depths");
        bind_shadow_depths->floats = shadow_depths;
        bind_shadow_depths->count = 4;
        latest_command = bind_shadow_depths;

        latest_command = add_texture_to_bucket(shadow_textures[0], material->depth_map_0_location,
                                               latest_command);
        latest_command = add_texture_to_bucket(shadow_textures[1], material->depth_map_1_location,
                                               latest_command);
        latest_command = add_texture_to_bucket(shadow_textures[2], material->depth_map_2_location,
                                               latest_command);
        latest_command = add_texture_to_bucket(shadow_textures[3], material->depth_map_3_location,
                                               latest_command);
      }

      if (instanced) {
        // update ubo with matrices
        // gfx::commands::UpdateUBO* update_ubo;

        // bind ubo

        gfx::commands::DrawInstancedIndexed* draw_instanced_indexed =
            m_gbuffer_bucket->append_command<gfx::commands::DrawInstancedIndexed>(latest_command,
                                                                                  0);
        draw_instanced_indexed->vertex_count = mc->asset->get_mesh()->get_vertex_count();
        draw_instanced_indexed->vao = get_mesh_vao(model_data.entity, mc->asset->get_mesh(),
                                                   material->get_required_vertex_data());
        draw_instanced_indexed->instance_count = m_back.matrix_data[ubo_index].size();

      } else {
        // render mesh
        gfx::commands::DrawIndexed* draw_indexed =
            m_gbuffer_bucket->append_command<gfx::commands::DrawIndexed>(latest_command, 0);

        draw_indexed->vertex_count = mc->asset->get_mesh()->get_vertex_count();
        draw_indexed->triangle_mode = gl::GL_TRIANGLES;

        draw_indexed->vao = get_mesh_vao(model_data.entity, mc->asset->get_mesh(),
                                         material->get_required_vertex_data());
      }
    }

    if (highlight) {
      add_wireframe_to_bucket(model_data, cam_pos);
    }
    const ShieldEffectComponent* sec = model_data.entity.get_if<ShieldEffectComponent>();
    if (sec != nullptr) {
      add_shield_to_bucket(context, model_data, cam_pos, sec);
    }
  }

  // update uniform buffers
  // for (auto it = instance_matrices.begin(); it != instance_matrices.end(); it++) {
  //   int i = map_model_id_to_index[it->first];
  //   m_back.matrix_ubos[i].update(sizeof(it->second.size()), &it->second[0]);
  // }
  m_time += context.dt;
}

void ForwardPass::add_skybox(const cor::FrameContext& context, const ProjectionComponent& camera,
                             const glm::mat4& camera_transform) {
  bool do_render_skybox = true;
  BSE_EDIT(do_render_skybox);

  if (!do_render_skybox) {
    return;
  }

  gfx::Key command_key;
  command_key.shader = m_skybox_shader;
  command_key.shader_index = command_key.shader.get_id();
  command_key.depth = 99999999;
  command_key.blend_type = 0;

  // add modelmatrix
  gfx::commands::BindUniformMat4* bind_model_matrix_cp =
      m_gbuffer_bucket->add_command<gfx::commands::BindUniformMat4>(command_key, 0);
  bind_model_matrix_cp->location = command_key.shader->get_uniform_location("MODEL_MATRIX");
  bind_model_matrix_cp->matrix = glm::scale(glm::mat4(1.f), glm::vec3(10000.f, 10000.f, 10000.f));

  void* previous_command = bind_model_matrix_cp;

  gfx::commands::BindUniformMat4* bind_projection_matrix =
      m_gbuffer_bucket->append_command<gfx::commands::BindUniformMat4>(previous_command, 0);
  bind_projection_matrix->location = command_key.shader->get_uniform_location("projection_matrix");
  bind_projection_matrix->matrix =
      glm::infinitePerspective(glm::radians(camera.field_of_view), camera.aspectRatio, camera.near);
  previous_command = bind_projection_matrix;

  gfx::commands::BindTexture* bind_texture =
      m_gbuffer_bucket->append_command<gfx::commands::BindTexture>(previous_command, 0);
  bind_texture->location = command_key.shader->get_uniform_location("texture_sampler");
  bind_texture->texture = m_skybox_texture->get_id();
  bind_texture->index = 0;
  previous_command = bind_texture;

  gfx::commands::DrawIndexed* draw_indexed =
      m_gbuffer_bucket->append_command<gfx::commands::DrawIndexed>(previous_command, 0);
  draw_indexed->triangle_mode = gl::GL_TRIANGLES;
  draw_indexed->vertex_count = m_skybox_model->get_mesh()->get_vertex_count();
  bse::Flags<bse::VertexTypes::COUNT> vertex_data;
  vertex_data.set(bse::VertexTypes::POSITION);
  vertex_data.set(bse::VertexTypes::UV);
  draw_indexed->vao = m_skybox_model->get_mesh()->get_buffers(vertex_data).m_vao;

  // bottom half
  command_key;
  command_key.shader =
      bse::Asset<gfx::Shader>(bse::ASSETS_ROOT / "shader/grayscale_texture.frag"_fp);
  command_key.shader_index = command_key.shader.get_id();
  command_key.depth = 99999999;
  command_key.blend_type = 0;

  // add modelmatrix
  bind_model_matrix_cp =
      m_gbuffer_bucket->add_command<gfx::commands::BindUniformMat4>(command_key, 0);
  bind_model_matrix_cp->location = command_key.shader->get_uniform_location("MODEL_MATRIX");
  bind_model_matrix_cp->matrix =
      glm::scale(glm::rotate(glm::mat4(1.0f), glm::pi<float>(), glm::vec3(1.0f, 0.0f, 0.0f)),
                 glm::vec3(10000.f, 10000.f, 10000.f));

  previous_command = bind_model_matrix_cp;

  bind_projection_matrix =
      m_gbuffer_bucket->append_command<gfx::commands::BindUniformMat4>(previous_command, 0);
  bind_projection_matrix->location = command_key.shader->get_uniform_location("PROJECTION_MATRIX");
  bind_projection_matrix->matrix =
      glm::infinitePerspective(glm::radians(camera.field_of_view), camera.aspectRatio, camera.near);
  previous_command = bind_projection_matrix;

  gfx::commands::BindUniformMat4* bind_view_matrix =
      m_gbuffer_bucket->append_command<gfx::commands::BindUniformMat4>(bind_projection_matrix, 0);
  bind_view_matrix->location = command_key.shader->get_uniform_location("VIEW_MATRIX");
  bind_view_matrix->matrix = camera_transform;
  previous_command = bind_view_matrix;

  gfx::commands::BindUniformVec3* bind_color =
      m_gbuffer_bucket->append_command<gfx::commands::BindUniformVec3>(previous_command, 0);
  bind_color->location = command_key.shader->get_uniform_location("COLOR");
  bind_color->vector = glm::vec3(178.0, 188.0, 237.0) * (1.f / 255.f);

  if (!m_plane_texture) {
    gfx::Context::run([this]() {
      gfx::Texture::Gray intensity = {255};
      m_plane_texture.reset(new gfx::Texture(&intensity, 1, 1));
    });
  }

  gfx::commands::BindTexture* bind_dummy_texture =
      m_gbuffer_bucket->append_command<gfx::commands::BindTexture>(bind_color, 0);
  bind_dummy_texture->location = command_key.shader->get_uniform_location("texture_sampler");
  bind_dummy_texture->texture = m_plane_texture->get_id();
  bind_dummy_texture->index = 0;
  previous_command = bind_dummy_texture;

  draw_indexed = m_gbuffer_bucket->append_command<gfx::commands::DrawIndexed>(previous_command, 0);
  draw_indexed->triangle_mode = gl::GL_TRIANGLES;
  draw_indexed->vertex_count = m_skybox_model->get_mesh()->get_vertex_count();
  draw_indexed->vao = m_skybox_model->get_mesh()->get_buffers(vertex_data).m_vao;
}

void ForwardPass::add_ropes(const cor::FrameContext& context) {
  cor::Key rope_key = cor::Key::create<TransformComponent, RopeComponent>();
  std::vector<cor::EntityHandle> ropes = context.entity_mgr.get_entities(rope_key);
  for (auto rope_handle : ropes) {
    auto rope = context.entity_mgr.get_entity(rope_handle);
    const RopeComponent& rc = rope;
    const TransformComponent& tc = rope;

    if (context.entity_mgr.is_valid(rc.other)) {
      auto other = context.entity_mgr.get_entity(rc.other);
      const TransformComponent& other_tc = other;

      bse::Asset<gfx::Model> model = bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/rope.fbx"_fp);
      bse::Asset<gfx::Material> material = model->get_material();

      // render rope
      gfx::Key command_key;
      command_key.shader = material->get_shader();
      command_key.shader_index = material->get_shader().get_id();
      // command_key.depth = depth;
      command_key.blend_type = 0;

      glm::mat4 matrix = glm::mat4(1);
      matrix = glm::inverse(glm::lookAt(tc.transform.get_position(),
                                        other_tc.transform.get_position(), glm::vec3(0, 0, 1)));
      matrix *= glm::scale(
          glm::mat4(1),
          glm::vec3(rc.scale, rc.scale,
                    glm::distance(tc.transform.get_position(), other_tc.transform.get_position())));

      // add modelmatrix
      gfx::commands::BindUniformMat4* bind_model_matrix_cp =
          m_gbuffer_bucket->add_command<gfx::commands::BindUniformMat4>(command_key, 0);

      bind_model_matrix_cp->location = material->model_matrix_location;
      bind_model_matrix_cp->matrix = matrix;

      gfx::commands::BindTexture* bind_texture =
          m_gbuffer_bucket->append_command<gfx::commands::BindTexture>(bind_model_matrix_cp, 0);
      bind_texture->location = material->get_shader()->get_uniform_location("texture_sampler");
      bind_texture->texture = rc.texture->get_id();
      bind_texture->index = 5;

      gfx::commands::BindUniformFloat* bind_length =
          m_gbuffer_bucket->append_command<gfx::commands::BindUniformFloat>(bind_texture, 0);
      bind_length->location = material->get_shader()->get_uniform_location("rope_length");
      bind_length->value =
          glm::distance(tc.transform.get_position(), other_tc.transform.get_position()) / rc.scale;

      gfx::commands::DrawIndexed* draw_indexed =
          m_gbuffer_bucket->append_command<gfx::commands::DrawIndexed>(bind_length, 0);
      draw_indexed->triangle_mode = gl::GL_TRIANGLES;
      draw_indexed->vertex_count = model->get_mesh()->get_vertex_count();
      draw_indexed->vao =
          model->get_mesh()->get_buffers(material->get_required_vertex_data()).m_vao;
    }
  }
}

void ForwardPass::add_text(const cor::FrameContext& context, const glm::mat4& view_matrix,
                           const glm::mat4& projection_matrix) {
  // asd
  cor::Key billboard_key = cor::Key::create<TransformComponent, GraphicTextComponent>();
  std::vector<cor::EntityHandle> billboards = context.entity_mgr.get_entities(billboard_key);

  gfx::Key billboard_command_key;
  billboard_command_key.shader = m_text_shader;
  billboard_command_key.shader_index = billboard_command_key.shader.get_id();
  billboard_command_key.blend_type = 1;

  std::vector<std::pair<float, cor::ConstEntity>> sorted_billboards;
  for (cor::EntityHandle& billboard_handle : billboards) {
    cor::ConstEntity billboard = context.entity_mgr.get_entity(billboard_handle);
    glm::vec4 pos =
        view_matrix * glm::vec4(billboard.get<TransformComponent>().transform.get_position(), 1);
    sorted_billboards.emplace_back(pos.z, billboard);
  }

  std::sort(sorted_billboards.begin(), sorted_billboards.end(),
            [](const auto& lhs, const auto& rhs) { return lhs.first < rhs.first; });

  for (const auto& [depth, billboard] : sorted_billboards) {
    const TransformComponent& tc = billboard;
    const GraphicTextComponent& gc = billboard;
    // create model_matrix
    glm::mat4 trans = glm::transpose(view_matrix);
    glm::vec3 forward = -trans[2];
    glm::vec3 right = trans[0];
    glm::vec3 up = trans[1];
    glm::vec4 pos(0.f, 0.f, 0.f, 1.f);
    pos = tc.transform.get_model_matrix() * pos;
    glm::mat4 billboard_transform = glm::mat4(glm::vec4(right, 0), glm::vec4(forward, 0),
                                              glm::vec4(up, 0), glm::vec4(0, 0, 0, 1));

    billboard_transform = glm::translate(glm::mat4(1.f), glm::vec3(pos)) * billboard_transform;

    gfx::commands::BindUniformMat4* bind_model_matrix =
        m_gbuffer_bucket->add_command<gfx::commands::BindUniformMat4>(billboard_command_key, 0);
    bind_model_matrix->location =
        billboard_command_key.shader->get_uniform_location("MODEL_MATRIX");
    bind_model_matrix->matrix = projection_matrix * view_matrix * tc.transform.get_model_matrix();

    gfx::commands::BindUniformVec3* bind_color =
        m_gbuffer_bucket->append_command<gfx::commands::BindUniformVec3>(bind_model_matrix, 0);
    bind_color->location = billboard_command_key.shader->get_uniform_location("COLOR");
    bind_color->vector = glm::vec3(gc.color) / 255.f;

    bind_color = m_gbuffer_bucket->append_command<gfx::commands::BindUniformVec3>(bind_color, 0);
    bind_color->location = billboard_command_key.shader->get_uniform_location("OUTLINE_COLOR");
    bind_color->vector =
        glm::vec3((gc.outline_thickness < 0.00001f) ? gc.color : gc.outline_color) / 255.f;

    gfx::commands::BindUniformFloat* bind_thickness =
        m_gbuffer_bucket->append_command<gfx::commands::BindUniformFloat>(bind_color, 0);
    bind_thickness->location = billboard_command_key.shader->get_uniform_location("THICKNESS");
    bind_thickness->value = gc.thickness;

    bind_thickness =
        m_gbuffer_bucket->append_command<gfx::commands::BindUniformFloat>(bind_thickness, 0);
    bind_thickness->location =
        billboard_command_key.shader->get_uniform_location("OUTLINE_THICKNESS");
    bind_thickness->value = gc.outline_thickness;

    gfx::TextCache::Entry text_cache_entry = m_text_cache->get_text({gc.font, gc.text});

    gfx::commands::BindTexture* bind_texture =
        m_gbuffer_bucket->append_command<gfx::commands::BindTexture>(bind_thickness, 0);
    bind_texture->location = billboard_command_key.shader->get_uniform_location("TEXTURE");
    bind_texture->texture = text_cache_entry.texture;
    bind_texture->index = 0;

    gfx::commands::DrawArrays* draw_arrays =
        m_gbuffer_bucket->append_command<gfx::commands::DrawArrays>(bind_texture, 0);
    draw_arrays->vertex_count = text_cache_entry.vertex_count;
    draw_arrays->vao = text_cache_entry.vao;
    draw_arrays->triangle_mode = text_cache_entry.MODE;

    // m_mesh.get_material()->bind_model_view_projection_matrix(
    //     &(projection * view * billboard_transform)[0][0]);
  }
}

void ForwardPass::add_particle_systems(const cor::FrameContext& context,
                                       gfx::CommandBucket* gbuffer_bucket,
                                       const glm::mat4& inv_camera_transform,
                                       const glm::vec3& cam_pos,
                                       std::vector<cor::EntityHandle>& particle_systems_to_create) {
  cor::Key particle_key = cor::Key::create<TransformComponent, ParticlesComponent>();
  std::vector<cor::EntityHandle> particles = context.entity_mgr.get_entities(particle_key);

  glm::mat4 trans = glm::transpose(inv_camera_transform);
  //   glm::vec3 forward = trans[2];
  glm::vec3 forward = -trans[2];
  glm::vec3 right = trans[0];
  glm::vec3 up = trans[1];
  glm::mat4 billboard_transform = glm::mat4(glm::vec4(right, 0), glm::vec4(forward, 0),
                                            glm::vec4(up, 0), glm::vec4(0, 0, 0, 1));

  m_particle_system_manager.prepare_for_frame();
  for (auto particle_handle : particles) {
    cor::ConstEntity particle = context.entity_mgr.get_entity(particle_handle);
    const ParticlesComponent& pc = particle;
    const TransformComponent& tc = particle;

    if (pc.positions.size() > 0) {
      int vao = m_particle_system_manager.update_buffer((float*)&pc.positions[0],
                                                        pc.num_particles * 9, pc.handle);
      if (vao < 0) {
        m_particle_systems_to_create.push_back(particle_handle);
      }
      // gfx::update_buffer((float*)&pc.positions[0], pc.num_particles * 9, pc.vbo,
      //                    pc.max_particles * 9);

      // setup commands
      gfx::Key command_key;
      command_key.shader = pc.shader;
      command_key.shader_index = command_key.shader.get_id();
      command_key.depth = 100 - glm::distance(cam_pos, tc.transform.get_position());
      command_key.blend_type = true;
      gfx::commands::BindUniformMat4* bind_model_matrix_cp =
          gbuffer_bucket->add_command<gfx::commands::BindUniformMat4>(command_key, 0);

      bind_model_matrix_cp->location = pc.shader->get_uniform_location("MODEL_MATRIX");
      bind_model_matrix_cp->matrix = billboard_transform;

      gfx::commands::BindTexture* bind_particle_texture =
          gbuffer_bucket->append_command<gfx::commands::BindTexture>(bind_model_matrix_cp, 0);
      bind_particle_texture->location = pc.shader->get_uniform_location("texture_sampler");
      bind_particle_texture->texture = pc.texture->get_id();
      bind_particle_texture->index = 0;

      gfx::commands::DrawInstancedIndexed* draw_instanced_indexed =
          gbuffer_bucket->append_command<gfx::commands::DrawInstancedIndexed>(bind_particle_texture,
                                                                              0);
      draw_instanced_indexed->vertex_count = 6;
      draw_instanced_indexed->vao = std::abs(vao);
      draw_instanced_indexed->instance_count = pc.num_particles;
    } else {
      // particle_systems_to_create.push_back(particle_handle);
    }
  }
}

}  // namespace gmp