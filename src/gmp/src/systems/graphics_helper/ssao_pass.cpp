#include "gmp/systems/graphics_helper/ssao_pass.hpp"

#include "gfx/command.hpp"                   // for BindTexture, BindUniform...
#include "gfx/full_frame_gaussian_pass.hpp"  // for FullFrameGaussianPass
#include "gfx/full_frame_ssao_pass.hpp"      // for FullFrameSSAOPass
#include "gfx/shader.hpp"                    // for Shader
#include <bse/edit.hpp>
#include <ostream>  // for operator<<
#include <string>   // for operator<<
#include <utility>  // for move
#include <variant>  // for get
namespace bse {
class StackAllocator;
}

namespace gmp {

SSAOPass::SSAOPass(bse::StackAllocator* front, bse::StackAllocator* back, float width, float height)
    : RenderPass(width, height) {
  float ssao_quality = 0.5f;

  m_ssao_pass = std::make_unique<gfx::FullFrameSSAOPass>(
      front, back, bse::Asset<gfx::Shader>("assets/shader/ssao.frag"), width * ssao_quality,
      height * ssao_quality);

  m_gaussian_pass = std::make_unique<gfx::FullFrameGaussianPass>(
      front, back, bse::Asset<gfx::Shader>("assets/shader/gaussian.frag"), width * ssao_quality,
      height * ssao_quality);
}

void SSAOPass::prepare_for_frame() { m_base_back.active = false; }

void SSAOPass::swap() {
  RenderPass::swap();
  m_ssao_pass->swap();
  m_gaussian_pass->swap();
}
void SSAOPass::sort() {}
void SSAOPass::submit() {
  if (m_base_front.active) {
    m_ssao_pass->submit();
    m_gaussian_pass->submit();
    // m_gaussian_pass->set_texture(m_gaussian_pass->get_texture(0));
    // m_gaussian_pass->set_radius(0.004);
    // m_gaussian_pass->submit();
  }
}
void SSAOPass::clear() {
  m_ssao_pass->clear();
  m_gaussian_pass->clear();
}

unsigned int SSAOPass::populate_frame(unsigned int depth_texture, const glm::mat4& p_matrix) {
  gfx::commands::BindTexture* bind_texture_depth =
      m_ssao_pass->add_command<gfx::commands::BindTexture>(0);
  bind_texture_depth->location = m_ssao_pass->get_shader()->get_uniform_location("depth_sampler");
  bind_texture_depth->texture = depth_texture;
  bind_texture_depth->index = 0;

  gfx::commands::BindUniformMat4* bind_projection =
      m_ssao_pass->add_command<gfx::commands::BindUniformMat4>(0);
  bind_projection->location = m_ssao_pass->get_shader()->get_uniform_location("projection_matrix");
  bind_projection->matrix = p_matrix;

  gfx::commands::BindUniformMat4* bind_inv_projection =
      m_ssao_pass->add_command<gfx::commands::BindUniformMat4>(0);
  bind_inv_projection->location =
      m_ssao_pass->get_shader()->get_uniform_location("inv_projection_matrix");
  bind_inv_projection->matrix = glm::inverse(p_matrix);

  // ssao_gaussian
  m_gaussian_pass->set_texture(m_ssao_pass->get_texture(0));
  float radius = 0.002f;
  BSE_EDIT(radius);
  m_gaussian_pass->set_radius(radius);
  m_base_back.active = true;
  return m_gaussian_pass->get_texture(0);
}

}  // namespace gmp