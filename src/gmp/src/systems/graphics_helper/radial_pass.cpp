#include "gmp/systems/graphics_helper/radial_pass.hpp"

#include "gfx/command.hpp"          // for BindTexture, BindUniformFloat
#include "gfx/full_frame_pass.hpp"  // for FullFramePass
#include "gfx/shader.hpp"           // for Shader
#include <gfx/framebuffer.hpp>      // for FrameBuffer
namespace bse {
class StackAllocator;
}

namespace gmp {

RadialPass::RadialPass(bse::StackAllocator* front, bse::StackAllocator* back, float width,
                       float height)
    : RenderPass(width, height) {
  m_shader = bse::Asset<gfx::Shader>("assets/shader/radial.frag");
  m_full_frame_pass =
      std::make_unique<gfx::FullFramePass>(front, back, m_shader, width, height, false);
}

void RadialPass::swap() {
  RenderPass::swap();
  m_full_frame_pass->swap();
  std::swap(m_front, m_back);
}
void RadialPass::prepare_for_frame() {
  m_base_back.active = false;
  m_back.framebuffer = nullptr;
}

void RadialPass::sort() {
  if (m_base_back.active) {
    gfx::commands::BindTexture* bind_input_texture =
        m_full_frame_pass->add_command<gfx::commands::BindTexture>(0);
    bind_input_texture->location = m_shader->get_uniform_location("texture_sampler");
    bind_input_texture->texture = m_back.in_texture;
    bind_input_texture->index = 0;

    gfx::commands::BindUniformFloat* bind_radius =
        m_full_frame_pass->add_command<gfx::commands::BindUniformFloat>(0);
    bind_radius->location = m_shader->get_uniform_location("radius");
    bind_radius->value = m_back.blur;
  }
}
void RadialPass::submit() {
  if (m_base_front.active) {
    if (m_front.framebuffer != nullptr) {
      m_front.framebuffer->bind();
    }
    m_full_frame_pass->submit();
    if (m_front.framebuffer != nullptr) {
      m_front.framebuffer->unbind();
    }
  }
}
void RadialPass::clear() { m_full_frame_pass->clear(); }

unsigned int RadialPass::set_texture(unsigned int texture) {
  m_base_back.active = true;
  m_back.in_texture = texture;
  // m_full_frame_pass->set_texture(texture);
  return m_full_frame_pass->get_texture(0);
}

void RadialPass::set_blur_radius(float blur) { m_back.blur = blur; }

void RadialPass::set_framebuffer(gfx::FrameBuffer* framebuffer) {
  m_back.framebuffer = framebuffer;
}

}  // namespace gmp