#include "gmp/systems/player_system.hpp"

#include "bse/log.hpp"              // for FATAL, LOG
#include "cor/entity_manager.tcc"   // for EntityM...
#include "cor/event.hpp"            // for Disable...
#include "cor/event_manager.hpp"    // for EventMa...
#include "cor/frame_context.hpp"    // for FrameCo...
#include "cor/key.hpp"              // for Key
#include "cor/system_id_proxy.hpp"  // for SystemI...
#include "gmp/components/ai_component.hpp"
#include "gmp/components/attach_to_joint_component.hpp"
#include "gmp/components/attach_to_triangle_component.hpp"
#include "gmp/components/audio_component.hpp"
#include "gmp/components/character_controller_component.hpp"  // for Charact...
#include "gmp/components/damage_component.hpp"
#include "gmp/components/debug_camera_component.hpp"
#include "gmp/components/destroyed_component.hpp"
#include "gmp/components/destructible_component.hpp"
#include "gmp/components/faction_component.hpp"
#include "gmp/components/lifetime_component.hpp"  // for Lifetim...
#include "gmp/components/material_component.hpp"
#include "gmp/components/model_component.hpp"
#include "gmp/components/particles_component.hpp"           // for SlowMot...
#include "gmp/components/physics_projectile_component.hpp"  // for SlowMot...
#include "gmp/components/player_component.hpp"              // for SlowMot...
#include "gmp/components/player_damage_visual_component.hpp"
#include "gmp/components/projection_component.hpp"   // for SlowMot...
#include "gmp/components/rope_component.hpp"         // for SlowMot...
#include "gmp/components/slow_motion_component.hpp"  // for SlowMot...
#include "gmp/components/transform_component.hpp"    // for SlowMot...
#include "gmp/gmp_helper_functions.hpp"
#include "gmp/player/player_aiming.hpp"        // for PlayerS...
#include "gmp/player/player_common_state.hpp"  // for PlayerC...
#include "gmp/player/player_dashing.hpp"       // for PlayerS...
#include "gmp/player/player_dead.hpp"          // for PlayerDead
#include "gmp/player/player_drawing.hpp"       // for PlayerS...
#include "gmp/player/player_in_air.hpp"        // for PlayerS...
#include "gmp/player/player_running.hpp"       // for PlayerS...
#include "gmp/player/player_slow_motion.hpp"   // for PlayerS...
#include "gmp/player/player_spawning.hpp"
#include "gmp/player/player_standing.hpp"  // for PlayerS...
#include "gmp/player/player_stunned.hpp"   // for PlayerS...
#include "gmp/player/player_swinging.hpp"  // for PlayerS...
#include "gmp/systems/graphics_system.hpp"
#include <bse/edit.hpp>
#include <cor/entity.tcc>  // for Entity
#include <gfx/texture.hpp>
#include <gmp/components/explode_component.hpp>
#include <gmp/components/frame_tag_component.hpp>
#include <gmp/components/name_component.hpp>
#include <gmp/systems/ai_combat_system.hpp>

namespace gmp {
class AISystem;
}
namespace gmp {
class AnimationSystem;
}
namespace gmp {
class PhysicsSystem;
}
namespace gmp {
class HealthSystem;
}
namespace gmp {
struct PlayerComponent;
}
namespace gmp {
struct ProjectionComponent;
}
namespace gmp {
struct RopeComponent;
}
namespace gmp {
struct TransformComponent;
}

namespace gmp {

PlayerSystem::PlayerSystem(cor::FrameContext& context) : m_context(context) {
  //
  m_exemplars[StateType::COMMON] = [this]() -> std::unique_ptr<PlayerBaseState> {
    std::unique_ptr<PlayerBaseState> ptr;
    ptr.reset(new PlayerCommonState(this));
    return ptr;
  };
  m_exemplars[StateType::AIMING] = [this]() -> std::unique_ptr<PlayerBaseState> {
    std::unique_ptr<PlayerAiming> ptr;
    ptr.reset(new PlayerAiming(this));
    return ptr;
  };
  m_exemplars[StateType::DASHING] = [this]() -> std::unique_ptr<PlayerBaseState> {
    std::unique_ptr<PlayerDashing> ptr;
    ptr.reset(new PlayerDashing(this, {0, 0, 0}));
    return ptr;
  };
  m_exemplars[StateType::DEAD] = [this]() -> std::unique_ptr<PlayerBaseState> {
    std::unique_ptr<PlayerDead> ptr;
    ptr.reset(new PlayerDead(this));
    return ptr;
  };
  m_exemplars[StateType::DRAWING] = [this]() -> std::unique_ptr<PlayerBaseState> {
    std::unique_ptr<PlayerDrawing> ptr;
    ptr.reset(new PlayerDrawing(this));
    return ptr;
  };
  m_exemplars[StateType::IN_AIR] = [this]() -> std::unique_ptr<PlayerBaseState> {
    std::unique_ptr<PlayerInAir> ptr;
    ptr.reset(new PlayerInAir(this));
    return ptr;
  };
  m_exemplars[StateType::RUNNING] = [this]() -> std::unique_ptr<PlayerBaseState> {
    std::unique_ptr<PlayerRunning> ptr;
    ptr.reset(new PlayerRunning(this));
    return ptr;
  };
  m_exemplars[StateType::SLOW_MOTION] = [this]() -> std::unique_ptr<PlayerBaseState> {
    std::unique_ptr<PlayerSlowMotion> ptr;
    ptr.reset(new PlayerSlowMotion(this));
    return ptr;
  };
  m_exemplars[StateType::STANDING] = [this]() -> std::unique_ptr<PlayerBaseState> {
    std::unique_ptr<PlayerStanding> ptr;
    ptr.reset(new PlayerStanding(this));
    return ptr;
  };
  m_exemplars[StateType::STUNNED] = [this]() -> std::unique_ptr<PlayerBaseState> {
    std::unique_ptr<PlayerStunned> ptr;
    ptr.reset(new PlayerStunned(this, 0.f));
    return ptr;
  };
  m_exemplars[StateType::SWINGING] = [this, &context]() -> std::unique_ptr<PlayerBaseState> {
    std::unique_ptr<PlayerSwinging> ptr;
    ptr.reset(new PlayerSwinging(this, context));
    return ptr;
  };
  m_exemplars[StateType::SPAWNING] = [this]() -> std::unique_ptr<PlayerBaseState> {
    std::unique_ptr<PlayerSpawning> ptr;
    ptr.reset(new PlayerSpawning(this));
    return ptr;
  };

  // Get player handle
  cor::Key player_key = cor::Key::create<PlayerComponent>();
  auto players = context.entity_mgr.get_entities(player_key);
  if (!players.size()) {
    return;
  }
  m_player_handle = players[0];

  // Get main camera handle
  cor::Key cam_key = cor::Key::create<ProjectionComponent, TransformComponent>();
  auto cameras = context.entity_mgr.get_entities(cam_key);
  for (auto& camera_handle : cameras) {
    cor::Entity camera = context.entity_mgr.get_entity(camera_handle);

    if (camera.has<NameComponent>()) {
      NameComponent& nc = camera;
      if (nc.name == "main_camera" && !camera.has<DebugCameraComponent>()) {
        m_camera_handle = camera_handle;
        break;
      }
    }
  }

  m_keys_pressed.reset();

  // Add event listeners
  add_listener(context.event_mgr.register_handler<MoveEvent>([this](const MoveEvent* event) {
    m_keys_pressed.moved = true;
    m_keys_pressed.move_forward = event->forward;
    m_keys_pressed.move_right = event->right;
  }));

  // Key events
  add_listener(context.event_mgr.register_handler<JumpEvent>(
      [this](const JumpEvent* event) { m_keys_pressed.jumped = true; }));
  add_listener(context.event_mgr.register_handler<DashEvent>(
      [this](const DashEvent* event) { m_keys_pressed.dashed = true; }));
  add_listener(context.event_mgr.register_handler<StartDrawEvent>(
      [this](const StartDrawEvent* event) { m_keys_pressed.draw_started = true; }));
  add_listener(context.event_mgr.register_handler<StopDrawEvent>(
      [this](const StopDrawEvent* event) { m_keys_pressed.draw_ended = true; }));
  add_listener(context.event_mgr.register_handler<StartAimEvent>(
      [this](const StartAimEvent* event) { m_keys_pressed.aim_started = true; }));
  add_listener(context.event_mgr.register_handler<StopAimEvent>(
      [this](const StopAimEvent* event) { m_keys_pressed.aim_ended = true; }));
  add_listener(context.event_mgr.register_handler<CancelActionEvent>(
      [this](const CancelActionEvent* event) { m_keys_pressed.action_canceled = true; }));
  add_listener(context.event_mgr.register_handler<SlowMotionEvent>(
      [this](const SlowMotionEvent* event) { m_keys_pressed.slow_motion = true; }));
  add_listener(context.event_mgr.register_handler<RopeEvent>([this](const RopeEvent* event) {
    if (!state_exists(StateType::DRAWING)) m_keys_pressed.rope_action = true;
  }));
  add_listener(context.event_mgr.register_handler<NormalArrowModeEvent>(
      [this](const NormalArrowModeEvent* event) { m_keys_pressed.normal_arrow = true; }));
  add_listener(context.event_mgr.register_handler<ExplosiveArrowModeEvent>(
      [this, &context](const ExplosiveArrowModeEvent* event) {
        if (m_explosive_arrows_count != 0)
          m_keys_pressed.explosive_arrow = true;
        else
          context.event_mgr.send_event(NormalArrowModeEvent());
      }));

  // Add explosive arrows
  add_listener(context.event_mgr.register_handler<AddExplosiveArrowsEvent>(
      [this, &context](const AddExplosiveArrowsEvent* event) {
        m_explosive_arrows_count += 6;
        if (m_explosive_arrows_count > m_max_explosive_arrows_count)
          m_explosive_arrows_count = m_max_explosive_arrows_count;
        context.event_mgr.send_event(ExplosiveArrowCountChanged{m_explosive_arrows_count});
      }));

  // Mouse movement
  add_listener(context.event_mgr.register_handler<MouseMovementEvent>(
      [this](const MouseMovementEvent* event) {
        if (!state_exists(StateType::SPAWNING)) {
          const float angle = 89.f * 0.0174532925f;
          m_camera_rotation_x -= event->dx * (m_camera_distance * 0.6f + 0.4f) * 0.8f;
          m_camera_rotation_z -= event->dy * (m_camera_distance * 0.6f + 0.4f) * 0.8f;
          if (m_camera_rotation_z > angle)
            m_camera_rotation_z = angle;
          else if (m_camera_rotation_z < -angle)
            m_camera_rotation_z = -angle;
        }
      }));

  // Death
  add_listener(context.event_mgr.register_handler<PlayerZeroHealth>(
      [this, &context](const PlayerZeroHealth* event) {
        //
        if (!state_exists(StateType::DEAD)) {
          auto player = context.entity_mgr.get_entity(m_player_handle);
          m_stack.clear();
          m_actions.clear();
          m_stack.push_back(std::make_unique<PlayerDead>(this));
          m_stack.back()->enter(context);
          context.event_mgr.send_event(DisableSystem{context.system_id_proxy.get_name<AISystem>()});
          context.event_mgr.send_event(
              DisableSystem{context.system_id_proxy.get_name<PhysicsSystem>()});
          context.event_mgr.send_event(
              DisableSystem{context.system_id_proxy.get_name<HealthSystem>()});

          if (m_rope_arrow_handle != cor::EntityHandle::NULL_HANDLE &&
              is_arrow_active(m_rope_arrow_handle))
            destroy_arrow(m_rope_arrow_handle, context);
          if (context.entity_mgr.is_valid(m_rope_handle))
            context.entity_mgr.destroy_entity(m_rope_handle);

          const TransformComponent& tc = player;
          if (tc.transform.get_position().z < 70) {
            auto camera = context.entity_mgr.get_entity(m_camera_handle);
            TransformComponent& ctc = camera;
            glm::vec3 pos = ctc.transform.get_position();
            ctc.transform.set_position(glm::vec3(pos.x, pos.y, 70.f), context);
          }
        }
      }));

  // Stunned
  add_listener(
      context.event_mgr.register_handler<StunEvent>([this, &context](const StunEvent* event) {
        //
        if (event->target == m_player_handle) {
          m_stack.clear();
          m_actions.clear();
          m_stack.push_back(std::make_unique<PlayerCommonState>(this));
          m_stack[0]->enter(context);
          m_stack.push_back(std::make_unique<PlayerStunned>(this, event->duration_time));

          if (m_rope_arrow_handle != cor::EntityHandle::NULL_HANDLE &&
              is_arrow_active(m_rope_arrow_handle))
            destroy_arrow(m_rope_arrow_handle, context);
          if (context.entity_mgr.is_valid(m_rope_handle))
            context.entity_mgr.destroy_entity(m_rope_handle);
        }
      }));

  // Rope arrow hit something
  add_listener(context.event_mgr.register_handler<CollisionEvent>([this, &context](
                                                                      const CollisionEvent* event) {
    //
    if ((event->entity_1 == m_rope_arrow_handle || event->entity_2 == m_rope_arrow_handle) &&
        !state_exists(StateType::SPAWNING)) {
      //
      auto player = context.entity_mgr.get_entity(m_player_handle);

      auto arrow = context.entity_mgr.get_entity(m_rope_arrow_handle);
      const glm::vec3 arrow_pos = arrow.get<TransformComponent>().transform.get_position();
      const TransformComponent& ptc = player;
      m_original_distance_to_swing_center = glm::length(ptc.transform.get_position() - arrow_pos);

      cor::EntityHandle target_handle =
          (event->entity_1 == m_rope_arrow_handle) ? event->entity_2 : event->entity_1;
      cor::Entity target = context.entity_mgr.get_entity(target_handle);

      const MaterialComponent matc =
          (target.has<MaterialComponent>()) ? target : MaterialComponent{MaterialType::GENERIC};

      // Attach
      if (m_original_distance_to_swing_center > 5.f && matc.material == MaterialType::WOOD &&
          m_camera_hit_point.z < 860.f) {
        m_keys_pressed.rope_hit = true;

        CharacterControllerComponent& cc = player;
        cc.contents.start_swing(arrow_pos);

        m_anchored_object_handle = target_handle;

        // Hit sound
        aud::SoundSettings settings;
        settings.gain = 0.4f;
        settings.gain_min = 0.1f;
        context.event_mgr.send_event(PlaySoundEvent(
            m_player_handle,
            bse::ASSETS_ROOT / "audio/effects/bow_and_arrow/rope_arrow_hit/post/hit.ogg"_fp,
            settings));
      }
      // Detach
      else {
        if (is_arrow_active(m_rope_arrow_handle)) destroy_arrow(m_rope_arrow_handle, context);
        if (context.entity_mgr.is_valid(m_rope_handle))
          context.entity_mgr.destroy_entity(m_rope_handle);

        m_rope_arrow_handle = cor::EntityHandle::NULL_HANDLE;

        // Rope break sound
        context.event_mgr.send_event(PlaySoundEvent(
            m_player_handle,
            bse::ASSETS_ROOT / "audio/effects/bow_and_arrow/rope_broke/post/rope_broke.ogg"_fp,
            aud::SoundSettings()));
      }
    }
  }));

  // Respawn
  add_listener(context.event_mgr.register_handler<CheckpointLoaded>(
      [this, &context](const CheckpointLoaded* event) {
        //
        reset_arrows();
        m_stack.clear();
        m_actions.clear();
        m_stack.push_back(std::make_unique<PlayerSpawning>(this));
        m_stack[0]->enter(context);
      }));
  // Disable checkpoints when fighting/killing bosses
  add_listener(context.event_mgr.register_handler<TookHornEvent>(
      [this](const TookHornEvent* event) { m_in_boss_fight = true; }));
  add_listener(context.event_mgr.register_handler<SpawnColeEvent>(
      [this](const SpawnColeEvent* event) { m_in_boss_fight = true; }));
  // Enable checkpoints again
  add_listener(
      context.event_mgr.register_handler<ReplayEnded>([this, &context](const ReplayEnded* event) {
        m_in_boss_fight = false;
        context.event_mgr.send_event(SaveCheckpoint());
      }));

  // Default player values
  auto player = context.entity_mgr.get_entity(m_player_handle);
  m_stamina_left = m_max_stamina;
  PlayerComponent& pc = player;
  pc.stamina = m_max_stamina;
  pc.max_stamina = m_max_stamina;

  pc.max_charge_time = m_max_draw_time;

  // Find spawn positions
  const auto entities =
      context.entity_mgr.get_entities_with_components<NameComponent, TransformComponent>();
  std::vector<int> indices;
  for (int i = 0; i < entities.size(); ++i) {
    if (entities[i].comp1.name == "spawn_point") {
      indices.push_back(i);
    }
  }
  // Create checkpoints
  for (auto c : entities) {
    if (c.comp1.name == "checkpoint_area") {
      // Find closest spawn position
      int closest = 0;
      float d = 9999999;
      for (int j = 0; j < indices.size(); ++j) {
        float td = glm::distance(entities[indices[j]].comp2.transform.get_position(),
                                 c.comp2.transform.get_position());
        if (td < d) {
          closest = j;
          d = td;
        }
      }

      const float rot =
          glm::eulerAngles(entities[indices[closest]].comp2.transform.get_rotation()).z;
      m_checkpoints.push_back({c.comp2.transform.get_position(), c.comp2.transform.get_scale(),
                               entities[indices[closest]].comp2.transform.get_position(), rot});
    }
  }

  // Load assets
  m_hit_generic = bse::Asset<aud::SoundBuffer>(bse::ASSETS_ROOT /
                                               "audio/effects/bow_and_arrow/hit/post/hit2.ogg"_fp);
  m_hit_generic.pre_load();
  m_hit_grass = bse::Asset<aud::SoundBuffer>(
      bse::ASSETS_ROOT / "audio/effects/bow_and_arrow/hit/post/hit_grass1.ogg"_fp);
  m_hit_grass.pre_load();
  m_hit_wood = bse::Asset<aud::SoundBuffer>(
      bse::ASSETS_ROOT / "audio/effects/bow_and_arrow/hit/post/hit_wood1.ogg"_fp);
  m_hit_wood.pre_load();
  m_hit_flesh = bse::Asset<aud::SoundBuffer>(
      bse::ASSETS_ROOT / "audio/effects/bow_and_arrow/hit/post/hit_flesh.ogg"_fp);
  m_hit_flesh.pre_load();
  m_explosion_sound =
      bse::Asset<aud::SoundBuffer>(bse::ASSETS_ROOT / "audio/effects/explosions/explosion1.ogg"_fp);
  m_explosion_sound.pre_load();
  m_rope_texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/rope.png"_fp);
  m_rope_texture.pre_load();
  m_arrow_model = bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/cool_arrow.fbx"_fp);
  m_arrow_model.pre_load();
  m_explosive_arrow_model = bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/bomb_arrow.fbx"_fp);
  m_explosive_arrow_model.pre_load();
  m_swirl_particle = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/swirl_particle01.png"_fp);
  m_swirl_particle.pre_load();
  m_explosion_particle = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/explosion.png"_fp);
  m_explosion_particle.pre_load();

  // Add first state
  m_stack.push_back(std::make_unique<PlayerSpawning>(this));
  m_stack[0]->enter(context);

  m_actions.reserve(5);

  const unsigned int arrow_count = 40;
  m_flying_arrows.reserve(arrow_count);  // The player cant shoot arrows faster
  m_stuck_arrows.reserve(arrow_count);   // Most of the arrows will be here
  m_unused_arrows.reserve(arrow_count);  // Will mostly be empty after a while
  // Create the arrows
  for (unsigned int i = 0; i < arrow_count; ++i) {
    cor::EntityHandle handle = context.entity_mgr.create_entity();
    cor::Entity arrow = context.entity_mgr.get_entity(handle);

    // Position and rotation
    TransformComponent tc;
    tc.transform.set_scale(0.5f, context);
    arrow = tc;

    // Mark the arrow with the frame it was created
    FrameTagComponent ftc;
    ftc.frame_num = context.frame_num;
    arrow = ftc;

    // Model
    ModelComponent mc;
    mc.asset = m_arrow_model;
    arrow = mc;

    // Name
    auto nc = NameComponent();
    nc.name = "player_arrow";
    arrow = nc;

    // Damage
    DamageComponent dmgc;
    dmgc.damage_value = 1;
    dmgc.friendly_fire = false;
    arrow = dmgc;

    // Faction
    FactionComponent fcc;
    fcc.type = Faction::PLAYER;
    arrow = fcc;

    // Slow motion
    SlowMotionComponent smc;
    smc.factor.set_factor(0.70f);
    arrow = smc;

    // Sound effects
    aud::SoundSettings settings;
    settings.rolloff_factor = 0.1f;
    AudioComponent ac;
    ac.audio.set_collision_sound(m_hit_generic, {std::make_pair(MaterialType::GENERIC, settings)});
    ac.audio.set_collision_sound(m_hit_grass, {std::make_pair(MaterialType::GRASS, settings)});
    ac.audio.set_collision_sound(m_hit_wood, {std::make_pair(MaterialType::WOOD, settings)});
    ac.audio.set_collision_sound(m_hit_flesh, {std::make_pair(MaterialType::FLESH, settings)});
    arrow = std::move(ac);

    m_unused_arrows.push_back(handle);
  }

  // Simple entity that will always be where the rope arrow should spawn.
  m_arrow_pos_handle = context.entity_mgr.create_entity();
  cor::Entity arrow_pos = context.entity_mgr.get_entity(m_arrow_pos_handle);
  arrow_pos = TransformComponent();
  AttachToJointComponent atjc;
  atjc.entity_handle = m_player_handle;
  atjc.joint_name = "R_hand";
  atjc.offset_matrix = glm::rotate(3.1415f * 0.5f, glm::vec3(0, 0, 1)) *
                       glm::rotate(3.1415f * -0.07f, glm::vec3(1, 0, 0)) *
                       glm::translate(glm::vec3(2.f, 43.f, 0.f));
  arrow_pos = atjc;
}

void PlayerSystem::read_update(const cor::FrameContext& context) {
  //
  m_actions.clear();

  auto player = context.entity_mgr.get_entity(m_player_handle);
  if (!player.is_valid()) return;
  auto camera = context.entity_mgr.get_entity(m_camera_handle);
  if (!camera.is_valid()) return;
  // Push camera back
  const SlowMotionComponent& smc = player;
  m_camera_distance += m_camera_zoom_speed * 0.5f * context.unslowed_dt;
  if (m_camera_distance > 1.0f) m_camera_distance = 1.0f;

  m_action_index = m_stack.size() - 1;

  if (!m_keys_pressed.slow_motion) m_has_released_slow_motion = true;
}

void PlayerSystem::write_update(cor::FrameContext& context) {
  //
  context.speed_factor *= 1.f + m_slowing_down_speed * 0.5f * context.unslowed_dt;
  if (context.speed_factor >= 1.0f) context.speed_factor = 1.0f;

  // Run update on all states in the stack (from the back) and store their returns
  for (auto it = m_stack.rbegin(); it < m_stack.rend(); it++) {
    (*it)->update(context, m_keys_pressed);
    --m_action_index;
  }

  // Reset all key presses
  m_keys_pressed.reset();

  auto player = context.entity_mgr.get_entity(m_player_handle);
  if (!player.is_valid()) return;
  auto camera = context.entity_mgr.get_entity(m_camera_handle);
  if (!camera.is_valid()) return;

  // Go through the return values from the states
  for (unsigned int i = 0; i < m_actions.size(); ++i) {
    StateAction& a = m_actions[i];

    switch (a.action) {
      case ActionType::REPLACE:
        m_stack[a.index] = std::move(a.new_state);
        m_stack[a.index]->enter(context);
        break;

      case ActionType::REMOVE:
        m_stack[a.index] = nullptr;
        break;

      case ActionType::PUSH:
        m_stack.push_back(std::move(a.new_state));
        m_stack.back()->enter(context);
        break;
    }
  }

  // Remove any of the states that have (purposefully) been set to nullptr
  for (int i = m_stack.size() - 1; i >= 0; --i) {
    if (m_stack[i] == nullptr) m_stack.erase(m_stack.begin() + i);
  }

  PlayerComponent& pc = player;
  pc.stamina = m_stamina_left;
  pc.charge_time = m_draw_time;

  // Update damage visual opacity
  PlayerDamageVisualComponent& pdvc = player;
  pdvc.color.a -= (pdvc.color.a / 1.1f) * context.dt;
  if (pdvc.color.a < 0.1f) pdvc.color.a = 0.f;

  // Flying arrows
  {
    for (int i = m_flying_arrows.size() - 1; i >= 0; --i) {
      if (is_arrow_active(m_flying_arrows[i].handle)) {
        //
        auto arrow = context.entity_mgr.get_entity(m_flying_arrows[i].handle);
        TransformComponent& tc = arrow;

        // Arrow is still flying
        if (arrow.has<PhysicsProjectileComponent>() &&
            !arrow.get<PhysicsProjectileComponent>().contents.get_has_hit()) {
          // Decrease lifetime
          const SlowMotionComponent& asmc = arrow;
          m_flying_arrows[i].lifetime -= asmc.factor.get_converted_dt(context);
          if (m_flying_arrows[i].lifetime < 0.f) {
            if (m_flying_arrows[i].to_be_destroyed) {
              if (m_flying_arrows[i].handle == m_rope_arrow_handle) {
                m_rope_arrow_handle == cor::EntityHandle::NULL_HANDLE;
                if (m_context.entity_mgr.is_valid(m_rope_handle))
                  m_context.entity_mgr.destroy_entity(m_rope_handle);
              }
              m_unused_arrows.push_back(m_flying_arrows[i].handle);
              destroy_arrow(m_flying_arrows[i].handle, context);
              m_flying_arrows.erase(m_flying_arrows.begin() + i);
            } else {
              m_flying_arrows[i].to_be_destroyed = true;
              if (m_flying_arrows[i].handle == m_rope_arrow_handle) {
              }
            }
          }
          // Direction change
          else if (m_flying_arrows[i].time_to_direction_change >= 0.f) {
            m_flying_arrows[i].time_to_direction_change -= asmc.factor.get_converted_dt(context);
            // Time to change direction
            if (m_flying_arrows[i].time_to_direction_change < 0.f) {
              PhysicsProjectileComponent& ppc = arrow;
              ppc.contents.set_velocity(m_flying_arrows[i].new_direction *
                                        glm::length(ppc.contents.get_velocity()));
              tc.transform.set_position(m_flying_arrows[i].dir_change_pos, context);
            }
          }
        }
        // Arrow has hit something
        else {
          m_stuck_arrows.push_back({m_flying_arrows[i].handle, tc.transform.get_position()});
          m_flying_arrows.erase(m_flying_arrows.begin() + i);
        }
      }
      // Arrow is not active
      else {
        m_unused_arrows.push_back(m_flying_arrows[i].handle);
        destroy_arrow(m_flying_arrows[i].handle, context);
        m_flying_arrows.erase(m_flying_arrows.begin() + i);
      }
    }
  }
  // Stuck arrows
  {
    for (int i = m_stuck_arrows.size() - 1; i >= 0; --i) {
      auto& arrow_data = m_stuck_arrows[i];
      auto arrow = context.entity_mgr.get_entity(arrow_data.handle);
      if (!is_arrow_active(arrow_data.handle)) {
        m_unused_arrows.push_back(arrow_data.handle);
        m_stuck_arrows.erase(m_stuck_arrows.begin() + i);
      } else {
        if (m_stuck_arrows[i].time_left_particles >= 0.f)
          m_stuck_arrows[i].time_left_particles -= context.dt;
        if (m_stuck_arrows[i].time_left_particles < 0) {
          cor::Entity arrow = context.entity_mgr.get_entity(m_stuck_arrows[i].handle);
          arrow.detach<ParticlesComponent>();
        }
      }
    }
  }

  // Break rope if the entity that it is attached to is destroyed
  if (state_exists(StateType::SWINGING) && !context.entity_mgr.is_valid(m_anchored_object_handle)) {
    // Rope break sound
    context.event_mgr.send_event(PlaySoundEvent(
        m_player_handle,
        bse::ASSETS_ROOT / "audio/effects/bow_and_arrow/rope_broke/post/rope_broke.ogg"_fp,
        aud::SoundSettings()));

    if (m_rope_arrow_handle != cor::EntityHandle::NULL_HANDLE) {
      destroy_arrow(m_rope_arrow_handle, context);
      m_unused_arrows.push_back(m_rope_arrow_handle);
    }

    end_swing();
  }

  // "Aim assist". Makes the rope arrow�s hit box bigger while it is in the air
  cor::Entity rope_arrow = context.entity_mgr.get_entity(m_rope_arrow_handle);
  if (rope_arrow.is_valid() && rope_arrow.has<PhysicsProjectileComponent>()) {
    PhysicsProjectileComponent& ppc = rope_arrow;
    ppc.contents.set_radius(ppc.contents.get_radius() + context.dt * 4.f);
    ppc.contents.set_length(ppc.contents.get_length() + context.dt * 4.f);
  }

  if (m_stack.size() == 0) LOG(FATAL) << "Stack is empty!\n";
}  // namespace gmp

void PlayerSystem::add_action(ActionType action, std::unique_ptr<PlayerBaseState> new_state) {
  m_actions.push_back(StateAction(action, std::move(new_state), m_action_index));
}

bool PlayerSystem::state_exists(const StateType state) const {
  for (const auto& s : m_stack) {
    if (s->m_state_type == state) return true;
  }
  return false;
}

void PlayerSystem::base_move(const float right, const float forward) {
  auto player = m_context.entity_mgr.get_entity(m_player_handle);
  const SlowMotionComponent& smc = player;

  const glm::mat4 rot_z = glm::rotate(glm::mat4(1), m_camera_rotation_x, glm::vec3(0, 0, 1));
  CharacterControllerComponent& cc = player;
  cc.contents.set_move_vector(glm::vec3(rot_z * glm::vec4(right, forward, 0, 0)) *
                              smc.factor.get_converted_dt(m_context));
}

void PlayerSystem::reset_arrows() {
  for (auto& a : m_flying_arrows) {
    destroy_arrow(a.handle, m_context);
    m_unused_arrows.push_back(a.handle);
  }
  for (auto& a : m_stuck_arrows) {
    destroy_arrow(a.handle, m_context);
    m_unused_arrows.push_back(a.handle);
  }
  m_flying_arrows.clear();
  m_stuck_arrows.clear();
}

bool PlayerSystem::is_arrow_active(cor::EntityHandle arrow_handle) const {
  cor::Entity arrow = m_context.entity_mgr.get_entity(arrow_handle);
  if (arrow.is_valid())
    return arrow.has<ModelComponent>();
  else {
    LOG(WARNING) << "Arrow was invalid\n";
    return false;
  }
}

cor::EntityHandle PlayerSystem::get_new_arrow() {
  // Look for an unused arrow
  cor::EntityHandle handle;
  if (m_unused_arrows.size() > 0) {
    handle = m_unused_arrows[m_unused_arrows.size() - 1];
    m_unused_arrows.pop_back();
  }
  // Look for a stuck arrow
  else if (m_stuck_arrows.size() > 1) {
    if (m_stuck_arrows[0].handle == m_rope_arrow_handle) {
      handle = m_stuck_arrows[1].handle;
      m_stuck_arrows.erase(m_stuck_arrows.begin() + 1);
    } else {
      handle = m_stuck_arrows[0].handle;
      m_stuck_arrows.erase(m_stuck_arrows.begin());
    }
  }
  // Take from flying arrows
  else {
    if (m_flying_arrows[0].handle == m_rope_arrow_handle) {
      handle = m_flying_arrows[1].handle;
      m_flying_arrows.erase(m_flying_arrows.begin() + 1);
    } else {
      handle = m_flying_arrows[0].handle;
      m_flying_arrows.erase(m_flying_arrows.begin());
    }
  }

  cor::Entity arrow = m_context.entity_mgr.get_entity(handle);
  arrow.attach<ModelComponent>().asset = m_arrow_model;
  arrow.detach<ParticlesComponent>();
  arrow.detach<DestructibleComponent>();
  arrow.detach<ExplodeComponent>();
  arrow.detach<AttachToJointComponent>();
  arrow.detach<AttachToTriangleComponent>();
  arrow.get<TransformComponent>().transform.set_scale(0.8f, m_context);
  arrow.get<DamageComponent>().damage_value = 1;

  return handle;
}

void PlayerSystem::end_swing() {
  auto player = m_context.entity_mgr.get_entity(m_player_handle);
  CharacterControllerComponent& cc = player;

  cc.contents.end_swing();

  PlayerComponent& pc = player;
  pc.is_swinging = false;

  m_rope_arrow_handle = cor::EntityHandle::NULL_HANDLE;

  if (m_context.entity_mgr.is_valid(m_rope_handle))
    m_context.entity_mgr.destroy_entity(m_rope_handle);

  m_launch_help_dir = glm::vec3(0, 0, 0);
}

void PlayerSystem::shoot_rope_arrow() {
  // Destroy old rope arrow if it still in the air (otherwise handle will be NULLHANDLE)
  if (m_rope_arrow_handle != cor::EntityHandle::NULL_HANDLE && is_arrow_active(m_rope_arrow_handle))
    destroy_arrow(m_rope_arrow_handle, m_context);
  if (m_context.entity_mgr.is_valid(m_rope_handle))
    m_context.entity_mgr.destroy_entity(m_rope_handle);

  auto player = m_context.entity_mgr.get_entity(m_player_handle);

  m_rope_arrow_handle = get_new_arrow();
  auto arrow = m_context.entity_mgr.get_entity(m_rope_arrow_handle);

  // Position and rotation
  TransformComponent& tc = arrow;
  cor::Entity arrow_pos_entity = m_context.entity_mgr.get_entity(m_arrow_pos_handle);
  const glm::vec3 arrow_pos = arrow_pos_entity.get<TransformComponent>().transform.get_position();
  tc.transform.set_model_matrix(
      glm::translate(glm::mat4(1), arrow_pos) *
          glm::toMat4(
              glm::rotation(glm::vec3(0, 1, 0), glm::normalize(m_arrow_aim_point - arrow_pos))),
      m_context);

  // Hitbox and physics
  PhysicsProjectileComponent ppc;
  const float speed = 450.f;
  ppc.contents.set_velocity(tc.transform.get_model_matrix() * glm::vec4(0, speed, 0, 0));
  ppc.stick_to_animated = true;
  const float scale = 0.8f;
  ppc.contents.set_offset_transform(glm::scale(glm::vec3(scale, scale, scale)));
  ppc.contents.set_radius(0.07f);
  ppc.contents.set_length(0.07f);
  ppc.character_group = phy::CollisionGroup::ENEMY;
  arrow = ppc;

  // Particles
  ParticlesComponent pc;
  pc.max_life = .8f;
  pc.spawn_per_second = 250;
  pc.min_start_size = 0.5f;
  pc.max_start_size = 0.6f;
  arrow = pc;

  player.get<PlayerComponent>().is_attacking = true;
  m_time_left_of_attacking_anim = 0.15f;
  m_face_aiming_dir_states++;

  // Play bow release sound effect
  aud::SoundSettings s1;
  s1.gain = 0.8f;
  m_context.event_mgr.send_event(PlaySoundEvent(
      m_player_handle,
      bse::ASSETS_ROOT / "audio/effects/bow_and_arrow/release_arrow/post/release1.ogg"_fp, s1));

  // play arrow flying sound
  aud::SoundSettings s2;
  s2.gain = 0.1f;
  m_context.event_mgr.send_event(PlaySoundEvent(
      m_player_handle,
      bse::ASSETS_ROOT / "audio/effects/bow_and_arrow/flying_arrow/post/flying1.ogg"_fp, s2));

  // Rope
  m_rope_handle = m_context.entity_mgr.create_entity();
  auto rope = m_context.entity_mgr.get_entity(m_rope_handle);
  AttachToJointComponent atjc;
  atjc.entity_handle = m_player_handle;
  atjc.joint_name = "spine_01";
  atjc.offset_matrix = glm::translate(glm::vec3(-0.45f, 0.25f, 0.f));
  rope = atjc;
  rope = TransformComponent();

  RopeComponent rc;
  rc.texture = m_rope_texture;
  rc.other = m_rope_arrow_handle;
  rc.scale = 0.2f;
  rope = rc;

  arrow = ppc;
  arrow = tc;

  auto camera = m_context.entity_mgr.get_entity(m_camera_handle);
  const TransformComponent& ctc = camera;
  m_flying_arrows.push_back({m_rope_arrow_handle, 0.06f,
                             glm::normalize(m_camera_hit_point - ctc.transform.get_position()),
                             m_arrow_aim_point, m_max_reach / speed + 0.03f});

  // Launch help
  // CharacterControllerComponent& cc = player;
  // if (cc.contents.on_ground()) {
  //  context.event_mgr.send_event(JumpEvent());
  //  m_launch_help_active = true;
  //  m_launch_help_dir = glm::vec3(rot_z * glm::vec4(0, 12, 0, 0));
  //}
}

cor::RayResult* PlayerSystem::get_closest_non_player_hit(std::vector<cor::RayResult>& hits,
                                                         const bool filter_out_ai) {
  if (hits.size() == 0) return nullptr;

  // Find first non-player hit
  int index = -1;
  bool has_ai_component = false;
  cor::Entity possible_ai;
  do {
    ++index;
    if (index == hits.size()) return nullptr;
    if (filter_out_ai) {
      possible_ai = m_context.entity_mgr.get_entity(hits[index].handle);
      if (possible_ai.is_valid() && possible_ai.has<AIComponent>()) has_ai_component = true;
    }
  } while (hits[index].handle == m_player_handle || has_ai_component);
  cor::RayResult* closest = &hits[index];

  // Find the closest non-player hit
  for (unsigned int i = 0; i < hits.size(); ++i) {
    if (filter_out_ai) {
      possible_ai = m_context.entity_mgr.get_entity(hits[index].handle);
      if (possible_ai.is_valid() && possible_ai.has<AIComponent>()) has_ai_component = true;
    }
    if (i != index && hits[i].fraction < closest->fraction && closest->handle != m_player_handle &&
        !has_ai_component) {
      closest = &hits[i];
    }
  }

  return closest;
}
};  // namespace gmp