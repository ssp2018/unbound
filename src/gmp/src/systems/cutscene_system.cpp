#include "gmp/systems/cutscene_system.hpp"

#include "cor/entity.tcc"
#include "cor/entity_manager.tcc"
#include "gmp/components/cutscene_waypoint_component.hpp"
#include "gmp/components/name_component.hpp"
#include "gmp/components/projection_component.hpp"
#include "gmp/components/transform_component.hpp"
#include "gmp/systems/hud_system.hpp"
#include "gmp/systems/player_system.hpp"
#include <ext/ext.hpp>
#include <gmp/components/texture_component.hpp>
#include <gmp/components/transform_2d_component.hpp>

namespace gmp {

CutsceneSystem::CutsceneSystem(cor::FrameContext& context) {
  //

  {
    auto entity_handles = context.entity_mgr.get_entities(
        cor::Key::create<TransformComponent, CutsceneWaypointComponent>());

    m_waypoints.resize(entity_handles.size());

    for (auto& entity_handle : entity_handles) {
      cor::Entity entity = context.entity_mgr.get_entity(entity_handle);

      CutsceneWaypointComponent& cwc = entity;
      TransformComponent& tc = entity;

      m_waypoints[cwc.index] = Waypoint{cwc.type, tc.transform, cwc.fov};

      entity.destroy();
    }
  }

  {
    auto entity_handles = context.entity_mgr.get_entities(
        cor::Key::create<TransformComponent, ProjectionComponent, NameComponent>());

    for (auto& entity_handle : entity_handles) {
      NameComponent& nc = context.entity_mgr.get_entity(entity_handle);
      if (nc.name == "main_camera") {
        m_camera = entity_handle;
      }
    }

    ASSERT(m_camera != cor::EntityHandle::NULL_HANDLE)
        << "No camera named main_camera was found for use in the cutscene!";
  }
  context.event_mgr.send_event(DisableSystem{context.system_id_proxy.get_name<PlayerSystem>()});
  context.event_mgr.send_event(DisableSystem{context.system_id_proxy.get_name<HudSystem>()});

  {
    m_letterbox_upper = context.entity_mgr.create_entity();
    cor::Entity letterbox_upper = context.entity_mgr.get_entity(m_letterbox_upper);

    m_letterbox_lower = context.entity_mgr.create_entity();
    cor::Entity letterbox_lower = context.entity_mgr.get_entity(m_letterbox_lower);

    gmp::Transform2dComponent t2dc;
    t2dc.size = glm::vec2(1.f, 0.25f);
    t2dc.rotation = 0.f;
    {
      t2dc.position = glm::vec3(0.5, 0.0f, 0);
      letterbox_upper = t2dc;
    }
    {
      t2dc.position = glm::vec3(0.5, 1.0f, 0);
      letterbox_lower = t2dc;
    }

    gmp::TextureComponent texc;
    texc.texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/black_pixel.png"_fp);
    letterbox_upper = texc;
    letterbox_lower = texc;
  }
  LOG(NOTICE) << "CUTSCENE STARTED";

  // End the cutscene if the player jumps
  add_listener(context.event_mgr.register_handler<JumpEvent>(
      [&, this](const JumpEvent* ev) { end_cutscene(context); }));
}

CutsceneSystem::~CutsceneSystem() {
  //
}

void CutsceneSystem::read_update(const cor::FrameContext& context) {
  //
}

void CutsceneSystem::write_update(cor::FrameContext& context) {
  //
  if (m_cutscene_running) {
    int i0 = glm::clamp<int>(m_cutscene_time - 1, 0, m_waypoints.size() - 1);
    int i1 = glm::clamp<int>(m_cutscene_time, 0, m_waypoints.size() - 1);
    int i2 = glm::clamp<int>(m_cutscene_time + 1, 0, m_waypoints.size() - 1);
    int i3 = glm::clamp<int>(m_cutscene_time + 2, 0, m_waypoints.size() - 1);

    if (i1 == i3) {
      end_cutscene(context);
      return;
    }

    if (m_waypoints[i1].type == CutsceneWaypointComponent::Type::INSTANT) {
      i0 = i1;
    }

    if (m_waypoints[i2].type == CutsceneWaypointComponent::Type::INSTANT) {
      // i2 = i1;
      m_cutscene_time += 1.f;
      return;
    }

    if (m_waypoints[i3].type == CutsceneWaypointComponent::Type::INSTANT) {
      i3 = i2;
    }

    //
    if (m_waypoints[i1].fov != -1) {
      m_last_fov = m_waypoints[i1].fov;
    }

    float internal_t = glm::fract(m_cutscene_time);

    Transform new_transform;

    glm::vec3 look_at_0 =
        glm::vec3(m_waypoints[i0].transform.get_model_matrix() * glm::vec4(0, 0, -1, 1));
    glm::vec3 look_at_1 =
        glm::vec3(m_waypoints[i1].transform.get_model_matrix() * glm::vec4(0, 0, -1, 1));
    glm::vec3 look_at_2 =
        glm::vec3(m_waypoints[i2].transform.get_model_matrix() * glm::vec4(0, 0, -1, 1));
    glm::vec3 look_at_3 =
        glm::vec3(m_waypoints[i3].transform.get_model_matrix() * glm::vec4(0, 0, -1, 1));

    glm::vec3 look_at = glm::catmullRom(look_at_0, look_at_1, look_at_2, look_at_3, internal_t);

    // glm::vec3 up_0 = m_waypoints[i0].transform.get_model_matrix() * glm::vec4(0, 1, 0, 1);
    // glm::vec3 up_1 = m_waypoints[i1].transform.get_model_matrix() * glm::vec4(0, 1, 0, 1);
    // glm::vec3 up_2 = m_waypoints[i2].transform.get_model_matrix() * glm::vec4(0, 1, 0, 1);
    // glm::vec3 up_3 = m_waypoints[i3].transform.get_model_matrix() * glm::vec4(0, 1, 0, 1);

    glm::vec3 up = {0, 0, 1};  // glm::catmullRom(up_0, up_1, up_2, up_3, internal_t);

    glm::vec3 eye = glm::catmullRom(m_waypoints[i0].transform.get_position(),
                                    m_waypoints[i1].transform.get_position(),
                                    m_waypoints[i2].transform.get_position(),
                                    m_waypoints[i3].transform.get_position(), internal_t);

    new_transform.set_model_matrix(glm::inverse(glm::lookAt(eye, look_at, up)), context);

    cor::Entity camera = context.entity_mgr.get_entity(m_camera);
    TransformComponent& tc = camera;
    tc.transform = new_transform;

    ProjectionComponent& pc = camera;
    // pc.aspectRatio = (float)1920 / (float)1080;
    pc.field_of_view = m_last_fov;
    // pc.near = 0.1f;
    // pc.far = 1000.0f;
    pc.projection_matrix =
        glm::perspective(glm::radians(pc.field_of_view), pc.aspectRatio, pc.near, pc.far);

    m_cutscene_time += context.dt;
  }
}

void CutsceneSystem::end_cutscene(cor::FrameContext& context) {
  if (m_cutscene_running) {
    m_cutscene_running = false;
    m_cutscene_time = 0.f;
    context.event_mgr.send_event(EnableSystem{context.system_id_proxy.get_name<PlayerSystem>()});
    context.event_mgr.send_event(EnableSystem{context.system_id_proxy.get_name<HudSystem>()});

    cor::Entity letterbox_upper = context.entity_mgr.get_entity(m_letterbox_upper);
    cor::Entity letterbox_lower = context.entity_mgr.get_entity(m_letterbox_lower);

    letterbox_upper.destroy();
    letterbox_lower.destroy();

    context.event_mgr.send_event(CutsceneFinished{});

    LOG(NOTICE) << "CUTSCENE FINISHED";
  }
}

}  // namespace gmp