#include "gmp/systems/health_system.hpp"  // for Healt...

#include "bse/asset.hpp"           // for Asset
#include "bse/directory_path.hpp"  // for ASSET...
#include "bse/file_path.hpp"       // for opera...
#include "cor/frame_context.hpp"   // for Frame...
#include "cor/key.hpp"             // for Key
#include "gfx/texture.hpp"         // for Texture
#include "gmp/ai_script_entity_wrapper.hpp"
#include "gmp/components/transform_component.hpp"               // for Trans...
#include <cor/entity.tcc>                                       // for Entity
#include <cor/entity_manager.tcc>                               // for Entit...
#include <cor/event.hpp>                                        // for Colli...
#include <cor/event_manager.hpp>                                // for Event...
#include <gmp/components/ai_component.hpp>                      // for AICom...
#include <gmp/components/apply_damage_over_time_component.hpp>  // for Apply...
#include <gmp/components/damage_component.hpp>                  // for Damag...
#include <gmp/components/faction_component.hpp>
#include <gmp/components/health_component.hpp>    // for Healt...
#include <gmp/components/lifetime_component.hpp>  // for Lifet...
#include <gmp/components/name_component.hpp>
#include <gmp/components/particles_component.hpp>  // for Parti...
#include <gmp/components/player_component.hpp>
#include <gmp/components/shield_effect_component.hpp>
#include <gmp/components/take_damage_over_time_component.hpp>  // for Damag...
#include <gmp/gmp_helper_functions.hpp>

namespace cor {
struct EntityHandle;
}

gmp::HealthSystem::HealthSystem(cor::FrameContext& context) { event_subscription(context); }

void gmp::HealthSystem::event_subscription(cor::FrameContext& context) {
  add_listener(context.event_mgr.register_handler<CollisionEvent>([&context](
                                                                      const CollisionEvent* event) {
    //
    if (context.entity_mgr.is_valid(event->entity_1) &&
        context.entity_mgr.is_valid(event->entity_2)) {
      cor::Entity colliders[2] = {context.entity_mgr.get_entity(event->entity_1),
                                  context.entity_mgr.get_entity(event->entity_2)};

      for (int i = 0; i < 2; ++i) {
        if (colliders[i].has<DamageComponent>()) {
          // If it was an invalid projectile hit skip the collider
          if (!validate_projectile_collision(colliders[i % 2], colliders[(i + 1) % 2])) {
            continue;
          }
          DamageComponent& damage = colliders[i].get<DamageComponent>();
          if (damage.impact_damage_radius > 0) {
            TransformComponent& trans = colliders[i].get<TransformComponent>();
            bool has_dot;
            ApplyDamageOverTimeComponent* dot = nullptr;
            if (has_dot = colliders[i].has<ApplyDamageOverTimeComponent>())
              dot = &colliders[i].get<ApplyDamageOverTimeComponent>();

            cor::Key key = cor::Key::create<HealthComponent, TransformComponent>();
            auto entities = context.entity_mgr.get_entities(key);
            for (auto& entity_handle : entities) {
              cor::Entity other = context.entity_mgr.get_entity(entity_handle);

              TransformComponent& trans_other = context.entity_mgr.get_entity(entity_handle);
              float range = glm::distance(trans.transform.get_position(),
                                          trans_other.transform.get_position());

              // friendly fire check
              if (colliders[0].has<FactionComponent>() && colliders[1].has<FactionComponent>()) {
                FactionComponent& fc1 = colliders[0];
                FactionComponent& fc2 = colliders[1];
                if (fc1.type == fc2.type && !damage.friendly_fire) {
                  continue;
                }
              }

              if (range < damage.impact_damage_radius) {
                // deal damage to other
                auto entity = context.entity_mgr.get_entity(entity_handle);
                if (!entity.has<ShieldEffectComponent>()) {
                  HealthComponent& hp_other = entity;
                  hp_other.health_points -= damage.damage_value;
                  hp_other.last_hit_by = colliders[i].get_handle();

                  if (hp_other.health_points > 0)
                    context.event_mgr.send_event(DamageTaken({entity_handle}));
                }

                if (has_dot) {
                  // apply dot to other
                  if (!other.has<TakeDamageOverTimeComponent>())
                    other.attach<TakeDamageOverTimeComponent>();
                  TakeDamageOverTimeComponent& other_dot = other;
                  other_dot.dots.push_back(
                      {dot->tick_damage, dot->tick_interval, dot->tick_amount, dot->tick_interval});
                }
              }
            }
          } else {
            // damage only other i
            int other = 1 - i;

            // friendly fire check
            if (colliders[0].has<FactionComponent>() && colliders[1].has<FactionComponent>()) {
              FactionComponent& fc1 = colliders[0];
              FactionComponent& fc2 = colliders[1];
              if (fc1.type == fc2.type && !damage.friendly_fire) {
                return;
              }
            }

            if (colliders[other].has<HealthComponent>() &&
                !colliders[other].has<ShieldEffectComponent>()) {
              // deal dmg to other
              HealthComponent& other_hp = colliders[other];
              other_hp.health_points -= damage.damage_value;
              other_hp.last_hit_by = colliders[i].get_handle();

              if (other_hp.health_points > 0)
                context.event_mgr.send_event(DamageTaken({colliders[other].get_handle()}));

              // AI taken_damage_previous_frame...
              if (colliders[other].has<AIComponent>()) {
                AIComponent& aic = colliders[other];
                aic.damage_taken_previous_frame = damage.damage_value;
                aic.frame_hit_count += 1;
                context.event_mgr.send_event(MoraleBoostEvent(colliders[other].get_handle()));
                context.event_mgr.send_event(MoraleLossEvent(1, aic.morale_data.group_name,
                                                             colliders[other].get_handle(),
                                                             damage.damage_value));
              }

              if (colliders[i].has<ApplyDamageOverTimeComponent>()) {
                // apply dot to other
                ApplyDamageOverTimeComponent& dot =
                    colliders[i].get<ApplyDamageOverTimeComponent>();

                if (!colliders[other].has<TakeDamageOverTimeComponent>())
                  colliders[other].attach<TakeDamageOverTimeComponent>();
                TakeDamageOverTimeComponent& other_dot = colliders[other];
                other_dot.dots.push_back(
                    {dot.tick_damage, dot.tick_interval, dot.tick_amount, dot.tick_interval});
              }
            }
          }
        }
      }
    }
  }));

  add_listener(
      context.event_mgr.register_handler<WeakSpotHit>([&context](const WeakSpotHit* event) {
        //
        cor::Entity particle_entity =
            context.entity_mgr.get_entity(context.entity_mgr.create_entity());

        ParticlesComponent psc;
        psc.texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/crosshair.png"_fp);
        psc.max_life = .3f;
        psc.min_start_color = glm::vec4(1, 1, 0, 1);
        psc.max_start_color = glm::vec4(1, 1, 0, 1);
        psc.min_end_color = glm::vec4(0, 0, 0, 0);
        psc.max_end_color = glm::vec4(0, 0, 0, 0);
        psc.min_start_size = 0.1f;
        psc.max_start_size = 0.5f;
        psc.spawn_per_second = 80;
        const float var = 0.05f;
        psc.min_spawn_pos = glm::vec3(0, 0, 0);
        psc.max_spawn_pos = glm::vec3(0, 0, 0);
        psc.min_start_velocity = -event->hit_point_normal * 1.f + glm::vec3(0.1f, 0.1f, 0.1f);
        psc.max_start_velocity = -event->hit_point_normal * 3.f + glm::vec3(0.1f, 0.1f, 0.1f);

        particle_entity = psc;

        TransformComponent tfc;
        tfc.transform.set_position(event->hit_point, context);

        particle_entity = tfc;

        LifetimeComponent ltc;
        ltc.time_left = 0.4f;

        particle_entity = ltc;

        cor::Entity target_entity = context.entity_mgr.get_entity(event->target_entity);
        if (target_entity.is_valid() && target_entity.has<HealthComponent>()) {
          //
        }
      }));

  add_listener(
      context.event_mgr.register_handler<DamageTaken>([&context](const DamageTaken* event) {
        //
        cor::Entity entity = context.entity_mgr.get_entity(event->handle);
        if (entity.is_valid() && entity.has<HealthComponent>()) {
          const HealthComponent& hc = entity;
          if (hc.damage_sound.is_valid()) {
            context.event_mgr.send_event(PlaySoundEvent(
                entity.get_handle(), hc.damage_sound.get_path(), aud::SoundSettings()));
          }
        }
      }));
}

void gmp::HealthSystem::read_update(const cor::FrameContext& context) {}

void gmp::HealthSystem::write_update(cor::FrameContext& context) {
  // apply damage over time effect
  cor::Key key = cor::Key::create<HealthComponent, TakeDamageOverTimeComponent>();
  auto entities = context.entity_mgr.get_entities(key);
  for (auto& entity_handle : entities) {
    cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
    HealthComponent& hp = entity;
    TakeDamageOverTimeComponent& dot = entity;
    const bool immune = entity.has<ShieldEffectComponent>();
    for (int i = 0; i < dot.dots.size(); ++i) {
      if (dot.dots.at(i).tick_interval < (dot.dots.at(i).tick_timer += context.dt)) {
        dot.dots.at(i).tick_timer -= dot.dots.at(i).tick_interval;
        dot.dots.at(i).ticks_remaining--;
        if (!immune) {
          hp.health_points -= dot.dots.at(i).tick_damage;
          // if (hp.health_points > 0) context.event_mgr.send_event(DamageTaken({entity_handle}));
        }
        if (dot.dots.at(i).ticks_remaining == 0) {
          dot.dots.erase(dot.dots.begin() + i);
          if (dot.dots.size() == 0) {
            entity.detach<TakeDamageOverTimeComponent>();
          }
        }
      }
    }
  }
  // Check if any entity is dead
  cor::Key hc_key = cor::Key::create<HealthComponent>();
  auto hc_entities = context.entity_mgr.get_entities(hc_key);
  for (auto& entity_handle : hc_entities) {
    cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
    HealthComponent& hp = entity;
    if (hp.health_points <= 0) {
      std::function<void(gmp::AIScriptEntityWrapper&, cor::Entity, cor::EventManager&,
                         cor::EntityManager&)>
          death_callback = hp.death_callback;

      AIScriptEntityWrapper e(&entity, context);

      death_callback(e, entity, context.event_mgr, context.entity_mgr);
    }
  }
}