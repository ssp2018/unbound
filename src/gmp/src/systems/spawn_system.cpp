#include "gmp/systems/spawn_system.hpp"

#include "aud/sound_buffer.hpp"
#include "cor/entity.tcc"
#include "cor/entity_manager.tcc"
#include "gmp/components/attach_to_joint_component.hpp"
#include "gmp/components/cole_component.hpp"
#include "gmp/components/destroyed_component.hpp"
#include "gmp/components/dynamic_component.hpp"
#include "gmp/components/joint_look_at_component.hpp"
#include "gmp/components/lifetime_component.hpp"
#include "gmp/components/static_component.hpp"
#include "gmp/systems/ai_combat_system.hpp"
#include <cor/key.hpp>
#include <gmp/components/ai_component.hpp>
#include <gmp/components/animation_component.hpp>
#include <gmp/components/attach_to_joint_component.hpp>
#include <gmp/components/attached_handles_component.hpp>
#include <gmp/components/audio_component.hpp>
#include <gmp/components/character_controller_component.hpp>
#include <gmp/components/explode_component.hpp>
#include <gmp/components/faction_component.hpp>
#include <gmp/components/health_component.hpp>
#include <gmp/components/material_component.hpp>
#include <gmp/components/model_component.hpp>
#include <gmp/components/name_component.hpp>
#include <gmp/components/particles_component.hpp>
#include <gmp/components/player_component.hpp>
#include <gmp/components/point_light_component.hpp>
#include <gmp/components/shield_effect_component.hpp>
#include <gmp/components/spawn_component.hpp>
#include <gmp/components/steering_component.hpp>
#include <gmp/components/threat_component.hpp>
#include <gmp/components/transform_component.hpp>
#include <gmp/systems/audio_system.hpp>

namespace gmp {
SpawnSystem::SpawnSystem(cor::FrameContext& context) : m_context(context) {
  //
  add_listener(m_context.event_mgr.register_handler<TookHornEvent>(
      [this, &context](const TookHornEvent* event) { spawn_golem_boss(); }));
  add_listener(m_context.event_mgr.register_handler<SpawnColeEvent>(
      [this, &context](const SpawnColeEvent* event) {
        // Destroy the skull spawners
        auto& spawners =
            context.entity_mgr.get_entities_with_components<SpawnComponent, TransformComponent>();
        for (auto& s : spawners) {
          if (s.comp1.type == AIType::FLYING_SKULL) {
            auto entity = context.entity_mgr.get_entity(s.handle);
            entity = DestroyedComponent();
          }
        }

        // Remove all skulls
        auto& ai = context.entity_mgr.get_entities_with_components<AIComponent, ExplodeComponent>();
        for (auto& a : ai) {
          if (a.comp1.type == "flying_skull") {
            auto entity = context.entity_mgr.get_entity(a.handle);
            auto& handles = entity.get<AttachedHandlesComponent>().handles;
            for (auto at : handles) {
              auto e = context.entity_mgr.get_entity(at);
              if (e.is_valid()) {
                e.get<ParticlesComponent>().spawn_per_second = 0.f;
              }
            }
            entity.attach<LifetimeComponent>().time_left = 4.f;
            entity.detach<AIComponent>();
            entity.detach<ModelComponent>();
            entity.detach<HealthComponent>();
          }
        }

        spawn_cole_boss();
      }));

  m_death_scripts = bse::Asset<scr::Script>("assets/script/gmp/death_callbacks.lua");
  m_death_scripts.pre_load();

  m_grunt_model = bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/grunt.fbx"_fp);
  m_grunt_model.pre_load();

  m_skull_model = bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/skull.fbx"_fp);
  m_skull_model.pre_load();

  m_golem_model = bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/boss.fbx"_fp);
  m_golem_model.pre_load();

  m_cole_model = bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/cole.fbx"_fp);
  m_cole_model.pre_load();

  m_cole_armor_model = bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/armor.fbx"_fp);
  m_cole_armor_model.pre_load();
}

void SpawnSystem::read_update(const cor::FrameContext& context) {}

void SpawnSystem::write_update(cor::FrameContext& context) {
  cor::Key key = cor::Key::create<SpawnComponent, TransformComponent>();
  std::vector<cor::EntityHandle> entities = context.entity_mgr.get_entities(key);
  cor::Key player_key = cor::Key::create<PlayerComponent>();
  auto& player_entities = context.entity_mgr.get_entities(player_key);
  for (cor::EntityHandle& entity_handle : entities) {
    cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
    SpawnComponent& sc = entity;

    // loop through all spawned entities and remove them if needed
    for (std::vector<cor::EntityHandle>::iterator spawned_entity_handle_it =
             sc.spawned_enteties.begin();
         spawned_entity_handle_it != sc.spawned_enteties.end();) {
      if (!m_context.entity_mgr.is_valid(*spawned_entity_handle_it)) {
        spawned_entity_handle_it = sc.spawned_enteties.erase(spawned_entity_handle_it);
      } else
        spawned_entity_handle_it++;
    }

    if (sc.spawned_enteties.size() < sc.max_spawns) {
      sc.timer += m_context.dt;
      if (sc.timer > sc.delay) {
        if (sc.min_dist_to_player > 0.0f) {
          if (player_entities.size() > 0) {
            auto& player_entity = context.entity_mgr.get_entity(player_entities[0]);
            auto& player_tc = player_entity.get<TransformComponent>();
            auto& spawn_tc = entity.get<TransformComponent>();
            float dist_to_player = glm::distance(player_tc.transform.get_position(),
                                                 spawn_tc.transform.get_position());
            if (dist_to_player < sc.min_dist_to_player) {  // if player is to close to the spawner
                                                           // skip spawner respawn
              continue;
            }
          }
        }
        sc.timer = 0;
        switch (sc.type) {
          case AIType::MELEE_GRUNT:
            sc.spawned_enteties.push_back(spawn_melee_grunt(entity.get_handle()));
            break;
          case AIType::RANGED_GRUNT:
            LOG(WARNING) << "Ranged grunt has not yet been implemented.";
            break;
          case AIType::FLYING_SKULL:
            sc.spawned_enteties.push_back(spawn_skull(entity.get_handle()));
            break;
          default:
            LOG(WARNING)
                << "There requested monster to spawn did not have a defined spawning funciton!";
        }
      }
    }
  }
}

cor::EntityHandle SpawnSystem::spawn_melee_grunt(cor::EntityHandle owner) {
  auto ai_entity_handle = m_context.entity_mgr.create_entity();
  auto ai_entity = m_context.entity_mgr.get_entity(ai_entity_handle);
  auto aic = AIComponent();
  glm::vec3 ai_pos =
      m_context.entity_mgr.get_entity(owner).get<TransformComponent>().transform.get_position();
  aic.type = "melee_grunt";
  aic.state_name = "walking_around";
  aic.spawn_point = glm::vec3(ai_pos.x, ai_pos.y, ai_pos.z);
  aic.exploration_radius = 10.0f;
  aic.aggro_range =
      50 * m_context.entity_mgr.get_entity(owner).get<SpawnComponent>().aggro_range_scale;
  aic.disengagement_range =
      80.f * m_context.entity_mgr.get_entity(owner).get<SpawnComponent>().aggro_range_scale;
  aic.speed_base = 3.0f;
  aic.movement_speed = aic.speed_base;
  aic.base_attack_damage = 1;
  aic.morale_data = MoraleData(AIType::GOLEM);
  AICombatSystem::add_enemy_to_group("grunts1", ai_entity_handle);
  aic.morale_data.group_name = "grunts1";
  ai_entity = aic;
  auto ai_tc = ThreatComponent();
  ai_entity = ai_tc;
  auto aitc = TransformComponent();
  aitc.transform.translate(ai_pos, m_context);
  ai_entity = aitc;
  auto aimc = ModelComponent();
  aimc.asset = m_grunt_model;
  ai_entity = aimc;
  {
    AnimationComponent& ac1 = ai_entity.attach<AnimationComponent>();
    ac1.character_group = phy::CollisionGroup::ENEMY;
    ac1.state_machine.set_machine_and_state("grunt", "idle");
    ac1.character_group = phy::CollisionGroup::ENEMY;
  }
  ai_entity.attach<AudioComponent>();
  AudioComponent& ai_audc = ai_entity;

  ai_audc.audio.add_periodic_sound_group("walking");
  // Set a time period
  ai_audc.audio.set_intervall_time_limit("walking", 0.5f);

  ai_audc.audio.set_looping_sound_group_enabled("walking", true);
  // uncomment this line for boss footstep sounds
  // ai_audc.audio.set_periodic_sound_group_as_active("walking_around");

  /*ai_audc.audio.add_to_group("test1", bse::ASSETS_ROOT /
  "audio/effects/grunts/post/grunt4.ogg"_fp, aud::SoundSettings()); aud::SoundSettings
  settings;
   settings.pitch = 0.8f;
   ai_audc.audio.add_to_group("test2", bse::ASSETS_ROOT /
   "audio/effects/grunts/post/grunt4.ogg"_fp,
                             settings);
   ai_audc.audio.play_group_sound("test1");*/
  ai_entity.attach<FactionComponent>();
  FactionComponent& fc = ai_entity;
  fc.type = Faction::HUMAN;
  fc.sound_category = aud::SoundType::GRUNT_SOUND;
  ai_entity.attach<CharacterControllerComponent>();
  CharacterControllerComponent& ai_ccc = ai_entity;
  ai_ccc.contents.set_height(m_context, 1.0f);
  ai_ccc.contents.set_radius(m_context, 0.5f);
  ai_ccc.contents.set_offset_transform(
      glm::translate(
          glm::mat4(1.0f),
          glm::vec3(0, 0, -ai_ccc.contents.get_height() * 0.5f - ai_ccc.contents.get_radius())) *
      glm::rotate(glm::mat4(1), glm::radians(180.f), glm::vec3(0.f, 0.f, 1.f)));
  ai_ccc.contents.set_base_turning_speed(5.0f);
  ai_ccc.contents.set_turning_speed(5.0f);
  // auto& ai_cc = ai_entity.get<CharacterControllerComponent>();
  // ai_cc.contents.set_height(m_context, 1.6f);
  // ai_cc.contents.set_radius(m_context, 0.6f);
  ai_ccc.contents.set_mass(m_context, 70.f);
  ai_ccc.contents.set_gravity(glm::vec3(0, 0, -20.0f));
  ai_entity.attach<HealthComponent>();
  auto& ai_hc = ai_entity.get<HealthComponent>();
  ai_hc.max_health_points = 2;
  ai_hc.health_points = ai_hc.max_health_points;
  ai_hc.damage_sound =
      bse::Asset<aud::SoundBuffer>(bse::ASSETS_ROOT / "audio/effects/ouch/ouch.ogg"_fp);
  ai_hc.death_callback = m_death_scripts->return_value()["grunt_death_callback"];
  MaterialComponent mc = MaterialComponent();
  mc.material = MaterialType::FLESH;
  ai_entity = mc;
  return ai_entity_handle;
}

cor::EntityHandle SpawnSystem::spawn_ranged_grunt(cor::EntityHandle owner) {
  auto ai_entity_handle = m_context.entity_mgr.create_entity();
  auto ai_entity = m_context.entity_mgr.get_entity(ai_entity_handle);
  auto aic = AIComponent();
  // glm::vec3 ai_pos = glm::vec3(0, 7, 5);
  glm::vec3 ai_pos =
      m_context.entity_mgr.get_entity(owner).get<TransformComponent>().transform.get_position();
  aic.type = "ranged_grunt";
  aic.state_name = "walking_around";
  aic.spawn_point = glm::vec3(ai_pos.x, ai_pos.y, ai_pos.z);
  aic.exploration_radius = 30.0f;
  aic.aggro_range =
      50 * m_context.entity_mgr.get_entity(owner).get<SpawnComponent>().aggro_range_scale;
  aic.disengagement_range =
      80.f * m_context.entity_mgr.get_entity(owner).get<SpawnComponent>().aggro_range_scale;
  aic.speed_base = 4.0f;
  aic.movement_speed = aic.speed_base;
  aic.base_attack_damage = 1;
  aic.morale_data = MoraleData(AIType::RANGED_GRUNT);
  AICombatSystem::add_enemy_to_group("grunts1", ai_entity_handle);
  aic.morale_data.group_name = "grunts1";
  ai_entity = aic;
  auto ai_tc = ThreatComponent();
  ai_entity = ai_tc;
  auto aitc = TransformComponent();
  aitc.transform.translate(ai_pos, m_context);
  ai_entity = aitc;
  auto aimc = ModelComponent();
  aimc.asset = bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/grunt_ranged.fbx"_fp);
  ai_entity = aimc;
  {
    AnimationComponent& ac1 = ai_entity.attach<AnimationComponent>();
    ac1.character_group = phy::CollisionGroup::ENEMY;
    ac1.state_machine.set_machine_and_state("grunt", "idle");
    ac1.character_group = phy::CollisionGroup::ENEMY;
  }
  ai_entity.attach<AudioComponent>();
  AudioComponent& ai_audc = ai_entity;

  ai_audc.audio.add_periodic_sound_group("walking");
  // Set a time period
  ai_audc.audio.set_intervall_time_limit("walking", 0.5f);

  ai_audc.audio.set_looping_sound_group_enabled("walking", true);
  // uncomment this line for boss footstep sounds
  // ai_audc.audio.set_periodic_sound_group_as_active("walking_around");

  /*ai_audc.audio.add_to_group("test1", bse::ASSETS_ROOT /
  "audio/effects/grunts/post/grunt4.ogg"_fp, aud::SoundSettings()); aud::SoundSettings
  settings;
   settings.pitch = 0.8f;
   ai_audc.audio.add_to_group("test2", bse::ASSETS_ROOT /
   "audio/effects/grunts/post/grunt4.ogg"_fp,
                             settings);
   ai_audc.audio.play_group_sound("test1");*/
  ai_entity.attach<FactionComponent>();
  FactionComponent& fc = ai_entity;
  fc.type = Faction::HUMAN;
  fc.sound_category = aud::SoundType::GRUNT_SOUND;
  ai_entity.attach<CharacterControllerComponent>();
  CharacterControllerComponent& ai_ccc = ai_entity;
  ai_ccc.contents.set_height(m_context, 1.0f);
  ai_ccc.contents.set_radius(m_context, 0.5f);
  ai_ccc.contents.set_offset_transform(glm::translate(
      glm::mat4(1.0f),
      glm::vec3(0, 0, -ai_ccc.contents.get_height() * 0.5f - ai_ccc.contents.get_radius())));
  ai_ccc.contents.set_base_turning_speed(0.4);
  ai_ccc.contents.set_turning_speed(0.4);
  auto& ai_cc = ai_entity.get<CharacterControllerComponent>();
  // ai_cc.contents.set_height(m_context, 1.6f);
  // ai_cc.contents.set_radius(m_context, 0.6f);
  ai_cc.contents.set_mass(m_context, 70.f);
  ai_cc.contents.set_gravity(glm::vec3(0, 0, -20.0f));
  ai_entity.attach<HealthComponent>();
  auto& ai_hc = ai_entity.get<HealthComponent>();
  ai_hc.max_health_points = 4;
  ai_hc.health_points = 4;
  ai_hc.death_callback = m_death_scripts->return_value()["ai_death_callback"];
  MaterialComponent mc = MaterialComponent();
  mc.material = MaterialType::FLESH;
  ai_entity = mc;
  return ai_entity_handle;
}

cor::EntityHandle SpawnSystem::spawn_skull(cor::EntityHandle owner) {
  auto ai_entity_handle = m_context.entity_mgr.create_entity();
  auto ai_entity = m_context.entity_mgr.get_entity(ai_entity_handle);
  auto aic = AIComponent();

  gmp::SpawnComponent& owner_spawn = m_context.entity_mgr.get_entity(owner);

  glm::vec3 offset = glm::vec3(((double)rand() / (RAND_MAX)), ((double)rand() / (RAND_MAX)),
                               ((double)rand() / (RAND_MAX)));

  offset = glm::normalize(offset);
  offset *= 20;
  glm::vec3 ai_pos =
      m_context.entity_mgr.get_entity(owner).get<TransformComponent>().transform.get_position() +
      offset;
  aic.type = "flying_skull";
  aic.state_name = "flying_around";
  aic.spawn_point = glm::vec3(ai_pos.x, ai_pos.y, ai_pos.z);
  aic.exploration_radius = 50.0f;
  aic.aggro_range =
      200 * m_context.entity_mgr.get_entity(owner).get<SpawnComponent>().aggro_range_scale;
  aic.disengagement_range =
      250.f * m_context.entity_mgr.get_entity(owner).get<SpawnComponent>().aggro_range_scale;
  aic.speed_base = 3.0f;
  aic.movement_speed = aic.speed_base;
  aic.death_sound_path = "assets/audio/effects/flying_skull/Dying/dying1.ogg"_fp;
  aic.death_sound_settings.gain = 7;
  aic.cutoff_dist = owner_spawn.cutoff_dist;
  ai_entity = aic;
  auto ai_tc = ThreatComponent();
  ai_entity = ai_tc;
  auto aitc = TransformComponent();
  aitc.transform.translate(ai_pos, m_context);
  ai_entity = aitc;
  auto aimc = ModelComponent();
  aimc.asset = m_skull_model;
  ai_entity = aimc;
  ai_entity.attach<FactionComponent>();
  FactionComponent& fc = ai_entity;
  fc.type = Faction::DEMON;
  fc.sound_category = aud::SoundType::DEMON_SOUND;
  ai_entity.attach<CharacterControllerComponent>();
  CharacterControllerComponent& ai_ccc = ai_entity;
  ai_ccc.contents.set_height(m_context, 3.f);
  ai_ccc.contents.set_radius(m_context, 3.f);
  ai_ccc.contents.set_gravity(glm::vec3(0, 0, 0));
  ai_ccc.contents.set_base_turning_speed(0.4);
  ai_ccc.contents.set_mass(m_context, 70.f);
  ai_ccc.contents.set_move_vector(glm::vec3(0, 0, 0));
  ai_ccc.contents.set_offset_transform(glm::rotate(glm::pi<float>(), glm::vec3(0, 0, 1)) *
                                       glm::scale(glm::mat4(1), glm::vec3(2, 2, 2)));
  ai_ccc.contents.set_use_navmesh_collision(false);
  ai_ccc.contents.set_is_flying(true);
  ExplodeComponent ec;
  ec.max_damage = 0;
  ec.damage_radius = 0;
  ec.particle_speed = 70;
  ec.particle_amount = 800;
  ec.particle_start_size = 0.6f;
  ec.particle_end_size = 1.3f;
  ec.time = 0.9;
  ec.min_start_color = glm::vec4(0.3, 0, 0.2, 1);
  ec.max_start_color = glm::vec4(0.4, 0, 0.3, 1);
  ec.min_end_color = glm::vec4(0, 0, 0, 1);
  ec.max_end_color = glm::vec4(0.1, 0, 0, 1);
  ec.texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/explosion.png"_fp);
  ai_entity = ec;

  auto ai_hc = HealthComponent();
  ai_hc.max_health_points = 4;
  ai_hc.health_points = 4;
  ai_hc.damage_sound =
      bse::Asset<aud::SoundBuffer>(bse::ASSETS_ROOT / "audio/effects/ouch/ouch3.ogg"_fp);
  ai_hc.death_callback = m_death_scripts->return_value()["skull_death_callback"];
  ai_entity = ai_hc;
  MaterialComponent mc = MaterialComponent();
  mc.material = MaterialType::MAGICAL;
  ai_entity = mc;
  AnimationComponent ac;
  ac.character_group = phy::CollisionGroup::ENEMY;
  ac.state_machine.set_machine_and_state("skull", "idle");
  ac.character_group = phy::CollisionGroup::ENEMY;
  ai_entity = ac;
  SteeringComponent sc;
  ai_entity = sc;
  AudioComponent audc;
  audc.audio.add_periodic_sound_group("skull_repeating");
  audc.audio.set_looping_sound_group_enabled("skull_repeating", true);
  audc.audio.set_intervall_time_limit("skull_repeating", 7.f);
  audc.audio.set_ss_periodic_sound("skull_repeating", aud::SoundSettings());

  ai_entity = std::move(audc);

  AttachedHandlesComponent ahc;
  // Attached particle systems
  ParticlesComponent pc;
  pc.spawn_per_second = 150;
  pc.min_life = 0.25f;
  pc.max_life = 0.30f;
  pc.min_rotation_speed = 5.f;
  pc.max_rotation_speed = 7.f;
  pc.min_start_size = 0.2f;
  pc.max_start_size = 0.3f;
  pc.min_end_size = 0.15f;
  pc.max_end_size = 0.2f;
  pc.size_mode = ParticlesComponent::Size::CHANGING;
  pc.min_spawn_pos = glm::vec3(-0.22f);
  pc.max_spawn_pos = glm::vec3(0.22f);
  pc.min_start_velocity = glm::vec3(0, 0, 1);
  pc.max_start_velocity = glm::vec3(0, 0, 2);
  pc.min_start_color = glm::vec4(0.4, 0, 0.4, 1);
  pc.max_start_color = glm::vec4(0.5, 0, 0.5, 1);
  pc.min_end_color = glm::vec4(0, 0, 0, 1);
  pc.max_end_color = glm::vec4(0, 0, 0, 1);

  PointLightComponent plc;
  plc.color = glm::vec4(1, 0, 1, 1);
  plc.radius = 10.f;
  // Left eye
  {
    cor::EntityHandle left_eye_handle = m_context.entity_mgr.create_entity();
    cor::Entity left_eye = m_context.entity_mgr.get_entity(left_eye_handle);
    left_eye = TransformComponent();
    AttachToJointComponent atjc;
    atjc.entity_handle = ai_entity_handle;
    atjc.joint_name = "L_eye";
    atjc.offset_matrix = glm::translate(glm::vec3(-5, -5, 0));
    left_eye = atjc;
    left_eye = pc;
    left_eye = plc;
    ahc.handles[0] = left_eye_handle;
  }
  // Right eye
  {
    cor::EntityHandle right_eye_handle = m_context.entity_mgr.create_entity();
    cor::Entity right_eye = m_context.entity_mgr.get_entity(right_eye_handle);
    right_eye = TransformComponent();
    AttachToJointComponent atjc;
    atjc.entity_handle = ai_entity_handle;
    atjc.joint_name = "R_eye";
    atjc.offset_matrix = glm::translate(glm::vec3(5, -5, 0));
    right_eye = atjc;
    right_eye = pc;
    right_eye = plc;
    ahc.handles[1] = right_eye_handle;
  }
  // Mouth
  cor::EntityHandle mouth_light_handle = m_context.entity_mgr.create_entity();
  cor::Entity mouth_light = m_context.entity_mgr.get_entity(mouth_light_handle);
  mouth_light = TransformComponent();
  AttachToJointComponent atjc;
  atjc.entity_handle = ai_entity_handle;
  atjc.joint_name = "root";
  atjc.offset_matrix = glm::translate(glm::vec3(210, 0, 0));
  mouth_light = atjc;
  pc.spawn_per_second = 13.f;
  pc.min_life = 1.f;
  pc.max_life = 2.f;
  pc.min_rotation_speed = 3.f;
  pc.max_rotation_speed = 5.f;
  pc.min_start_velocity = glm::vec3(0, 0, 0.5);
  pc.max_start_velocity = glm::vec3(0, 0, 1);
  pc.min_start_size = 1.7f;
  pc.max_start_size = 1.9f;
  pc.min_end_size = 0.2f;
  pc.max_end_size = 0.2f;
  mouth_light = pc;
  plc.radius = 20.f;
  mouth_light = plc;
  ahc.handles[2] = mouth_light_handle;

  ai_entity = ahc;

  return ai_entity_handle;
}  // namespace gmp

void SpawnSystem::spawn_golem_boss() {
  auto boss_entity_handle = m_context.entity_mgr.create_entity();
  auto boss_entity = m_context.entity_mgr.get_entity(boss_entity_handle);
  auto boss_aic = AIComponent();

  glm::vec3 boss_pos = glm::vec3(0);
  cor::Key key = cor::Key::create<NameComponent, TransformComponent>();
  auto handles = m_context.entity_mgr.get_entities(key);
  for (auto handle : handles) {
    cor::Entity entity = m_context.entity_mgr.get_entity(handle);
    if (entity.get<NameComponent>().name == "boss_golem_spawn") {
      boss_pos = entity.get<TransformComponent>().transform.get_position();
      break;
    }
  }

  boss_aic.type = "boss_golem";
  boss_aic.state_name = "walking_around";
  boss_aic.spawn_point = glm::vec3(boss_pos.x, boss_pos.y, boss_pos.z);
  boss_aic.exploration_radius = 50;
  boss_aic.aggro_range = 150;
  boss_aic.disengagement_range = 200;
  boss_aic.speed_base = 10.0f;
  boss_aic.movement_speed = boss_aic.speed_base;
  boss_aic.base_attack_damage = 1;
  boss_entity = boss_aic;
  boss_aic.morale_data = MoraleData(AIType::GOLEM);
  auto boss_tc = ThreatComponent();
  boss_entity = boss_tc;
  auto boss_trans = TransformComponent();
  boss_trans.transform.translate(boss_pos, m_context);
  boss_entity = boss_trans;
  auto boss_model = ModelComponent();
  boss_model.asset = m_golem_model;
  boss_entity = boss_model;
  auto boss_name = NameComponent();
  boss_name.name = "Golem";
  boss_entity = boss_name;
  {
    AnimationComponent& boss_ac1 = boss_entity.attach<AnimationComponent>();
    boss_ac1.character_group = phy::CollisionGroup::ENEMY;
    boss_ac1.state_machine.set_machine_and_state("golem", "idle");
    boss_ac1.character_group = phy::CollisionGroup::ENEMY;
  }
  boss_entity.attach<AudioComponent>();
  AudioComponent& boss_audc = boss_entity;
  // Create a periodic sound group
  boss_audc.audio.add_periodic_sound_group("walking");
  // Set a time period
  boss_audc.audio.set_intervall_time_limit("walking", 0.5f);
  // Add sounds to the newly created periodic sound group
  m_context.audio_mgr.add_sound_group("walking");
  m_context.audio_mgr.add_to_group("walking", "footsteps1",
                                   bse::ASSETS_ROOT / "audio/effects/boss1/footstep1.ogg"_fp);
  m_context.audio_mgr.add_to_group("walking", "footsteps2",
                                   bse::ASSETS_ROOT / "audio/effects/boss1/footstep2.ogg"_fp);
  m_context.audio_mgr.add_to_group("walking", "footsteps3",
                                   bse::ASSETS_ROOT / "audio/effects/boss1/footstep3.ogg"_fp);
  m_context.audio_mgr.add_to_group("walking", "footsteps4",
                                   bse::ASSETS_ROOT / "audio/effects/boss1/footstep4.ogg"_fp);
  m_context.audio_mgr.add_to_group("walking", "footsteps5",
                                   bse::ASSETS_ROOT / "audio/effects/boss1/footstep5.ogg"_fp);
  boss_audc.audio.set_looping_sound_group_enabled("walking", true);

  aud::SoundSettings boss_melee_attack_ss = aud::SoundSettings();
  boss_melee_attack_ss.rolloff_factor = 0.25f;
  // boss_melee_attack.gain = 3.f;
  m_context.audio_mgr.add_sound_group("boss_melee_attack");
  m_context.audio_mgr.add_to_group("boss_melee_attack", "melee1",
                                   bse::ASSETS_ROOT / "audio/effects/boss1/melee1.ogg"_fp);
  m_context.audio_mgr.add_to_group("boss_melee_attack", "melee2",
                                   bse::ASSETS_ROOT / "audio/effects/boss1/melee2.ogg"_fp);
  m_context.audio_mgr.add_to_group("boss_melee_attack", "melee3",
                                   bse::ASSETS_ROOT / "audio/effects/boss1/melee3.ogg"_fp);

  boss_entity.attach<FactionComponent>();
  FactionComponent& boss_fc = boss_entity;
  boss_fc.type = Faction::HUMAN;
  boss_fc.sound_category = aud::SoundType::GRUNT_SOUND;
  boss_entity.attach<CharacterControllerComponent>();
  CharacterControllerComponent& boss_ccc = boss_entity;
  boss_ccc.contents.set_height(m_context, 18.0f);
  boss_ccc.contents.set_radius(m_context, 10.0f);
  boss_ccc.contents.set_offset_transform(glm::scale(
      glm::translate(
          glm::mat4(1.f),
          glm::vec3(0, 0, -boss_ccc.contents.get_height() * 0.5f - boss_ccc.contents.get_radius())),
      glm::vec3(4, 4, 4)));
  boss_ccc.contents.set_base_turning_speed(0.4);
  boss_ccc.contents.set_turning_speed(0.4);
  boss_ccc.contents.set_mass(m_context, 800.f);
  boss_ccc.contents.set_gravity(glm::vec3(0, 0, -20.0f));
  boss_entity.attach<HealthComponent>();
  auto& boss_hc = boss_entity.get<HealthComponent>();
  boss_hc.max_health_points = 80;
  boss_hc.health_points = 80;
  boss_hc.death_callback = m_death_scripts->return_value()["golem_death_callback"];
  MaterialComponent mc = MaterialComponent();
  mc.material = MaterialType::FLESH;
  boss_entity = mc;

  JointLookAtComponent jlac;
  jlac.m_joint_name = "neck";
  boss_entity = jlac;
}

void SpawnSystem::spawn_cole_boss() {
  // Cole
  auto cole_entity_handle = m_context.entity_mgr.create_entity();
  auto cole_entity = m_context.entity_mgr.get_entity(cole_entity_handle);
  auto aic_cole = AIComponent();
  aic_cole.type = "boss_cole";
  aic_cole.state_name = "idle";
  aic_cole.aggro_range = 1000;
  aic_cole.base_attack_damage = 4;  // I assume that this is how much damage the beam will do
  // aic_cole.death_sound_path = "";   // TODO insert a death sound path here
  aic_cole.death_sound_settings = aud::SoundSettings();  // default, may need to tweak the gain
  aic_cole.disengagement_range = 1000;
  aic_cole.exploration_radius = 1000;
  aic_cole.morale_data =
      MoraleData(AIType::GOLEM);  // TODO: rename, cole as the hook boos, should never flee
  aic_cole.movement_speed = 0;    // will never move, just teleport

  // Start at the first spawn position it finds
  glm::vec3 cole_spawn_pos = glm::vec3(0);
  auto& positions =
      m_context.entity_mgr.get_entities_with_components<NameComponent, TransformComponent>();
  for (auto& p : positions) {
    if (p.comp1.name == "cole_teleport_point") {
      cole_spawn_pos = p.comp2.transform.get_position();
      break;
    }
  }

  aic_cole.spawn_point = cole_spawn_pos;
  aic_cole.speed_base = 0;  // will never move, just teleport
  cole_entity.attach(aic_cole);

  NameComponent cole_nc;
  cole_nc.name = "COLE";
  cole_entity = cole_nc;

  TransformComponent tc;
  tc.transform.set_position(cole_spawn_pos, m_context);
  // tc.transform.set_scale(1.43f, m_context);
  cole_entity = tc;

  /*CharacterControllerComponent cole_ccc;
  cole_ccc.contents.set_height(m_context, 0.f);
  cole_ccc.contents.set_radius(m_context, 25.f);
  cole_ccc.contents.set_gravity(glm::vec3(0, 0, 0));
  cole_ccc.contents.set_base_turning_speed(0.4);
  cole_ccc.contents.set_mass(m_context, 70.f);
  cole_ccc.contents.set_move_vector(glm::vec3(0, 0, 0));
  cole_ccc.contents.set_offset_transform(glm::rotate(glm::pi<float>(), glm::vec3(0, 0, 1)) *
                                         glm::scale(glm::mat4(1), glm::vec3(2, 2, 2)));

  cole_entity = cole_ccc;*/

  ModelComponent mc;
  mc.asset = m_cole_model;
  cole_entity = mc;

  MaterialComponent cole_mc;
  cole_mc.material = MaterialType::MAGICAL;
  cole_entity = cole_mc;

  SpawnComponent cole_sc;
  cole_sc.delay = 10;
  cole_sc.aggro_range_scale = 5.f;
  // change this number to spawn skulls
  cole_sc.max_spawns = 0;
  cole_sc.cutoff_dist = 3500;
  cole_sc.type = AIType::FLYING_SKULL;
  cole_entity = cole_sc;

  COLEComponent colec;
  int index = 0;
  for (int x = 0; x < 4; x++) {
    for (int y = 0; y < 4; y++) {
      for (int z = 0; z < 4; z++) {
        if (x < 1 || x > 2 || y < 1 || y > 2 || z < 1 || z > 2) {
          auto armor_piece_h = m_context.entity_mgr.create_entity();
          auto armor_piece = m_context.entity_mgr.get_entity(armor_piece_h);

          colec.body[index++] = armor_piece_h;

          ModelComponent armor_piece_mc;
          armor_piece_mc.asset = m_cole_armor_model;
          armor_piece = armor_piece_mc;
          auto& skel = mc.asset->get_model_data().skel->get_joint_map();
          int val = skel.size();
          TransformComponent armor_piece_tc;
          armor_piece_tc.transform.set_scale(1.5f, m_context);
          armor_piece = armor_piece_tc;

          StaticComponent armor_piece_sc;
          armor_piece_sc.contents.set_collission_shape(
              m_context, std::make_shared<phy::BoxCollisionShape>(glm::vec3(1.f, 1.f, 1.f)));
          armor_piece = armor_piece_sc;

          HealthComponent armor_hc;
          armor_hc.max_health_points = 3;
          armor_hc.health_points = 3;
          armor_hc.death_callback = m_death_scripts->return_value()["armor_death_callback"];
          armor_piece = armor_hc;

          FactionComponent armor_fc;
          armor_fc.sound_category = aud::DEMON_SOUND;
          armor_fc.type = Faction::DEMON;
          armor_piece = armor_fc;

          AttachToJointComponent armor_piece_atjc;
          armor_piece_atjc.entity_handle = cole_entity_handle;
          armor_piece_atjc.joint_name = "j_" + std::to_string(x + 1) + '_' + std::to_string(y + 1) +
                                        '_' + std::to_string(z + 1);
          armor_piece = armor_piece_atjc;
        }
      }
    }
  }
  cole_entity = colec;

  FactionComponent cole_fc;
  cole_fc.sound_category = aud::DEMON_SOUND;
  cole_fc.type = Faction::DEMON;
  cole_entity = cole_fc;

  HealthComponent cole_hc;
  cole_hc.death_callback = m_death_scripts->return_value()["cole_box_death_callback"];
  cole_hc.max_health_points = 5;
  cole_hc.health_points = 5;
  cole_entity = cole_hc;

  ThreatComponent cole_tc;
  cole_entity = cole_tc;

  CharacterControllerComponent cole_ccc;
  cole_ccc.contents.set_use_navmesh_collision(false);
  cole_ccc.contents.set_is_flying(true);
  cole_ccc.contents.set_height(m_context, 0.f);
  cole_ccc.contents.set_radius(m_context, 8.f);
  cole_entity = cole_ccc;

  AudioComponent cole_ac;
  cole_entity = std::move(cole_ac);
  ShieldEffectComponent sec;
  cole_entity = sec;

  {
    AnimationComponent& boss_ac1 = cole_entity.attach<AnimationComponent>();
    boss_ac1.character_group = phy::CollisionGroup::ENEMY;
    boss_ac1.state_machine.set_machine_and_state("COLE", "idle");
    boss_ac1.character_group = phy::CollisionGroup::ENEMY;
  }
}
}  // namespace gmp
