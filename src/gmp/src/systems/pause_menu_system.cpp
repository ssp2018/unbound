#include "gmp/systems/pause_menu_system.hpp"

#include "cor/entity_manager.tcc"  // for EntityManager
#include "cor/frame_context.hpp"   // for FrameContext
#include "cor/key.hpp"             // for Key
#include "gmp/components/name_component.hpp"
#include "gmp/components/transform_2d_component.hpp"
#include <cor/entity.tcc>  // for Entity

namespace gmp {
PauseMenuSystem::PauseMenuSystem(cor::FrameContext &context, gfx::Window &window)
    : m_context(context), m_window(window) {
  //
  cor::Entity continue_entity =
      m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());
  {
    GraphicTextComponent gtc;
    gtc.outline_color = {128, 107, 0};
    gtc.color = {255, 215, 0};
    gtc.outline_thickness = 0.05f;
    gtc.font = "arial";
    gtc.text = "Continue";
    continue_entity = gtc;

    gmp::Transform2dComponent t2dc;
    t2dc.position = glm::vec3(0.5f, 1.4f, 0);
    t2dc.size = glm::vec2(0.15f, 0.15f);
    t2dc.rotation = 0.f;
    t2dc.centering = Transform2dComponent::Centering::CENTER;
    continue_entity = t2dc;

    gmp::NameComponent nc;
    nc.name = "Continue";
    continue_entity = nc;

    m_continue_handle = continue_entity.get_handle();
  }

  cor::Entity menu_entity = m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());
  {
    GraphicTextComponent gtc;
    gtc.outline_color = {128, 107, 0};
    gtc.color = {255, 215, 0};
    gtc.outline_thickness = 0.05f;
    gtc.font = "arial";
    gtc.text = "Main menu";
    menu_entity = gtc;

    gmp::Transform2dComponent t2dc;
    t2dc.position = glm::vec3(0.5f, 1.5f, 0);
    t2dc.size = glm::vec2(0.15f, 0.15f);
    t2dc.rotation = 0.f;
    t2dc.centering = Transform2dComponent::Centering::CENTER;
    menu_entity = t2dc;

    gmp::NameComponent nc;
    nc.name = "Menu";
    menu_entity = nc;

    m_menu_handle = menu_entity.get_handle();
  }

  cor::Entity quit_entity = m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());
  {
    GraphicTextComponent gtc;
    gtc.outline_color = {128, 107, 0};
    gtc.color = {255, 215, 0};
    gtc.outline_thickness = 0.05f;
    gtc.font = "arial";
    gtc.text = "Quit game";
    quit_entity = gtc;

    gmp::Transform2dComponent t2dc;
    t2dc.position = glm::vec3(0.5f, 1.6f, 0);
    t2dc.size = glm::vec2(0.15f, 0.15f);
    t2dc.rotation = 0.f;
    t2dc.centering = Transform2dComponent::Centering::CENTER;
    quit_entity = t2dc;

    gmp::NameComponent nc;
    nc.name = "Quit";
    quit_entity = nc;

    m_quit_handle = quit_entity.get_handle();
  }

  aud::SoundSettings settings;
  settings.loop = false;
  settings.gain = 0.75f;
  m_select_sound = std::move(bse::RuntimeAsset<aud::SoundSource>(
      bse::Asset<aud::SoundBuffer>("assets/audio/effects/bow_and_arrow/hit/post/hit2.ogg"),
      settings));
  m_click_sound = std::move(bse::RuntimeAsset<aud::SoundSource>(
      bse::Asset<aud::SoundBuffer>("assets/audio/effects/bow_and_arrow/hit/post/hit1.ogg"),
      settings));

  // Change to explosive arrow
  add_listener(
      context.event_mgr.register_handler<PauseGameEvent>([this](const PauseGameEvent *event) {
        m_paused = !m_paused;

        if (m_paused) {
          show_menu();
        } else {
          hide_menu();
        }
      }));
}

void PauseMenuSystem::show_menu() {
  std::cout << "show" << std::endl;
  cor::Entity continue_entity = m_context.entity_mgr.get_entity(m_continue_handle);
  {
    gmp::Transform2dComponent &t2dc = continue_entity;
    t2dc.position = glm::vec3(t2dc.position.x, .4f, t2dc.position.z);
  }
  cor::Entity menu_entity = m_context.entity_mgr.get_entity(m_menu_handle);
  {
    gmp::Transform2dComponent &t2dc = menu_entity;
    t2dc.position = glm::vec3(t2dc.position.x, .5f, t2dc.position.z);
  }
  cor::Entity quit_entity = m_context.entity_mgr.get_entity(m_quit_handle);
  {
    gmp::Transform2dComponent &t2dc = quit_entity;
    t2dc.position = glm::vec3(t2dc.position.x, .6f, t2dc.position.z);
  }
}

void PauseMenuSystem::hide_menu() {
  cor::Entity continue_entity = m_context.entity_mgr.get_entity(m_continue_handle);
  {
    gmp::Transform2dComponent &t2dc = continue_entity;
    t2dc.position = glm::vec3(t2dc.position.x, 1.4, t2dc.position.z);
  }
  cor::Entity menu_entity = m_context.entity_mgr.get_entity(m_menu_handle);
  {
    gmp::Transform2dComponent &t2dc = menu_entity;
    t2dc.position = glm::vec3(t2dc.position.x, 1.5, t2dc.position.z);
  }
  cor::Entity quit_entity = m_context.entity_mgr.get_entity(m_quit_handle);
  {
    gmp::Transform2dComponent &t2dc = quit_entity;
    t2dc.position = glm::vec3(t2dc.position.x, 1.6, t2dc.position.z);
  }
}

void PauseMenuSystem::read_update(const cor::FrameContext &context) {}

void PauseMenuSystem::write_update(cor::FrameContext &context) {
  if (m_paused) {
    int button = glfwGetMouseButton(m_window.get_window(), GLFW_MOUSE_BUTTON_LEFT);

    double xPos, yPos, width, height;
    width = m_window.get_width();
    height = m_window.get_height();

    glfwGetCursorPos(m_window.get_window(), &xPos, &yPos);

    std::string name = "";

    cor::Key key = cor::Key::create<Transform2dComponent, GraphicTextComponent, NameComponent>();
    auto entities = m_context.entity_mgr.get_entities(key);
    for (auto handle : entities) {
      cor::Entity entity = m_context.entity_mgr.get_entity(handle);
      Transform2dComponent &tc = entity;
      GraphicTextComponent &text = entity;
      text.outline_thickness = 0.05f;
      NameComponent &nc = entity;

      glm::vec2 position = tc.position;
      glm::vec2 size = tc.size;
      size.y *= .33;
      position.x -= size.x / 2.f;
      position.y -= size.y * 0.75f;

      if (xPos > position.x * width && xPos < (position.x + size.x) * width) {
        if (yPos > position.y * height && yPos < (position.y + size.y) * height) {
          name = nc.name;
          text.outline_thickness = 0.1f;
        }
      }
    }

    if (m_highlighted_entity_name == "" || m_highlighted_entity_name != name) {
      if (name != "") {
        m_select_sound->play(true);
      }

      m_highlighted_entity_name = name;
    }

    if (button == GLFW_PRESS) {
      if (name != "") {
        if (m_clicked_entity_name != name) {
          m_click_sound->play(true);
        }
        m_clicked_entity_name = name;
      }
    } else if (button == GLFW_RELEASE) {
      if (name != "" && m_clicked_entity_name == name) {
        if (name == "Continue") {
          m_context.event_mgr.send_event(PauseGameEvent());
        } else if (name == "Menu") {
          m_context.event_mgr.send_event(EnterStartEvent());
        } else if (name == "Quit") {
          m_context.event_mgr.send_event(CloseApplication());
        }
      }
      m_clicked_entity_name = "";
    }
  }
}

}  // namespace gmp