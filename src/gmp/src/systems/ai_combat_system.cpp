#include <cor/entity.tcc>
#include <gmp/components/ai_component.hpp>
#include <gmp/components/health_component.hpp>
#include <gmp/components/threat_component.hpp>
#include <gmp/components/transform_component.hpp>
#include <gmp/systems/ai_combat_system.hpp>

std::unordered_map<std::string, gmp::EnemyParty> gmp::AICombatSystem::m_enemy_parties;

namespace gmp {

AICombatSystem::AICombatSystem(cor::FrameContext& context) : m_context{context} {
  add_listener(EVENT_REGISTER_MEMBER_FUNCTION_I(AddThreatEvent, context.event_mgr,
                                                add_threat_event_reciever));
  add_listener(EVENT_REGISTER_MEMBER_FUNCTION_I(MoraleLossEvent, context.event_mgr,
                                                morale_loss_event_handler));
  add_listener(EVENT_REGISTER_MEMBER_FUNCTION_I(MoraleBoostEvent, context.event_mgr,
                                                morale_boost_event_handler));
}

void AICombatSystem::read_update(const cor::FrameContext& context) {}

void AICombatSystem::write_update(cor::FrameContext& context) {
  cor::Key key = cor::Key::create<ThreatComponent, AIComponent, HealthComponent>();

  auto entities = context.entity_mgr.get_entities(key);

  // handles threat
  for (auto& e_handle : entities) {
    cor::Entity e = context.entity_mgr.get_entity(e_handle);
    ThreatComponent& t_c = e;
    TransformComponent& trans_c1 = e;
    AIComponent& ai_c = e;

    for (std::unordered_map<cor::EntityHandle, float>::iterator it = t_c.threats.begin();
         it != t_c.threats.end();) {
      // automatically decrease threat by a small amount
      // it->second -= m_threat_loss * context.dt;  //nahhh, lets not do that

      TransformComponent& trans_c2 = context.entity_mgr.get_entity(it->first);
      // remove if the entity poses no threat to the ai
      if (ai_c.funcs.get_distance(trans_c1, trans_c2) > ai_c.disengagement_range ||
          it->second <= 0.f)
        it = t_c.threats.erase(it);
      else
        ++it;
    }
  }

  // Updates the parties median positions.
  // Removes dead party members from the groups.

  // for each party
  for (std::unordered_map<std::string, EnemyParty>::iterator party = m_enemy_parties.begin();
       party != m_enemy_parties.end(); party++) {
    // for each member in the given party
    glm::vec3 median_point = {0.f, 0.f, 0.f};
    for (std::vector<cor::EntityHandle>::iterator party_member = party->second.members.begin();
         party_member != party->second.members.end();) {
      // Check if the member is DEAD(!) and remove if it is
      cor::Entity member_entity = m_context.entity_mgr.get_entity(*party_member);
      if (member_entity.is_valid()) {
        if (member_entity.has<HealthComponent>()) {
          TransformComponent& member_tc = member_entity;
          median_point += member_tc.transform.get_position();
          party_member++;
        } else {
          // entity is dead and should no longer be considered a member of the party
          party_member = party->second.members.erase(party_member);
        }
      } else {
        // entity is dead and should no longer be considered a member of the party
        party_member = party->second.members.erase(party_member);
      }
    }
    // update median point
    party->second.median_point = party->second.members.empty()
                                     ? glm::vec3(0.f, 0.f, 0.f)
                                     : median_point / (float)party->second.members.size();
  }

  // update morale every second
  std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
  auto elapsed_time = std::chrono::duration_cast<std::chrono::seconds>(now - time_point);
  if (elapsed_time.count() > std::chrono::seconds(1).count()) {
    for (auto& entity_handle : entities) {
      AIComponent& aic = context.entity_mgr.get_entity(entity_handle);
      // make sure that the ai belongs to a group, if not, skip it
      if (aic.morale_data.group_name != "") {
        aic.morale_data.morale =
            std::min(100, aic.morale_data.morale + aic.morale_data.morale_regeneration_val);
        // LOG(NOTICE) << aic.morale_data.morale + aic.morale_data.morale_boost;

        // reset the boost
        aic.morale_data.morale_boost = 0;
        for (auto& party : m_enemy_parties) {
          if (distance_check(m_enemy_parties[aic.morale_data.group_name].median_point,
                             party.second.median_point, 2)) {
            aic.morale_data.morale_boost += aic.morale_data.morale_boost_from_group;
          }
        }
      }
    }
    // LOG(NOTICE) << " ";
    time_point = std::move(now);
  }
}

void AICombatSystem::add_enemy_to_group(std::string name, cor::EntityHandle handle) {
  // make sure that the group exists
  if (m_enemy_parties.find(name) != m_enemy_parties.end()) {
  } else
    m_enemy_parties[name] = EnemyParty();

  m_enemy_parties[name].members.push_back(handle);
}

void AICombatSystem::add_threat_event_reciever(const AddThreatEvent* event) {
  // Get the threat component of the entity who should have the other entity as a threat
  auto entity = m_context.entity_mgr.get_entity(event->self);
  if (entity.is_valid() && entity.has<ThreatComponent>()) {
    ThreatComponent& t_c = entity;

    // Add the other entity as a threat to the entity with a given threat level
    t_c.threats[event->other] = event->threat_level;
  }
}

void AICombatSystem::increase_threat_event_reciever(const IncreaseThreatEvent* event) {
  // Get the threat component of the entity who should have the treat of certain entity increased

  auto entity = m_context.entity_mgr.get_entity(event->self);
  if (entity.is_valid() && entity.has<ThreatComponent>()) {
    ThreatComponent& t_c = entity;

    // Add the other entity as a threat to the entity with a given threat level
    t_c.threats[event->other] += event->threat_addition;
  }
}

void AICombatSystem::morale_loss_event_handler(const MoraleLossEvent* event) {
  switch (event->type) {
    case 0: {  // A AI has died
      morale_loss_to_own_party(event->handle, event->party, 0);
      morale_loss_for_other_parties(event->party, 0);
      break;
    }
    case 1: {  // An AI has taken dmg
      // The hit ai takes morale damage
      if (m_context.entity_mgr.is_valid(event->handle)) {
        cor::Entity e = m_context.entity_mgr.get_entity(event->handle);
        if (e.has<HealthComponent, AIComponent>()) {
          AIComponent& aic = e;
          aic.morale_data.morale -=
              aic.morale_data.morale_loss_per_damage *
              event->misc;  // Here misc contains how much damage has been dealth to the entity
        }
      }

      morale_loss_to_own_party(event->handle, event->party, 1);
      morale_loss_for_other_parties(event->party, 1);
      break;
    }
    case 2: {  // the ally is fleeing
      morale_loss_to_own_party(event->handle, event->party, 2);
      morale_loss_for_other_parties(event->party, 2);
    }
    default:
      return;
  }
}  // namespace gmp

void AICombatSystem::morale_loss_for_other_parties(std::string group, int type) {
  // deal morale damage to nearby enemy parties
  for (auto& party : m_enemy_parties) {
    if (distance_check(party.second.median_point, m_enemy_parties[group].median_point, 2.f)) {
      for (auto party_member : party.second.members) {
        cor::Entity e = m_context.entity_mgr.get_entity(party_member);
        if (e.has<HealthComponent, AIComponent>()) {
          AIComponent& aic = e;
          if (type == 0)
            aic.morale_data.morale -= aic.morale_data.morale_loss_ally_die;
          else if (type == 1)
            aic.morale_data.morale -= aic.morale_data.morale_loss_ally_hit;
          else if (type == 2)
            aic.morale_data.morale -= aic.morale_data.morale_loss_ally_flee;
        }
      }
    }
  }
}

void AICombatSystem::morale_loss_to_own_party(cor::EntityHandle self, std::string group, int type) {
  // Deal morale damage to the ai and its party members
  if (group != "" && m_enemy_parties.find(group) != m_enemy_parties.end()) {
    float morale_decrease_factor =
        1.f / m_enemy_parties[group].members.size();  // calculate "expensive division" once for
                                                      // the group, such optimization .... hue
    for (cor::EntityHandle& member : m_enemy_parties[group].members) {
      cor::Entity e = m_context.entity_mgr.get_entity(member);
      if (e.has<HealthComponent, AIComponent>()) {
        AIComponent& aic = e;
        if (type == 0) {
          // don't want to modify the dying entity, can lead to problems
          if (member != self) {
            aic.morale_data.morale -= aic.morale_data.morale_loss_ally_die * morale_decrease_factor;
          }
        } else if (type == 1) {
          aic.morale_data.morale -= aic.morale_data.morale_loss_ally_hit;
        } else if (type == 2) {
          // don't want to modify the dying entity, can lead to problems
          if (member != self) {
            aic.morale_data.morale -= aic.morale_data.morale_loss_ally_flee;
          }
        }
      }
    }
  }
}

bool AICombatSystem::distance_check(glm::vec3& first, glm::vec3& second, float distance) {
  glm::vec3 dist = first - second;
  // check if the groups median point is close enough to the given AIs group median point
  return (sqrt(dist.x * dist.x + dist.y * dist.y + dist.z * dist.z) < distance);
}

void AICombatSystem::morale_boost_event_handler(const MoraleBoostEvent* event) {
  if (m_context.entity_mgr.is_valid(event->handle)) {
    cor::Entity e = m_context.entity_mgr.get_entity(event->handle);
    if (e.has<HealthComponent, AIComponent>()) {
      AIComponent& aic = e;
      aic.morale_data.morale += aic.morale_data.morale_boost_dealing_damage;
    }
  }
}
}  // namespace gmp