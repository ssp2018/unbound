#include "gmp/systems/fall_damage_system.hpp"

#include "cor/entity.tcc"
#include "gmp/components/health_component.hpp"
#include "gmp/components/transform_component.hpp"
#include <cor/frame_context.hpp>
#include <ext/ext.hpp>
#include <gmp/components/player_component.hpp>

namespace gmp {

void FallDamageSystem::read_update(const cor::FrameContext& context) {
  // Ssschh, I am hiding here...
}

void FallDamageSystem::write_update(cor::FrameContext& context) {
  m_time_since_update += context.dt;
  if (m_time_since_update > 1.f) {
    m_time_since_update -= 1.f;

    cor::Key key = cor::Key::create<HealthComponent, TransformComponent>();
    auto entity_handles = context.entity_mgr.get_entities(key);
    for (auto& entity_handle : entity_handles) {
      cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
      if (entity.is_valid()) {
        const TransformComponent& tc = entity;
        HealthComponent& hc = entity;

        const glm::vec3 pos = tc.transform.get_position();

        // Start to take damage
        if (pos.z < 40.f) {
          hc.health_points -= 1;

          if (hc.health_points > 0) context.event_mgr.send_event(DamageTaken({entity_handle}));

          // Fell too far and died
          if (pos.z < -10.f) {
            hc.health_points = 0;
          }
        }
      }
    }
  }
}

}  // namespace gmp