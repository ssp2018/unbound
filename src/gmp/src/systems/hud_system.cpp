#include "gmp/systems/hud_system.hpp"

#include "cor/entity_manager.tcc"               // for EntityManager
#include "cor/frame_context.hpp"                // for FrameContext
#include "cor/key.hpp"                          // for Key
#include "gmp/components/ai_component.hpp"      // for Transform2dComp...
#include "gmp/components/health_component.hpp"  // for HealthComponent
#include "gmp/components/name_component.hpp"
#include "gmp/components/player_component.hpp"  // for TextureComponent
#include "gmp/components/player_damage_visual_component.hpp"
#include "gmp/components/projection_component.hpp"
#include "gmp/components/texture_component.hpp"       // for TextureComponent
#include "gmp/components/transform_2d_component.hpp"  // for Transform2dComp...
#include "gmp/components/transform_component.hpp"     // for Transform, Tran...
#include <cor/entity.tcc>                             // for Entity
namespace gmp {
struct AIComponent;
}
namespace gmp {
struct PlayerComponent;
}

namespace gmp {
HudSystem::HudSystem(cor::FrameContext &context) : m_context(context) {
  // Show hud event listener
  add_listener(context.event_mgr.register_handler<CutsceneFinished>(
      [this](const CutsceneFinished *event) { show_hud(); }));

  // Change crosshair event listener
  add_listener(
      context.event_mgr.register_handler<RopeReachEvent>([this](const RopeReachEvent *event) {
        auto crosshair = m_context.entity_mgr.get_entity(m_crosshair_handle);
        TextureComponent &tc = crosshair;
        if (event->can_reach)
          tc.texture =
              bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/crosshair-can-reach.png"_fp);
        else
          tc.texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/crosshair.png"_fp);
      }));

  // Change to piercing arrow
  add_listener(context.event_mgr.register_handler<NormalArrowModeEvent>(
      [this](const NormalArrowModeEvent *event) {
        cor::Entity arrow_entity = m_context.entity_mgr.get_entity(m_piercing_arrow_handle);
        cor::Entity selected_entity = m_context.entity_mgr.get_entity(m_selected_arrow_handle);
        if (arrow_entity.is_valid() && selected_entity.is_valid()) {
          Transform2dComponent &atc = arrow_entity;
          Transform2dComponent &stc = selected_entity;
          stc.position = atc.position;
          stc.position.z = 0;
        }
      }));

  // Change to explosive arrow
  add_listener(context.event_mgr.register_handler<ExplosiveArrowModeEvent>(
      [this](const ExplosiveArrowModeEvent *event) {
        cor::Entity arrow_entity = m_context.entity_mgr.get_entity(m_explosive_arrow_handle);
        cor::Entity selected_entity = m_context.entity_mgr.get_entity(m_selected_arrow_handle);
        if (arrow_entity.is_valid() && selected_entity.is_valid()) {
          Transform2dComponent &atc = arrow_entity;
          Transform2dComponent &stc = selected_entity;
          stc.position = atc.position;
          stc.position.z = 0;
        }
      }));

  // Change to grayed explosive arrow texture
  add_listener(context.event_mgr.register_handler<ExplosiveArrowCountChanged>(
      [this](const ExplosiveArrowCountChanged *event) {
        cor::Entity arrow_entity = m_context.entity_mgr.get_entity(m_explosive_arrow_handle);
        if (arrow_entity.is_valid()) {
          TextureComponent &tc = arrow_entity;
          if (event->new_count == 0)
            tc.texture = m_grayed_explosive_arrow_texture;
          else
            tc.texture = m_explosive_arrow_texture;

          cor::Entity counter = m_context.entity_mgr.get_entity(m_explosive_arrow_count_handle);
          counter.get<GraphicTextComponent>().text = std::to_string(event->new_count);
        }
      }));
}

void HudSystem::show_hud() {
  const float width_multiplier = 1280.f / 720.f;

  // Add direction indicator
  cor::Entity direction_entity =
      m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());
  direction_entity = TextureComponent(
      {bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/DirectionIndicator.png"_fp)});
  direction_entity =
      Transform2dComponent({glm::vec3(0.5, 0, 0), glm::vec2(0.04, 0.04 * width_multiplier), 0});
  m_direction_indicator_handle = direction_entity.get_handle();

  // Add health container
  cor::Entity container_entity =
      m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());
  container_entity = TextureComponent(
      {bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/container.png"_fp)});
  container_entity = Transform2dComponent({glm::vec3(0.2, 0.95, 0), glm::vec2(0.35, 0.05), 0});
  m_container_handle = container_entity.get_handle();

  // Add stamina container
  cor::Entity stamina_container_entity =
      m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());
  stamina_container_entity = TextureComponent(
      {bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/container.png"_fp)});
  stamina_container_entity =
      Transform2dComponent({glm::vec3(0.2, 0.915, 0), glm::vec2(0.35, 0.02), 0});
  m_stamina_container_handle = stamina_container_entity.get_handle();

  // Add stamina
  cor::Entity stamina_entity =
      m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());
  stamina_entity =
      TextureComponent({bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/blue.png"_fp)});
  stamina_entity = Transform2dComponent({glm::vec3(0.2, 0.915, 0), glm::vec2(0.35, 0.02), 0});
  m_stamina_handle = stamina_entity.get_handle();

  // Add crosshair
  cor::Entity crosshair_entity =
      m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());
  crosshair_entity = TextureComponent(
      {bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/crosshair.png"_fp)});
  crosshair_entity = Transform2dComponent({glm::vec3(0.5, 0.5, 0), glm::vec2(.03, .03), 0});
  m_crosshair_handle = crosshair_entity.get_handle();

  // Add boss name
  cor::Entity boss_name_entity =
      m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());
  {
    GraphicTextComponent gtc;
    gtc.outline_color = {128, 107, 0};
    gtc.color = {255, 215, 0};
    gtc.outline_thickness = 0.05f;
    gtc.font = "arial";
    gtc.text = "";
    boss_name_entity = gtc;

    gmp::Transform2dComponent t2dc;
    t2dc.position = glm::vec3(0.5f, .11f, 0);
    t2dc.size = glm::vec2(0.15f, 0.15f);
    t2dc.rotation = 0.f;
    t2dc.centering = Transform2dComponent::Centering::CENTER;

    boss_name_entity = t2dc;

    m_boss_name_handle = boss_name_entity.get_handle();
  }

  // Set selected arrow
  cor::Entity selected_arrow_entity =
      m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());
  selected_arrow_entity = TextureComponent(
      {bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/SelectedArrow.png"_fp)});
  selected_arrow_entity =
      Transform2dComponent({glm::vec3(0.85, 0.9, 0), glm::vec2(.1, .1 * width_multiplier), 0});
  m_selected_arrow_handle = selected_arrow_entity.get_handle();

  // Add piercing arrow
  cor::Entity piercing_arrow_entity =
      m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());
  piercing_arrow_entity = TextureComponent(
      {bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/normal-arrow.png"_fp)});
  piercing_arrow_entity =
      Transform2dComponent({glm::vec3(0.85, 0.9, 1), glm::vec2(.08, .08 * width_multiplier), 0});
  m_piercing_arrow_handle = piercing_arrow_entity.get_handle();

  // Add explosive arrow
  m_explosive_arrow_texture =
      bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/explosive-arrow.png"_fp);
  m_explosive_arrow_texture.pre_load();
  m_grayed_explosive_arrow_texture =
      bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/explosive-arrow-grayed.png"_fp);
  m_grayed_explosive_arrow_texture.pre_load();
  cor::Entity explosive_arrow_entity =
      m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());
  explosive_arrow_entity = TextureComponent({m_grayed_explosive_arrow_texture});
  explosive_arrow_entity =
      Transform2dComponent({glm::vec3(0.94, 0.9, 1), glm::vec2(.08, .08 * width_multiplier), 0});
  m_explosive_arrow_handle = explosive_arrow_entity.get_handle();

  // Explosive arrows left
  m_explosive_arrow_count_container_handle = m_context.entity_mgr.create_entity();
  cor::Entity count_container =
      m_context.entity_mgr.get_entity(m_explosive_arrow_count_container_handle);
  count_container =
      Transform2dComponent({glm::vec3(0.91, 0.85, 2), glm::vec2(.033, .033 * width_multiplier), 0});
  count_container = TextureComponent{
      bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/arrows-count-container.png"_fp)};

  cor::Entity counter_entity =
      m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());
  {
    GraphicTextComponent gtc;
    gtc.outline_color = {0, 0, 0};
    gtc.color = {0, 0, 0};
    gtc.outline_thickness = 0.f;
    gtc.font = "arial";
    gtc.text = "0";
    counter_entity = gtc;

    gmp::Transform2dComponent t2dc;
    t2dc.position = glm::vec3(0.909f, .855f, 3);
    t2dc.size = glm::vec2(0.15f, 0.15f);
    t2dc.rotation = 0.f;
    t2dc.centering = Transform2dComponent::Centering::CENTER;
    counter_entity = t2dc;

    m_explosive_arrow_count_handle = counter_entity.get_handle();
  }
}

void HudSystem::read_update(const cor::FrameContext &context) {
  // Update stamina value
  cor::Key key = cor::Key::create<PlayerComponent>();
  auto entities = m_context.entity_mgr.get_entities(key);
  if (entities.size() > 0) {
    cor::EntityHandle handle = entities[0];
    if (m_context.entity_mgr.is_valid(handle) && m_context.entity_mgr.is_valid(m_stamina_handle)) {
      cor::Entity player_entity = m_context.entity_mgr.get_entity(handle);
      PlayerComponent &pc = player_entity;
      m_stamina = pc.stamina / pc.max_stamina;
    }
  }

  // Update waypoint
  key = cor::Key::create<ProjectionComponent, TransformComponent>();
  entities = m_context.entity_mgr.get_entities(key);
  if (entities.size() > 0) {
    cor::EntityHandle handle = entities[0];
    if (m_context.entity_mgr.is_valid(handle)) {
      cor::Entity camera_entity = m_context.entity_mgr.get_entity(handle);
      TransformComponent &ctc = camera_entity;
      ProjectionComponent &cpc = camera_entity;

      // Loop through all other stuff
      key = cor::Key::create<NameComponent, TransformComponent, HealthComponent>();
      auto entities = m_context.entity_mgr.get_entities(key);
      for (auto handle : entities) {
        cor::Entity enemy_entity = m_context.entity_mgr.get_entity(handle);
        NameComponent &nc = enemy_entity;
        TransformComponent &tc = enemy_entity;
        HealthComponent &hc = enemy_entity;

        if ((nc.name == "Golem" || nc.name == "COLE") && hc.health_points > 0) {
          glm::vec3 enemy_pos = tc.transform.get_position();
          glm::vec3 camera_pos = ctc.transform.get_position();
          glm::vec3 target = enemy_pos - camera_pos;

          glm::vec3 forward = ctc.transform.get_model_matrix() * glm::vec4(0, 0, -1, 0);
          forward.z = 0;
          forward = glm::normalize(forward);
          glm::vec4 enemy_clip = cpc.projection_matrix *
                                 glm::inverse(ctc.transform.get_model_matrix()) *
                                 glm::vec4(enemy_pos, 1.f);

          glm::vec2 enemy_screen = ((enemy_clip / enemy_clip.w) + 1.f) / 2.f;
          enemy_screen = glm::vec2(enemy_screen.x, 1.f - enemy_screen.y);

          glm::vec2 clamp = glm::vec2(glm::clamp(enemy_screen.x, 0.01f, 0.99f),
                                      glm::clamp(enemy_screen.y, 0.02f, 0.98f));
          if (glm::dot(enemy_pos - camera_pos, forward) > 0.f) {
            if (glm::length(target) < 300.f && enemy_screen == clamp) {
              enemy_screen = glm::vec2(-1, -1);
            } else {
              enemy_screen = clamp;
            }
          } else {
            if (clamp.y < .5f) {
              enemy_screen.y = .98f;
            } else {
              enemy_screen.y = .02f;
            }
            enemy_screen.x = 1 - clamp.x;
          }

          m_golem_waypoint = enemy_screen;
          break;
        }
        m_golem_waypoint = glm::vec2(-1, -1);
      }
    }
  }

  // Update charge
  key = cor::Key::create<PlayerComponent>();
  entities = m_context.entity_mgr.get_entities(key);
  if (entities.size() > 0) {
    cor::EntityHandle handle = entities[0];
    if (m_context.entity_mgr.is_valid(handle)) {
      cor::Entity player_entity = m_context.entity_mgr.get_entity(handle);
      cor::Entity charge_entity = m_context.entity_mgr.get_entity(m_charge_handle);
      PlayerComponent &pc = player_entity;

      if (pc.charge_time > pc.max_charge_time || pc.charge_time == 0) {
        m_charge.x = -1;
      } else if (pc.charge_time > .1) {
        const float time = (pc.max_charge_time - pc.charge_time) / pc.max_charge_time;
        const float size = 1.5;
        m_charge.x = time * .09 * size;
        m_charge.y = time * .16 * size;
      }
    }
  }
}

void HudSystem::write_update(cor::FrameContext &context) {
  cor::Key key = cor::Key::create<HealthComponent, PlayerComponent>();
  auto entities = m_context.entity_mgr.get_entities(key);
  if (entities.size() > 0) {
    cor::EntityHandle handle = entities[0];
    if (m_context.entity_mgr.is_valid(handle)) {
      cor::Entity player_entity = m_context.entity_mgr.get_entity(handle);
      HealthComponent &hp = player_entity;

      const int HEARTS_DIFF = hp.health_points - m_heart_entities.size();

      if (HEARTS_DIFF > 0) {
        remove_death_screen();
        add_hearts(HEARTS_DIFF);
        m_max_health = hp.max_health_points;
        update_hearts();
      } else if (HEARTS_DIFF < 0) {
        if (player_entity.has<PlayerDamageVisualComponent>()) {
          gmp::PlayerDamageVisualComponent &pdvc = player_entity;
          pdvc.color.a += 10.0 * -HEARTS_DIFF;
        }
        remove_hearts(-HEARTS_DIFF);
        if (m_heart_entities.size() <= 0) show_death_screen();
        update_hearts();
      }
    }
  }
  update_enemy_health();

  update_charge();

  update_stamina();

  update_direction();
}

void HudSystem::add_hearts(int heartsToAdd) {
  for (size_t i = 0; i < heartsToAdd; i++) {
    cor::Entity heart_entity =
        m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());
    heart_entity = TextureComponent(
        {bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/player_heart.png"_fp)});
    heart_entity = Transform2dComponent({});
    m_heart_entities.push_back(heart_entity.get_handle());
  }
}

void HudSystem::remove_hearts(int heartsToRemove) {
  for (size_t i = 0; i < heartsToRemove; i++) {
    if (m_heart_entities.size() > 0) {
      cor::EntityHandle eh = m_heart_entities.back();
      if (m_context.entity_mgr.is_valid(eh)) {
        cor::Entity heart = m_context.entity_mgr.get_entity(eh);
        heart.destroy();
      }
      m_heart_entities.pop_back();
    }
  }
}

void HudSystem::update_hearts() {
  for (size_t i = 0; i < m_heart_entities.size(); i++) {
    if (m_context.entity_mgr.is_valid(m_heart_entities[i])) {
      cor::Entity heart = m_context.entity_mgr.get_entity(m_heart_entities[i]);
      Transform2dComponent &tc = heart;
      Transform2dComponent container = m_context.entity_mgr.get_entity(m_container_handle);
      auto width = container.size.x / m_max_health;
      tc.position = glm::vec3(width * i + container.position.x - ((container.size.x - width) / 2),
                              container.position.y, 0);
      tc.size = glm::vec2(width, container.size.y);
    }
  }
}

void HudSystem::update_enemy_health() {
  cor::EntityHandle handle;
  glm::vec3 player_position;
  float distance = 2500;

  cor::Key key = cor::Key::create<HealthComponent, TransformComponent, PlayerComponent>();
  auto entities = m_context.entity_mgr.get_entities(key);
  if (entities.size() > 0) {
    cor::EntityHandle handle = entities[0];
    if (m_context.entity_mgr.is_valid(handle)) {
      cor::Entity player_entity = m_context.entity_mgr.get_entity(handle);
      TransformComponent &tp = player_entity;
      player_position = tp.transform.get_position();
    }
  }

  key = cor::Key::create<HealthComponent, TransformComponent, AIComponent, NameComponent>();
  entities = m_context.entity_mgr.get_entities(key);
  auto name_entity = m_context.entity_mgr.get_entity(m_boss_name_handle);
  for (auto entity : entities) {
    TransformComponent &aitp = m_context.entity_mgr.get_entity(entity);
    HealthComponent &hc = m_context.entity_mgr.get_entity(entity);
    NameComponent &nc = m_context.entity_mgr.get_entity(entity);
    float this_distance = glm::distance(aitp.transform.get_position(), player_position);
    if (this_distance < distance && (nc.name == "Golem" || nc.name == "COLE")) {
      handle = entity;
      distance = this_distance;
      m_opponent_max_health = hc.max_health_points;
      if (name_entity.is_valid()) {
        GraphicTextComponent &gtc = name_entity;
        gtc.text = nc.name;
      }
    }
  }

  if (m_context.entity_mgr.is_valid(handle)) {
    if (!m_context.entity_mgr.is_valid(m_boss_container_handle)) {
      // Add boss health container
      cor::Entity boss_container_entity =
          m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());
      boss_container_entity = TextureComponent(
          {bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/container.png"_fp)});
      boss_container_entity =
          Transform2dComponent({glm::vec3(0.5, 0.05, 0), glm::vec2(0.9, 0.05), 0});
      m_boss_container_handle = boss_container_entity.get_handle();
    }

    cor::Entity player_entity = m_context.entity_mgr.get_entity(handle);
    HealthComponent &hp = player_entity;

    const int HEARTS_DIFF = hp.health_points - m_enemy_heart_entities.size();

    if (HEARTS_DIFF > 0) {
      for (size_t i = 0; i < HEARTS_DIFF; i++) {
        cor::Entity heart_entity =
            m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());
        heart_entity = TextureComponent(
            {bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/red.png"_fp)});
        Transform2dComponent container = m_context.entity_mgr.get_entity(m_boss_container_handle);
        auto width = container.size.x / m_opponent_max_health;
        heart_entity = Transform2dComponent(
            {glm::vec3(width * i + container.position.x - ((container.size.x - width) / 2),
                       container.position.y, 0),
             glm::vec2(width * .9, container.size.y * .9), 0});
        m_enemy_heart_entities.push_back(heart_entity.get_handle());
      }
    } else if (HEARTS_DIFF < 0) {
      for (size_t i = 0; i < -HEARTS_DIFF; i++) {
        if (m_enemy_heart_entities.size() > 0) {
          cor::EntityHandle eh = m_enemy_heart_entities.back();
          if (m_context.entity_mgr.is_valid(eh)) {
            cor::Entity boss_heart = m_context.entity_mgr.get_entity(eh);
            boss_heart.destroy();
          }
          m_enemy_heart_entities.pop_back();
        }
      }
    }
  } else {
    for (size_t i = 0; i < m_enemy_heart_entities.size(); i++) {
      if (m_enemy_heart_entities.size() > 0) {
        cor::EntityHandle eh = m_enemy_heart_entities.back();
        if (m_context.entity_mgr.is_valid(eh)) {
          cor::Entity boss_heart = m_context.entity_mgr.get_entity(eh);
          boss_heart.destroy();
        }
        m_enemy_heart_entities.pop_back();
      }
    }

    if (m_boss_container_handle != cor::EntityHandle::NULL_HANDLE) {
      auto container = m_context.entity_mgr.get_entity(m_boss_container_handle);
      container.destroy();
      m_boss_container_handle = cor::EntityHandle::NULL_HANDLE;
      if (name_entity.is_valid()) {
        GraphicTextComponent &gtc = name_entity;
        gtc.text = "";
      }
    }
  }
}

void HudSystem::update_stamina() {
  cor::Entity stamina_entity = m_context.entity_mgr.get_entity(m_stamina_handle);
  cor::Entity stamina_container_entity =
      m_context.entity_mgr.get_entity(m_stamina_container_handle);
  Transform2dComponent &tc = stamina_entity;
  Transform2dComponent &ctc = stamina_container_entity;

  TextureComponent &texc = stamina_entity;
  if (m_stamina < .24) {
    texc.texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/grey.png"_fp);
  } else {
    texc.texture = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/blue.png"_fp);
  }

  tc.size = glm::vec2(m_stamina * ctc.size.x, ctc.size.y);
  tc.position =
      glm::vec3(ctc.position.x - (ctc.size.x - (m_stamina * ctc.size.x)) / 2, ctc.position.y, 0);
}

void HudSystem::update_charge() {
  cor::Entity charge_entity = m_context.entity_mgr.get_entity(m_charge_handle);
  if (m_charge.x > 0) {
    if (charge_entity.is_valid()) {
      Transform2dComponent &tc = charge_entity;
      tc.size = m_charge;
    } else {
      charge_entity = m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());
      charge_entity = TextureComponent(
          {bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/ChargeArrow.png"_fp)});
      charge_entity = Transform2dComponent({glm::vec3(0.5, 0.5, 0), m_charge, 0});
      m_charge_handle = charge_entity.get_handle();
    }
  } else if (charge_entity.is_valid()) {
    charge_entity.destroy();
  }
}

void HudSystem::update_direction() {
  auto entity = m_context.entity_mgr.get_entity(m_direction_indicator_handle);
  Transform2dComponent &t2dc = entity;
  t2dc.position = glm::vec3(m_golem_waypoint, 0);
}

void HudSystem::show_death_screen() {
  cor::Entity overlay_entity = m_context.entity_mgr.get_entity(m_death_screen_handle);
  if (!overlay_entity.is_valid()) {
    overlay_entity = m_context.entity_mgr.get_entity(m_context.entity_mgr.create_entity());
    overlay_entity = TextureComponent(
        {bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/hud/death-overlay.png"_fp)});
    overlay_entity = Transform2dComponent({glm::vec3(0.5, 0.5, 0), glm::vec2(1, 1), 0});
    m_death_screen_handle = overlay_entity.get_handle();
  }
}

void HudSystem::remove_death_screen() {
  cor::Entity overlay_entity = m_context.entity_mgr.get_entity(m_death_screen_handle);
  if (overlay_entity.is_valid()) overlay_entity.destroy();
}

}  // namespace gmp