#include "gmp/systems/rotation_system.hpp"

#include "cor/entity_handle.hpp"                   // for EntityHandle
#include "cor/entity_manager.tcc"                  // for EntityManager
#include "cor/frame_context.hpp"                   // for FrameContext
#include "cor/key.hpp"                             // for Key
#include "gmp/components/rotate_z_component.hpp"   // for RotateZComponent
#include "gmp/components/transform_component.hpp"  // for Transform, Transfo...
#include <cor/entity.tcc>                          // for Entity

namespace gmp {

void RotationSystem::read_update(const cor::FrameContext& context) {}
void RotationSystem::write_update(cor::FrameContext& context) {
  cor::Key key = cor::Key::create<TransformComponent, RotateZComponent>();

  std::vector<cor::EntityHandle> entities = context.entity_mgr.get_entities(key);
  for (cor::EntityHandle& entity_handle : entities) {
    cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
    TransformComponent& tc = entity;
    RotateZComponent& rc = entity;

    rc.rotation += context.dt * 0.1f;
    tc.transform.rotate(glm::vec3(0, 0, 1), context.dt * 0.1f, context);

    // if (m_renderer.add_renderable_to_frame(mc.index) < 0) {
    //   m_renderer.add_renderable(mc.index, mc.asset, tc.model_matrix);
    // }
  }
}

}  // namespace gmp