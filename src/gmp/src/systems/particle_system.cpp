#include "gmp/systems/particle_system.hpp"

#include "cor/entity.tcc"                           // for Entity, ConstEntity
#include "cor/entity_handle.hpp"                    // for EntityHandle
#include "cor/entity_manager.hpp"                   // for EntityManager
#include "cor/frame_context.hpp"                    // for FrameContext
#include "cor/key.hpp"                              // for Key
#include "gmp/components/particles_component.hpp"   // for ParticlesComponent
#include "gmp/components/projection_component.hpp"  // for ParticlesComponent
#include "gmp/components/transform_component.hpp"   // for Transform, Transfo...
namespace gmp {
struct ProjectionComponent;
}
namespace gmp {
void ParticleSystem::read_update(const cor::FrameContext& context) {
  m_updated_systems.clear();

  // get camera position
  cor::Key camera_key = cor::Key::create<TransformComponent, ProjectionComponent>();
  std::vector<cor::EntityHandle> cameras = context.entity_mgr.get_entities(camera_key);

  // get camera
  cor::ConstEntity active_camera;
  if (cameras.size() > 0) {
    active_camera = context.entity_mgr.get_entity(cameras[0]);
  }
  const TransformComponent& camera_tc = active_camera;
  glm::vec3 cam_pos = camera_tc.transform.get_position();

  // systems
  cor::Key particle_systems_key = cor::Key::create<TransformComponent, ParticlesComponent>();
  std::vector<cor::EntityHandle> particle_systems =
      context.entity_mgr.get_entities(particle_systems_key);

  for (auto particle_system_handle : particle_systems) {
    cor::ConstEntity particle_system = context.entity_mgr.get_entity(particle_system_handle);
    m_updated_systems.emplace_back(particle_system_handle,
                                   particle_system.get<ParticlesComponent>());
    ParticlesComponent& pc = m_updated_systems.back().second;
    pc.max_particles = pc.max_life * pc.spawn_per_second + 1.0f;
    const TransformComponent& tc = particle_system;

    size_t buffer_size = std::max(pc.num_particles, pc.max_particles);
    pc.positions.resize(buffer_size * 9);
    pc.particles.resize(buffer_size);

    if (pc.last_pos == glm::vec3(0, 0, 0)) {
      pc.last_pos = tc.transform.get_position();
    }

    if (pc.spawn_shape == ParticlesComponent::SpawnShape::WAVE) {
      pc.wave_time += context.dt;
      pc.wave_pos = (pc.wave_time / pc.life_time) * pc.wave_radius;
    }

    pc.time_since_particle += context.dt;
    if (pc.spawn_duration_left > 0.f || pc.spawn_duration_left < -0.99f) {
      if (pc.spawn_duration_left > 0.f) pc.spawn_duration_left -= context.dt;
      while (pc.time_since_particle > 1.0f / pc.spawn_per_second) {
        // add new particles
        int new_index = find_unused_particle(pc);
        pc.particles[new_index].start_life = get_random_float(pc.min_life, pc.max_life);
        pc.particles[new_index].life = pc.particles[new_index].start_life;

        glm::vec3 pos = glm::vec3(0);

        switch (pc.spawn_shape) {
          case ParticlesComponent::SpawnShape::BOX: {
            pc.particles[new_index].velocity =
                get_random_vec3(pc.min_start_velocity, pc.max_start_velocity);
            pos = glm::mix(tc.transform.get_position(), pc.last_pos, get_random_float(0.f, 1.f));
            break;
          }
          case ParticlesComponent::SpawnShape::SPHERE: {
            pc.particles[new_index].velocity =
                get_random_vec3(pc.min_start_velocity, pc.max_start_velocity);
            pc.particles[new_index].velocity =
                glm::normalize(pc.particles[new_index].velocity) *
                get_random_float(pc.sphere_min_speed, pc.sphere_max_speed);
            pos = glm::mix(tc.transform.get_position(), pc.last_pos, get_random_float(0.f, 1.f));
            break;
          }
          case ParticlesComponent::SpawnShape::WAVE: {
            pc.particles[new_index].velocity =
                get_random_vec3(pc.min_start_velocity, pc.max_start_velocity);
            pc.particles[new_index].velocity =
                glm::normalize(pc.particles[new_index].velocity) *
                get_random_float(pc.sphere_min_speed, pc.sphere_max_speed);
            pos = glm::normalize(get_random_vec3(glm::vec3(-1, -1, 0), glm::vec3(1, 1, 0)));
            pos = pos * pc.wave_pos;
            pos = pos + get_random_float(-pc.wave_length, pc.wave_length);
            pos = pos + tc.transform.get_position();
            break;
          }
        }

        // Position
        pc.particles[new_index].position = pos + pc.offset_from_transform_component +
                                           get_random_vec3(pc.min_spawn_pos, pc.max_spawn_pos);
        // Color
        pc.particles[new_index].start_color =
            get_random_vec4(pc.min_start_color, pc.max_start_color);
        pc.particles[new_index].end_color = get_random_vec4(pc.min_end_color, pc.max_end_color);
        pc.particles[new_index].color = pc.particles[new_index].start_color;
        // Size
        pc.particles[new_index].start_size = get_random_float(pc.min_start_size, pc.max_start_size);
        pc.particles[new_index].end_size = get_random_float(pc.min_end_size, pc.max_end_size);
        pc.particles[new_index].size = pc.particles[new_index].start_size;
        // Rotation
        pc.particles[new_index].rotation = get_random_float(0.f, 6.3f);
        pc.particles[new_index].rotation_speed =
            get_random_float(pc.min_rotation_speed, pc.max_rotation_speed);

        //
        pc.time_since_particle -= 1.0f / pc.spawn_per_second;
      }
    }

    pc.last_pos = tc.transform.get_position();

    // update all particles
    for (int i = 0; i < pc.max_particles; i++) {
      if (pc.particles[i].life > 0) {
        pc.particles[i].life -= context.dt;
        const float life_percent =
            (pc.particles[i].start_life - pc.particles[i].life) / pc.particles[i].start_life;
        if (pc.velocity_mode == ParticlesComponent::Velocity::DECELERATE)
          pc.particles[i].velocity *= (1 - context.dt * life_percent * 25.f);
        pc.particles[i].position += pc.particles[i].velocity * context.dt;
        pc.particles[i].color =
            glm::mix(pc.particles[i].start_color, pc.particles[i].end_color, life_percent);
        pc.particles[i].rotation -= pc.particles[i].rotation_speed * context.dt;
        if (pc.size_mode == ParticlesComponent::Size::CHANGING)
          pc.particles[i].size =
              glm::mix(pc.particles[i].start_size, pc.particles[i].end_size, life_percent);
      }
    }

    pc.num_particles = 0;
    for (int i = 0; i < pc.max_particles; i++) {
      if (pc.particles[i].life >= 0) {
        constexpr int size = 9;

        // position
        pc.positions[pc.num_particles * size] = pc.particles[i].position.x;
        pc.positions[pc.num_particles * size + 1] = pc.particles[i].position.y;
        pc.positions[pc.num_particles * size + 2] = pc.particles[i].position.z;

        // color
        pc.positions[pc.num_particles * size + 3] = pc.particles[i].color.r;
        pc.positions[pc.num_particles * size + 4] = pc.particles[i].color.g;
        pc.positions[pc.num_particles * size + 5] = pc.particles[i].color.b;
        pc.positions[pc.num_particles * size + 6] = pc.particles[i].color.a;

        // size
        pc.positions[pc.num_particles * size + 7] = pc.particles[i].size;

        // rotation
        pc.positions[pc.num_particles * size + 8] = pc.particles[i].rotation;

        pc.num_particles++;
      }
    }
  }
}

void ParticleSystem::write_update(cor::FrameContext& context) {
  for (const auto& [handle, system] : m_updated_systems) {
    cor::Entity entity = context.entity_mgr.get_entity(handle);
    if (!entity.is_valid() || !entity.has<ParticlesComponent>()) {
      continue;
    }

    if (system.do_destroy_entity) {
      if (system.num_particles == 0 &&
          system.time_since_particle > 1.f / system.spawn_per_second + 0.01f) {
        context.entity_mgr.destroy_entity(handle);
        continue;
      }
    }

    entity.get<ParticlesComponent>() = system;
  }
}

int ParticleSystem::find_unused_particle(ParticlesComponent& pc) {
  if (pc.last_unused_particle > pc.max_particles) {
    pc.last_unused_particle = 0;
  }
  for (int i = pc.last_unused_particle; i < pc.max_particles; i++) {
    if (pc.particles[i].life < 0) {
      pc.last_unused_particle = i + 1;
      return i;
    }
  }

  for (int i = 0; i < std::min(pc.last_unused_particle, pc.max_particles); i++) {
    if (pc.particles[i].life < 0) {
      pc.last_unused_particle = i + 1;
      return i;
    }
  }

  return 0;  // All particles are taken, override the first one
}

glm::vec3 ParticleSystem::get_random_vec3(glm::vec3 min, glm::vec3 max) {
  return min + glm::vec3(rand(), rand(), rand()) / ((float)RAND_MAX / (max - min));
}
glm::vec4 ParticleSystem::get_random_vec4(glm::vec4 min, glm::vec4 max) {
  return min + glm::vec4(rand(), rand(), rand(), rand()) / ((float)RAND_MAX / (max - min));
}
float ParticleSystem::get_random_float(float min, float max) {
  return min + rand() / ((float)RAND_MAX / (max - min));
}
}  // namespace gmp
