#include "gmp/systems/debug_camera_system.hpp"

#include "cor/entity_manager.tcc"                     // for EntityManager
#include "cor/event.hpp"                              // for MouseMovementEvent
#include "cor/event_manager.hpp"                      // for EventManager
#include "cor/frame_context.hpp"                      // for FrameContext
#include "cor/key.hpp"                                // for Key
#include "gmp/components/debug_camera_component.hpp"  // for Transform, Transfo...
#include "gmp/components/projection_component.hpp"    // for Transform, Transfo...
#include "gmp/components/transform_component.hpp"     // for Transform, Transfo...
#include <cor/entity.tcc>                             // for Entity
namespace gmp {
struct DebugCameraComponent;
}
namespace gmp {
struct ProjectionComponent;
}

namespace gmp {
DebugCameraSystem::DebugCameraSystem(cor::FrameContext& context) {
  add_listener(context.event_mgr.register_handler<MoveEvent>([this](const MoveEvent* event) {
    m_forward = event->forward;
    m_right = event->right;
  }));

  add_listener(context.event_mgr.register_handler<MouseMovementEvent>(
      [this](const MouseMovementEvent* event) {
        float angle = 89 * 0.0174532925f;
        m_rotation_x -= event->dx;
        m_rotation_z -= event->dy;
        if (m_rotation_z > angle)
          m_rotation_z = angle;
        else if (m_rotation_z < -angle)
          m_rotation_z = -angle;
      }));
}

void DebugCameraSystem::read_update(const cor::FrameContext& context) {
  // This is not the function you are looking for...
}

void DebugCameraSystem::write_update(cor::FrameContext& context) {
  // if (m_debug_camera_active) {

  glm::mat4 rotation_z = glm::rotate(glm::mat4(1), m_rotation_x, glm::vec3(0, 0, 1));
  glm::mat4 rotation_x = glm::rotate(glm::mat4(1), m_rotation_z, glm::vec3(1, 0, 0));

  glm::vec3 forward_dir = glm::vec3(rotation_z * rotation_x * glm::vec4(0, 1, 0, 0));
  glm::vec3 right_dir = glm::vec3(rotation_z * rotation_x * glm::vec4(1, 0, 0, 0));

  cor::Key cam_key =
      cor::Key::create<DebugCameraComponent, ProjectionComponent, TransformComponent>();
  auto entities = context.entity_mgr.get_entities(cam_key);

  for (auto handle : entities) {
    auto camera = context.entity_mgr.get_entity(handle);

    TransformComponent& ctc = camera;
    glm::vec3 position = ctc.transform.get_position();
    position += (forward_dir * m_forward + right_dir * m_right) *
                camera.get<DebugCameraComponent>().speed * context.dt;

    ctc.transform.set_model_matrix(
        glm::inverse(glm::lookAt(position, position + forward_dir, glm::vec3(0, 0, 1))), context);
  }

  m_forward = 0;
  m_right = 0;
  // }
}
}  // namespace gmp
