#include "gmp/systems/homing_system.hpp"

#include "cor/entity.tcc"
#include "gmp/components/homing_component.hpp"
#include "gmp/components/physics_projectile_component.hpp"
#include "gmp/components/transform_component.hpp"
#include <cor/cor.hpp>
#include <cor/entity_manager.tcc>
#include <cor/frame_context.hpp>
#include <ext/ext.hpp>

namespace gmp {

void HomingSystem::read_update(const cor::FrameContext& context) {
  // Carry on, nothing to see here
}

void HomingSystem::write_update(cor::FrameContext& context) {
  int a = 0;
  cor::Key key =
      cor::Key::create<HomingComponent, PhysicsProjectileComponent, TransformComponent>();
  auto entity_handles = context.entity_mgr.get_entities(key);
  for (auto entity_handle : entity_handles) {
    cor::Entity& entity = context.entity_mgr.get_entity(entity_handle);

    if (entity.is_valid()) {
      PhysicsProjectileComponent& ppc = entity;
      TransformComponent& tc = entity;
      HomingComponent& hc = entity;

      cor::Entity& target = context.entity_mgr.get_entity(hc.target);
      if (target.is_valid()) {
        TransformComponent& target_tc = target;

        glm::vec3 target_direction =
            glm::normalize(target_tc.transform.get_position() - tc.transform.get_position());

        glm::vec3 new_direction =
            glm::mix(ppc.contents.get_velocity(), target_direction, context.dt * hc.turn_speed);

        glm::vec3 velocity = glm::normalize(new_direction) * hc.speed;

        ppc.contents.set_velocity(velocity);
      }
    }
    a++;
  }
}

}  // namespace gmp