#include <cor/entity.tcc>
#include <cor/entity_manager.tcc>
#include <cor/frame_context.hpp>
#include <gmp/components/sticky_triangle_component.hpp>
#include <gmp/components/transform_component.hpp>
#include <gmp/systems/sticky_triangle_system.hpp>

namespace gmp {

void StickyTriangleSystem::add_dirty(cor::EntityHandle sticky_entity,
                                     const cor::FrameContext& context) {
  cor::ConstEntity entity = context.entity_mgr.get_entity(sticky_entity);
  const StickyTriangleComponent& sticky = entity;
  uint16_t frame_updated = entity.get<TransformComponent>().transform.get_frame_updated();

  // Fetch transforms of all points.
  // Set dirty if any transform is
  // fresher than the sticky's.
  bool is_dirty = false;
  std::array<const Transform*, 3> transforms;
  for (size_t i = 0; i < sticky.points.size(); i++) {
    cor::ConstEntity point = context.entity_mgr.get_entity(sticky.points[i]);
    if (!point.is_valid() || !point.has<TransformComponent>()) {
      return;
    }

    const Transform& transform = point.get<TransformComponent>().transform;
    transforms[i] = &transform;
    if (transform.get_frame_updated() >= frame_updated) {
      is_dirty = true;
    }
  }

  if (!is_dirty) {
    return;
  }

  DirtySticky dirty;
  dirty.handle = sticky_entity;
  dirty.transform.set_model_matrix(glm::mat4(glm::vec4(transforms[0]->get_position(), 0.f),
                                             glm::vec4(transforms[1]->get_position(), 0.f),
                                             glm::vec4(transforms[2]->get_position(), 0.f),  //
                                             glm::vec4(0.f, 0.f, 0.f, 1.f)),
                                   context);
  m_dirty_stickies.push_back(dirty);
}

void StickyTriangleSystem::read_update(const cor::FrameContext& context) {
  m_dirty_stickies.clear();

  cor::Key key;
  key.include<StickyTriangleComponent, TransformComponent>();
  for (cor::EntityHandle handle : context.entity_mgr.get_entities(key)) {
    add_dirty(handle, context);
  }
}

void StickyTriangleSystem::write_update(cor::FrameContext& context) {
  for (const DirtySticky& dirty : m_dirty_stickies) {
    cor::Entity entity = context.entity_mgr.get_entity(dirty.handle);
    entity = TransformComponent{dirty.transform};
  }
}
}  // namespace gmp
