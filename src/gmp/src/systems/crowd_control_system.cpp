#include "bse/asset.hpp"
#include "cor/entity.tcc"
#include "cor/entity_handle.hpp"
#include "cor/entity_manager.tcc"
#include "cor/event.hpp"
#include "cor/event_manager.hpp"
#include "cor/frame_context.hpp"
#include "cor/key.hpp"
#include "gmp/components/ai_component.hpp"
#include "gmp/components/ai_force_state_change_component.hpp"
#include "gmp/components/attach_to_entity_component.hpp"
#include "gmp/components/attach_to_joint_component.hpp"
#include "gmp/components/audio_component.hpp"
#include "gmp/components/character_controller_component.hpp"
#include "gmp/components/hook_component.hpp"
#include "gmp/components/hook_hit_component.hpp"
#include "gmp/components/lifetime_component.hpp"
#include "gmp/components/player_component.hpp"
#include "gmp/components/static_component.hpp"
#include <gmp/components/dynamic_component.hpp>
#include <gmp/components/force_move_to_entity_component.hpp>
#include <gmp/components/force_move_to_position_component.hpp>
#include <gmp/components/hook_component.hpp>
#include <gmp/components/knocked_back_stun_component.hpp>
#include <gmp/components/physics_projectile_component.hpp>
#include <gmp/gmp_helper_functions.hpp>
#include <gmp/systems/crowd_control_system.hpp>

namespace gmp {
namespace {
static const cor::Key G_POSITION_FORCE_KEY =
    cor::Key::create<ForceMoveToPositionComponent, TransformComponent,
                     CharacterControllerComponent>();

static const cor::Key G_ENTITY_FORCE_KEY =
    cor::Key::create<ForceMoveToEntityComponent, TransformComponent,
                     CharacterControllerComponent>();

static const cor::Key G_HOOK_KEY = cor::Key::create<HookComponent>();

static const cor::Key G_CCC_KEY = cor::Key::create<CharacterControllerComponent>();

static const cor::Key G_DC_KEY = cor::Key::create<DynamicComponent>();
static const cor::Key G_KNOCKED_BACK_STUN_KEY =
    cor::Key::create<KnockedBackStunComponent, CharacterControllerComponent>();
}  // namespace
}  // namespace gmp

gmp::CrowdControlSystem::CrowdControlSystem(cor::FrameContext &context) : m_context(context) {
  add_listener(EVENT_REGISTER_MEMBER_FUNCTION_I(CollisionEvent, context.event_mgr,
                                                collision_event_listener));
}

void gmp::CrowdControlSystem::collision_event_listener(const CollisionEvent *event) {
  if (m_context.entity_mgr.is_valid(event->entity_1) &&
      m_context.entity_mgr.is_valid(event->entity_2)) {
    for (int i = 0; i < 2; ++i) {
      cor::EntityHandle first_handle = (i == 0 ? event->entity_1 : event->entity_2);
      cor::EntityHandle second_handle = (i == 1 ? event->entity_1 : event->entity_2);
      cor::Entity first_entity = m_context.entity_mgr.get_entity(first_handle);
      cor::Entity second_entity = m_context.entity_mgr.get_entity(second_handle);

      if (first_entity.has<HookComponent>() && first_entity.has<PhysicsProjectileComponent>()) {
        if (second_entity.has<PlayerComponent>() || second_entity.has<AIComponent>()) {
          // hook hit living entity
          HookComponent &hook = first_entity;
          if (m_context.entity_mgr.is_valid(hook.owner)) {
            cor::Entity hook_owner = m_context.entity_mgr.get_entity(hook.owner);

            if (hook_owner.has<AIComponent>()) {
              TransformComponent &hook_owner_trans = hook_owner;
              TransformComponent &hooked_target_trans = first_entity;

              hooked_target_trans.transform.set_position(
                  hooked_target_trans.transform.get_position() + glm::vec3(0, 0, 0.3), m_context);

              // Attach hook to hit target
              // AttachToEntityComponent atec;
              // atec.entity_handle = second_handle;
              // gmp::Transform offset;  // add offset so the hook is attached
              // offset.set_position({0.5f, -4.0f, 1.0f}, m_context);
              // offset.rotate({0.0f, 1.0f, 0.0f}, glm::pi<float>() / 2.0f, m_context);
              // atec.offset_transform = offset;
              // first_entity.attach(atec);

              AttachToJointComponent atjc;
              atjc.entity_handle = second_handle;
              atjc.joint_name = std::string("pelvis");
              gmp::Transform offset;  // add offset so the hook is attached

              offset.set_position({50.0f, -150.0f, 100.0f}, m_context);
              offset.rotate({0.0f, 1.0f, 0.0f}, glm::pi<float>() / 2.0f, m_context);
              // atec.offset_transform = offset;
              // first_entity.attach(atec);
              // offset.set_position({0.5f, -4.0f, 1.0f}, m_context);
              // offset.rotate({0.0f, 1.0f, 0.0f}, glm::pi<float>() / 2.0f, m_context);
              atjc.offset_matrix = offset.get_model_matrix();
              first_entity.attach(atjc);

              ForceMoveToEntityComponent move;
              move.target = hook.owner;
              glm::vec3 move_vec = hook_owner_trans.transform.get_position() -
                                   hooked_target_trans.transform.get_position();
              move.release_distance = hook.release_distance;
              move.speed = hook.hook_pull_in_speed;
              first_entity.attach(move);
              second_entity.attach(move);

              // attach HookHitComponent to owner of the hook
              HookHitComponent hhc;
              hhc.hook = first_handle;
              hhc.target = second_handle;
              if (first_entity.has<HookComponent>()) {
                auto &hc = first_entity.get<HookComponent>();
                hhc.rope = hc.rope;
              }
              hook_owner.attach(hhc);

              // Removes HookComponent from hook because it has already hit a target
              first_entity.detach<HookComponent>();
              // hook.hooked = true;

              AIComponent &hook_owner_ai = hook_owner;
              hook_owner_ai.misc = std::to_string(move.release_distance);

              // OBS! decouples camera atm
              StunEvent stun;
              stun.source = "hook";
              stun.target = second_handle;
              stun.duration_time =
                  glm::length(move_vec) / move.speed;  // depends on range and hook return speed
              // stun.duration_time = 5;
              m_context.event_mgr.send_event(stun);
              if (first_entity.has<PhysicsProjectileComponent>()) {
                PhysicsProjectileComponent &ppc = first_entity;
                ppc.contents.set_has_hit(true);
                // ppc.contents.disable_collision_reports_for(999);
              }
              if (first_entity.has<DynamicComponent>()) {
                first_entity.detach<DynamicComponent>();
              }
              if (first_entity.has<LifetimeComponent>()) {
                first_entity.detach<LifetimeComponent>();
              }
            }
          }
        } else if (first_entity.has<PhysicsProjectileComponent>() &&
                   second_entity.has<PhysicsProjectileComponent>()) {
          // auto &second_ppc = second_entity.get<PhysicsProjectileComponent>();
          if (validate_projectile_collision(first_entity, second_entity)) {
            auto &first_ppc = first_entity.get<PhysicsProjectileComponent>();
            first_ppc.contents.set_has_hit(true);
          } else {
            // Attach LifetimeComponent to smaller objective to destroy it
            // LifetimeComponent lc;
            // lc.time_left = 0.0f;
            // second_entity.attach(lc);
          }
        } else {
          // hook hit terrain
          if (!first_entity.has<HookHitComponent>()) {
            if (first_entity.has<PhysicsProjectileComponent>()) {
              auto &first_ppc = first_entity.get<PhysicsProjectileComponent>();
              first_ppc.contents.set_has_hit(true);
              DynamicComponent dc;
              dc.contents.set_mass(m_context, first_ppc.contents.get_mass());
              dc.contents.set_collission_shape(
                  m_context, std::make_shared<phy::BoxCollisionShape>(glm::vec3(1.4f, 3.0f, 0.2f)));
              dc.contents.apply_impulse(first_ppc.contents.get_velocity());
              first_entity.attach(dc);
            }
            LifetimeComponent lc;
            lc.time_left = 6.0f;
            first_entity.attach(lc);
          }
        }
      }

      if (first_entity.has<KnockBackComponent>()) {
        KnockBackComponent &kbc = first_entity;
        TransformComponent &tc = first_entity;
        if (kbc.radius == 0) {  // only apply knock to collided entity
          if (second_entity.has<DynamicComponent>()) {
            DynamicComponent &second_dc = second_entity;
            TransformComponent &second_tc = second_entity;

            glm::vec3 dir = glm::normalize(
                glm::vec3(second_tc.transform.get_position() - tc.transform.get_position()));

            second_dc.contents.apply_force(dir * kbc.force);
          } else if (second_entity.has<CharacterControllerComponent>()) {
            KnockedBackStunComponent kbsc;
            TransformComponent &second_tc = second_entity;
            CharacterControllerComponent &second_ccc = second_entity;

            if (second_ccc.contents.get_mass() / kbc.force < 5) {  // <----
              glm::vec3 dir = glm::normalize(
                  glm::vec3(second_tc.transform.get_position() - tc.transform.get_position()));

              for (int i = 0; i < 3; ++i)
                if (kbc.force_dir_override[i].apply_force) dir[i] = kbc.force_dir_override[i].force;

              kbsc.max_time = kbc.max_time;
              kbsc.movement = (dir * kbc.force);
              second_entity.attach(kbsc);
            }
          }
        } else {  // apply knock to area next update cycle
          TransformComponent &tc = first_entity;
          if (second_entity.has<CharacterControllerComponent>()) {
            CharacterControllerComponent &second_ccc = second_entity;
            if (second_ccc.contents.get_mass() / kbc.force < 5) {
              area_knock_back_vector.push_back(std::make_pair(kbc, tc));
            }
          } else {
            area_knock_back_vector.push_back(std::make_pair(kbc, tc));
          }
        }
      }
    }
  }
}

void gmp::CrowdControlSystem::read_update(const cor::FrameContext &context) {
  m_pos_force_entities = context.entity_mgr.get_entities(G_POSITION_FORCE_KEY);
  m_ent_force_entities = context.entity_mgr.get_entities(G_ENTITY_FORCE_KEY);
  m_hook_entities = context.entity_mgr.get_entities(G_HOOK_KEY);
  m_ccc_entities = context.entity_mgr.get_entities(G_CCC_KEY);
  m_dc_entities = context.entity_mgr.get_entities(G_DC_KEY);
  m_knocked_back_stun_entities = context.entity_mgr.get_entities(G_KNOCKED_BACK_STUN_KEY);
}

void gmp::CrowdControlSystem::write_update(cor::FrameContext &context) {
  handle_force_move(context);
  handle_hooks(context);
  handle_knock_back(context);
}

void gmp::CrowdControlSystem::handle_force_move(cor::FrameContext &context) {
  for (auto &entity_handle : m_pos_force_entities) {
    cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
    if (!entity.is_valid() || !entity.has(G_POSITION_FORCE_KEY)) {
      continue;
    }
    ForceMoveToPositionComponent &move = entity;
    TransformComponent &trans = entity;
    CharacterControllerComponent &ccc = entity;

    glm::vec3 move_vec = move.destination - trans.transform.get_position();
    glm::vec3 direction = glm::normalize(move_vec);

    ccc.contents.dash(direction * move.speed, context.dt);

    if (glm::distance(move.destination, trans.transform.get_position()) < move.release_distance) {
      entity.detach<ForceMoveToPositionComponent>();
      if (entity.has<HookComponent>()) {
        // entity.detach<HookComponent>();
        // HookComponent &hook = entity;
        // hook.destination_reached = true;
        entity.destroy();
      }
    }
  }

  for (auto &entity_handle : m_ent_force_entities) {
    cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
    if (!entity.is_valid() || !entity.has(G_ENTITY_FORCE_KEY)) {
      continue;
    }
    ForceMoveToEntityComponent &move = entity;
    TransformComponent &trans = entity;
    CharacterControllerComponent &ccc = entity;

    if (context.entity_mgr.is_valid(move.target)) {
      TransformComponent &target_trans = context.entity_mgr.get_entity(move.target);

      glm::vec3 move_vec = target_trans.transform.get_position() - trans.transform.get_position();
      glm::vec3 direction = glm::normalize(move_vec);

      ccc.contents.dash(direction * move.speed, context.dt);

      if (glm::distance(target_trans.transform.get_position(), trans.transform.get_position()) <
          move.release_distance) {
        entity.detach<ForceMoveToEntityComponent>();
        if (entity.has<HookComponent>()) {
          entity.detach<HookComponent>();
          HookComponent &hook = entity;
          hook.destination_reached = true;
        }
      }
    } else {
      // if entity we are moving towards does not exist...
      entity.detach<ForceMoveToEntityComponent>();
      if (entity.has<HookComponent>()) {
        entity.detach<HookComponent>();
        HookComponent &hook = entity;
        hook.destination_reached = true;
      }
    }
  }
}

void gmp::CrowdControlSystem::handle_hooks(cor::FrameContext &context) {
  for (auto &entity_handle : m_hook_entities) {
    cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
    if (!entity.is_valid() || !entity.has(G_HOOK_KEY)) {
      continue;
    }
    HookComponent &hook = entity;

    if (hook.destination_reached) {
      entity.destroy();
    }
  }
}

void gmp::CrowdControlSystem::handle_knock_back(cor::FrameContext &context) {
  while (area_knock_back_vector.size() > 0) {
    KnockBackComponent &kbc_knock = std::get<0>(area_knock_back_vector.back());
    TransformComponent &tc_knock = std::get<1>(area_knock_back_vector.back());

    for (auto &entity_handle : m_ccc_entities) {
      cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
      if (!entity.is_valid() || !entity.has(G_CCC_KEY)) {
        continue;
      }
      TransformComponent &tc = entity;

      float range = glm::distance(tc.transform.get_position(), tc_knock.transform.get_position());

      if (range < kbc_knock.radius) {
        glm::vec3 dir = glm::normalize(
            glm::vec3(tc.transform.get_position() - tc_knock.transform.get_position()));

        for (int i = 0; i < 3; ++i)
          if (kbc_knock.force_dir_override[i].apply_force)
            dir[i] = kbc_knock.force_dir_override[i].force;

        float force = kbc_knock.force - (kbc_knock.force * (range / kbc_knock.radius));

        KnockedBackStunComponent kbsc;
        kbsc.max_time = kbc_knock.max_time;
        kbsc.movement = dir * force;
        entity.attach(kbsc);
      }
    }

    for (auto &entity_handle : m_dc_entities) {
      cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
      if (!entity.is_valid() || !entity.has(G_DC_KEY)) {
        continue;
      }
      TransformComponent &tc = entity;

      float range = glm::distance(tc.transform.get_position(), tc_knock.transform.get_position());

      if (range < kbc_knock.radius) {
        glm::vec3 dir = glm::normalize(
            glm::vec3(tc.transform.get_position() - tc_knock.transform.get_position()));

        for (int i = 0; i < 3; ++i)
          if (kbc_knock.force_dir_override[i].apply_force)
            dir[i] = kbc_knock.force_dir_override[i].force;

        float force = kbc_knock.force - (kbc_knock.force * (range / kbc_knock.radius));

        DynamicComponent &dc = entity;
        dc.contents.apply_force(dir * force);
      }
    }

    area_knock_back_vector.pop_back();
  }

  for (auto &entity_handle : m_knocked_back_stun_entities) {
    cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
    if (!entity.is_valid() || !entity.has(G_KNOCKED_BACK_STUN_KEY)) {
      continue;
    }
    KnockedBackStunComponent &kbsc = entity;
    CharacterControllerComponent &ccc = entity;

    // stun time
    kbsc.current_time += context.dt;
    if (ccc.contents.on_ground()) kbsc.ground_time += context.dt;

    // attenuation & movement
    // kbsc.movement -= kbsc.movement * (glm::vec3(0, 0, 9.82f) * context.dt);
    kbsc.movement -= kbsc.movement * (glm::vec3(0, 0, 2.f) * context.dt);
    kbsc.movement *= 1 - 0.1 * context.dt;
    ccc.contents.dash(kbsc.movement, context.dt);
    // ccc.contents.dash(glm::vec3(50, 50, 100), context.dt);

    if (kbsc.current_time > kbsc.max_time || kbsc.ground_time > 1) {
      entity.detach<KnockedBackStunComponent>();

      // abort stun of player & AI
      //...
    }
  }
}