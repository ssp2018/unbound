#include "aud/audio_manager.hpp"                              // for AudioMa...
#include "aud/sound_buffer.hpp"                               // for SoundBu...
#include "aud/sound_source.hpp"                               // for SoundSo...
#include "bse/asset.hpp"                                      // for Asset
#include "bse/log.hpp"                                        // for WARNING
#include "cor/entity_manager.tcc"                             // for EntityM...
#include "cor/event.hpp"                                      // for Collisi...
#include "cor/frame_context.hpp"                              // for FrameCo...
#include "cor/key.hpp"                                        // for Key
#include <cor/entity.tcc>                                     // for Entity
#include <cor/event_manager.hpp>                              // for EVENT_R...
#include <gmp/components/character_controller_component.hpp>  // for Charact...
#include <gmp/components/debug_camera_component.hpp>          // for Dynamic...
#include <gmp/components/dynamic_component.hpp>               // for Dynamic...
#include <gmp/components/material_component.hpp>
#include <gmp/components/physics_projectile_component.hpp>
#include <gmp/components/player_component.hpp>      // for PlayerC...
#include <gmp/components/projection_component.hpp>  // for ProjecC...
#include <gmp/components/static_component.hpp>      // for StaticC...
#include <gmp/components/transform_component.hpp>   // for Transform
#include <gmp/systems/audio_system.hpp>             // for AudioSy...
namespace gmp {
struct DebugCameraComponent;
}
namespace gmp {
struct PlayerComponent;
}
namespace gmp {
struct ProjectionComponent;
}

namespace gmp {
namespace {
static const cor::Key G_PLAYER_KEY = cor::Key::create<PlayerComponent, TransformComponent>();
static const cor::Key G_CAMERA_KEY = cor::Key::create<ProjectionComponent, TransformComponent>();
static const cor::Key G_AUDIO_KEY = cor::Key::create<AudioComponent, TransformComponent>();
}  // namespace

AudioSystem::AudioSystem(cor::FrameContext& context) : m_context{context} {
  // Registers the audio_event_handler function to be called whenever a PlaySoundEvent is called
  add_listener(EVENT_REGISTER_MEMBER_FUNCTION_I(PlaySoundEvent, context.event_mgr,
                                                audio_play_sound_event_handler));
  add_listener(EVENT_REGISTER_MEMBER_FUNCTION_I(PlaySoundDelayedEvent, context.event_mgr,
                                                audio_play_sound_delayed_event_handler));
  add_listener(EVENT_REGISTER_MEMBER_FUNCTION_I(StopSoundEvent, context.event_mgr,
                                                audio_stop_sound_event_handler));
  add_listener(EVENT_REGISTER_MEMBER_FUNCTION_I(ChangeMasterVolumeEvent, context.event_mgr,
                                                audio_change_master_volume_event_handler));
  add_listener(EVENT_REGISTER_MEMBER_FUNCTION_I(ChangeMasterPitchEvent, context.event_mgr,
                                                audio_change_master_pitch_event_handler));

  add_listener(
      EVENT_REGISTER_MEMBER_FUNCTION_I(CollisionEvent, context.event_mgr, collision_event_handler));

  m_context.audio_mgr.add_sound_group("walking");
  m_context.audio_mgr.add_to_group("walking", "footsteps1",
                                   bse::ASSETS_ROOT / "audio/effects/boss1/footstep1.ogg"_fp);
  m_context.audio_mgr.add_to_group("walking", "footsteps2",
                                   bse::ASSETS_ROOT / "audio/effects/boss1/footstep2.ogg"_fp);
  m_context.audio_mgr.add_to_group("walking", "footsteps3",
                                   bse::ASSETS_ROOT / "audio/effects/boss1/footstep3.ogg"_fp);
  m_context.audio_mgr.add_to_group("walking", "footsteps4",
                                   bse::ASSETS_ROOT / "audio/effects/boss1/footstep4.ogg"_fp);
  m_context.audio_mgr.add_to_group("walking", "footsteps5",
                                   bse::ASSETS_ROOT / "audio/effects/boss1/footstep5.ogg"_fp);

  aud::SoundSettings boss_melee_attack_ss = aud::SoundSettings();
  boss_melee_attack_ss.rolloff_factor = 0.25f;

  m_context.audio_mgr.add_sound_group("boss_melee_attack");
  m_context.audio_mgr.add_to_group("boss_melee_attack", "melee1",
                                   bse::ASSETS_ROOT / "audio/effects/boss1/melee1.ogg"_fp);
  m_context.audio_mgr.add_to_group("boss_melee_attack", "melee2",
                                   bse::ASSETS_ROOT / "audio/effects/boss1/melee2.ogg"_fp);
  m_context.audio_mgr.add_to_group("boss_melee_attack", "melee3",
                                   bse::ASSETS_ROOT / "audio/effects/boss1/melee3.ogg"_fp);

  m_context.audio_mgr.add_sound_group("ambient");
  aud::SoundSettings player_ambient_ss;
  player_ambient_ss.gain = 0.03f;
  m_context.audio_mgr.add_to_group(
      "ambient", "forest_ambient",
      bse::ASSETS_ROOT / "audio/effects/ambient/ambient_forest.ogg"_fp);

  /*m_context.audio_mgr.add_sound_group("walking");
  m_context.audio_mgr.add_to_group("walking", "footsteps1",
                            bse::ASSETS_ROOT / "audio/effects/boss1/footstep1.ogg"_fp,
                            aud::SoundSettings());
  m_context.audio_mgr.add_to_group("walking", "footsteps2",
                            bse::ASSETS_ROOT / "audio/effects/boss1/footstep2.ogg"_fp,
                            aud::SoundSettings());
  m_context.audio_mgr.add_to_group("walking", "footsteps3",
                            bse::ASSETS_ROOT / "audio/effects/boss1/footstep3.ogg"_fp,
                            aud::SoundSettings());
  m_context.audio_mgr.add_to_group("walking", "footsteps4",
                            bse::ASSETS_ROOT / "audio/effects/boss1/footstep4.ogg"_fp,
                            aud::SoundSettings());
  m_context.audio_mgr.add_to_group("walking", "footsteps5",
                            bse::ASSETS_ROOT / "audio/effects/boss1/footstep5.ogg"_fp,
                            aud::SoundSettings());*/

  /* aud::SoundSettings boss_melee_attack_ss = aud::SoundSettings();
   boss_melee_attack_ss.rolloff_factor = 0.25f;

   m_context.audio_mgr.add_sound_group("boss_melee_attack");
   m_context.audio_mgr.add_to_group("boss_melee_attack", "melee1",
                             bse::ASSETS_ROOT / "audio/effects/boss1/melee1.ogg"_fp,
                             boss_melee_attack_ss);
   m_context.audio_mgr.add_to_group("boss_melee_attack", "melee2",
                             bse::ASSETS_ROOT / "audio/effects/boss1/melee2.ogg"_fp,
                             boss_melee_attack_ss);
   m_context.audio_mgr.add_to_group("boss_melee_attack", "melee3",
                             bse::ASSETS_ROOT / "audio/effects/boss1/melee3.ogg"_fp,
                             boss_melee_attack_ss);*/

  m_context.audio_mgr.add_sound_group("sword_swinging");
  m_context.audio_mgr.add_to_group(
      "sword_swinging", "sword_swinging1",
      bse::ASSETS_ROOT / "audio/effects/grunts/post/Fast_Sword_Swing_Sound.ogg"_fp);

  m_context.audio_mgr.add_sound_group("skull_repeating");
  m_context.audio_mgr.add_to_group(
      "skull_repeating", "monster1",
      bse::ASSETS_ROOT / "audio/effects/flying_skull/Repeating/monster1.ogg"_fp);

  aud::SoundSettings set;
  set.gain = 8.5f;

  m_context.audio_mgr.add_to_group(
      "skull_repeating", "monster2",
      bse::ASSETS_ROOT / "audio/effects/flying_skull/Repeating/monster2.ogg"_fp);
}

AudioSystem::~AudioSystem() {}

void AudioSystem::read_update(const cor::FrameContext& context) {
  m_players = context.entity_mgr.get_entities(G_PLAYER_KEY);
  m_cameras = context.entity_mgr.get_entities(G_CAMERA_KEY);
  m_audios = context.entity_mgr.get_entities(G_AUDIO_KEY);
}

void AudioSystem::write_update(cor::FrameContext& context) {
  if (m_players.empty()) {
    return;
  }

  cor::Entity player = context.entity_mgr.get_entity(m_players.front());

  if (!player.is_valid() || !player.has(G_PLAYER_KEY)) {
    return;
  }

  TransformComponent& tc = player;
  glm::vec3 tmp_vec = tc.transform.get_position();
  m_context.audio_mgr.set_listener_position(tmp_vec);

  // get the camera entity and use it to set listener forward and up
  for (auto& camera_entity_handle : m_cameras) {
    auto cam_entity = context.entity_mgr.get_entity(camera_entity_handle);
    if (!cam_entity.is_valid() || !cam_entity.has(G_CAMERA_KEY)) {
      continue;
    }

    auto debug_cam_key = cor::Key::create<DebugCameraComponent>();
    // If the camera is not the debug camera
    if (!cam_entity.has(debug_cam_key)) {
      TransformComponent& tc = cam_entity;

      // set listener forward
      glm::vec3 tmp_vec = tc.transform.get_model_matrix() * glm::vec4(0.f, 1.f, 0.f, 0.f);
      m_context.audio_mgr.set_listener_forward(tmp_vec);

      // set listener up
      tmp_vec = tc.transform.get_model_matrix() * glm::vec4(0.f, 0.f, 1.f, 0.f);
      m_context.audio_mgr.set_listener_up(tmp_vec);

      // need to set the listener velocity here....

      if (!player.has<CharacterControllerComponent>()) {
        return;
      }
      CharacterControllerComponent& pcc = context.entity_mgr.get_entity(m_players[0]);
      glm::vec3 vel = pcc.contents.get_velocity();
      m_context.audio_mgr.set_listener_velocity(vel);
    }
  }

  // For each entity
  for (auto& entity_handle : m_audios) {
    cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
    if (!entity.is_valid() || !entity.has(G_AUDIO_KEY)) {
      continue;
    }
    AudioComponent& ac = entity;
    TransformComponent& tc = entity;

    glm::vec3 pos = tc.transform.get_position();
    glm::vec3 vel{0.f};

    // retrieve the velocity if any of the components is attached
    if (entity.has<DynamicComponent>()) {
      DynamicComponent& dc = entity;
      vel = dc.contents.get_velocity();
    } else if (entity.has<CharacterControllerComponent>()) {
      CharacterControllerComponent& ccc = entity;
      vel = ccc.contents.get_velocity();
    } else if (entity.has<PhysicsProjectileComponent>()) {
      PhysicsProjectileComponent& ppc = entity;
      vel = ppc.contents.get_velocity();
    }

    // For all of the of existing looping sound groups
    for (auto& periodic_snd_grp : ac.audio.m_periodic_sound_groups) {
      // check if the looping sound group is activated and that it contains sounds
      if (periodic_snd_grp.second.active &&
          m_context.audio_mgr.m_sound_groups[periodic_snd_grp.first].size() > 0) {
        // check if the current source has finished playing and if the sound group has not been
        // playing a sound for the requested amount of time
        if (periodic_snd_grp.second.cur_source_time_left > 0) {
          periodic_snd_grp.second.cur_source_time_left -= m_context.unslowed_dt;
          continue;
        }
        if (periodic_snd_grp.second.silent_time_counter <
            periodic_snd_grp.second.silent_intervall_time) {
          periodic_snd_grp.second.silent_time_counter += m_context.unslowed_dt;
          continue;
        }

        std::string sound_name =
            m_context.audio_mgr.get_random_sound_from_group(periodic_snd_grp.first);

        auto new_ss = m_context.audio_mgr.create_sound_source(
            m_context.audio_mgr.m_sound_groups[periodic_snd_grp.first][sound_name],
            periodic_snd_grp.second.settings);

        new_ss->play(true);
        periodic_snd_grp.second.cur_source_time_left = new_ss->get_time_left();
        ac.audio.sources.push_back(std::move(new_ss));
        periodic_snd_grp.second.silent_time_counter = 0.f;
      }
    }

    // update the entitiy regular source positions
    for (std::vector<bse::RuntimeAsset<aud::SoundSource>>::iterator it = ac.audio.sources.begin();
         it != ac.audio.sources.end();) {
      if ((*it)->is_playing()) {
        (*it)->set_position(pos);
        (*it)->set_velocity(vel);
        (*it)->set_source_val(aud::PITCH, m_global_pitch);
        ++it;
        // ac.sources[i].set_velocity(
        //    aud::float3(0.f, 0.f, 0.f));  // need this from physics or something
      } else {
        it = ac.audio.sources.erase(it);
      }
    }

    for (auto delayed_sound = ac.audio.m_play_sound_vector.begin();
         delayed_sound != ac.audio.m_play_sound_vector.end();) {
      if (delayed_sound->time_left <= 0.f && entity_handle == delayed_sound->handle) {
        // play the sound and remove it from the vector
        auto new_ss = m_context.audio_mgr.create_sound_source(delayed_sound->data.buffer,
                                                              delayed_sound->data.settings);
        new_ss->play(true);
        ac.audio.sources.push_back(std::move(new_ss));

        delayed_sound = ac.audio.m_play_sound_vector.erase(delayed_sound);
      } else {
        delayed_sound->time_left -= m_context.dt;
        delayed_sound++;
      }
    }
  }
}  // namespace gmp

void AudioSystem::audio_play_sound_event_handler(const PlaySoundEvent* event) {
  auto entity = m_context.entity_mgr.get_entity(event->id);
  if (entity.is_valid() && entity.has<AudioComponent>()) {
    AudioComponent& ac = entity;

    // create a sound source and place it in the audio component
    aud::SoundSettings settings = event->settings;

    ac.audio.sources.push_back(std::move(m_context.audio_mgr.create_sound_source(
        bse::Asset<aud::SoundBuffer>(event->buffer_path), settings)));

    // play the sound source
    ac.audio.sources.at(ac.audio.sources.size() - 1)->play(true);
  }
}

void AudioSystem::audio_play_sound_delayed_event_handler(const PlaySoundDelayedEvent* event) {
  auto entity = m_context.entity_mgr.get_entity(event->id);
  if (entity.is_valid() && entity.has<AudioComponent>()) {
    AudioComponent& ac = entity;

    DelayedSoundInfo d_info;
    d_info.data.settings = event->settings;
    d_info.handle = event->id;
    d_info.time_left = event->delay_time;
    d_info.data.buffer = bse::Asset<aud::SoundBuffer>(event->buffer_path);

    if (d_info.data.buffer.get_id() == -1) {
      LOG(WARNING) << "The requested sound buffer is NULL";
      return;
    }
    ac.audio.play_sound_delayed(std::move(d_info));
  }
}

void AudioSystem::audio_stop_sound_event_handler(const StopSoundEvent* event) {
  auto entity = m_context.entity_mgr.get_entity(event->id);
  if (entity.has<AudioComponent>()) {
    AudioComponent& ac = entity;

    for (bse::RuntimeAsset<aud::SoundSource>& source : ac.audio.sources) {
      source->play(false);
    }
  }
}

void AudioSystem::audio_change_master_volume_event_handler(const ChangeMasterVolumeEvent* event) {
  m_context.audio_mgr.change_listener_gain(0.005f * (event->increase ? 1.f : -1.f));
}

void AudioSystem::audio_change_master_pitch_event_handler(const ChangeMasterPitchEvent* event) {
  m_global_pitch = event->new_pitch_val;
}

void AudioSystem::collision_event_handler(const CollisionEvent* event) {
  if (!m_context.entity_mgr.is_valid(event->entity_1) ||
      !m_context.entity_mgr.is_valid(event->entity_2)) {
    return;
  }

  cor::Entity first = m_context.entity_mgr.get_entity(event->entity_1);
  cor::Entity second = m_context.entity_mgr.get_entity(event->entity_2);

  for (int i = 0; i < 2; ++i) {
    if (first.has<AudioComponent>()) {
      if (second.has<MaterialComponent>()) {
        // play sound
        AudioComponent& auc = first;
        MaterialComponent& mc = second;
        auc.audio.play_collision_sound(mc.material, event->hit_point);
      } else {
        cor::Entity temp = first;
        first = second;
        second = temp;
        continue;
      }
    }
    cor::Entity temp = first;
    first = second;
    second = temp;
  }
}
}  // namespace gmp