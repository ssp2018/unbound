#include "gmp/systems/pickup_system.hpp"

#include "cor/entity_manager.tcc"  // for EntityM...
#include "cor/event.hpp"           // for Disable...
#include "cor/event_manager.hpp"   // for EventMa...
#include "cor/frame_context.hpp"   // for FrameCo...
#include "cor/key.hpp"             // for Key
#include "gmp/components/audio_component.hpp"
#include "gmp/components/lifetime_component.hpp"
#include "gmp/components/model_component.hpp"
#include "gmp/components/particles_component.hpp"
#include "gmp/components/pickup_component.hpp"
#include "gmp/components/player_component.hpp"
#include "gmp/components/point_light_component.hpp"
#include "gmp/components/static_component.hpp"
#include "gmp/components/transform_component.hpp"
#include "gmp/components/trigger_script_component.hpp"
#include <bse/edit.hpp>
#include <cor/entity.tcc>  // for Entity
#include <scr/object.hpp>
#include <scr/script.hpp>

namespace gmp {

PickupSystem::PickupSystem(cor::FrameContext &context) {
  // Spawn health orb
  add_listener(context.event_mgr.register_handler<SpawnHealthOrb>(
      [this, &context](const SpawnHealthOrb *event) {
        cor::EntityHandle handle = context.entity_mgr.create_entity();
        cor::Entity orb = context.entity_mgr.get_entity(handle);

        ParticlesComponent pc;
        pc.spawn_per_second = 0.f;
        cor::EntityHandle health_pickup_handle = context.entity_mgr.create_entity();
        cor::Entity health_pickup = context.entity_mgr.get_entity(health_pickup_handle);
        PickupComponent puc;
        puc.type = PickupComponent::HEALTH_ORB;
        puc.time_to_respawn = event->delay;
        puc.respawn_cooldown = -1;
        puc.position = event->position;
        health_pickup = puc;
        TransformComponent tc;
        tc.transform.set_position(puc.position, context);
        tc.transform.set_scale(0.5f, context);
        health_pickup = tc;
        pc.min_start_color = glm::vec4(1, 0, 0, 1);
        pc.max_start_color = glm::vec4(1, 0, 0, 1);
        pc.min_end_color = glm::vec4(0.7, 0, 0, 1);
        pc.max_end_color = glm::vec4(0.7, 0, 0, 1);
        health_pickup = pc;
        AudioComponent ac;
        health_pickup = std::move(ac);
      }));

  m_health_orb_model = bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/health_upgrade.fbx"_fp);
  m_health_orb_model.pre_load();
  m_arrow_pickup_model =
      bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/bomb_arrow_pickup.fbx"_fp);
  m_arrow_pickup_model.pre_load();
  m_swirl_particle = bse::Asset<gfx::Texture>(bse::ASSETS_ROOT / "texture/swirl_particle01.png"_fp);
  m_swirl_particle.pre_load();

  auto entities =
      context.entity_mgr.get_entities(cor::Key::create<TransformComponent, PickupComponent>());
  for (auto entity_handle : entities) {
    cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
    TransformComponent &tc = entity;
    PickupComponent &pc = entity;

    pc.position = tc.transform.get_position();
  }
}

void PickupSystem::read_update(const cor::FrameContext &context) {}

void PickupSystem::write_update(cor::FrameContext &context) {
  //
  cor::Key pickup_key = cor::Key::create<PickupComponent>();
  auto pickup_handles = context.entity_mgr.get_entities(pickup_key);

  cor::Key player_key = cor::Key::create<PlayerComponent>();
  auto &player_handles = context.entity_mgr.get_entities(player_key);
  if (player_handles.size() == 0) {
    return;
  }
  cor::Entity player = context.entity_mgr.get_entity(player_handles[0]);
  if (!player.is_valid()) {
    return;
  }

  for (auto &pickup_handle : pickup_handles) {
    cor::Entity pickup = context.entity_mgr.get_entity(pickup_handle);
    TransformComponent &tc = pickup;

    // Respawning
    PickupComponent &puc = pickup;
    if (puc.time_to_respawn >= 0.f) {
      puc.time_to_respawn -= context.dt;
    }

    // Eligible to respawn
    if (puc.time_to_respawn < 0.f && puc.time_to_respawn > -.95f) {
      TransformComponent &ptc = player;
      // (Re)spawn if player is far enough away or the pick is one time only
      if (glm::length(ptc.transform.get_position() - tc.transform.get_position()) >
              puc.player_range ||
          puc.respawn_cooldown < 0.f) {
        //
        puc.time_to_respawn = -1;
        puc.vertical_value = rand() / RAND_MAX * 7.f;
        ModelComponent mc;
        PointLightComponent plc;
        plc.radius = 25.f;
        TriggerScriptComponent tsc;
        tsc.target_entity = pickup_handle;

        StaticComponent sc;
        sc.contents.set_is_trigger(true);
        sc.contents.set_collission_shape(context,
                                         std::make_shared<phy::SphereCollisionShape>(1.0f));
        pickup = sc;

        // Need to reset the values after particle burst
        ParticlesComponent &pc = pickup;
        pc.spawn_per_second = 15;
        pc.spawn_duration_left = -1;
        pc.min_life = 0.5f;
        pc.max_life = 1.f;
        pc.min_start_size = 0.1f;
        pc.max_start_size = 0.3f;
        pc.min_spawn_pos = glm::vec3(-0.2, -0.2, 0.7);
        pc.max_spawn_pos = glm::vec3(0.2, 0.2, 0.7);
        pc.min_start_velocity = glm::vec3(-0.2, -0.2, 1);
        pc.max_start_velocity = glm::vec3(0.2, 0.2, 2);
        pc.velocity_mode = ParticlesComponent::Velocity::CONSTANT;
        pc.min_rotation_speed = 3.f;
        pc.max_rotation_speed = 5.f;
        pc.texture = m_swirl_particle;

        // Type specific stuff
        switch (puc.type) {
          case PickupComponent::HEALTH_ORB: {
            mc.asset = m_health_orb_model;
            tsc.on_touch_callback =
                bse::Asset<scr::Script>(bse::ASSETS_ROOT / "script/gmp/trigger_callbacks.lua"_fp)
                    ->return_value()["pickup_health_orb_callback"];
            tsc.name = "health_orb";
            plc.color = glm::vec3(1, 0.5, 0.4);
            LifetimeComponent ltc;
            ltc.time_left = 60.f;
            pickup = ltc;
            break;
          }
          case PickupComponent::EXPLOSIVE_ARROWS: {
            mc.asset = m_arrow_pickup_model;
            tsc.target_entity = pickup_handle;
            tsc.on_touch_callback =
                bse::Asset<scr::Script>(bse::ASSETS_ROOT / "script/gmp/trigger_callbacks.lua"_fp)
                    ->return_value()["pickup_explosive_arrows_callback"];
            tsc.name = "arrow_pickup";
            plc.color = glm::vec3(1, 1, 1);
            break;
          }
          case PickupComponent::NOTE: {
            mc.asset = bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/scroll.fbx"_fp);
            tsc.target_entity = pickup_handle;
            tsc.on_touch_callback =
                bse::Asset<scr::Script>(bse::ASSETS_ROOT / "script/gmp/trigger_callbacks.lua"_fp)
                    ->return_value()["pickup_note_callback"];
            tsc.name = "note_pickup";
            plc.color = glm::vec3(1, 1, 1);
            break;
          }
          case PickupComponent::HORN: {
            mc.asset = bse::Asset<gfx::Model>(bse::ASSETS_ROOT / "model/horn.fbx"_fp);
            tsc.target_entity = pickup_handle;
            tsc.on_touch_callback =
                bse::Asset<scr::Script>(bse::ASSETS_ROOT / "script/gmp/trigger_callbacks.lua"_fp)
                    ->return_value()["pickup_horn_callback"];
            tsc.name = "horn_pickup";
            plc.color = glm::vec3(1, 1, 1);
            break;
          }
        }

        pickup = mc;
        pickup = plc;
        pickup = tsc;
      }
    }

    // Rotation
    tc.transform.rotate(glm::vec3(0, 0, 1), context.dt * 0.6f, context);

    // Floating
    puc.vertical_value += context.dt * 1.7f;
    tc.transform.set_position(puc.position + glm::vec3(0, 0, sinf(puc.vertical_value) * 0.2),
                              context);
  }
}  // namespace gmp

}  // namespace gmp
