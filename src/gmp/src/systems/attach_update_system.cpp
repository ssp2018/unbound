#include "gmp/systems/attach_update_system.hpp"

#include "bse/asset.hpp"                                  // for Asset
#include "bse/file_path.hpp"                              // for operator<<
#include "bse/mesh/animation_clip.hpp"                    // for KeyFrame, SQT
#include "bse/mesh/joint.hpp"                             // for Joint
#include "bse/mesh/skeleton.hpp"                          // for Skeleton
#include "cor/entity_manager.tcc"                         // for EntityManager
#include "cor/frame_context.hpp"                          // for FrameContext
#include "cor/key.hpp"                                    // for Key
#include "gfx/model.hpp"                                  // for Model
#include "gmp/animation/transform_pipeline.hpp"           // for TransformPip...
#include "gmp/components/attach_to_joint_component.hpp"   // for AttachToJoin...
#include "gmp/components/model_component.hpp"             // for ModelComponent
#include "gmp/components/transform_component.hpp"         // for Transform
#include "gmp/components/transforms_component.hpp"        // for TransformsCo...
#include <bse/log.hpp>                                    // for WARNING, Log
#include <cor/entity.tcc>                                 // for Entity
#include <gmp/animation/animation.hpp>                    // for Animation
#include <gmp/components/animation_component.hpp>         // for AnimationCom...
#include <gmp/components/attach_to_entity_component.hpp>  // for AnimationCom...
namespace cor {
struct EntityHandle;
}

namespace gmp {

void AttachUpdateSystem::read_update(const cor::FrameContext& context) {}

void AttachUpdateSystem::write_update(cor::FrameContext& context) {
  cor::Key key = cor::Key::create<AttachToEntityComponent, TransformComponent>();
  auto handles = context.entity_mgr.get_entities(key);
  for (auto handle : handles) {
    if (context.entity_mgr.is_valid(handle)) {
      auto entity = context.entity_mgr.get_entity(handle);
      update_entity(context, entity);
    }
  }
}

void AttachUpdateSystem::update_entity(cor::FrameContext& context, cor::Entity& entity) {
  TransformComponent& tc = entity;
  AttachToEntityComponent& atec = entity;
  if (atec.m_frame_updated != context.frame_num) {
    if (context.entity_mgr.is_valid(atec.entity_handle)) {
      auto parent_entity = context.entity_mgr.get_entity(atec.entity_handle);
      TransformComponent ptc = parent_entity;
      if (ptc.transform.get_frame_updated() >= context.frame_num - 1) {
        // check if parent needs to be updated.
        if (parent_entity.has<AttachToEntityComponent, TransformComponent>()) {
          // update parent
          update_entity(context, parent_entity);
        }
        // update this.
        tc.transform.set_model_matrix(
            ptc.transform.get_model_matrix() * atec.offset_transform.get_model_matrix(), context);
        atec.m_frame_updated = context.frame_num;
      }
    }
  }
}
}  // namespace gmp
