#include "gmp/systems/animation_system.hpp"

#include "bse/asset.hpp"                         // for Asset
#include "bse/file_path.hpp"                     // for operator<<
#include "bse/mesh/animation_clip.hpp"           // for KeyFrame, SQT
#include "bse/mesh/joint.hpp"                    // for Joint
#include "bse/mesh/skeleton.hpp"                 // for Skeleton
#include "cor/entity_manager.tcc"                // for EntityManager
#include "cor/frame_context.hpp"                 // for FrameContext
#include "cor/key.hpp"                           // for Key
#include "gfx/model.hpp"                         // for Model
#include "gmp/animation/transform_pipeline.hpp"  // for TransformPip...
#include "gmp/components/ai_component.hpp"
#include "gmp/components/aim_offset_component.hpp"
#include "gmp/components/attach_to_joint_component.hpp"  // for AttachToJoin...
#include "gmp/components/joint_look_at_component.hpp"
#include "gmp/components/model_component.hpp"       // for ModelComponent
#include "gmp/components/transform_component.hpp"   // for Transform
#include "gmp/components/transforms_component.hpp"  // for TransformsCo...
#include <bse/log.hpp>                              // for WARNING, Log
#include <bse/utility.hpp>
#include <cor/entity.tcc>  // for Entity
#include <ext/ext.hpp>
#include <gfx/frustum_shape.hpp>
#include <gmp/animation/animation.hpp>  // for Animation
#include <gmp/animation/skinning_streaming_task.hpp>
#include <gmp/components/animation_component.hpp>  // for AnimationCom...
#include <gmp/components/attach_to_triangle_component.hpp>
#include <gmp/components/character_controller_component.hpp>
#include <gmp/components/dynamic_component.hpp>
#include <gmp/components/name_component.hpp>
#include <gmp/components/physics_projectile_component.hpp>
#include <gmp/components/player_component.hpp>
#include <gmp/components/projection_component.hpp>
#include <gmp/components/static_component.hpp>
#include <gmp/components/transform_component.hpp>

// Don't use overloaded new/delete here, bullet has them overloaded
#undef new

namespace cor {
struct EntityHandle;
}

namespace gmp {

AnimationSystem::AnimationSystem(cor::FrameContext& context) {
  m_animation_event_lid =
      EVENT_REGISTER_MEMBER_FUNCTION_I(AnimationEvent, context.event_mgr, animation_event_listener);
}

void AnimationSystem::animation_event_listener(const AnimationEvent* e) {
  // Fetch event and store it

  for (int i = 0; i < m_animation_events.size(); ++i) {
    if (m_animation_events.at(i).handle == e->handle) {
      m_animation_events.erase(m_animation_events.begin() + i);
      i--;
    }
  }

  AnimationEvent local;
  local.animation_name = e->animation_name;
  local.handle = e->handle;
  local.blend_time = e->blend_time;
  m_animation_events.push_back(local);
}

void AnimationSystem::read_update(const cor::FrameContext& context) {
  m_new_mesh_datas.clear();

  update_animations(context);

  skin_all_meshes(context);
}

void AnimationSystem::update_animations(const cor::FrameContext& context) const {
  auto animated_entities =
      context.entity_mgr.get_entities(cor::Key::create<ModelComponent, AnimationComponent>());
  for (const cor::EntityHandle& handle : animated_entities) {
    cor::ConstEntity entity = context.entity_mgr.get_entity(handle);
    const AnimationStateMachine& state_machine = entity.get<AnimationComponent>().state_machine;
    const AnimationStateMachine& attack_state_machine =
        entity.get<AnimationComponent>().attack_state_machine;
    const AnimationEventPlayer& animation_event_player =
        entity.get<AnimationComponent>().animation_event_player;
    const ModelComponent& model = entity.get<ModelComponent>();
    // const TransformComponent& transform = entity.get<TransformComponent>();

    if (!state_machine.get_tree()) {
      continue;
    }

    state_machine.update(handle, context.entity_mgr, context.dt);
    attack_state_machine.update(handle, context.entity_mgr, context.dt);
    const std::vector<bse::SQT>& quat_transforms = state_machine.get_tree()->get_sqt_palette();

    std::vector<bse::SQT> final_sqt;

    const bse::Skeleton& skel = model.asset->get_mesh()->get_skeleton();

    bool use_attack_state_machine = (attack_state_machine.get_machine() != "");

    if (use_attack_state_machine) {
      final_sqt = quat_transforms;

      const std::vector<bse::SQT>& attack_quat_transforms =
          attack_state_machine.get_tree()->get_sqt_palette();

      // Blend regular state machine and attack state machine
      blend_quat_transforms(final_sqt, quat_transforms, attack_quat_transforms,
                            attack_state_machine.get_start_joint(),
                            attack_state_machine.get_tree()->get_machine_weight(), skel);
    }

    // If attack state machine is not enabled, directly use sqts from regular state machine
    const std::vector<bse::SQT>& final_result =
        use_attack_state_machine ? final_sqt : quat_transforms;

    std::vector<bse::SQT> quat_palette = final_result;

    auto it = m_animation_events.begin();
    while (it != m_animation_events.end()) {
      auto& animation_event = *it;
      if (animation_event.animation_name != "") {
        if (handle.full_handle == animation_event.handle.full_handle) {
          // Animation event found
          // if (animation_event.)
          // If animation event player returns null, delete it, animation is done
          // quat_transforms

          const AnimationNode* animation_node = animation_event_player.get_animation_node();

          if (animation_node && animation_node->get_animation() &&
              animation_event.animation_name == animation_node->get_animation()->m_name) {
            // If same, update animation
            auto result = animation_event_player.update(
                context.dt, m_animation_event_player_notify_events, handle);
            if (!result.has_value()) {
              it = m_animation_events.erase(it);
              continue;
            } else {
              blend_quat_transforms(quat_palette, quat_palette, *result,
                                    skel.get_joint_map().at("root")->m_id,
                                    animation_event_player.get_current_blend_factor(), skel);
            }
          } else {
            // If not the same start playing new animation
            animation_event_player.play_animation(model.asset, animation_event.animation_name,
                                                  animation_event.blend_time);
          }
        }
      }
      it++;
    }

    // Update aim offsets
    if (entity.has<AimOffsetComponent, PlayerComponent>()) {
      update_aim_offset(context, handle, quat_palette);
    }

    static thread_local std::vector<glm::mat4> mat_transforms;
    mat_transforms.clear();

    for (int i = 0; i < quat_transforms.size(); i++) {
      // Get interpolated rotation and translation and multiply them together
      mat_transforms.push_back(glm::translate(glm::mat4(1.0f), quat_palette[i].m_translation) *
                               glm::toMat4(quat_palette[i].m_rotation));
    }

    // Update look at components
    if (entity.has<JointLookAtComponent>()) {
      const JointLookAtComponent& jla = entity;

      cor::ConstEntity target = context.entity_mgr.get_entity(jla.m_entity_to_look_at);
      // Target the player
      auto target_vector = context.entity_mgr.get_entities(cor::Key::create<PlayerComponent>());
      if (target_vector.size()) {
        target = context.entity_mgr.get_entity(target_vector[0]);
      }

      if (target.is_valid()) {
        bse::Skeleton& skeleton = model.asset->get_mesh()->get_skeleton();
        int joint_id = skeleton.get_joint_map().at(jla.m_joint_name)->m_id;
        glm::quat joint_quat = glm::toQuat(mat_transforms[joint_id]);
        glm::vec3 joint_translation = {mat_transforms[joint_id][3]};
        bse::SQT joint_sqt;
        joint_sqt.m_rotation = joint_quat;
        joint_sqt.m_translation = joint_translation;
        update_look_at(entity, *(skeleton.get_joint_map().at(jla.m_joint_name)), joint_sqt, target);
        glm::vec4 column_backup = mat_transforms[joint_id][3];
        mat_transforms[joint_id] = glm::toMat4(joint_sqt.m_rotation);
        // std::cout << joint_id << std::endl;
        mat_transforms[joint_id][3] = column_backup;
      }
    }

    // Create final joint transforms
    create_final_joint_transform(mat_transforms,
                                 glm::inverse(skel.get_root_joint()->m_offset_matrix), 0, skel);

    if (m_mesh_datas.count(handle.full_handle)) {
      m_mesh_datas.at(handle.full_handle)->joint_transforms = mat_transforms;
    }
  }
}

void AnimationSystem::write_update(cor::FrameContext& context) {
  gfx::Context::run_async([tasks = m_buffer_tasks]() {
    for (UpdateModelTask task : tasks) {
      AnimatedMeshData& data = *task.data;
      bse::Asset<gfx::Mesh>& mesh = task.mesh;
      if (task.is_mesh_data_new) {
        gfx::VertexFlags parent_flags = mesh->get_vertex_data_types();
        gfx::VertexFlags flags;
        flags.set(bse::VertexTypes::POSITION);
        if (parent_flags.test(bse::VertexTypes::NORMAL)) {
          flags.set(bse::VertexTypes::NORMAL);
        }
        data.buffers = mesh->get_buffers_copy(flags);
      }

      if (task.is_skinning_dirty) {
        // Upload mesh data
        gl::glBindBuffer(gl::GL_ARRAY_BUFFER, data.buffers.m_vbo_normal);

        gl::glBufferData(gl::GL_ARRAY_BUFFER,
                         sizeof(float) * 3 * data.parent_mesh_data->mesh.vertex_count, data.normals,
                         gl::GL_STREAM_DRAW);

        gl::glBindBuffer(gl::GL_ARRAY_BUFFER, data.buffers.m_vbo_position);

        gl::glBufferData(gl::GL_ARRAY_BUFFER,
                         sizeof(float) * 3 * data.parent_mesh_data->mesh.vertex_count,
                         data.positions, gl::GL_STREAM_DRAW);
      }
    }
  });

  cor::Key animation_key = cor::Key::create<AnimationComponent, TransformComponent>();
  std::vector<cor::EntityHandle> animated_models = context.entity_mgr.get_entities(animation_key);

  // Trigger animation notify events
  for (auto& model : animated_models) {
    cor::Entity animated_model = context.entity_mgr.get_entity(model);
    if (animated_model.has<AnimationComponent>()) {
      AnimationComponent& ac = animated_model;
      for (auto& notify_event : ac.state_machine.get_notify_events()) {
        context.event_mgr.send_event(notify_event);
      }
      ac.state_machine.get_notify_events().clear();
    }
  }

  // Send notifies triggered by animation events
  for (auto& notify_event : m_animation_event_player_notify_events) {
    context.event_mgr.send_event(notify_event);
  }
  m_animation_event_player_notify_events.clear();

  // Update needed mesh datas
  for (auto& [handle, amd] : m_new_mesh_datas) {
    cor::Entity entity = context.entity_mgr.get_entity(handle);

    if (entity.has<AnimationComponent>()) {
      entity.get<AnimationComponent>().data.mesh_data = amd;
    }
  }
}

void AnimationSystem::write_update_late(cor::FrameContext& context) {
  update_attachments(context);
  update_triangle_attachments(context);
}

AnimationSystem::~AnimationSystem() {}

void AnimationSystem::UpdateModelTask::kernel() {
  if (is_skinning_dirty) {
    AnimationSystem::skin_mesh(*data);
  }

  if (is_acceleration_structure_dirty) {
    RecalcAccelerationStructureTask recalc_task(data->mesh_shape);
    recalc_task.kernel();
  }
}

std::vector<AnimationSystem::UpdateModelTask> AnimationSystem::get_update_model_tasks(
    const cor::FrameContext& context) {
  std::vector<UpdateModelTask> tasks;

  cor::Key animation_key =
      cor::Key::create<AnimationComponent, ModelComponent, TransformComponent>();
  std::vector<cor::EntityHandle> animated_models = context.entity_mgr.get_entities(animation_key);

  cor::Key camera_key = cor::Key::create<ProjectionComponent, TransformComponent>();
  std::vector<cor::EntityHandle> cameras = context.entity_mgr.get_entities(camera_key);

  cor::Key projectile_key = cor::Key::create<TransformComponent, PhysicsProjectileComponent>();
  std::vector<cor::EntityHandle> projectile_handles =
      context.entity_mgr.get_entities(projectile_key);

  std::vector<cor::ConstEntity> projectiles;
  for (cor::EntityHandle projectile_handle : projectile_handles) {
    cor::ConstEntity projectile = context.entity_mgr.get_entity(projectile_handle);
    if (projectile.get<PhysicsProjectileComponent>().stick_to_animated) {
      projectiles.push_back(projectile);
    }
  }

  cor::EntityHandle camera_handle;
  cor::ConstEntity camera;
  if (cameras.size()) {
    camera_handle = cameras[0];
  }
  if (!context.entity_mgr.is_valid(camera_handle)) {
    return tasks;
  }
  camera = context.entity_mgr.get_entity(camera_handle);

  const Transform& camera_transform = camera.get<TransformComponent>().transform;
  glm::mat4 camera_projection = camera.get<ProjectionComponent>().projection_matrix;

  glm::mat4 vp = camera_projection * glm::inverse(camera_transform.get_model_matrix());
  gfx::FrustumShape cull_frustum(vp);

  std::vector<bse::TaskHandle> skinning_tasks;
  for (cor::EntityHandle& entity_handle : animated_models) {
    if (!context.entity_mgr.is_valid(entity_handle)) {
      continue;
    }

    bool is_new = !m_mesh_datas.count(entity_handle.full_handle);

    // Add mesh data if not found
    if (is_new) {
      add_mesh_data(context, entity_handle);
    }

    // Set frame updated
    m_mesh_datas[entity_handle.full_handle]->frame_updated = context.frame_num;

    AnimatedMeshData& amd = *m_mesh_datas.at(entity_handle.full_handle);

    cor::ConstEntity entity = context.entity_mgr.get_entity(entity_handle);
    const TransformComponent& tc = entity;

    gfx::AABB aabb;
    size_t skin_interval = 1;
    if (entity.has<StaticComponent>()) {
      const StaticComponent& sc = entity;
      aabb = sc.contents.get_aabb();
    } else if (entity.has<DynamicComponent>()) {
      const DynamicComponent& dc = entity;
      aabb = dc.contents.get_aabb();
    } else if (entity.has<CharacterControllerComponent>()) {
      const CharacterControllerComponent& ccc = entity;
      aabb = ccc.contents.get_aabb();
    } else {
      aabb = entity.get<ModelComponent>().asset->get_mesh()->get_aabb();
      aabb.size *= tc.transform.get_scale();
      aabb.position += tc.transform.get_position();
    }

    if (!is_new) {
      if (!cull_frustum.aabb_intersect(aabb)) {
        continue;
      }

      glm::vec4 p1 = glm::vec4(aabb.position, 1.f);
      glm::vec4 p2 = glm::vec4(aabb.position + aabb.size, 1.f);

      p1 = vp * p1;
      p1 /= p1.w;

      p2 = vp * p2;
      p2 /= p2.w;

      float screen_radius = std::max(std::abs(p1.x - p2.x), std::abs(p1.y - p2.y));
      if (screen_radius < 0.01f) {
        continue;
      }

      skin_interval = std::min(1.f, std::floor(screen_radius / 0.1f));
    }

    // Loop through active projectiles and update mesh acceleration structure if a projectile is
    // close and it hasn't been updated in a while
    bool is_projectile_close = false;
    for (const cor::ConstEntity& projectile : projectiles) {
      const Transform& projectile_transform = projectile.get<TransformComponent>().transform;

      // If the projectile is close
      if (glm::distance(tc.transform.get_position(), projectile_transform.get_position()) >
          50.0f * tc.transform.get_scale()) {
        continue;
      }

      const int update_interval = 13;
      if (context.frame_num - amd.acceleration_structure_frame_updated <= update_interval) {
        continue;
      }

      amd.acceleration_structure_frame_updated = context.frame_num;

      // Only update at most once per frame
      is_projectile_close = true;
      break;
    }

    UpdateModelTask task;
    task.data = m_mesh_datas.at(entity_handle.full_handle);
    task.mesh = entity.get<ModelComponent>().asset->get_mesh();
    task.is_skinning_dirty =
        (is_new || context.frame_num - amd.skinning_frame_updated >= skin_interval);
    if (task.is_skinning_dirty) {
      amd.skinning_frame_updated = context.frame_num;
    }
    task.is_acceleration_structure_dirty = is_projectile_close;
    task.is_mesh_data_new = is_new;
    tasks.push_back(task);
  }

  return tasks;
}

void AnimationSystem::skin_all_meshes(const cor::FrameContext& context) {
  std::vector<UpdateModelTask> update_model_tasks = get_update_model_tasks(context);

  // Remove mesh data of deleted entities
  remove_stale_mesh_data(context);

  bse::TaskScheduler* ts = bse::TaskScheduler::get();
  for (const UpdateModelTask& task : m_update_model_tasks) {
    ts->wait_on_task(task.handle);
  }

  m_buffer_tasks = m_update_model_tasks;
  m_update_model_tasks = update_model_tasks;
  for (UpdateModelTask& task : m_update_model_tasks) {
    task.handle = ts->add_task(task);
  }
}

void AnimationSystem::add_mesh_data(const cor::FrameContext& context, cor::EntityHandle& handle) {
  cor::ConstEntity entity = context.entity_mgr.get_entity(handle);

  if (entity.is_valid() && entity.has<ModelComponent>()) {
    const ModelComponent& model = entity.get<ModelComponent>();
    const AnimationComponent& anim = entity.get<AnimationComponent>();

    ASSERT(!m_mesh_datas.count(handle.full_handle))
        << "Attempted to add duplicate animated mesh!\n";

    // Add mesh data to map
    m_mesh_datas.emplace(std::make_pair(handle.full_handle, new AnimatedMeshData()));

    AnimatedMeshData& amd = *m_mesh_datas[handle.full_handle];
    amd.entity_handle = entity.get_handle();

    std::pair<cor::EntityHandle, AnimatedMeshData*> new_pair;
    new_pair.first = handle;
    new_pair.second = &amd;
    m_new_mesh_datas.push_back(new_pair);

    // Initialize variables
    amd.parent_mesh_data = &(model.asset->get_mesh()->get_model_data());
    amd.num_vertices = amd.parent_mesh_data->mesh.vertex_count;
    anim.data.num_vertices = amd.num_vertices;
    amd.frame_updated = context.frame_num;
    amd.skinning_frame_updated = context.frame_num;

    // Resize joint transform vector
    amd.joint_transforms.resize(amd.parent_mesh_data->skel->num_joints());

    m_new_buffers.emplace_back(handle, model.asset);

    gfx::VertexFlags parent_flags = model.asset->get_mesh()->get_vertex_data_types();

    ASSERT(parent_flags.test(bse::VertexTypes::POSITION))
        << "Animated mesh did not include position vertex data!\n";
    ASSERT(parent_flags.test(bse::VertexTypes::SKELETAL))
        << "Animated mesh did not include skeletal vertex data!\n";

    amd.acceleration_structure_frame_updated = context.frame_num;

    size_t buffer_size(amd.parent_mesh_data->mesh.vertex_count * 3 * sizeof(float));
    amd.positions = new glm::vec3[amd.parent_mesh_data->mesh.vertex_count];
    anim.data.positions = amd.positions;
    // // Copy parent data to animated data
    memcpy(amd.positions, amd.parent_mesh_data->mesh.positions.data(),
           sizeof(float) * 3 * amd.parent_mesh_data->mesh.vertex_count);

    if (parent_flags.test(bse::VertexTypes::NORMAL)) {
      amd.normals = new glm::vec3[amd.parent_mesh_data->mesh.vertex_count];
      anim.data.normals = amd.normals;
      // Copy parent data to animated data
      memcpy(amd.normals, amd.parent_mesh_data->mesh.normals.data(),
             sizeof(float) * 3 * amd.parent_mesh_data->mesh.vertex_count);
    }

    // Add Bullet object that refers to mesh data
    btIndexedMesh mesh_ref;
    mesh_ref.m_numTriangles = amd.parent_mesh_data->mesh.face_count;
    mesh_ref.m_numVertices = amd.parent_mesh_data->mesh.vertex_count;
    mesh_ref.m_triangleIndexBase =
        reinterpret_cast<const unsigned char*>(amd.parent_mesh_data->mesh.indices.data());
    mesh_ref.m_triangleIndexStride = 3 * sizeof(unsigned);
    mesh_ref.m_vertexBase = reinterpret_cast<const unsigned char*>(amd.positions);
    mesh_ref.m_vertexStride = 3 * sizeof(float);

    amd.mesh_vertex_array = new btTriangleIndexVertexArray;
    amd.mesh_vertex_array->addIndexedMesh(mesh_ref);
    amd.mesh_shape = new btBvhTriangleMeshShape(amd.mesh_vertex_array, true);
    amd.mesh_shape->setUserIndex(handle.full_handle);

    anim.data.mesh_shape = amd.mesh_shape;
    anim.data.task = &amd.task;
  }
}

AnimationSystem::AnimatedMeshData* AnimationSystem::get_animated_mesh_data(
    const cor::EntityHandle& handle) {
  if (m_mesh_datas.count(handle.full_handle)) {
    return m_mesh_datas[handle.full_handle].get();
  }
  return nullptr;
}

void AnimationSystem::remove_stale_mesh_data(const cor::FrameContext& context) {
  auto it = m_mesh_datas.begin();

  while (it != m_mesh_datas.end()) {
    auto& [id, mesh_data] = *it;
    cor::ConstEntity entity = context.entity_mgr.get_entity(mesh_data->entity_handle);
    if (!entity.is_valid() || !entity.has<AnimationComponent>()) {
      it = m_mesh_datas.erase(it);
    } else {
      it++;
    }
  }
}

void AnimationSystem::create_final_joint_transform(std::vector<glm::mat4>& joint_transforms,
                                                   glm::mat4 parent_transform, int current_id,
                                                   const bse::Skeleton& skel) const {
  if (current_id >= joint_transforms.size()) {
    return;
  }

  const bse::Joint& current_joint = *skel.get_joint_map().at(skel.get_joint_name(current_id));

  glm::mat4 joint_transform = parent_transform * joint_transforms[current_id];

  joint_transforms[current_id] = joint_transform * current_joint.m_offset_matrix;

  // Repeat for joint's children
  for (auto& child : current_joint.m_children) {
    create_final_joint_transform(joint_transforms, joint_transform, child->m_id, skel);
  }
}

void AnimationSystem::blend_quat_transforms(std::vector<bse::SQT>& output,
                                            const std::vector<bse::SQT>& input1,
                                            const std::vector<bse::SQT>& input2, int current_id,
                                            float weight, const bse::Skeleton& skel) const {
  const bse::Joint& current_joint = *skel.get_joint_map().at(skel.get_joint_name(current_id));

  output[current_id].m_rotation =
      glm::slerp(input1[current_id].m_rotation, input2[current_id].m_rotation, weight);

  output[current_id].m_translation =
      input1[current_id].m_translation +
      (input2[current_id].m_translation - input1[current_id].m_translation) * weight;

  // Repeat for joint's children
  for (auto& child : current_joint.m_children) {
    blend_quat_transforms(output, input1, input2, child->m_id, weight, skel);
  }
}

void AnimationSystem::update_attachments(cor::FrameContext& context) const {
  cor::Key key = cor::Key::create<AttachToJointComponent, TransformComponent>();
  auto entities = context.entity_mgr.get_entities(key);
  for (cor::EntityHandle& handle : entities) {
    cor::Entity entity = context.entity_mgr.get_entity(handle);
    TransformComponent& src_tfc = entity;
    AttachToJointComponent& src_atc = entity;

    // If AnimatedMeshData has not been created, use base mesh
    bool is_animated = m_mesh_datas.count(src_atc.entity_handle.full_handle);

    cor::Entity target_entity = context.entity_mgr.get_entity(src_atc.entity_handle);
    if (!target_entity.is_valid() || !target_entity.has<ModelComponent, TransformComponent>()) {
      continue;
    }

    ModelComponent& tgt_moc = target_entity;
    TransformComponent& tgt_tfc = target_entity;

    const std::map<std::string, std::unique_ptr<bse::Joint>>& joint_map =
        tgt_moc.asset->get_mesh()->get_skeleton().get_joint_map();

    auto joint_it = joint_map.find(src_atc.joint_name);
    if (joint_it == joint_map.end() || !joint_it->second) {
      LOG(WARNING) << "Cannot find joint '" << src_atc.joint_name << "':";
      continue;
    }
    const bse::Joint& joint = *joint_it->second;

    float start_scale = src_tfc.transform.get_scale();

    if (is_animated) {
      src_tfc.transform.set_model_matrix(
          tgt_tfc.transform.get_model_matrix() *
              m_mesh_datas[src_atc.entity_handle.full_handle]->joint_transforms[joint.m_id] *
              glm::inverse(joint.m_offset_matrix) * src_atc.offset_matrix,
          context);
    } else {
      src_tfc.transform.set_model_matrix(tgt_tfc.transform.get_model_matrix() *
                                             glm::inverse(joint.m_offset_matrix) *
                                             src_atc.offset_matrix,
                                         context);
    }
    // Ignore skeleton scale
    src_tfc.transform.set_scale(start_scale, context);
  }
}

void AnimationSystem::update_triangle_attachments(cor::FrameContext& context) {
  cor::Key attach_key = cor::Key::create<AttachToTriangleComponent, TransformComponent>();
  auto attach_entity_handles = context.entity_mgr.get_entities(attach_key);

  for (auto& attach_handle : attach_entity_handles) {
    if (context.entity_mgr.is_valid(attach_handle)) {
      cor::Entity attach = context.entity_mgr.get_entity(attach_handle);

      AttachToTriangleComponent& attc = attach;
      TransformComponent& tc = attach;

      if (context.entity_mgr.is_valid(attc.attached_to)) {
        cor::Entity animated = context.entity_mgr.get_entity(attc.attached_to);

        if (animated.has<AnimationComponent, TransformComponent, ModelComponent>()) {
          AnimationComponent& anim = animated;
          ModelComponent& model = animated;
          TransformComponent& anim_tc = animated;

          const bse::ModelData& parent = model.asset->get_model_data();

          glm::mat4 m = anim_tc.transform.get_model_matrix();

          glm::vec3 triangle_normal =
              glm::cross(anim.data.positions[parent.mesh.indices[attc.triangle_index * 3 + 1]] -
                             anim.data.positions[parent.mesh.indices[attc.triangle_index * 3]],
                         anim.data.positions[parent.mesh.indices[attc.triangle_index * 3 + 2]] -
                             anim.data.positions[parent.mesh.indices[attc.triangle_index * 3]]);

          // Rotate the projectile, then translate to correct position, then move to world space
          // with attached entity's model matrix
          // glm::quat rot =
          //    attc.rotation * glm::rotation(glm::vec3(0, 1, 0),
          //    glm::normalize(triangle_normal));
          glm::vec3 pos = anim.data.positions[parent.mesh.indices[attc.triangle_index * 3]] *
                              attc.barycentric.z +
                          anim.data.positions[parent.mesh.indices[attc.triangle_index * 3 + 1]] *
                              attc.barycentric.y +
                          anim.data.positions[parent.mesh.indices[attc.triangle_index * 3 + 2]] *
                              attc.barycentric.x -
                          triangle_normal * attc.distance_from_plane;

          glm::vec3 tri_forward = glm::normalize(
              (anim.data.positions[parent.mesh.indices[attc.triangle_index * 3]] +
               (anim.data.positions[parent.mesh.indices[attc.triangle_index * 3 + 1]] -
                anim.data.positions[parent.mesh.indices[attc.triangle_index * 3]]) *
                   0.5f) -
              anim.data.positions[parent.mesh.indices[attc.triangle_index * 3 + 2]]);

          // Matrix for converting from triangle space
          glm::mat4 triangle_space;
          triangle_space[0] = glm::vec4(tri_forward, 0.0f);
          triangle_space[1] = glm::vec4(glm::normalize(triangle_normal), 0.0f);
          triangle_space[2] =
              glm::vec4(glm::cross(tri_forward, glm::normalize(triangle_normal)), 0.0f);
          triangle_space[3] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);

          // Rotate in triangle space, then move to model space
          glm::mat4 rotation = triangle_space * glm::toMat4(attc.rotation);

          tc.transform.set_model_matrix(
              anim_tc.transform.get_model_matrix() * glm::translate(pos) * rotation, context);
        }

      } else {
        attach.detach<AttachToTriangleComponent>();
      }
    }
  }
}

void AnimationSystem::update_aim_offset(const cor::FrameContext& context,
                                        const cor::EntityHandle handle,
                                        std::vector<bse::SQT>& quat_palette) const {
  cor::ConstEntity entity = context.entity_mgr.get_entity(handle);
  const PlayerComponent& pc = entity;
  const AimOffsetComponent& ao = entity;
  const ModelComponent& model = entity.get<ModelComponent>();
  const bse::Skeleton& skel = model.asset->get_mesh()->get_skeleton();
  int joint_id = skel.get_joint_map().at(ao.m_joint_name)->m_id;

  if (pc.is_attacking) {
    ao.m_blend_ratio = 0.0f;

    if (ao.m_blend_ratio < 1.0f) {
      ao.m_blend_ratio += ao.m_blend_aim_blend_speed * context.dt;
    }
    const float new_angle = glm::mix(ao.m_previous_angle, pc.aim_angle, ao.m_blend_ratio);

    ao.m_previous_angle = new_angle;

    glm::mat4 joint_rot_mat = glm::toMat4(quat_palette.at(joint_id).m_rotation);
    joint_rot_mat = glm::rotate(joint_rot_mat, new_angle, glm::vec3(0, 0, 1));
    quat_palette.at(joint_id).m_rotation = glm::toQuat(joint_rot_mat);
  } else {
    if (ao.m_blend_ratio < 1.0f) {
      ao.m_blend_ratio += ao.m_blend_return_speed * context.dt;

      float new_angle = glm::mix(ao.m_previous_angle, 0.f, quadratic_easing(ao.m_blend_ratio));

      glm::mat4 joint_rot_mat = glm::toMat4(quat_palette.at(joint_id).m_rotation);
      joint_rot_mat = glm::rotate(joint_rot_mat, new_angle, glm::vec3(0, 0, 1));
      quat_palette.at(joint_id).m_rotation = glm::toQuat(joint_rot_mat);
    } else {
      ao.m_previous_angle = 0.f;
    }
  }
}

float AnimationSystem::quadratic_easing(float t) const {
  if (t <= 0.5f) return 2.0f * glm::pow2(t);
  t -= 0.5f;
  return 2.0f * t * (1.0f - t) + 0.5f;
}

void AnimationSystem::update_look_at(cor::ConstEntity origin, bse::Joint& origin_joint,
                                     bse::SQT& joint_transform, cor::ConstEntity target) const {
  const TransformComponent& origin_tc = origin;
  const TransformComponent& target_tc = target;

  glm::vec3 target_vector =
      glm::normalize(target_tc.transform.get_position() - origin_tc.transform.get_position());
  float dot = glm::dot(origin_tc.transform.get_forward(), target_vector);

  if (dot > 0.f) {
    glm::vec3 target_position_js = glm::vec3(origin_joint.m_offset_matrix *
                                             glm::inverse(origin_tc.transform.get_model_matrix()) *
                                             (glm::vec4(target_tc.transform.get_position(), 1.0f)));

    joint_transform.m_rotation = glm::rotation({0, 1, 0}, glm::normalize(target_position_js));
  }
}

void AnimationSystem::skin_mesh(AnimatedMeshData& mesh) {
  // Get TaskScheduler pointer
  auto* ts = bse::TaskScheduler::get();

  // Get pointers to original mesh data as input
  const glm::vec3* positions = (glm::vec3*)&mesh.parent_mesh_data->mesh.positions[0];
  const glm::vec3* normals = (glm::vec3*)&mesh.parent_mesh_data->mesh.normals[0];

  // Lock collision mesh data. These values are not actually used, they just return pointers to
  // the data Bullet received when creating the mesh, which is already stored in 'mesh'
  unsigned char* vertexbase = nullptr;
  int numverts = 0;
  PHY_ScalarType type = PHY_FLOAT;
  int stride = 0;
  unsigned char* indexbase = nullptr;
  int indexstride = 0;
  int numfaces = 0;
  PHY_ScalarType indicestype = PHY_INTEGER;
  mesh.mesh_vertex_array->getLockedVertexIndexBase(&vertexbase, numverts, type, stride, &indexbase,
                                                   indexstride, numfaces, indicestype, 0);

  SkinningTask sst(positions, normals, 0, mesh.num_vertices, &mesh);
  sst.kernel();
}

}  // namespace gmp
