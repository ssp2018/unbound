#include "gmp/systems/special_effects_system.hpp"

#include "aud/sound_buffer.hpp"
#include "aud/sound_source.hpp"
#include "bse/asset.hpp"
#include "bse/directory_path.hpp"
#include "bse/file_path.hpp"
#include "cor/entity.tcc"
#include "cor/entity_manager.tcc"
#include "cor/event.hpp"
#include "cor/event_manager.hpp"
#include "cor/frame_context.hpp"
#include "cor/key.hpp"
#include "gfx/model.hpp"
#include "gfx/texture.hpp"
#include "gmp/ai_script_entity_wrapper.hpp"
#include "gmp/components/ai_component.hpp"
#include "gmp/components/ai_force_state_change_component.hpp"
#include "gmp/components/attach_to_entity_component.hpp"
#include "gmp/components/attach_to_joint_component.hpp"
#include "gmp/components/audio_component.hpp"
#include "gmp/components/character_controller_component.hpp"
#include "gmp/components/continue_movement_after_impact_component.hpp"
#include "gmp/components/damage_component.hpp"
#include "gmp/components/destructible_component.hpp"
#include "gmp/components/dynamic_component.hpp"
#include "gmp/components/explode_component.hpp"
#include "gmp/components/fracture_on_impact_component.hpp"
#include "gmp/components/health_component.hpp"
#include "gmp/components/homing_component.hpp"
#include "gmp/components/hook_component.hpp"
#include "gmp/components/hook_hit_component.hpp"
#include "gmp/components/lifetime_component.hpp"
#include "gmp/components/model_component.hpp"
#include "gmp/components/particles_component.hpp"
#include "gmp/components/physics_projectile_component.hpp"
#include "gmp/components/player_component.hpp"
#include "gmp/components/shield_effect_component.hpp"
#include "gmp/components/static_component.hpp"
#include "gmp/components/transform_component.hpp"
#include "phy/collision_shapes.hpp"
#include "scr/script.hpp"
#include <gmp/components/destroyed_component.hpp>
#include <gmp/components/emitter_death_countdown_component.hpp>
#include <gmp/components/faction_component.hpp>
#include <gmp/components/force_move_to_entity_component.hpp>
#include <gmp/components/force_move_to_position_component.hpp>
#include <gmp/components/name_component.hpp>
#include <gmp/gmp_helper_functions.hpp>
#include <gmp/systems/audio_system.hpp>

namespace gmp {
struct PlayerComponent;
}
namespace cor {
struct FrameContext;
}
namespace gmp {

gmp::SpecialEffectsSystem::SpecialEffectsSystem(cor::FrameContext& context) : m_context(context) {
  add_listener(context.event_mgr.register_handler<CollisionEvent>(
      [this, &context](const CollisionEvent* event) {
        if (m_context.entity_mgr.is_valid(event->entity_1) &&
            m_context.entity_mgr.is_valid(event->entity_2)) {
          for (int i = 0; i < 2; ++i) {
            cor::EntityHandle first_handle = (i == 0 ? event->entity_1 : event->entity_2);
            cor::EntityHandle second_handle = (i == 1 ? event->entity_1 : event->entity_2);
            cor::Entity first_entity = m_context.entity_mgr.get_entity(first_handle);
            cor::Entity second_entity = m_context.entity_mgr.get_entity(second_handle);

            if (first_entity.has<FractureOnImpactComponent>()) {
              // trigger fraction
              if (first_entity.has<PhysicsProjectileComponent>()) {
                auto& first_ppc = first_entity.get<PhysicsProjectileComponent>();
                if (validate_projectile_collision(first_entity, second_entity)) {
                  trigger_fracture(first_handle);
                  first_ppc.contents.set_has_hit(true);
                } else {
                  // LifetimeComponent lc;
                  // lc.time_left = 0.0f;
                  // second_entity.attach(lc);
                }
              } else {
                trigger_fracture(first_handle);
              }
            }

            // check for explosives
            if (first_entity.has<ExplodeComponent>() &&
                first_entity.get<ExplodeComponent>().explode_on_impact) {
              // Trigger explosion
              trigger_explosion(first_handle);
              if (second_entity.has<DestructibleComponent>()) {
                DestructibleComponent& dc = second_entity;
                dc.health_points -= first_entity.get<ExplodeComponent>().max_damage;
                if (dc.health_points <= 0) {
                  destroy_destructible_entity(second_handle);
                }
              }
            }
          }
        }
      }));
}

// update with read only
void gmp::SpecialEffectsSystem::read_update(const cor::FrameContext& context) {}

// update with write only
void gmp::SpecialEffectsSystem::write_update(cor::FrameContext& context) {
  // handle active emitters
  auto active_emitters = context.entity_mgr.get_entities(
      cor::Key::create<ParticlesComponent, EmitterDeathCountdownComponent>());
  for (auto& emitter : active_emitters) {
    auto entity = context.entity_mgr.get_entity(emitter);
    EmitterDeathCountdownComponent& edcc = entity;
    ParticlesComponent& pc = entity;

    edcc.death_countdown -= context.dt;

    if (edcc.death_countdown < 0 && !edcc.handled) {
      pc.spawn_per_second = 0;
      edcc.handled = true;
    }
    if (edcc.death_countdown < -0.5) context.entity_mgr.destroy_entity(emitter);
  }
}

void gmp::SpecialEffectsSystem::trigger_fracture(cor::EntityHandle handle) {
  cor::Entity entity = m_context.entity_mgr.get_entity(handle);

  TransformComponent& ent_tr = entity;
  FractureOnImpactComponent& ent_explode = entity;
  PhysicsProjectileComponent& ent_proj = entity;

  cor::EntityHandle emitter_handle = m_context.entity_mgr.create_entity();
  cor::Entity emitter_entity = m_context.entity_mgr.get_entity(emitter_handle);

  glm::vec3 vel = ent_proj.contents.get_velocity();

  ParticlesComponent emitter_component;
  emitter_component.max_particles = ent_explode.fragment_amount;
  emitter_component.spawn_per_second = 60 * ent_explode.fragment_amount;
  emitter_component.min_start_color = glm::vec4(1, 1, 1, 1);
  emitter_component.max_start_color = glm::vec4(1, 1, 1, 1);
  emitter_component.min_end_color = glm::vec4(1, 1, 1, 1);
  emitter_component.max_end_color = glm::vec4(1, 1, 1, 1);
  emitter_component.min_start_velocity = glm::vec3(vel.x - 1, vel.y - 1, vel.z - 1);
  emitter_component.max_start_velocity = glm::vec3(vel.x + 1, vel.y + 1, vel.z + 1);
  emitter_component.min_start_size = ent_explode.fragment_size - 0.5;
  emitter_component.max_start_size = ent_explode.fragment_size + 0.5;
  emitter_component.max_life = 1;
  emitter_component.min_spawn_pos =
      glm::vec3(-ent_explode.radius, -ent_explode.radius, -ent_explode.radius);
  emitter_component.max_spawn_pos =
      glm::vec3(ent_explode.radius, ent_explode.radius, ent_explode.radius);
  emitter_component.texture = ent_explode.texture;

  TransformComponent transform_component;
  transform_component.transform.set_position(ent_tr.transform.get_position(), m_context);

  LifetimeComponent life_component;
  life_component.time_left = 0.5;

  emitter_entity.attach(transform_component);
  emitter_entity.attach(emitter_component);
  emitter_entity.attach(life_component);

  // m_active_emitters.push_back({0.01f, emitter_handle, false});
  // entity.attach(SpecialProjectileKillComponent());
  DestroyedComponent destroy_component;
  entity.attach(destroy_component);
}

void gmp::SpecialEffectsSystem::trigger_explosion(cor::EntityHandle handle) {
  cor::Entity entity = m_context.entity_mgr.get_entity(handle);

  const TransformComponent& ent_tr = entity;
  const glm::vec3 ent_pos = ent_tr.transform.get_position();
  const ExplodeComponent& ent_explode = entity;

  create_particle_explosion(entity, m_context);

  // Destroy the entity that caused the explosion
  if (entity.has<NameComponent>() && entity.get<NameComponent>().name == "player_arrow")
    destroy_arrow(handle, m_context);
  else
    entity = DestroyedComponent();

  // Normal health
  auto vulnerables =
      m_context.entity_mgr.get_entities(cor::Key::create<HealthComponent, TransformComponent>());
  for (auto vulnerable : vulnerables) {
    cor::Entity entity = m_context.entity_mgr.get_entity(vulnerable);
    const TransformComponent& tc = entity;
    const float distance = glm::distance(tc.transform.get_position(), ent_pos);
    if (distance < ent_explode.damage_radius) {
      HealthComponent& hc = entity;

      int damage = glm::min(ceil(ent_explode.max_damage / distance), (float)ent_explode.max_damage);
      if (!entity.has<ShieldEffectComponent>()) {
        hc.health_points -= damage;

        if (hc.health_points > 0)
          m_context.event_mgr.send_event(DamageTaken({entity.get_handle()}));
      }
    }
  }

  // "Special"/destructable health
  auto destructibles = m_context.entity_mgr.get_entities(
      cor::Key::create<DestructibleComponent, TransformComponent>());
  for (auto destructible : destructibles) {
    cor::Entity target_entity = m_context.entity_mgr.get_entity(destructible);
    float radius = 0.0f;
    const TransformComponent& tc = target_entity;
    glm::vec3 pos = tc.transform.get_position();
    if (target_entity.has<PhysicsProjectileComponent>()) {
      auto& ppc = target_entity.get<PhysicsProjectileComponent>();
      auto aabb = ppc.contents.get_aabb();
      radius = std::max(std::max(aabb.size.x, aabb.size.y), aabb.size.z);
      pos = aabb.position;
    }
    // Check distance
    float distance = glm::distance(pos, ent_pos);
    distance = glm::max(distance - radius, 0.00001f);
    if (distance < ent_explode.damage_radius) {
      DestructibleComponent& dc = target_entity;
      dc.health_points -=
          glm::min(ceil(ent_explode.max_damage / distance), (float)ent_explode.max_damage);
      // Destroyed
      if (dc.health_points <= 0) {
        destroy_destructible_entity(destructible);
      }
      // Send away with force impulse
      else if (dc.health_points > 0 && target_entity.has(cor::Key::create<DynamicComponent>())) {
        DynamicComponent& dc = target_entity;
        dc.contents.apply_impulse(glm::normalize(tc.transform.get_position() - ent_pos) *
                                  (ent_explode.damage_radius / distance) * 10.f);
      }
    }
  }
}

void gmp::SpecialEffectsSystem::destroy_destructible_entity(cor::EntityHandle handle) {
  cor::Entity entity = m_context.entity_mgr.get_entity(handle);

  DestructibleComponent& dc = entity;

  if (!dc.death_callback.is_null()) {
    std::function<void(gmp::AIScriptEntityWrapper&, cor::Entity, cor::EventManager&,
                       cor::EntityManager&)>
        death_callback = dc.death_callback;

    AIScriptEntityWrapper e(&entity, m_context);

    death_callback(e, entity, m_context.event_mgr, m_context.entity_mgr);
  } else if (entity.has<NameComponent>() && entity.get<NameComponent>().name == "player_arrow") {
    destroy_arrow(handle, m_context);
  } else {
    entity = DestroyedComponent();
  }
}

}  // namespace gmp