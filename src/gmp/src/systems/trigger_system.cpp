#include "gmp/systems/trigger_system.hpp"

#include "bse/reflect.hpp"                              // for operator<<
#include "cor/entity.tcc"                               // for Entity
#include "cor/entity_manager.tcc"                       // for EntityManager
#include "cor/event.hpp"                                // for CollisionEvent
#include "cor/event_manager.hpp"                        // for EventManager
#include "cor/frame_context.hpp"                        // for FrameContext
#include "gmp/components/trigger_script_component.hpp"  // for TriggerScript...
#include "scr/object.hpp"                               // for Object
namespace cor {
struct EntityHandle;
}

namespace gmp {
//
TriggerSystem::TriggerSystem(cor::FrameContext& context) {
  //
  add_listener(
      context.event_mgr.register_handler<CollisionEvent>([&context](const CollisionEvent* event) {
        //
        cor::Entity first = context.entity_mgr.get_entity(event->entity_1);
        cor::Entity second = context.entity_mgr.get_entity(event->entity_2);
        if (first.is_valid() && second.is_valid()) {
          for (int i = 0; i < 2; i++) {
            if (first.has<TriggerScriptComponent>()) {
              TriggerScriptComponent& tsc = first;
              std::function<void(CollisionEvent, std::string, cor::EntityHandle, cor::EntityHandle,
                                 cor::EventManager&, cor::EntityManager&)>
                  on_touch_callback = tsc.on_touch_callback;
              on_touch_callback(*event, tsc.name, second.get_handle(), tsc.target_entity,
                                context.event_mgr, context.entity_mgr);
            }

            // Swap
            cor::Entity temp = first;
            first = second;
            second = temp;
          }
        }
      }));

  add_listener(context.event_mgr.register_handler<AnimationNotifyEvent>(
      [&context](const AnimationNotifyEvent* event) {
        //
        cor::Entity entity = context.entity_mgr.get_entity(event->handle);
        if (entity.is_valid()) {
          if (entity.has<TriggerScriptComponent>()) {
            TriggerScriptComponent& tsc = entity;
            std::function<void(std::string, cor::Entity&, cor::EventManager&, cor::EntityManager&,
                               cor::FrameContext&)>
                on_touch_callback = tsc.on_touch_callback;
            on_touch_callback(event->animation_name, entity, context.event_mgr, context.entity_mgr,
                              context);
          }
        }
      }));
}

TriggerSystem::~TriggerSystem() {
  //
}

void TriggerSystem::read_update(const cor::FrameContext& context) {
  //
}

void TriggerSystem::write_update(cor::FrameContext& context) {
  //
}

}  // namespace gmp
