#include "cor/entity.tcc"                          // for Entity
#include "cor/entity_handle.hpp"                   // for EntityHandle, hash
#include "cor/entity_manager.tcc"                  // for EntityManager
#include "cor/event.hpp"                           // for AddThreatEvent, Incre...
#include "cor/event_manager.hpp"                   // for EVENT_REGISTER_MEMBER...
#include "cor/frame_context.hpp"                   // for FrameContext
#include "cor/key.hpp"                             // for Key
#include <gmp/components/ai_component.hpp>         // for AIComponent, AICompon...
#include <gmp/components/threat_component.hpp>     // for ThreatComponent
#include <gmp/components/transform_component.hpp>  // for ThreatComponent
#include <gmp/systems/threat_system.hpp>           // for ThreatSystem
namespace gmp {
struct TransformComponent;
}
namespace gmp {

ThreatSystem::ThreatSystem(cor::FrameContext& context) : m_context{context} {
  add_listener(EVENT_REGISTER_MEMBER_FUNCTION_I(AddThreatEvent, context.event_mgr,
                                                add_threat_event_reciever));
}

void ThreatSystem::read_update(const cor::FrameContext& context) {}

void ThreatSystem::write_update(cor::FrameContext& context) {
  cor::Key key = cor::Key::create<ThreatComponent, AIComponent>();

  auto entities = context.entity_mgr.get_entities(key);
  for (auto& e_handle : entities) {
    cor::Entity e = context.entity_mgr.get_entity(e_handle);  // current AI
    ThreatComponent& t_c = e;
    TransformComponent& trans_c1 = e;
    AIComponent& ai_c = e;

    for (std::unordered_map<cor::EntityHandle, float>::iterator it = t_c.threats.begin();
         it != t_c.threats.end();) {
      // automatically decrease threat by a small amount
      if (context.entity_mgr.get_entity(it->first).is_valid()) {
        it->second -= m_threat_loss * context.dt;

        TransformComponent& trans_c2 = context.entity_mgr.get_entity(it->first);  // other entity

        glm::vec3 diff = trans_c1.transform.get_position() - trans_c2.transform.get_position();
        diff.z *= 0.3;  // reduce impact of z-distance differance
        float dist = glm::distance(deff);

        // remove if the entity poses no threat to the ai
        if (dist > ai_c.disengagement_range || it->second <= 0.f)
          it = t_c.threats.erase(it);
        else
          ++it;
      } else
        it = t_c.threats.erase(it);  // entity is not valid, remove it
    }
  }
}

void ThreatSystem::add_threat_event_reciever(const AddThreatEvent* event) {
  // Get the threat component of the entity who should have the other entity as a threat
  ThreatComponent& t_c = m_context.entity_mgr.get_entity(event->self);

  // Add the other entity as a threat to the entity with a given threat level
  t_c.threats[event->other] = event->threat_level;
}

void ThreatSystem::increase_threat_event_reciever(const IncreaseThreatEvent* event) {
  // Get the threat component of the entity who should have the treat of certain entity increased
  ThreatComponent& t_c = m_context.entity_mgr.get_entity(event->self);

  // Add the other entity as a threat to the entity with a given threat level
  t_c.threats[event->other] += event->threat_addition;
}

}  // namespace gmp