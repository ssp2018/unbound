#include "cor/entity_manager.hpp"  // for EntityManager
#include "cor/frame_context.hpp"   // for FrameContext
#include "cor/key.hpp"             // for Key
#include "gmp/components/character_controller_component.hpp"
#include "gmp/components/dynamic_component.hpp"
#include "gmp/components/player_component.hpp"
#include "gmp/components/projection_component.hpp"
#include "gmp/components/transform_component.hpp"
#include <cor/entity.tcc>               // for Entity
#include <gmp/systems/test_system.hpp>  // for TestSystem
namespace gmp {
struct DynamicComponent;
}
namespace gmp {
struct ProjectionComponent;
}
namespace gmp {
struct TransformComponent;
}

namespace gmp {
TestSystem::TestSystem(cor::FrameContext& context) {}

void TestSystem::read_update(const cor::FrameContext& context) {}
void TestSystem::write_update(cor::FrameContext& context) {
  {
    cor::Key key = cor::Key::create<PlayerComponent, TransformComponent>();
    cor::Key cam_key = cor::Key::create<ProjectionComponent, TransformComponent>();

    auto entities = context.entity_mgr.get_entities(key);
    auto cams = context.entity_mgr.get_entities(cam_key);
    for (auto& e_handle : entities) {
      cor::Entity e = context.entity_mgr.get_entity(e_handle);
      TransformComponent& ctc = context.entity_mgr.get_entity(cams[0]);

      TransformComponent& tc = e;
      CharacterControllerComponent& ccc = e;
      PlayerComponent& dc = e;
    }
  }
}
}  // namespace gmp