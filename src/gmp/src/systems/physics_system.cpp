#include "bse/result.hpp"           // for Result
#include "cor/entity_handle.hpp"    // for EntityH...
#include "cor/entity_manager.tcc"   // for EntityM...
#include "cor/event.hpp"            // for Collisi...
#include "cor/frame_context.hpp"    // for FrameCo...
#include "cor/key.hpp"              // for Key
#include "gfx/mesh.hpp"             // for AABB
#include "phy/physics_manager.hpp"  // for Physics...
#include <bse/edit.hpp>             // for BSE_EDIT
#include <cor/entity.tcc>           // for Entity
#include <cor/event_manager.hpp>    // for EventMa...
#include <gmp/components/animation_component.hpp>
#include <gmp/components/attach_to_triangle_component.hpp>
#include <gmp/components/character_controller_component.hpp>  // for Charact...
#include <gmp/components/dynamic_component.hpp>               // for Dynamic...
#include <gmp/components/lifetime_component.hpp>
#include <gmp/components/model_component.hpp>
#include <gmp/components/name_component.hpp>
#include <gmp/components/particles_component.hpp>
#include <gmp/components/physics_projectile_component.hpp>  // for Physics...
#include <gmp/components/pickup_component.hpp>
#include <gmp/components/player_component.hpp>
#include <gmp/components/projection_component.hpp>
#include <gmp/components/slow_motion_component.hpp>  // for SlowMot...
#include <gmp/components/static_component.hpp>       // for StaticC...
#include <gmp/components/transform_component.hpp>    // for Transform
#include <gmp/systems/physics_system.hpp>            // for Physics...
#include <phy/collision_shapes.hpp>                  // for vc

namespace gmp {
struct MoveToGroundComponent;
}

namespace gmp {

PhysicsSystem::PhysicsSystem(cor::FrameContext& context, float dt_multiplier)
    : m_pm(glm::vec3(0.0f, 0.0f, -9.82f)),
      m_debug_drawing_enabled(false),
      m_debug_drawer(this),
      m_dt_multiplier{dt_multiplier} {
  // set_debug_draw(true);

  // Set PhysicsManager to use for raycasting by other systems
  context.utilities.set_physics_manager(&m_pm);

  create_navmesh_shapes(context);
  m_pm.add_navmesh_triangle_shapes(m_navmesh_shapes);
};
PhysicsSystem::~PhysicsSystem() {}

void PhysicsSystem::read_update(const cor::FrameContext& context) {
  m_pm.swap_frame();

  BSE_EDIT(m_debug_drawing_enabled);
  set_debug_draw(m_debug_drawing_enabled);

  read_static(context);
  read_dynamic(context);
  read_character_controller(context);
  read_projectile(context);

  m_pm.simulate(context.dt * m_dt_multiplier);
}
void PhysicsSystem::write_update(cor::FrameContext& context) {
  // --------------------------------------
  // -------- Send debug draw data --------
  // --------------------------------------
  if (m_debug_drawing_enabled) {
    m_pm.debug_draw();
  }

  write_static(context);
  write_dynamic(context);
  write_character_controller(context);
  write_projectile(context);

  send_collision_events(context);

  // ----------------------------------------
  // -------- Send debug draw events --------
  // ----------------------------------------
  if (m_debug_drawing_enabled) {
    DebugDrawLineEvent ddl;
    for (size_t i = 0; i < m_debug_lines.size(); i += 3) {
      ddl.from = m_debug_lines[i];
      ddl.to = m_debug_lines[i + 1];
      ddl.color = m_debug_lines[i + 2];
      context.event_mgr.send_event(ddl);
    }

    DebugDrawPointEvent ddp;
    for (size_t i = 0; i < m_debug_points.size(); i += 2) {
      ddp.position = m_debug_points[i];
      ddp.color = m_debug_points[i + 1];
      context.event_mgr.send_event(ddp);
    }

    m_debug_lines.clear();
    m_debug_points.clear();
  }

  // -----------------------------------------------------
  // -------- Animated mesh/projectile collisions --------
  // -----------------------------------------------------

  cor::Key animation_key =
      cor::Key::create<AnimationComponent, TransformComponent, ModelComponent>();
  auto animated_entity_handles = context.entity_mgr.get_entities(animation_key);

  cor::Key projectile_key = cor::Key::create<PhysicsProjectileComponent, TransformComponent>();
  auto projectile_entity_handles = context.entity_mgr.get_entities(projectile_key);

  // For all animated meshes
  for (auto& animated_handle : animated_entity_handles) {
    auto animated = context.entity_mgr.get_entity(animated_handle);
    AnimationComponent& ac = animated;
    ModelComponent& mc = animated;
    TransformComponent& tc = animated;

    // Ignore this entity if AnimatedData has not yet been created
    if (!ac.data.positions) {
      continue;
    }

    // For all projectiles
    for (auto& projectile_handle : projectile_entity_handles) {
      if (!context.entity_mgr.is_valid(projectile_handle)) {
        continue;
      }

      auto projectile = context.entity_mgr.get_entity(projectile_handle);

      if (!projectile.has<PhysicsProjectileComponent>()) {
        continue;
      }

      PhysicsProjectileComponent& pc = projectile;
      TransformComponent& tcp = projectile;

      if (pc.contents.m_disable_reporting_time > 0.0f || !pc.stick_to_animated) continue;

      if (ac.data.task && *ac.data.task != bse::TaskHandle::NULL_HANDLE) {
        auto* ts = bse::TaskScheduler::get();
        ts->wait_on_task(*ac.data.task);
        *ac.data.task = bse::TaskHandle::NULL_HANDLE;
      }

      // Check collision
      phy::PhysicsManager::ProjectileTriangleCollision result =
          m_pm.projectile_animated_mesh_collision_test(
              projectile_handle.full_handle, ac.data.mesh_shape, tc.transform.get_model_matrix(),
              pc.character_group, ac.character_group);

      if (result.hit) {
        // Send collision event
        CollisionEvent ce;
        ce.entity_1 = animated_handle;
        ce.entity_2 = projectile_handle;
        ce.hit_point =
            tc.transform.get_model_matrix() *
            glm::vec4(
                ac.data.positions[mc.asset->get_model_data().mesh.indices[result.face_index * 3]],
                1.0f);
        ce.hit_point_normal = glm::vec3(0, 0, 1);

        if (!context.is_replay) {
          context.event_mgr.send_event(ce);
        }

        // Add AttatchToTriangleComponent
        if (pc.contents.m_disable_destruction_time <= 0.0f) {
          // Attach the projectile to the mesh
          AttachToTriangleComponent attc;
          attc.attached_to = animated.get_handle();
          attc.triangle_index = result.face_index;
          attc.barycentric = result.barycentric;
          attc.distance_from_plane = result.distance_from_plane;
          attc.rotation = result.rotation;
          projectile = attc;
          projectile.detach<PhysicsProjectileComponent>();
        }
      }
    }
  }
}

std::vector<cor::RayResult> PhysicsSystem::raycast(glm::vec3 origin, glm::vec3 direction,
                                                   float length) {
  btDiscreteDynamicsWorld::AllHitsRayResultCallback result =
      m_pm.raycast(origin, direction, length);

  std::vector<cor::RayResult> results_vector;

  if (result.m_collisionObjects.size() > 0) {
    results_vector.resize(result.m_collisionObjects.size());

    for (int i = 0; i < result.m_collisionObjects.size(); i++) {
      results_vector[i] =
          cor::RayResult(phy::vc(result.m_hitPointWorld[i]), phy::vc(result.m_hitNormalWorld[i]),
                         result.m_hitFractions[i], result.m_collisionObjects[i]->getUserIndex());
    }
  }

  return results_vector;
}

void PhysicsSystem::DebugDraw::drawLine(const btVector3& from, const btVector3& to,
                                        const btVector3& color) {
  m_system->m_debug_lines.push_back(phy::vc(from));
  m_system->m_debug_lines.push_back(phy::vc(to));
  m_system->m_debug_lines.push_back(phy::vc(color));
}
void PhysicsSystem::DebugDraw::drawContactPoint(const btVector3& PointOnB,
                                                const btVector3& normalOnB, btScalar distance,
                                                int lifeTime, const btVector3& color) {
  m_system->m_debug_points.push_back(phy::vc(PointOnB));
  m_system->m_debug_points.push_back(phy::vc(color));

  m_system->m_debug_lines.push_back(phy::vc(PointOnB));
  m_system->m_debug_lines.push_back(phy::vc(PointOnB + normalOnB));
  m_system->m_debug_lines.push_back(phy::vc(color));
}
void PhysicsSystem::DebugDraw::reportErrorWarning(const char* warningString) {}
void PhysicsSystem::DebugDraw::setDebugMode(int debugMode) {}
int PhysicsSystem::DebugDraw::getDebugMode() const {
  return DBG_DrawWireframe | DBG_DrawAabb | DBG_DrawContactPoints | DBG_EnableCCD |
         DBG_DrawConstraints | DBG_DrawConstraintLimits | DBG_FastWireframe | DBG_DrawNormals |
         DBG_DrawFrames;
}
void PhysicsSystem::DebugDraw::draw3dText(const btVector3& location, const char* textString) {}

void PhysicsSystem::set_debug_draw(bool draw) {
  m_debug_drawing_enabled = draw;
  m_pm.set_debug_drawer(/*draw ? */ &m_debug_drawer /* : nullptr*/);
}

void PhysicsSystem::activate_static_object(uint32_t id) { m_pm.activate_static_object(id); }

void PhysicsSystem::activate_dynamic_object(uint32_t id) { m_pm.activate_dynamic_object(id); }

void PhysicsSystem::activate_character_controller(uint32_t id) {
  m_pm.activate_character_controller(id);
}

void PhysicsSystem::send_collision_events(cor::FrameContext& context) {
  const std::vector<phy::PhysicsManager::Collision>& collisions = m_pm.get_new_collisions();

  for (auto c : collisions) {
    CollisionEvent ce;
    ce.hit_point = c.collision_point;
    ce.hit_point_normal = c.collision_normal;

    cor::EntityHandle ob1;
    ob1.full_handle = c.object1;
    if (!context.entity_mgr.is_valid(ob1)) continue;
    ce.entity_1 = ob1;

    cor::EntityHandle ob2;
    ob2.full_handle = c.object2;
    if (!context.entity_mgr.is_valid(ob2)) continue;
    ce.entity_2 = ob2;

    remove_component_if_projectile(context, ob1, ob2);

    if (!context.is_replay) {
      context.event_mgr.send_event(ce);
    }
  }

  m_pm.clear_collisions();
}

void PhysicsSystem::remove_component_if_projectile(cor::FrameContext& context,
                                                   cor::EntityHandle object,
                                                   cor::EntityHandle object2) {
  remove_projectile_if_not_trigger(context, object, object2);
  remove_projectile_if_not_trigger(context, object2, object);
  auto& mgr = context.entity_mgr;
  if (mgr.get_entity(object).has<NameComponent>() && mgr.get_entity(object2).has<NameComponent>()) {
    NameComponent& first = mgr.get_entity(object);
    NameComponent& second = mgr.get_entity(object2);
  }
}

void PhysicsSystem::remove_projectile_if_not_trigger(cor::FrameContext& context,
                                                     cor::EntityHandle projectile,
                                                     cor::EntityHandle trigger) {
  cor::Key static_key = cor::Key::create<StaticComponent>();

  cor::Entity p = context.entity_mgr.get_entity(projectile);
  cor::Entity t = context.entity_mgr.get_entity(trigger);

  if (t.has(static_key)) {
    StaticComponent& sc = t;

    if (!sc.contents.get_is_trigger()) {
      remove_projectile(context, p);
    }
  } else {
    remove_projectile(context, p);
  }
}

void PhysicsSystem::remove_projectile(cor::FrameContext& context, cor::Entity& projectile) {
  cor::Key projectile_key = cor::Key::create<PhysicsProjectileComponent>();

  if (projectile.has(projectile_key)) {
    PhysicsProjectileComponent& dc = projectile;

    if (dc.contents.m_disable_destruction_time <= 0.0f) {
      dc.contents.m_has_hit = true;
    }
  }
}

void PhysicsSystem::read_static(const cor::FrameContext& context) {
  cor::Key static_key = cor::Key::create<StaticComponent, TransformComponent>();
  m_dirty_transforms.clear();
  auto static_entities = context.entity_mgr.get_entities(static_key);
  for (auto& e : static_entities) {
    bool exists = m_pm.update_static_object(e.full_handle);
    const TransformComponent& tc = context.entity_mgr.get_entity(e);
    const StaticComponent& sc = context.entity_mgr.get_entity(e);

    if (sc.contents.m_shape == nullptr) {
      continue;
    }

    bool is_transform_dirty = false;
    if (!exists) {
      // Scale should not be set for static objects, as it is not overwritten by Bullet like in
      // dynamic objects
      glm::mat4 matrix_no_scale = glm::translate(glm::mat4(1.0f), tc.transform.get_position()) *
                                  glm::toMat4(tc.transform.get_rotation());

      add_static_object(e.full_handle, matrix_no_scale, tc.transform.get_scale_vector(),
                        sc.contents.m_shape, sc.contents.get_is_trigger());
      is_transform_dirty = true;
    }

    // If another system updated transform last frame, grab the new transform
    if (tc.transform.get_physics_dirty()) {
      // Scale should not be set for static objects, as it is not overwritten by Bullet like in
      // dynamic objects
      glm::mat4 matrix_no_scale = glm::translate(glm::mat4(1.0f), tc.transform.get_position()) *
                                  glm::toMat4(tc.transform.get_rotation());

      m_pm.set_static_transform(e.full_handle, matrix_no_scale, tc.transform.get_scale_vector());
      activate_static_object(e.full_handle);
      tc.transform.clean_physics();
      is_transform_dirty = true;
    }

    if (is_transform_dirty) {
      m_dirty_transforms.push_back(e);
    }

    // If another system has modified an attribute of the StaticComponent, send new info to
    // PhysicsManager
    if (sc.contents.is_dirty()) {
      m_pm.update_static_object_properties(e.full_handle, sc.contents.m_shape,
                                           sc.contents.m_is_trigger);
      sc.contents.set_dirty(false);
    }
  }

  m_pm.remove_stale_static_objects();
}

void PhysicsSystem::read_dynamic(const cor::FrameContext& context) {
  cor::Key dynamic_key = cor::Key::create<DynamicComponent, TransformComponent>();

  auto dynamic_entities = context.entity_mgr.get_entities(dynamic_key);
  for (auto& e : dynamic_entities) {
    bool exists = m_pm.update_dynamic_object(e.full_handle);
    const TransformComponent& tc = context.entity_mgr.get_entity(e);
    const DynamicComponent& dc = context.entity_mgr.get_entity(e);

    if (!exists) {
      add_dynamic_object(e.full_handle, tc.transform.get_model_matrix(), dc.contents.m_shape,
                         dc.contents.get_mass());
    }

    // If another system updated transform last frame, grab the new transform
    if (tc.transform.get_physics_dirty()) {
      m_pm.set_dynamic_transform(e.full_handle, tc.transform.get_model_matrix());
      activate_dynamic_object(e.full_handle);
      tc.transform.clean_physics();
    }

    // If another system has modified an attribute of the DynamicComponent, send new info to
    // PhysicsManager
    if (dc.contents.is_dirty()) {
      m_pm.update_dynamic_object_properties(e.full_handle, dc.contents.m_shape,
                                            dc.contents.get_mass());
      dc.contents.set_dirty(false);
    }
  }

  m_pm.remove_stale_dynamic_objects();
}
void PhysicsSystem::read_character_controller(const cor::FrameContext& context) {
  m_character_controllers_jumped.clear();

  cor::Key character_key = cor::Key::create<CharacterControllerComponent, TransformComponent>();

  auto character_entities = context.entity_mgr.get_entities(character_key);
  for (auto& e : character_entities) {
    bool exists = m_pm.update_character_controller(e.full_handle);
    const TransformComponent& tc = context.entity_mgr.get_entity(e);
    const CharacterControllerComponent& cc = context.entity_mgr.get_entity(e);

    if (!exists) {
      // Get translation of offset transform
      glm::mat4 offset = cc.contents.get_offset_transform();
      glm::vec3 pos_offset = {offset[3][0], offset[3][1], offset[3][2]};

      // Correct position to account for offset transform
      add_character_controller(e.full_handle,                             //
                               tc.transform.get_position() - pos_offset,  //
                               cc.contents.get_radius(),                  //
                               cc.contents.get_height(),                  //
                               cc.contents.get_turning_speed(),           //
                               cc.contents.get_facing_angle(),            //
                               cc.contents.get_face_moving_direction(),   //
                               cc.contents.get_mass(),                    //
                               cc.contents.get_gravity(),                 //
                               cc.contents.get_velocity(),                //
                               cc.contents.m_use_navmesh_collision,       //
                               cc.contents.get_is_flying(),               //
                               cc.contents.get_up(),                      //
                               cc.contents.m_desired_direction,           //
                               cc.contents.m_facing_direction,            //
                               cc.contents.on_ground(),                   //
                               cc.contents.m_is_swinging,                 //
                               cc.contents.m_swing_velocity,              //
                               cc.contents.m_swing_center);
    }
    // If another system updated transform last frame, grab the new transform
    if (tc.transform.get_physics_dirty()) {
      m_pm.set_character_controller_transform(
          e.full_handle,
          tc.transform.get_model_matrix() * glm::inverse(cc.contents.get_offset_transform()));
      // activate_character_controller(e.full_handle);
      tc.transform.clean_physics();
    }

    // If another system has modified an attribute of the CharacterControllerComponent, send new
    // info to PhysicsManager
    if (cc.contents.is_dirty()) {
      m_pm.update_character_controller_properties(
          e.full_handle, cc.contents.get_mass(), cc.contents.get_radius(), cc.contents.get_height(),
          cc.contents.get_gravity(), cc.contents.get_turning_speed(),
          cc.contents.get_facing_angle(), cc.contents.get_face_moving_direction(),
          cc.contents.m_use_navmesh_collision, cc.contents.get_is_flying(), cc.contents.get_up());

      cc.contents.set_dirty(false);
    }

    // activate_character_controller(e.full_handle);

    // Call the move function for the character controller
    float character_dt = context.dt * m_dt_multiplier;
    cor::ConstEntity character = context.entity_mgr.get_entity(e);
    if (character.has<SlowMotionComponent>() && context.speed_factor != 1.0f) {
      const SlowMotionComponent& smc = character;
      character_dt = smc.factor.get_converted_dt(context);
    }

    if (cc.contents.m_jump_strength != 0.0f) {
      m_pm.character_controller_jump(e.full_handle, cc.contents.m_jump_strength);
      m_character_controllers_jumped.push_back(e);
    }

    m_pm.character_controller_move(e.full_handle, cc.contents.m_move_vector, character_dt);
    cc.contents.m_move_vector = glm::vec3(0, 0, 0);
    cc.contents.m_desired_direction =
        m_pm.get_character_controller_desired_direction(e.full_handle);
    cc.contents.m_facing_direction = m_pm.get_character_controller_current_direction(e.full_handle);
  }
  m_pm.remove_stale_character_controllers();
}
void PhysicsSystem::read_projectile(const cor::FrameContext& context) {
  cor::Key projectile_key = cor::Key::create<PhysicsProjectileComponent, TransformComponent>();

  auto projectile_entities = context.entity_mgr.get_entities(projectile_key);
  for (auto& e : projectile_entities) {
    bool exists = m_pm.update_projectile(e.full_handle);
    const TransformComponent& tc = context.entity_mgr.get_entity(e);
    const PhysicsProjectileComponent& pp = context.entity_mgr.get_entity(e);

    if (!exists) {
      add_projectile(e.full_handle, tc.transform.get_position(), pp.contents.get_radius(),
                     pp.contents.get_length(), pp.contents.get_velocity(),
                     pp.contents.get_gravity(), pp.contents.get_mass());
    }

    // If another system updated transform last frame, grab the new transform
    if (tc.transform.get_physics_dirty()) {
      m_pm.set_projectile_position(e.full_handle, tc.transform.get_position());
      tc.transform.clean_physics();
    }

    // If another system has modified an attribute of the PhysicsProjectileComponent, send new
    // info to PhysicsManager
    if (pp.contents.is_dirty()) {
      m_pm.update_projectile_properties(e.full_handle, pp.contents.get_radius(),
                                        pp.contents.get_length(), pp.contents.get_velocity(),
                                        pp.contents.get_gravity(), pp.contents.get_mass());

      pp.contents.set_dirty(false);
    }

    if (pp.contents.m_disable_reporting_time > 0.0f) {
      pp.contents.m_disable_reporting_time -= context.dt * m_dt_multiplier;
    }
    if (pp.contents.m_disable_destruction_time > 0.0f) {
      pp.contents.m_disable_destruction_time -= context.dt * m_dt_multiplier;
    }
    m_pm.projectile_set_collision_enabled(
        e.full_handle, pp.contents.m_disable_reporting_time <= 0.0f,
        pp.contents.m_disable_destruction_time <= 0.0f, pp.stick_to_animated);

    // Call the simulate function for the projectile
    float projectile_dt = context.dt * m_dt_multiplier;
    cor::ConstEntity projectile = context.entity_mgr.get_entity(e);

    if (projectile.has<SlowMotionComponent>() && context.speed_factor != 1.0f) {
      const SlowMotionComponent& smc = projectile;
      projectile_dt = smc.factor.get_converted_dt(context);
    }

    m_pm.projectile_simulate(e.full_handle, projectile_dt);
  }

  m_pm.remove_stale_projectiles();
}
void PhysicsSystem::write_static(cor::FrameContext& context) {
  cor::Key static_key = cor::Key::create<StaticComponent, TransformComponent>();

  auto static_entities = context.entity_mgr.get_entities(static_key);

  for (auto& e : static_entities) {
    cor::Entity ent = context.entity_mgr.get_entity(e);
    TransformComponent& tc = ent;
    StaticComponent& sc = ent;

    if (sc.contents.m_shape == nullptr) {
      continue;
    }

    // Update the object. This marks it as existing this frame, and add it id it does not exist
    if (!m_pm.update_static_object(e.full_handle)) {
      // Scale should not be set for static objects, as it is not overwritten by Bullet like in
      // dynamic objects
      glm::mat4 matrix_no_scale = glm::translate(glm::mat4(1.0f), tc.transform.get_position()) *
                                  glm::toMat4(tc.transform.get_rotation());

      add_static_object(e.full_handle, matrix_no_scale, tc.transform.get_scale_vector(),
                        sc.contents.m_shape, sc.contents.get_is_trigger());

      // Update AABB
      phy::PhysicsManager::AABB aabb = m_pm.get_static_object_aabb(e.full_handle);

      sc.contents.m_aabb.position = (aabb.min + aabb.max) * 0.5f;
      sc.contents.m_aabb.size =
          glm::vec3((aabb.max.x - aabb.min.x) * 0.5f, (aabb.max.y - aabb.min.y) * 0.5f,
                    (aabb.max.z - aabb.min.z) * 0.5f);
    }
    if (tc.transform.get_physics_dirty()) {
      glm::mat4 matrix_no_scale = glm::translate(glm::mat4(1.0f), tc.transform.get_position()) *
                                  glm::toMat4(tc.transform.get_rotation());
      m_pm.set_static_transform(e.full_handle, matrix_no_scale, tc.transform.get_scale_vector());
      tc.transform.clean_physics();
    }
  }

  for (const cor::EntityHandle& handle : m_dirty_transforms) {
    cor::Entity entity = context.entity_mgr.get_entity(handle);
    if (!entity.is_valid() || !entity.has<StaticComponent>()) {
      continue;
    }
    StaticComponent& sc = entity;
    phy::PhysicsManager::AABB aabb = m_pm.get_static_object_aabb(handle.full_handle);

    sc.contents.m_aabb.position = (aabb.min + aabb.max) * 0.5f;
    sc.contents.m_aabb.size =
        glm::vec3((aabb.max.x - aabb.min.x) * 0.5f, (aabb.max.y - aabb.min.y) * 0.5f,
                  (aabb.max.z - aabb.min.z) * 0.5f);
  }
}

void PhysicsSystem::write_dynamic(cor::FrameContext& context) {
  cor::Key dynamic_key = cor::Key::create<DynamicComponent, TransformComponent>();

  auto dynamic_entities = context.entity_mgr.get_entities(dynamic_key);

  for (auto& e : dynamic_entities) {
    TransformComponent& tc = context.entity_mgr.get_entity(e);
    DynamicComponent& dc = context.entity_mgr.get_entity(e);

    if (!m_pm.update_dynamic_object(e.full_handle)) {
      add_dynamic_object(e.full_handle, tc.transform.get_model_matrix(), dc.contents.m_shape,
                         dc.contents.get_mass());
    }

    // Apply pending forces and impulses
    if (dc.contents.m_force_to_apply != glm::vec3(0, 0, 0)) {
      m_pm.dynamic_object_add_force(e.full_handle, dc.contents.m_force_to_apply);
      dc.contents.m_force_to_apply = glm::vec3(0, 0, 0);
    }
    if (dc.contents.m_impulse_to_apply != glm::vec3(0, 0, 0)) {
      m_pm.dynamic_object_add_impulse(e.full_handle, dc.contents.m_impulse_to_apply);
      dc.contents.m_impulse_to_apply = glm::vec3(0, 0, 0);
    }

    bse::Result<glm::mat4> tr = m_pm.get_dynamic_transform(e.full_handle);

    if (tr && !tc.transform.get_physics_dirty()) {
      tc.transform.set_model_matrix_no_update(
          (*tr) * dc.contents.get_offset_transform() *
              glm::scale(glm::mat4(1.0f), m_pm.get_dynamic_scale(e.full_handle)),
          context);
    }

    // Set velocity
    dc.contents.m_velocity = m_pm.get_dynamic_object_velocity(e.full_handle);

    // Update AABB
    phy::PhysicsManager::AABB aabb = m_pm.get_dynamic_object_aabb(e.full_handle);

    dc.contents.m_aabb.position = (aabb.min + aabb.max) * 0.5f;
    dc.contents.m_aabb.size =
        glm::vec3((aabb.max.x - aabb.min.x) * 0.5f, (aabb.max.y - aabb.min.y) * 0.5f,
                  (aabb.max.z - aabb.min.z) * 0.5f);
  }
}
void PhysicsSystem::write_character_controller(cor::FrameContext& context) {
  cor::Key character_key = cor::Key::create<CharacterControllerComponent, TransformComponent>();

  auto character_entities = context.entity_mgr.get_entities(character_key);

  for (auto& e : character_entities) {
    TransformComponent& tc = context.entity_mgr.get_entity(e);
    CharacterControllerComponent& cc = context.entity_mgr.get_entity(e);

    bool added_this_frame = false;

    if (!m_pm.update_character_controller(e.full_handle)) {
      // Get translation of offset transform
      glm::mat4 offset = cc.contents.get_offset_transform();
      glm::vec3 pos_offset = {offset[3][0], offset[3][1], offset[3][2]};

      added_this_frame = true;

      // Correct position to account for offset transform
      add_character_controller(e.full_handle,                             //
                               tc.transform.get_position() - pos_offset,  //
                               cc.contents.get_radius(),                  //
                               cc.contents.get_height(),                  //
                               cc.contents.get_turning_speed(),           //
                               cc.contents.get_facing_angle(),            //
                               cc.contents.get_face_moving_direction(),   //
                               cc.contents.get_mass(),                    //
                               cc.contents.get_gravity(),                 //
                               cc.contents.get_velocity(),                //
                               cc.contents.m_use_navmesh_collision,       //
                               cc.contents.get_is_flying(),               //
                               cc.contents.get_up(),                      //
                               cc.contents.m_desired_direction,           //
                               cc.contents.m_facing_direction,            //
                               cc.contents.on_ground(),                   //
                               cc.contents.m_is_swinging,                 //
                               cc.contents.m_swing_velocity,              //
                               cc.contents.m_swing_center);
    }

    bse::Result<glm::mat4> tr = m_pm.get_character_controller_transform(e.full_handle);

    if (tr && !tc.transform.get_physics_dirty()) {
      // If added this frame, ignore matrix update because it messes up rotation for a frame
      if (tr && !added_this_frame) {
        tc.transform.set_model_matrix_no_update((*tr) * cc.contents.get_offset_transform(),
                                                context);
        tc.transform.clean_physics();
      }

      if (cc.contents.m_dash_vector != glm::vec3(0, 0, 0)) {
        m_pm.character_controller_dash(e.full_handle, cc.contents.m_dash_vector,
                                       cc.contents.m_dash_duration);

        cc.contents.m_dash_vector = glm::vec3(0, 0, 0);
        cc.contents.m_dash_duration = 0.0f;
      }

      if (cc.contents.m_jump_strength != 0.0f &&
          std::find(m_character_controllers_jumped.begin(), m_character_controllers_jumped.end(),
                    e) == m_character_controllers_jumped.end()) {
        m_pm.character_controller_jump(e.full_handle, cc.contents.m_jump_strength);
        cc.contents.m_jump_strength = 0.0f;
      }

      cc.contents.m_velocity = m_pm.get_character_controller_velocity(e.full_handle);

      cc.contents.m_on_ground = m_pm.get_character_controller_on_ground(e.full_handle);

      // Sync swing-related variables
      if (cc.contents.m_end_swing) {
        m_pm.character_controller_end_swing(e.full_handle);
        cc.contents.m_end_swing = false;
      }
      if (cc.contents.m_start_swing) {
        m_pm.character_controller_start_swing(e.full_handle, cc.contents.m_swing_center);
        cc.contents.m_start_swing = false;
      }
      if (cc.contents.m_set_rope_length != 0.0f) {
        m_pm.character_controller_set_rope_length(e.full_handle, cc.contents.m_set_rope_length);
        cc.contents.m_set_rope_length = 0.0f;
      }
      cc.contents.m_rope_length = m_pm.character_controller_get_rope_length(e.full_handle);
      if (cc.contents.m_swing_velocity != glm::vec3(0.0f, 0.0f, 0.0f)) {
        m_pm.character_controller_add_swing_velocity(e.full_handle, cc.contents.m_swing_velocity);
        cc.contents.m_swing_velocity = glm::vec3(0.0f, 0.0f, 0.0f);
      }

      cc.contents.m_is_swinging = m_pm.character_controller_get_is_swinging(e.full_handle);
    }

    // Update AABB
    phy::PhysicsManager::AABB aabb = m_pm.get_character_controller_aabb(e.full_handle);

    cc.contents.m_aabb.position = (aabb.min + aabb.max) * 0.5f;
    cc.contents.m_aabb.size =
        glm::vec3((aabb.max.x - aabb.min.x) * 0.5f, (aabb.max.y - aabb.min.y) * 0.5f,
                  (aabb.max.z - aabb.min.z) * 0.5f);
  }

  // Remove jump strength from characters who jumped in read_update
  for (auto& e : m_character_controllers_jumped) {
    if (context.entity_mgr.is_valid(e)) {
      cor::Entity entity = context.entity_mgr.get_entity(e);

      if (entity.has<CharacterControllerComponent>())
        entity.get<CharacterControllerComponent>().contents.m_jump_strength = 0.0f;
    }
  }
}
void PhysicsSystem::write_projectile(cor::FrameContext& context) {
  cor::Key projectile_key = cor::Key::create<PhysicsProjectileComponent, TransformComponent>();

  auto projectile_entities = context.entity_mgr.get_entities(projectile_key);

  for (auto& e : projectile_entities) {
    TransformComponent& tc = context.entity_mgr.get_entity(e);
    PhysicsProjectileComponent& pp = context.entity_mgr.get_entity(e);

    // If projectile hit an object last frame, remove it
    if (pp.contents.m_has_hit) {
      cor::Entity entity = context.entity_mgr.get_entity(e);

      entity.detach<PhysicsProjectileComponent>();
      break;
    }

    if (!m_pm.update_projectile(e.full_handle)) {
      add_projectile(e.full_handle, tc.transform.get_position(), pp.contents.get_radius(),
                     pp.contents.get_length(), pp.contents.get_velocity(),
                     pp.contents.get_gravity(), pp.contents.get_mass());
    }

    bse::Result<glm::mat4> tr = m_pm.get_projectile_transform(e.full_handle);

    if (tr) {
      tc.transform.set_model_matrix_no_update((*tr) * pp.contents.get_offset_transform(), context);
    }
    if (!pp.contents.is_dirty()) {
      pp.contents.m_velocity = m_pm.get_projectile_velocity(e.full_handle);
    }

    // Update AABB
    phy::PhysicsManager::AABB aabb = m_pm.get_projectile_aabb(e.full_handle);

    pp.contents.m_aabb.position = (aabb.min + aabb.max) * 0.5f;
    pp.contents.m_aabb.size =
        glm::vec3((aabb.max.x - aabb.min.x) * 0.5f, (aabb.max.y - aabb.min.y) * 0.5f,
                  (aabb.max.z - aabb.min.z) * 0.5f);
  }
}

void PhysicsSystem::add_static_object(uint32_t id, glm::mat4 transform_no_scale, glm::vec3 scale,
                                      std::shared_ptr<phy::CollisionShape> shape, bool is_trigger) {
  m_pm.add_static_object(id, transform_no_scale, scale, shape, is_trigger);
}

void PhysicsSystem::add_dynamic_object(uint32_t id, glm::mat4 transform,
                                       std::shared_ptr<phy::CollisionShape> shape, float mass) {
  m_pm.add_dynamic_object(id, transform, shape, mass);
}

void PhysicsSystem::add_character_controller(uint32_t id,                  //
                                             glm::vec3 position,           //
                                             float radius,                 //
                                             float height,                 //
                                             float turning_speed,          //
                                             float angle,                  //
                                             bool face_moving_direction,   //
                                             float mass,                   //
                                             glm::vec3 gravity,            //
                                             glm::vec3 velocity,           //
                                             bool navmesh_collision,       //
                                             bool is_flying,               //
                                             glm::vec3 up,                 //
                                             glm::quat desired_direction,  //
                                             glm::quat facing_direction,   //
                                             bool on_ground,               //
                                             bool swinging,                //
                                             glm::vec3 swing_velocity,     //
                                             glm::vec3 swing_center) {
  m_pm.add_character_controller(id,                     //
                                position,               //
                                radius,                 //
                                height,                 //
                                turning_speed,          //
                                angle,                  //
                                face_moving_direction,  //
                                mass,                   //
                                gravity,                //
                                velocity,               //
                                navmesh_collision,      //
                                is_flying,              //
                                up,                     //
                                desired_direction,      //
                                facing_direction,       //
                                on_ground);

  if (swinging) {
    m_pm.character_controller_start_swing(id, swing_center);
    m_pm.character_controller_add_swing_velocity(id, velocity);
  }
}

void PhysicsSystem::add_projectile(uint32_t id, glm::vec3 position, float radius, float length,
                                   glm::vec3 velocity, glm::vec3 gravity, float mass) {
  m_pm.add_projectile(id, position, radius, length, velocity, gravity, mass);
}

void PhysicsSystem::create_navmesh_shapes(const cor::FrameContext& context) {
  // Clear vectors to reset shapes
  m_navmesh_shapes.clear();
  m_navmesh_mesh_datas.clear();

  for (auto& [name, navmesh] : context.navmeshes) {
    // This object is used by Bullet to refer to the mesh
    btIndexedMesh mesh_ref;
    mesh_ref.m_numTriangles = navmesh.m_triangles.size();
    mesh_ref.m_numVertices = navmesh.m_points.size();
    mesh_ref.m_triangleIndexBase =
        reinterpret_cast<const unsigned char*>(&navmesh.m_triangles[0].point_indices);
    mesh_ref.m_triangleIndexStride = sizeof(ai::NavTriangle);
    mesh_ref.m_vertexBase = reinterpret_cast<const unsigned char*>(navmesh.m_points.data());
    mesh_ref.m_vertexStride = sizeof(glm::vec3);

    std::unique_ptr<btTriangleIndexVertexArray> vertex_array =
        std::make_unique<btTriangleIndexVertexArray>();
    vertex_array->addIndexedMesh(mesh_ref);

    std::unique_ptr<btBvhTriangleMeshShape> mesh_shape =
        std::make_unique<btBvhTriangleMeshShape>(vertex_array.get(), true);

    m_navmesh_mesh_datas.push_back(std::move(vertex_array));
    m_navmesh_shapes.push_back(std::move(mesh_shape));
  }
}

}  // namespace gmp