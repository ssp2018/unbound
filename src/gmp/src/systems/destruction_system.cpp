#include "gmp/systems/destruction_system.hpp"

#include "cor/entity.tcc"                          // for Entity
#include "cor/entity_manager.tcc"                  // for EntityManager
#include "cor/key.hpp"                             // for Key
#include "gmp/components/destroyed_component.hpp"  // for DestroyedComponent
#include "gmp/components/lifetime_component.hpp"   // for LifetimeComponent
#include "gmp/gmp_helper_functions.hpp"
#include <cor/frame_context.hpp>  // for FrameContext
#include <gmp/components/audio_component.hpp>
#include <gmp/components/name_component.hpp>

namespace gmp {

void DestructionSystem::read_update(const cor::FrameContext& context) {}

void DestructionSystem::write_update(cor::FrameContext& context) {
  // Decrease lifetime on lifetime components
  {
    cor::Key key_lifetime = cor::Key::create<LifetimeComponent>();
    auto entity_handles = context.entity_mgr.get_entities(key_lifetime);
    for (auto& entity_handle : entity_handles) {
      auto entity = context.entity_mgr.get_entity(entity_handle);
      LifetimeComponent& ltc = entity;
      ltc.time_left -= context.dt;

      // If no time left, set to be destroyed next frame
      if (ltc.time_left < 0.f) entity = DestroyedComponent();
    }
  }

  // Destroy entities that have been marked with DestroyedComponent
  cor::Key key_destroyed = cor::Key::create<DestroyedComponent>();
  auto entity_handles = context.entity_mgr.get_entities(key_destroyed);
  for (auto& entity_handle : entity_handles) {
    auto entity = context.entity_mgr.get_entity(entity_handle);

    // Since the destructors are not called for the audio component audio will still play
    // after a entity has been "deleted", hence we need to remove all the looping sounds and let the
    // remaining non looping sounds finish playing.
    if (entity.has<AudioComponent>()) {
      // Create a new placeholder entity
      cor::EntityHandle new_entity_h = context.entity_mgr.create_entity();
      cor::Entity new_entity = context.entity_mgr.get_entity(new_entity_h);

      // Move the audio component to the new entity
      {
        AudioComponent auc;
        auc.audio = std::move(entity.get<AudioComponent>().audio);
        new_entity = std::move(auc);
      }
      // Calculate how long the entity should exist
      LifetimeComponent ltc;
      ltc.time_left = 0;

      AudioComponent& auc = new_entity;
      for (auto it_source = auc.audio.sources.begin(); it_source != auc.audio.sources.end();) {
        if ((*it_source)->is_playing()) {
          // If the source is looping, stop it
          if ((*it_source)->get_looping()) {
            (*it_source)->play(false);
            it_source = auc.audio.sources.erase(it_source);
          } else {
            // If the source is not looping, check if the lifetime component needs to be updated to
            // let the longest sound finish playing
            ltc.time_left = std::max((*it_source)->get_time_left(), ltc.time_left);
            it_source++;
          }
        }
      }
    }
    if (entity.has<NameComponent>() && entity.get<NameComponent>().name == "player_arrow")
      destroy_arrow(entity_handle, context);
    else
      entity.destroy();
  }
}

}  // namespace gmp