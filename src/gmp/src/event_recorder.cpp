#include "gmp/event_recorder.hpp"

#include "cor/entity.hpp"
#include "cor/event_variant.hpp"
#include <gmp/components/ai_component.hpp>
#include <gmp/components/frame_tag_component.hpp>
#include <gmp/components/physics_projectile_component.hpp>
#include <gmp/components/player_component.hpp>
#include <gmp/components/projection_component.hpp>
#include <gmp/components/transform_component.hpp>
#include <gmp/systems/ai_system.hpp>
#include <gmp/systems/graphics_system.hpp>
#include <gmp/systems/particle_system.hpp>
#include <gmp/systems/animation_system.hpp>
#include <gmp/systems/physics_system.hpp>
#include <gmp/systems/player_system.hpp>
#include <gmp/systems/replay_system.hpp>

template class std::unique_ptr<std::vector<Event>>;

namespace gmp {
//

template <typename T>
void EventRecorder::add_listener_to_event(cor::FrameContext& context) {
  //
  m_lids.push_back(context.event_mgr.register_handler<T>([this](const T* event) {
    //
    if (m_recording_state == RecorderState::RECORDING) {
      m_recording_head.frame->events->push_back({*event});
    }
  }));
}

EventRecorder::EventRecorder(cor::FrameContext& context) {
  //
  // m_idle_frame.events = std::make_unique<std::vector<Event>>();

  add_listener_to_event<MoveEvent>(context);
  add_listener_to_event<MouseMovementEvent>(context);
  add_listener_to_event<StartAimEvent>(context);
  add_listener_to_event<StopAimEvent>(context);
  add_listener_to_event<StartDrawEvent>(context);
  add_listener_to_event<StopDrawEvent>(context);
  add_listener_to_event<JumpEvent>(context);
  add_listener_to_event<DashEvent>(context);
  add_listener_to_event<CancelActionEvent>(context);
  add_listener_to_event<SlowMotionEvent>(context);
  add_listener_to_event<RopeEvent>(context);
  add_listener_to_event<NormalArrowModeEvent>(context);
  add_listener_to_event<ExplosiveArrowModeEvent>(context);
  add_listener_to_event<ToggleDebugCameraMovementEvent>(context);
  add_listener_to_event<CollisionEvent>(context);
  add_listener_to_event<EnableSystem>(context);
  add_listener_to_event<DisableSystem>(context);
  add_listener_to_event<ToggleSystem>(context);

  m_recording_head = {
      std::begin(m_segments),                     //
      std::begin(std::begin(m_segments)->frames)  //
  };
  m_playback_head = m_recording_head;

  for (auto& segment : m_segments) {
    // segment.end_frame = std::begin(segment.frames);
    for (auto& frame : segment.frames) {
      frame.events = std::make_unique<std::vector<Event>>();
      frame.dt = 0.f;
      frame.speed_factor = 1.f;
      frame.unslowed_dt = frame.dt;
      frame.frame_num = 0xFFFF;
    }
  }
}

float EventRecorder::update(cor::FrameContext& context, cor::SystemManager& system_mgr, float dt,
                            float factor) {
  //
  dt = std::min(dt, 1.f / 20.f);

  if (m_playback_just_finished) {
    restore_game_state(context, system_mgr, m_end_playback_restoration_point);
    system_mgr.set_active_fingerprint(m_system_fingerprint_restoration);

    m_playback_just_finished = false;
    std::cout << "RESTORE \n";
  }

  switch (m_recording_state) {
    //
    // case RecorderState::IDLE: {
    //   m_idle_frame.events->clear();
    //   m_idle_frame.dt = dt;
    //   m_idle_frame.speed_factor = context.speed_factor;
    //   m_idle_frame.unslowed_dt = context.unslowed_dt;
    //   m_idle_frame.frame_num = context.frame_num;
    //   return dt;
    // } break;

    //
    case RecorderState::RECORDING: {
      FrameData& new_frame = *m_recording_head.frame;

      if (m_recording_head.frame == std::begin(m_recording_head.segment->frames)) {
        m_recording_head.segment->restoration_point =
            std::move(save_game_state(context, system_mgr));

        m_recording_length -= m_recording_head.segment->recording_length;
        m_recording_head.segment->recording_length = 0.f;
        // std::cout << "New Segment\n";
      }

      context.is_replay = false;

      // Keeps track of the current length of the recording.
      m_recording_length += dt;
      m_recording_head.segment->recording_length += dt;

      // std::cout << m_recording_length << "\n";

      new_frame.dt = dt;
      new_frame.speed_factor = context.speed_factor;
      new_frame.unslowed_dt = context.unslowed_dt;
      new_frame.frame_num = context.frame_num;

      {
        auto entity_handles = context.entity_mgr.get_entities(
            cor::Key::create<TransformComponent, PlayerComponent>());
        for (auto& entity_handle : entity_handles) {
          TransformComponent& tc = context.entity_mgr.get_entity(entity_handle);
          // if (tc.transform.get_frame_updated() >= new_frame.frame_num - 1) {
          new_frame.transform_map[entity_handle] = tc.transform;
          // }
        }
      }

      {
        auto entity_handles = context.entity_mgr.get_entities(
            cor::Key::create<TransformComponent, ProjectionComponent>());
        for (auto& entity_handle : entity_handles) {
          TransformComponent& tc = context.entity_mgr.get_entity(entity_handle);
          // if (tc.transform.get_frame_updated() == new_frame.frame_num - 1) {
          new_frame.transform_map[entity_handle] = tc.transform;
          // }
        }
      }
      {
        auto entity_handles =
            context.entity_mgr.get_entities(cor::Key::create<TransformComponent, AIComponent>());
        for (auto& entity_handle : entity_handles) {
          TransformComponent& tc = context.entity_mgr.get_entity(entity_handle);
          // if (tc.transform.get_frame_updated() == new_frame.frame_num - 1) {
          new_frame.transform_map[entity_handle] = tc.transform;
          // }
        }
      }
      {
        auto entity_handles = context.entity_mgr.get_entities(
            cor::Key::create<TransformComponent, FrameTagComponent>());
        for (auto& entity_handle : entity_handles) {
          TransformComponent& tc = context.entity_mgr.get_entity(entity_handle);
          // if (tc.transform.get_frame_updated() == new_frame.frame_num - 1) {
          new_frame.transform_map[entity_handle] = tc.transform;
          // }
        }
      }
      {
        auto entity_handles = context.entity_mgr.get_entities(
            cor::Key::create<TransformComponent, PhysicsProjectileComponent>());
        for (auto& entity_handle : entity_handles) {
          TransformComponent& tc = context.entity_mgr.get_entity(entity_handle);
          // if (tc.transform.get_frame_updated() == new_frame.frame_num - 1) {
          new_frame.transform_map[entity_handle] = tc.transform;
          // }
        }
      }

      // std::cout << "dt: " << dt << ",\tframe_num: " << new_frame.frame_num << ",\t";
      // auto recorded_events = context.event_mgr.get_event_indices();
      // for (auto& idx : recorded_events) {
      //   std::cout << " " << idx;
      // }
      // std::cout << "\n";

      m_post_playback_time = POST_PLAYBACK_DELAY_TIME;

      step_recording_head();
      return dt;
    }

    //
    case RecorderState::PLAYING: {
      FrameData& current_frame = *m_playback_head.frame;

      context.is_replay = true;

      if (m_playback_just_started) {
        restore_game_state(context, system_mgr, m_playback_head.segment->restoration_point);
        // system_mgr.disable_system(system_mgr.get_name<PhysicsSystem>());aaaa
        // system_mgr.disable_system(system_mgr.get_name<PhysicsSystem>());
        m_playback_just_started = false;
        context.replay_time_left = m_recording_length;
        // }
        // if (m_playback_head.frame == std::begin(m_playback_head.segment->frames)) {

        // Restore saved context parameters only for the first frame in a segment
        // restore_game_state(context, system_mgr, m_playback_head.segment->restoration_point);

        context.speed_factor = current_frame.speed_factor;
        context.unslowed_dt = current_frame.unslowed_dt;
        context.frame_num = current_frame.frame_num;

        context.event_mgr.send_event(ReplayStarted{});
      }
      context.frame_num = current_frame.frame_num;

      // std::cout << "dt: " << recorded_dt << "\t\tframe_num: " << context.frame_num << ",\t";
      // auto recorded_events = context.event_mgr.get_event_indices();
      // for (auto& idx : recorded_events) {
      //   std::cout << " " << idx;
      // }
      // std::cout << "\n";

      m_accumulated_playback_time += dt * factor;
      if (m_accumulated_playback_time >= current_frame.dt) {
        float recorded_dt = current_frame.dt;

        system_mgr.set_active_fingerprint(m_system_fingerprint_continuous);
        system_mgr.disable_system(context.system_id_proxy.get_name<PhysicsSystem>());
        system_mgr.disable_system(context.system_id_proxy.get_name<AnimationSystem>());
        // system_mgr.disable_system(context.system_id_proxy.get_name<AISystem>());
        // system_mgr.disable_system(context.system_id_proxy.get_name<GraphicsSystem>());
        // system_mgr.disable_system(context.system_id_proxy.get_name<PlayerSystem>());

        FrameData& next = *(next_frame(m_playback_head).frame);
        for (auto& [entity_handle, transform] : next.transform_map) {
          if (context.entity_mgr.is_valid(entity_handle)) {
            cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
            if (entity.has<TransformComponent>()) {
              TransformComponent& tc = entity;
              tc.transform.set_model_matrix(transform.get_model_matrix(), context);
            }
          }
        }

        for (auto& event : *next.events) {
          context.event_mgr.send_event(event);
        }
        m_accumulated_playback_time -= current_frame.dt;
        step_playback_head();

        context.replay_time_left -= current_frame.dt;

        return recorded_dt;
      } else {
        float progress = (m_accumulated_playback_time / current_frame.dt);

        FrameData& next = *(next_frame(m_playback_head).frame);

        for (auto& [entity_handle, transform] : current_frame.transform_map) {
          if (context.entity_mgr.is_valid(entity_handle)) {
            // If an entry exists for this entity in the current frame and the next frame...
            if (auto next_found = next.transform_map.find(entity_handle);
                next_found != next.transform_map.end()) {
              cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
              if (entity.has<TransformComponent>()) {
                TransformComponent& tc = entity;

                glm::mat4 current_model_matrix = transform.get_model_matrix();
                glm::mat4 next_model_matrix = next_found->second.get_model_matrix();

                // tc.transform.set_model_matrix(
                //     glm::interpolate(current_model_matrix, next_model_matrix, progress),
                //     context);
                tc.transform.set_model_matrix(
                    (1.f - progress) * current_model_matrix + (progress)*next_model_matrix,
                    context);
              }
            }
          }
        }

        system_mgr.disable_all_systems();
        system_mgr.enable_system(context.system_id_proxy.get_name<GraphicsSystem>());
        system_mgr.enable_system(context.system_id_proxy.get_name<ParticleSystem>());
        system_mgr.enable_system(context.system_id_proxy.get_name<ReplaySystem>());
        return dt * factor;
      }
    }
    case RecorderState::POST_PLAYBACK_DELAY: {
      //
      context.is_replay = true;
      system_mgr.set_active_fingerprint(m_system_fingerprint_continuous);
      // for (auto& [entity_handle, transform] : m_recording_head.frame->transform_map) {
      //   if (context.entity_mgr.is_valid(entity_handle)) {
      //     cor::Entity entity = context.entity_mgr.get_entity(entity_handle);
      //     if (entity.has<TransformComponent>()) {
      //       TransformComponent& tc = entity;
      //       tc.transform.set_model_matrix(transform.get_model_matrix(), context);
      //     }
      //   }
      // }

      if (m_post_playback_time <= 0.f) {
        m_recording_state = RecorderState::RECORDING;
        m_playback_just_finished = true;
        context.event_mgr.send_event(ReplayEnded{});
      }
      m_post_playback_time -= dt;
      return dt * factor;

    } break;
  }
  return dt;
}

// void EventRecorder::start_recording(::bse::SerializedData restoration_point) {
//   //
//   m_recording_state = RecorderState::RECORDING;
//   m_frames.clear();
//   m_frames.push_back(std::move(m_idle_frame));
//   m_idle_frame.events = std::make_unique<std::vector<Event>>();

//   m_restoration_points.push_back(std::move(restoration_point));

//   m_current_frame = std::end(m_frames) - 1;
// }

// void EventRecorder::stop_recording() {
//   //
//   m_recording_state = RecorderState::IDLE;
// }

void EventRecorder::play(cor::FrameContext& context, cor::SystemManager& system_mgr) {
  //
  m_end_playback_restoration_point = std::move(save_game_state(context, system_mgr));
  rewind_playback_head();
  m_recording_state = RecorderState::PLAYING;
  m_playback_just_started = true;
  m_system_fingerprint_restoration = system_mgr.get_active_fingerprint();
  m_system_fingerprint_continuous = m_system_fingerprint_restoration;

  std::cout << "PLAYYYYYYYYYYYYYYYYYY \n";

  // if (!m_frames.empty()) {
  //   m_recording_state = RecorderState::PLAYING;
  //   m_current_frame = std::begin(m_frames);

  //   context.dt = m_current_frame->dt;
  //   context.speed_factor = m_current_frame->speed_factor;
  //   context.unslowed_dt = m_current_frame->unslowed_dt;
  //   context.frame_num = m_current_frame->frame_num;

  //   for (auto& event : *m_current_frame->events) {
  //     context.event_mgr.send_event(event);
  //   }
  //   float recorded_dt = m_current_frame->dt;
  //   m_current_frame++;
  // }
}

bool EventRecorder::is_playing() const {
  //
  return m_recording_state == RecorderState::PLAYING;
}

void EventRecorder::serialize(::bse::SerializedData& data) const {
  bse::serialize(m_segments, data);
  bse::serialize(m_recording_state, data);
  // int index = m_current_segment - std::begin(m_segments);
  // data += bse::serialize(index);
  return;
}

void EventRecorder::deserialize(const ::bse::SerializedData& data, int& offset) {
  bse::deserialize(m_segments, data, offset);
  bse::deserialize(m_recording_state, data, offset);
  // int index = 0;
  // bse::deserialize(index, data, offset);
  // m_current_segment = std::begin(m_segments) + index;
}

//

void EventRecorder::FrameData::serialize(::bse::SerializedData& data) const {
  bse::serialize(events, data);
  bse::serialize(dt, data);
  bse::serialize(speed_factor, data);
  bse::serialize(unslowed_dt, data);
  bse::serialize(frame_num, data);
  return;
}

void EventRecorder::FrameData::deserialize(const ::bse::SerializedData& data, int& offset) {
  bse::deserialize(events, data, offset);
  bse::deserialize(dt, data, offset);
  bse::deserialize(speed_factor, data, offset);
  bse::deserialize(unslowed_dt, data, offset);
  bse::deserialize(frame_num, data, offset);
}

void EventRecorder::Segment::serialize(::bse::SerializedData& data) const {
  bse::serialize(frames, data);
  bse::serialize(restoration_point, data);
  bse::serialize(is_full, data);
  return;
}

void EventRecorder::Segment::deserialize(const ::bse::SerializedData& data, int& offset) {
  bse::deserialize(frames, data, offset);
  bse::deserialize(restoration_point, data, offset);
  bse::deserialize(is_full, data, offset);
}

bool EventRecorder::is_recording() const {
  //
  return m_recording_state == RecorderState::RECORDING;
}

void EventRecorder::step_recording_head() {
  //
  m_recording_head.frame++;
  if (m_recording_head.frame == std::end(m_recording_head.segment->frames)) {
    m_recording_head.segment->is_full = true;

    m_recording_head.segment = next_segment(m_recording_head.segment);
    m_recording_head.frame = std::begin(m_recording_head.segment->frames);
    m_recording_head.segment->is_full = false;
  }

  m_recording_head.frame->events->clear();
  m_recording_head.frame->transform_map.clear();

  // return *(m_recording_head.frame);
}

void EventRecorder::step_playback_head() {
  //
  m_playback_head.frame++;

  if (m_playback_head.frame == std::end(m_playback_head.segment->frames)) {
    m_playback_head.segment->is_full = true;

    m_playback_head.segment = next_segment(m_playback_head.segment);
    m_playback_head.frame = std::begin(m_playback_head.segment->frames);
    m_playback_head.segment->is_full = false;
  }

  if (m_playback_head.frame->frame_num >= previous_frame(m_recording_head).frame->frame_num - 2) {
    m_recording_state = RecorderState::POST_PLAYBACK_DELAY;
  }

  // return *(m_playback_head.frame);
}

void EventRecorder::rewind_playback_head() {
  m_playback_head.segment = next_segment(m_recording_head.segment);
  while (m_playback_head.segment->restoration_point.bytes.empty()) {
    m_playback_head.segment = next_segment(m_playback_head.segment);
  }
  m_playback_head.frame = std::begin(m_playback_head.segment->frames);
}

bse::SerializedData EventRecorder::save_game_state(cor::FrameContext& context,
                                                   cor::SystemManager& system_mgr) {
  bse::SerializedData state;
  state.reset();
  bse::serialize(context.entity_mgr, state);
  bse::serialize(system_mgr, state);
  bse::serialize(context.event_mgr, state);
  return state;
}

void EventRecorder::restore_game_state(cor::FrameContext& context, cor::SystemManager& system_mgr,
                                       bse::SerializedData& game_state) {
  int offset = 0;
  // context.entity_mgr.clear();
  bse::deserialize(context.entity_mgr, game_state, offset);
  bse::deserialize(system_mgr, game_state, offset);
  bse::deserialize(context.event_mgr, game_state, offset);
}

EventRecorder::SegmentIterator EventRecorder::next_segment(SegmentIterator it) {
  it++;
  if (it == std::end(m_segments)) {
    it = std::begin(m_segments);
  }
  return it;
}

EventRecorder::SegmentIterator EventRecorder::previous_segment(SegmentIterator it) {
  if (it == std::begin(m_segments)) {
    it = std::end(m_segments) - 1;
  } else {
    it--;
  }
  return it;
}

EventRecorder::Head EventRecorder::next_frame(Head it) {
  it.frame++;
  if (it.frame == std::end(it.segment->frames)) {
    it.segment = next_segment(it.segment);
    it.frame = std::begin(it.segment->frames);
  }
  return it;
}

EventRecorder::Head EventRecorder::previous_frame(Head it) {
  if (it.frame == std::begin(it.segment->frames)) {
    it.segment = previous_segment(it.segment);
    it.frame = std::end(it.segment->frames) - 1;
  } else {
    it.frame--;
  }
  return it;
}

}  // namespace gmp
