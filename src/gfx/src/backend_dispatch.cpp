#include "gfx/backend_dispatch.hpp"

#include "gfx/command.hpp"

namespace gfx {
namespace backend_dispatch {

namespace gl = gl44;

void bind_shader(const void* data) {
  const commands::BindShader* real_data = static_cast<const commands::BindShader*>(data);
  gl::glUseProgram(real_data->shader_handle);
}
void bind_uniform_mat4(const void* data) {
  const commands::BindUniformMat4* real_data = static_cast<const commands::BindUniformMat4*>(data);
  gl::glUniformMatrix4fv(real_data->location, 1, false, &real_data->matrix[0][0]);
}

void bind_uniform_mat4_list(const void* data) {
  const commands::BindUniformMat4List* real_data =
      static_cast<const commands::BindUniformMat4List*>(data);
  gl::glUniformMatrix4fv(real_data->location, real_data->count, false, real_data->matrices);
}

void bind_uniform_vec3(const void* data) {
  const commands::BindUniformVec3* real_data = static_cast<const commands::BindUniformVec3*>(data);
  gl::glUniform3fv(real_data->location, 1, &real_data->vector[0]);
}

void bind_uniform_vec3_list(const void* data) {
  const commands::BindUniformVec3List* real_data =
      static_cast<const commands::BindUniformVec3List*>(data);
  gl::glUniform3fv(real_data->location, real_data->count, real_data->vertices);
}

void bind_uniform_vec4(const void* data) {
  const commands::BindUniformVec4* real_data = static_cast<const commands::BindUniformVec4*>(data);
  gl::glUniform4fv(real_data->location, 1, &real_data->vector[0]);
}

void bind_uniform_float(const void* data) {
  const commands::BindUniformFloat* real_data =
      static_cast<const commands::BindUniformFloat*>(data);
  gl::glUniform1f(real_data->location, real_data->value);
}

void bind_uniform_float_list(const void* data) {
  const commands::BindUniformFloatList* real_data =
      static_cast<const commands::BindUniformFloatList*>(data);
  gl::glUniform1fv(real_data->location, real_data->count, real_data->floats);
}

void bind_uniform_int(const void* data) {
  const commands::BindUniformInt* real_data = static_cast<const commands::BindUniformInt*>(data);
  gl::glUniform1i(real_data->location, real_data->value);
}

void bind_uniform_bool(const void* data) {
  const commands::BindUniformBool* real_data = static_cast<const commands::BindUniformBool*>(data);
  gl::glUniform1i(real_data->location, real_data->value);
}

void bind_texture(const void* data) {
  const commands::BindTexture* real_data = static_cast<const commands::BindTexture*>(data);
  gl::glUniform1i(real_data->location, real_data->index);

  gl::glActiveTexture(gl::GL_TEXTURE0 + real_data->index);
  gl::glBindTexture(gl::GL_TEXTURE_2D, real_data->texture);
}

// bind a uniform buffer
void bind_uniform_buffer(const void* data) {
  const commands::BindUniformBuffer* real_data =
      static_cast<const commands::BindUniformBuffer*>(data);
  // gl::glUniformBlockBinding(real_data->program, real_data->id, real_data->location);
  gl::glBindBufferBase(gl::GL_UNIFORM_BUFFER, 3, real_data->id);
}

void draw_arrays(const void* data) {
  const commands::DrawArrays* real_data = static_cast<const commands::DrawArrays*>(data);
  gl::glBindVertexArray(real_data->vao);

  // Draw the triangles !
  gl::glDrawArrays(real_data->triangle_mode,               // mode
                   0,                                      // base
                   (gl::GLsizei)real_data->vertex_count);  // count
}

void draw_indexed(const void* data) {
  const commands::DrawIndexed* real_data = static_cast<const commands::DrawIndexed*>(data);
  gl::glBindVertexArray(real_data->vao);

  // Draw the triangles !
  gl::glDrawElements(real_data->triangle_mode,              // mode
                     (gl::GLsizei)real_data->vertex_count,  // count
                     gl::GL_UNSIGNED_INT,                   // type
                     (void*)0                               // element array buffer offset
  );
}

void draw_instanced_indexed(const void* data) {
  const commands::DrawInstancedIndexed* real_data =
      static_cast<const commands::DrawInstancedIndexed*>(data);
  gl::glBindVertexArray(real_data->vao);
  gl::glDrawElementsInstanced(gl::GL_TRIANGLES, real_data->vertex_count, gl::GL_UNSIGNED_INT, 0,
                              real_data->instance_count);
}

void draw_lines(const void* data) {
  const commands::DrawLines* real_data = static_cast<const commands::DrawLines*>(data);
  gl::glBindVertexArray(real_data->vao);

  gl::glDrawArrays(gl::GL_LINES, 0, real_data->vertex_count);
}

void draw_points(const void* data) {
  const commands::DrawPoints* real_data = static_cast<const commands::DrawPoints*>(data);
  gl::glPointSize(real_data->point_size);
  gl::glBindVertexArray(real_data->vao);

  gl::glDrawArrays(gl::GL_POINTS, 0, real_data->vertex_count);
}

void set_stencil_ref(const void* data) {
  const commands::SetStencilRef* real_data = static_cast<const commands::SetStencilRef*>(data);
  gl::glStencilFunc(gl::GL_ALWAYS, real_data->value, 0xFF);
}

};  // namespace backend_dispatch
};  // namespace gfx