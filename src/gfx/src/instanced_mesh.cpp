#include <ext/ext.hpp>
#include <gfx/context.hpp>
#include <gfx/instanced_mesh.hpp>

namespace gfx {
InstancedMesh::InstancedMesh() {
  gfx::Context::run([this]() {
    gl::glGenBuffers(1, &m_vbo);
    gl::glBindBuffer(gl::GL_ARRAY_BUFFER, m_vbo);

    // Initialize with empty (NULL) buffer : it will be updated later, each frame.
    gl::glBufferData(gl::GL_ARRAY_BUFFER, 100 * 16 * sizeof(gl::GLfloat), NULL, gl::GL_STREAM_DRAW);
    glm::mat4 m = glm::mat4(1);
    gl::glBufferSubData(gl::GL_ARRAY_BUFFER, 0, 16 * sizeof(gl::GLfloat), &m);
    gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);

    // create VAO
    gl::glGenVertexArrays(1, &m_vao);
    gl::glBindVertexArray(m_vao);

    // bind instanced vbo
    gl::glBindBuffer(gl::GL_ARRAY_BUFFER, m_vbo);

    // model matrices
    gl::glEnableVertexAttribArray(2);
    gl::glVertexAttribPointer(2, 16, gl::GL_FLOAT, gl::GL_FALSE, 16 * sizeof(gl::GLfloat),
                              (void *)0);
    gl::glVertexAttribDivisor(2, 1);

    gl::glBindVertexArray(0);
  });
}
InstancedMesh::~InstancedMesh() {}

void InstancedMesh::set_mesh(unsigned int vbo) {
  gfx::Context::run([this, vbo]() {
    gl::glBindVertexArray(m_vao);

    gl::glBindBuffer(gl::GL_ARRAY_BUFFER, vbo);

    gl::glBindVertexArray(0);
  });
}
unsigned int InstancedMesh::get_vao() { return m_vao; }
}  // namespace gfx