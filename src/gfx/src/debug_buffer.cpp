#include "gfx/debug_buffer.hpp"

#include "bse/asset.hpp"
#include "gfx/shader.hpp"

namespace gfx {
DebugBuffer::DebugBuffer() {
  gl::glGenVertexArrays(1, &m_vao);
  gl::glBindVertexArray(m_vao);

  // Generate and bind VBO
  gl::glGenBuffers(1, &m_vbo);
  gl::glBindBuffer(gl::GL_ARRAY_BUFFER, m_vbo);

  // Specify vertex attributes
  // Position
  gl::glEnableVertexAttribArray(0);
  gl::glVertexAttribPointer(0, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(glm::vec3) * 2,
                            (void *)(0 * sizeof(gl::GLfloat)));
  // Color
  gl::glEnableVertexAttribArray(1);
  gl::glVertexAttribPointer(1, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(glm::vec3) * 2,
                            (void *)(3 * sizeof(gl::GLfloat)));

  // Unbind VAO and VBO
  gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
  gl::glBindVertexArray(0);

  m_shader = bse::Asset<Shader>("assets/shader/debug_draw.frag");
}

DebugBuffer::~DebugBuffer() {
  gl::glDeleteVertexArrays(1, &m_vao);
  gl::glDeleteBuffers(1, &m_vbo);
}

void DebugBuffer::add_vertices(std::vector<float> vertices) {
  m_vertices.insert(m_vertices.end(), vertices.begin(), vertices.end());
}

void DebugBuffer::update_buffer() {
  if (m_vertices.size() > 0) {
    gl::glBindBuffer(gl::GL_ARRAY_BUFFER, m_vbo);
    gl::glBufferData(gl::GL_ARRAY_BUFFER, (m_vertices.size()) * sizeof(float), &(m_vertices[0]),
                     gl::GL_DYNAMIC_DRAW);

    gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
  }
}

unsigned int DebugBuffer::get_vao() const { return m_vao; }

bse::Asset<Shader> DebugBuffer::get_shader() const { return m_shader; }

int DebugBuffer::get_vertex_count() const { return m_vertices.size() / 6; }

void DebugBuffer::clear() { m_vertices.clear(); }

}  // namespace gfx