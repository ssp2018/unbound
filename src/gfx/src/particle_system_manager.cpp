
#include "gfx/particle_system_manager.hpp"

#include <bse/mesh/mesh_loader.hpp>
#include <bse/task_handle.hpp>
#include <gfx/context.hpp>

namespace gfx {

ParticleSystemManager::ParticleSystemManager() {
  // create initial 50 particle systems
  for (int i = 0; i < 50; i++) {
    ParticleSystem ps;
    ps.vbo = create_vbo_bufffer(1000);
    create_vao_buffer(ps);
    ps.last_used = 99;
    ps.handle.iteration = 0;
    ps.handle.register_index = i;
    m_particle_systems.push_back(ps);
    m_unused_particle_systems.push_back(i);
  }
}

ParticleSystemManager::~ParticleSystemManager() {
  for (auto particle_system : m_particle_systems) {
    gl::glDeleteBuffers(1, &particle_system.vbo);
    gl::glDeleteBuffers(1, &particle_system.instanced_vbo);
    gl::glDeleteBuffers(1, &particle_system.ebo);
    gl::glDeleteVertexArrays(1, &particle_system.vao);
  }
}

void ParticleSystemManager::prepare_for_frame() { m_used_particle_index = 0; }

ParticleSystemHandle ParticleSystemManager::get_particle_system() {
  // find the first free particle system
  // if (m_unused_particle_systems.size() > 0) {
  //   ParticleSystemHandle handle =
  //       m_particle_systems[m_unused_particle_systems[m_unused_particle_systems.size() - 1]];
  //   handle.iteration += 1;
  // } else {
  // find oldest particle system or create new?
  ParticleSystem *oldest = &m_particle_systems[0];
  for (auto &particle_system : m_particle_systems) {
    // particle_system.
    if (particle_system.last_used > oldest->last_used) {
      oldest = &particle_system;
    }
  }
  if (oldest->last_used > 0) {
    ParticleSystemHandle &handle = oldest->handle;
    handle.iteration += 1;
    oldest->handle.iteration = handle.iteration;
    oldest->last_used = 0;
    // oldest.handle =
    return handle;
  } else {
    ParticleSystem ps;
    ps.vbo = create_vbo_bufffer(1000);
    create_vao_buffer(ps);
    ps.last_used = 99;
    ps.handle.iteration = 0;
    ps.handle.register_index = m_particle_systems.size();
    m_particle_systems.push_back(ps);
    LOG(NOTICE) << "Increasing particle system pool size";
    return ps.handle;
    // m_unused_particle_systems.push_back(i);
  }
  // }
}

int ParticleSystemManager::update_buffer(float *floats, int count, ParticleSystemHandle handle) {
  // update the current particle system

  if (m_used_particle_index >= m_particle_systems.size()) {
    for (int i = 0; i < 50; i++) {
      ParticleSystem ps;
      ps.vbo = create_vbo_bufffer(1000);
      create_vao_buffer(ps);
      m_particle_systems.push_back(ps);
    }
  }

  gfx::Context::run_async([this, handle, count, floats, index = m_used_particle_index]() {
    gl::glBindBuffer(gl::GL_ARRAY_BUFFER, m_particle_systems[index].vbo);
    gl::glBufferSubData(gl::GL_ARRAY_BUFFER, 0, count * sizeof(gl::GLfloat), floats);
  });
  return m_particle_systems[m_used_particle_index++].vao;
}

unsigned int ParticleSystemManager::get_vao(ParticleSystemHandle handle) {
  if (handle.register_index > m_particle_systems.size()) {
    return 0;
  }
  if (handle.iteration != m_particle_systems[handle.register_index].handle.iteration) {
    return 0;
  } else {
    return m_particle_systems[handle.register_index].vao;
  }
}

void ParticleSystemManager::update_age() {
  for (ParticleSystem &ps : m_particle_systems) {
    ps.last_used += 1;
  }
}

// void update_buffer(float *floats, int count, unsigned int vbo, int max_particles) {
//   gl::glBindBuffer(gl::GL_ARRAY_BUFFER, vbo);
//   /*gl::glBufferData(gl::GL_ARRAY_BUFFER, max_particles * 8 * sizeof(gl::GLfloat), NULL,
//                    gl::GL_STREAM_DRAW);*/
//   gl::glBufferSubData(gl::GL_ARRAY_BUFFER, 0, count * sizeof(gl::GLfloat), floats);
// }

unsigned int ParticleSystemManager::create_vbo_bufffer(int max_particles) {
  unsigned int vbo;
  gfx::Context::run([&vbo, max_particles]() {
    // gl::glBindVertexArray(m_mesh.get_vao());

    // create VBO
    gl::glGenBuffers(1, &vbo);
    gl::glBindBuffer(gl::GL_ARRAY_BUFFER, vbo);

    // Initialize with empty (NULL) buffer : it will be updated later, each frame.
    gl::glBufferData(gl::GL_ARRAY_BUFFER, max_particles * 9 * sizeof(gl::GLfloat), NULL,
                     gl::GL_STREAM_DRAW);
    gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
  });
  return vbo;
}

void ParticleSystemManager::create_vao_buffer(ParticleSystem &ps) {
  // unsigned int vao;

  gfx::Context::run([&ps]() {
    bse::Flags<bse::VertexTypes::COUNT> types;
    types.set(bse::VertexTypes::POSITION);
    types.set(bse::VertexTypes::UV);

    float vertices[] = {-1, 0, -1, 0,
                        1,  //
                        1,  0, -1, 1,
                        1,  //
                        -1, 0, 1,  0,
                        0,  //
                        1,  0, 1,  1, 0};

    unsigned int indices[] = {0, 1, 2, 1, 3, 2};

    // create VAO
    gl::glGenVertexArrays(1, &ps.vao);
    gl::glBindVertexArray(ps.vao);

    // create VBO
    gl::glGenBuffers(1, &ps.instanced_vbo);
    gl::glBindBuffer(gl::GL_ARRAY_BUFFER, ps.instanced_vbo);

    // upload data to VBO
    gl::glBufferData(gl::GL_ARRAY_BUFFER, 20 * sizeof(gl::GLfloat), &vertices[0],
                     gl::GL_STATIC_DRAW);

    // bind mesh ebo
    // unsigned int ebo;
    gl::glGenBuffers(1, &ps.ebo);
    gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, ps.ebo);
    gl::glBufferData(gl::GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(unsigned int), &indices[0],
                     gl::GL_STATIC_DRAW);

    // // position
    int vertexSize = 5 * sizeof(gl::GLfloat);
    gl::glEnableVertexAttribArray(0);
    gl::glVertexAttribPointer(0, 3, gl::GL_FLOAT, gl::GL_FALSE, vertexSize,
                              (void *)(0 * sizeof(gl::GLfloat)));
    gl::glVertexAttribDivisor(0, 0);
    // uv
    gl::glEnableVertexAttribArray(1);
    gl::glVertexAttribPointer(1, 2, gl::GL_FLOAT, gl::GL_FALSE, 5 * sizeof(gl::GLfloat),
                              (void *)(3 * sizeof(gl::GLfloat)));
    gl::glVertexAttribDivisor(1, 0);

    // bind instanced vbo
    gl::glBindBuffer(gl::GL_ARRAY_BUFFER, ps.vbo);

    constexpr int size = 9;
    // position
    gl::glEnableVertexAttribArray(2);
    gl::glVertexAttribPointer(2, 3, gl::GL_FLOAT, gl::GL_FALSE, size * sizeof(gl::GLfloat),
                              (void *)0);
    gl::glVertexAttribDivisor(2, 1);
    // colors
    gl::glEnableVertexAttribArray(3);
    gl::glVertexAttribPointer(3, 4, gl::GL_FLOAT, gl::GL_FALSE, size * sizeof(gl::GLfloat),
                              (void *)(3 * sizeof(gl::GLfloat)));
    gl::glVertexAttribDivisor(3, 1);
    // size
    gl::glEnableVertexAttribArray(4);
    gl::glVertexAttribPointer(4, 1, gl::GL_FLOAT, gl::GL_FALSE, size * sizeof(gl::GLfloat),
                              (void *)(7 * sizeof(gl::GLfloat)));
    gl::glVertexAttribDivisor(4, 1);
    // rotation
    gl::glEnableVertexAttribArray(5);
    gl::glVertexAttribPointer(5, 1, gl::GL_FLOAT, gl::GL_FALSE, size * sizeof(gl::GLfloat),
                              (void *)(8 * sizeof(gl::GLfloat)));
    gl::glVertexAttribDivisor(5, 1);

    gl::glBindVertexArray(0);

    // gl::glDeleteBuffers(1, &ebo);
    // gl::glDeleteBuffers(1, &mesh_vbo);

    gl::GLenum error = gl::glGetError();
    if (error != gl::GL_NO_ERROR) {
      int i = 0;
    }
  });

  // return vao;
}

// bse::Asset<gfx::Shader> get_shader() { return m_shader; }

}  // namespace gfx