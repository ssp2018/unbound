#include "gfx/command.hpp"

#include "gfx/backend_dispatch.hpp"

namespace gfx {
namespace commands {

typedef void (*BackendDispatchFunction)(const void*);

const BackendDispatchFunction BindShader::DISPATCH_FUNCTION = &backend_dispatch::bind_shader;
const BackendDispatchFunction BindUniformMat4::DISPATCH_FUNCTION =
    &backend_dispatch::bind_uniform_mat4;
const BackendDispatchFunction BindUniformMat4List::DISPATCH_FUNCTION =
    &backend_dispatch::bind_uniform_mat4_list;
const BackendDispatchFunction BindUniformVec3::DISPATCH_FUNCTION =
    &backend_dispatch::bind_uniform_vec3;
const BackendDispatchFunction BindUniformVec3List::DISPATCH_FUNCTION =
    &backend_dispatch::bind_uniform_vec3_list;
const BackendDispatchFunction BindUniformVec4::DISPATCH_FUNCTION =
    &backend_dispatch::bind_uniform_vec4;
const BackendDispatchFunction BindUniformFloat::DISPATCH_FUNCTION =
    &backend_dispatch::bind_uniform_float;
const BackendDispatchFunction BindUniformFloatList::DISPATCH_FUNCTION =
    &backend_dispatch::bind_uniform_float_list;
const BackendDispatchFunction BindUniformInt::DISPATCH_FUNCTION =
    &backend_dispatch::bind_uniform_int;
const BackendDispatchFunction BindUniformBool::DISPATCH_FUNCTION =
    &backend_dispatch::bind_uniform_bool;
const BackendDispatchFunction BindTexture::DISPATCH_FUNCTION = &backend_dispatch::bind_texture;
const BackendDispatchFunction BindUniformBuffer::DISPATCH_FUNCTION =
    &backend_dispatch::bind_uniform_buffer;
const BackendDispatchFunction DrawArrays::DISPATCH_FUNCTION = &backend_dispatch::draw_arrays;
const BackendDispatchFunction DrawIndexed::DISPATCH_FUNCTION = &backend_dispatch::draw_indexed;
const BackendDispatchFunction DrawInstancedIndexed::DISPATCH_FUNCTION =
    &backend_dispatch::draw_instanced_indexed;
const BackendDispatchFunction DrawLines::DISPATCH_FUNCTION = &backend_dispatch::draw_lines;
const BackendDispatchFunction DrawPoints::DISPATCH_FUNCTION = &backend_dispatch::draw_points;
const BackendDispatchFunction SetStencilRef::DISPATCH_FUNCTION = &backend_dispatch::set_stencil_ref;

}  // namespace commands
}  // namespace gfx