#include "gfx/shader.hpp"

#include "bse/error.hpp"
#include "bse/file_path.hpp"
#include "bse/result.hpp"
#include <bse/file.hpp>
#include <bse/log.hpp>
#include <ext/ext.hpp>
template class bse::Asset<gfx::Shader>;

namespace gfx {

namespace gl = gl44;

Shader::Shader() {}
Shader::Shader(Shader&& other) {
  m_shader_handler = std::move(other.m_shader_handler);
  m_uniforms = std::move(other.m_uniforms);
}

Shader::~Shader() {}

Shader& Shader::operator=(Shader&& other) {
  if (&other != this) {
    m_shader_handler = std::move(other.m_shader_handler);
    m_uniforms = std::move(other.m_uniforms);
  }
  return *this;
}

bse::Result<Shader> Shader::load(bse::FilePath path) {
  Shader shader;
  path.set_extension("vert");
  std::string vert_res = *bse::load_file_str(path);

  path.set_extension("frag");
  std::string frag_res = *bse::load_file_str(path);

  int vert_program =
      shader.create_shader_program((vert_res), static_cast<int>(gl::GL_VERTEX_SHADER));
  int frag_program =
      shader.create_shader_program((frag_res), static_cast<int>(gl::GL_FRAGMENT_SHADER));
  int res = shader.generate_shader(vert_program, frag_program);
  shader.print_all_uniforms();
  if (res == -1) {
    return bse::Error() << "Failed to load shader" << path;
  }

  return std::move(shader);
}

int Shader::create_shader_program(std::string program, unsigned int program_stage_mask) const {
  gl::GLenum program_enum = static_cast<gl::GLenum>(program_stage_mask);

  int shader_program = gl::glCreateShader(program_enum);

  const char* c_str = program.c_str();
  gl::glShaderSource(shader_program, 1, &c_str, NULL);
  gl::glCompileShader(shader_program);

  // verify status:

  // GLint compiled;
  gl::GLint success = 0;
  gl::glGetShaderiv(shader_program, gl::GL_COMPILE_STATUS, &success);
  if (!success) {
    gl::GLint logSize = 0;
    gl::glGetShaderiv(shader_program, gl::GL_INFO_LOG_LENGTH, &logSize);
    char infolog[1024];
    gl::glGetShaderInfoLog(shader_program, 1024, NULL, infolog);
    // ASSERT(false) << infolog;
    LOG(NOTICE) << infolog << "\n";
  }
  return shader_program;
}

int Shader::generate_shader(int vertex_program, int fragment_program) {
  if (m_shader_handler != -1) {
    // delete old shaderprogram
    gl::glDeleteProgram(m_shader_handler);
  }

  m_shader_handler = gl::glCreateProgram();

  gl::glAttachShader(m_shader_handler, vertex_program);
  gl::glAttachShader(m_shader_handler, fragment_program);

  gl::glLinkProgram(m_shader_handler);

  // check link status
  gl::GLint isLinked = 0;
  gl::glGetProgramiv(m_shader_handler, gl::GL_LINK_STATUS, (int*)&isLinked);
  if (!isLinked) {
    gl::GLint logSize = 0;
    gl::glGetShaderiv(m_shader_handler, gl::GL_INFO_LOG_LENGTH, &logSize);
    char infolog[1024];
    gl::glGetProgramInfoLog(m_shader_handler, 1024, NULL, infolog);
    infolog[1023] = NULL;
    LOG(NOTICE) << infolog;
    return -1;
  }

  return m_shader_handler;  // success
}
unsigned int Shader::get_shader_handle() const { return m_shader_handler; }

void Shader::bind_program() const {
  gl::glUseProgram(m_shader_handler);
  // LOG(NOTICE) << "bind shader" << m_shader_handler << "\n";
}

int Shader::get_uniform_location(const std::string& name) const {
  auto uniform = m_uniforms.find(name);
  if (uniform == m_uniforms.end()) {
    return -1;
  }
  return uniform->second;
  // return gl::glGetUniformLocation(m_shader_handler, name);
}

void Shader::print_all_uniforms() {
  gl::GLint i;
  gl::GLint count;

  gl::GLint size;   // size of the variable
  gl::GLenum type;  // type of the variable (float, vec3 or mat4, etc)

  const gl::GLsizei bufSize = 30;  // maximum name length
  gl::GLchar name[bufSize];        // variable name in GLSL
  gl::GLsizei length;              // name length

  gl::glGetProgramiv(m_shader_handler, gl::GL_ACTIVE_UNIFORMS, &count);
  // printf("Active Uniforms: %d\n", count);

  for (i = 0; i < count; i++) {
    gl::glGetActiveUniform(m_shader_handler, (gl::GLuint)i, bufSize, &length, &size, &type, name);

    unsigned int location = gl::glGetUniformLocation(m_shader_handler, name);
    std::string name_string = name;
    if (name_string.back() == ']') {
      name_string.erase(name_string.length() - 3);
    }

    // printf("Uniform #%d Type: %u Name: %s\n", i, type, name);
    m_uniforms.emplace(name_string, location);
    // m_uniforms[name_string] = location;
  }
}
};  // namespace gfx
