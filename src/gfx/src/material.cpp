#include "gfx/material.hpp"

#include "bse/asset.hpp"
#include "bse/file_path.hpp"
#include "bse/mesh/mesh_loader.hpp"
#include "bse/result.hpp"
#include "gfx/shader.hpp"
#include "gfx/texture.hpp"
#include <bse/file.hpp>
template class bse::Asset<gfx::Material>;

namespace gfx {

namespace gl = gl44;

Material::Material() {
  // create default shader
  m_shader = bse::Asset<Shader>("assets/shader/shader.shd");
  get_uniform_locations();
}
Material::Material(Material&& other) {
  model_matrix_location = std::move(other.model_matrix_location);
  m_required_vertex_data = std::move(other.m_required_vertex_data);
  view_projection_matrix_location = std::move(other.view_projection_matrix_location);
  texture_location = std::move(other.texture_location);
  // m_model_view_projection_matrix_location =
  //     std::move(other.m_model_view_projection_matrix_location);
  directional_light_location = std::move(other.directional_light_location);
  directional_light_color_location = std::move(other.directional_light_color_location);
  light_space_matrix_location = std::move(other.light_space_matrix_location);
  point_light_position_location = std::move(other.point_light_position_location);
  point_light_color_location = std::move(other.point_light_color_location);
  point_light_radius_location = std::move(other.point_light_radius_location);
  point_light_count_location = std::move(other.point_light_count_location);
  depth_map_0_location = std::move(other.depth_map_0_location);
  depth_map_1_location = std::move(other.depth_map_1_location);
  depth_map_2_location = std::move(other.depth_map_2_location);
  depth_map_3_location = std::move(other.depth_map_3_location);
  model_matrix_ubo_location = std::move(other.model_matrix_ubo_location);
  m_shader = std::move(other.m_shader);
}

Material::~Material() {}

Material& Material::operator=(Material&& other) {
  if (&other != this) {
    model_matrix_location = std::move(other.model_matrix_location);
    m_required_vertex_data = std::move(other.m_required_vertex_data);
    view_projection_matrix_location = std::move(other.view_projection_matrix_location);
    texture_location = std::move(other.texture_location);
    // m_model_view_projection_matrix_location =
    //     std::move(other.m_model_view_projection_matrix_location);
    directional_light_location = std::move(other.directional_light_location);
    directional_light_color_location = std::move(other.directional_light_color_location);
    light_space_matrix_location = std::move(other.light_space_matrix_location);
    point_light_position_location = std::move(other.point_light_position_location);
    point_light_color_location = std::move(other.point_light_color_location);
    point_light_radius_location = std::move(other.point_light_radius_location);
    point_light_count_location = std::move(other.point_light_count_location);
    depth_map_0_location = std::move(other.depth_map_0_location);
    depth_map_1_location = std::move(other.depth_map_1_location);
    depth_map_2_location = std::move(other.depth_map_2_location);
    depth_map_3_location = std::move(other.depth_map_3_location);
    model_matrix_ubo_location = std::move(other.model_matrix_ubo_location);
    m_shader = std::move(other.m_shader);
  }
  return *this;
}

bse::Result<Material> Material::load(bse::FilePath path) {
  path.set_extension("mtl");
  std::string material_file = *bse::load_file_str(path);

  Material material;

  // parse material file.

  bse::Flags<bse::VertexTypes::COUNT> vertex_data;

  std::istringstream inputstream(material_file);
  std::string line;
  while (std::getline(inputstream, line)) {
    std::istringstream iss(line);

    std::string result;
    if (std::getline(iss, result, ':')) {
      // vertex data key found, check what types it consists of
      if (result == "vertex_data") {
        while (inputstream.peek() == '-') {
          inputstream.get();
          std::getline(inputstream, line);

          std::istringstream shaderType(line);
          if (std::getline(shaderType, result)) {
            // handle result
            if (result.back() == '\r') {
              result.pop_back();
            }
            if (result == "position") {
              vertex_data.set(bse::VertexTypes::POSITION);
            } else if (result == "normal") {
              vertex_data.set(bse::VertexTypes::NORMAL);
            } else if (result == "color") {
              vertex_data.set(bse::VertexTypes::COLOR);
            } else if (result == "uv") {
              vertex_data.set(bse::VertexTypes::UV);
            } else if (result == "skeletal") {
              vertex_data.set(bse::VertexTypes::SKELETAL);
            }
          }
        }
      } else if (result == "shader") {
        // create the shader.
        std::getline(iss, result);
        if (result.back() == '\r') {
          result.pop_back();
        }
        // shader name in result, use it to set shader.
        material.m_shader = bse::Asset<Shader>("assets/shader/" + result + ".shd");
      } else if (result == "texture") {
        std::getline(iss, result);
        if (result.back() == '\r') {
          result.pop_back();
        }
        material.m_texture = bse::Asset<Texture>("assets/texture/" + result);
        material.m_has_texture = true;
      }
    }
  }
  material.m_required_vertex_data = vertex_data;
  material.get_uniform_locations();
  // material.m_shader->print_all_uniforms();
  return std::move(material);
}

bse::Asset<Shader> Material::get_shader() const { return m_shader; }

unsigned int Material::get_shader_handle() const { return m_shader->get_shader_handle(); }

int Material::get_uniform_location(const std::string& name) const {
  return m_shader->get_uniform_location(name);
}

bse::Flags<bse::VertexTypes::COUNT> Material::get_required_vertex_data() const {
  return m_required_vertex_data;
}

void Material::get_uniform_locations() {
  model_matrix_location = m_shader->get_uniform_location("MODEL_MATRIX");
  view_projection_matrix_location = m_shader->get_uniform_location("VIEW_PROJECTION_MATRIX");
  texture_location = m_shader->get_uniform_location("TEXTURE_SAMPLER");
  // model_view_projection_matrix_location =
  //     m_shader->get_uniform_location("MODEL_VIEW_PROJECTION_MATRIX");
  directional_light_location = m_shader->get_uniform_location("DIRECTIONAL_LIGHT");
  directional_light_color_location = m_shader->get_uniform_location("DIRECTIONAL_LIGHT_COLOR");
  light_space_matrix_location = m_shader->get_uniform_location("LIGHT_SPACE_MATRIX");

  // point light
  point_light_position_location = m_shader->get_uniform_location("POINT_LIGHT_POSITION");
  point_light_color_location = m_shader->get_uniform_location("POINT_LIGHT_COLOR");
  point_light_radius_location = m_shader->get_uniform_location("POINT_LIGHT_RADIUS");
  point_light_count_location = m_shader->get_uniform_location("POINT_LIGHT_COUNT");

  // shadow locations
  depth_map_0_location = m_shader->get_uniform_location("DEPTH_MAP_0");
  depth_map_1_location = m_shader->get_uniform_location("DEPTH_MAP_1");
  depth_map_2_location = m_shader->get_uniform_location("DEPTH_MAP_2");
  depth_map_3_location = m_shader->get_uniform_location("DEPTH_MAP_3");
  shadow_matrix_location = m_shader->get_uniform_location("MATRIX_PALETTE");

  model_matrix_ubo_location =
      gl::glGetUniformBlockIndex(m_shader->get_shader_handle(), "MODEL_MATRICES");
}

bool Material::has_texture() const { return m_has_texture; }

unsigned int Material::get_texture() const { return m_texture->get_id(); }
// void Material::bind_uniform_mat4(const float* matrix, const char* name, int count) const {
//   gl::glUniformMatrix4fv(m_shader->get_uniform_location(name), count, false, matrix);
// }

// void Material::bind_uniform_vec3(const float* vec3, const char* name, int count) const {
//   gl::glUniform3fv(m_shader->get_uniform_location(name), count, vec3);
// }

// void Material::bind_texture(const char* name, unsigned int texture, int index) const {
//   // LOG(NOTICE) << index << "\n";
//   gl::glUniform1i(m_shader->get_uniform_location(name), index);

//   gl::glActiveTexture(gl::GL_TEXTURE0 + index);
//   gl::glBindTexture(gl::GL_TEXTURE_2D, texture);
// }

}  // namespace gfx
