
#include "gfx/command_packet.hpp"

#include "gfx/command.hpp"

namespace gfx {
namespace command_packet {

CommandPacket* get_next_command_packet(CommandPacket packet) {
  return reinterpret_cast<CommandPacket*>(reinterpret_cast<char*>(packet) +
                                          OFFSET_NEXT_COMMAND_PACKET);
}

// get the method to execute for this command
commands::BackendDispatchFunction* get_backend_dispatch_function(CommandPacket packet) {
  return reinterpret_cast<commands::BackendDispatchFunction*>(reinterpret_cast<char*>(packet) +
                                                              OFFSET_BACKEND_DISPATCH_FUNCTION);
}

// sotre the nex command packet
void store_next_command_packet(CommandPacket packet, CommandPacket nextPacket) {
  *command_packet::get_next_command_packet(packet) = nextPacket;
}

// set thte backend dispatch function
void store_backend_dispatch_function(CommandPacket packet,
                                     commands::BackendDispatchFunction dispatchFunction) {
  *command_packet::get_backend_dispatch_function(packet) = dispatchFunction;
}

// returns the next command packet
const CommandPacket load_next_command_packet(const CommandPacket packet) {
  return *get_next_command_packet(packet);
}

// get backend dispatch function
const commands::BackendDispatchFunction load_backend_dispatch_function(const CommandPacket packet) {
  return *get_backend_dispatch_function(packet);
}

// get command
const void* load_command(const CommandPacket packet) {
  return reinterpret_cast<char*>(packet) + OFFSET_COMMAND;
}

}  // namespace command_packet
}  // namespace gfx