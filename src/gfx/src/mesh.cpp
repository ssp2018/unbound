
#include "gfx/mesh.hpp"

#include "bse/asset.hpp"     // for Asset
#include "gfx/material.hpp"  // for Material
#include <bse/log.hpp>
#include <bse/utility.hpp>
#include <ext/ext.hpp>
#include <gfx/context.hpp>
template class bse::Asset<gfx::Mesh>;

namespace gfx {

namespace gl = gl44;

const bse::Flags<bse::VertexTypes::COUNT> Mesh::M_TEXTURED_QUAD_VERTEX_TYPES = []() {
  bse::Flags<bse::VertexTypes::COUNT> types;
  types.set(bse::VertexTypes::POSITION);
  types.set(bse::VertexTypes::UV);
  return types;
}();

const std::vector<float> Mesh::M_TEXTURED_QUAD_VERTICES = {-1, 0, -1,  //
                                                           1,  0, -1,  //
                                                           -1, 0, 1,   //
                                                           1,  0, 1};

const std::vector<float> Mesh::M_TEXTURED_QUAD_UVS = {0, 0, 1, 0, 0, 1, 1, 1};

const std::vector<unsigned int> Mesh::M_TEXTURED_QUAD_INDICES = {0, 1, 2, 2, 1, 3};

const std::vector<float> Mesh::M_FULLSCREEN_QUAD_VERTICES = {-1, -1, -0,  //
                                                             1,  -1, -0,  //
                                                             -1, 1,  0,   //
                                                             1,  1,  0};

Mesh::Mesh() {}

Mesh::Mesh(Mesh &&other) { move_from(std::move(other)); }

Mesh::~Mesh() { destroy(); }

Mesh &Mesh::operator=(Mesh &&other) {
  if (&other != this) {
    destroy();
    move_from(std::move(other));
  }
  return *this;
}

void Mesh::destroy() {
  // Find the MeshBuffers object containing all vertex data types
  MeshBuffers *all_types = find_buffers(m_vertex_data);

  // If this is not found, the object has been moved from
  if (all_types) {
    gl::glDeleteVertexArrays(1, &all_types->m_vao);
    gl::glDeleteVertexArrays(1, &all_types->m_instanced_vao);
    all_types->m_vao = 0;
    gl::glDeleteBuffers(1, &all_types->m_vbo_position);
    gl::glDeleteBuffers(1, &all_types->m_vbo_normal);
    gl::glDeleteBuffers(1, &all_types->m_vbo_uv);
    gl::glDeleteBuffers(1, &all_types->m_vbo_color);
    gl::glDeleteBuffers(1, &all_types->m_ebo);
    gl::glDeleteBuffers(1, &all_types->m_instanced_vbo_matrices);

    // Delete VAO for the rest of the buffer objects, these are the only unique resource they hold
    for (auto &[types, buffers] : m_buffers) {
      if (buffers.m_vao) {
        gl::glDeleteVertexArrays(1, &buffers.m_vao);
        gl::glDeleteVertexArrays(1, &buffers.m_instanced_vao);
      }
    }
  }
}

Mesh::Mesh(bse::ModelData &model_data) {
  m_vertex_data = model_data.mesh.available_vertex_data;
  m_mesh_data = std::move(model_data);

  auto buffers_pair = std::make_pair(m_vertex_data, MeshBuffers());

  m_buffers.push_back(buffers_pair);

  auto &[flags, buffers] = m_buffers[0];

  // create VAO
  generate_buffers(m_vertex_data, buffers);

  // create ebo
  gl::glGenBuffers(1, &buffers.m_ebo);
  gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, buffers.m_ebo);
  gl::glBufferData(gl::GL_ELEMENT_ARRAY_BUFFER,
                   m_mesh_data.mesh.indices.size() * sizeof(unsigned int),
                   &m_mesh_data.mesh.indices[0], gl::GL_STATIC_DRAW);

  gl::glBindVertexArray(0);

  // Generate all permutations of MeshBuffers objects. This avoids having to create them during
  // read_update()
  // There's probably a better solution to this
  unsigned long long i = 0;
  while (i < (1 << bse::VertexTypes::COUNT)) {
    if (i & m_vertex_data.to_ullong()) {
      bse::Flags<bse::VertexTypes::COUNT> types = i & m_vertex_data.to_ullong();

      if (!find_buffers(types)) {
        MeshBuffers buffers;
        generate_subset_buffers(types, buffers);

        m_buffers.push_back(std::make_pair(types, buffers));
      }
    }
    i++;
  }

  calculate_aabb(m_mesh_data.mesh.positions, sizeof(gl::GLfloat) * 3);
}

void Mesh::generate_vbo(unsigned int &save_location, void *data, size_t data_size) {
  // create VBOs
  gl::glGenBuffers(1, &save_location);
  gl::glBindBuffer(gl::GL_ARRAY_BUFFER, save_location);

  // upload data to VBO
  gl::glBufferData(gl::GL_ARRAY_BUFFER, data_size, data, gl::GL_STATIC_DRAW);
}

bse::Result<Mesh> Mesh::load(bse::FilePath path) {
  bse::Result<bse::ModelData> mesh_data = bse::MeshLoader::load_mesh(path);

  if (mesh_data) {
    return Mesh(*mesh_data);
  } else {
    return bse::Error() << "Failed to load mesh " << path;
  }
}

void Mesh::generate_buffers(bse::Flags<bse::VertexTypes::COUNT> vertex_data, MeshBuffers &buffers) {
  gl::glGenVertexArrays(1, &buffers.m_vao);
  gl::glBindVertexArray(buffers.m_vao);

  if (vertex_data.test(bse::VertexTypes::POSITION)) {
    generate_vbo(buffers.m_vbo_position, &m_mesh_data.mesh.positions[0],
                 m_mesh_data.mesh.vertex_count * 3 * sizeof(gl::GLfloat));

    gl::glEnableVertexAttribArray(bse::VertexTypes::POSITION);
    gl::glVertexAttribPointer(bse::VertexTypes::POSITION, 3, gl::GL_FLOAT, gl::GL_FALSE,
                              3 * sizeof(gl::GLfloat), (void *)(0));

    gl::glVertexAttribDivisor(bse::VertexTypes::POSITION, 0);
  }
  if (vertex_data.test(bse::VertexTypes::NORMAL)) {
    generate_vbo(buffers.m_vbo_normal, &m_mesh_data.mesh.normals[0],
                 m_mesh_data.mesh.vertex_count * 3 * sizeof(gl::GLfloat));

    gl::glEnableVertexAttribArray(bse::VertexTypes::NORMAL);
    gl::glVertexAttribPointer(bse::VertexTypes::NORMAL, 3, gl::GL_FLOAT, gl::GL_FALSE,
                              3 * sizeof(gl::GLfloat), (void *)(0));

    gl::glVertexAttribDivisor(bse::VertexTypes::NORMAL, 0);
  }
  if (vertex_data.test(bse::VertexTypes::COLOR)) {
    generate_vbo(buffers.m_vbo_color, &m_mesh_data.mesh.color[0],
                 m_mesh_data.mesh.vertex_count * 3 * sizeof(gl::GLfloat));

    gl::glEnableVertexAttribArray(bse::VertexTypes::COLOR);
    gl::glVertexAttribPointer(bse::VertexTypes::COLOR, 3, gl::GL_FLOAT, gl::GL_FALSE,
                              3 * sizeof(gl::GLfloat), (void *)(0));

    gl::glVertexAttribDivisor(bse::VertexTypes::COLOR, 0);
  }
  if (vertex_data.test(bse::VertexTypes::UV)) {
    generate_vbo(buffers.m_vbo_uv, &m_mesh_data.mesh.uv[0],
                 m_mesh_data.mesh.vertex_count * 2 * sizeof(gl::GLfloat));

    gl::glEnableVertexAttribArray(bse::VertexTypes::UV);
    gl::glVertexAttribPointer(bse::VertexTypes::UV, 2, gl::GL_FLOAT, gl::GL_FALSE,
                              2 * sizeof(gl::GLfloat), (void *)(0));
    gl::glVertexAttribDivisor(bse::VertexTypes::UV, 0);
  }
  // glm::mat4 matrices[100];
  // generate_vbo(buffers.m_instanced_vbo_matrices, &matrices[0], 1600 * sizeof(gl::GLfloat));
  // // glm::mat4 m = glm::mat4(1);
  // float m[16] = {1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4};
  // gl::glBufferSubData(gl::GL_ARRAY_BUFFER, 0, 16 * sizeof(float), &m[0]);
  // gl::glBufferSubData(gl::GL_ARRAY_BUFFER, 16, 16 * sizeof(float), &m[0]);

  // gl::glEnableVertexAttribArray(bse::VertexTypes::MATRICES);
  // gl::glVertexAttribPointer(bse::VertexTypes::MATRICES, 4, gl::GL_FLOAT, gl::GL_FALSE,
  //                           16 * sizeof(gl::GLfloat), (void *)(0));
  // gl::glVertexAttribDivisor(bse::VertexTypes::MATRICES, 1);
  // gl::glEnableVertexAttribArray(bse::VertexTypes::MATRICES + 1);
  // gl::glVertexAttribPointer(bse::VertexTypes::MATRICES + 1, 4, gl::GL_FLOAT, gl::GL_FALSE,
  //                           16 * sizeof(gl::GLfloat), (void *)(16));
  // gl::glVertexAttribDivisor(bse::VertexTypes::MATRICES + 1, 1);
  // gl::glEnableVertexAttribArray(bse::VertexTypes::MATRICES + 2);
  // gl::glVertexAttribPointer(bse::VertexTypes::MATRICES + 2, 4, gl::GL_FLOAT, gl::GL_FALSE,
  //                           16 * sizeof(gl::GLfloat), (void *)(32));
  // gl::glVertexAttribDivisor(bse::VertexTypes::MATRICES + 2, 1);
  // gl::glEnableVertexAttribArray(bse::VertexTypes::MATRICES + 3);
  // gl::glVertexAttribPointer(bse::VertexTypes::MATRICES + 3, 4, gl::GL_FLOAT, gl::GL_FALSE,
  //                           16 * sizeof(gl::GLfloat), (void *)(48));
  // gl::glVertexAttribDivisor(bse::VertexTypes::MATRICES + 3, 1);

  // glm::mat4 matrices[100];
  // generate_vbo(buffers.m_instanced_vbo_matrices, &matrices[0], 1600 * sizeof(gl::GLfloat));
  // // glm::mat4 m = glm::mat4(1);
  // // gl::glBufferSubData(gl::GL_ARRAY_BUFFER, 0, 16 * sizeof(gl::GLfloat), &m);

  // gl::glEnableVertexAttribArray(bse::VertexTypes::MATRICES);
  // gl::glVertexAttribPointer(bse::VertexTypes::MATRICES, 16, gl::GL_FLOAT, gl::GL_FALSE,
  //                           16 * sizeof(gl::GLfloat), (void *)(0));
  // gl::glVertexAttribDivisor(bse::VertexTypes::MATRICES, 1);

  // gl::glBindVertexArray(0);

  // instanced
  // gl::glGenVertexArrays(1, &buffers.m_instanced_vao);
  // gl::glBindVertexArray(buffers.m_instanced_vao);

  // if (vertex_data.test(bse::VertexTypes::POSITION)) {
  //   gl::glEnableVertexAttribArray(bse::VertexTypes::POSITION);
  //   gl::glVertexAttribPointer(bse::VertexTypes::POSITION, 3, gl::GL_FLOAT, gl::GL_FALSE,
  //                             3 * sizeof(gl::GLfloat), (void *)(0));
  //   gl::glVertexAttribDivisor(bse::VertexTypes::POSITION, 0);
  // }
  // if (vertex_data.test(bse::VertexTypes::NORMAL)) {
  //   gl::glEnableVertexAttribArray(bse::VertexTypes::NORMAL);
  //   gl::glVertexAttribPointer(bse::VertexTypes::NORMAL, 3, gl::GL_FLOAT, gl::GL_FALSE,
  //                             3 * sizeof(gl::GLfloat), (void *)(0));
  //   gl::glVertexAttribDivisor(bse::VertexTypes::NORMAL, 0);
  // }
  // if (vertex_data.test(bse::VertexTypes::COLOR)) {
  //   gl::glEnableVertexAttribArray(bse::VertexTypes::COLOR);
  //   gl::glVertexAttribPointer(bse::VertexTypes::COLOR, 3, gl::GL_FLOAT, gl::GL_FALSE,
  //                             3 * sizeof(gl::GLfloat), (void *)(0));
  //   gl::glVertexAttribDivisor(bse::VertexTypes::COLOR, 0);
  // }
  // if (vertex_data.test(bse::VertexTypes::UV)) {
  //   gl::glEnableVertexAttribArray(bse::VertexTypes::UV);
  //   gl::glVertexAttribPointer(bse::VertexTypes::UV, 2, gl::GL_FLOAT, gl::GL_FALSE,
  //                             2 * sizeof(gl::GLfloat), (void *)(0));
  //   gl::glVertexAttribDivisor(bse::VertexTypes::UV, 0);
  // }
  // // glm::mat4 matrices[100];
  // // generate_vbo(buffers.m_instanced_vbo_matrices, &matrices[0], 160 * sizeof(gl::GLfloat));
  // // glm::mat4 m = glm::mat4(1);
  // // gl::glBufferSubData(gl::GL_ARRAY_BUFFER, 0, 16 * sizeof(gl::GLfloat), &m);

  // gl::glEnableVertexAttribArray(bse::VertexTypes::MATRICES);
  // gl::glVertexAttribPointer(bse::VertexTypes::MATRICES, 16, gl::GL_FLOAT, gl::GL_FALSE,
  //                           16 * sizeof(gl::GLfloat), (void *)(0));
  // gl::glVertexAttribDivisor(bse::VertexTypes::MATRICES, 1);

  // gl::glBindVertexArray(buffers.m_vao);
}

Mesh::MeshBuffers *Mesh::find_buffers(bse::Flags<bse::VertexTypes::COUNT> vertex_data) {
  for (auto &[vertex_types, buffers] : m_buffers) {
    if (vertex_data == vertex_types) {
      return &buffers;
    }
  }
  return nullptr;
}

const Mesh::MeshBuffers *Mesh::find_buffers(bse::Flags<bse::VertexTypes::COUNT> vertex_data) const {
  for (auto &[vertex_types, buffers] : m_buffers) {
    if (vertex_data == vertex_types) {
      return &buffers;
    }
  }
  return nullptr;
}

Mesh::MeshBuffers Mesh::get_buffers(bse::Flags<bse::VertexTypes::COUNT> vertex_data) {
  bse::Flags<bse::VertexTypes::COUNT> present_data = vertex_data & m_vertex_data;

  MeshBuffers *buffers = find_buffers(present_data);

  if (!buffers) {
    auto buffers_pair = std::make_pair(m_vertex_data, MeshBuffers());

    auto &[flags, buffers] = buffers_pair;
    generate_subset_buffers(present_data, buffers);

    m_buffers.push_back(buffers_pair);

    return buffers_pair.second;
  } else {
    return *buffers;
  }
}

const Mesh::MeshBuffers Mesh::get_buffers(bse::Flags<bse::VertexTypes::COUNT> vertex_data) const {
  bse::Flags<bse::VertexTypes::COUNT> present_data = vertex_data & m_vertex_data;

  const MeshBuffers *found_buffers = find_buffers(present_data);

  if (found_buffers) {
    return *found_buffers;
  } else {
    return MeshBuffers();
  }
}

bse::Skeleton &Mesh::get_skeleton() const {
  ASSERT(m_mesh_data.skel.get()) << "Tried to get the skeleton of a mesh without a skeleton!\n";
  return *(m_mesh_data.skel.get());
}

Mesh::MeshBuffers Mesh::get_buffers_copy(bse::Flags<bse::VertexTypes::COUNT> vertex_data) const {
  // Find the MeshBuffers object containing all vertex data types
  const MeshBuffers &all_types = find_all_types_buffer();

  bse::Flags<bse::VertexTypes::COUNT> present_data = vertex_data | m_vertex_data;

  MeshBuffers new_buffers;

  gl::glGenVertexArrays(1, &new_buffers.m_vao);
  gl::glBindVertexArray(new_buffers.m_vao);

  // Bind the required buffer to register into VAO and fill in vertex attributes
  if (m_vertex_data.test(bse::VertexTypes::POSITION)) {
    if (vertex_data.test(bse::VertexTypes::POSITION)) {
      gl::glGenBuffers(1, &new_buffers.m_vbo_position);
      gl::glBindBuffer(gl::GL_ARRAY_BUFFER, new_buffers.m_vbo_position);
    } else {
      gl::glBindBuffer(gl::GL_ARRAY_BUFFER, find_all_types_buffer().m_vbo_position);
    }
    gl::glEnableVertexAttribArray(bse::VertexTypes::POSITION);
    gl::glVertexAttribPointer(bse::VertexTypes::POSITION, 3, gl::GL_FLOAT, gl::GL_FALSE,
                              3 * sizeof(gl::GLfloat), (void *)(0));
    gl::glVertexAttribDivisor(bse::VertexTypes::POSITION, 0);
  }
  if (m_vertex_data.test(bse::VertexTypes::NORMAL)) {
    if (vertex_data.test(bse::VertexTypes::NORMAL)) {
      gl::glGenBuffers(1, &new_buffers.m_vbo_normal);
      gl::glBindBuffer(gl::GL_ARRAY_BUFFER, new_buffers.m_vbo_normal);
    } else {
      gl::glBindBuffer(gl::GL_ARRAY_BUFFER, find_all_types_buffer().m_vbo_normal);
    }
    gl::glEnableVertexAttribArray(bse::VertexTypes::NORMAL);
    gl::glVertexAttribPointer(bse::VertexTypes::NORMAL, 3, gl::GL_FLOAT, gl::GL_FALSE,
                              3 * sizeof(gl::GLfloat), (void *)(0));
    gl::glVertexAttribDivisor(bse::VertexTypes::NORMAL, 0);
  }
  if (m_vertex_data.test(bse::VertexTypes::COLOR)) {
    if (vertex_data.test(bse::VertexTypes::COLOR)) {
      gl::glGenBuffers(1, &new_buffers.m_vbo_color);
      gl::glBindBuffer(gl::GL_ARRAY_BUFFER, new_buffers.m_vbo_color);
    } else {
      gl::glBindBuffer(gl::GL_ARRAY_BUFFER, find_all_types_buffer().m_vbo_color);
    }
    gl::glEnableVertexAttribArray(bse::VertexTypes::COLOR);
    gl::glVertexAttribPointer(bse::VertexTypes::COLOR, 3, gl::GL_FLOAT, gl::GL_FALSE,
                              3 * sizeof(gl::GLfloat), (void *)(0));
    gl::glVertexAttribDivisor(bse::VertexTypes::COLOR, 0);
  }
  if (m_vertex_data.test(bse::VertexTypes::UV)) {
    if (vertex_data.test(bse::VertexTypes::UV)) {
      gl::glGenBuffers(1, &new_buffers.m_vbo_uv);
      gl::glBindBuffer(gl::GL_ARRAY_BUFFER, new_buffers.m_vbo_uv);
    } else {
      gl::glBindBuffer(gl::GL_ARRAY_BUFFER, find_all_types_buffer().m_vbo_uv);
    }
    gl::glEnableVertexAttribArray(bse::VertexTypes::UV);
    gl::glVertexAttribPointer(bse::VertexTypes::UV, 2, gl::GL_FLOAT, gl::GL_FALSE,
                              2 * sizeof(gl::GLfloat), (void *)(0));
    gl::glVertexAttribDivisor(bse::VertexTypes::UV, 0);
  }

  // if (vertex_data.test(bse::VertexTypes::MATRICES)) {
  //   gl::glGenBuffers(1, &new_buffers.m_instanced_vbo_matrices);
  //   gl::glBindBuffer(gl::GL_ARRAY_BUFFER, new_buffers.m_instanced_vbo_matrices);
  // } else {
  //   gl::glBindBuffer(gl::GL_ARRAY_BUFFER, find_all_types_buffer().m_instanced_vbo_matrices);
  // }
  // gl::glEnableVertexAttribArray(bse::VertexTypes::MATRICES);
  // gl::glVertexAttribPointer(bse::VertexTypes::MATRICES, 16, gl::GL_FLOAT, gl::GL_FALSE,
  //                           16 * sizeof(gl::GLfloat), (void *)(0));
  // gl::glVertexAttribDivisor(bse::VertexTypes::MATRICES, 0);

  // Bind EBO
  gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, all_types.m_ebo);
  new_buffers.m_ebo = all_types.m_ebo;

  gl::glBindVertexArray(0);

  return new_buffers;
}

Mesh::MeshBuffers &Mesh::find_all_types_buffer() {
  // Find the MeshBuffers object containing all vertex data types
  MeshBuffers *all_types = find_buffers(m_vertex_data);

  ASSERT(all_types) << "Mesh buffers could not be found!\n";

  return *all_types;
}

const Mesh::MeshBuffers &Mesh::find_all_types_buffer() const {
  // Find the MeshBuffers object containing all vertex data types
  const MeshBuffers *all_types = find_buffers(m_vertex_data);

  ASSERT(all_types) << "Mesh buffers could not be found!\n";

  return *all_types;
}

void Mesh::generate_subset_buffers(bse::Flags<bse::VertexTypes::COUNT> vertex_data,
                                   Mesh::MeshBuffers &buffers) {
  // Find the MeshBuffers object containing all vertex data types
  const MeshBuffers &all_types = find_all_types_buffer();

  gl::glGenVertexArrays(1, &buffers.m_vao);
  gl::glBindVertexArray(buffers.m_vao);

  // Bind the required buffer to register into VAO and fill in vertex attributes
  if (vertex_data.test(bse::VertexTypes::POSITION)) {
    gl::glBindBuffer(gl::GL_ARRAY_BUFFER, all_types.m_vbo_position);
    buffers.m_vbo_position = all_types.m_vbo_position;

    gl::glEnableVertexAttribArray(bse::VertexTypes::POSITION);
    gl::glVertexAttribPointer(bse::VertexTypes::POSITION, 3, gl::GL_FLOAT, gl::GL_FALSE,
                              3 * sizeof(gl::GLfloat), (void *)(0));
    gl::glVertexAttribDivisor(bse::VertexTypes::POSITION, 0);
  }
  if (vertex_data.test(bse::VertexTypes::NORMAL)) {
    gl::glBindBuffer(gl::GL_ARRAY_BUFFER, all_types.m_vbo_normal);
    buffers.m_vbo_normal = all_types.m_vbo_normal;

    gl::glEnableVertexAttribArray(bse::VertexTypes::NORMAL);
    gl::glVertexAttribPointer(bse::VertexTypes::NORMAL, 3, gl::GL_FLOAT, gl::GL_FALSE,
                              3 * sizeof(gl::GLfloat), (void *)(0));
    gl::glVertexAttribDivisor(bse::VertexTypes::NORMAL, 0);
  }
  if (vertex_data.test(bse::VertexTypes::COLOR)) {
    gl::glBindBuffer(gl::GL_ARRAY_BUFFER, all_types.m_vbo_color);
    buffers.m_vbo_color = all_types.m_vbo_color;

    gl::glEnableVertexAttribArray(bse::VertexTypes::COLOR);
    gl::glVertexAttribPointer(bse::VertexTypes::COLOR, 3, gl::GL_FLOAT, gl::GL_FALSE,
                              3 * sizeof(gl::GLfloat), (void *)(0));
    gl::glVertexAttribDivisor(bse::VertexTypes::COLOR, 0);
  }
  if (vertex_data.test(bse::VertexTypes::UV)) {
    gl::glBindBuffer(gl::GL_ARRAY_BUFFER, all_types.m_vbo_uv);
    buffers.m_vbo_uv = all_types.m_vbo_uv;

    gl::glEnableVertexAttribArray(bse::VertexTypes::UV);
    gl::glVertexAttribPointer(bse::VertexTypes::UV, 2, gl::GL_FLOAT, gl::GL_FALSE,
                              2 * sizeof(gl::GLfloat), (void *)(0));
    gl::glVertexAttribDivisor(bse::VertexTypes::UV, 0);
  }

  // gl::glBindVertexArray(buffers.m_vao);
  // glm::mat4 matrices[100];
  // gl::glBindBuffer(gl::GL_ARRAY_BUFFER, all_types.m_instanced_vbo_matrices);
  // buffers.m_instanced_vbo_matrices = all_types.m_instanced_vbo_matrices;
  // generate_vbo(buffers.m_instanced_vbo_matrices, &matrices[0], 1600 * sizeof(gl::GLfloat));
  // if (vertex_data.test(bse::VertexTypes::MATRICES)) {
  // gl::glBindBuffer(gl::GL_ARRAY_BUFFER, buffers.m_instanced_vbo_matrices);
  // buffers.m_instanced_vbo_matrices = all_types.m_instanced_vbo_matrices;

  // gl::glEnableVertexAttribArray(bse::VertexTypes::MATRICES);
  // gl::glVertexAttribPointer(bse::VertexTypes::MATRICES, 4, gl::GL_FLOAT, gl::GL_FALSE,
  //                           16 * sizeof(gl::GLfloat), (void *)(0));
  // gl::glVertexAttribDivisor(bse::VertexTypes::MATRICES, 1);
  // gl::glEnableVertexAttribArray(bse::VertexTypes::MATRICES + 1);
  // gl::glVertexAttribPointer(bse::VertexTypes::MATRICES + 1, 4, gl::GL_FLOAT, gl::GL_FALSE,
  //                           16 * sizeof(gl::GLfloat), (void *)(16));
  // gl::glVertexAttribDivisor(bse::VertexTypes::MATRICES + 1, 1);
  // gl::glEnableVertexAttribArray(bse::VertexTypes::MATRICES + 2);
  // gl::glVertexAttribPointer(bse::VertexTypes::MATRICES + 2, 4, gl::GL_FLOAT, gl::GL_FALSE,
  //                           16 * sizeof(gl::GLfloat), (void *)(32));
  // gl::glVertexAttribDivisor(bse::VertexTypes::MATRICES + 2, 1);
  // gl::glEnableVertexAttribArray(bse::VertexTypes::MATRICES + 3);
  // gl::glVertexAttribPointer(bse::VertexTypes::MATRICES + 3, 4, gl::GL_FLOAT, gl::GL_FALSE,
  //                           16 * sizeof(gl::GLfloat), (void *)(48));
  // gl::glVertexAttribDivisor(bse::VertexTypes::MATRICES + 3, 1);
  // }

  // Bind EBO
  gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, all_types.m_ebo);
  buffers.m_ebo = all_types.m_ebo;

  // generate instanced

  // gl::glGenVertexArrays(1, &buffers.m_instanced_vao);
  // gl::glBindVertexArray(buffers.m_instanced_vao);

  // // Bind the required buffer to register into VAO and fill in vertex attributes
  // if (vertex_data.test(bse::VertexTypes::POSITION)) {
  //   gl::glBindBuffer(gl::GL_ARRAY_BUFFER, all_types.m_vbo_position);
  //   gl::glEnableVertexAttribArray(bse::VertexTypes::POSITION);
  //   gl::glVertexAttribPointer(bse::VertexTypes::POSITION, 3, gl::GL_FLOAT, gl::GL_FALSE,
  //                             3 * sizeof(gl::GLfloat), (void *)(0));
  //   gl::glVertexAttribDivisor(bse::VertexTypes::POSITION, 0);
  // }
  // if (vertex_data.test(bse::VertexTypes::NORMAL)) {
  //   gl::glBindBuffer(gl::GL_ARRAY_BUFFER, all_types.m_vbo_normal);

  //   gl::glEnableVertexAttribArray(bse::VertexTypes::NORMAL);
  //   gl::glVertexAttribPointer(bse::VertexTypes::NORMAL, 3, gl::GL_FLOAT, gl::GL_FALSE,
  //                             3 * sizeof(gl::GLfloat), (void *)(0));
  //   gl::glVertexAttribDivisor(bse::VertexTypes::NORMAL, 0);
  // }
  // if (vertex_data.test(bse::VertexTypes::COLOR)) {
  //   gl::glBindBuffer(gl::GL_ARRAY_BUFFER, all_types.m_vbo_color);

  //   gl::glEnableVertexAttribArray(bse::VertexTypes::COLOR);
  //   gl::glVertexAttribPointer(bse::VertexTypes::COLOR, 3, gl::GL_FLOAT, gl::GL_FALSE,
  //                             3 * sizeof(gl::GLfloat), (void *)(0));
  //   gl::glVertexAttribDivisor(bse::VertexTypes::COLOR, 0);
  // }
  // if (vertex_data.test(bse::VertexTypes::UV)) {
  //   gl::glBindBuffer(gl::GL_ARRAY_BUFFER, all_types.m_vbo_uv);

  //   gl::glEnableVertexAttribArray(bse::VertexTypes::UV);
  //   gl::glVertexAttribPointer(bse::VertexTypes::UV, 2, gl::GL_FLOAT, gl::GL_FALSE,
  //                             2 * sizeof(gl::GLfloat), (void *)(0));
  //   gl::glVertexAttribDivisor(bse::VertexTypes::UV, 0);
  // }

  // gl::glBindBuffer(gl::GL_ARRAY_BUFFER, all_types.m_instanced_vbo_matrices);
  // buffers.m_instanced_vbo_matrices = all_types.m_instanced_vbo_matrices;

  // gl::glEnableVertexAttribArray(bse::VertexTypes::MATRICES);
  // gl::glVertexAttribPointer(bse::VertexTypes::MATRICES, 16, gl::GL_FLOAT, gl::GL_FALSE,
  //                           16 * sizeof(gl::GLfloat), (void *)(0));
  // gl::glVertexAttribDivisor(bse::VertexTypes::MATRICES, 1);

  // // Bind EBO
  // gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, all_types.m_ebo);

  gl::glBindVertexArray(0);
}

bse::Result<Mesh> Mesh::generate_final_meshes(bse::ModelData &model_data, bse::FilePath path) {
  Mesh mesh;

  // create mesh
  ASSERT(model_data.mesh.vertex_count) << "Mesh '" << path << "' contained no vertices!\n";
  mesh = Mesh(model_data);

  return mesh;
}

size_t Mesh::get_vertex_count() const { return m_mesh_data.mesh.face_count * 3; }

const bse::ModelData &Mesh::get_model_data() const { return m_mesh_data; }

AABB Mesh::get_aabb() const { return m_aabb; }

const int Mesh::get_vertex_size(bse::Flags<bse::VertexTypes::COUNT> vertexTypes) const {
  int size = 0;
  if (vertexTypes.test(bse::VertexTypes::POSITION)) {
    size += 3;
  }
  if (vertexTypes.test(bse::VertexTypes::NORMAL)) {
    size += 3;
  }
  if (vertexTypes.test(bse::VertexTypes::COLOR)) {
    size += 3;
  }
  if (vertexTypes.test(bse::VertexTypes::UV)) {
    size += 2;
  }
  // Skeletal vertex data is not sent to GPU anymore
  // if (vertexTypes.test(bse::VertexTypes::SKELETAL)) {
  //  size += 8;
  //}
  return size;
}

const Mesh &Mesh::texture_quad() {
  static bse::ModelData model_data;
  model_data.mesh.available_vertex_data = M_TEXTURED_QUAD_VERTEX_TYPES;
  model_data.mesh.positions = M_TEXTURED_QUAD_VERTICES;
  model_data.mesh.indices = M_TEXTURED_QUAD_INDICES;
  model_data.mesh.uv = M_TEXTURED_QUAD_UVS;
  model_data.mesh.face_count = 2;
  model_data.mesh.vertex_count = 4;
  static std::unique_ptr<Mesh> mesh = nullptr;
  if (!mesh) {
    Context::run([&]() { mesh = std::make_unique<Mesh>(model_data); });
  }
  return *mesh;
}

const Mesh &Mesh::fullscreen_quad() {
  static bse::ModelData model_data;
  model_data.mesh.available_vertex_data = M_TEXTURED_QUAD_VERTEX_TYPES;
  model_data.mesh.positions = M_FULLSCREEN_QUAD_VERTICES;
  model_data.mesh.indices = M_TEXTURED_QUAD_INDICES;
  model_data.mesh.uv = M_TEXTURED_QUAD_UVS;
  model_data.mesh.face_count = 2;
  model_data.mesh.vertex_count = 4;
  static std::unique_ptr<Mesh> mesh = nullptr;
  if (!mesh) {
    Context::run([&]() { mesh = std::make_unique<Mesh>(model_data); });
  }
  return *mesh;
}

void Mesh::calculate_aabb(std::vector<float> vertices, int stride) {
  glm::vec3 max = glm::vec3(-9999);
  glm::vec3 min = glm::vec3(9999);
  for (int i = 0; i < vertices.size(); i += stride) {
    if (vertices[i] < min.x) {
      min.x = vertices[i];
    }
    if (vertices[i] > max.x) {
      max.x = vertices[i];
    }
    if (vertices[i + 1] < min.y) {
      min.y = vertices[i + 1];
    }
    if (vertices[i + 1] > max.y) {
      max.y = vertices[i + 1];
    }
    if (vertices[i + 2] < min.z) {
      min.z = vertices[i + 2];
    }
    if (vertices[i + 2] > max.z) {
      max.z = vertices[i + 2];
    }
  }
  m_aabb.size = max - min;
  m_aabb.size *= 0.5;
  m_aabb.position = (min + max);
  m_aabb.position *= 0.5;
}

std::bitset<bse::VertexTypes::COUNT> Mesh::get_vertex_data_types() const { return m_vertex_data; }

void Mesh::move_from(Mesh &&other) {
  m_aabb = other.m_aabb;

  m_aabb = other.m_aabb;
  m_vertex_data = other.m_vertex_data;

  m_buffers = std::move(other.m_buffers);
  m_mesh_data = std::move(other.m_mesh_data);
}

};  // namespace gfx
