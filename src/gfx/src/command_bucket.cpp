
#include "gfx/command_bucket.hpp"

#include "bse/allocators.hpp"
#include "bse/asset.hpp"
#include "gfx/command.hpp"
#include "gfx/command_packet.hpp"
#include "gfx/key.hpp"
#include "gfx/shader.hpp"
#include <bse/edit.hpp>

namespace gfx {

CommandBucket::CommandBucket() {}

CommandBucket::CommandBucket(bse::StackAllocator* front, bse::StackAllocator* back, int width,
                             int height) {
  m_front.stack_allocator = front;
  m_back.stack_allocator = back;
  m_width = width;
  m_height = height;
  // enable gl states
  gl::glEnable(gl::GL_CULL_FACE);
  gl::glEnable(gl::GL_DEPTH_TEST);
  gl::glCullFace(gl::GL_FRONT);
  gl::glDisable(gl::GL_BLEND);
  gl::glBlendFunc(gl::GL_SRC_ALPHA, gl::GL_ONE_MINUS_SRC_ALPHA);
  gl::glBlendFuncSeparate(gl::GL_SRC_ALPHA, gl::GL_ONE_MINUS_SRC_ALPHA, gl::GL_ONE,
                          gl::GL_ONE_MINUS_SRC_ALPHA);

  prepare_for_new_frame();
}

void CommandBucket::unbind_fbo() { /*gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);*/
}

// submit the queue for rendering
void CommandBucket::submit() {
  Key last_key;
  // gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);
  gl::glViewport(0, 0, m_width, m_height);
  glm::vec4 clear_color(0, 0, 0, 1);
  BSE_EDIT_COLOR(clear_color);
  gl::glClearColor(clear_color.r, clear_color.g, clear_color.b, clear_color.a);
  // gl::glDisable(gl::GL_BLEND);
  // gl::glClear(gl::GL_DEPTH_BUFFER_BIT);
  // gl::glDepthMask(gl::GL_TRUE);
  if (m_front.number_keys > 0) {
    gl::glUseProgram(m_front.keys[m_front.sorted[0]].shader->get_shader_handle());
    if (m_front.keys[m_front.sorted[0]].blend_type) {
      gl::glEnable(gl::GL_BLEND);
      gl::glDepthMask(gl::GL_FALSE);
    }
    last_key = m_front.keys[m_front.sorted[0]];
  }
  for (unsigned int i = 0; i < m_front.number_keys; ++i) {
    CommandPacket packet = m_front.packets[m_front.sorted[i]];
    // bind less...
    if (last_key.shader_index != m_front.keys[m_front.sorted[i]].shader_index) {
      gl::glUseProgram(m_front.keys[m_front.sorted[i]].shader->get_shader_handle());
      if (last_key.blend_type != m_front.keys[m_front.sorted[i]].blend_type) {
        if (m_front.keys[m_front.sorted[i]].blend_type) {
          gl::glEnable(gl::GL_BLEND);
          gl::glDepthMask(gl::GL_FALSE);
        }
      }
    }
    last_key = m_front.keys[m_front.sorted[i]];
    do {
      submit_packet(packet);
      packet = command_packet::load_next_command_packet(packet);
    } while (packet != nullptr);
  }
  gl::glDepthMask(gl::GL_TRUE);
}

// execute a command
void CommandBucket::submit_packet(const CommandPacket packet) {
  const commands::BackendDispatchFunction dispatch_func =
      command_packet::load_backend_dispatch_function(packet);
  const void* command = command_packet::load_command(packet);
  dispatch_func(command);
}

// sort keys
void CommandBucket::sort() {
  size_t mIndex[4][256] = {0};  // count / index matrix

  uint32_t* a = (uint32_t*)m_back.stack_allocator->allocate(m_back.number_keys * sizeof(uint32_t));
  uint32_t* b = (uint32_t*)m_back.stack_allocator->allocate(m_back.number_keys * sizeof(uint32_t));
  size_t i, j, m, n;
  uint32_t u;
  for (i = 0; i < m_back.number_keys; i++) {  // generate histograms
    u = m_back.keys[i].full_handle;
    a[i] = i;
    for (j = 0; j < 4; j++) {
      mIndex[j][(size_t)(u & 0xff)]++;
      u >>= 8;
    }
  }
  for (j = 0; j < 4; j++) {  // convert to indices
    m = 0;
    for (i = 0; i < 256; i++) {
      n = mIndex[j][i];
      mIndex[j][i] = m;
      m += n;
    }
  }
  j = 0;
  for (j = 0; j < 4; j++) {                     // radix sort
    for (i = 0; i < m_back.number_keys; i++) {  //  sort by current lsb
      u = m_back.keys[a[i]].full_handle;
      m = (size_t)(u >> (j << 3)) & 0xff;
      b[mIndex[j][m]++] = a[i];
    }
    std::swap(a, b);  //  swap ptrs
  }
  m_back.sorted = a;
}

// clear old frame
void CommandBucket::clear() { m_back.number_keys = 0; }

// clear framebuffers
void CommandBucket::prepare_for_new_frame() {
  m_back.number_keys = 0;
  m_back.keys = (Key*)m_back.stack_allocator->allocate(m_max_number_of_keys * sizeof(Key));
  m_back.packets = (void**)m_back.stack_allocator->allocate(m_max_number_of_keys * sizeof(void*));
}

unsigned int CommandBucket::get_texture(int i) { return -1; }

void CommandBucket::swap() { std::swap(m_front, m_back); }

};  // namespace gfx