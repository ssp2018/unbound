#include "agg_curves.cpp"
#include "bse/assert.hpp"
#include "bse/error.hpp"
#include "bse/file_path.hpp"
#include "bse/result.hpp"
#include <bse/file.hpp>
#include <freetype/ftoutln.h>
#include <gfx/font.hpp>
#include <gfx/texture.hpp>
template class bse::Asset<gfx::Font>;

namespace gfx {

bool Font::m_is_freetype_initialized = false;
FT_Library Font::m_library;

Font::Font(Font &&other) { move(std::forward<decltype(other)>(other)); }

Font::~Font() {
  if (m_face) {
    FT_Done_Face(m_face);
  }
}

void Font::destroy() {
  if (m_is_freetype_initialized) {
    FT_Done_FreeType(m_library);
  }
}

Font &Font::operator=(Font &&other) {
  move(std::forward<decltype(other)>(other));
  return *this;
}

bse::Result<Font> Font::load(bse::FilePath path) {
  bse::Result<std::vector<char>> data = bse::load_file_raw(path);

  if (!data) {
    return data.get_error();
  }

  if (!m_is_freetype_initialized) {
    FT_Error error = FT_Init_FreeType(&m_library);
    if (error) {
      return bse::Error() << "Failed to initialize freetype.";
    }
    m_is_freetype_initialized = true;
  }

  FT_Face face;
  FT_Error error =
      FT_New_Memory_Face(m_library, (const FT_Byte *)data->data(), data->size(), 0, &face);
  if (error == FT_Err_Unknown_File_Format) {
    return bse::Error() << "Unsupported font file format.";
  } else if (error) {
    return bse::Error() << "Failed to load font file.";
  }

  error = FT_Set_Char_Size(face, FONT_SIZE * 64, FONT_SIZE * 64, 300, 300);
  if (error) {
    return bse::Error() << "Failed to set font char size";
  }

  if (!face) {
    return bse::Error() << "Font face was null!";
  }

  std::unique_ptr<gfx::Texture> atlas;
  bse::FilePath atlas_file_path =
      path.get_directory().get_string() + "/" + path.get_name() + ".sdf";
  bse::Result<std::vector<char>> atlas_data = bse::load_file_raw(atlas_file_path);
  if (atlas_data) {
    atlas.reset(
        new gfx::Texture((gfx::Texture::Gray *)atlas_data->data(), TEXTURE_WIDTH, TEXTURE_HEIGHT));
  }

  Font font = Font(face, std::move(atlas));
  if (!atlas_data) {
    std::unique_ptr<gfx::Texture::Intermediate> store = font.get_texture().store();
    const std::vector<gfx::Texture::Gray> &pixels =
        std::get<std::vector<gfx::Texture::Gray>>(store->pixels);
    std::vector<char> raw_data((char *)pixels.data(), (char *)pixels.data() + pixels.size());
    bse::write_file(atlas_file_path, raw_data);
  }

  return std::move(font);
}

Font::Font(FT_Face face, std::unique_ptr<gfx::Texture> &&atlas)
    : m_face(face), m_atlas(std::move(atlas)) {
  initialize();
}

void Font::move(Font &&other) {
  m_face = other.m_face;
  m_atlas = std::move(other.m_atlas);
  m_metas = std::move(other.m_metas);

  other.m_face = nullptr;
}

struct Line {
  glm::dvec2 a;
  glm::dvec2 b;
};
struct Decomposition {
  // std::vector<std::vector<glm::dvec2>> loops = {{}};

  glm::vec2 previous_point;
  std::vector<Line> lines;
};

int move_to(const FT_Vector *pos, void *data) {
  Decomposition *decomposition = (Decomposition *)data;
  // decomposition->loops.push_back({{pos->x, pos->y}});

  decomposition->previous_point = {pos->x, pos->y};
  return 0;
}

int line_to(const FT_Vector *pos, void *data) {
  Decomposition *decomposition = (Decomposition *)data;
  // decomposition->loops.back().push_back({pos->x, pos->y});

  glm::vec2 point = {pos->x, pos->y};
  decomposition->lines.push_back({decomposition->previous_point, point});
  decomposition->previous_point = point;
  return 0;
}

int cubic_to(const FT_Vector *control_1, const FT_Vector *control_2, const FT_Vector *pos,
             void *data) {
  Decomposition *decomposition = (Decomposition *)data;
  // if (decomposition->loops.back().empty()) {
  //  return 0;
  //}

  // std::vector<glm::dvec2> &loop = decomposition->loops.back();
  // glm::dvec2 previous_point = loop.back();

  glm::vec2 previous_point = decomposition->previous_point;
  agg::curve4_div curve(previous_point.x, previous_point.y, control_1->x, control_1->y,
                        control_2->x, control_2->y, pos->x, pos->y);

  double x;
  double y;
  while (curve.vertex(&x, &y) != agg::path_cmd_stop) {
    // loop.push_back({x, y});

    glm::vec2 point = {x, y};
    decomposition->lines.push_back({decomposition->previous_point, point});
    decomposition->previous_point = point;
  }

  return 0;
}

int conic_to(const FT_Vector *control, const FT_Vector *pos, void *data) {
  Decomposition *decomposition = (Decomposition *)data;
  // if (decomposition->loops.back().empty()) {
  //  return 0;
  //}
  // std::vector<glm::dvec2> &loop = decomposition->loops.back();
  // glm::dvec2 previous_point = loop.back();
  glm::vec2 previous_point = decomposition->previous_point;

  agg::curve3_div curve(previous_point.x, previous_point.y, control->x, control->y, pos->x, pos->y);

  double x;
  double y;
  while (curve.vertex(&x, &y) != agg::path_cmd_stop) {
    // loop.push_back({pos->x, pos->y});

    glm::vec2 point = {x, y};
    decomposition->lines.push_back({decomposition->previous_point, point});
    decomposition->previous_point = point;
  }

  return 0;
}

double get_distance_to_line_sqrd(const glm::dvec2 &point, const Line &line) {
  double length_sqrd = glm::length2(line.a - line.b);
  if (length_sqrd < 0.00000001f) {
    return glm::length2(line.a - point);
  }

  double t = ((point.x - line.a.x) * (line.b.x - line.a.x) +
              (point.y - line.a.y) * (line.b.y - line.a.y)) /
             length_sqrd;

  if (t < 0.f) {
    return glm::length2(line.a - point);
  }
  if (t > 1.f) {
    return glm::length2(line.b - point);
  }

  glm::dvec2 projection;
  projection.x = line.a.x + t * (line.b.x - line.a.x);
  projection.y = line.a.y + t * (line.b.y - line.a.y);
  return glm::length2(projection - point);
}

bool is_intersecting_horizontal(const glm::dvec2 &point, const Line &line) {
  return (
      ((line.a.y > point.y) != (line.b.y > point.y)) &&
      (point.x < (line.b.x - line.a.x) * (point.y - line.a.y) / (line.b.y - line.a.y) + line.a.x));
}

double get_signed_edge_distance(int x, int y, const std::vector<Line> &outline) {
  double min_distance_sqrd = 9999999.f;
  glm::dvec2 point = {x, y};
  bool is_inside = false;
  for (const Line &line : outline) {
    double distance_sqrd = get_distance_to_line_sqrd(point, line);
    min_distance_sqrd = std::min(min_distance_sqrd, distance_sqrd);

    if (is_intersecting_horizontal(point, line)) {
      is_inside = !is_inside;
    }
  }

  double distance = std::sqrt(min_distance_sqrd);
  // distance = std::min(distance, 8192.f) / 8192.f;

  if (is_inside) {
    return distance;
    return 192 + (255 - 192) * distance;
  } else {
    return -distance;
    return 192 * (1.f - distance);
  }
}

const gfx::Texture &Font::get_texture() const { return *m_atlas; }

std::pair<glm::vec2, glm::vec2> Font::get_bounds(const std::string &text) const {
  std::vector<Character> characters = build_text(text);

  glm::vec2 min = {99999.f, 99999.f};
  glm::vec2 max = -min;

  for (const Character &c : characters) {
    min.x = std::min(min.x, c.top_left.x);
    min.y = std::min(min.y, c.top_left.y);
    max.x = std::max(max.x, c.top_left.x + c.size.x);
    max.y = std::max(max.y, c.top_left.y + c.size.y);
  }

  return {min, max};
}

std::vector<Font::Character> Font::build_text(const std::string &path) const {
  std::vector<Character> characters;
  characters.reserve(path.size());
  glm::vec2 offset = {0, 0};
  for (char c : path) {
    if (c < MIN_CHAR || c > MAX_CHAR) {
      c = ' ';
    }
    const CharacterMeta &meta = m_metas[c - MIN_CHAR];
    Character character{};
    character.top_left = offset;
    offset.x += meta.size.x;
    character.top_left.x -= BUFFER_SIZE;
    character.uv_top_left = meta.uv;
    character.uv_size = meta.uv_size;
    character.size = meta.size;
    character.size.x += BUFFER_SIZE * 2;
    character.top_left /= float(FONT_SIZE);
    character.size /= float(FONT_SIZE);
    characters.push_back(character);
  }

  return characters;
}

void Font::initialize() {
  std::vector<Texture::Gray> texture;
  if (!m_atlas) {
    texture.resize(TEXTURE_WIDTH * TEXTURE_HEIGHT);
  }

  glm::vec2 units_per_pixel;
  units_per_pixel.x = 300.f / 72.f;
  units_per_pixel.y = 300.f / 72.f;
  for (char character = MIN_CHAR; character <= MAX_CHAR; character++) {
    size_t index = character - MIN_CHAR;
    CharacterMeta &meta = m_metas[index];
    FT_Error error =
        FT_Load_Glyph(m_face, FT_Get_Char_Index(m_face, character), FT_LOAD_NO_HINTING);
    ASSERT(!error) << "Failed to load glyph metrics.";
    FT_GlyphSlot slot = m_face->glyph;
    meta.ascender = m_face->size->metrics.ascender >> 6;
    meta.descender = -(m_face->size->metrics.descender >> 6);
    meta.size = {float(slot->metrics.horiAdvance >> 6) / units_per_pixel.x,
                 float(meta.ascender + meta.descender) / units_per_pixel.y};

    meta.uv.x = float(index * BUFFERED_CHAR_SIZE) / float(TEXTURE_WIDTH);
    meta.uv.y = 0;
    meta.uv_size.x = float(meta.size.x + BUFFER_SIZE * 2) /
                     float(TEXTURE_WIDTH);  // meta.size.x / float(TEXTURE_WIDTH);
    meta.uv_size.y =
        float(BUFFERED_CHAR_SIZE) / float(TEXTURE_HEIGHT);  // meta.size.y / float(TEXTURE_HEIGHT);

    if (m_atlas) {
      continue;
    }

    Texture::Gray *dst = texture.data() + index * BUFFERED_CHAR_SIZE;

    // unsigned char *src = map.buffer;
    FT_Outline outline = m_face->glyph->outline;
    FT_Outline_Funcs funcs;
    funcs.delta = 0;
    funcs.shift = 0;
    funcs.move_to = move_to;
    funcs.line_to = line_to;
    funcs.cubic_to = cubic_to;
    funcs.conic_to = conic_to;

    Decomposition decomposition;
    ASSERT(m_face->glyph->format == FT_GLYPH_FORMAT_OUTLINE);
    error = FT_Outline_Decompose(&outline, &funcs, &decomposition);
    ASSERT(!error) << "Failed to decompose glyph outline.";

    std::vector<Line> &lines = decomposition.lines;
    /*std::vector<Line> lines;
    for (const std::vector<glm::dvec2> &loop : decomposition.loops) {
      if (loop.empty()) {
        continue;
      }

      for (size_t i = 0; i + 1 < loop.size(); i++) {
        lines.push_back({loop[i], loop[i + 1]});
      }

      if (loop.front() != loop.back()) {
        lines.push_back({loop.back(), loop.front()});
      }
    }*/

    for (Line &l : lines) {
      l.a.x += float(BUFFER_SIZE * 2) * units_per_pixel.x * 64.f;
      l.a.y += float(BUFFER_SIZE * 2) * units_per_pixel.y * 64.f;
      l.b.x += float(BUFFER_SIZE * 2) * units_per_pixel.x * 64.f;
      l.b.y += float(BUFFER_SIZE * 2) * units_per_pixel.y * 64.f;
    }

    for (size_t y = 0; y < BUFFERED_CHAR_SIZE; y++) {
      Texture::Gray *row = &dst[y * TEXTURE_WIDTH];
      for (size_t x = 0; x < BUFFERED_CHAR_SIZE; x++) {
        int xx = int(int(x + BUFFER_SIZE) * units_per_pixel.x) << 6;
        int yy = int(int(y + BUFFER_SIZE) * units_per_pixel.y - int(meta.descender)) << 6;
        //( * 64 - int(y * units_per_pixel.y));
        double d = get_signed_edge_distance(xx, yy, lines);
        double radius = int(16 * units_per_pixel.x) << 6;
        if (d < -radius) {
          d = -radius;
        } else if (d > radius) {
          d = radius;
        }
        d /= radius;
        if (d < 0.f) {
          row[x].g = 192 * (1.f + d);
        } else {
          row[x].g = 192 + (255 - 192) * d;
        }
      }
    }
  }

  if (!m_atlas) {
    m_atlas.reset(new Texture(texture.data(), TEXTURE_WIDTH, TEXTURE_HEIGHT));
  }
}
}  // namespace gfx