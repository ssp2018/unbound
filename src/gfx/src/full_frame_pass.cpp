#include "gfx/full_frame_pass.hpp"

#include "bse/assert.hpp"
#include "bse/asset.hpp"
#include "bse/mesh/mesh_loader.hpp"
#include "gfx/command.hpp"
#include "gfx/command_packet.hpp"
#include "gfx/shader.hpp"
namespace bse {
class StackAllocator;
}

namespace gfx {

FullFramePass::FullFramePass(bse::StackAllocator* front, bse::StackAllocator* back,
                             bse::Asset<gfx::Shader> shader, int width, int height, bool buffer) {
  m_base_front.stack_allocator = front;
  m_base_back.stack_allocator = back;
  m_height = height;
  m_width = width;

  bse::Flags<bse::VertexTypes::COUNT> types;
  types.set(bse::VertexTypes::POSITION);
  types.set(bse::VertexTypes::UV);

  m_shader = shader;
  m_has_fbo = buffer;

  if (m_has_fbo) {
    gl::glGenFramebuffers(1, &m_fbo);
    gl::glBindFramebuffer(gl::GL_DRAW_FRAMEBUFFER, m_fbo);

    // Depth texture. Slower than a depth buffer, but you can sample it later in your shader
    gl::glGenTextures(1, &m_texture);
    gl::glBindTexture(gl::GL_TEXTURE_2D, m_texture);
    gl::glTexImage2D(gl::GL_TEXTURE_2D, 0, gl::GL_RGB, m_width, m_height, 0, gl::GL_RGB,
                     gl::GL_FLOAT, 0);
    gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);
    gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR);
    gl::glFramebufferTexture2D(gl::GL_FRAMEBUFFER, gl::GL_COLOR_ATTACHMENT0, gl::GL_TEXTURE_2D,
                               m_texture, 0);

    ASSERT(glCheckFramebufferStatus(gl::GL_FRAMEBUFFER) == gl::GL_FRAMEBUFFER_COMPLETE)
        << "Creation of shadow_map framebuffer failed\n";

    /*gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);*/
  }
}
FullFramePass::~FullFramePass() {
  if (m_texture > 0) {
    gl::glDeleteTextures(1, &m_texture);
  }
  gl::glDeleteFramebuffers(1, &m_fbo);
}

void FullFramePass::swap() { std::swap(m_base_front, m_base_back); }

void FullFramePass::submit() {
  if (m_has_fbo) {
    gl::glBindFramebuffer(gl::GL_DRAW_FRAMEBUFFER, m_fbo);
  }
  gl::glViewport(0, 0, m_width, m_height);
  // gl::glClear(gl::GL_DEPTH_BUFFER_BIT);
  // bind shader
  gl::glUseProgram(m_shader->get_shader_handle());

  for (unsigned int i = 0; i < m_base_front.packets.size(); ++i) {
    CommandPacket packet = m_base_front.packets[i];
    do {
      submit_packet(packet);
      packet = command_packet::load_next_command_packet(packet);
    } while (packet != nullptr);
  }

  // render quad
  VertexFlags flags;
  flags.set(bse::VertexTypes::POSITION);
  flags.set(bse::VertexTypes::UV);
  gl::glBindVertexArray(Mesh::fullscreen_quad().get_buffers(flags).m_vao);

  // Draw the triangles !
  glDrawElements(gl::GL_TRIANGLES,                                         // mode
                 (gl::GLsizei)Mesh::fullscreen_quad().get_vertex_count(),  // count
                 gl::GL_UNSIGNED_INT,                                      // type
                 (void*)0  // element array buffer offset
  );

  /*gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);*/
}

void FullFramePass::submit_packet(const CommandPacket packet) {
  const commands::BackendDispatchFunction dispatch_func =
      command_packet::load_backend_dispatch_function(packet);
  const void* command = command_packet::load_command(packet);
  dispatch_func(command);
}

void FullFramePass::clear() { m_base_back.packets.clear(); }

int FullFramePass::get_texture(int index) { return m_texture; }

bse::Asset<Shader> FullFramePass::get_shader() { return m_shader; }

unsigned int FullFramePass::get_fbo() { return m_fbo; }

}  // namespace gfx