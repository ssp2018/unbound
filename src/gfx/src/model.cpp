#include "gfx/model.hpp"

#include "bse/asset.hpp"
#include "bse/error.hpp"
#include "bse/file_path.hpp"
#include "bse/log.hpp"
#include "bse/mesh/skeleton.hpp"
#include "bse/result.hpp"
#include "gfx/material.hpp"
#include "gfx/mesh.hpp"
#include <bse/assert.hpp>
#include <bse/file.hpp>
#include <bse/mesh/mesh_loader.hpp>
template class bse::Asset<gfx::Model>;

namespace gfx {

Model::Model() {}
Model::Model(Model &&other) {
  m_mesh = std::move(other.m_mesh);
  m_material = other.m_material;
  m_shadow_material = other.m_shadow_material;
}
Model::~Model() {}

Model &Model::operator=(Model &&other) {
  if (&other != this) {
    m_mesh = std::move(other.m_mesh);
    m_material = other.m_material;
    m_shadow_material = other.m_shadow_material;
  }
  return *this;
}

bse::Result<Model> gfx::Model::load(bse::FilePath path) {
  // load model...
  LOG(NOTICE) << path << "\n";

  // Load material
  std::string shadow_material;
  // for each material, save a list of mesh ids
  std::string material;

  path.set_extension("skins");
  std::string skins = *bse::load_file_str(path);

  std::istringstream inputstream(skins);
  std::string line;
  while (std::getline(inputstream, line)) {
    std::istringstream iss(line);

    std::string result;
    if (std::getline(iss, result, ':')) {
      // vertex data key found, check what types it consists of
      if (result != "shadow_material") {
        while (inputstream.peek() == '-') {
          inputstream.get();
          std::getline(inputstream, line);
          if (line.back() == '\r') {
            line.pop_back();
          }
          if (line == "all") {
            // save material for mesh
            material = result;
          }
        }
      } else {
        // shadow_material found, load it
        std::getline(iss, shadow_material);
        if (shadow_material.back() == '\r') {
          shadow_material.pop_back();
        }
      }
    }
  }

  Model model;

  if (!shadow_material.empty()) {
    // Create shadow material
    model.m_shadow_material = bse::Asset<Material>("assets/material/" + shadow_material + ".mtl");
  }
  // Create material
  model.m_material = bse::Asset<Material>("assets/material/" + material + ".mtl");

  model.m_mesh = bse::Asset<Mesh>(path);

  return model;
}

const bse::ModelData &Model::get_model_data() const { return m_mesh->get_model_data(); }

bse::Asset<Material> Model::get_material() const { return m_material; }

bse::Asset<Material> Model::get_shadow_material() const { return m_shadow_material; }

void Model::set_shadow_material(bse::Asset<Material> shadow_material) {
  m_shadow_material = shadow_material;
}

void Model::set_material(bse::Asset<Material> material) { m_material = material; }

const bse::Asset<Mesh> Model::get_mesh() const { return m_mesh; }

VertexFlags Model::get_vertex_data_types() const { return m_mesh->get_vertex_data_types(); }

}  // namespace gfx