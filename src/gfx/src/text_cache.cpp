#include "gfx/text_cache.hpp"

#include "bse/asset.hpp"
#include "bse/directory_path.hpp"
#include "bse/file_path.hpp"
#include "bse/log.hpp"
#include "gfx/font.hpp"
#include "gfx/texture.hpp"
#include <gfx/context.hpp>

namespace gfx {

TextCache::TextCache(TextCache&& other) {
  //
  move(std::forward<decltype(other)>(other));
}

TextCache& TextCache::operator=(TextCache&& other) {
  //
  if (this != &other) {
    move(std::forward<decltype(other)>(other));
  }
  return *this;
}

TextCache::Entry TextCache::get_text(const Text& text) {
  Entry entry;
  bse::Asset<Font> font = get_font(text);

  if (!font.is_valid()) {
    entry.texture = 0;
    entry.vao = 0;
    return entry;
  }

  std::string key = text.font + ":" + text.text;

  const CacheEntry* cache_entry = nullptr;
  auto cache_it = m_cache.find(key);
  if (cache_it == m_cache.end()) {
    cache_entry = &(m_cache[key] = create_cache_entry(text.text, font));
  } else {
    cache_entry = &cache_it->second;
  }

  entry.vao = cache_entry->vao;
  entry.vertex_count = cache_entry->vertex_count;
  entry.texture = cache_entry->texture;
  entry.bounds = cache_entry->bounds;
  entry.top_left = cache_entry->top_left;
  return entry;
}

void TextCache::CacheEntry::destroy() {
  if (vao) {
    gl::glDeleteVertexArrays(1, &vao);
  }
  if (vbo) {
    gl::glDeleteBuffers(1, &vbo);
  }
}

TextCache::CacheEntry TextCache::create_cache_entry(const std::string& text,
                                                    const bse::Asset<Font>& font) {
  std::vector<Font::Character> glyphs = font->build_text(text);

  struct Vertex {
    glm::vec3 position;
    glm::vec2 uv;
  };

  glm::vec2 min_bounds = {99999.f, 99999.f};
  glm::vec2 max_bounds = -min_bounds;
  std::vector<Vertex> vertices(glyphs.size() * 6);
  for (size_t i = 0; i < glyphs.size(); i++) {
    Font::Character& glyph = glyphs[i];

    static const glm::vec2 directions[6] = {{0, 0}, {1, 0}, {0, 1}, {1, 0}, {1, 1}, {0, 1}};

    for (size_t j = 0; j < 6; j++) {
      Vertex& v = vertices[i * 6 + j];
      const glm::vec2& d = directions[j];
      v.position.x = glyph.top_left.x + d.x * glyph.size.x;
      v.position.z = glyph.top_left.y + (d.y - 1.f) * glyph.size.y;
      v.position.y = 0;

      v.uv.x = glyph.uv_top_left.x + d.x * glyph.uv_size.x;
      v.uv.y = glyph.uv_top_left.y + (d.y - 1.f) * glyph.uv_size.y;

      min_bounds.x = std::min(min_bounds.x, v.position.x);
      min_bounds.y = std::min(min_bounds.y, v.position.z);
      max_bounds.x = std::max(max_bounds.x, v.position.x);
      max_bounds.y = std::max(max_bounds.y, v.position.z);
    }
  }

  CacheEntry entry;
  entry.bounds = max_bounds - min_bounds;
  entry.top_left = min_bounds;

  gfx::Context::run([&entry, &vertices, &font]() {
    unsigned int vao;
    gl::glGenVertexArrays(1, &vao);
    gl::glBindVertexArray(vao);

    unsigned int vbo;
    gl::glGenBuffers(1, &vbo);

    gl::glBindBuffer(gl::GL_ARRAY_BUFFER, vbo);

    gl::glBufferData(gl::GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(),
                     gl::GL_STATIC_DRAW);

    // position
    gl::glEnableVertexAttribArray(0);
    gl::glVertexAttribPointer(0, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(Vertex), 0);

    // uv
    gl::glEnableVertexAttribArray(3);
    gl::glVertexAttribPointer(3, 2, gl::GL_FLOAT, gl::GL_FALSE, sizeof(Vertex),
                              (void*)(sizeof(Vertex::position)));

    gl::glBindVertexArray(0);

    entry.vao = vao;
    entry.vbo = vbo;
    entry.vertex_count = vertices.size();
    entry.texture = font->get_texture().get_id();
  });

  return entry;
}

bse::Asset<Font> TextCache::get_font(const Text& text) {
  constexpr std::string_view suffix = ".ttf";

  // Attempts to load the given font
  bse::Asset<gfx::Font> font(bse::ASSETS_ROOT / "font"_dp /
                             bse::FilePath(std::string(text.font) + std::string(suffix)));

  // Use fallback font if the given font was invalid
  if (!font.is_valid()) {
    constexpr std::string_view fallback_font_name = "arial";

    LOG(WARNING) << "Failed to render text '" << text.text << "' with font'" << text.font
                 << "'. Falling back to '" << fallback_font_name << "'.";

    font =
        bse::Asset<gfx::Font>(bse::ASSETS_ROOT / "font"_dp /
                              bse::FilePath(std::string(fallback_font_name) + std::string(suffix)));

    // Return nullptr if the fallback failed
    if (!font.is_valid()) {
      LOG(WARNING) << "Failed to load fallback font '" << fallback_font_name << "'.";
    }
  }

  return font;
}

void TextCache::destroy() {
  Context::run([this]() {
    for (auto& [key, entry] : m_cache) {
      entry.destroy();
    }
  });
  m_cache.clear();
}

void TextCache::move(TextCache&& other) {
  destroy();
  //
  m_cache = std::move(other.m_cache);
}

std::unique_ptr<TextCache::Intermediate> TextCache::store() const {
  //
  std::unique_ptr<TextCache::Intermediate> intermediate =
      std::make_unique<TextCache::Intermediate>();
  for (auto& [key, entry] : m_cache) {
    size_t separator_index = key.find(':');
    ASSERT(separator_index < key.size());

    Text text;
    text.font.assign(key.data(), key.data() + separator_index);
    text.text.assign(key.data() + separator_index, key.data() + key.size());
    intermediate->text.push_back(text);
  }
  return intermediate;
}

void TextCache::restore(TextCache::Intermediate& intermediate) {
  destroy();
  //
  for (const Text& text : intermediate.text) {
    get_text(text);
  }
}

}  // namespace gfx