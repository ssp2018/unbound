#include "gfx/framebuffer.hpp"

#include <bse/assert.hpp>
#include <ext/ext.hpp>

namespace gfx {

FrameBuffer::FrameBuffer() : m_fbo(0) {}

FrameBuffer::FrameBuffer(float width, float height, bool depth, bool stencil) {
  m_width = width;
  m_height = height;
  m_has_depth = depth;
  m_has_stencil = stencil;
}
FrameBuffer::~FrameBuffer() {
  if (m_textures.size() > 0) {
    gl::glDeleteTextures(m_textures.size(), &m_textures[0]);
  }
  if (m_has_depth) {
    gl::glDeleteTextures(1, &m_depth_texture);
  }
  gl::glDeleteFramebuffers(1, &m_fbo);
}

unsigned int FrameBuffer::create() {
  gl::glGenFramebuffers(1, &m_fbo);
  gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, m_fbo);

  // states????

  // enable gl states
  gl::glEnable(gl::GL_CULL_FACE);
  gl::glEnable(gl::GL_DEPTH_TEST);
  gl::glCullFace(gl::GL_BACK);
  gl::glDisable(gl::GL_BLEND);
  gl::glBlendFunc(gl::GL_SRC_ALPHA, gl::GL_ONE_MINUS_SRC_ALPHA);
  gl::glBlendFuncSeparate(gl::GL_SRC_ALPHA, gl::GL_ONE_MINUS_SRC_ALPHA, gl::GL_ONE,
                          gl::GL_ONE_MINUS_SRC_ALPHA);

  // Depth texture. Slower than a depth buffer, but you can sample it later in your shader
  if (m_has_depth) {
    gl::glGenTextures(1, &m_depth_texture);
    gl::glBindTexture(gl::GL_TEXTURE_2D, m_depth_texture);
    if (m_has_stencil) {
      gl::glTexImage2D(gl::GL_TEXTURE_2D, 0, gl::GL_DEPTH24_STENCIL8, m_width, m_height, 0,
                       gl::GL_DEPTH_STENCIL, gl::GL_UNSIGNED_INT_24_8, 0);
      gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);
      gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR);
      gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
      gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);
      gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_COMPARE_MODE,
                          gl::GL_COMPARE_REF_TO_TEXTURE);
      gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_DEPTH_TEXTURE_MODE, gl::GL_RED);
      gl::glFramebufferTexture(gl::GL_FRAMEBUFFER, gl::GL_DEPTH_STENCIL_ATTACHMENT, m_depth_texture,
                               0);
    } else {
      gl::glTexImage2D(gl::GL_TEXTURE_2D, 0, gl::GL_DEPTH_COMPONENT24, m_width, m_height, 0,
                       gl::GL_DEPTH_COMPONENT, gl::GL_FLOAT, 0);
      gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);
      gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR);
      gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
      gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);
      gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_COMPARE_MODE,
                          gl::GL_COMPARE_REF_TO_TEXTURE);
      gl::glFramebufferTexture(gl::GL_FRAMEBUFFER, gl::GL_DEPTH_ATTACHMENT, m_depth_texture, 0);
    }
  }

  std::vector<gl::GLenum> attachments;
  if (m_textures.size() > 0) {
    for (int i = 0; i < m_textures.size(); i++) {
      gl::glBindTexture(gl::GL_TEXTURE_2D, m_textures[i]);
      gl::glFramebufferTexture2D(gl::GL_FRAMEBUFFER, gl::GL_COLOR_ATTACHMENT0 + i,
                                 gl::GL_TEXTURE_2D, m_textures[i], 0);
      attachments.push_back(gl::GL_COLOR_ATTACHMENT0 + i);
    }
    gl::glDrawBuffers(attachments.size(), &attachments[0]);
  } else {
    gl::glDrawBuffer(gl::GL_DEPTH_STENCIL_ATTACHMENT);
    gl::glReadBuffer(gl::GL_DEPTH_STENCIL_ATTACHMENT);
  }

  ASSERT(glCheckFramebufferStatus(gl::GL_FRAMEBUFFER) == gl::GL_FRAMEBUFFER_COMPLETE)
      << "Creation of framebuffer failed\n";

  /*gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);*/
  return m_depth_texture;
}

unsigned int FrameBuffer::add_texture(TextureType texture_flags) {
  unsigned int texture;
  gl::glGenTextures(1, &texture);

  gl::GLenum texture_type = gl::GL_RGBA;
  gl::GLenum texture_format = gl::GL_RGBA;
  switch (texture_flags) {
    case RGBA:
      texture_type = gl::GL_RGBA;
      break;
    case RGBA16F:
      texture_type = gl::GL_RGBA16F;
      break;
    case RGB16F:
      texture_type = gl::GL_RGB16F;
      texture_format = gl::GL_RGB;
      break;
    case RED:
      texture_type = gl::GL_RED;
      texture_format = gl::GL_RED;
      break;
    case RGB:
      texture_type = gl::GL_RGB;
      texture_format = gl::GL_RGB;
      break;
    default:
      texture_type = gl::GL_RGBA;
      break;
  }

  // texture_type = gl::GL_RGBA;

  // // color buffer
  gl::glBindTexture(gl::GL_TEXTURE_2D, texture);
  gl::glTexImage2D(gl::GL_TEXTURE_2D, 0, texture_type, m_width, m_height, 0, texture_format,
                   gl::GL_FLOAT, NULL);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);

  m_textures.push_back(texture);
  gl::glBindTexture(gl::GL_TEXTURE_2D, 0);

  // return texture;
  return texture;
}

void FrameBuffer::bind() { gl::glBindFramebuffer(gl::GL_DRAW_FRAMEBUFFER, m_fbo); }

void FrameBuffer::unbind() { /*gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);*/
}

unsigned int FrameBuffer::get_id() { return m_fbo; }

float FrameBuffer::get_width() { return m_width; }

float FrameBuffer::get_height() { return m_height; }

}  // namespace gfx