#include "gfx/helper.hpp"

#define GLM_ENABLE_EXPERIMENTAL
#include <ext/ext.hpp>
#if defined(WIN32)
#include <initguid.h>
#include <psapi.h>
#include <windows.h>
//
#include <d3d11_4.h>
#include <dxgi1_6.h>
#endif

#include <bse/edit.hpp>
// #include <string>

namespace gfx {

ShadowFrustumDetails fit_shadow_frustum_detailed(const std::vector<glm::vec3>& points,
                                                 const glm::vec3& light_direction) {
  ShadowFrustumDetails details;
  details.view = glm::mat4(1.f);
  details.projection = glm::mat4(1.f);
  details.position = glm::vec3(0, 0, 0);
  details.near = 0;
  details.far = 0;
  details.left = 0;
  details.right = 0;
  details.top = 0;
  details.bottom = 0;
  details.radius = 0;
  details.forward_direction = light_direction;
  details.right_direction = glm::normalize(glm::cross(light_direction, glm::vec3(0, 1, 0)));
  details.up_direction = glm::normalize(glm::cross(details.right_direction, light_direction));
  if (points.empty()) {
    return details;
  }

  float furthest_point = glm::dot(light_direction, points[0]);
  float closest_point = furthest_point;

  float right_point = glm::dot(details.right_direction, points[0]);
  float left_point = right_point;

  float top_point = glm::dot(details.up_direction, points[0]);
  float bottom_point = top_point;

  glm::vec3 closest = points[0];
  glm::vec3 furthest = closest;
  glm::vec3 right = closest;
  glm::vec3 left = closest;
  glm::vec3 top = closest;
  glm::vec3 bottom = closest;

  for (int i = 1; i < points.size(); i++) {
    float p = glm::dot(light_direction, points[i]);
    if (p > furthest_point) {
      furthest_point = p;
      furthest = points[i];
    }
    if (p < closest_point) {
      closest_point = p;
      closest = points[i];
    }
    p = glm::dot(details.right_direction, points[i]);
    if (p > right_point) {
      right_point = p;
      right = points[i];
    }
    if (p < left_point) {
      left_point = p;
      left = points[i];
    }
    p = glm::dot(details.up_direction, points[i]);
    if (p > top_point) {
      top_point = p;
      top = points[i];
    }
    if (p < bottom_point) {
      bottom_point = p;
      bottom = points[i];
    }
  }

  float width = glm::abs(right_point - left_point) / 2.0f;
  glm::vec3 middle_right_left = (right + left) / 2.0f;
  float height = glm::abs(top_point - bottom_point) / 2.0f;
  glm::vec3 middle_top_bottom = (top + bottom) / 2.0f;

  // calculate view matrix
  float d[3];
  d[0] = glm::dot(light_direction, closest);
  d[1] = glm::dot(details.right_direction, middle_right_left);
  d[2] = glm::dot(details.up_direction, middle_top_bottom);
  float far_light_plane = glm::abs(furthest_point - closest_point);
  glm::mat3 equations =
      glm::mat3(light_direction.x, details.right_direction.x, details.up_direction.x,
                light_direction.y, details.right_direction.y, details.up_direction.y,
                light_direction.z, details.right_direction.z, details.up_direction.z);
  glm::mat3 solution = glm::inverse(equations);
  details.position = solution * glm::vec3(d[0], d[1], d[2]);
  details.view =
      glm::lookAt(details.position, details.position + light_direction, glm::vec3(0, 1, 0));

  details.left = -width;
  details.right = width;
  details.top = height;
  details.bottom = -height;
  details.near = 0.f;
  details.far = far_light_plane;
  details.radius = -1.f;
  for (size_t i = 0; i < points.size(); i++) {
    for (size_t j = i + 1; j < points.size(); j++) {
      float d_sqrd = glm::distance2(points[i], points[j]);
      details.radius = std::max(details.radius, d_sqrd);
    }
  }
  details.radius = std::sqrt(details.radius) / 2.f;
  details.projection = glm::ortho(-width, width, -height, height, 0.f, far_light_plane);

  return details;
}

glm::mat4 fit_shadow_frustum(const std::vector<glm::vec3>& points,
                             const glm::vec3& light_direction) {
  ShadowFrustumDetails details = fit_shadow_frustum_detailed(points, light_direction);
  return details.projection * details.view;
}

ShadowFrustumDetails calculate_shadow_frustum_detailed(float near, float far,
                                                       glm::mat4 cam_projection_matrix,
                                                       glm::mat4 cam_view_matrix,
                                                       glm::vec3 light_direction) {
  // TODO imporve this function. Simplify it and comment it. Fix naming standard
  // LOG(NOTICE) << "sd\n";

  glm::mat4 cam_view_projection_matrix = cam_projection_matrix * cam_view_matrix;

  light_direction = glm::normalize(light_direction);

  glm::mat4 inverse_vp_matrix = glm::inverse(cam_view_projection_matrix);

  glm::vec4 depth_vector = cam_projection_matrix * glm::vec4(0, 0, -far, 1);
  depth_vector = depth_vector / depth_vector.w;
  float far_plane = depth_vector.z;

  depth_vector = cam_projection_matrix * glm::vec4(0, 0, -near, 1);
  depth_vector = depth_vector / depth_vector.w;
  float near_plane = depth_vector.z;

  glm::vec3 camera_corners[8];
  glm::vec4 vp_points[8];
  vp_points[0] = inverse_vp_matrix * glm::vec4(-1, -1, near_plane, 1);
  vp_points[1] = inverse_vp_matrix * glm::vec4(1, -1, near_plane, 1);
  vp_points[2] = inverse_vp_matrix * glm::vec4(-1, 1, near_plane, 1);
  vp_points[3] = inverse_vp_matrix * glm::vec4(1, 1, near_plane, 1);
  vp_points[4] = inverse_vp_matrix * glm::vec4(-1, -1, far_plane, 1);
  vp_points[5] = inverse_vp_matrix * glm::vec4(1, -1, far_plane, 1);
  vp_points[6] = inverse_vp_matrix * glm::vec4(-1, 1, far_plane, 1);
  vp_points[7] = inverse_vp_matrix * glm::vec4(1, 1, far_plane, 1);

  for (size_t i = 0; i < 8; i++) {
    camera_corners[i] = glm::vec3(vp_points[i] / vp_points[i].w);
  }

  return fit_shadow_frustum_detailed(
      std::vector<glm::vec3>(&camera_corners[0], &camera_corners[0] + 8), light_direction);
}

glm::mat4 calcuate_shadow_frustum(float near, float far, glm::mat4 cam_projection_matrix,
                                  glm::mat4 cam_view_matrix, glm::vec3 light_direction) {
  ShadowFrustumDetails details = calculate_shadow_frustum_detailed(
      near, far, cam_projection_matrix, cam_view_matrix, light_direction);
  return details.projection * details.view;
}

float get_vram_usage() {
#if defined(WIN32)
  float res = -1;
  IDXGIFactory* dxgifactory = nullptr;
  HRESULT ret_code =
      ::CreateDXGIFactory(__uuidof(IDXGIFactory), reinterpret_cast<void**>(&dxgifactory));

  if (SUCCEEDED(ret_code)) {
    IDXGIAdapter* dxgiAdapter = nullptr;

    if (SUCCEEDED(dxgifactory->EnumAdapters(0, &dxgiAdapter))) {
      IDXGIAdapter4* dxgiAdapter4 = NULL;
      if (SUCCEEDED(dxgiAdapter->QueryInterface(__uuidof(IDXGIAdapter4), (void**)&dxgiAdapter4))) {
        DXGI_QUERY_VIDEO_MEMORY_INFO info;

        if (SUCCEEDED(
                dxgiAdapter4->QueryVideoMemoryInfo(0, DXGI_MEMORY_SEGMENT_GROUP_LOCAL, &info))) {
          res = float(info.CurrentUsage / 1024.0 / 1024.0);  // MiB

          // char msg[100];
          // sprintf_s(msg, "%.2f MiB used", memoryUsage);
          // MessageBoxA(0, msg, "VRAM", 0);
          // res = std::to_string(memoryUsage) + "MiB";
        };

        dxgiAdapter4->Release();
      }
      dxgiAdapter->Release();
    }
    dxgifactory->Release();
  }
  return res;
#else

  return 0.f;
#endif
}

float get_ram_usage() {
#if defined(WIN32)
  float res = -1;
  // src:
  //
  // https://docs.microsoft.com/en-us/windows/desktop/api/psapi/ns-psapi-_process_memory_counters

  DWORD currentProcessID = GetCurrentProcessId();

  HANDLE hProcess =
      OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, currentProcessID);

  if (NULL == hProcess) return -1;

  PROCESS_MEMORY_COUNTERS pmc{};
  if (GetProcessMemoryInfo(hProcess, &pmc, sizeof(pmc))) {
    // PagefileUsage is the:
    // The Commit Charge value in bytes for this process.
    // Commit Charge is the total amount of memory that the memory manager has committed for a
    // running process.

    res = float(pmc.PagefileUsage / 1024.0 / 1024.0);  // MiB
  }

  CloseHandle(hProcess);
  return res - get_vram_usage();
#else
  return 0.f;
#endif
}

}  // namespace gfx
