#include "gfx/uniform_buffer.hpp"

#include <ext/ext.hpp>

namespace gfx {

UniformBuffer::UniformBuffer() {}

UniformBuffer::UniformBuffer(UniformBuffer &&other) {
  m_ubo = other.m_ubo;
  other.m_ubo = -1;
}

UniformBuffer &UniformBuffer::operator=(UniformBuffer &&other) {
  if (&other != this) {
    m_ubo = other.m_ubo;
    other.m_ubo = -1;
  }
  return *this;
}

UniformBuffer::~UniformBuffer() {
  if (m_ubo != -1) {
    gl::glDeleteBuffers(1, &m_ubo);
  }
}

void UniformBuffer::create(size_t size, void *data) {
  gl::glGenBuffers(1, &m_ubo);
  gl::glBindBuffer(gl::GL_UNIFORM_BUFFER, m_ubo);
  gl::glBufferData(gl::GL_UNIFORM_BUFFER, size, data, gl::GL_DYNAMIC_DRAW);
  gl::glBindBuffer(gl::GL_UNIFORM_BUFFER, 0);
}
void UniformBuffer::update(size_t size, void *data) {
  // gl::glBindBuffer(gl::GL_UNIFORM_BUFFER, m_ubo);
  // gl::GLvoid *p = gl::glMapBuffer(gl::GL_UNIFORM_BUFFER, gl::GL_WRITE_ONLY);
  // memcpy(p, &data, size);
  // gl::glUnmapBuffer(gl::GL_UNIFORM_BUFFER);
  gl::glBindBuffer(gl::GL_UNIFORM_BUFFER, m_ubo);
  gl::glBufferSubData(gl::GL_UNIFORM_BUFFER, 0, size, data);
  // gl::glBufferData(gl::GL_UNIFORM_BUFFER, size, data, gl::GL_DYNAMIC_DRAW);
  gl::glBindBuffer(gl::GL_UNIFORM_BUFFER, 0);
}

void UniformBuffer::bind(int index) { gl::glBindBufferBase(gl::GL_UNIFORM_BUFFER, index, m_ubo); }
void UniformBuffer::unbind(int index) {}

unsigned int UniformBuffer::get_handle() { return m_ubo; }
}  // namespace gfx