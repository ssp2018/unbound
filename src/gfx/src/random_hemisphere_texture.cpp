#include "gfx/random_hemisphere_texture.hpp"

namespace gfx {

RandomHemisphereTexture::RandomHemisphereTexture() {
  std::uniform_real_distribution<float> randomFloats(0.0, 1.0);  // random floats between 0.0-1.0
  std::default_random_engine generator;

  std::vector<glm::vec3> ssaoNoise;
  for (unsigned int i = 0; i < 16; i++) {
    glm::vec3 noise(randomFloats(generator) * 2.0 - 1.0, randomFloats(generator) * 2.0 - 1.0, 0.0f);
    ssaoNoise.push_back(noise);
  }

  gl::glGenTextures(1, &m_id);
  gl::glBindTexture(gl::GL_TEXTURE_2D, m_id);
  // gl::glTexImage2D((gl::GLenum)GL_TEXTURE_2D, 0, (gl::GLenum)GL_RGBA, width, height, 0,
  //                  (gl::GLenum)GL_RGBA, (gl::GLenum)GL_UNSIGNED_BYTE, pixels);
  gl::glTexImage2D(gl::GL_TEXTURE_2D, 0, gl::GL_RGB16F, 4, 4, 0, gl::GL_RGB, gl::GL_FLOAT,
                   &ssaoNoise[0]);
  glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MIN_FILTER, gl::GL_NEAREST);
  glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MAG_FILTER, gl::GL_NEAREST);
  glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_S, gl::GL_REPEAT);
  glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_T, gl::GL_REPEAT);
  // debug texture...
}

RandomHemisphereTexture::RandomHemisphereTexture(RandomHemisphereTexture &&other) {
  move(std::forward<decltype(other)>(other));
}

RandomHemisphereTexture::~RandomHemisphereTexture() {
  if (m_id) {
    gl::glDeleteTextures(1, &m_id);
  }
}

RandomHemisphereTexture &RandomHemisphereTexture::operator=(RandomHemisphereTexture &&other) {
  move(std::forward<decltype(other)>(other));
  return *this;
}

unsigned int RandomHemisphereTexture::get_id() const { return m_id; }

void RandomHemisphereTexture::move(RandomHemisphereTexture &&other) {
  m_id = other.m_id;
  other.m_id = 0;
}

// private
float RandomHemisphereTexture::lerp(float a, float b, float f) { return a + f * (b - a); }

};  // namespace gfx
