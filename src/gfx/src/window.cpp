#include "gfx/window.hpp"

#include <bse/edit.hpp>
#include <gfx/context.hpp>
struct ImDrawData;

#define GLFW_INCLUDE_NONE
#include <ext/ext.hpp>

namespace gfx {

namespace gl = gl44;

void glfw_error_callback(int error, const char *description) {
  // LOG(FATAL) << "GLFW Error " << error << ": " << description;
}

Window::Window(int width, int height, std::string title, bool fullscreen)
    : m_width(width), m_height(height), m_title(title), m_fullscreen(fullscreen) {
  // --------- creation of window --------- //

  /* Initialize the library */
  if (!glfwInit()) {
    ASSERT(false);
    return;
  }

  glfwSetErrorCallback(glfw_error_callback);

  /* Create a windowed mode window and its OpenGL context */
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);  // changed to accommodate "older" systems
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef UNBOUND_PROFILE
  bool is_fullscreen = false;  // true;
#else
  bool is_fullscreen = true;
#endif
  m_window = glfwCreateWindow(m_width, m_height, m_title.c_str(),
                              m_fullscreen ? glfwGetPrimaryMonitor() : NULL, NULL);
  if (!m_window) {
    ASSERT(false);
    glfwTerminate();
    return;
  }

  Context::initialize(m_window);
}

Window::~Window() {
  // bse::Edit::destroy();
  glfwTerminate();
  glfwDestroyWindow(m_window);
}

void Window::run_loop() {
  //   /* Swap front and back buffers */
  // gl::GLsync sync = gl::glFenceSync(gl::GL_SYNC_GPU_COMMANDS_COMPLETE, (gl::UnusedMask)0);

  // gl::glFlush();
  // glfwSwapBuffers(m_window);
  // gl::glFlush();
  // glfwPollEvents();
  // m_dt += 1.f / 3000.f;
}  // namespace gfx
bool Window::should_run() { return !glfwWindowShouldClose(m_window); }

bool Window::has_swapped() {
  if (sync_state == -1) {
    // gl::glFlush();
    // m_sync = gl::glFenceSync(gl::GL_SYNC_GPU_COMMANDS_COMPLETE, (gl::UnusedMask)0);
    // gl::glFlush();
    sync_state = 0;
    // return true;
  } else if (sync_state == 0) {
    m_sync = gl::glFenceSync(gl::GL_SYNC_GPU_COMMANDS_COMPLETE, (gl::UnusedMask)0);
    gl::glFlush();
    sync_state = 1;
  }
  gl::GLenum res = gl::glClientWaitSync(m_sync, (gl::SyncObjectMask)0, 100);
  bool return_value = false;
  if (res == gl::GL_CONDITION_SATISFIED || res == gl::GL_ALREADY_SIGNALED) {  // 100ms
    // gl::glBlitNamedFramebuffer(result, 0, 0, 0, 1920, 1080, 0, 0, 1920, 1080,
    //                            gl::GL_COLOR_BUFFER_BIT, gl::GL_NEAREST);
    // gl::glFinish();
    glfwSwapBuffers(m_window);
    return_value = true;

    glDeleteSync(m_sync);
    m_sync = 0;
    sync_state = -1;
  }
  return return_value;
}

void Window::close() { glfwSetWindowShouldClose(m_window, GLFW_TRUE); }

bool Window::can_swap() {
  if (sync_state == -1) {
    // gl::glFlush();
    // m_sync = gl::glFenceSync(gl::GL_SYNC_GPU_COMMANDS_COMPLETE, (gl::UnusedMask)0);
    // gl::glFlush();
    sync_state = 0;
    // return true;
  } else if (sync_state == 0) {
    glDeleteSync(m_sync);
    m_sync = gl::glFenceSync(gl::GL_SYNC_GPU_COMMANDS_COMPLETE, (gl::UnusedMask)0);
    gl::glFlush();
    sync_state = 1;
  }
  gl::GLenum res = gl::glClientWaitSync(m_sync, (gl::SyncObjectMask)0, 100);
  bool return_value = false;
  if (res == gl::GL_CONDITION_SATISFIED || res == gl::GL_ALREADY_SIGNALED) {  // 100ms
    // gl::glBlitNamedFramebuffer(result, 0, 0, 0, 1920, 1080, 0, 0, 1920, 1080,
    //                            gl::GL_COLOR_BUFFER_BIT, gl::GL_NEAREST);
    // gl::glFinish();
    // glfwSwapBuffers(m_window);
    return_value = true;
    sync_state = -1;
  }
  return return_value;
}

void Window::swap() {
  glfwSwapInterval(0);

  glfwPollEvents();
  bse::Edit::new_frame();
  bse::Edit::draw();
  gfx::Context::run_async([this]() {
    std::chrono::time_point<std::chrono::steady_clock> pre_swap = std::chrono::steady_clock::now();

    glfwSwapBuffers(m_window);
    std::chrono::time_point<std::chrono::steady_clock> post_swap = std::chrono::steady_clock::now();

    float swap_dt =
        static_cast<float>(
            std::chrono::duration_cast<std::chrono::milliseconds>(post_swap - pre_swap).count()) /
        1000.f;
    float dt = static_cast<float>(std::chrono::duration_cast<std::chrono::milliseconds>(
                                      post_swap - m_last_swap_time)
                                      .count()) /
               1000.f;
    m_last_swap_time = post_swap;
    if (dt > 0.00001f) {
      m_fps = m_fps * 0.95f + (1.f / dt) * 0.05f;
      m_swap_percent = m_swap_percent * 0.95f + (swap_dt / dt) * 0.05f;
    }
  });
}

float Window::get_fps() const { return m_fps; }

float Window::get_swap_time_percent() const { return m_swap_percent; }

GLFWwindow *Window::get_window() { return m_window; }

int Window::get_width() const {
  return m_width;  //
}

int Window::get_height() const {
  return m_height;  //
}

bool Window::is_visible() const { return glfwGetWindowAttrib(m_window, GLFW_ICONIFIED) == 0; }

void Window::set_title(const std::string &title) const {
  glfwSetWindowTitle(m_window, title.c_str());
}

bool Window::is_fullscreen() { return m_fullscreen; }

}  // namespace gfx