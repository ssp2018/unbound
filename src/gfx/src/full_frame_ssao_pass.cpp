
#include "gfx/full_frame_ssao_pass.hpp"

#include "bse/assert.hpp"
#include "bse/asset.hpp"
#include "gfx/command_packet.hpp"
#include "gfx/mesh.hpp"
#include "gfx/shader.hpp"
namespace bse {
class StackAllocator;
}

namespace gfx {

float lerp(float a, float b, float f) { return a + f * (b - a); }

FullFrameSSAOPass::FullFrameSSAOPass(bse::StackAllocator* front, bse::StackAllocator* back,
                                     bse::Asset<gfx::Shader> shader, int width, int height)
    : FullFramePass(front, back, shader, width, height) {
  // generate kernel samples
  std::uniform_real_distribution<float> randomFloats(0.0, 1.0);  // random floats between 0.0 - 1.0
  std::default_random_engine generator;
  for (unsigned int i = 0; i < 64; ++i) {
    glm::vec3 sample(randomFloats(generator) * 2.0 - 1.0, randomFloats(generator) * 2.0 - 1.0,
                     randomFloats(generator));
    sample = glm::normalize(sample);
    sample *= randomFloats(generator);
    float scale = (float)i / 64.0;
    scale = lerp(0.1f, 1.0f, scale * scale);
    sample *= scale;
    m_ssao_kernel.push_back(sample);
  }

  // setup framebuffer
  gl::glGenFramebuffers(1, &m_fbo);
  gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, m_fbo);

  // Depth texture. Slower than a depth buffer, but you can sample it later in your shader
  gl::glGenTextures(1, &m_ssao_texture);
  gl::glBindTexture(gl::GL_TEXTURE_2D, m_ssao_texture);
  gl::glTexImage2D(gl::GL_TEXTURE_2D, 0, gl::GL_RGB, m_width, m_height, 0, gl::GL_RGB, gl::GL_FLOAT,
                   0);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MAG_FILTER, gl::GL_NEAREST);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MIN_FILTER, gl::GL_NEAREST);
  // gl::glTexParameteri((gl::GLenum)GL_TEXTURE_2D, (gl::GLenum)GL_TEXTURE_WRAP_S,
  //                     gl::GL_CLAMP_TO_EDGE);
  // gl::glTexParameteri((gl::GLenum)GL_TEXTURE_2D, (gl::GLenum)GL_TEXTURE_WRAP_T,
  //                     gl::GL_CLAMP_TO_EDGE);
  // gl::glTexParameteri((gl::GLenum)GL_TEXTURE_2D, gl::GL_TEXTURE_COMPARE_MODE,
  //                     gl::GL_COMPARE_REF_TO_TEXTURE);

  // gl::glFramebufferTexture(gl::GL_FRAMEBUFFER, gl::GL_DEPTH_ATTACHMENT, m_ssao_texture, 0);
  gl::glFramebufferTexture2D(gl::GL_FRAMEBUFFER, gl::GL_COLOR_ATTACHMENT0, gl::GL_TEXTURE_2D,
                             m_ssao_texture, 0);

  // glDrawBuffer(GL_NONE);
  // glReadBuffer(GL_NONE);

  ASSERT(glCheckFramebufferStatus(gl::GL_FRAMEBUFFER) == gl::GL_FRAMEBUFFER_COMPLETE)
      << "Creation of shadow_map framebuffer failed\n";

  /*gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);*/
}

void FullFrameSSAOPass::swap() { FullFramePass::swap(); }

FullFrameSSAOPass::~FullFrameSSAOPass() { gl::glDeleteTextures(1, &m_ssao_texture); }

// render the pass
void FullFrameSSAOPass::submit() {
  gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, m_fbo);
  gl::glViewport(0, 0, m_width, m_height);
  gl::glClear(gl::GL_DEPTH_BUFFER_BIT);
  gl::glDisable(gl::GL_DEPTH_TEST);
  gl::glDisable(gl::GL_BLEND);
  // bind shader
  gl::glUseProgram(m_shader->get_shader_handle());

  // bind random texture at 0.
  gl::glUniform1i(m_shader->get_uniform_location("noise_sampler"), 4);
  gl::glActiveTexture(gl::GL_TEXTURE0 + 4);
  gl::glBindTexture(gl::GL_TEXTURE_2D, m_noise_texture.get_id());

  gl::glUniform3fv(m_shader->get_uniform_location("samples"), m_ssao_kernel.size(),
                   &m_ssao_kernel[0][0]);

  for (unsigned int i = 0; i < m_base_front.packets.size(); ++i) {
    CommandPacket packet = m_base_front.packets[i];
    do {
      submit_packet(packet);
      packet = command_packet::load_next_command_packet(packet);
    } while (packet != nullptr);
  }

  // render quad    gfx::VertexFlags flags;
  VertexFlags flags;
  flags.set(bse::VertexTypes::POSITION);
  flags.set(bse::VertexTypes::UV);
  gl::glBindVertexArray(Mesh::fullscreen_quad().get_buffers(flags).m_vao);

  // Draw the triangles !
  glDrawElements(gl::GL_TRIANGLES,                                         // mode
                 (gl::GLsizei)Mesh::fullscreen_quad().get_vertex_count(),  // count
                 gl::GL_UNSIGNED_INT,                                      // type
                 (void*)0  // element array buffer offset
  );

  gl::glEnable(gl::GL_DEPTH_TEST);
  gl::glEnable(gl::GL_BLEND);
  /*gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);*/
}

int FullFrameSSAOPass::get_texture(int index) { return m_ssao_texture; }

}  // namespace gfx