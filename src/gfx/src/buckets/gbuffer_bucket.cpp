#include "gfx/buckets/gbuffer_bucket.hpp"

#include "bse/assert.hpp"
#include "bse/asset.hpp"
#include "gfx/command_packet.hpp"
#include "gfx/key.hpp"
#include "gfx/shader.hpp"
#include <bse/edit.hpp>
namespace bse {
class StackAllocator;
}

namespace gfx {

GBufferBucket::GBufferBucket(bse::StackAllocator* front, bse::StackAllocator* back, int width,
                             int height, bool color_only) {
  m_front.stack_allocator = front;
  m_back.stack_allocator = back;
  m_width = width;
  m_height = height;
}

GBufferBucket::~GBufferBucket() {}

void GBufferBucket::set_size(size_t width, size_t height) {
  m_width = width;
  m_height = height;
}

// submit the queue for rendering
void GBufferBucket::submit() {
  gl::GLboolean masks[4];
  gl::glGetBooleanv(gl::GL_COLOR_WRITEMASK, masks);
  bool is_depth_only = !masks[0] && !masks[1] && !masks[2] && !masks[3];

  Key last_key;

  gl::glViewport(0, 0, m_width, m_height);
  // gl::glClear(gl::GL_DEPTH_BUFFER_BIT);
  static const float black[] = {1, 1, 1, 0};
  gl::glClearBufferfv(gl::GL_COLOR, 1, black);

  if (is_depth_only) {
    bse::Asset<gfx::Shader> depth_only_shader("assets/shader/depth_only.shd");
    bse::Asset<gfx::Shader> shader("assets/shader/shader.shd");
    bse::Asset<gfx::Shader> cloud("assets/shader/cloud.shd");

    if (m_front.number_keys > 0) {
      gl::glUseProgram(depth_only_shader->get_shader_handle());
    }
    for (unsigned int i = 0; i < m_front.number_keys; ++i) {
      const gfx::Key& key = m_front.keys[m_front.sorted[i]];
      // bind less...

      if (key.blend_type) {
        continue;
      }

      if (last_key.shader_index != key.shader_index) {
        if (shader.get_id() == key.shader.get_id() || cloud.get_id() == key.shader.get_id()) {
          gl::glUseProgram(depth_only_shader->get_shader_handle());
        } else {
          gl::glUseProgram(key.shader->get_shader_handle());
        }
      }
      last_key = key;
      CommandPacket packet = m_front.packets[m_front.sorted[i]];
      do {
        submit_packet(packet);
        packet = command_packet::load_next_command_packet(packet);
      } while (packet != nullptr);
    }
  } else {
    if (m_front.number_keys > 0) {
      gl::glUseProgram(m_front.keys[m_front.sorted[0]].shader->get_shader_handle());
    }
    for (unsigned int i = 0; i < m_front.number_keys; ++i) {
      CommandPacket packet = m_front.packets[m_front.sorted[i]];
      // bind less...
      if (last_key.shader_index != m_front.keys[m_front.sorted[i]].shader_index) {
        gl::glUseProgram(m_front.keys[m_front.sorted[i]].shader->get_shader_handle());
        if (last_key.blend_type != m_front.keys[m_front.sorted[i]].blend_type) {
          if (m_front.keys[m_front.sorted[i]].blend_type) {
            gl::glEnable(gl::GL_BLEND);
            gl::glDepthMask(gl::GL_FALSE);
            gl::glDepthFunc(gl::GL_LESS);
          }
        }
      }
      last_key = m_front.keys[m_front.sorted[i]];
      do {
        submit_packet(packet);
        packet = command_packet::load_next_command_packet(packet);
      } while (packet != nullptr);
    }
  }
}

}  // namespace gfx