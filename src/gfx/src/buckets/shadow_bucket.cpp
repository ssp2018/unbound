#include "gfx/buckets/shadow_bucket.hpp"

#include "bse/assert.hpp"
#include "bse/asset.hpp"
#include "gfx/command_packet.hpp"
#include "gfx/helper.hpp"
#include "gfx/key.hpp"
#include "gfx/shader.hpp"
#include <bse/edit.hpp>
REFLECT((glm::vec3), x, y, z)

namespace bse {
class StackAllocator;
}
namespace gfx {

ShadowBucket::ShadowBucket(bse::StackAllocator* front, bse::StackAllocator* back, int size) {
  m_front.stack_allocator = front;
  m_back.stack_allocator = back;

  // m_size = {size_t(size * 2), size_t(size), size_t(size), size_t(size), size_t(size) / 4};
  m_size = size;
  gl::glGenFramebuffers(1, &m_fbo);

  // Depth texture. Slower than a depth buffer, but you can sample it later in your shader
  gl::glGenTextures(1, &m_texture);
  gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, m_fbo);
  gl::glBindTexture(gl::GL_TEXTURE_2D, m_texture);
  gl::glTexImage2D(gl::GL_TEXTURE_2D, 0, gl::GL_DEPTH_COMPONENT32, m_size, m_size, 0,
                   gl::GL_DEPTH_COMPONENT, gl::GL_FLOAT, 0);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_S, gl::GL_CLAMP_TO_EDGE);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_T, gl::GL_CLAMP_TO_EDGE);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_COMPARE_MODE,
                      gl::GL_COMPARE_REF_TO_TEXTURE);
  gl::glFramebufferTexture(gl::GL_FRAMEBUFFER, gl::GL_DEPTH_ATTACHMENT, m_texture, 0);

  gl::glDrawBuffer(gl::GL_NONE);
  gl::glReadBuffer(gl::GL_NONE);
  ASSERT(gl::glCheckFramebufferStatus(gl::GL_FRAMEBUFFER) == gl::GL_FRAMEBUFFER_COMPLETE)
      << "Creation of shadow_map framebuffer failed\n";

  /* gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);*/
}

ShadowBucket::~ShadowBucket() {
  gl::glDeleteTextures(1, &m_texture);
  gl::glDeleteFramebuffers(1, &m_fbo);
}

void ShadowBucket::fit_camera(const glm::mat4& p_matrix, const glm::mat4& v_matrix,
                              const glm::vec3& direction, float near, float far) {
  glm::vec4 delta_view = glm::vec4(0, 0, 1, 1);
  delta_view = p_matrix * delta_view;
  delta_view /= delta_view.w;
  delta_view.x = 1;
  delta_view.y = 1;
  delta_view = glm::inverse(p_matrix) * delta_view;
  delta_view /= delta_view.w;
  delta_view.z = 1;
  delta_view = glm::abs(delta_view);

  ShadowFrustumDetails details =
      calculate_shadow_frustum_detailed(near, far, p_matrix, v_matrix, direction);

  float radius = details.radius;
  radius = glm::distance(glm::vec3(delta_view) * near, glm::vec3(-glm::vec2(delta_view), 1) * far);
  radius = std::max(radius, glm::distance(glm::vec3(delta_view) * far,
                                          glm::vec3(-glm::vec2(delta_view), 1) * far));
  radius /= 2.f;
  float world_per_tex = (radius * 2.f) / float(m_size);
  glm::vec2 subpixel_delta = glm::vec2(glm::dot(details.position, details.right_direction),
                                       glm::dot(details.position, details.up_direction));
  subpixel_delta -= glm::floor(subpixel_delta / world_per_tex) * world_per_tex;
  details.position -=
      subpixel_delta.x * details.right_direction + subpixel_delta.y * details.up_direction;
  glm::mat4 view = glm::lookAt(details.position, details.position + details.forward_direction,
                               glm::vec3(0, 1, 0));
  m_shadow_back.shadow_matrix =
      glm::ortho(-radius, radius, -radius, radius, details.near, details.far) * view;

  /*m_shadow_back.cull_matrix =
      glm::ortho(-radius, radius, -radius, radius, -10000.f, 10000.f) * view;*/
  m_shadow_back.cull_matrix =
      glm::ortho(details.left, details.right, details.bottom, details.top, -10000.f, 10000.f) *
      details.view;
  m_shadow_back.depth = std::abs(details.far - details.near);
}

void ShadowBucket::fit_points(const std::vector<glm::vec3>& world_rim, const glm::vec3& direction) {
  ShadowFrustumDetails details = fit_shadow_frustum_detailed(world_rim, direction);
  m_shadow_back.shadow_matrix = details.projection * details.view;
  m_shadow_back.cull_matrix =
      glm::ortho(details.left, details.right, details.bottom, details.top, -10000.f, details.far) *
      details.view;
  m_shadow_back.depth = std::abs(details.far - details.near);
}

// submit the queue for rendering
void ShadowBucket::submit() {
  gl::glCullFace(gl::GL_BACK);
  // glDisable(gl::GL_CULL_FACE);
  gl::glDepthMask(gl::GL_TRUE);
  // calculate matrix for shadowmapping

  gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, m_fbo);
  gl::glClear(gl::GL_DEPTH_BUFFER_BIT);
  gl::glViewport(0, 0, m_size, m_size);

  Key last_key;
  if (m_front.number_keys > 0) {
    last_key = m_front.keys[0];
    gl::glUseProgram(m_front.keys[0].shader->get_shader_handle());
    gl::glUniformMatrix4fv(m_front.keys[0].shader->get_uniform_location("VIEW_PROJECTION_MATRIX"),
                           1, false, &m_shadow_front.shadow_matrix[0][0]);
  }
  for (unsigned int j = 0; j < m_front.number_keys; ++j) {
    CommandPacket packet = m_front.packets[j];
    if (last_key.shader_index != m_front.keys[j].shader_index) {
      gl::glUseProgram(m_front.keys[j].shader->get_shader_handle());
      gl::glUniformMatrix4fv(m_front.keys[j].shader->get_uniform_location("VIEW_PROJECTION_MATRIX"),
                             1, false, &m_shadow_front.shadow_matrix[0][0]);
    }
    last_key = m_front.keys[j];
    do {
      submit_packet(packet);
      packet = command_packet::load_next_command_packet(packet);
    } while (packet != nullptr);
  }
  /*gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);*/
  gl::glCullFace(gl::GL_BACK);
  glEnable(gl::GL_CULL_FACE);
}

const glm::mat4& ShadowBucket::get_shadow_matrix() const { return m_shadow_back.shadow_matrix; }
const glm::mat4& ShadowBucket::get_cull_matrix() const { return m_shadow_back.cull_matrix; }

unsigned int ShadowBucket::get_texture(int) { return m_texture; }

float ShadowBucket::get_shadow_depth() const { return m_shadow_back.depth; }

void ShadowBucket::swap() {
  CommandBucket::swap();
  std::swap(m_shadow_front, m_shadow_back);
}

}  // namespace gfx