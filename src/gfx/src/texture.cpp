#include "bse/error.hpp"
#include "bse/file_path.hpp"
#include "bse/log.hpp"
#include "bse/result.hpp"
#include <bse/file.hpp>
#include <gfx/texture.hpp>
#include <png.h>
template class bse::Asset<gfx::Texture>;

namespace gfx {
class Material;
}

namespace gfx {

namespace gl = gl44;

Texture::Texture(Rgba *pixels, size_t width, size_t height) {
  gl::glGenTextures(1, &m_id);
  gl::glBindTexture(gl::GL_TEXTURE_2D, m_id);

  gl::glBindTexture(gl::GL_TEXTURE_2D, m_id);

  gl::glTexImage2D(gl::GL_TEXTURE_2D, 0, gl::GL_SRGB_ALPHA, width, height, 0, gl::GL_RGBA,
                   gl::GL_UNSIGNED_BYTE, pixels);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_S, gl::GL_REPEAT);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_T, gl::GL_REPEAT);

  m_is_grayscale = false;
}

Texture::Texture(Gray *pixels, size_t width, size_t height) {
  gl::glGenTextures(1, &m_id);
  gl::glBindTexture(gl::GL_TEXTURE_2D, m_id);

  gl::glBindTexture(gl::GL_TEXTURE_2D, m_id);

  gl::glTexImage2D(gl::GL_TEXTURE_2D, 0, gl::GL_RED, width, height, 0, gl::GL_RED,
                   gl::GL_UNSIGNED_BYTE, pixels);

  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_S, gl::GL_MIRRORED_REPEAT);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_WRAP_T, gl::GL_MIRRORED_REPEAT);

  m_is_grayscale = true;
}

Texture::Texture(Texture &&other) { move(std::forward<decltype(other)>(other)); }

Texture::~Texture() {
  if (m_id) {
    gl::glDeleteTextures(1, &m_id);
  }
}

Texture &Texture::operator=(Texture &&other) {
  if (this != &other) {
    move(std::forward<decltype(other)>(other));
  }
  return *this;
}

// Helper for PNG loading
struct Stream {
  const std::vector<char> &data;
  size_t index;
};

// Helper for PNG loading
struct Image {
  std::vector<Texture::Rgba> pixels;
  size_t width = 0;
  size_t height = 0;
};

// PNG read callback
void png_reader(png_structp png_ptr, png_bytep out_bytes, png_size_t byte_count_to_read) {
  png_voidp io_ptr = png_get_io_ptr(png_ptr);
  if (!io_ptr) {
    LOG(FATAL) << "Unexpected null pointer.";
    return;
  }

  Stream &stream = *(Stream *)io_ptr;
  size_t num_bytes_left = stream.data.size() - stream.index;
  size_t num_bytes_read = std::min(num_bytes_left, byte_count_to_read);
  memcpy(out_bytes, &stream.data[stream.index], num_bytes_read);

  stream.index += num_bytes_read;
}

// Decode PNG data to raw pixel data
bse::Result<Image> read_png_file(const std::vector<char> &data) {
  Image image;
  static constexpr size_t SIGNATURE_SIZE = 8;
  if (data.size() <= SIGNATURE_SIZE) {
    return bse::Error() << "Unexpected image data size.";
  }

  if (png_sig_cmp((unsigned char *)&data[0], 0, SIGNATURE_SIZE)) {
    return bse::Error() << "Non-PNG images are not supported.";
  }

  png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!png_ptr) {
    return bse::Error() << "Failed to create PNG read struct.";
  }

  png_infop info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr) {
    return bse::Error() << "Failed to create PNG info struct.";
  }

  Stream stream = {data, SIGNATURE_SIZE};
  png_set_read_fn(png_ptr, (void *)&stream, png_reader);
  png_set_sig_bytes(png_ptr, SIGNATURE_SIZE);

  png_read_info(png_ptr, info_ptr);
  if (png_get_color_type(png_ptr, info_ptr) != PNG_COLOR_TYPE_RGBA) {
    return bse::Error() << "Cannot load non-RGBA textures";
  }

  image.width = png_get_image_width(png_ptr, info_ptr);
  image.height = png_get_image_height(png_ptr, info_ptr);
  if (png_get_rowbytes(png_ptr, info_ptr) != sizeof(Texture::Rgba) * image.width) {
    return bse::Error() << "Unexpected row size.";
  }

  png_read_update_info(png_ptr, info_ptr);
  image.pixels.resize(image.width * image.height);

  std::vector<png_bytep> rows;
  rows.resize(image.height);
  for (size_t i = 0; i < image.height; i++) {
    rows[i] = (png_bytep)&image.pixels[i * image.width];
  }
  png_read_image(png_ptr, &rows[0]);
  png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);

  return image;
}

bse::Result<Texture> Texture::load(bse::FilePath path) {
  bse::Result<std::vector<char>> data = bse::load_file_raw(path);

  if (!data) {
    return bse::Error() << "Failed to open file";
  }

  bse::Result<Image> image = read_png_file(*data);
  if (!image) {
    return image.get_error();
  }

  return std::move(Texture(image->pixels.data(), image->width, image->height));
}

void Texture::move(Texture &&other) {
  m_id = other.m_id;
  m_is_grayscale = other.m_is_grayscale;

  other.m_id = 0;
}

void Texture::bind(const bse::Asset<Material> &material) const {
  // if (m_id) {
  //   material->bind_texture("textureSampler", m_id, 0);
  // }
}

unsigned int Texture::get_id() const { return m_id; }

std::unique_ptr<Texture::Intermediate> Texture::store() const {
  //
  auto intermediate = std::make_unique<Texture::Intermediate>();

  gl::GLint width = 0;
  gl::GLint height = 0;

  gl::glBindTexture(gl::GL_TEXTURE_2D, m_id);

  gl::glGetTexLevelParameteriv(gl::GL_TEXTURE_2D, 0, gl::GL_TEXTURE_WIDTH, &width);
  gl::glGetTexLevelParameteriv(gl::GL_TEXTURE_2D, 0, gl::GL_TEXTURE_HEIGHT, &height);

  if (m_is_grayscale) {
    std::vector<Gray> pixels;
    pixels.resize(width * height);
    gl::glGetTexImage(gl::GL_TEXTURE_2D, 0, gl::GL_RED, gl::GL_UNSIGNED_BYTE, pixels.data());
    intermediate->pixels = std::move(pixels);
    gl::glBindTexture(gl::GL_TEXTURE_2D, 0);
    return intermediate;
  } else {
    std::vector<Rgba> pixels;
    pixels.resize(width * height);
    gl::glGetTexImage(gl::GL_TEXTURE_2D, 0, gl::GL_RGBA, gl::GL_UNSIGNED_BYTE, pixels.data());
    intermediate->pixels = std::move(pixels);
    gl::glBindTexture(gl::GL_TEXTURE_2D, 0);
    return intermediate;
  }
}

void Texture::restore(Intermediate &intermediate) {
  //
  if (auto *rgba_vec = std::get_if<std::vector<Rgba>>(&intermediate.pixels)) {
    *this = std::move(Texture(rgba_vec->data(), intermediate.width, intermediate.height));
  }

  auto &gray_vec = std::get<std::vector<Gray>>(intermediate.pixels);
  *this = std::move(Texture(gray_vec.data(), intermediate.width, intermediate.height));
}

}  // namespace gfx
