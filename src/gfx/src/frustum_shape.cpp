
#include "gfx/frustum_shape.hpp"

#include "gfx/mesh.hpp"

namespace gfx {

PlaneShape::PlaneShape(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3) {
  glm::vec3 aux1, aux2;

  aux1 = v1 - v2;
  aux2 = v3 - v2;

  m_normal = glm::cross(aux2, aux1);

  m_normal = glm::normalize(m_normal);
  // point.copy(v2);
  m_point = v2;
  // d = -(normal.innerProduct(point));
  m_d = -glm::dot(m_normal, m_point);
}
PlaneShape::~PlaneShape() {}

float PlaneShape::distance(const glm::vec3 &p) const { return (m_d + glm::dot(m_normal, p)); }

FrustumShape::FrustumShape() {}

FrustumShape::FrustumShape(glm::mat4 vp_matrix) {
  glm::mat4 inverse_vp_matrix = glm::inverse(vp_matrix);

  glm::vec3 camera_corners[8];
  glm::vec4 vp_points[8];
  vp_points[0] = inverse_vp_matrix * glm::vec4(-1, -1, -1, 1);
  vp_points[1] = inverse_vp_matrix * glm::vec4(1, -1, -1, 1);
  vp_points[2] = inverse_vp_matrix * glm::vec4(-1, 1, -1, 1);
  vp_points[3] = inverse_vp_matrix * glm::vec4(1, 1, -1, 1);
  vp_points[4] = inverse_vp_matrix * glm::vec4(-1, -1, 1, 1);
  vp_points[5] = inverse_vp_matrix * glm::vec4(1, -1, 1, 1);
  vp_points[6] = inverse_vp_matrix * glm::vec4(-1, 1, 1, 1);
  vp_points[7] = inverse_vp_matrix * glm::vec4(1, 1, 1, 1);

  for (size_t i = 0; i < 8; i++) {
    camera_corners[i] = glm::vec3(vp_points[i] / vp_points[i].w);
  }
  // near
  m_planes[0] = PlaneShape(camera_corners[0], camera_corners[1], camera_corners[2]);
  // far
  m_planes[1] = PlaneShape(camera_corners[5], camera_corners[4], camera_corners[6]);
  // bottom
  m_planes[2] = PlaneShape(camera_corners[1], camera_corners[0], camera_corners[4]);
  // top
  m_planes[3] = PlaneShape(camera_corners[2], camera_corners[3], camera_corners[6]);

  //
  m_planes[4] = PlaneShape(camera_corners[3], camera_corners[1], camera_corners[5]);
  m_planes[5] = PlaneShape(camera_corners[0], camera_corners[2], camera_corners[6]);
}

FrustumShape::FrustumShape(glm::mat4 p_matrix, glm::mat4 v_matrix)
    : FrustumShape(p_matrix * v_matrix) {
  glm::mat4 vp_matrix = p_matrix * v_matrix;
}

bool FrustumShape::aabb_intersect(gfx::AABB aabb) const {
  int result = false;

  for (int i = 0; i < 6; i++) {
    if (m_planes[i].distance(get_vertex_p(aabb, m_planes[i].m_normal)) > 0) {
      return false;
    } else if (m_planes[i].distance(get_vertex_n(aabb, m_planes[i].m_normal)) < 0) {
      result = true;
    }
    // if (m_planes[i].distance(aabb.getVertexP(m_planes[i].normal)) < 0)
    //   return false;
    // else if (m_planes[i].distance(aabb.getVertexN(m_planes[i].normal)) < 0)
    //   result = true;
  }
  return (result);
  // return true;
}

glm::vec3 FrustumShape::get_vertex_p(AABB aabb, glm::vec3 normal) const {
  glm::vec3 res = aabb.position + aabb.size;

  if (normal.x > 0) res.x -= aabb.size.x * 2;

  if (normal.y > 0) res.y -= aabb.size.y * 2;

  if (normal.z > 0) res.z -= aabb.size.z * 2;

  return (res);
}

glm::vec3 FrustumShape::get_vertex_n(AABB aabb, glm::vec3 normal) const {
  glm::vec3 res = aabb.position - aabb.size;

  if (normal.x < 0) res.x += aabb.size.x * 2;

  if (normal.y < 0) res.y += aabb.size.y * 2;

  if (normal.z < 0) res.z += aabb.size.z * 2;

  return (res);
}

FrustumShape::~FrustumShape() {}

}  // namespace gfx