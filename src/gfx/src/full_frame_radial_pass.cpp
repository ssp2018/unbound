
#include "gfx/full_frame_radial_pass.hpp"

#include "bse/asset.hpp"
#include "gfx/command_packet.hpp"
#include "gfx/mesh.hpp"
#include "gfx/shader.hpp"
namespace bse {
class StackAllocator;
}
namespace gfx {

FullFrameRadialPass::FullFrameRadialPass(bse::StackAllocator* front, bse::StackAllocator* back,
                                         bse::Asset<gfx::Shader> shader, int width, int height)
    : FullFramePass(front, back, shader, width, height, true) {}

FullFrameRadialPass::~FullFrameRadialPass() {}

// render the pass
void FullFrameRadialPass::submit() {
  gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, m_fbo);
  gl::glViewport(0, 0, m_width, m_height);
  gl::glClear(gl::GL_DEPTH_BUFFER_BIT);
  // bind shader
  gl::glUseProgram(m_shader->get_shader_handle());

  // bind input_texture
  gl::glUniform1i(m_shader->get_uniform_location("texture_sampler"), 0);
  gl::glActiveTexture(gl::GL_TEXTURE0);
  gl::glBindTexture(gl::GL_TEXTURE_2D, m_front.input_texture);

  gl::glUniform1f(m_shader->get_uniform_location("damage_radius"), m_front.radius);

  for (unsigned int i = 0; i < m_base_front.packets.size(); ++i) {
    CommandPacket packet = m_base_front.packets[i];
    do {
      submit_packet(packet);
      packet = command_packet::load_next_command_packet(packet);
    } while (packet != nullptr);
  }

  // render quad
  VertexFlags flags;
  flags.set(bse::VertexTypes::POSITION);
  flags.set(bse::VertexTypes::UV);
  gl::glBindVertexArray(Mesh::fullscreen_quad().get_buffers(flags).m_vao);

  // Draw the triangles !
  glDrawElements(gl::GL_TRIANGLES,                                         // mode
                 (gl::GLsizei)Mesh::fullscreen_quad().get_vertex_count(),  // count
                 gl::GL_UNSIGNED_INT,                                      // type
                 (void*)0  // element array buffer offset
  );

  /*gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);*/
}

void FullFrameRadialPass::swap() {
  FullFramePass::swap();
  std::swap(m_front, m_back);
}

// set the blur radius
void FullFrameRadialPass::set_radius(float radius) { m_back.radius = radius; }

// set the texture to blur
void FullFrameRadialPass::set_texture(unsigned int texture_id) {
  m_back.input_texture = texture_id;
}

}  // namespace gfx