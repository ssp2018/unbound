#include "gfx/full_frame_gaussian_pass.hpp"

#include "bse/assert.hpp"
#include "bse/asset.hpp"
#include "gfx/command_packet.hpp"
#include "gfx/mesh.hpp"
#include "gfx/shader.hpp"
namespace bse {
class StackAllocator;
}

namespace gfx {

FullFrameGaussianPass::FullFrameGaussianPass(bse::StackAllocator* front, bse::StackAllocator* back,
                                             bse::Asset<gfx::Shader> shader, int width, int height)
    : FullFramePass(front, back, shader, width, height, true) {
  gl::glGenFramebuffers(1, &m_internal_fbo);
  gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, m_internal_fbo);

  // Depth texture. Slower than a depth buffer, but you can sample it later in your shader
  gl::glGenTextures(1, &m_internal_texture);
  gl::glBindTexture(gl::GL_TEXTURE_2D, m_internal_texture);
  gl::glTexImage2D(gl::GL_TEXTURE_2D, 0, gl::GL_RGBA, m_width, m_height, 0, gl::GL_RGBA,
                   gl::GL_FLOAT, 0);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MAG_FILTER, gl::GL_NEAREST);
  gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MIN_FILTER, gl::GL_NEAREST);
  gl::glFramebufferTexture2D(gl::GL_FRAMEBUFFER, gl::GL_COLOR_ATTACHMENT0, gl::GL_TEXTURE_2D,
                             m_internal_texture, 0);

  ASSERT(glCheckFramebufferStatus(gl::GL_FRAMEBUFFER) == gl::GL_FRAMEBUFFER_COMPLETE)
      << "Creation of shadow_map framebuffer failed\n";

  /*gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);*/
}

FullFrameGaussianPass::~FullFrameGaussianPass() {
  gl::glDeleteTextures(1, &m_internal_texture);
  gl::glDeleteFramebuffers(1, &m_internal_fbo);
}

void FullFrameGaussianPass::swap() {
  FullFramePass::swap();
  std::swap(m_front, m_back);
}

// render the pass
void FullFrameGaussianPass::submit() {
  gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, m_internal_fbo);
  gl::glViewport(0, 0, m_width, m_height);
  gl::glClearColor(0, 0, 0, 0);
  gl::glClear(gl::GL_COLOR_BUFFER_BIT);
  // bind shader
  gl::glUseProgram(m_shader->get_shader_handle());

  // bind input_texture
  gl::glUniform1i(m_shader->get_uniform_location("texture_sampler"), 0);
  gl::glActiveTexture(gl::GL_TEXTURE0);
  gl::glBindTexture(gl::GL_TEXTURE_2D, m_front.input_texture);

  gl::glUniform1f(m_shader->get_uniform_location("radius"), m_front.radius);
  gl::glUniform2f(m_shader->get_uniform_location("direction"), 1, 0);

  for (unsigned int i = 0; i < m_base_front.packets.size(); ++i) {
    CommandPacket packet = m_base_front.packets[i];
    do {
      submit_packet(packet);
      packet = command_packet::load_next_command_packet(packet);
    } while (packet != nullptr);
  }

  // render quad
  VertexFlags flags;
  flags.set(bse::VertexTypes::POSITION);
  flags.set(bse::VertexTypes::UV);
  gl::glBindVertexArray(Mesh::fullscreen_quad().get_buffers(flags).m_vao);

  // Draw the triangles !
  glDrawElements(gl::GL_TRIANGLES,                                         // mode
                 (gl::GLsizei)Mesh::fullscreen_quad().get_vertex_count(),  // count
                 gl::GL_UNSIGNED_INT,                                      // type
                 (void*)0  // element array buffer offset
  );

  // render second pass

  gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, m_fbo);
  gl::glViewport(0, 0, m_width, m_height);
  gl::glClear(gl::GL_COLOR_BUFFER_BIT);
  // bind shader
  gl::glUseProgram(m_shader->get_shader_handle());

  // bind input_texture
  gl::glUniform1i(m_shader->get_uniform_location("texture_sampler"), 0);
  gl::glActiveTexture(gl::GL_TEXTURE0);
  gl::glBindTexture(gl::GL_TEXTURE_2D, m_internal_texture);

  gl::glUniform1f(m_shader->get_uniform_location("radius"), m_front.radius);
  gl::glUniform2f(m_shader->get_uniform_location("direction"), 0, 1);

  for (unsigned int i = 0; i < m_base_front.packets.size(); ++i) {
    CommandPacket packet = m_base_front.packets[i];
    do {
      submit_packet(packet);
      packet = command_packet::load_next_command_packet(packet);
    } while (packet != nullptr);
  }

  // render quad
  gl::glBindVertexArray(Mesh::fullscreen_quad().get_buffers(flags).m_vao);

  // Draw the triangles !
  glDrawElements(gl::GL_TRIANGLES,                                         // mode
                 (gl::GLsizei)Mesh::fullscreen_quad().get_vertex_count(),  // count
                 gl::GL_UNSIGNED_INT,                                      // type
                 (void*)0  // element array buffer offset
  );

  /*gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);*/
}

// set the blur radius
void FullFrameGaussianPass::set_radius(float radius) { m_back.radius = radius; }

// set the texture to blur
void FullFrameGaussianPass::set_texture(unsigned int texture_id) {
  m_back.input_texture = texture_id;
}

}  // namespace gfx