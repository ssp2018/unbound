#include "gfx/context.hpp"

#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include <bse/edit.hpp>
#include <bse/task_scheduler.hpp>

namespace gfx {

// void gl::GLAPIENTRY MessageCallback(gl::GLenum source, gl::GLenum type, gl::GLuint id,
//                                     gl::GLenum severity, gl::GLsizei length,
//                                     const gl::GLchar* message, const void* userParam) {
//   fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
//           (type == gl::GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""), type, severity, message);
// }

void Context::initialize(GLFWwindow* window) {
  static bool is_initialized = false;
  if (is_initialized) {
    return;
  }
  is_initialized = true;

  glbinding::Binding::initialize();
  glbinding::setCallbackMaskExcept(glbinding::CallbackMask::After, {"glGetError"});
  glbinding::setAfterCallback([](const glbinding::FunctionCall& call) {
    std::cout << call.function->name() << "(";
    for (unsigned i = 0; i < call.parameters.size(); ++i) {
      std::cout << call.parameters[i]->asString();
      if (i < call.parameters.size() - 1) std::cout << ", ";
    }
    std::cout << ")";

    if (call.returnValue) std::cout << " -> " << call.returnValue->asString();

    std::cout << std::endl;
  });

  bse::Edit::initialize(
      [](GLFWwindow* window) {
        return ImGui_ImplGlfw_InitForOpenGL(window, true) && ImGui_ImplOpenGL3_Init("#version 130");
      },
      []() {
        Context::run([]() { ImGui_ImplOpenGL3_NewFrame(); });
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
      },
      []() {
        Context::run([] {
          //
        });
        ImGui::EndFrame();
      },
      [](ImDrawData* draw_data) {
        Context::run([]() {
          ImGui::Render();
          ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        });
      },
      window);
  glfwMakeContextCurrent(nullptr);
  run([window]() {
    glfwMakeContextCurrent(window);
    glbinding::Binding::useCurrentContext();
    gl::glFrontFace(gl::GL_CCW);

    bse::Edit::new_frame();
    bse::Edit::draw();
    bse::Edit::end_frame();
  });
}
void Context::run(const std::function<void()>& func) {
  if (is_render_thread()) {
    func();
    return;
  }

  bse::TaskScheduler::get()->wait_on_task(run_async(func));
}
bse::TaskHandle Context::run_async(const std::function<void()>& func) {
  struct JustDoItTask : public bse::Task {
    virtual void kernel() override { func(); }

    std::function<void()> func;
  };

  JustDoItTask task;
  task.func = func;
  return bse::TaskScheduler::get()->add_task_to_thread(bse::TaskScheduler::Thread::RENDER, task);
}

bool Context::is_render_thread() {
  return bse::TaskScheduler::get()->get_thread_id() == (int)bse::TaskScheduler::Thread::RENDER;
}
}  // namespace gfx