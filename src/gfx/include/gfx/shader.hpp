#pragma once
#ifndef GFX_Shader_HPP
#define GFX_Shader_HPP

#include <bse/file_path.hpp>
#include <bse/result.hpp>
//#include <ext.>
#include <bse/asset.hpp>
namespace gfx {

// A dumb class for handling compiling and binding of shaders
class Shader {
 public:
  Shader();
  Shader(Shader &&other);
  Shader(const Shader &other) = delete;
  ~Shader();

  Shader &operator=(Shader &&other);
  Shader &operator=(Shader &other) = delete;

  // load shader from file. result is stoored globally.
  static bse::Result<Shader> load(bse::FilePath path);

  // get shader handle
  unsigned int get_shader_handle() const;

  // bind the shader for rendering. Does not set any uniforms
  void bind_program() const;

  // get matrix locations
  int get_uniform_location(const std::string &name) const;

  // print_all_uniforms
  void print_all_uniforms();

 private:
  // generates a shaderprogram
  int create_shader_program(std::string program, unsigned int program_stage_mask) const;
  // link the shader programs
  int generate_shader(int vertex_program, int fragment_program);

  unsigned int m_shader_handler = (unsigned int)-1;
  std::unordered_map<std::string, unsigned int> m_uniforms;
};

};  // namespace gfx
extern template class bse::Asset<gfx::Shader>;

#endif
