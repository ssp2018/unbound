#pragma once
#ifndef GFX_FRAMEBUFFER_HPP
#define GFX_FRAMEBUFFER_HPP

#include <ext/ext.hpp>

namespace gfx {

enum TextureType { RGBA, RGBA16F, RGB16F, RED, RGB };

// framebuffer component that is used to create a framebuffer with the textures specified
class FrameBuffer {
 public:
  // Default frame buffer
  FrameBuffer();

  FrameBuffer(float width, float height, bool depth, bool stencil = false);
  ~FrameBuffer();

  // create the actual fbo. the textrures must be added before calling create. Returns the depth
  // texture
  unsigned int create();

  // add a texture to the framebuffer and get it's id
  unsigned int add_texture(TextureType texture_flags);

  // bind the framebuffer
  void bind();

  // unbind the framebuffer
  void unbind();

  // get the gl handle
  unsigned int get_id();

  float get_width();
  float get_height();

 private:
  unsigned int m_fbo = -1;
  std::vector<unsigned int> m_textures;
  unsigned int m_depth_texture;

  float m_width;
  float m_height;
  bool m_has_depth;
  bool m_has_stencil;
};

};  // namespace gfx

#endif
