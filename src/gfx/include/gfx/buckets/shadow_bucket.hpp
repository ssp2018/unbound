
#pragma once

#ifndef GFX_SHADOW_BUCKET_HPP
#define GFX_SHADOW_BUCKET_HPP
#include "gfx/command_bucket.hpp"
namespace bse {
class StackAllocator;
}

namespace gfx {

// A collecion of commands. Takes a list and renders it into it's buffers
// template <typename T>
class ShadowBucket : public CommandBucket {
  // typedef unsigned Key;

 public:
  ShadowBucket(bse::StackAllocator* front, bse::StackAllocator* back, int size);
  virtual ~ShadowBucket();

  // submit the queue for rendering
  void submit() override;

  // Get light frustum depth
  float get_shadow_depth() const;

  // Get world -> light transform
  const glm::mat4& get_shadow_matrix() const;

  // Get shadow matrix with uncapped near.
  // Useful for frustum culling
  const glm::mat4& get_cull_matrix() const;

  // get texture
  // doesn't matter what "i" is, you'll always get the
  // same texture.
  unsigned int get_texture(int i) override;

  // Fit the light frustum around a list of points
  void fit_points(const std::vector<glm::vec3>& world_rim, const glm::vec3& direction);

  // Fit the light frustum around a camera
  void fit_camera(const glm::mat4& p_matrix, const glm::mat4& v_matrix, const glm::vec3& direction,
                  float near, float far);

  void swap() override;

 private:
  struct ShadowQueue {
    glm::mat4 shadow_matrix;
    glm::mat4 cull_matrix;
    float depth;
  };

  unsigned int m_fbo;
  unsigned int m_texture;
  size_t m_size;

  ShadowQueue m_shadow_front;
  ShadowQueue m_shadow_back;
};

}  // namespace gfx
#endif