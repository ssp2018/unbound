
#pragma once

#ifndef GFX_GBUFFER_BUCKET_HPP
#define GFX_GBUFFER_BUCKET_HPP

#include "gfx/command_bucket.hpp"
namespace bse {
class StackAllocator;
}

namespace gfx {

// A collecion of commands. Takes a list and renders it into it's buffers
// template <typename T>
class GBufferBucket : public CommandBucket {
  // typedef unsigned Key;

 public:
  GBufferBucket(bse::StackAllocator* front, bse::StackAllocator* back, int width, int height,
                bool color_only = true);
  virtual ~GBufferBucket();

  // submit the queue for rendering
  void submit() override;

  void set_size(size_t width, size_t height);

 private:
};

}  // namespace gfx
#endif