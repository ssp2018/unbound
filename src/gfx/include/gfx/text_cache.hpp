#pragma once

#ifndef GFX_TEXT_CACHE_HPP
#define GFX_TEXT_CACHE_HPP

#include <bse/asset.hpp>
#include <gfx/font.hpp>
#include <gfx/text.hpp>
#include <gfx/texture.hpp>

namespace bse {
template <typename T>
class RuntimeAsset;
}

namespace gfx {
class Font;
class Texture;
struct Text;

// A class for storing textures from texts and fonts for later use
class TextCache {
 public:
  struct Entry {
    unsigned int vao;
    unsigned int texture;
    unsigned int vertex_count;
    static constexpr gl::GLenum MODE = gl::GL_TRIANGLES;
    glm::vec2 bounds;
    glm::vec2 top_left;
  };

  TextCache(TextCache&& other);
  TextCache& operator=(TextCache&& other);

  // Get text draw data
  Entry get_text(const Text& text);

 private:
  struct Intermediate {
    std::vector<Text> text;
  };

 private:
  struct CacheEntry {
    void destroy();

    unsigned int vao;
    unsigned int vbo;
    unsigned int texture;
    unsigned int vertex_count;
    glm::vec2 bounds;
    glm::vec2 top_left;
  };

  TextCache() = default;

  bse::Asset<Font> get_font(const Text& text);
  CacheEntry create_cache_entry(const std::string& text, const bse::Asset<Font>& font);

  // Performs move operation
  void move(TextCache&& other);

  // Destroy buffers
  void destroy();

  // Stores the current state (RuntimeAsset)
  std::unique_ptr<Intermediate> store() const;

  // Restores the state
  void restore(Intermediate& intermediate);

  std::unordered_map<std::string, CacheEntry> m_cache;
  friend class bse::RuntimeAsset<TextCache>;
};

}  // namespace gfx

#endif