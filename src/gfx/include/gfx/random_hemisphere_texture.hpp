#pragma once
#ifndef GFX_RANDOM_HEMISPHERE_TEXTURE_HPP
#define GFX_RANDOM_HEMISPHERE_TEXTURE_HPP

namespace gfx {
// texture for random points in a hemisphere
class RandomHemisphereTexture {
 public:
  RandomHemisphereTexture();
  RandomHemisphereTexture(RandomHemisphereTexture &&other);
  RandomHemisphereTexture(const RandomHemisphereTexture &other) = delete;
  ~RandomHemisphereTexture();

  RandomHemisphereTexture &operator=(RandomHemisphereTexture &&other);
  RandomHemisphereTexture &operator=(RandomHemisphereTexture &other) = delete;

  // Load Texture from file
  // static bse::Result<Texture> load(bse::FilePath path);

  // Bind texture to material.
  // void bind(bse::Asset<Material> &material) const;

  unsigned int get_id() const;

 private:
  // Move other to this and nullify other
  void move(RandomHemisphereTexture &&other);
  // helper for generating noise
  float lerp(float a, float b, float f);

  unsigned int m_id = 0;
};

};  // namespace gfx

#endif
