#pragma once
#ifndef GFX_BACKEND_DISPATCH_HPP
#define GFX_BACKEND_DISPATCH_HPP

namespace gfx {

// All methods for commands
namespace backend_dispatch {

// bind a shader
void bind_shader(const void* data);
// bind an uniform mat4
void bind_uniform_mat4(const void* data);
// bind a list of uniform mat4
void bind_uniform_mat4_list(const void* data);
// bind an uniform vec3
void bind_uniform_vec3(const void* data);
// bind an uniform list of vec3
void bind_uniform_vec3_list(const void* data);
// bind an uniform vec4
void bind_uniform_vec4(const void* data);
// bind an uniform float
void bind_uniform_float(const void* data);
// bind a list of uniform float
void bind_uniform_float_list(const void* data);
// bind an uniform int
void bind_uniform_int(const void* data);
// bind an uniform bool
void bind_uniform_bool(const void* data);
// bind a texture
void bind_texture(const void* data);
// bind a uniform buffer
void bind_uniform_buffer(const void* data);

// draw vertices non-indexed
void draw_arrays(const void* data);

// draw vertices indexed
void draw_indexed(const void* data);
// draw vertices indexed and instanced
void draw_instanced_indexed(const void* data);
// draw lines
void draw_lines(const void* data);
// draw points
void draw_points(const void* data);
// set stencil ref
void set_stencil_ref(const void* data);

}  // namespace backend_dispatch
}  // namespace gfx
#endif
