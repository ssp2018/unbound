#ifndef GFX_FONT_HPP
#define GFX_FONT_HPP

#include <bse/asset.hpp>
#include <bse/file_path.hpp>
#include <bse/result.hpp>

struct FT_FaceRec_;
typedef FT_FaceRec_ *FT_Face;

namespace gfx {
class Texture;

// Font resource
// Used to generate text textures.
class Font {
 public:
  struct Character {
    glm::vec2 top_left;
    glm::vec2 size;
    glm::vec2 uv_top_left;
    glm::vec2 uv_size;
  };

  Font(Font &&other);
  Font(const Font &other) = delete;
  ~Font();

  static void destroy();

  Font &operator=(Font &&other);
  Font &operator=(Font &other) = delete;

  // Get font atlas texture
  const gfx::Texture &get_texture() const;

  // Build a string of text.
  std::vector<Character> build_text(const std::string &text) const;

  // Load font from file
  static bse::Result<Font> load(bse::FilePath path);

  // Min/Max bound
  std::pair<glm::vec2, glm::vec2> get_bounds(const std::string &text) const;

 private:
  struct CharacterMeta {
    glm::vec2 uv;
    glm::vec2 uv_size;
    glm::vec2 size;
    size_t ascender;
    size_t descender;
  };

  Font(FT_Face face, std::unique_ptr<gfx::Texture> &&atlas);

  void initialize();

  // Move other to this and nullify other
  void move(Font &&other);

  static constexpr char MIN_CHAR = ' ';
  static constexpr char MAX_CHAR = '~';
  static constexpr char NUM_CHARS = MAX_CHAR - MIN_CHAR + 1;
  static constexpr size_t BUFFER_SIZE = 8;
  static constexpr size_t FONT_SIZE = 24;
  static constexpr size_t BUFFERED_CHAR_SIZE = FONT_SIZE + BUFFER_SIZE * 2;
  static constexpr size_t TEXTURE_WIDTH = BUFFERED_CHAR_SIZE * NUM_CHARS;
  static constexpr size_t TEXTURE_HEIGHT = BUFFERED_CHAR_SIZE;
  static_assert(TEXTURE_WIDTH % 4 == 0);  // plz no pesky padding

  std::unique_ptr<gfx::Texture> m_atlas;
  FT_Face m_face = nullptr;
  std::array<CharacterMeta, NUM_CHARS> m_metas;
  static bool m_is_freetype_initialized;
  static FT_Library m_library;
};

};  // namespace gfx

extern template class bse::Asset<gfx::Font>;

#endif
