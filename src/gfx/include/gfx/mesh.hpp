#pragma once
#ifndef GFX_MESH_HPP
#define GFX_MESH_HPP

#include "bse/mesh/mesh_loader.hpp"
#include <bse/asset.hpp>
#include <ext/ext.hpp>
#include <gfx/aabb.hpp>
#include <gfx/material.hpp>

namespace gfx {
class Material;
using VertexFlags = bse::Flags<bse::VertexTypes::COUNT>;

// Mesh needs to dynamically allocate space for the required types.
// For a shadowpass only the position attribute is required.
class Mesh {
 public:
  Mesh();  // remove??

  // create a mesh and set its data
  Mesh(bse::ModelData &model_data);

  Mesh(Mesh &&other);
  Mesh(Mesh &other) = delete;
  ~Mesh();

  Mesh &operator=(Mesh &&other);
  Mesh &operator=(Mesh &other) = delete;

  static bse::Result<Mesh> load(bse::FilePath path);

  // generate the final meshes for this model
  static bse::Result<Mesh> generate_final_meshes(bse::ModelData &model, bse::FilePath path);

  bse::Skeleton &get_skeleton() const;

  // set a material, useful for ECS to dynamically change materials
  void set_material(bse::Asset<Material> material);

  // get the total number of vertices
  size_t get_vertex_count() const;

  AABB get_aabb() const;

  VertexFlags get_vertex_data_types() const;

  // get quad mesh along x and z
  static const Mesh &texture_quad();

  // get quad mesh along x and y
  static const Mesh &fullscreen_quad();

  struct MeshBuffers {
    unsigned int m_vao = 0;
    unsigned int m_vbo_position = 0;
    unsigned int m_vbo_color = 0;
    unsigned int m_vbo_normal = 0;
    unsigned int m_vbo_uv = 0;
    unsigned int m_vbo_skeletal = 0;
    unsigned int m_ebo = 0;
    unsigned int m_instanced_vao = 0;
    unsigned int m_instanced_vbo_matrices = 0;
  };

  // Get struct of references to OpenGL resources containing the required vertex data
  MeshBuffers get_buffers(VertexFlags vertex_data);

  // Get struct of references to OpenGL resources containing the required vertex data
  const MeshBuffers get_buffers(VertexFlags vertex_data) const;

  // Create a new set of OpenGL resources for the mesh. Datatypes set in 'vertex_data' will have a
  // new, empty buffer created. The other types will use a copy of the mesh's data. The EBO will
  // refer to this mesh. Intended for use with CPU skinning
  MeshBuffers get_buffers_copy(VertexFlags vertex_data) const;

  const bse::ModelData &get_model_data() const;

 private:
  // get the size of one vertex
  const int get_vertex_size(VertexFlags vertexTypes) const;

  // calculate an aabb for this mesh based on vertices.
  void calculate_aabb(std::vector<float> vertices, int stride);

  // Moves other into this
  void move_from(Mesh &&other);

  // Destroys this object
  void destroy();

  // Helper function for generating VBOs
  void generate_vbo(unsigned int &save_location, void *data, size_t data_size);

  // Helper function for generating the VAO and VBOs of a MeshBuffers struct
  void generate_buffers(VertexFlags vertex_data, MeshBuffers &buffers);

  // Function for generating VAO for MeshBuffers object that does not include all available types of
  // mesh data
  void generate_subset_buffers(VertexFlags vertex_data, MeshBuffers &buffers);

  // Returns the appropriate MeshBuffers from m_buffers, or nullptr if it does not exist
  MeshBuffers *find_buffers(VertexFlags vertex_data);

  // Returns the appropriate MeshBuffers from m_buffers, or nullptr if it does not exist
  const MeshBuffers *find_buffers(VertexFlags vertex_data) const;

  const MeshBuffers &find_all_types_buffer() const;
  MeshBuffers &find_all_types_buffer();

  // Stores buffers for different combinations of vertex data
  // The pair containing m_vertex_data is considered the main owner of the resources
  std::vector<std::pair<VertexFlags, MeshBuffers>> m_buffers;

  VertexFlags m_vertex_data;

  // aabb
  AABB m_aabb;

  // vertex information

  // The vertex data of this mesh
  bse::ModelData m_mesh_data;

  static const VertexFlags M_TEXTURED_QUAD_VERTEX_TYPES;
  static const std::vector<float> M_TEXTURED_QUAD_VERTICES;
  static const std::vector<float> M_FULLSCREEN_QUAD_VERTICES;
  static const std::vector<float> M_TEXTURED_QUAD_UVS;
  static const std::vector<unsigned int> M_TEXTURED_QUAD_INDICES;
};

};  // namespace gfx
extern template class bse::Asset<gfx::Mesh>;

#endif
