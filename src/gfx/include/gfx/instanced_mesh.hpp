#pragma once

#ifndef GFX_INSTANCED_MESH_HPP
#define GFX_INSTANCED_MESH_HPP

namespace gfx {

// A class for managing instanced meshes
class InstancedMesh {
 public:
  InstancedMesh();
  ~InstancedMesh();

  void set_mesh(unsigned int vbo);

  unsigned int get_vao();

 private:
  unsigned int m_vbo;
  unsigned int m_vao;
  unsigned int m_ebo;
  unsigned int m_instanced_vbo;
};

}  // namespace gfx

#endif