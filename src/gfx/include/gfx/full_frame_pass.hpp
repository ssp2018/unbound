#pragma once
#ifndef GFX_FULL_FRAME_PASS_HPP
#define GFX_FULL_FRAME_PASS_HPP

#include "gfx/command_packet.hpp"
#include "gfx/mesh.hpp"
#include <bse/allocators.hpp>
#include <bse/asset.hpp>
namespace gfx {
class Shader;
}

namespace gfx {

// render a single quad for the whole screen. will hold a mesh and shader.
class FullFramePass {
 public:
  FullFramePass(bse::StackAllocator* front, bse::StackAllocator* back,
                bse::Asset<gfx::Shader> shader, int width, int height, bool buffer = false);
  virtual ~FullFramePass();

  // add commands for binding textures etc..
  template <typename U>
  U* add_command(size_t aux_memory_size) {
    size_t total_size = command_packet::get_size<U>(aux_memory_size);
    CommandPacket packet = m_base_back.stack_allocator->allocate(total_size);
    command_packet::store_next_command_packet(packet, nullptr);
    command_packet::store_backend_dispatch_function(packet, U::DISPATCH_FUNCTION);

    m_base_back.packets.push_back(packet);
    return command_packet::get_command<U>(packet);
  };

  // render the pass
  virtual void submit();
  // no sorting needed since everything is one drawcall.

  // clear old frame
  void clear();

  // swap front and back buffers
  virtual void swap();

  // return the shader
  bse::Asset<Shader> get_shader();

  // return a texture based on index
  virtual int get_texture(int index);

  // return athe fbo
  unsigned int get_fbo();

 protected:
  struct BaseQueue {
    std::vector<void*> packets;
    bse::StackAllocator* stack_allocator;
  };

  BaseQueue m_base_front;
  BaseQueue m_base_back;

  // execute a command
  void submit_packet(const CommandPacket packet);

  bse::Asset<gfx::Shader> m_shader;

  int m_height;
  int m_width;
  unsigned int m_texture = -1;
  unsigned int m_fbo;
  bool m_has_fbo;
};

};  // namespace gfx

#endif
