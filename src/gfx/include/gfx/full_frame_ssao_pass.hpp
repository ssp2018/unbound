#pragma once
#ifndef GFX_FULL_FRAME_SSAO_PASS_HPP
#define GFX_FULL_FRAME_SSAO_PASS_HPP

#include "gfx/full_frame_pass.hpp"
#include "gfx/random_hemisphere_texture.hpp"
#include <bse/asset.hpp>
namespace bse {
class StackAllocator;
}
namespace gfx {
class Shader;
}

namespace gfx {

// render a single quad for the whole screen. will calculate ssao for the scene
class FullFrameSSAOPass : public FullFramePass {
 public:
  FullFrameSSAOPass(bse::StackAllocator* front, bse::StackAllocator* back,
                    bse::Asset<gfx::Shader> shader, int width, int height);
  virtual ~FullFrameSSAOPass();

  // render the pass
  void submit() override;

  int get_texture(int index) override;
  void swap() override;

 private:
  RandomHemisphereTexture m_noise_texture;
  std::vector<glm::vec3> m_ssao_kernel;
  unsigned int m_ssao_texture;
  // unsigned int m_fbo;
};

};  // namespace gfx

#endif
