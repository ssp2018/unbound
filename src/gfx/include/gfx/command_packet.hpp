#pragma once

#ifndef GFX_COMMAND_PACKET_HPP
#define GFX_COMMAND_PACKET_HPP

#include "command.hpp"

namespace gfx {

typedef void* CommandPacket;

namespace command_packet {

static const size_t OFFSET_NEXT_COMMAND_PACKET = 0u;
static const size_t OFFSET_BACKEND_DISPATCH_FUNCTION =
    OFFSET_NEXT_COMMAND_PACKET + sizeof(CommandPacket);
static const size_t OFFSET_COMMAND =
    OFFSET_BACKEND_DISPATCH_FUNCTION + sizeof(commands::BackendDispatchFunction);

// return the total size needed by a commandPacket
template <typename T>
size_t get_size(size_t auxMemorySize) {
  return OFFSET_COMMAND + sizeof(T) + auxMemorySize;
};

// return the next command packet
CommandPacket* get_next_command_packet(CommandPacket packet);

// return the next command packet
template <typename T>
CommandPacket* get_next_command_packet(T* command) {
  return reinterpret_cast<CommandPacket*>(reinterpret_cast<char*>(command) - OFFSET_COMMAND +
                                          OFFSET_NEXT_COMMAND_PACKET);
}

// get the method to execute for this command
commands::BackendDispatchFunction* get_backend_dispatch_function(CommandPacket packet);

// get the command in a packet
template <typename T>
T* get_command(CommandPacket packet) {
  return reinterpret_cast<T*>(reinterpret_cast<char*>(packet) + OFFSET_COMMAND);
}

// get auxilary memory, i.e buffer data from command
template <typename T>
char* get_auxiliary_memory(T* command) {
  return reinterpret_cast<char*>(command) + sizeof(T);
}

// sotre the nex command packet
void store_next_command_packet(CommandPacket packet, CommandPacket nextPacket);

// sotre the nex command packet
template <typename T>
void store_next_command_packet(T* command, CommandPacket nextPacket) {
  *command_packet::get_next_command_packet<T>(command) = nextPacket;
}

// set thte backend dispatch function
void store_backend_dispatch_function(CommandPacket packet,
                                     commands::BackendDispatchFunction dispatchFunction);

// returns the next command packet
const CommandPacket load_next_command_packet(const CommandPacket packet);

// get backend dispatch function
const commands::BackendDispatchFunction load_backend_dispatch_function(const CommandPacket packet);

// get command
const void* load_command(const CommandPacket packet);
};  // namespace command_packet
};  // namespace gfx
#endif