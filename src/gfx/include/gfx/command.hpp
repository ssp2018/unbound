#pragma once

#ifndef GFX_COMMAND_HPP
#define GFX_COMMAND_HPP

#include <ext/ext.hpp>

namespace gfx {
namespace commands {

typedef void (*BackendDispatchFunction)(const void *);

// bind a shader
struct BindShader {
  static const BackendDispatchFunction DISPATCH_FUNCTION;

  unsigned int shader_handle;
};
static_assert(std::is_pod<BindShader>::value == true, "BindShader must be a POD.");

// bind an uniform mat4
struct BindUniformMat4 {
  static const BackendDispatchFunction DISPATCH_FUNCTION;

  unsigned int location;
  glm::mat4 matrix;
};
static_assert(std::is_pod<BindUniformMat4>::value == true, "BindUniformMat4 must be a POD.");

// bind an uniform mat4
struct BindUniformMat4List {
  static const BackendDispatchFunction DISPATCH_FUNCTION;

  unsigned int location;
  const float *matrices;
  int count;
};
static_assert(std::is_pod<BindUniformMat4List>::value == true,
              "BindUniformMat4List must be a POD.");

// bind an uniform vec3
struct BindUniformVec3 {
  static const BackendDispatchFunction DISPATCH_FUNCTION;

  unsigned int location;
  glm::vec3 vector;
};
static_assert(std::is_pod<BindUniformVec3>::value == true, "BindUniformVec3 must be a POD.");

// bind a uniform list of vec3
struct BindUniformVec3List {
  static const BackendDispatchFunction DISPATCH_FUNCTION;

  unsigned int location;
  float *vertices;
  int count;
};
static_assert(std::is_pod<BindUniformVec3List>::value == true,
              "BindUniformVec3List must be a POD.");

// bind an uniform vec4
struct BindUniformVec4 {
  static const BackendDispatchFunction DISPATCH_FUNCTION;

  unsigned int location;
  glm::vec4 vector;
};
static_assert(std::is_pod<BindUniformVec4>::value == true, "BindUniformVec4 must be a POD.");

// bind an uniform float
struct BindUniformFloat {
  static const BackendDispatchFunction DISPATCH_FUNCTION;

  unsigned int location;
  float value;
};
static_assert(std::is_pod<BindUniformFloat>::value == true, "BindUniformFloat must be a POD.");

// bind an uniform float
struct BindUniformFloatList {
  static const BackendDispatchFunction DISPATCH_FUNCTION;

  unsigned int location;
  float *floats;
  int count;
};
static_assert(std::is_pod<BindUniformFloatList>::value == true,
              "BindUniformFloatList must be a POD.");

// bind an uniform int
struct BindUniformInt {
  static const BackendDispatchFunction DISPATCH_FUNCTION;

  unsigned int location;
  int value;
};

// bind an uniform bool
struct BindUniformBool {
  static const BackendDispatchFunction DISPATCH_FUNCTION;

  unsigned int location;
  bool value;
};
static_assert(std::is_pod<BindUniformBool>::value == true, "BindUniformBool must be a POD.");

// bind an uniform vec4
struct BindTexture {
  static const BackendDispatchFunction DISPATCH_FUNCTION;

  unsigned int location;
  unsigned int texture;
  unsigned int index;
};
static_assert(std::is_pod<BindTexture>::value == true, "BindTexture must be a POD.");

// bind an uniform buffer
struct BindUniformBuffer {
  static const BackendDispatchFunction DISPATCH_FUNCTION;

  unsigned int location;
  unsigned int id;
  unsigned int program;
};
static_assert(std::is_pod<BindUniformBuffer>::value == true, "BindUniformBuffer must be a POD.");

// draw non-indexed mesh
struct DrawArrays {
  static const BackendDispatchFunction DISPATCH_FUNCTION;

  unsigned int vertex_count;
  unsigned int vao;
  gl::GLenum triangle_mode;
};
static_assert(std::is_pod<DrawArrays>::value == true, "DrawIndexed must be a POD.");

// draw indexed mesh
struct DrawIndexed {
  static const BackendDispatchFunction DISPATCH_FUNCTION;

  unsigned int vertex_count;
  unsigned int vao;
  gl::GLenum triangle_mode;
};
static_assert(std::is_pod<DrawIndexed>::value == true, "DrawIndexed must be a POD.");

// draw instanced indexed mesh
struct DrawInstancedIndexed {
  static const BackendDispatchFunction DISPATCH_FUNCTION;

  unsigned int vertex_count;
  unsigned int vao;
  unsigned int instance_count;
};
static_assert(std::is_pod<DrawInstancedIndexed>::value == true, "DrawIndexed must be a POD.");

// draw mesh
struct DrawLines {
  static const BackendDispatchFunction DISPATCH_FUNCTION;

  unsigned int vertex_count;
  unsigned int vao;
};
static_assert(std::is_pod<DrawLines>::value == true, "Draw must be a POD.");

// draw mesh
struct DrawPoints {
  static const BackendDispatchFunction DISPATCH_FUNCTION;

  unsigned int vertex_count;
  unsigned int vao;
  unsigned int point_size;
};
static_assert(std::is_pod<DrawPoints>::value == true, "Draw must be a POD.");

// stencil draw value
struct SetStencilRef {
  static const BackendDispatchFunction DISPATCH_FUNCTION;

  unsigned int value;
};
static_assert(std::is_pod<SetStencilRef>::value == true, "SetStencilRef must be a POD.");

};  // namespace commands
};  // namespace gfx
#endif