#pragma once
#ifndef GFX_MODEL_HPP
#define GFX_MODEL_HPP

#include "mesh.hpp"
#include <bse/asset.hpp>
#include <bse/file_path.hpp>
#include <bse/mesh/mesh_loader.hpp>
#include <bse/result.hpp>

namespace bse {}

namespace gfx {
class Skeleton;
// The only class exposed to ECS? ECS requests a model and then it is able to select different skins
// for the model.
class Model {
 public:
  Model();
  Model(Model &&other);
  Model(const Model &other) = delete;
  ~Model();

  Model &operator=(Model &&other);
  Model &operator=(Model &other) = delete;

  // loader creation method
  static bse::Result<Model> load(bse::FilePath path);

  // return the material, mostly for binding of uniforms
  bse::Asset<Material> get_material() const;

  void set_material(bse::Asset<Material> material);

  // return the shadow material, mostly for binding of uniforms
  bse::Asset<Material> get_shadow_material() const;

  void set_shadow_material(bse::Asset<Material> shadow_material);

  void set_mesh(bse::Asset<Mesh> mesh);

  // return the mesh in model
  const bse::Asset<Mesh> get_mesh() const;

  // Return vertex data for mesh
  const bse::ModelData &get_model_data() const;

  // Return vertex data types for mesh
  VertexFlags get_vertex_data_types() const;

 private:
  bse::Asset<Mesh> m_mesh;

  // a material for this specific model.
  bse::Asset<Material> m_material;
  bse::Asset<Material> m_shadow_material;
};

};  // namespace gfx
extern template class bse::Asset<gfx::Model>;

#endif
