#pragma once

#ifndef GFX_FULL_FRAME_GAUSSIAN_PASS_HPP
#define GFX_FULL_FRAME_GAUSSIAN_PASS_HPP

#include "gfx/full_frame_pass.hpp"
#include <bse/asset.hpp>
namespace bse {
class StackAllocator;
}
namespace gfx {
class Shader;
}

namespace gfx {

// render a single quad for the whole screen. will run two passes adding a gaussian blur filter to
// the texture
class FullFrameGaussianPass : public FullFramePass {
 public:
  FullFrameGaussianPass(bse::StackAllocator* front, bse::StackAllocator* back,
                        bse::Asset<gfx::Shader> shader, int width, int height);
  virtual ~FullFrameGaussianPass();

  // render the pass
  void submit() override;

  // set the blur radius
  void set_radius(float radius);

  // set the texture to blur
  void set_texture(unsigned int texture_id);

  void swap() override;

 private:
  struct Queue {
    unsigned int input_texture;
    float radius = 0.05f;
  };

  Queue m_front;
  Queue m_back;

  unsigned int m_internal_texture;
  unsigned int m_internal_fbo;
};

};  // namespace gfx

#endif
