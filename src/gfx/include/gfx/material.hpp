#pragma once
#ifndef GFX_MATERIAL_HPP
#define GFX_MATERIAL_HPP

#include <bse/asset.hpp>
#include <bse/file_path.hpp>
#include <bse/mesh/mesh_loader.hpp>
#include <bse/result.hpp>
#include <gfx/shader.hpp>

namespace gfx {
class Shader;
class Texture;
// Materia describes all the properties of an object/material. i.e. colors, shadows, shaders
class Material {
 public:
  Material();
  Material(Material &&other);
  Material(const Material &other) = delete;
  ~Material();

  Material &operator=(Material &&other);
  Material &operator=(Material &other) = delete;

  // load shader from file. result is stoored globally.
  static bse::Result<Material> load(bse::FilePath path);

  // return shader asset
  bse::Asset<Shader> get_shader() const;

  // get shader handle
  unsigned int get_shader_handle() const;

  // get an uniform location
  int get_uniform_location(const std::string &name) const;

  // check if material have a texture
  bool has_texture() const;

  // get the texture id
  unsigned int get_texture() const;

  // bind shader
  //   void bind() const;

  //   // bind_model_matrix
  //   void bind_model_matrix(const float *model_matrix) const;

  //   // bind_view_projection_matrix
  //   void bind_view_projection_matrix(const float *view_projection_matrix) const;

  //   // bind_model_view_projection_matrix
  //   void bind_model_view_projection_matrix(const float *model_view_projection_matrix) const;

  //   // only supports binding a single directional light
  //   void bind_light(const float *light_vec, const float *light_color) const;

  //   // bind a uniform based on name only.
  //   void bind_uniform_mat4(const float *matrix, const char *name, int count) const;

  //   // bind <count> vec3 uniforms at <name>
  //   void bind_uniform_vec3(const float *vec3, const char *name, int count) const;

  //   // bind a texture
  //   void bind_texture(const char *name, unsigned int texture, int index) const;

  bse::Flags<bse::VertexTypes::COUNT> get_required_vertex_data() const;

  // matrix locations
  int model_matrix_location;
  int view_projection_matrix_location;

  // texture location
  int texture_location;

  // light locations
  int directional_light_location;
  int directional_light_color_location;
  int light_space_matrix_location;

  // point lights
  int point_light_position_location;
  int point_light_color_location;
  int point_light_radius_location;
  int point_light_count_location;

  // shadow locations
  int depth_map_0_location;
  int depth_map_1_location;
  int depth_map_2_location;
  int depth_map_3_location;
  int shadow_matrix_location;

  int model_matrix_ubo_location;

 private:
  // load all uniform locations
  void get_uniform_locations();

  bse::Asset<Shader> m_shader;
  bse::Asset<Texture> m_texture;

  bool m_has_texture = false;

  // attributes
  bool m_casts_shadow = true;  // wether it will be rendered to shadowmap
  bool m_has_shadow = true;    // wether it will use the shadowmap
  bool m_affected_by_directional_light =
      true;  // wether it will use directional lights. maybe use a bitmap??
  bool m_affected_by_point_light = true;
  bse::Flags<bse::VertexTypes::COUNT> m_required_vertex_data;
};

};  // namespace gfx

extern template class bse::Asset<gfx::Material>;
#endif
