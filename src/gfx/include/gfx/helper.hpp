#ifndef GFX_HELPER_HPP
#define GFX_HELPER_HPP

#include <ext/ext.hpp>

namespace gfx {

struct ShadowFrustumDetails {
  glm::mat4 view;
  glm::mat4 projection;
  glm::vec3 position;
  float near;
  float far;
  float left;
  float right;
  float top;
  float bottom;
  float radius;
  glm::vec3 forward_direction;
  glm::vec3 right_direction;
  glm::vec3 up_direction;
};

// calculate a shadow frustum
glm::mat4 calcuate_shadow_frustum(float near, float far, glm::mat4 cam_projection_matrix,
                                  glm::mat4 cam_view_matrix, glm::vec3 light_direction);

ShadowFrustumDetails calculate_shadow_frustum_detailed(float near, float far,
                                                       glm::mat4 cam_projection_matrix,
                                                       glm::mat4 cam_view_matrix,
                                                       glm::vec3 light_direction);

// Fit a shadow frustum to contain points
glm::mat4 fit_shadow_frustum(const std::vector<glm::vec3>& points,
                             const glm::vec3& light_direction);

ShadowFrustumDetails fit_shadow_frustum_detailed(const std::vector<glm::vec3>& points,
                                                 const glm::vec3& light_direction);

float get_vram_usage();
float get_ram_usage();
};  // namespace gfx

#endif