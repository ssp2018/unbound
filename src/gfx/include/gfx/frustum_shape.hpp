#pragma once

#ifndef GFX_FRUSTUM_SHAPE_HPP
#define GFX_FRUSTUM_SHAPE_HPP

namespace gfx {
struct AABB;
}

namespace gfx {

class PlaneShape {
 public:
  PlaneShape() {}
  PlaneShape(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3);
  ~PlaneShape();

  float distance(const glm::vec3 &p) const;

  glm::vec3 m_normal;
  glm::vec3 m_point;
  float m_d;
};

// Frustum shape which haqndles intersection checking between frustum and aabb
class FrustumShape {
 public:
  FrustumShape();
  FrustumShape(glm::mat4 vp_matrix);
  FrustumShape(glm::mat4 p_matrix, glm::mat4 v_matrix);
  ~FrustumShape();

  // check if aabb intersects with frustum.
  bool aabb_intersect(gfx::AABB aabb) const;

 private:
  // get positve vector in normal direction
  glm::vec3 get_vertex_p(AABB aabb, glm::vec3 normal) const;
  // get negative vector in normal direction
  glm::vec3 get_vertex_n(AABB aabb, glm::vec3 normal) const;

  PlaneShape m_planes[6];
};

};  // namespace gfx

#endif
