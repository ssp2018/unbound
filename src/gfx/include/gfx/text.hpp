#pragma once
#ifndef GFX_TEXT_HPP
#define GFX_TEXT_HPP

#include <bse/file_path.hpp>
#include <bse/result.hpp>

namespace gfx {

// Descriptor for drawable text.
struct Text {
  // font name, e.g. "arial"
  std::string font;

  // text to be drawn
  std::string text;
};

};  // namespace gfx

#endif
