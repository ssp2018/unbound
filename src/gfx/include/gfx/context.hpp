#ifndef GFX_CONTEXT_HPP
#define GFX_CONTEXT_HPP

namespace bse {
struct TaskHandle;
}

struct GLFWwindow;

namespace gfx {

// Utility for initializing and running stuff
// on the render thread.
class Context {
 public:
  // Initialize context on render thread to window.
  static void initialize(GLFWwindow* window);

  // Run function on render thread synchronously
  static void run(const std::function<void()>& func);

  // Run functiono n render thread asynchronously
  static bse::TaskHandle run_async(const std::function<void()>& func);

  // Check if this thread is the render thread.
  static bool is_render_thread();
};

};  // namespace gfx

#endif  // GFX_CONTEXT_HPP
