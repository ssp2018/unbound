#pragma once
#ifndef GFX_DEBUG_BUFFER_HPP
#define GFX_DEBUG_BUFFER_HPP

#include <bse/asset.hpp>
#include <gfx/shader.hpp>
namespace gfx {
class Shader;
}

namespace gfx {

// a buffer for storing all debug data.
class DebugBuffer {
 public:
  DebugBuffer();
  ~DebugBuffer();

  // add vertices to buffer
  void add_vertices(std::vector<float> vertices);

  // uodate gl buffer data
  void update_buffer();

  // get vao
  unsigned int get_vao() const;

  bse::Asset<Shader> get_shader() const;

  int get_vertex_count() const;

  void clear();

 private:
  std::vector<float> m_vertices;

  unsigned int m_vao;
  unsigned int m_vbo;

  bse::Asset<Shader> m_shader;
};

}  // namespace gfx
#endif
