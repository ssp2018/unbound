#pragma once
#ifndef GFX_UNIFORM_BUFFER_HPP
#define GFX_UNIFORM_BUFFER_HPP

#include <bse/file_path.hpp>
#include <bse/result.hpp>

namespace gfx {

// A class for creating and updating uniform buffers
class UniformBuffer {
 public:
  UniformBuffer();
  ~UniformBuffer();

  UniformBuffer(UniformBuffer &&other);
  UniformBuffer(UniformBuffer &other) = delete;

  UniformBuffer &operator=(UniformBuffer &&other);
  UniformBuffer &operator=(UniformBuffer &other) = delete;

  void create(size_t size, void *data);
  void update(size_t size, void *data);

  void bind(int index);
  void unbind(int index);

  unsigned int get_handle();

 private:
  unsigned int m_ubo;
};

};  // namespace gfx

#endif
