
#pragma once

#ifndef GFX_COMMAND_BUCKET_HPP
#define GFX_COMMAND_BUCKET_HPP

#include "command_packet.hpp"
#include "key.hpp"
#include <bse/allocators.hpp>

namespace gfx {

// A collecion of commands. Takes a list and renders it into it's buffers
// template <typename T>
class CommandBucket {
  // typedef unsigned Key;

 public:
  CommandBucket();
  CommandBucket(bse::StackAllocator* front, bse::StackAllocator* back, int width, int height);

  virtual ~CommandBucket() = default;

  // unbind vbo
  void unbind_fbo();

  // add a new command into commandBucket
  template <typename U>
  U* add_command(Key key, size_t aux_memory_size);

  // append a command after an existing command
  template <typename U, typename V>
  U* append_command(V* command, size_t aux_memory_size);

  // submit the queue for rendering
  virtual void submit();

  // execute a command
  void submit_packet(const CommandPacket packet);

  // sort keys
  void sort();

  // clear old frame
  void clear();

  // get texture at index
  virtual unsigned int get_texture(int i);

  // clear framebuffers
  virtual void prepare_for_new_frame();

  // swap front and back command queues
  virtual void swap();

 protected:
  struct Queue {
    Key* keys;
    void** packets;
    uint32_t* sorted;
    // std::vector<Key> m_keys;
    // std::vector<void*> m_packets;
    // std::vector<uint32_t> m_sorted;
    bse::StackAllocator* stack_allocator;
    int number_keys = 0;
  };

  Queue m_front;
  Queue m_back;

  int m_width;
  int m_height;

  int m_max_number_of_keys = 2048;

  // unsigned int m_fbo = -1;
  // int m_num_textures = 0;
  // std::vector<unsigned int> m_textures;
};

template <typename U>
U* CommandBucket::add_command(Key key, size_t aux_memory_size) {
  // CommandPacket packet = commandPacket::Create<U>(aux_memory_size);
  size_t total_size = command_packet::get_size<U>(aux_memory_size);
  CommandPacket packet = m_back.stack_allocator->allocate(total_size);
  command_packet::store_next_command_packet(packet, nullptr);
  command_packet::store_backend_dispatch_function(packet, U::DISPATCH_FUNCTION);

  // replace push_back to increase performance!!
  // m_packets.push_back(packet);
  // m_keys.push_back(key);
  // m_sorted.push_back(m_sorted.size() - 1);

  if (m_back.number_keys >= m_max_number_of_keys) {
    // increase size of array.
    m_max_number_of_keys *= 2;
    Key* old_keys = m_back.keys;
    void** old_packets = m_back.packets;
    m_back.keys = (Key*)m_back.stack_allocator->allocate(m_max_number_of_keys * sizeof(Key));
    m_back.packets = (void**)m_back.stack_allocator->allocate(m_max_number_of_keys * sizeof(void*));
    for (int i = 0; i < m_back.number_keys; i++) {
      m_back.keys[i] = old_keys[i];
      m_back.packets[i] = old_packets[i];
    }
  }
  // add to lists
  m_back.keys[m_back.number_keys] = key;
  m_back.packets[m_back.number_keys] = packet;
  m_back.number_keys++;

  return command_packet::get_command<U>(packet);
};

template <typename U, typename V>
U* CommandBucket::append_command(V* command, size_t aux_memory_size) {
  // CommandPacket packet = commandPacket::Create<U>(aux_memory_size);

  size_t total_size = command_packet::get_size<U>(aux_memory_size);
  CommandPacket packet = m_back.stack_allocator->allocate(total_size);

  // append this command to the given one
  command_packet::store_next_command_packet<V>(command, packet);

  command_packet::store_next_command_packet(packet, nullptr);
  command_packet::store_backend_dispatch_function(packet, U::DISPATCH_FUNCTION);

  return command_packet::get_command<U>(packet);
};

}  // namespace gfx
#endif