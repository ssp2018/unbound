#pragma once

#ifndef GFX_FULL_FRAME_RADIAL_PASS_HPP
#define GFX_FULL_FRAME_RADIAL_PASS_HPP

#include "gfx/full_frame_pass.hpp"
#include <bse/asset.hpp>
namespace bse {
class StackAllocator;
}
namespace gfx {
class Shader;
}
namespace gfx {

// render a single quad for the whole screen. will render the screen with a radial shader
class FullFrameRadialPass : public FullFramePass {
 public:
  FullFrameRadialPass(bse::StackAllocator* front, bse::StackAllocator* back,
                      bse::Asset<gfx::Shader> shader, int width, int height);
  virtual ~FullFrameRadialPass();

  // render the pass
  void submit() override;

  // set the blur radius
  void set_radius(float radius);

  // set the texture to blur
  void set_texture(unsigned int texture_id);
  void swap() override;

 private:
  struct Queue {
    unsigned int input_texture;
    float radius = 0.05f;
  };

  Queue m_front;
  Queue m_back;
};
}  // namespace gfx

#endif
