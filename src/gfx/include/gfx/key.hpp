#pragma once
#ifndef GFX_KEY_HPP
#define GFX_KEY_HPP

#include "gfx/shader.hpp"
#include <bse/asset.hpp>

namespace gfx {

// a key for command packets. used for sorting based on shader and depth among others
struct Key {
  using value_type = uint32_t;

  union {
    struct {
      value_type shader_index : 6;  // the index for the resource
      value_type depth : 25;        // a number to keep track of the version of the asset.
      value_type blend_type : 1;    // highest prio
    };
    value_type full_handle;
  };
  bse::Asset<Shader> shader;
};
}  // namespace gfx
#endif
