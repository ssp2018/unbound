#pragma once
#ifndef GFX_TEXTURE_HPP
#define GFX_TEXTURE_HPP

#include <bse/file_path.hpp>
#include <bse/result.hpp>
// #include <bse/runtime_asset.hpp>
#include <bse/asset.hpp>
#include <gfx/material.hpp>

// Forward declaration
namespace bse {
template <typename T>
class RuntimeAsset;
}

namespace gfx {
// Texture resource loaded into GPU
class Texture {
 public:
  // Rgba pixel
  struct Rgba {
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char a;
  };

  // Grayscale pixel
  struct Gray {
    unsigned char g;
  };

  Texture() = default;
  Texture(Rgba *pixels, size_t width, size_t height);
  Texture(Gray *pixels, size_t width, size_t height);
  Texture(Texture &&other);
  Texture(const Texture &other) = delete;
  ~Texture();

  Texture &operator=(Texture &&other);
  Texture &operator=(Texture &other) = delete;

  // Load Texture from file
  static bse::Result<Texture> load(bse::FilePath path);

  // Bind texture to material.
  void bind(const bse::Asset<Material> &material) const;

  unsigned int get_id() const;

  struct Intermediate {
    std::variant<std::vector<Rgba>, std::vector<Gray>> pixels;
    size_t width;
    size_t height;
  };

  // Store the texture data in the intermediate format
  std::unique_ptr<Intermediate> store() const;

  // Restore texture from the intermediate format
  void restore(Intermediate &intermediate);

 private:
  // Move other to this and nullify other
  void move(Texture &&other);

  unsigned int m_id = 0;
  bool m_is_grayscale;

  friend class bse::RuntimeAsset<Texture>;
};

};  // namespace gfx
extern template class bse::Asset<gfx::Texture>;

#endif
