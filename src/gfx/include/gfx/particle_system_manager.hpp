#pragma once

#ifndef GFX_PARTICLE_SYSTEM_MANAGER_HPP
#define GFX_PARTICLE_SYSTEM_MANAGER_HPP

namespace gfx {
// a handle to a particle system
struct ParticleSystemHandle {
  using value_type = uint32_t;
  static const ParticleSystemHandle NULL_HANDLE;

  // ParticleSystemHandle();
  // ParticleSystemHandle(int handle);

  operator value_type();
  union {
    struct {
      value_type register_index : 24;
      value_type iteration : 8;
    };
    value_type full_handle = ~value_type(0);
  };
};

// A class for managing particle systems
class ParticleSystemManager {
 public:
  struct ParticleSystem {
    unsigned int vbo;
    unsigned int vao;
    unsigned int ebo;
    unsigned int instanced_vbo;
    ParticleSystemHandle handle;
    int last_used;
  };

  ParticleSystemManager();
  ~ParticleSystemManager();

  // return a new handle to a particle system
  ParticleSystemHandle get_particle_system();

  // update the buffer of a particle system
  int update_buffer(float *floats, int count, ParticleSystemHandle handle);

  // get the vao from a handle
  unsigned int get_vao(ParticleSystemHandle handle);

  // age all particle systems
  void update_age();

  //   bse::Asset<gfx::Shader> get_shader();

  // Prepare for frame.
  void prepare_for_frame();

 private:
  // create a vbo
  unsigned int create_vbo_bufffer(int max_size);

  // create a vao
  void create_vao_buffer(ParticleSystem &ps);

  //   bse::Asset<gfx::Shader> m_shader;
  std::vector<ParticleSystem> m_particle_systems;
  std::vector<int> m_unused_particle_systems;
  size_t m_used_particle_index = 0;
};

}  // namespace gfx

#endif