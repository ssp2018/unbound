#ifndef GFX_AABB_HPP
#define GFX_AABB_HPP

namespace gfx {

struct AABB {
  glm::vec3 position = glm::vec3(0, 0, 0);  // position in modelspace
  glm::vec3 size =
      glm::vec3(0, 0, 0);  // size. position + size is one corner, position - size is the oposite

  // test if the point is inside the aabb
  bool is_point_inside(glm::vec3 point) {
    if (point.x < position.x + size.x && point.x > position.x - size.x) {
      if (point.y < position.y + size.y && point.y > position.y - size.y) {
        if (point.z < position.z + size.z && point.z > position.z - size.z) {
          // collision occured!
          return true;
        }
      }
    }
    return false;
  }
};

};  // namespace gfx

#endif
