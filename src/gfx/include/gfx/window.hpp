#pragma once
#ifndef GFX_WINDOW_HPP
#define GFX_WINDOW_HPP

#include <bse/asset.hpp>
#include <gfx/model.hpp>

struct GLFWwindow;

namespace gfx {

class Renderer;
class Model;

// Window creation
class Window {
 public:
  Window(int width, int height, std::string title, bool fullscreen);
  ~Window();

  // start main loop
  void run_loop();

  // check if the window is running or should be closed
  bool should_run();

  // check if the window have swapped recently
  bool has_swapped();

  // check if the window is ready for a swap
  bool can_swap();

  // do the actual swap
  void swap();

  // Close window
  void close();

  // Returns the width of the window
  int get_width() const;

  // Returns the height of the window
  int get_height() const;

  // get the window
  GLFWwindow *get_window();

  // Check if window is visible.
  bool is_visible() const;

  // get FPS (num swaps per second)
  float get_fps() const;

  // Get percentage of frame time dedicated
  // to buffer swap block.
  float get_swap_time_percent() const;

  // Set window title
  void set_title(const std::string &title) const;

  // Check if fullscreen
  bool is_fullscreen();

 private:
  GLFWwindow *m_window;
  int m_width = 720;
  int m_height = 1280;
  std::string m_title = "Hello";
  bool m_fullscreen;

  // Test variables to move camera
  // float posx = 0.0f, posy = 0.0f, posz = 0.0f;
  float pitch = 0.0f, yaw = 0.0f;

  float m_dt = 0;

  gl::GLsync m_sync = 0;
  int sync_state = -1;
  float m_fps = 60.f;
  float m_swap_percent = 0.5f;
  std::chrono::time_point<std::chrono::steady_clock> m_last_swap_time;
};

};  // namespace gfx

#endif
