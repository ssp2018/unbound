cmake_minimum_required(VERSION 3.11)

if(${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
  message(FATAL_ERROR "Please run cmake out of tree. E.g. create a separate build directory and run cmake from there.")
endif()

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR})
if(DEFINED ENV{VCPKG_ROOT} AND NOT DEFINED CMAKE_TOOLCHAIN_FILE)
  set(CMAKE_TOOLCHAIN_FILE "$ENV{VCPKG_ROOT}/scripts/buildsystems/vcpkg.cmake" CACHE STRING "")
endif()

project(unbound CXX)

include(GoogleTest)
enable_testing()

if (MSVC)
  #add_compile_options(/wd4251 /wd4275)


  add_compile_options(
    /wd4244 # type conversion, possible loss of information
    /wd4201 # nameless struct/union
    /wd4702 # unreachable code
    /wd4706 # assignment within conditional expression
  )
  
  if(NOT DEFINED UNBOUND_NWERROR)
    add_compile_options(/WX)
  endif()

  if(DEFINED UNBOUND_PROFILE)
    add_compile_options(/Z7)
    add_definitions(-DUNBOUND_PROFILE)
  endif()

  if(CMAKE_CXX_FLAGS_DEBUG MATCHES "/Zi")
    string(REGEX REPLACE "/Zi" "/Z7" CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG}")
  endif()
  if(CMAKE_CXX_FLAGS_RELWITHDEBINFO MATCHES "/Zi")
    string(REGEX REPLACE "/Zi" "/Z7" CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO}")
  endif()

  if(CMAKE_CXX_FLAGS MATCHES "/W[0-4]")
    string(REGEX REPLACE "/W[0-4]" "/W4" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
  else()
    add_compile_options(/W4)
  endif()

  if(DEFINED UNBOUND_PROFILE)
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /DEBUG")
  else()
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /DEBUG:FASTLINK")
  endif()

endif()

find_package(glbinding CONFIG REQUIRED)
find_package(GTest REQUIRED)
find_package(glm CONFIG REQUIRED)
find_package(glfw3 CONFIG REQUIRED)
find_package(Lua REQUIRED)
find_package(sol2 CONFIG REQUIRED)
find_package(OpenAL CONFIG REQUIRED) 
find_package(LibSndFile CONFIG REQUIRED NAMES LibSndFile SndFile)
find_package(Bullet REQUIRED)
find_package(assimp CONFIG REQUIRED)
find_package(Freetype REQUIRED)
find_package(imgui CONFIG REQUIRED)
find_package(PNG REQUIRED)



if(TARGET sol2)
  set(UNBOUND_EXT_SOL2 sol2)
elseif(TARGET sol2::sol2)
  set(UNBOUND_EXT_SOL2 sol2::sol2)
else()
  message(FATAL_ERROR "Failed to identify sol2 target name.")
endif()

if(TARGET sndfile-shared)
  set(UNBOUND_EXT_SNDFILE sndfile-shared)
elseif(TARGET SndFile::sndfile)
  set(UNBOUND_EXT_SNDFILE SndFile::sndfile)
elseif(TARGET sndfile-static)
  set(UNBOUND_EXT_SNDFILE sndfile-static)
else()
  message(FATAL_ERROR "Failed to identify sndfile target name.")
endif()

if(NOT MSVC)
  set(ASSIMP_LIBRARIES -L${ASSIMP_LIBRARY_DIRS} ${ASSIMP_LIBRARIES} -lIrrXML -lz)
  set(FREETYPE_DEPS -lpng -lbz2)
endif()
    
    
    

set(CMAKE_CXX_STANDARD 17)

include(CheckCXXCompilerFlag)
#set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS ON)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

add_definitions("-D_SILENCE_CXX17_ADAPTOR_TYPEDEFS_DEPRECATION_WARNING")



function(add_target_pch target ...)
  target_include_directories(${target} 
    PRIVATE 
      $<TARGET_PROPERTY:${PROJECT_NAME}_ext,INTERFACE_INCLUDE_DIRECTORIES>
  )

  if(MSVC)
    target_link_libraries(${target}
    PRIVATE
      $<TARGET_OBJECTS:${PROJECT_NAME}_ext>
      glbinding::glbinding # needs this for unknown reason, haven't investigated
    ) 

    target_compile_options(${target}
      PRIVATE
        /FI${EXT_HEADER} 
        /Yu${EXT_HEADER} 
        /Fp${EXT_PCH}
    )
  else()
    target_link_libraries(${target}
    PRIVATE
      ${PROJECT_NAME}_ext
      glbinding::glbinding # needs this for unknown reason, haven't investigated
    )

    target_compile_options(${target}
      PRIVATE
        -include ext/ext.hpp
    )
  endif()
  
  
  add_dependencies(${target} ${PROJECT_NAME}_ext)


  if(NOT MSVC)
    set(SOURCE_FILE_COMPILE_FLAGS "-Winvalid-pch")
  endif()

  SET_SOURCE_FILES_PROPERTIES(
    ${ARGV}
      PROPERTIES 
        COMPILE_FLAGS "${SOURCE_FILE_COMPILE_FLAGS}"
        OBJECT_DEPENDS ${EXT_OBJ}
  ) 


endfunction(add_target_pch)

function(add_library_pch target ...)
  add_library(${ARGV})
  add_target_pch(${ARGV})
endfunction(add_library_pch)



function(add_executable_pch target ...)
  add_executable(${ARGV})
  add_target_pch(${ARGV})
endfunction(add_executable_pch)

function(add_unbound_test ...)
  set(LAYER ${ARGV0})
  set(LAYER_TEST ${LAYER}_test)
  add_executable_pch(${LAYER_TEST} ${ARGN})
  target_link_libraries(${LAYER_TEST}
    PRIVATE
      ${LAYER}
      GTest::GTest
      GTest::Main
  )

  if(DEFINED UNBOUND_TEST_WHOLEARCHIVE)
    target_link_libraries(${LAYER_TEST}
      PRIVATE
        -WHOLEARCHIVE:$<TARGET_FILE:${LAYER}>
    )
  endif()

  gtest_discover_tests(${LAYER}_test)
endfunction()

add_subdirectory(ext)
add_subdirectory(bse)
add_subdirectory(gfx)
add_subdirectory(phy)
add_subdirectory(ai)
add_subdirectory(cor)
add_subdirectory(aud)
add_subdirectory(scr)
add_subdirectory(gmp)

set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS OFF)

add_subdirectory(client)
