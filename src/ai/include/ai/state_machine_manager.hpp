#pragma once
#ifndef AI_STATE_MACHINE_MANAGER_HPP
#define AI_STATE_MACHINE_MANAGER_HPP

#include <bse/serialize.hpp>

namespace ai {
// handler for creation/deletion of states and the like
class StateMachineManager {
 public:
  StateMachineManager();
  ~StateMachineManager();

  // initialize the member variables and enable the update function
  void initialize();

  // does stuff (placeholder)
  void update();

  CLASS_SERFUN((StateMachineManager)) {}
  CLASS_DESERFUN((StateMachineManager)) {}

 private:
};
}  // namespace ai
#endif