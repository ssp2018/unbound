#pragma once
#ifndef AI_GRID_HPP
#define AI_GRID_HPP

enum FieldType {
  CONSTANT,             // constant charge around charger
  DECREMENTING,         // strong at the charger, decreasing intensity further from source
  OVERWRITING_CONSTANT  // sets a definitive value, can only be changed by another definitive value
};

struct charge {
  glm::ivec2 pos;
  int radius;
  float charge;
  FieldType type;
};

namespace ai {

enum FieldType {
  CONSTANT,             // constant charge around charger
  DECREMENTING,         // strong at the charger, decreasing intensity further from source
  OVERWRITING_CONSTANT  // sets a definitive value, can only be changed by another definitive value
};

// charges creating fields in the map
struct charge {
  glm::ivec2 pos;
  int radius;
  float charge;
  FieldType type;
};

// nodes in A* algorithm
struct a_star_node {
  glm::ivec2 pos;
  glm::ivec2 back_link;
  int back_link_pos;

  friend bool operator==(const a_star_node&, const a_star_node&);
};

// handler of the influence map
class Grid {
 public:
  Grid();
  ~Grid();

  // initialize member functions and create the grid
  void initialize(int size_x, int size_y, float size_ratio, glm::vec2 world_origin, float value);

  // does stuff (placeholder)
  void update();

  // called to destroy current influence map and redo with same chargers
  void redo_map();

  // setters
  void set_definitive_map_value(glm::vec2 world_pos, float value);
  void set_definitive_map_value(glm::ivec2 grid_pos, float value);

  // getters
  float get_map_value(glm::vec2 world_pos);
  float get_map_value(glm::ivec2 grid_pos);

  // translations between our world and the grid
  glm::ivec2 world_to_grid(glm::vec2 world_pos);
  glm::vec2 grid_to_world(glm::ivec2 grid_pos);
  int world_to_grid(float world_length);
  float grid_to_world(int grid_length);
  std::vector<glm::vec2> grid_to_world(const std::vector<glm::ivec2>& grid_pos_list);

  // calculate the most effective path between 2 positions, returned as grid-path
  std::vector<glm::vec2> closest_path(glm::vec2 world_start, glm::vec2 world_destination,
                                      int neighbours = 4);
  std::vector<glm::vec2> closest_path(glm::ivec2 grid_start, glm::ivec2 grid_destination,
                                      int neighbours = 4);

  // move infuence map to new position, only affects normal chargers (WARNING!!! UNPREDICTABLE
  // RESULT)
  void move_sub_field_world(glm::vec2 world_old_pos, glm::vec2 world_new_pos);  // world pos
  void move_sub_field_grid(glm::ivec2 grid_old_pos, glm::ivec2 grid_new_pos);   // grid coords
  void move_sub_field(int charger_index, glm::ivec2 grid_new_pos);              // charge index

  // get influence values of the 8 neighbours, starting with north and moving clockwise
  std::array<float, 8> get_8_neighbours(glm::vec2 world_pos);
  std::array<float, 8> get_8_neighbours(glm::ivec2 grid_pos);

  // get influence values of the 4 neighbours, starting with north and moving clockwise
  std::array<float, 4> get_4_neighbours(glm::vec2 world_pos);
  std::array<float, 4> get_4_neighbours(glm::ivec2 grid_pos);

  // create new charger and place in field (WARNING! NO OBSTICLES CONSIDERED)
  void create_charger(glm::ivec2 grid_pos, int radius, float value, FieldType type);

  // remove existing charge from map
  void remove_charge(glm::ivec2 grid_pos);  // WILL REMOVE ALL CHARGES ON COORD
  void remove_overwriting_charge(int charger_index);
  void remove_normal_charge(int charger_index);

  // get all chargers or only those affecting a coord
  std::vector<charge> get_chargers();
  std::vector<charge> get_normal_chargers_affecting_pos(glm::ivec2 grid_pos);
  std::vector<charge> get_normal_chargers_affecting_pos(glm::vec2 world_pos);

  // creates chargers in the map based on the input points convex hull in the grid plane.
  void create_chargers_convex_hull(const std::vector<glm::vec3>& points, float value,
                                   FieldType field_type);
  // returns the grid values in a string, for debugging
  std::string to_string();

 private:
  std::vector<std::vector<float>> m_influence_map;
  std::vector<charge> m_chargers;
  std::vector<charge> m_overwriting_chargers;  // only overwriters

  glm::ivec2 m_world_size;
  glm::ivec2 m_world_origin;
  float m_world_to_grid_ratio;  // example: 0.5 means 2.0 world units fit in 1 grid cell length
  float m_base_map_value;

  // pathfinding helper functions
  bool in_closed_list(glm::ivec2 coord, std::vector<a_star_node>* closed_list);
  bool in_open_list(glm::ivec2 coord, std::vector<a_star_node>* open_list);
  void add_neighbours(glm::ivec2 coord, std::vector<a_star_node>* open_list,
                      std::vector<a_star_node>* closed_list, int neighbours);
  void check_node(glm::ivec2 coord, std::vector<a_star_node>* open_list,
                  std::vector<a_star_node>* closed_list);
  float flying_distance(glm::ivec2 pos1, glm::ivec2 pos2);
  void reconstruct_path(std::vector<a_star_node>* closed_list, std::vector<glm::ivec2>* ret_vector);

  // checking grid coords validity
  bool coord_is_valid(glm::ivec2 coord);

  // hardcoded manipulation of influence map
  void change_field(glm::ivec2 pos, int radius, float charge_change, FieldType type);
  void apply_charge(int pos, FieldType type);

  // takes a world position and transforms it into grid space.
  glm::vec2 world_to_grid_space(glm::vec2 world_pos);
};
}  // namespace ai
#endif