#ifndef NAV_TRIANGLE_HPP
#define NAV_TRIANGLE_HPP
#include <bse/serialize.hpp>
#include <ext/ext.hpp>
namespace ai {
// Triangle structure for the navmesh
struct NavTriangle {
  glm::vec3 pos;
  std::array<int, 3> point_indices;
  std::array<int, 3> neighbour_indices;
};

// returns a string showing the contents of the NavTriangle tri.
std::string nav_tri_to_string(const NavTriangle &tri);

};  // namespace ai

SERFUN((ai::NavTriangle)) {
  SERIALIZE(self.pos);
  SERIALIZE(self.point_indices);
  SERIALIZE(self.neighbour_indices);
}
DESERFUN((ai::NavTriangle)) {
  DESERIALIZE(self.pos);
  DESERIALIZE(self.point_indices);
  DESERIALIZE(self.neighbour_indices);
}

#endif
