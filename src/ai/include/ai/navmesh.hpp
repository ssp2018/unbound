#ifndef NAVMESH_HPP
#define NAVMESH_HPP
#include <ai/nav_triangle.hpp>
#include <bse/serialize.hpp>
#include <ext/ext.hpp>
namespace ai {
// Class that contains the navmesh, and it's responsebility is to give AI's world information as
// walking paths.
class Navmesh {
 public:
  Navmesh();
  ~Navmesh();
  // adds a triangle with no neigbours, returns the index of the added triangle
  int add_triangle(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3);
  // adds neighbours to triangle at index tri_index
  bool add_neighbours(int tri_index, int neighbours_index[3]);
  // Add triangle at index tri2 as neighbour to tri1 (Returns false if a
  // relationship could not be made, for example if one of the triangles have to many neighbours)
  bool add_neighbour(int tri1, int tri2);
  //
  std::vector<glm::vec3> closest_path(glm::vec3 start, glm::vec3 goal);
  std::vector<glm::vec3> m_points;
  std::vector<NavTriangle> m_triangles;

  CLASS_SERFUN((Navmesh)) {
    SERIALIZE(self.m_points);
    SERIALIZE(self.m_triangles);
  }
  CLASS_DESERFUN((Navmesh)) {
    DESERIALIZE(self.m_points);
    DESERIALIZE(self.m_triangles);
  }

 private:
  // std::vector<glm::vec3> m_points;
  // std::vector<NavTriangle> m_triangles;
  // Finds a free neighbour of the triangle with index tri (returns -1 if the triangle does not have
  // any free neighbours)
  int find_free_neighbour(int tri);
  // adds a point to the m_points list and returns its index
  int add_point(glm::vec3 point);
  // Performs A* algorithm on navmesh triangles as nodes, and then returns the path as a std::vector
  // containing which triangles that will be visited to get to goal node.
  std::vector<int> a_star(int start_tri, int goal_tri, glm::vec3 goal_pos);
  // a struct containing information needed for nodes in the A* algorithm.
  struct AStarNode {
    int index;
    int back_link;
    float est_dist_total;
    float est_dist;
    bool operator<(const AStarNode &other) const { return est_dist_total < other.est_dist_total; }
    bool operator>(const AStarNode &other) const { return est_dist_total > other.est_dist_total; }
    bool operator==(const AStarNode &other) const { return index == other.index; }
    bool operator<=(const AStarNode &other) const { return est_dist_total <= other.est_dist_total; }
    bool operator>=(const AStarNode &other) const { return est_dist_total >= other.est_dist_total; }
    bool operator!=(const AStarNode &other) const { return est_dist_total != other.est_dist_total; }
    bool operator==(const int &id) const { return index == id; }
  };
  // reconstructs the path by going from the goal node through all the backwards links
  std::vector<int> reconstruct_path(std::unordered_map<int, AStarNode> const &closed_list,
                                    const AStarNode &goal);
  // calculates the distance between the centriods of the triangles at index tri1 and tri2.
  float distance(int tri1, int tri2);
  // heuristic cost estimation from the triangle at index tri to goal point vec3.
  float heuristic_cost_est(int tri, glm::vec3 goal);
  // finds the closest triangle that is hit by the ray, and returns its index(returns -1 if it does
  // not hit any triangles)
  int triangle_pick(glm::vec3 ray_ori, glm::vec3 ray_dir);
  // finds the closest triangle middle to the input pos and returns its index
  int closest_triangle(glm::vec3 pos);
  // struct containing index of points of a portal
  struct Portal {
    glm::vec3 left, right;
    glm::vec3 &operator[](int x) { return (x < 1 ? left : right); }
  };
  // finds which two points of the two triangles that are the same and then calculates which point
  // is the left side and right side of the portal when traveling from tri1 to tri2.
  Portal find_portal(int tri1, int tri2);
  // finds the two portal points of the two portals, but has not defined which is left and right
  // side.
  Portal find_portal_points(int tri1, int tri2);
  // Cecks if the left and right point of the portal are correct based on triangle at index tris
  // position.
  bool check_left_right(int tri, int left, int right);
  bool check_left_right(int tri, const Portal &p);
  bool check_left_right(const glm::vec3 p1, const Portal &p);
  bool check_left_right(const glm::vec3 &p0, const glm::vec3 &p1, const glm::vec3 &p2);
  // takes in a list of portals with corresponding triangles and returns its optimized path in
  // path variable.
  void string_pull(std::vector<Portal> &portals, std::vector<glm::vec3> &path);
};
};  // namespace ai
#endif
