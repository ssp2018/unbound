#pragma once
#ifndef AI_AI_BASE_HPP
#define AI_AI_BASE_HPP
#include <ai/ai_component_manager.hpp>
#include <ai/navmesh.hpp>
#include <ai/state_machine_manager.hpp>
#include <bse/serialize.hpp>
namespace ai {
class Grid;
}

namespace ai {
// base AI handler for state machine, grid and AI component system
class AIBase {
 public:
  AIBase();
  ~AIBase();

  // initialize the member variables and enable the update function
  void initialize();

  // does stuff (placeholder)
  void update();

  // get the shortest path between start_pos and end_pos.
  // std::vector<glm::vec3> get_path(glm::vec3 start_pos, glm::vec3 end_pos);

  // converts a point from world to grid space
  // glm::ivec2 get_world_to_grid(glm::vec2 pos);

  CLASS_SERFUN((AIBase)) {
    SERIALIZE(self.m_component_manager);
    SERIALIZE(self.m_state_machine_mananger);
    // SERIALIZE(self.m_navmesh);
  }
  CLASS_DESERFUN((AIBase)) {
    DESERIALIZE(self.m_component_manager);
    DESERIALIZE(self.m_state_machine_mananger);
    // DESERIALIZE(self.m_navmesh);
  }

 private:
  std::unique_ptr<AIComponentManager> m_component_manager;
  std::unique_ptr<StateMachineManager> m_state_machine_mananger;
  // Grid* m_grid;
  // Navmesh m_navmesh;
};
}  // namespace ai
#endif