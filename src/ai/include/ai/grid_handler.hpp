#ifndef AI_GRID_HANDLER_HPP
#define AI_GRID_HANDLER_HPP

namespace ai {
class Grid;
}
namespace ai {
// a class that that have functionality to more easily fill the influence grid.
class GridHandler {
 public:
  // Adds the grid that the handler controlls in the constructor.
  GridHandler(Grid* grid);
  ~GridHandler();
  // Initializes the grid
  void initialize_grid(int size_x, int size_y, float size_ratio, glm::vec2 origin, float value);
  // Setting a spawn point in the grid, and the radius the spaned entity will be able to move
  void set_spawn_point(glm::vec3 pos, float radius);
  // Setting impassable on squares "touched" by the specified cylinder
  void set_impassable_cylinder(glm::vec3 pos, float radius);
  // Setting impassable on squares "touched" by the specified bounding box
  // The bounding box is specified with middle position and with 3 vectors representing which
  // orientation each side of the OBB is oriented and their length is the OBB's half length in that
  // direction
  void set_impassable_obb(glm::vec3 pos, glm::vec3 x_axis, glm::vec3 y_axis, glm::vec3 z_axis);

  // Converts the handled grid into a string for debugging.
  std::string to_string();

 private:
  Grid* m_grid;
  // Default constructor should not be used. Because gridhandler should always have a pointer to a
  // grid object
  GridHandler();
};
}  // namespace ai
#endif  // !AI_GRID_HANDLER_HPP
