#ifndef AI_STATE_BASE_HPP
#define AI_STATE_BASE_HPP

namespace ai {
// Base class of ai states, implementations of ai states will perform
// logic behaviour for specific states. Implementations of ai states
// should be implemented as singleton.
class StateBase {
 public:
  StateBase();
  virtual ~StateBase();
  // Updates the behaviour logic of the state
  virtual void update() = 0;
  // "get_instance" function should be implemented in implemented state class to create singleton
  // state* get_instance();
  void set_id(const std::string& s);
  std::string get_id();

 private:
  std::string m_id;
  // instance pointer should be added to implemented state for
  // state *m_instance;
};
}  // namespace ai
#endif
