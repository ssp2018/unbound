#pragma once
#ifndef AI_AI_COMPONENT_MANAGER_HPP
#define AI_AI_COMPONENT_MANAGER_HPP

#include <bse/serialize.hpp>
namespace ai {
// base handler of all AI-components in the entities
class AIComponentManager {
 public:
  AIComponentManager();
  ~AIComponentManager();

  // initialize the member variables and enable the update function
  void initialize();

  // does stuff (placeholder)
  void update();

  CLASS_SERFUN((AIComponentManager)) {}
  CLASS_DESERFUN((AIComponentManager)) {}

 private:
};
}  // namespace ai
#endif