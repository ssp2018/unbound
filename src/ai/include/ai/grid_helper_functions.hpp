#ifndef GRID_HELPER_FUNCTIONS_HPP
#define GRID_HELPER_FUNCTIONS_HPP

// Checks if a collsions has happened in a certain axis between a convex hull and a square.
bool check_collision_along_axis(const glm::vec2& axis, const std::vector<glm::vec2>& BBVertices,
                                const std::vector<glm::vec2>& squareVertices);

// Checks if collision between a convex hull and a square
bool check_convex_hull_square_collision(const std::vector<glm::vec2>& BBVertices,
                                        const std::vector<glm::vec2>& squareVertices);

// Calculates the hull of inputed points with Jarvis March method (also called Gift wrapping
// algorithm) and puts them in the second argument called hull.
// OBS! Function needs 3 points in argument points to run.
void calc_hull_jm(const std::vector<glm::vec2>& points, std::vector<glm::vec2>& hull);

// Enum that gives the results of a 2D orientation.
enum class Orientation2D { COLLINEAR, COUNTERCLOCKWISE, CLOCKWISE };
// Function takes three points and checks if they are ordered clockwise or counterclockwise
Orientation2D orientation_2d(glm::vec2 p1, glm::vec2 p2, glm::vec2 p3);

// ray/triangle intersection test, (vertex0, vertex1, vertex2 is the three points of the triangle
// being tested), hit_dist is the distance from ray_ori the intersection, this is only set if an
// intersection occurs.
bool ray_triangle_intersection(glm::vec3 ray_ori, glm::vec3 ray_dir, glm::vec3 vertex0,
                               glm::vec3 vertex1, glm::vec3 vertex2, float& hit_dist);
bool ray_triangle_intersection(glm::vec3 ray_ori, glm::vec3 ray_dir, glm::vec3 vertex0,
                               glm::vec3 vertex1, glm::vec3 vertex2);
// calculates the normal of the three points
glm::vec3 tri_normal(const glm::vec3& p0, const glm::vec3& p1, const glm::vec3& p2);
// calculates the triarea
float triarea2(const glm::vec3& a, const glm::vec3& b, const glm::vec3& c);
// projects the point to the nearest line of the triangle
glm::vec3 point_triangle_line_projection(const glm::vec3& point, const glm::vec3& p0,
                                         const glm::vec3& p1, const glm::vec3& p2,
                                         const glm::vec3& tri_center);

#endif
