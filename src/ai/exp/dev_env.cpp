#include "ai/ai_base.hpp"
#include "ai/navmesh.hpp"

using namespace glm;
void add_neighbour_rela(ai::Navmesh& nav, int tri1, int tri2) {
  nav.add_neighbour(tri1, tri2);
  nav.add_neighbour(tri2, tri1);
}

int main() {
  ai::AIBase* base = new ai::AIBase();
  base->initialize();

  ai::Navmesh navmesh;
  // Create triangles for the navmesh.
  int tri[11];
  tri[0] = navmesh.add_triangle({0.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, {1.0f, 1.0f, 0.0f});
  tri[1] = navmesh.add_triangle({1.0f, 1.0f, 0.0f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f});
  tri[2] = navmesh.add_triangle({1.0f, 0.0f, 0.0f}, {1.0f, 1.0f, 0.0f}, {2.0f, 1.0f, 0.0f});
  tri[3] = navmesh.add_triangle({1.0f, 1.0f, 0.0f}, {2.0f, 1.0f, 0.0f}, {2.0f, 2.0f, 0.0f});
  tri[4] = navmesh.add_triangle({1.0f, 1.0f, 0.0f}, {1.0f, 2.0f, 0.0f}, {2.0f, 2.0f, 0.0f});
  tri[5] = navmesh.add_triangle({1.0f, 2.0f, 0.0f}, {1.0f, 3.0f, 0.0f}, {2.0f, 2.0f, 0.0f});
  tri[6] = navmesh.add_triangle({1.0f, 0.0f, 0.0f}, {2.0f, 1.0f, 0.0f}, {2.0f, 0.0f, 0.0f});
  tri[7] = navmesh.add_triangle({1.0f, 0.0f, 0.0f}, {2.0f, 0.0f, 0.0f}, {2.0f, -1.0f, 0.0f});
  tri[8] = navmesh.add_triangle({2.0f, 0.0f, 0.0f}, {3.0f, 0.0f, 0.0f}, {2.0f, -1.0f, 0.0f});
  tri[9] = navmesh.add_triangle({2.0f, -1.0f, 0.0f}, {3.0f, 0.0f, 0.0f}, {3.0f, -1.0f, 0.0f});
  tri[10] = navmesh.add_triangle({2.0f, 1.0f, 0.0f}, {3.0f, 0.0f, 0.0f}, {2.0f, 0.0f, 0.0f});
  // Add triangles neighbour relationship
  add_neighbour_rela(navmesh, tri[0], tri[1]);
  add_neighbour_rela(navmesh, tri[1], tri[2]);
  add_neighbour_rela(navmesh, tri[2], tri[3]);
  add_neighbour_rela(navmesh, tri[3], tri[4]);
  add_neighbour_rela(navmesh, tri[4], tri[5]);
  add_neighbour_rela(navmesh, tri[2], tri[6]);
  add_neighbour_rela(navmesh, tri[7], tri[6]);
  add_neighbour_rela(navmesh, tri[10], tri[6]);
  add_neighbour_rela(navmesh, tri[7], tri[8]);
  add_neighbour_rela(navmesh, tri[8], tri[9]);
  add_neighbour_rela(navmesh, tri[8], tri[10]);

  for (int i = 0; i < 6; i++) {
    std::cout << "tri" << i << ": \n"
              << nav_tri_to_string(navmesh.m_triangles[tri[i]]) << std::endl;
  }

  // std::vector<glm::vec3> path = navmesh.closest_path({0.7f, 0.7f, 1.0f}, {1.1f, 2.1f, 1.0f});
  // std::vector<glm::vec3> path = navmesh.closest_path({0.7f, 0.7f, 1.0f}, {1.9f, 0.9f, 1.0f});
  // std::vector<glm::vec3> path = navmesh.closest_path({0.2f, 0.1f, 1.0f}, {2.1f, -0.9f, 1.0f});
  std::vector<glm::vec3> path = navmesh.closest_path({0.2f, 0.1f, 1.0f}, {0.5f, 0.1f, 1.0f});
  std::cout << "Path: \n";
  for (auto& point : path) {
    std::cout << "X: " << point.x << " Y: " << point.y << " Z: " << point.z << std::endl;
  }
  getchar();
  return 0;
}