#include <ai/nav_triangle.hpp>

std::string ai::nav_tri_to_string(const NavTriangle &tri) {
  std::string out_string = "Position: \n";
  out_string = out_string + "X: " + std::to_string(tri.pos.x) + "\n";
  out_string = out_string + "Y: " + std::to_string(tri.pos.y) + "\n";
  out_string = out_string + "Z: " + std::to_string(tri.pos.z) + "\n";

  out_string = out_string + "Point Indices:\n";
  for (int i = 0; i < 3; i++) {
    out_string =
        out_string + std::to_string(i) + ": " + std::to_string(tri.point_indices[i]) + "\n";
  }
  out_string = out_string + "Neighbour Indices:\n";
  for (int i = 0; i < 3; i++) {
    out_string =
        out_string + std::to_string(i) + ": " + std::to_string(tri.neighbour_indices[i]) + "\n";
  }
  return out_string;
}