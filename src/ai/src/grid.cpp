#include "ai/grid.hpp"

#include "ai/grid_helper_functions.hpp"
#include "bse/log.hpp"

bool ai::operator==(const a_star_node& a, const a_star_node& b) {
  return a.back_link == b.back_link && a.back_link_pos == b.back_link_pos && a.pos == b.pos;
}

ai::Grid::Grid() {}

ai::Grid::~Grid() {}

void ai::Grid::initialize(int size_x, int size_y, float size_ratio, glm::vec2 origin, float value) {
  // create and fill the matrix with "value"
  m_influence_map.resize(size_x);
  for (int i = 0; i < size_x; ++i) m_influence_map[i].resize(size_y, value);

  // fill member variables
  m_world_size = {size_x, size_y};
  m_world_origin = origin;
  m_world_to_grid_ratio = size_ratio;
  m_base_map_value = value;
}

void ai::Grid::update() {}

void ai::Grid::redo_map() {
  for (int i = 0; i < m_world_size.x; ++i)
    m_influence_map[i].resize(m_world_size.y, m_base_map_value);

  for (int i = 0; i < m_chargers.size(); ++i) {
    apply_charge(i, m_chargers.at(i).type);
  }

  for (int i = 0; i < m_overwriting_chargers.size(); ++i) {
    apply_charge(i, FieldType::OVERWRITING_CONSTANT);
  }
}

void ai::Grid::set_definitive_map_value(glm::vec2 world_pos, float value) {
  glm::ivec2 pos = world_to_grid(world_pos);

  create_charger(pos, 0, value, FieldType::OVERWRITING_CONSTANT);
}

void ai::Grid::set_definitive_map_value(glm::ivec2 grid_pos, float value) {
  create_charger(grid_pos, 0, value, FieldType::OVERWRITING_CONSTANT);
}

float ai::Grid::get_map_value(glm::vec2 world_pos) {
  glm::vec2 grid_pos = world_to_grid(world_pos);
  float value = m_influence_map[grid_pos.x][grid_pos.y];
  return value;
}

float ai::Grid::get_map_value(glm::ivec2 grid_pos) {
  if (!coord_is_valid(grid_pos)) {
    LOG(WARNING) << "invalid grid coordinates, out of bounds";
  }
  float value = m_influence_map.at(grid_pos.x).at(grid_pos.y);
  return value;
}

glm::ivec2 ai::Grid::world_to_grid(glm::vec2 world_pos) {
  glm::ivec2 grid_coord = glm::round(world_to_grid_space(world_pos));

  if (grid_coord.x < 0 || grid_coord.x > m_influence_map.size() - 1 || grid_coord.y < 0 ||
      grid_coord.y > m_influence_map[0].size() - 1) {
    LOG(WARNING) << "world_to_grid: incorrect parameters or miscalculation\n";
    return glm::ivec2(0, 0);
  }

  return grid_coord;
}

glm::vec2 ai::Grid::world_to_grid_space(glm::vec2 world_pos) {
  return (world_pos * m_world_to_grid_ratio) - glm::vec2(m_world_origin);
}

glm::vec2 ai::Grid::grid_to_world(glm::ivec2 grid_pos) {
  glm::vec2 grid_coord;
  grid_coord.x = grid_to_world(grid_pos.x + m_world_origin.x);
  grid_coord.y = grid_to_world(grid_pos.y + m_world_origin.y);
  return grid_coord;
}

int ai::Grid::world_to_grid(float world_length) {
  return (int)(glm::ceil(world_length * m_world_to_grid_ratio));
}

float ai::Grid::grid_to_world(int grid_length) { return (grid_length) / m_world_to_grid_ratio; }

std::vector<glm::vec2> ai::Grid::grid_to_world(const std::vector<glm::ivec2>& grid_pos_list) {
  std::vector<glm::vec2> ret_list(grid_pos_list.size());
  for (unsigned int i = 0; i < ret_list.size(); i++) {
    ret_list[i] = grid_to_world(grid_pos_list[i]);
  }
  return ret_list;
};

// pathfinding start...

std::vector<glm::vec2> ai::Grid::closest_path(glm::vec2 world_start, glm::vec2 world_destination,
                                              int neighbours) {
  return closest_path(world_to_grid(world_start), world_to_grid(world_destination), neighbours);
}

bool ai::Grid::in_closed_list(glm::ivec2 coord, std::vector<a_star_node>* closed_list) {
  for (int i = 0; i < closed_list->size(); ++i)
    if (coord == closed_list->at(i).pos) return true;

  return false;
}

bool ai::Grid::in_open_list(glm::ivec2 coord, std::vector<a_star_node>* open_list) {
  for (int i = 0; i < open_list->size(); ++i)
    if (coord == open_list->at(i).pos) return true;

  return false;
}

void ai::Grid::add_neighbours(glm::ivec2 coord, std::vector<a_star_node>* open_list,
                              std::vector<a_star_node>* closed_list, int neighbours) {
  // add neighbours to open list
  check_node({coord.x, coord.y + 1}, open_list, closed_list);
  check_node({coord.x + 1, coord.y}, open_list, closed_list);
  check_node({coord.x, coord.y - 1}, open_list, closed_list);
  check_node({coord.x - 1, coord.y}, open_list, closed_list);

  if (neighbours == 8) {
    check_node({coord.x + 1, coord.y + 1}, open_list, closed_list);
    check_node({coord.x + 1, coord.y - 1}, open_list, closed_list);
    check_node({coord.x - 1, coord.y - 1}, open_list, closed_list);
    check_node({coord.x - 1, coord.y + 1}, open_list, closed_list);
  }
}

void ai::Grid::check_node(glm::ivec2 pos, std::vector<a_star_node>* open_list,
                          std::vector<a_star_node>* closed_list) {
  if (pos.x >= 0 && pos.x < m_influence_map.size() && pos.y >= 0 &&
      pos.y < m_influence_map.at(0).size()) {
    if (m_influence_map[pos.x][pos.y] != -1 && !in_closed_list(pos, closed_list) &&
        !in_open_list(pos, open_list))
      open_list->push_back({{pos},
                            {closed_list->at((int)closed_list->size() - 1).pos},
                            (int)closed_list->size() - 1});
  } else {
    LOG(WARNING) << "invalid grid coordinates, out of bounds";
  }
}

float ai::Grid::flying_distance(glm::ivec2 pos1, glm::ivec2 pos2) {
  float x = abs(pos1.x - pos2.x);
  float y = abs(pos1.y - pos2.y);
  float ret = sqrt(pow(x, 2) + pow(y, 2));
  return ret;
}

void ai::Grid::reconstruct_path(std::vector<a_star_node>* closed_list,
                                std::vector<glm::ivec2>* ret_vector) {
  std::vector<a_star_node> ret_inverse;

  int link = closed_list->size() - 1;
  while (1) {
    ret_inverse.push_back(closed_list->at(link));
    link = closed_list->at(link).back_link_pos;
    if (link == -1) {
      break;
    }
  }

  for (int i = 0; i < ret_inverse.size(); ++i) {
    ret_vector->push_back(ret_inverse.at(ret_inverse.size() - 1 - i).pos);
  }
}

std::vector<glm::vec2> ai::Grid::closest_path(glm::ivec2 grid_start, glm::ivec2 grid_destination,
                                              int neighbours) {
  std::vector<glm::vec2> ret_vector;

  if (neighbours != 4 && neighbours != 8) {
    LOG(FATAL) << "attempted to pathfind with unsupported number of neighbours, defaulting to 4\n";
    neighbours = 4;
  }
  if (grid_start == grid_destination) {
    // LOG(WARNING) << "closest_path: start same as destination, exiting\n";
    ret_vector.push_back(grid_to_world(grid_start));
    return ret_vector;
  }

  // A* pathfinding algorithm through grid.
  // detailed description can be found at:
  // https://en.wikipedia.org/wiki/A*_search_algorithm#Pseudocode

  std::vector<a_star_node> open_list, closed_list;

  // add origin to closed list
  closed_list.push_back({{grid_start.x, grid_start.y}, {-1, -1}, -1});
  add_neighbours({grid_start.x, grid_start.y}, &open_list, &closed_list, neighbours);

  glm::ivec2 lowest_coord;
  int open_list_pos = -1;

  while (open_list.size() > 0) {
    float lowest_value = 9999;

    for (int i = 0; i < open_list.size(); ++i) {
      float dist = 0;
      if ((dist = flying_distance(open_list.at(i).pos, grid_destination)) < lowest_value) {
        // this is (for the moment) the best node in the open list
        lowest_value = dist;
        lowest_coord = open_list.at(i).pos;
        open_list_pos = i;
      }
    }
    if (lowest_coord == grid_destination) {
      // we have found the destination!
      closed_list.push_back(open_list.at(open_list_pos));
      std::vector<glm::ivec2> grid_pos_list;
      ai::Grid::reconstruct_path(&closed_list, &grid_pos_list);
      ret_vector = grid_to_world(grid_pos_list);
      return ret_vector;
    }

    // proceed to this node
    a_star_node temp = open_list.at(open_list_pos);
    open_list.erase(open_list.begin() + open_list_pos);
    closed_list.push_back(temp);

    add_neighbours(lowest_coord, &open_list, &closed_list, neighbours);
  }

  // no path found :(
  LOG(WARNING) << "closest_path: no path found\n";
  ret_vector.push_back(grid_to_world(grid_start));
  return ret_vector;
}

//...pathfinding end

void ai::Grid::move_sub_field_world(glm::vec2 world_old_pos, glm::vec2 world_new_pos) {
  move_sub_field_grid(world_to_grid(world_old_pos), world_to_grid(world_new_pos));
}

void ai::Grid::move_sub_field_grid(glm::ivec2 grid_old_pos, glm::ivec2 grid_new_pos) {
  std::vector<int> move_list;

  for (int i = 0; i < m_chargers.size(); ++i)
    if (m_chargers.at(i).pos == grid_old_pos) move_list.push_back(i);

  for (int i = 0; i < move_list.size(); ++i) {
    move_sub_field(move_list.at(i), grid_new_pos);
  }
}

void ai::Grid::move_sub_field(int charger_index, glm::ivec2 grid_new_pos) {
  // remove old field
  change_field(m_chargers.at(charger_index).pos, m_chargers.at(charger_index).radius,
               -m_chargers.at(charger_index).charge, m_chargers.at(charger_index).type);

  // change pos of charger
  m_chargers.at(charger_index).pos = grid_new_pos;

  // add field to new location
  change_field(grid_new_pos, m_chargers.at(charger_index).radius,
               m_chargers.at(charger_index).charge, m_chargers.at(charger_index).type);
}

std::array<float, 8> ai::Grid::get_8_neighbours(glm::vec2 world_pos) {
  return get_8_neighbours(world_to_grid(world_pos));
}

std::array<float, 8> ai::Grid::get_8_neighbours(glm::ivec2 grid_pos) {
  std::array<float, 8> ret;

  if (coord_is_valid({grid_pos.x, grid_pos.y + 1}))
    ret.at(0) = m_influence_map[grid_pos.x][grid_pos.y + 1];
  else {
    LOG(WARNING) << "get_neighbours out of bounds\n";
    ret.at(0) = -1.f;
  }

  if (coord_is_valid({grid_pos.x + 1, grid_pos.y + 1}))
    ret.at(1) = m_influence_map[grid_pos.x + 1][grid_pos.y + 1];
  else {
    LOG(WARNING) << "get_neighbours out of bounds\n";
    ret.at(1) = -1.f;
  }

  if (coord_is_valid({grid_pos.x + 1, grid_pos.y}))
    ret.at(2) = m_influence_map[grid_pos.x + 1][grid_pos.y];
  else {
    LOG(WARNING) << "get_neighbours out of bounds\n";
    ret.at(2) = -1.f;
  }

  if (coord_is_valid({grid_pos.x + 1, grid_pos.y - 1}))
    ret.at(3) = m_influence_map[grid_pos.x + 1][grid_pos.y - 1];
  else {
    LOG(WARNING) << "get_neighbours out of bounds\n";
    ret.at(3) = -1.f;
  }

  if (coord_is_valid({grid_pos.x, grid_pos.y - 1}))
    ret.at(4) = m_influence_map[grid_pos.x][grid_pos.y - 1];
  else {
    LOG(WARNING) << "get_neighbours out of bounds\n";
    ret.at(4) = -1.f;
  }

  if (coord_is_valid({grid_pos.x - 1, grid_pos.y - 1}))
    ret.at(5) = m_influence_map[grid_pos.x - 1][grid_pos.y - 1];
  else {
    LOG(WARNING) << "get_neighbours out of bounds\n";
    ret.at(5) = -1.f;
  }

  if (coord_is_valid({grid_pos.x - 1, grid_pos.y}))
    ret.at(6) = m_influence_map[grid_pos.x - 1][grid_pos.y];
  else {
    LOG(WARNING) << "get_neighbours out of bounds\n";
    ret.at(6) = -1.f;
  }

  if (coord_is_valid({grid_pos.x - 1, grid_pos.y + 1}))
    ret.at(7) = m_influence_map[grid_pos.x - 1][grid_pos.y + 1];
  else {
    LOG(WARNING) << "get_neighbours out of bounds\n";
    ret.at(7) = -1.f;
  }

  return ret;
}

std::array<float, 4> ai::Grid::get_4_neighbours(glm::vec2 world_pos) {
  return get_4_neighbours(world_to_grid(world_pos));
}

std::array<float, 4> ai::Grid::get_4_neighbours(glm::ivec2 grid_pos) {
  std::array<float, 4> ret;

  if (coord_is_valid({grid_pos.x, grid_pos.y + 1}))
    ret.at(0) = m_influence_map[grid_pos.x][grid_pos.y + 1];
  else {
    LOG(WARNING) << "get_neighbours out of bounds\n";
    ret.at(0) = -1.f;
  }

  if (coord_is_valid({grid_pos.x + 1, grid_pos.y}))
    ret.at(1) = m_influence_map[grid_pos.x + 1][grid_pos.y];
  else {
    LOG(WARNING) << "get_neighbours out of bounds\n";
    ret.at(1) = -1.f;
  }

  if (coord_is_valid({grid_pos.x, grid_pos.y - 1}))
    ret.at(2) = m_influence_map[grid_pos.x][grid_pos.y - 1];
  else {
    LOG(WARNING) << "get_neighbours out of bounds\n";
    ret.at(2) = -1.f;
  }

  if (coord_is_valid({grid_pos.x - 1, grid_pos.y}))
    ret.at(3) = m_influence_map[grid_pos.x - 1][grid_pos.y];
  else {
    LOG(WARNING) << "get_neighbours out of bounds\n";
    ret.at(3) = -1.f;
  }

  return ret;
}

void ai::Grid::create_charger(glm::ivec2 grid_pos, int radius, float value, FieldType type) {
  if (!coord_is_valid(grid_pos)) {
    LOG(WARNING) << "invalid grid coordinates, out of bounds";
    return;
  }

  if (type != OVERWRITING_CONSTANT) {
    m_chargers.push_back(charge{grid_pos, radius, value, type});
  } else {
    m_overwriting_chargers.push_back(charge{grid_pos, radius, value, type});
  }

  change_field(grid_pos, radius, value, type);
}

void ai::Grid::remove_charge(glm::ivec2 grid_pos) {
  // this function could be restructured to 2 for-loops, but i reckon the logging is helpful

  std::vector<int> delete_list_normal;
  std::vector<int> delete_list_overwrite;

  // find all charges on spot
  for (int i = 0; i < m_chargers.size(); ++i)
    if (m_chargers.at(i).pos == grid_pos) delete_list_normal.push_back(i);

  for (int i = 0; i < m_overwriting_chargers.size(); ++i)
    if (m_overwriting_chargers.at(i).pos == grid_pos) delete_list_overwrite.push_back(i);

  // check number of charges found
  if (delete_list_normal.size() == 0 && delete_list_overwrite.size() == 0) {
    LOG(WARNING) << "remove_charge: no charge at those coords\n";
    return;
  }

  // delete found charges
  for (int i = 0; i < delete_list_normal.size(); ++i) {
    charge temp = m_chargers.at(delete_list_normal.at(i));
    change_field(temp.pos, temp.radius, -temp.charge, temp.type);  // OBS the inverse charge
    m_chargers.erase(m_chargers.begin() + delete_list_normal.at(i));
  }

  for (int i = 0; i < delete_list_overwrite.size(); ++i) {
    charge temp = m_overwriting_chargers.at(delete_list_overwrite.at(i));
    change_field(temp.pos, temp.radius, -temp.charge, temp.type);  // OBS the inverse charge
    m_overwriting_chargers.erase(m_overwriting_chargers.begin() + delete_list_overwrite.at(i));
  }
}

void ai::Grid::remove_overwriting_charge(int charger_index) {
  if (charger_index > m_overwriting_chargers.size() - 1) {
    LOG(FATAL) << "remove_charge: attempted to remove non-existant charge";
    return;
  }

  charge temp = m_overwriting_chargers.at(charger_index);
  change_field(temp.pos, temp.radius, -temp.charge, temp.type);  // OBS the inverse charge

  m_overwriting_chargers.erase(m_overwriting_chargers.begin() + charger_index);
}

void ai::Grid::remove_normal_charge(int charger_index) {
  if (charger_index > m_chargers.size() - 1) {
    LOG(FATAL) << "remove_charge: attempted to remove non-existant charge";
    return;
  }

  charge temp = m_chargers.at(charger_index);
  change_field(temp.pos, temp.radius, -temp.charge, temp.type);  // OBS the inverse charge

  m_chargers.erase(m_chargers.begin() + charger_index);
}

std::vector<ai::charge> ai::Grid::get_chargers() {
  return m_chargers;
}  // this might be inefficient, will investigate after early deployment

std::vector<ai::charge> ai::Grid::get_normal_chargers_affecting_pos(glm::ivec2 pos) {
  std::vector<charge> res;

  for (int i = 0; i < m_chargers.size(); ++i) {
    if (glm::distance(glm::vec2(pos.x, pos.y),
                      glm::vec2(m_chargers.at(i).pos.x, m_chargers.at(i).pos.y)) <
        m_chargers.at(i).radius) {
      res.push_back(m_chargers.at(i));
    }
  }

  return res;
}

std::vector<ai::charge> ai::Grid::get_normal_chargers_affecting_pos(glm::vec2 pos) {
  glm::ivec2 grid_coord;
  grid_coord.x = glm::trunc(pos.x);
  grid_coord.y = glm::trunc(pos.y);

  return get_normal_chargers_affecting_pos(grid_coord);
}

void ai::Grid::create_chargers_convex_hull(const std::vector<glm::vec3>& points, float value,
                                           FieldType field_type) {
  // Project 3D points onto the grids plane
  std::vector<glm::vec2> points_grid_space(points.size());
  for (unsigned int i = 0; i < points_grid_space.size(); i++) {
    points_grid_space[i] = world_to_grid_space(points[i]);
  }

  // Create the hull of the points projected in the plane
  std::vector<glm::vec2> hull;
  calc_hull_jm(points_grid_space, hull);

  // check min/max in x and y of the hull to be able to check over a smaller area of the influence
  // map

  glm::vec2 max(-FLT_MAX);
  glm::vec2 min(FLT_MAX);
  for (unsigned int i = 0; i < hull.size(); i++) {
    if (hull[i].x > max.x) max.x = hull[i].x;
    if (hull[i].y > max.y) max.y = hull[i].y;
    if (hull[i].x < min.x) min.x = hull[i].x;
    if (hull[i].y < min.y) min.y = hull[i].y;
  }
  max = glm::ceil(max) + glm::vec2(1);
  min = glm::floor(min);

  // defines at which x and y coordinates that the loops will start and end, which is either the max
  // and min points of the OBB or at the edges of the grid.
  unsigned int x_start = glm::max((int)0, (int)min.x);
  unsigned int x_end = glm::min((int)m_influence_map.size(), (int)max.x);
  unsigned int y_start = glm::max((int)0, (int)min.x);
  unsigned int y_end = glm::min((int)m_influence_map.size(), (int)max.y);

  // check hull against grid squares, and if intersect set a charger at position
  std::vector<glm::vec2> square_points(4);
  for (unsigned int x = x_start; x < x_end; x++) {
    for (unsigned int y = y_start; y < y_end; y++) {
      // get the square center
      glm::ivec2 grid_index(x, y);
      glm::vec2 square_center = grid_to_world(grid_index);

      // calculate the square points
      square_points[0] = square_center + glm::vec2(-0.5f, -0.5f);
      square_points[1] = square_center + glm::vec2(0.5f, -0.5f);
      square_points[2] = square_center + glm::vec2(0.5f, 0.5f);
      square_points[3] = square_center + glm::vec2(-0.5f, 0.5f);

      if (check_convex_hull_square_collision(hull, square_points)) {
        create_charger(grid_index, 0, value, field_type);
      }
    }
  }
}

std::string ai::Grid::to_string() {
  std::string out_string;
  if (m_influence_map.size() > 0) {
    // Loops through the whole grid adding their value to the string
    for (unsigned int y = 0; y < m_influence_map[0].size(); y++) {
      for (unsigned int x = 0; x < m_influence_map.size(); x++) {
        out_string += std::to_string(m_influence_map[x][y]) + " ";
      }
      out_string += "\n";
    }
  }
  return out_string;
}

bool ai::Grid::coord_is_valid(glm::ivec2 coord) {
  if (coord.x >= 0 && coord.x < m_influence_map.size() && coord.y >= 0 &&
      coord.y < m_influence_map.at(0).size()) {
    return true;
  }
  return false;
}

void ai::Grid::change_field(glm::ivec2 pos, int radius, float charge_change, FieldType type) {
  // check what definitive fields are within the area
  std::vector<int> affected_overwrite_fields;

  for (int i = 0; i < m_overwriting_chargers.size(); ++i) {
    if (glm::distance(glm::vec2(pos.x, pos.y), glm::vec2(m_overwriting_chargers.at(i).pos.x,
                                                         m_overwriting_chargers.at(i).pos.y)) -
            m_overwriting_chargers.at(i).radius <
        radius)
      affected_overwrite_fields.push_back(i);
  }

  // apply the charge on the grid
  for (int i = pos.x - radius; i <= pos.x + radius; ++i) {    // loop over all x
    for (int j = pos.y - radius; j <= pos.y + radius; ++j) {  // loop over all y
      if (i >= 0 && i < m_influence_map.size() && j >= 0 && j < m_influence_map.at(0).size()) {
        float distance = abs(glm::distance(glm::vec2(i, j), glm::vec2(pos.x, pos.y)));
        float intensity;

        if (radius == 0 || distance == 0)
          intensity = 1;
        else
          intensity = 1 - (distance / radius);

        if (intensity > 0) {
          if (type == DECREMENTING)
            m_influence_map[i][j] += intensity * charge_change;
          else
            m_influence_map[i][j] = charge_change;
        }
      }
    }
  }

  // apply the overwrite charges again
  for (int i = 0; i < affected_overwrite_fields.size(); ++i) {
    /*set_definitive_map_value(m_overwriting_chargers.at(affected_overwrite_fields.at(i)).pos,
                             m_overwriting_chargers.at(affected_overwrite_fields.at(i)).charge);*/

    apply_charge(affected_overwrite_fields.at(i), ai::OVERWRITING_CONSTANT);
  }
}

void ai::Grid::apply_charge(int pos, FieldType type) {
  charge c;

  if (type == ai::OVERWRITING_CONSTANT)
    c = m_overwriting_chargers.at(pos);
  else
    c = m_chargers.at(pos);

  for (int i = c.pos.x - c.radius; i <= c.pos.x + c.radius; ++i) {
    for (int j = c.pos.y - c.radius; j <= c.pos.y + c.radius; ++j) {
      if (i >= 0 && i < m_influence_map.size() && j >= 0 && j < m_influence_map.at(0).size()) {
        float distance = abs(glm::distance(glm::vec2(i, j), glm::vec2(c.pos.x, c.pos.y)));
        float intensity;

        if (c.radius == 0 || distance == 0)
          intensity = 1;
        else
          intensity = 1 - (distance / c.radius);

        if (intensity > 0) {
          if (type == DECREMENTING)
            m_influence_map[i][j] += intensity * c.charge;
          else
            m_influence_map[i][j] = c.charge;
        }
      }
    }
  }
}