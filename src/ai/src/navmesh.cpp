#include <ai/grid_helper_functions.hpp>
#include <ai/navmesh.hpp>
#include <bse/log.hpp>

ai::Navmesh::Navmesh() {}
ai::Navmesh::~Navmesh() {}

int ai::Navmesh::add_triangle(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3) {
  NavTriangle tri;
  tri.point_indices[0] = add_point(p1);
  tri.point_indices[1] = add_point(p2);
  tri.point_indices[2] = add_point(p3);
  // calc the centroid of the triangle, and use it as the triangles position.
  tri.pos = {};
  for (auto& point_index : tri.point_indices) {
    tri.pos += m_points[point_index];
  }
  tri.pos = tri.pos / 3.0f;
  // set all neighbours to -1 because they do not have any yet.
  for (auto& neighbour : tri.neighbour_indices) {
    neighbour = -1;
  }
  m_triangles.push_back(tri);
  return m_triangles.size() - 1;
}

bool ai::Navmesh::add_neighbours(int tri_index, int neighbours_index[3]) {
  bool change_made = false;
  for (int i = 0; i < 3; i++) {
    if (0 <= neighbours_index[i]) {
      if (!add_neighbour(tri_index, neighbours_index[i])) {
        return false;
      }
      change_made = true;
    }
  }
  return change_made;
}

bool ai::Navmesh::add_neighbour(int tri1, int tri2) {
  int free_spot1 = find_free_neighbour(tri1);
  if (free_spot1 < 0) {
    return false;
  }
  // set the triangle ones free spot neighbour to triangle two
  m_triangles[tri1].neighbour_indices[free_spot1] = tri2;
  return true;
}

std::vector<glm::vec3> ai::Navmesh::closest_path(glm::vec3 start, glm::vec3 goal) {
  std::vector<glm::vec3> path;
  // find start and goal triangles in navmesh.
  int start_tri = triangle_pick(start, {0.0f, 0.0f, -1.0f});
  int end_tri = triangle_pick(goal, {0.0f, 0.0f, -1.0f});
  // if either goal or start positions are not within the navmesh return empty path.
  if (start_tri < 0) {
    start_tri = closest_triangle(start);
  }
  if (end_tri < 0) {
    end_tri = closest_triangle(goal);
    goal = point_triangle_line_projection(goal, m_points[m_triangles[end_tri].point_indices[0]],
                                          m_points[m_triangles[end_tri].point_indices[1]],
                                          m_points[m_triangles[end_tri].point_indices[2]],
                                          m_triangles[end_tri].pos);
  }

  if (start_tri == end_tri) {  // if in same triangle as  goal just go directly to goal
                               // becuase it is a convex hull
    path.push_back(goal);
  } else {
    // use A* to find best path from goal triangle to goal triangle.
    std::vector<int> path_tris = a_star(start_tri, end_tri, goal);
    // define portals between triangles in the triangle path.
    std::vector<Portal> portals(path_tris.size() + 1);
    Portal port_start;
    port_start.left = start;
    port_start.right = start;
    portals[0] = port_start;
    for (unsigned int i = 1; i < path_tris.size(); i++) {
      portals[i] = find_portal(path_tris[i - 1], path_tris[i]);
    }
    Portal port_end;
    port_end.left = goal;
    port_end.right = goal;
    portals[portals.size() - 1] = port_end;
    // calculate which shortcuts can be made through the triangle path.
    string_pull(portals, path);
  }
  return path;
}

//---------------------------------
// private functions
//---------------------------------

int ai::Navmesh::find_free_neighbour(int tri) {
  for (int i = 0; i < 3; i++) {
    if (m_triangles[tri].neighbour_indices[i] < 0) {
      return i;
    }
  }
  return -1;
}

// function that calculates if two glm::vec3 are almost equal to each other based on epsilon called
// epsi.
bool vec3_equal(glm::vec3 v1, glm::vec3 v2, float epsi = 0.0001f) {
  if (fabs(v1.x - v2.x) > epsi) return false;
  if (fabs(v1.y - v2.y) > epsi) return false;
  if (fabs(v1.z - v2.z) > epsi) return false;
  return true;
}

int ai::Navmesh::add_point(glm::vec3 point) {
  for (int i = 0; i < m_points.size(); i++) {
    if (vec3_equal(m_points[i], point)) {
      return i;
    }
  }
  m_points.push_back(point);
  return m_points.size() - 1;
}

std::vector<int> ai::Navmesh::a_star(int start_tri, int goal_tri, glm::vec3 goal_pos) {
  std::unordered_map<int, AStarNode> closed_list;
  AStarNode first;
  first.index = start_tri;
  first.back_link = -1;
  first.est_dist = 0.0f;
  first.est_dist_total = heuristic_cost_est(first.index, goal_pos);
  std::vector<AStarNode> open_list;
  open_list.push_back(first);

  while (open_list.size() > 0) {
    // Sort to have a the best estimated node to be in the end of the array.
    std::sort(open_list.begin(), open_list.end(), std::greater<AStarNode>());
    // get the best estimated node and set as current, and then remove it from the open_list
    AStarNode current = open_list.back();
    open_list.pop_back();
    if (current == goal_tri) {
      return reconstruct_path(closed_list, current);
    }
    for (auto& neighbour : m_triangles[current.index].neighbour_indices) {
      // Check if neighbour is valid
      if (neighbour >= 0) {
        auto search_closed = closed_list.find(neighbour);
        if (search_closed != closed_list.end()) {
          continue;
        }
        float tentative_est_dist = current.est_dist + distance(current.index, neighbour);
        // neighbor not in open_list
        if (std::find(open_list.begin(), open_list.end(), neighbour) == open_list.end()) {
          AStarNode tmp;
          tmp.index = neighbour;
          tmp.est_dist = FLT_MAX;
          tmp.est_dist_total = FLT_MAX;
          open_list.push_back(tmp);
        }
        auto neighbour_node = std::find(open_list.begin(), open_list.end(), neighbour);
        if (tentative_est_dist >= neighbour_node->est_dist) {
          continue;
        }
        neighbour_node->back_link = current.index;
        neighbour_node->est_dist = tentative_est_dist;
        neighbour_node->est_dist_total =
            neighbour_node->est_dist + heuristic_cost_est(neighbour, goal_pos);
      }
    }
    closed_list[current.index] = current;
  }
  return std::vector<int>();
}

std::vector<int> ai::Navmesh::reconstruct_path(
    std::unordered_map<int, AStarNode> const& closed_list, const AStarNode& goal) {
  int current = goal.index;
  int next = goal.back_link;
  std::vector<int> path;
  path.push_back(goal.index);
  while (next > -1) {
    current = next;
    AStarNode tmp_node = closed_list.at(current);
    next = tmp_node.back_link;
    path.push_back(current);
  }
  std::reverse(path.begin(), path.end());
  return path;
}

float ai::Navmesh::distance(int tri1, int tri2) {
  return glm::distance(m_triangles[tri1].pos, m_triangles[tri2].pos);
}

float ai::Navmesh::heuristic_cost_est(int tri, glm::vec3 goal) {
  return glm::distance(m_triangles[tri].pos, goal);
}

int ai::Navmesh::triangle_pick(glm::vec3 ray_ori, glm::vec3 ray_dir) {
  float hit_dist = FLT_MAX;
  int hit_index = -1;
  for (int i = 0; i < m_triangles.size(); i++) {
    glm::vec3 p0 = m_points[m_triangles[i].point_indices[0]];
    glm::vec3 p1 = m_points[m_triangles[i].point_indices[1]];
    glm::vec3 p2 = m_points[m_triangles[i].point_indices[2]];
    float tmp_dist = FLT_MAX;
    if (ray_triangle_intersection(ray_ori, ray_dir, p0, p1, p2, tmp_dist)) {
      if (tmp_dist < hit_dist) {
        hit_dist = tmp_dist;
        hit_index = i;
      }
    }
  }
  return hit_index;
}

int ai::Navmesh::closest_triangle(glm::vec3 pos) {
  int hit_index = -1;
  float closest = FLT_MAX;
  for (int i = 0; i < m_triangles.size(); i++) {
    float dist = glm::distance(m_triangles[i].pos, pos);
    if (dist < closest) {
      hit_index = i;
      closest = dist;
    }
  }
  return hit_index;
}

ai::Navmesh::Portal ai::Navmesh::find_portal(int tri1, int tri2) {
  // find which points that in the triangles that make up the portal between them.
  Portal out_port = find_portal_points(tri1, tri2);
  // check if left and right side of portal is correct, if not swap left and right
  if (!check_left_right(tri1, out_port)) {
    glm::vec3 tmp = out_port.right;
    out_port.right = out_port.left;
    out_port.left = tmp;
  }
  return out_port;
}

ai::Navmesh::Portal ai::Navmesh::find_portal_points(int tri1, int tri2) {
  Portal out_port;
  int curr_index = 0;
  // Loop through indices of the two triangles until two equal points are found.
  for (auto& tri1_point : m_triangles[tri1].point_indices) {
    for (auto& tri2_point : m_triangles[tri2].point_indices) {
      if (tri1_point == tri2_point) {
        out_port[curr_index++] = m_points[tri1_point];
        if (curr_index > 1) {
          return out_port;
        }
      }
    }
  }
  out_port.left = glm::vec3();
  out_port.right = glm::vec3();
  return out_port;
}

bool ai::Navmesh::check_left_right(int tri, int left, int right) {
  Portal portal;
  portal.left = m_points[left];
  portal.right = m_points[right];
  return check_left_right(tri, portal);
}
bool ai::Navmesh::check_left_right(int tri, const Portal& p) {
  return check_left_right(m_triangles[tri].pos, p);
}
bool ai::Navmesh::check_left_right(const glm::vec3 p1, const Portal& p) {
  return check_left_right(p1, p.left, p.right);
}

bool ai::Navmesh::check_left_right(const glm::vec3& p0, const glm::vec3& p1, const glm::vec3& p2) {
  float area = triarea2(p0, p1, p2);
  if (area < 0) {
    return false;
  }
  return true;
}

void ai::Navmesh::string_pull(std::vector<Portal>& portals, std::vector<glm::vec3>& path) {
  //
  int apex_index = 0, left_index = 0, right_index = 0;
  glm::vec3 portal_apex, portal_left, portal_right;

  portal_apex = portals[0].left;
  portal_left = portals[0].left;
  portal_right = portals[0].right;

  for (int i = 1; i < portals.size(); i++) {
    const glm::vec3 left = portals[i].left;
    const glm::vec3 right = portals[i].right;
    // update right vertex
    if (triarea2(portal_apex, portal_right, right) <= 0.0f) {
      if (vec3_equal(portal_apex, portal_right) ||
          triarea2(portal_apex, portal_left, right) >= 0.0f) {
        portal_right = right;
        right_index = i;
      } else {
        // Right is over left so insert left to path and restart scan from portal left point
        path.push_back(portal_left);
        // Make current left new apex.
        portal_apex = portal_left;
        apex_index = left_index;
        // Reset portal
        portal_left = portal_apex;
        portal_right = portal_apex;
        left_index = apex_index;
        right_index = apex_index;
        // Restart scan
        i = apex_index;
        continue;
      }
    }
    // update left vertex
    if (triarea2(portal_apex, portal_left, left) >= 0.0f) {
      if (vec3_equal(portal_apex, portal_left) ||
          triarea2(portal_apex, portal_right, left) <= 0.0f) {
        portal_left = left;
        left_index = i;
      } else {
        // Right is over left so insert left to path and restart scan from portal left point
        path.push_back(portal_right);
        // Make current left new apex.
        portal_apex = portal_right;
        apex_index = right_index;
        // Reset portal
        portal_left = portal_apex;
        portal_right = portal_apex;
        left_index = apex_index;
        right_index = apex_index;
        // Restart scan
        i = apex_index;
        continue;
      }
    }
  }
  // Append goal point to path
  path.push_back(portals[portals.size() - 1].left);
}