#include "ai/grid_helper_functions.hpp"

#include "bse/assert.hpp"
#include "bse/log.hpp"
#include <bse/edit.hpp>

bool check_collision_along_axis(const glm::vec2& axis, const std::vector<glm::vec2>& BBVertices,
                                const std::vector<glm::vec2>& squareVertices) {
  // Calc min and max for both shapes
  float bbMin = FLT_MAX;
  float squareMin = FLT_MAX;
  float bbMax = -FLT_MAX;
  float squareMax = -FLT_MAX;

  float tmpVal;

  // finds the max and min points of the convex hull on the input axis
  for (auto c : BBVertices) {
    tmpVal = axis.x * c.x + axis.y * c.y;
    if (tmpVal < bbMin) {
      bbMin = tmpVal;
    }
    if (tmpVal > bbMax) {
      bbMax = tmpVal;
    }
  }

  // finds the max and min points of the square on the input axis
  for (auto c : squareVertices) {
    tmpVal = axis.x * c.x + axis.y * c.y;
    if (tmpVal < squareMin) {
      squareMin = tmpVal;
    }
    if (tmpVal > squareMax) {
      squareMax = tmpVal;
    }
  }

  if (bbMin > squareMax || squareMin > bbMax)
    return false;
  else
    return true;
}

bool check_convex_hull_square_collision(const std::vector<glm::vec2>& BBVertices,
                                        const std::vector<glm::vec2>& squareVertices) {
  // Calculate all normals
  std::vector<glm::vec2> normals;
  glm::vec2 tmp = BBVertices[BBVertices.size() - 1] - BBVertices[0];

  float len = glm::length(tmp);

  tmp.x /= len;
  tmp.y /= len;

  normals.push_back(glm::vec2(tmp.y, -tmp.x));

  // Calculates normals for convex hull (bounding box).
  for (int i = 0; i < BBVertices.size() - 1; i++) {
    tmp = BBVertices[i] - BBVertices[i + 1];

    len = glm::length(tmp);
    tmp.x /= len;
    tmp.y /= len;
    normals.push_back(glm::vec2(tmp.y, -tmp.x));
  }

  tmp = squareVertices[3] - squareVertices[0];
  len = glm::length(tmp);
  tmp.x /= len;
  tmp.y /= len;

  normals.push_back(glm::vec2(tmp.y, -tmp.x));

  // Calculates normals for square.
  for (int i = 0; i < squareVertices.size() - 1; i++) {
    tmp = squareVertices[i] - squareVertices[i + 1];
    len = glm::length(tmp);
    tmp.x /= len;
    tmp.y /= len;
    normals.push_back(glm::vec2(tmp.y, -tmp.x));
  }

  // Performs intersection checks between convex hull and square along all their normals
  for (auto normal : normals) {
    if (!check_collision_along_axis(normal, BBVertices, squareVertices)) return false;
  }
  return true;
}

// Calculates the hull of inputed points with Jarvis March method (also called Gift wrapping
// algorithm) and puts them in the second argument called hull.
// OBS! Function needs 3 points in argument points to run.
void calc_hull_jm(const std::vector<glm::vec2>& points, std::vector<glm::vec2>& hull) {
  unsigned int n = points.size();
  unsigned int n_squared = n * n;
  if (n < 3) {
    LOG(WARNING) << "Function calc_hull_jm needs atleast 3 points in the list to run.";
    return;
  }
  // Find the left most point in the x-axis, which will be the start and end of the hull
  int l = 0;
  for (unsigned int i = 1; i < n; i++) {
    if (points[i].x < points[l].x) {
      l = i;
    }
  }
  int p = l, q;
  unsigned int iter = 0;

  do {
    hull.push_back(points[p]);
    q = (p + 1) % n;
    for (unsigned int i = 0; i < n; i++) {
      if (i != p) {
        if (orientation_2d(points[p], points[i], points[q]) == Orientation2D::COUNTERCLOCKWISE) {
          q = i;
        }
      }
    }
    p = q;
    ASSERT(iter++ < n_squared) << "calc_hull_jm error to many iterations\n";
  } while (p != l);
}

Orientation2D orientation_2d(glm::vec2 p1, glm::vec2 p2, glm::vec2 p3) {
  //
  float epsi = 1e-5;
  float val = (p2.y - p1.y) * (p3.x - p2.x) - (p2.x - p1.x) * (p3.y - p2.y);
  if (fabs(val) < epsi) {
    return Orientation2D::COLLINEAR;
  }
  return (val > 0) ? Orientation2D::CLOCKWISE : Orientation2D::COUNTERCLOCKWISE;
}

bool ray_triangle_intersection(glm::vec3 ray_ori, glm::vec3 ray_dir, glm::vec3 vertex0,
                               glm::vec3 vertex1, glm::vec3 vertex2, float& hit_dist) {
  const float EPSILON = 0.0000001;
  glm::vec3 edge1, edge2, h, s, q;
  float a, f, u, v;
  edge1 = vertex1 - vertex0;
  edge2 = vertex2 - vertex0;
  h = glm::cross(ray_dir, edge2);
  a = glm::dot(edge1, h);
  if (a > -EPSILON && a < EPSILON) return false;  // This ray is parallel to this triangle.
  f = 1.0 / a;
  s = ray_ori - vertex0;
  u = f * (glm::dot(s, h));
  if (u < 0.0 || u > 1.0) return false;
  q = glm::cross(s, edge1);
  v = f * glm::dot(ray_dir, q);
  if (v < 0.0 || u + v > 1.0) return false;
  // At this stage we can compute t to find out where the intersection point is on the line.
  float t = f * glm::dot(edge2, q);
  if (t > EPSILON)  // ray intersection
  {
    // glm::vec3 outIntersectionPoint = ray_ori + ray_dir * t;
    hit_dist = t;
    return true;
  } else  // This means that there is a line intersection but not a ray intersection.
    return false;
}
bool ray_triangle_intersection(glm::vec3 ray_ori, glm::vec3 ray_dir, glm::vec3 vertex0,
                               glm::vec3 vertex1, glm::vec3 vertex2) {
  float hit_dist;
  return ray_triangle_intersection(ray_ori, ray_dir, vertex0, vertex1, vertex2, hit_dist);
}

glm::vec3 tri_normal(const glm::vec3& p0, const glm::vec3& p1, const glm::vec3& p2) {
  glm::vec3 edge0 = p1 - p0;
  glm::vec3 edge1 = p2 - p0;

  glm::vec3 norm = glm::cross(edge0, edge1);

  return norm;
}

float triarea2(const glm::vec3& a, const glm::vec3& b, const glm::vec3& c) {
  const float ax = b.x - a.x;
  const float ay = b.y - a.y;
  const float bx = c.x - a.x;
  const float by = c.y - a.y;
  return bx * ay - ax * by;
}

struct PointDist {
  float dist;
  glm::vec3 p;
  PointDist() {
    this->dist = 0.0f;
    this->p = glm::vec3();
  }
  PointDist(float dist, const glm::vec3& p) {
    this->dist = dist;
    this->p = p;
  }
  bool operator>(const PointDist& p) const { return dist > p.dist; }
  bool operator>=(const PointDist& p) const { return dist >= p.dist; }
  bool operator<(const PointDist& p) const { return dist < p.dist; }
  bool operator<=(const PointDist& p) const { return dist <= p.dist; }
  bool operator==(const PointDist& p) const { return dist == p.dist; }
  bool operator!=(const PointDist& p) const { return dist != p.dist; }
};

glm::vec3 point_triangle_line_projection(const glm::vec3& point, const glm::vec3& p0,
                                         const glm::vec3& p1, const glm::vec3& p2,
                                         const glm::vec3& tri_center) {
  std::array<PointDist, 3> dists;
  // get distances from point for each point in the triangle
  dists[0] = PointDist(glm::distance(point, p0), p0);
  dists[1] = PointDist(glm::distance(point, p1), p1);
  dists[2] = PointDist(glm::distance(point, p2), p2);
  // sort by distance
  std::sort(dists.begin(), dists.end());
  glm::vec3 p1p0 = dists[1].p - dists[0].p;
  glm::vec3 ptp0 = point - dists[0].p;
  // project point onto line
  float proj_dist_ptp0 = glm::dot(glm::normalize(p1p0), ptp0);
  glm::vec3 ret_point;
  if (proj_dist_ptp0 < 0) {
    ret_point = dists[0].p;
  } else {
    ret_point = dists[0].p + glm::normalize(p1p0) * proj_dist_ptp0;
  }
  // move point closer to triangle center
  glm::vec3 center_dir = glm::normalize(tri_center - ret_point);
  float dist_from_edge = 2.f;
  BSE_EDIT_NAMED(dist_from_edge, "Navmesh/dist_from_edge");
  ret_point = ret_point + center_dir * dist_from_edge;
  return ret_point;
}
