#include "ai/ai_base.hpp"

#include "ai/ai_component_manager.hpp"
#include "ai/grid.hpp"
#include "ai/state_machine_manager.hpp"
#include "bse/log.hpp"

ai::AIBase::AIBase() {
  m_component_manager = nullptr;
  m_state_machine_mananger = nullptr;
  // m_grid = nullptr;

  // initialize();  // should prob be called from Core, but Core does not exist :(
}

ai::AIBase::~AIBase() {
  // delete m_grid;
}

void ai::AIBase::initialize() {
  m_component_manager = std::make_unique<AIComponentManager>();
  m_state_machine_mananger = std::make_unique<StateMachineManager>();
  // m_grid = new Grid();

  m_component_manager->initialize();
  m_state_machine_mananger->initialize();
  // m_grid->initialize(600, 1800, 1.0, glm::vec2(-300.0f, -400.0f), 0);
  // initialize navmesh
  // int tri1 = m_navmesh.add_triangle({-100000.0f, -100000.0f, 0.0f}, {-100000.0f, 100000.0f,
  // 0.0f},
  //                                  {100000.0f, -100000.0f, 0.0f});
  // int tri2 = m_navmesh.add_triangle({-100000.0f, 100000.0f, 0.0f}, {100000.0f, 100000.0f, 0.0f},
  //                                 {100000.0f, -100000.0f, 0.0f});
  // m_navmesh.add_neighbour(tri1, tri2);
  // m_navmesh.add_neighbour(tri2, tri1);

  LOG(NOTICE) << "AI Base Init() done\n";
}

void ai::AIBase::update() {
  if (m_component_manager) m_component_manager->update();
  if (m_state_machine_mananger) m_state_machine_mananger->update();
  // if (m_grid) m_grid->update();
}

// std::vector<glm::vec3> ai::AIBase::get_path(glm::vec3 start_pos, glm::vec3 end_pos) {
//  return m_navmesh.closest_path(start_pos, end_pos);
//}

// glm::ivec2 ai::AIBase::get_world_to_grid(glm::vec2 pos) { return m_grid->world_to_grid(pos); }