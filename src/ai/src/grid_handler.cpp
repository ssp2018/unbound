#include "ai/grid_handler.hpp"

#include "ai/grid.hpp"

ai::GridHandler::GridHandler() {}

ai::GridHandler::GridHandler(Grid* grid) { m_grid = grid; }

ai::GridHandler::~GridHandler() {}

void ai::GridHandler::initialize_grid(int size_x, int size_y, float size_ratio, glm::vec2 origin,
                                      float value) {
  m_grid->initialize(size_x, size_y, size_ratio, origin, value);
}

void ai::GridHandler::set_spawn_point(glm::vec3 pos, float radius) {
  glm::vec2 world_pos_2d = glm::vec2(pos);
  glm::ivec2 grid_pos = m_grid->world_to_grid(world_pos_2d);
  int grid_radius = m_grid->world_to_grid(radius);
  m_grid->create_charger(grid_pos, grid_radius, 1.0f, FieldType::CONSTANT);
}

void ai::GridHandler::set_impassable_cylinder(glm::vec3 pos, float radius) {
  glm::vec2 world_pos_2d = glm::vec2(pos);
  glm::ivec2 grid_pos = m_grid->world_to_grid(world_pos_2d);
  int grid_radius = m_grid->world_to_grid(radius);
  m_grid->create_charger(grid_pos, grid_radius, -1.0f, FieldType::OVERWRITING_CONSTANT);
}

void ai::GridHandler::set_impassable_obb(glm::vec3 pos, glm::vec3 x_axis, glm::vec3 y_axis,
                                         glm::vec3 z_axis) {
  // Make OBB points out of the axis vectors
  // Do orthographic projection of the OBB points onto the grid(remove z component)
  std::vector<glm::vec3> points(8);
  points[0] = glm::vec3(pos + x_axis + y_axis + z_axis);
  points[1] = glm::vec3(pos - x_axis + y_axis + z_axis);
  points[2] = glm::vec3(pos + x_axis - y_axis + z_axis);
  points[3] = glm::vec3(pos - x_axis - y_axis + z_axis);
  points[4] = glm::vec3(pos + x_axis + y_axis - z_axis);
  points[5] = glm::vec3(pos - x_axis + y_axis - z_axis);
  points[6] = glm::vec3(pos + x_axis - y_axis - z_axis);
  points[7] = glm::vec3(pos - x_axis - y_axis - z_axis);
  std::vector<glm::vec3> hull;
  // Calculate convex hull of the projected points
  // calc_hull_jm(points, hull);

  m_grid->create_chargers_convex_hull(points, -1, FieldType::OVERWRITING_CONSTANT);
  // Run intersection test for convex hull against squares in grid and activate them as
  // OVERWRITING_CONSTANT
}

std::string ai::GridHandler::to_string() { return m_grid->to_string(); }
