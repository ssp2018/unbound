#include "ai/state_base.hpp"

ai::StateBase::StateBase() { this->m_id = ""; }

ai::StateBase::~StateBase() {}

void ai::StateBase::set_id(const std::string& s) { this->m_id = s; }

std::string ai::StateBase::get_id() { return this->m_id; }
