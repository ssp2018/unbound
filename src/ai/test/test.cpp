#include "ai/ai_base.hpp"
#include "ai/ai_component_manager.hpp"
#include "ai/grid.hpp"
#include "ai/grid_handler.hpp"
#include "ai/state_base.hpp"
#include "ai/state_machine_manager.hpp"
#include <ext/ext.hpp>

TEST(Grid, world_to_grid) {
  ai::Grid* grid = new ai::Grid();
  grid->initialize(10, 10, 0.5, {1, 1}, 0);

  EXPECT_EQ(grid->world_to_grid({8, 8}), glm::ivec2(3, 3));
}

TEST(Grid, grid_to_world) {
  ai::Grid* grid = new ai::Grid();
  grid->initialize(10, 10, 0.5, {1, 1}, 0);

  EXPECT_EQ(grid->grid_to_world({2, 2}), glm::vec2(6, 6));
}

TEST(Grid, map_value) {
  ai::Grid* grid = new ai::Grid();
  grid->initialize(10, 10, 1, {0, 0}, 0);

  grid->set_definitive_map_value(glm::vec2(4.5, 4.5), 5);
  EXPECT_EQ(grid->get_map_value(glm::vec2(4.5, 4.5)), 5);

  grid->set_definitive_map_value(glm::vec2(4, 4), 0);

  grid->set_definitive_map_value(glm::ivec2(4.5, 4.5), 5);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(4, 4)), 5);
}

TEST(Grid, closest_path) {
  ai::Grid* grid = new ai::Grid();
  grid->initialize(10, 10, 1, {0, 0}, 0);

  grid->set_definitive_map_value(glm::ivec2(3, 4), -1);
  grid->set_definitive_map_value(glm::ivec2(4, 4), -1);
  grid->set_definitive_map_value(glm::ivec2(5, 4), -1);
  grid->set_definitive_map_value(glm::ivec2(6, 4), -1);

  EXPECT_EQ(grid->closest_path(glm::ivec2(5, 2), glm::ivec2(5, 7)).size(), 2 + 8);
  EXPECT_EQ(grid->closest_path(glm::vec2(5, 2), glm::vec2(5, 7)).size(), 2 + 8);
}

TEST(Grid, move_sub_field) {
  ai::Grid* grid = new ai::Grid();
  grid->initialize(10, 10, 1, {0, 0}, 0);

  grid->create_charger(glm::ivec2(2, 2), 3, 1, ai::FieldType::DECREMENTING);
  grid->move_sub_field_grid(glm::ivec2(2, 2), glm::ivec2(6, 6));

  EXPECT_EQ(grid->get_map_value(glm::ivec2(2, 2)), 0);

  float val = grid->get_map_value(glm::ivec2(5, 5));
  EXPECT_TRUE(val > 0 && val < 1);

  grid->move_sub_field(0, glm::ivec2(8, 8));

  EXPECT_EQ(grid->get_map_value(glm::ivec2(5, 5)), 0);
  float val2 = grid->get_map_value(glm::ivec2(8, 8));
  EXPECT_EQ(grid->get_map_value(glm::ivec2(8, 8)), 1);
}

TEST(Grid, redo_map) {
  ai::Grid* grid = new ai::Grid();
  grid->initialize(10, 10, 1, {0, 0}, 0);

  for (int i = 0; i < 10; ++i) {
    for (int j = 0; j < 10; ++j) {
      grid->set_definitive_map_value(glm::ivec2(i, j), (i * 10 + j + 1));
    }
  }

  grid->redo_map();

  for (int i = 0; i < 10; ++i) {
    for (int j = 0; j < 10; ++j) {
      EXPECT_EQ(grid->get_map_value(glm::ivec2(i, j)), (i * 10 + j + 1));
    }
  }
}

TEST(GridHandler, set_impassable_obb) {
  ai::Grid* grid = new ai::Grid();
  ai::GridHandler grid_handler(grid);
  grid_handler.initialize_grid(5, 5, 1, {0, 0}, 0);

  float halfside = 0.49f;
  grid_handler.set_impassable_obb({2.0f, 2.0f, 5.0f}, {halfside, 0.0f, 0.0f},
                                  {0.0f, halfside, 0.0f}, {0.0f, 0.0f, halfside});

  EXPECT_EQ(grid->get_map_value(glm::ivec2(2, 2)), -1);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(1, 2)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(2, 1)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(3, 2)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(2, 3)), 0);

  grid_handler.set_impassable_obb({2.0f, 2.0f, 5.0f}, {halfside, 0.0f, 0.0f},
                                  {0.0f, 2 * halfside, 0.0f}, {0.0f, 0.0f, halfside});

  EXPECT_EQ(grid->get_map_value(glm::ivec2(0, 0)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(0, 1)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(0, 2)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(0, 3)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(0, 4)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(1, 0)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(1, 1)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(1, 2)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(1, 3)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(1, 4)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(2, 0)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(2, 1)), -1);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(2, 2)), -1);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(2, 3)), -1);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(2, 4)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(3, 0)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(3, 1)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(3, 2)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(3, 3)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(3, 4)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(4, 0)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(4, 1)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(4, 2)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(4, 3)), 0);
  EXPECT_EQ(grid->get_map_value(glm::ivec2(4, 4)), 0);

  delete grid;
}