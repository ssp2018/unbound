set(LAYER_BIN_NAME "${PROJECT_NAME}_bse")

add_library_pch(${LAYER_BIN_NAME}
  src/log.cpp
  src/assert.cpp
  src/allocators.cpp
  src/error.cpp
  src/directory_path.cpp
  src/file_path.cpp
  src/file.cpp
  src/system_data.cpp
  src/serialize.cpp
  src/edit.cpp
  src/task_scheduler.cpp
  src/task_handle.cpp
  src/task_queue.cpp
  src/timer.cpp
  src/tmp/task_bench.cpp
  src/utility.cpp
	src/mesh/animation_clip.cpp
	src/mesh/skeleton.cpp
	src/mesh/mesh_loader.cpp
	src/mesh/mesh_load_util.cpp
  src/mesh_builder.cpp
  src/slot_creator.cpp
	src/memory_debug.cpp
)

target_link_libraries(${LAYER_BIN_NAME}
  PUBLIC
    glm
  PRIVATE
    imgui::imgui
    ${ASSIMP_LIBRARIES}
)

target_include_directories(${LAYER_BIN_NAME}
  PUBLIC 
    include/
)

add_unbound_test(${LAYER_BIN_NAME}
  test/test.cpp
  test/test_runtime_asset.cpp
)