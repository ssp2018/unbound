#pragma once

#ifndef BSE_RESULT_HPP
#define BSE_RESULT_HPP

#include "bse/assert.hpp"
#include "bse/error.hpp"

namespace bse {

// Represents either a successful result, in which case it contains the specified type T,
// or a bse::Error, which holds an error message.
template <typename T>
class Result {
 public:
  // Is initialized to an error state by default
  Result();

  Result(T&& success_result);
  Result(const Error& error_result);
  Result(Error&& error_result);

  Result(Result<T>&& other);

  ~Result() = default;

  Result<T>& operator=(Result&& other);

  // Is used to access the underlying type.
  // Prints an error and aborts the program if the underlying type was the error type.
  T& operator*();
  T* operator->();

  // Implicitly converts to true or false if it contains T or Error, respectively.
  operator bool();
  bool is_valid() const;

  // Logs the stored error if an error is currently held.
  // Aborts the program if it does not hold an error.
  void log_error();

  // Returns the internally held error, if one is contained
  Error get_error() const;

 private:
  std::variant<T, Error> m_result;
};

template <typename T>
Result<T>::Result() : m_result(bse::Error() << "Result was not initialized with a valid value") {}

template <typename T>
Result<T>::Result(T&& success_result) : m_result(std::forward<T>(success_result)) {}

template <typename T>
Result<T>::Result(const Error& error_result) : m_result(error_result) {}

template <typename T>
Result<T>::Result(Error&& error_result) : m_result(error_result) {}

template <typename T>
Result<T>::Result(Result<T>&& other)
    : m_result(std::forward<decltype(other.m_result)>(other.m_result)) {}

template <typename T>
Result<T>& Result<T>::operator=(Result&& other) {
  //
  if (this != &other) {
    m_result = std::move(other.m_result);
  }
  return *this;
}

template <typename T>
T& Result<T>::operator*() {
  //
  if (auto e = std::get_if<bse::Error>(&m_result)) {
    //
    e->break_and_log();
  }
  return std::get<T>(m_result);
}

template <typename T>
T* Result<T>::operator->() {
  //
  if (auto e = std::get_if<bse::Error>(&m_result)) {
    //
    e->break_and_log();
  }
  return &std::get<T>(m_result);
}

template <typename T>
Result<T>::operator bool() {
  //
  return is_valid();
}

template <typename T>
bool Result<T>::is_valid() const {
  //
  return std::holds_alternative<T>(m_result);
}

template <typename T>
void Result<T>::log_error() {
  //
  ASSERT(std::holds_alternative<Error>(m_result)) << "Result cannot log an error if it succeeded!";
  std::get<Error>(m_result).log();
}

template <typename T>
Error Result<T>::get_error() const {
  //
  ASSERT(!is_valid()) << "Result did not contain an error!";
  return std::get<Error>(m_result);
}

}  // namespace bse
#endif  // BSE_RESULT_HPP
