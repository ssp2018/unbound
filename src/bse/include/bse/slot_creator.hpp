#pragma once

#ifndef BSE_SLOT_CREATOR_HPP
#define BSE_SLOT_CREATOR_HPP

namespace bse {
// A slot is a recyclable index.
// Can be used for sparse arrays, for example.
class SlotCreator {
 public:
  SlotCreator();

  // Create slot
  size_t create();

  // Free a slot.
  void destroy(size_t slot);

  // Check if slot is occupied.
  bool exists(size_t slot) const;

 private:
  std::vector<size_t> m_free_slots;
  size_t m_back_slot;
};

}  // namespace bse
#endif  // BSE_SLOT_CREATOR_HPP