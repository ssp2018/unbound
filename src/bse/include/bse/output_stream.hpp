#pragma once

#ifndef BSE_OUTPUT_STREAM_HPP
#define BSE_OUTPUT_STREAM_HPP

#include <bse/assert.hpp>
#include <ext/ext.hpp>

namespace bse {

// Keeps track of a range of WRITABLE values to be used in a streaming task.
template <typename T>
class OutputStream {
 public:
  // Sets the range of values.
  void set_range(T* begin, size_t size);

  // Accesses the range of values.
  T& operator[](size_t index);

  size_t get_size() const;
  size_t get_element_size() const;

 private:
  T* m_data;
  size_t m_size;
  size_t m_size_of_element;
};

template <typename T>
void OutputStream<T>::set_range(T* begin, size_t size) {
  m_data = begin;
  m_size = size;
  m_size_of_element = sizeof(*begin);
}

template <typename T>
size_t OutputStream<T>::get_size() const {
  //
  return m_size;
}

template <typename T>
size_t OutputStream<T>::get_element_size() const {
  //
  return m_size_of_element;
}

template <typename T>
T& OutputStream<T>::operator[](size_t index) {
  //
  // ASSERT(index < m_size) << "Out of range access to OutputStream!\n";

  return m_data[index];
}

}  // namespace bse
#endif  // BSE_OUTPUT_STREAM_HPP
