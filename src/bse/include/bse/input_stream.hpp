#pragma once

#ifndef BSE_INPUT_STREAM_HPP
#define BSE_INPUT_STREAM_HPP

#include <bse/assert.hpp>
#include <ext/ext.hpp>

namespace bse {
// Keeps track of a range of READ-ONLY values to be used in a streaming task.
template <typename T>
class InputStream {
 public:
  // Sets the range of values
  void set_range(T* begin, size_t size);

  // Accesses the range of values
  const T& operator[](size_t index) const;

  size_t get_size() const;
  size_t get_element_size() const;

 private:
  T* m_data;
  size_t m_size;
  size_t m_size_of_element;
};

template <typename T>
void InputStream<T>::set_range(T* begin, size_t size) {
  m_data = begin;
  m_size = size;
  m_size_of_element = sizeof(*begin);
}

template <typename T>
size_t InputStream<T>::get_size() const {
  //
  return m_size;
}

template <typename T>
size_t InputStream<T>::get_element_size() const {
  //
  return m_size_of_element;
}

template <typename T>
const T& InputStream<T>::operator[](size_t index) const {
  //
  // ASSERT(index < m_size) << "Out of range access to InputStream!\n";

  return m_data[index];
}

}  // namespace bse
#endif  // BSE_INPUT_STREAM_HPP
