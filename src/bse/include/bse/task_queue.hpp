#pragma once

#ifndef BSE_TASK_QUEUE_HPP
#define BSE_TASK_QUEUE_HPP

#include "bse/task_handle.hpp"
#include <ext/ext.hpp>

namespace bse {
// Is used to queue tasks in a thread safe manner
class TaskQueue {
 public:
  static constexpr uint32_t MAX_TASKS = 1024u;

  // Adds the given task to the queue.
  void queue_task(TaskHandle handle);

  // Removes the next task from the queue, waits until one is available. (May return an invalid
  // handle after a timeout to avoid threads getting stuck at the end of the program).
  TaskHandle dequeue_task();

  // Removes the next task from the queue, but does not wait. Returns an invalid handle if none were
  // available.
  TaskHandle dequeue_task_if_available();

 private:
  std::array<TaskHandle, MAX_TASKS> m_queued_tasks;
  uint32_t m_next_free_task = 0;
  uint32_t m_next_queued_task = 0;
  std::condition_variable m_queue_cv;
  std::mutex m_queue_lock;
};
}  // namespace bse
#endif  // BSE_TASK_QUEUE_HPP
