#pragma once

#ifndef BSE_TIMER_HPP
#define BSE_TIMER_HPP

#include <ext/ext.hpp>

namespace bse {
// Helper class to time code.
// Acts recursively, meaning it counts how many times it has been started and only
// stops after the same amount of calls to stop.
class Timer {
 public:
  using TimePoint = decltype(std::chrono::high_resolution_clock::now());
  using Clock = std::chrono::high_resolution_clock;

 public:
  // Starts the timer.
  void start();

  // Pauses the timer, stores the accumulated time so far.
  void pause();

  // Resumes the timer
  void resume();

  // Stops the timer and returns the accumulated time only if this is the final call to stop.
  std::optional<float> stop();

  // Essentially calls stop and then start.
  // Assumes that stop and start are on the same level.
  float reset();

  // Calculates the time difference between the last start/resume and the current time.
  float calculate_current_difference();

 private:
 private:
  TimePoint m_last_point;
  float m_accumulated_time = 0.f;
  int m_depth = 0;
};
}  // namespace bse
#endif  // BSE_TIMER_HPP
