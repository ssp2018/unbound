#pragma once

#ifndef BSE_ERROR_HPP
#define BSE_ERROR_HPP

namespace bse {
// A type representing an error that can be returned in conjunction with some other possible result
class Error {
 public:
  Error();
  Error(const Error& other);
  Error(Error&& other);
  ~Error() = default;

  Error& operator=(const Error& other);
  Error& operator=(Error&& other);

  // Acts as a regular stream operator for storing an error message
  template <typename T>
  Error& operator<<(T&& item);

  // Aborts the program and logs the issue (similar to an assert)
  void break_and_log();

  // Logs the issue
  void log();

  std::string get_message() const;

 private:
  std::stringstream m_stream;
};

template <typename T>
Error& Error::operator<<(T&& item) {
  m_stream << item;
  return *this;
}

}  // namespace bse
#endif  // BSE_ERROR_HPP
