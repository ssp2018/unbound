#ifndef _BSE_MEMORY_DEBUG_
#define _BSE_MEMORY_DEBUG_
#include <map>

// namespace bse {
struct AllocInfo {
  const char *file;
  int line;
  size_t size;
};

void report_memory_leaks();
void report_size_per_file();

std::map<void *, AllocInfo> &get_allocation_map();
// }  // namespace bse

#endif;