#pragma once

#ifndef BSE_LOG_HPP
#define BSE_LOG_HPP

#include <ext/ext.hpp>

// The available severity-levels for logging purposes
constexpr int NOTICE = 0;
constexpr int WARNING = 1;
constexpr int FATAL = 2;

namespace bse {

// If no log level was provided, default to the lowest available
#if !defined(UNBOUND_MIN_LOG)
#define UNBOUND_MIN_LOG NOTICE
#endif

// Provides logging functionality through the stream (<<) operator. The level is provided in the
// constructor
class Log {
 public:
  Log(const int level);
  ~Log();

  // Outputs any streamable object if logging is enabled
  template <typename T>
  Log& operator<<(T&& item);

 private:
#if !defined(UNBOUND_NO_LOG)
  const int m_level;
  std::lock_guard<std::mutex> m_lock;
#endif
};

template <typename T>
Log& Log::operator<<(T&& item) {
//
#if !defined(UNBOUND_NO_LOG)
  switch (m_level) {
#if NOTICE >= UNBOUND_MIN_LOG
    case NOTICE:
      std::cout << item;
      break;
#endif

#if WARNING >= UNBOUND_MIN_LOG
    case WARNING:
      std::cout << item;
      break;
#endif

#if FATAL >= UNBOUND_MIN_LOG
    case FATAL:
      std::cerr << item;
      break;
#endif
  }
#endif
  return *this;
}

// An assisting macro used to easily print messages of different severity levels
#define LOG(level)                                                         \
  bse::Log((level)) << __FILE__ << " : " << __LINE__ << " "                \
                    << (level == NOTICE ? "(Notice) "                      \
                                        : (level == WARNING ? "(Warning) " \
                                                            : (level == FATAL ? "(Fatal) " : "")))

template <>
Log& Log::operator<<<const std::string_view&>(const std::string_view& string);

}  // namespace bse
#endif  // BSE_LOG_HPP
