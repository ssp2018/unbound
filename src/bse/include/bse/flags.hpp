#pragma once

#ifndef BSE_FLAGS_HPP
#define BSE_FLAGS_HPP

#include <bse/assert.hpp>

namespace bse {

// Until a wrapper for bitset is needed (if ever), use an alias.
template <int N>
using Flags = std::bitset<N>;

// // A class-based representation of a bitfield that allows users to set an arbitrary number of
// bits template <int N> class Flags {
//   static_assert(N > 0, "Can only have a positive number of flags");

//  public:
//   // Sets the given bit to 1
//   void set(int index);

//   // Sets the given bit to 0
//   void unset(int index);

//   // Flips the given bit
//   void flip(int index);

//   // Returns whether the given bit is currently set or unset
//   bool is_set(int index);

//   // Implicitly converts to true if any bit is set, otherwise false
//   operator bool() const;

//   // Checks equality between Flags instances
//   bool operator==(const Flags<N>& other) const;

//   // Performs & operator on the entire bitfield;
//   Flags<N> operator&(const Flags<N>& other) const;

//   // Performs | operator on the entire bitfield;
//   Flags<N> operator|(const Flags<N>& other) const;

//   // Performs ^ operator on the entire bitfield;
//   Flags<N> operator^(const Flags<N>& other) const;

//  private:
//   std::array<uint64_t, (N - 1) / 64 + 1> m_sub_fields = {0};
// };

// template <int N>
// void Flags<N>::set(int index) {
//   //
//   ASSERT(index >= 0 && index < N) << "Flag index was out of range";

//   const int field_index = index / 64;
//   const int internal_index = index % 64;

//   m_sub_fields[field_index] |= (1 << internal_index);
// }

// template <int N>
// void Flags<N>::unset(int index) {
//   //
//   ASSERT(index >= 0 && index < N) << "Flag index was out of range";

//   const int field_index = index / 64;
//   const int internal_index = index % 64;

//   m_sub_fields[field_index] &= (1 << internal_index);
// }

// template <int N>
// void Flags<N>::flip(int index) {
//   //
//   ASSERT(index >= 0 && index < N) << "Flag index was out of range";

//   const int field_index = index / 64;
//   const int internal_index = index % 64;

//   m_sub_fields[field_index] ^= (1 << internal_index);
// }

// template <int N>
// bool Flags<N>::is_set(int index) {
//   //
//   ASSERT(index >= 0 && index < N) << "Flag index was out of range";

//   const int field_index = index / 64;
//   const int internal_index = index % 64;

//   return m_sub_fields[field_index] & (1 << internal_index);
// }

// template <int N>
// Flags<N>::operator bool() const {
//   //
//   for (auto& sub_field : m_sub_fields) {
//     if (sub_field > 0) {
//       return true;
//     }
//   }
// }

// template <int N>
// bool Flags<N>::operator==(const Flags<N>& other) const {
//   //
//   return m_sub_fields == other.m_sub_fields;
// }

// template <int N>
// Flags<N> Flags<N>::operator&(const Flags<N>& other) const {
//   //
//   Flags<N> result;
//   for (int i = 0; i < m_sub_fields.size(); ++i) {
//     result.m_sub_fields[i] = m_sub_fields[i] & other.m_sub_fields[i];
//   }
//   return result;
// }

// template <int N>
// Flags<N> Flags<N>::operator|(const Flags<N>& other) const {
//   //
//   Flags<N> result;
//   for (int i = 0; i < m_sub_fields.size(); ++i) {
//     result.m_sub_fields[i] = m_sub_fields[i] | other.m_sub_fields[i];
//   }
//   return result;
// }

// template <int N>
// Flags<N> Flags<N>::operator^(const Flags<N>& other) const {
//   //
//   Flags<N> result;
//   for (int i = 0; i < m_sub_fields.size(); ++i) {
//     result.m_sub_fields[i] = m_sub_fields[i] ^ other.m_sub_fields[i];
//   }
//   return result;
// }
}  // namespace bse

#endif  // BSE_FLAGS_HPP