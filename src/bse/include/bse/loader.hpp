#pragma once

#ifndef BSE_LOADER_HPP
#define BSE_LOADER_HPP

#include "bse/assert.hpp"
#include "bse/asset_traits.hpp"
#include "bse/file_path.hpp"
#include "bse/result.hpp"
#include "bse/task_scheduler.hpp"
#include <ext/ext.hpp>

namespace bse {

template <typename T>
class Asset;

template <typename T>
class Loader;

// The AssetID is an implementation detail behind assets.
// Different asset types may provide a specialization of the ID for internal purposes.
template <typename T>
using AssetID = int32_t;

template <typename T>
struct AssetTask : public bse::Task {
  void kernel() override;
  virtual ~AssetTask<T>() = default;
  AssetID<T> asset_id;
  bse::FilePath path;
};

struct LoaderProperties {
  bool restrict_to_thread = false;
  bse::TaskScheduler::Thread thread = bse::TaskScheduler::Thread::MAIN;
  bool restrict_to_caller_thread = false;
  bool should_fallback = false;
  bse::FilePath fallback_path;
};

template <typename T>
void AssetTask<T>::kernel() {
  //

  // LOG(NOTICE) << "Loading from thread " << bse::TaskScheduler::get()->get_thread_id()
  //             << "\nResource: " << path << "\n";

  Result<T> new_raw_asset;
  new_raw_asset = T::load(path);
  // ASSERT(new_raw_asset.is_valid()) << "Failed to load asset!";
  if (!new_raw_asset.is_valid()) {
    LOG(WARNING) << "Failed to load asset: " << path
                 << "\n-> With Error: " << new_raw_asset.get_error().get_message() << '\n';
    return;
  }

  // LOG(NOTICE) << "Finished loading on thread  " << bse::TaskScheduler::get()->get_thread_id()
  //             << '\n';

  bse::Loader<T>::set_promise(asset_id, std::move(*new_raw_asset));
}

template <typename T>
struct AssetPromise {
  std::optional<T> asset = std::nullopt;
  bse::TaskHandle load_task;

  friend class Loader<T>;
};

// Is used to "cache" assets during runtime by loading them once when first needed, and then
// repeatedly issuing them when called subsequent times. This class SHOULD NOT be used manually.
// Asset provides a more usable interface into the loader.
//
// When a special resource calls for it, the Loader (and Asset) can be specialized to enforce a
// consistent outwards facing interface regardless of implementation details.
template <typename T>
class Loader {
  static_assert(is_asset_v<T>, UNBOUND_ASSET_ERROR_MESSAGE);

 public:
  static void destroy();

  // Reloads all assets stored in this loader
  static void refresh();

  // Sets properties related to the loader
  static void set_properties(bool restrict_to_thread, bse::TaskScheduler::Thread thread,
                             bool restrict_to_caller_thread, bool should_fallback,
                             bse::FilePath fallback_path);

 private:
  // Provides a unique AssetID for the asset pointed at by the given path.
  static AssetID<T> get_asset_id(bse::FilePath path);

  // Provides a safe (const) reference to the true asset.
  static const T& get(AssetID<T> id);

  // Returns the path associated with the given asset ID.
  static const bse::FilePath& get_path(AssetID<T> id);

  // Sets the promise after a task has been finished
  static void set_promise(AssetID<T> id, T&& asset);

  // Returns true if the asset is valid
  static bool is_valid(AssetID<T> id);

  static std::unordered_map<bse::FilePath, AssetID<T>>& get_path_to_id_map();
  static std::unordered_map<AssetID<T>, bse::FilePath>& get_id_to_path_map();

  // static std::unordered_map<bse::FilePath, AssetID<T>> m_path_to_id_map;
  // static std::unordered_map<AssetID<T>, bse::FilePath> m_id_to_path_map;
  static std::array<AssetPromise<T>, 50> m_raw_assets;

  // static std::mutex m_load_mutex;

  static std::atomic<AssetID<T>> m_next_id;

  static LoaderProperties m_properties;

  friend class Asset<T>;
  friend class AssetTask<T>;
};

// template <typename T>
// std::unordered_map<bse::FilePath, AssetID<T>> Loader<T>::m_path_to_id_map;

// template <typename T>
// std::unordered_map<AssetID<T>, bse::FilePath> Loader<T>::m_id_to_path_map;

template <typename T>
std::array<AssetPromise<T>, 50> Loader<T>::m_raw_assets;

// template <typename T>
// std::mutex Loader<T>::m_load_mutex;

template <typename T>
std::atomic<AssetID<T>> Loader<T>::m_next_id = 0;

template <typename T>
LoaderProperties Loader<T>::m_properties;

template <typename T>
void Loader<T>::destroy() {
  //
  for (auto& raw_asset : m_raw_assets) {
    raw_asset.asset = std::nullopt;
  }
}

template <typename T>
void Loader<T>::refresh() {
  //

  for (auto& [path, id] : get_path_to_id_map()) {
    Result<T> new_raw_asset;
    new_raw_asset = T::load(path);
    if (new_raw_asset.is_valid()) {
      m_raw_assets[id].asset = std::move(*new_raw_asset);
    } else {
      LOG(WARNING) << "Asset failed to refresh. Was it removed? (" << path << ")\n";
    }
  }
}

template <typename T>
void Loader<T>::set_properties(bool restrict_to_thread, bse::TaskScheduler::Thread thread,
                               bool restrict_to_caller_thread, bool should_fallback,
                               bse::FilePath fallback_path) {
  //
  m_properties.restrict_to_thread = restrict_to_thread;
  m_properties.thread = thread;
  m_properties.restrict_to_caller_thread = restrict_to_caller_thread;
  m_properties.should_fallback = should_fallback;
  m_properties.fallback_path = fallback_path;
}

template <typename T>
AssetID<T> Loader<T>::get_asset_id(bse::FilePath path) {
  //
  ASSERT(path.get_string() != "") << "Asset path may not be empty!";

  if (auto found = get_path_to_id_map().find(path); found != get_path_to_id_map().end()) {
    return found->second;
  } else {
    bse::TaskScheduler* ts = bse::TaskScheduler::get();

    AssetID<T> new_id = m_next_id++;

    get_path_to_id_map()[path] = new_id;
    get_id_to_path_map()[new_id] = path;

    AssetTask<T> task;
    task.asset_id = new_id;
    task.path = path;

    if (m_properties.restrict_to_caller_thread) {
      task.kernel();
    } else if (m_properties.restrict_to_thread) {
      m_raw_assets[new_id].load_task = ts->add_task_to_thread(m_properties.thread, task);
    } else {
      m_raw_assets[new_id].load_task = ts->add_task(task);
    }

    return new_id;
  }
}

template <typename T>
const T& Loader<T>::get(AssetID<T> id) {
  //
  if (!m_raw_assets[id].asset.has_value()) {
    // if (m_raw_assets[id].load_task != bse::TaskHandle::NULL_HANDLE) {
    bse::TaskScheduler* ts = bse::TaskScheduler::get();
    ts->wait_on_task(m_raw_assets[id].load_task);
    ASSERT(m_raw_assets[id].asset.has_value())
        << "Asset was not initialized with a valid resource! Check filenames.";
  }
  return *m_raw_assets[id].asset;
}

template <typename T>
const bse::FilePath& Loader<T>::get_path(AssetID<T> id) {
  //
  return get_id_to_path_map()[id];
}

template <typename T>
void Loader<T>::set_promise(AssetID<T> id, T&& asset) {
  //
  m_raw_assets[id].asset = std::move(asset);
  m_raw_assets[id].load_task = bse::TaskHandle::NULL_HANDLE;
}

template <typename T>
bool Loader<T>::is_valid(AssetID<T> id) {
  //
  if (id < 0) {
    return false;
  }

  if (m_raw_assets[id].load_task != bse::TaskHandle::NULL_HANDLE) {
    bse::TaskScheduler* ts = bse::TaskScheduler::get();
    ts->wait_on_task(m_raw_assets[id].load_task);
  }
  return m_raw_assets[id].asset.has_value();
}

template <typename T>
std::unordered_map<bse::FilePath, AssetID<T>>& Loader<T>::get_path_to_id_map() {
  static std::unordered_map<bse::FilePath, AssetID<T>> map;
  return map;
}

template <typename T>
std::unordered_map<AssetID<T>, bse::FilePath>& Loader<T>::get_id_to_path_map() {
  static std::unordered_map<AssetID<T>, bse::FilePath> map;
  return map;
}

}  // namespace bse
#endif  // BSE_LOADER_HPP
