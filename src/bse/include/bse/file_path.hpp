#pragma once

#ifndef BSE_FILE_PATH_HPP
#define BSE_FILE_PATH_HPP

#include "bse/serialize.hpp"
#include <bse/directory_path.hpp>

namespace bse {

// Representation of a complete file path.
// Must contain a filename and an extension
class FilePath {
 public:
  FilePath() = default;
  FilePath(std::string str);
  FilePath(const char* str);
  ~FilePath();

  bool operator==(const FilePath& other) const;

  operator const std::string() const;

  // Gets and sets only the filename
  std::string get_name() const;
  void set_name(std::string name);

  // Gets and sets only the extension
  std::string get_extension();
  void set_extension(std::string extension);

  // Returns the full string (path + filename + extension)
  std::string get_string() const;

  // Returns only the path
  const DirectoryPath& get_directory() const;

  // Returns the filename + extension
  std::string get_file() const;

  // Changes the path without changing the file
  void set_directory(DirectoryPath path);

  CLASS_SERFUN((FilePath)) {
    SERIALIZE(self.m_file_str);
    SERIALIZE(self.m_extension_point);
    SERIALIZE(self.m_directory);
  }
  CLASS_DESERFUN((FilePath)) {
    DESERIALIZE(self.m_file_str);
    DESERIALIZE(self.m_extension_point);
    DESERIALIZE(self.m_directory);
  }

 private:
  // Processes the current string to conform to our standards as much as possible
  void clean_path();

  std::string m_file_str;
  int m_extension_point;

  DirectoryPath m_directory;
};

std::ostream& operator<<(std::ostream& os, const bse::FilePath& fp);
}  // namespace bse

bse::FilePath operator/(const bse::DirectoryPath& left, const bse::FilePath& right);
bse::FilePath operator""_fp(const char* op, std::size_t size);

// Override hash functionality for FilePath
// (Required to be used in unordered_map)
namespace std {
template <>
struct hash<bse::FilePath> {
  size_t operator()(const bse::FilePath& fp) const {
    return hash<std::string>()(fp.get_string());  //
  }
};
}  // namespace std

#endif  // BSE_FILE_PATH_HPP
