#pragma once

#ifndef BSE_ASSERT_HPP
#define BSE_ASSERT_HPP

#include "bse/log.hpp"

namespace bse {
//

namespace detail {

// An implementation detail behind the assertion macro.
// It calls abort when its destructor is called.
// Should NOT be used outside of the macro.
class Assert {
 public:
  Assert(const bool cond);
  ~Assert();

 private:
  const bool m_cond;
};
}  // namespace detail

// The assert macro allows for any condition to be checked, and if it fails it will print everything
// sent to it through the stream (<<) operator (as an error) and then abort.
#define ASSERT(cond)                               \
  if (auto a = bse::detail::Assert(cond); !(cond)) \
  LOG(FATAL) << "ASSERTION FAILED (" << #cond << ") "

}  // namespace bse

#endif  // BSE_ASSERT_HPP