#pragma once

#ifndef BSE_SLOT_VECTOR_TCC
#define BSE_SLOT_VECTOR_TCC

#include <bse/assert.hpp>
#include <bse/slot_vector.hpp>

namespace bse {

template <typename T>
size_t SlotVector<T>::insert(const T& t) {
  size_t slot = m_slots.create();
  if (slot >= m_data.size()) {
    m_data.resize(slot + 1);
  }
  m_data[slot] = t;
  m_size++;
  return slot;
}

template <typename T>
bool SlotVector<T>::exists(size_t slot) const {
  return m_slots.exists(slot);
}

template <typename T>
bool SlotVector<T>::erase(size_t slot) {
  if (!exists(slot)) {
    return false;
  }

  m_size--;
  m_slots.destroy(slot);
  return true;
}

template <typename T>
T& SlotVector<T>::operator[](size_t slot) {
  ASSERT(exists(slot)) << "Indexing null element";
  return m_data[slot];
}

template <typename T>
const T& SlotVector<T>::operator[](size_t slot) const {
  ASSERT(exists(slot)) << "Indexing null element";
  return m_data[slot];
}

template <typename T>
size_t SlotVector<T>::size() const {
  return m_size;
}
template <typename T>
size_t SlotVector<T>::capacity() const {
  return m_data.size();
}

template <typename T>
size_t SlotVector<T>::find(const T& t) const {
  for (size_t i = 0; i < m_data.size(); i++) {
    if (m_slots.exists(i) && m_data[i] == t) {  // Really inefficient
      return i;
    }
  }
  return -1;
}

}  // namespace bse
#endif  // BSE_SLOT_VECTOR_TCC