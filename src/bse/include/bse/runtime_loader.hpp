#pragma once

#ifndef BSE_RUNTIME_LOADER_HPP
#define BSE_RUNTIME_LOADER_HPP

#include "bse/assert.hpp"
#include <ext/ext.hpp>

namespace bse {

// Forward-declaration
template <typename T>
class RuntimeAsset;

// The ID representing a runtime asset in the loader
template <typename T>
using RuntimeAssetID = int32_t;

// Keeps track of (but doesn't store) all runtime resources so that they can be collectively
// stored and restored
template <typename T>
class RuntimeLoader {
 public:
  // Calls store and restore
  static void refresh();

  // Stores the state of all runtime assets of type T
  static void store();

  // Restores the state of all runtime assets of type T
  static void restore();

 private:
  // Returns a new handle to the runtime asset
  static RuntimeAssetID<T> create_handle(RuntimeAsset<T>* asset);

  // Updates the pointer corresponding to a given handle
  static void update_handle(RuntimeAssetID<T> id, RuntimeAsset<T>* asset);

  // Destroys a handle
  static void destroy_handle(RuntimeAssetID<T> id);

 private:
  static std::vector<RuntimeAsset<T>*> m_runtime_assets;
  static std::vector<RuntimeAssetID<T>> m_free_ids;

  friend class RuntimeAsset<T>;
};

template <typename T>
std::vector<RuntimeAsset<T>*> RuntimeLoader<T>::m_runtime_assets;

template <typename T>
std::vector<RuntimeAssetID<T>> RuntimeLoader<T>::m_free_ids;

template <typename T>
void RuntimeLoader<T>::refresh() {
  //
  store();
  restore();
}

template <typename T>
void RuntimeLoader<T>::store() {
  //
  for (RuntimeAsset<T>* asset : m_runtime_assets) {
    if (asset) {
      asset->store();
    }
  }
}

template <typename T>
void RuntimeLoader<T>::restore() {
  //
  for (RuntimeAsset<T>* asset : m_runtime_assets) {
    if (asset) {
      asset->restore();
    }
  }
}

template <typename T>
RuntimeAssetID<T> RuntimeLoader<T>::create_handle(RuntimeAsset<T>* asset) {
  //
  if (m_free_ids.empty()) {
    constexpr size_t RESIZE_STEP = 25;

    m_free_ids.resize(RESIZE_STEP);
    m_runtime_assets.reserve(m_runtime_assets.size() + RESIZE_STEP);

    for (int i = 0; i < RESIZE_STEP; i++) {
      RuntimeAssetID<T> new_id;
      new_id = m_runtime_assets.size();
      m_free_ids[RESIZE_STEP - i - 1] = new_id;

      m_runtime_assets.push_back(nullptr);
    }
  }

  RuntimeAssetID<T> id = m_free_ids.back();
  m_free_ids.pop_back();

  m_runtime_assets[id] = asset;

  return id;
}

template <typename T>
void RuntimeLoader<T>::update_handle(RuntimeAssetID<T> id, RuntimeAsset<T>* asset) {
  //
  ASSERT(id >= 0) << "Tried to update a runtime asset that didn't exist!";
  ASSERT(id < m_runtime_assets.size()) << "Tried to update a runtime asset that didn't exist!";

  m_runtime_assets[id] = asset;
}

template <typename T>
void RuntimeLoader<T>::destroy_handle(RuntimeAssetID<T> id) {
  //
  m_runtime_assets[id] = nullptr;
  m_free_ids.push_back(id);
}

}  // namespace bse
#endif  // BSE_RUNTIME_LOADER_HPP
