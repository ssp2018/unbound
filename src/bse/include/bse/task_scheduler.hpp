#pragma once

#ifndef BSE_TASK_SCHEDULER_HPP
#define BSE_TASK_SCHEDULER_HPP

#include "bse/assert.hpp"
#include "bse/input_stream.hpp"
#include "bse/log.hpp"
#include "bse/output_stream.hpp"
#include "bse/task_handle.hpp"
#include "bse/task_queue.hpp"
#include "bse/timer.hpp"
#include <ext/ext.hpp>
namespace bse {
// Defines the underlying interface of basic tasks.
namespace detail {
class AbstractTask {
 public:
  virtual void kernel() = 0;
  virtual ~AbstractTask() = default;
};
}  // namespace detail

// Aliases for everyday use.
using Task = detail::AbstractTask;
using UniTask = detail::AbstractTask;

// Collects useful parameters for streaming tasks.
struct StreamInfo {
  uint32_t num_slices;
  uint32_t inputs_per_slice;
  uint32_t outputs_per_slice;
};

// Defines the interface for 'streaming tasks' which provides the task with an input and an output
// stream. The benefit of streaming tasks is that the TaskScheduler implementation can use
// heuristics to split up the work into smaller tasks based on user hints.
template <typename In, typename Out>
class StreamingTask : public detail::AbstractTask {
 public:
  virtual ~StreamingTask() = default;
  virtual void kernel(bse::InputStream<In>& input, bse::OutputStream<Out>& output,
                      StreamInfo& stream_info) = 0;

  void kernel() final override {
    kernel(m_input_stream, m_output_stream, m_stream_info);  //
  }

 private:
  bse::InputStream<In> m_input_stream;
  bse::OutputStream<Out> m_output_stream;
  StreamInfo m_stream_info;

  friend class TaskScheduler;
};

// Emptyy task used in special logic (streaming tasks)
class EmptyTask : public Task {
 public:
  virtual ~EmptyTask() = default;
  void kernel() override{};
};

// The TaskScheduler is responsible for issuing tasks to a thread-pool.
// Based on:
// https://blog.molecular-matters.com/2012/04/05/building-a-load-balanced-task-scheduler-part-1-basics/
class TaskScheduler {
 public:
  static constexpr uint32_t NUM_THREADS = 4;

  struct Diagnostics {
    float total_util = 0.f;
    std::array<float, NUM_THREADS + 1> thread_util = {0.f};
  };

  enum class Thread {
    MAIN,      //
    RENDER,    //
    THREAD_1,  //
    THREAD_2,  //
    THREAD_3,  //
    THREAD_4,  //
    THREAD_5,  //
    THREAD_6,  //
    THREAD_7   //
  };

 public:
  ~TaskScheduler();

  // Returns the singleton instance of the TaskScheduler.
  static TaskScheduler* get();
  // Destroys the singleton instance.
  static void destroy();

  // Adds a basic task to the queue.
  template <typename Derived>
  TaskHandle add_task(Derived& task_impl);

  // Adds a basic task to only one thread queue.
  template <typename Derived>
  TaskHandle add_task_to_thread(Thread thread, Derived& task_impl);

  // Adds a uni (universal) task to the queue that must run on each thread once.
  template <typename Derived>
  TaskHandle add_uni_task(Derived& task_impl);

  // Adds the given tasks and returns a handle to their parent.
  template <typename Derived>
  TaskHandle add_task_group(std::vector<Derived>& task_impls);

  // Adds and divides a streaming task and adds the subtasks to the queue.
  template <typename Derived, typename In, typename Out>
  TaskHandle add_streaming_task(Derived& task_impl, InputStream<In> input_stream,
                                OutputStream<Out> output_stream, int inputs_per_slice,
                                int outputs_per_slice);

  // Forces the current thread to wait until all tasks in the pipeline are completed.
  // WARNING: MUST NEVER BE CALLED FROM A TASK
  void wait_until_idle();

  // Forces the current thread to wait until the given task has completed.
  // If the task has not finished the thread will NOT block IF there are more tasks
  // queued up. Otherwise, the thread will yield itself to the OS to save resources.
  void wait_on_task(TaskHandle handle);

  // Returns true of the given task is finished.
  bool is_finished(TaskHandle handle);

  // Returns diagnostics for the running threads since last time this function was called.
  Diagnostics compile_diagnostics();

  // Returns the ID of the current thread
  uint32_t get_thread_id();

 private:
  // Helper representation of a task in the TaskScheduler.
  struct ScheduledTask {
    ScheduledTask* unused_next = nullptr;
    std::atomic<uint32_t> open_tasks;
    TaskHandle handle;
    TaskHandle parent_handle;
    std::unique_ptr<detail::AbstractTask> task;
  };

  // Helper collection of data calculated by the 'calculate_stream_division' method.
  struct StreamDivision {
    uint32_t num_slices;
    uint32_t max_slices_per_task;
  };

 private:
  // Private constructor to be used by singleton-pattern.
  TaskScheduler();

  // The central loop to each thread.
  void task_worker(int id);

  // Begins work on the given task on the current thread.
  void work_on_task(ScheduledTask* task);

  // Returns a task from any queue or nullptr after a timeout
  ScheduledTask* dequeue_task();

  // Returns a task from any queue or nullptr if none were available
  ScheduledTask* dequeue_task_if_available();

  // Returns the task represented by the given handle.
  ScheduledTask* get_task(TaskHandle handle);

  // Checks whether the given task CAN be executed or if it is blocked by its children.
  bool can_execute_task(ScheduledTask* task);

  // Works if work is available or yields resources.
  void help_with_work();

  // Decrements the 'open_tasks' field and deallocates the task if open_tasks becomes 0.
  void finish_task(ScheduledTask* task);

  // Calculates how a streaming task should be split up.
  StreamDivision calculate_stream_division(uint32_t input_size, uint32_t output_size,
                                           uint32_t output_element_size, int inputs_per_slice,
                                           int outputs_per_slice);

  // Allocates a task that can be placed into the queue.
  ScheduledTask* allocate_task();

  // Frees the task to be allocated at a later date.
  void deallocate_task(ScheduledTask* task);

 private:
  std::array<ScheduledTask, TaskQueue::MAX_TASKS> m_tasks;
  ScheduledTask* m_next_free;
  std::mutex m_allocation_lock;

  bse::TaskQueue m_task_queue;
  std::array<bse::TaskQueue, NUM_THREADS + 1> m_thread_specific_task_queues;

  std::atomic<uint32_t> m_num_running_tasks = 0;

  std::array<float, NUM_THREADS + 1> m_thread_work_time;
  std::array<bse::Timer, NUM_THREADS + 1> m_thread_work_timer;

  volatile bool m_should_finish = false;

  std::vector<std::thread> m_threads;

  static TaskScheduler* m_singleton;
};

////////////////////////////////////////////
////////////////////////////////////////////
////////////////////////////////////////////

template <typename Derived>
TaskHandle TaskScheduler::add_task(Derived& task_impl) {
  //
  static_assert(std::is_base_of_v<Task, Derived>,
                "User provided task must derive from 'bse::Task'");
  static_assert(std::is_copy_constructible_v<Derived>, "User provided task must be copyable");

  TaskScheduler::ScheduledTask* task = allocate_task();

  // task->task.reset(new Derived(task_impl));
  task->task = std::make_unique<Derived>(task_impl);
  task->open_tasks = 1;

  m_task_queue.queue_task(task->handle);

  return task->handle;
}

template <typename Derived>
TaskHandle TaskScheduler::add_task_to_thread(Thread thread, Derived& task_impl) {
  //
  static_assert(std::is_base_of_v<Task, Derived>,
                "User provided task must derive from 'bse::Task'");
  static_assert(std::is_copy_constructible_v<Derived>, "User provided task must be copyable");

  ASSERT((int)thread < NUM_THREADS + 1) << "Cannot submit to thread (" << (int)thread << "). Only "
                                        << NUM_THREADS + 1 << " thread(s) are available.";

  TaskScheduler::ScheduledTask* task = allocate_task();

  task->task.reset(new Derived(task_impl));
  task->open_tasks = 1;

  m_thread_specific_task_queues[(int)thread].queue_task(task->handle);

  return task->handle;
}

template <typename Derived>
TaskHandle TaskScheduler::add_uni_task(Derived& task_impl) {
  //
  static_assert(std::is_base_of_v<UniTask, Derived>,
                "User provided universal task must derive from 'bse::UniTask'");
  static_assert(std::is_copy_constructible_v<Derived>,
                "User provided universal task must be copyable");

  TaskScheduler::ScheduledTask* root = allocate_task();
  root->task.reset(new EmptyTask());
  root->open_tasks = m_thread_specific_task_queues.size() + 1;
  m_task_queue.queue_task(root->handle);

  for (auto& queue : m_thread_specific_task_queues) {
    TaskScheduler::ScheduledTask* task = allocate_task();

    task->task.reset(new Derived(task_impl));
    task->open_tasks = 1;
    task->parent_handle = root->handle;

    queue.queue_task(task->handle);
  }

  // TaskScheduler::ScheduledTask* task = allocate_task();

  // task->task.reset(new Derived(task_impl));
  // task->open_tasks = 1;
  // task->parent_handle = root->handle;

  // work_on_task(task);

  return root->handle;
}

template <typename Derived>
TaskHandle TaskScheduler::add_task_group(std::vector<Derived>& task_impls) {
  //
  static_assert(std::is_base_of_v<Task, Derived>,
                "User provided task must derive from 'bse::Task'");
  static_assert(std::is_copy_constructible_v<Derived>, "User provided task must be copyable");

  TaskScheduler::ScheduledTask* root = allocate_task();
  root->task.reset(new EmptyTask());
  root->open_tasks = task_impls.size() + 1;

  m_task_queue.queue_task(root->handle);

  for (auto& task_impl : task_impls) {
    TaskScheduler::ScheduledTask* child_task = allocate_task();

    child_task->task.reset(new Derived(task_impl));

    child_task->parent_handle = root->handle;
    child_task->open_tasks = 1;

    m_task_queue.queue_task(child_task->handle);
  }

  return root->handle;
}

template <typename Derived, typename In, typename Out>
TaskHandle TaskScheduler::add_streaming_task(Derived& task_impl, bse::InputStream<In> input_stream,
                                             bse::OutputStream<Out> output_stream,
                                             int inputs_per_slice, int outputs_per_slice) {
  //
  static_assert(std::is_base_of_v<StreamingTask<In, Out>, Derived>,
                "User provided streaming task must derive from 'bse::StreamingTask'");
  static_assert(std::is_copy_constructible_v<Derived>,
                "User provided streaming task must be copyable");

  ASSERT(input_stream.get_size() ==
         (std::size_t)std::round(output_stream.get_size() *
                                 ((float)inputs_per_slice / outputs_per_slice)))
      << "Specified \"input_per_slice\" and \"output_per_slice\" in streaming task did not match"
         "given input and output stream sizes!";

  StreamDivision division = calculate_stream_division(
      input_stream.get_size(), output_stream.get_size(), output_stream.get_element_size(),
      inputs_per_slice, outputs_per_slice);

  const uint32_t NUM_TASKS =
      (uint32_t)std::ceil((float)division.num_slices / division.max_slices_per_task);

  TaskScheduler::ScheduledTask* root = allocate_task();
  root->task.reset(new EmptyTask());
  root->open_tasks = NUM_TASKS + 1;

  m_task_queue.queue_task(root->handle);

  In* input_walker = const_cast<In*>(&input_stream[0]);
  Out* output_walker = &output_stream[0];

  for (int i = 0; i < division.num_slices; i += division.max_slices_per_task) {
    const int NUM_SLICES_THIS_TASK =
        std::min(division.num_slices - i, division.max_slices_per_task);

    InputStream<In> is;
    OutputStream<Out> os;

    is.set_range(input_walker,                              // From this address...
                 NUM_SLICES_THIS_TASK * inputs_per_slice);  // Designate this many elements...

    os.set_range(output_walker,                              // From this address...
                 NUM_SLICES_THIS_TASK * outputs_per_slice);  // Designate this many elements...

    // Create a child task to use the calculated slices
    TaskScheduler::ScheduledTask* child_task = allocate_task();

    task_impl.m_input_stream = is;
    task_impl.m_output_stream = os;
    task_impl.m_stream_info.inputs_per_slice = inputs_per_slice;
    task_impl.m_stream_info.outputs_per_slice = outputs_per_slice;
    task_impl.m_stream_info.num_slices = NUM_SLICES_THIS_TASK;
    child_task->task.reset(new Derived(task_impl));

    child_task->parent_handle = root->handle;
    child_task->open_tasks = 1;

    m_task_queue.queue_task(child_task->handle);

    // Progress the walkers to the next chunk
    input_walker += NUM_SLICES_THIS_TASK * inputs_per_slice;
    output_walker += NUM_SLICES_THIS_TASK * outputs_per_slice;
  }

  return root->handle;
}

}  // namespace bse
#endif  // BSE_TASK_SCHEDULER_HPP
