#pragma once

#ifndef BSE_RUNTIME_ASSET_HPP
#define BSE_RUNTIME_ASSET_HPP

#include "bse/runtime_loader.hpp"
#include <bse/serialize.hpp>
#include <ext/ext.hpp>

namespace bse {

// Represents a type of asset that is created during runtime.
// It allows resources to recreate themselves during runtime and later
// during complete game state restorations (serialization, etc.)
template <typename T>
class RuntimeAsset {
 public:
  // Constructor forwards all arguments to the constructor of the asset (T)
  template <typename... CArgs>
  RuntimeAsset(CArgs&&... c_args);
  ~RuntimeAsset();

  RuntimeAsset(RuntimeAsset&& other);
  RuntimeAsset& operator=(RuntimeAsset&& other);

  RuntimeAsset(const RuntimeAsset&) = delete;    // No copy-constructor
  void operator=(const RuntimeAsset&) = delete;  // No assignment operator

  // Explicitly creates a true copy of the resource.
  // For a GPU resource, this would mean there are two instances of the same resource on the GPU.
  RuntimeAsset<T> copy();

  // Returns true if the current id is positive
  bool is_valid();

  // Stores the state of the runtime resource into the intermediate pointer
  void store() const;

  // Restores the state of the runtime resource based on the intermediate pointer
  void restore();

  // Accesses the public methods provided in the asset (T).
  T* operator->();
  const T* operator->() const;

  CLASS_SERFUN((RuntimeAsset<T>)) {
    self.store();
    SERIALIZE(self.m_intermediate);
  }
  CLASS_DESERFUN((RuntimeAsset<T>)) {
    DESERIALIZE(self.m_intermediate);
    self.restore();
  }

 private:
  T m_runtime_asset;
  RuntimeAssetID<T> m_id = -1;

  mutable std::unique_ptr<typename T::Intermediate> m_intermediate = nullptr;
};

template <typename T>
template <typename... CArgs>
RuntimeAsset<T>::RuntimeAsset(CArgs&&... c_args) : m_runtime_asset(std::forward<CArgs>(c_args)...) {
  //
  m_id = bse::RuntimeLoader<T>::create_handle(this);
}

template <typename T>
RuntimeAsset<T>::~RuntimeAsset() {
  //
  if (is_valid()) {
    bse::RuntimeLoader<T>::destroy_handle(m_id);
  }
}

template <typename T>
RuntimeAsset<T>::RuntimeAsset(RuntimeAsset<T>&& other)
    : m_runtime_asset{std::move(other.m_runtime_asset)} {
  //
  m_id = other.m_id;

  other.m_id = -1;

  bse::RuntimeLoader<T>::update_handle(m_id, this);
}

template <typename T>
RuntimeAsset<T>& RuntimeAsset<T>::operator=(RuntimeAsset&& other) {
  //
  if (this != &other) {
    if (is_valid()) {
      bse::RuntimeLoader<T>::destroy_handle(m_id);
    }

    m_runtime_asset = std::move(other.m_runtime_asset);
    m_id = other.m_id;

    other.m_id = -1;

    bse::RuntimeLoader<T>::update_handle(m_id, this);
  }
  return *this;
}

template <typename T>
RuntimeAsset<T> RuntimeAsset<T>::copy() {
  static_assert(std::is_copy_constructible_v<T>,
                "Attempted to copy a runtime asset that was not copyable!");

  return RuntimeAsset<T>(m_runtime_asset);
}

template <typename T>
bool RuntimeAsset<T>::is_valid() {
  //
  return m_id >= 0;
}

template <typename T>
void RuntimeAsset<T>::store() const {
  //
  ASSERT(m_id >= 0) << "Attempted to store an invalid runtime asset!";
  m_intermediate = m_runtime_asset.store();
}

template <typename T>
void RuntimeAsset<T>::restore() {
  //
  ASSERT(m_id >= 0) << "Attempted to restore an invalid runtime asset!";
  ASSERT(m_intermediate != nullptr) << "Did not store runtime asset before restoring!";
  m_runtime_asset.restore(*m_intermediate);
  m_intermediate.reset();
}

template <typename T>
T* RuntimeAsset<T>::operator->() {
  //
  ASSERT(m_id >= 0) << "Attempted to access an invalid runtime asset!";
  return &m_runtime_asset;
}

template <typename T>
const T* RuntimeAsset<T>::operator->() const {
  //
  ASSERT(m_id >= 0) << "Attempted to access an invalid runtime asset!";
  return &m_runtime_asset;
}
}  // namespace bse
#endif  // BSE_RUNTIME_ASSET_HPP
