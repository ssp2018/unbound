#pragma once

#ifndef BSE_SERIALIZE_HPP
#define BSE_SERIALIZE_HPP

#include "bse/reflect.hpp"
#include <bse/assert.hpp>
#include <ext/ext.hpp>

namespace bse {

namespace detail {
template <typename T>
// Extracts bytes from any type T
struct ExtractBytes {
  union {
    T item;
    struct {
      char bytes[sizeof(T)];
    };
  };
};
}  // namespace detail

struct SerializedData {
  std::vector<char> bytes;
  uint64_t head = 0U;

  void reset();
};

template <typename, typename T>
struct has_serialize {
  static_assert(std::integral_constant<T, false>::value,
                "Second template parameter needs to be of function type.");
};

template <typename C, typename Ret, typename... Args>
struct has_serialize<C, Ret(Args...)> {
 private:
  template <typename T>
  static constexpr auto check(T*) ->
      typename std::is_same<decltype(std::declval<T>().serialize(std::declval<Args>()...)),
                            Ret       // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            >::type;  // attempt to call it and see if the return type is correct

  template <typename>
  static constexpr std::false_type check(...);

  typedef decltype(check<C>(0)) type;

 public:
  static constexpr bool value = type::value;
};

template <typename C, typename Ret, typename... Args>
constexpr bool has_serialize_v = has_serialize<C, Ret, Args...>::value;

// SerializedData& operator+=(SerializedData& lhs, const SerializedData& rhs);

template <typename T>
void serialize(const T& unknown, SerializedData& data);

template <typename T>
void deserialize(T& unknown, const SerializedData& data, int& offset);

template <typename T>
void serialize(const std::vector<T>& array_value, SerializedData& data);

template <typename T>
void deserialize(std::vector<T>& array_value, const SerializedData& data, int& offset);

template <typename T, int N>
void serialize(const std::array<T, N>& array_value, SerializedData& data);

template <typename T, int N>
void deserialize(std::array<T, N>& array_value, const SerializedData& data, int& offset);

template <typename KeyT, typename ValueT>
void serialize(const std::unordered_map<KeyT, ValueT>& map_value, SerializedData& data);

template <typename KeyT, typename ValueT>
void deserialize(std::unordered_map<KeyT, ValueT>& map_value, const SerializedData& data,
                 int& offset);

template <typename KeyT, typename ValueT>
void serialize(const std::map<KeyT, ValueT>& map_value, SerializedData& data);

template <typename KeyT, typename ValueT>
void deserialize(std::map<KeyT, ValueT>& map_value, const SerializedData& data, int& offset);

template <typename T>
void serialize(const std::unique_ptr<T>& ptr_value, SerializedData& data);

template <typename T>
void deserialize(std::unique_ptr<T>& ptr_value, const SerializedData& data, int& offset);

namespace detail {
template <int I, typename... MArgs>
void impl_variant_serialize(const std::variant<MArgs...>& variant_value, SerializedData& data);
}  // namespace detail

template <typename... MArgs>
void serialize(const std::variant<MArgs...>& variant_value, SerializedData& data);
namespace detail {
template <int I, typename... MArgs>
void impl_variant_deserialize(std::variant<MArgs...>& variant_value, const SerializedData& data,
                              int& offset, std::size_t index);
}  // namespace detail

template <typename... MArgs>
void deserialize(std::variant<MArgs...>& variant_value, const SerializedData& data, int& offset);
template <typename FirstT, typename SecondT>
void serialize(const std::pair<FirstT, SecondT>& pair_value, SerializedData& data);

template <typename FirstT, typename SecondT>
void deserialize(std::pair<FirstT, SecondT>& pair_value, const SerializedData& data, int& offset);

void serialize(const std::string& str_value, SerializedData& data);
void deserialize(std::string& str_value, const SerializedData& data, int& offset);

void serialize(const glm::vec2& vec_value, SerializedData& data);
void serialize(const glm::vec3& vec_value, SerializedData& data);
void serialize(const glm::vec4& vec_value, SerializedData& data);
void serialize(const glm::ivec2& vec_value, SerializedData& data);
void serialize(const glm::ivec3& vec_value, SerializedData& data);
void serialize(const glm::ivec4& vec_value, SerializedData& data);
void serialize(const glm::mat2& mat_value, SerializedData& data);
void serialize(const glm::mat3& mat_value, SerializedData& data);
void serialize(const glm::mat4& mat_value, SerializedData& data);

void deserialize(glm::vec2& vec_value, const SerializedData& data, int& offset);
void deserialize(glm::vec3& vec_value, const SerializedData& data, int& offset);
void deserialize(glm::vec4& vec_value, const SerializedData& data, int& offset);
void deserialize(glm::ivec2& vec_value, const SerializedData& data, int& offset);
void deserialize(glm::ivec3& vec_value, const SerializedData& data, int& offset);
void deserialize(glm::ivec4& vec_value, const SerializedData& data, int& offset);
void deserialize(glm::mat2& mat_value, const SerializedData& data, int& offset);
void deserialize(glm::mat3& mat_value, const SerializedData& data, int& offset);
void deserialize(glm::mat4& mat_value, const SerializedData& data, int& offset);

void serialize(const std::deque<float>& value, bse::SerializedData& data);
void deserialize(std::deque<float>& value, const bse::SerializedData& data, int& offset);

template <typename T>
void serialize(const T& unknown, SerializedData& data) {
  if constexpr (std::is_trivially_copyable_v<T>) {
    // If the array has not been allocated yet, allocate 128 KiB (should be enough for the entire
    // game state)
    // if (data.bytes.size() == 0) {
    //   data.bytes.resize(1 << 17);
    // }

    // data.bytes.resize(sizeof(unknown));

    // Prefer memcpy for trivial types
    memcpy(&data.bytes[data.head], &unknown, sizeof(unknown));
    data.head += sizeof(unknown);

    return;
  } else if constexpr (bse::has_serialize_v<T, SerializedData()>) {
    unknown.serialize(data);
    return;

  } else if constexpr (bse::meta<T>::is_defined) {
    // Otherwise, if reflectable, use reflection to serialize all given types
    bse::for_each_field((T&)unknown, [&data](const auto& field) {
      if constexpr (!std::is_member_function_pointer_v<decltype(field.value)>) {
        serialize(field.value, data);  //
      }
    });
    return;

  } else {
    unknown.serialize(data);

    // static_assert(bse::has_serialize_method_v<T> || std::is_trivially_copyable_v<T> ||
    //                   bse::meta<T>::is_defined,
    //               "Failed to serialize a type! No valid definition was given");
    return;
  }
}

template <typename T>
void deserialize(T& unknown, const SerializedData& data, int& offset) {
  if (offset >= data.bytes.size()) {
    return;
  }

  if constexpr (std::is_trivially_copyable_v<T>) {
    // Prefer memcpy for trivial types
    memcpy(&unknown, &data.bytes[offset], sizeof(unknown));
    offset += sizeof(unknown);
  } else if constexpr (bse::has_serialize_v<T, SerializedData()>) {
    unknown.deserialize(data, offset);
  } else if constexpr (bse::meta<T>::is_defined) {
    // Otherwise, if reflectable, use reflection to serialize all given types
    bse::for_each_field(unknown, [&data, &offset](auto& field) {
      if constexpr (!std::is_member_function_pointer_v<decltype(field.value)>) {
        deserialize(field.value, data, offset);  //
      }
    });
  } else {
    unknown.deserialize(data, offset);
    // static_assert(bse::has_deserialize_method_v<T> || std::is_trivially_copyable_v<T> ||
    //                   bse::meta<T>::is_defined,
    //               "Failed to deserialize a type! No valid definition was given");
  }
}

template <typename T>
void serialize(const std::vector<T>& array_value, SerializedData& data) {
  serialize(array_value.size(), data);
  for (const auto& value : array_value) {
    serialize(value, data);
  }
  return;
}

template <typename T>
void deserialize(std::vector<T>& array_value, const SerializedData& data, int& offset) {
  typename std::vector<T>::size_type size{0};
  deserialize(size, data, offset);

  array_value.reserve(size);
  for (int i = 0; i < size; i++) {
    if (i < array_value.size()) {
      deserialize(array_value[i], data, offset);
    } else {
      T value;
      deserialize(value, data, offset);
      array_value.emplace_back(std::move(value));
    }
  }
  if (array_value.size() > size) {
    array_value.resize(size);
  }
}

template <typename T, int N>
void serialize(const std::array<T, N>& array_value, SerializedData& data) {
  serialize(array_value.size(), data);
  for (const auto& value : array_value) {
    serialize(value, data);
  }
  return;
}

template <typename T, int N>
void deserialize(std::array<T, N>& array_value, const SerializedData& data, int& offset) {
  typename std::array<T, N>::size_type size{0};
  deserialize(size, data, offset);
  for (int i = 0; i < size; i++) {
    T value;
    deserialize(value, data, offset);
    array_value[i] = std::move(value);
  }
}

template <typename KeyT, typename ValueT>
void serialize(const std::unordered_map<KeyT, ValueT>& map_value, SerializedData& data) {
  serialize(map_value.size(), data);
  for (const auto& [key, value] : map_value) {
    serialize(key, data);
    serialize(value, data);
  }
  return;
}

template <typename KeyT, typename ValueT>
void deserialize(std::unordered_map<KeyT, ValueT>& map_value, const SerializedData& data,
                 int& offset) {
  typename std::unordered_map<KeyT, ValueT>::size_type size{0};
  deserialize(size, data, offset);
  for (int i = 0; i < size; i++) {
    KeyT key;
    deserialize(key, data, offset);
    if (map_value.find(key) == map_value.end()) {
      ValueT value;
      deserialize(value, data, offset);
      map_value[key] = std::move(value);
    } else {
      deserialize(map_value[key], data, offset);
    }
  }
}

template <typename KeyT, typename ValueT>
void serialize(const std::map<KeyT, ValueT>& map_value, SerializedData& data) {
  serialize(map_value.size(), data);
  for (const auto& [key, value] : map_value) {
    serialize(key, data);
    serialize(value, data);
  }
  return;
}

template <typename KeyT, typename ValueT>
void deserialize(std::map<KeyT, ValueT>& map_value, const SerializedData& data, int& offset) {
  typename std::map<KeyT, ValueT>::size_type size{0};
  deserialize(size, data, offset);
  for (int i = 0; i < size; i++) {
    KeyT key;
    deserialize(key, data, offset);
    if (map_value.find(key) == map_value.end()) {
      ValueT value;
      deserialize(value, data, offset);
      map_value[key] = std::move(value);
    } else {
      deserialize(map_value[key], data, offset);
    }
  }
}

template <typename T>
void serialize(const std::unique_ptr<T>& ptr_value, SerializedData& data) {
  bool is_nullptr = (ptr_value == nullptr);
  serialize(is_nullptr, data);
  if (!is_nullptr) {
    serialize(*ptr_value, data);
  }
  return;
}

template <typename T>
void deserialize(std::unique_ptr<T>& ptr_value, const SerializedData& data, int& offset) {
  bool was_nullptr = false;
  deserialize(was_nullptr, data, offset);

  if (!was_nullptr) {
    if constexpr (std::is_default_constructible_v<T>) {
      if (ptr_value == nullptr) {
        ptr_value = std::make_unique<T>();
      }
    } else {
      ASSERT(ptr_value != nullptr)
          << "Deserialize was handed a unique ptr to a type that could not be default "
             "constructed AND that was null. Deserialize has no way of handling this.";
    }

    deserialize(*ptr_value, data, offset);
  }
}

namespace detail {
template <int I, typename... MArgs>
void impl_variant_serialize(const std::variant<MArgs...>& variant_value, SerializedData& data) {
  //
  using variant_type_t = std::remove_const_t<std::remove_reference_t<decltype(variant_value)>>;
  if constexpr (I < std::variant_size_v<variant_type_t>) {
    using current_variant_t = std::variant_alternative_t<I, variant_type_t>;

    if (I == variant_value.index()) {
      serialize(std::get<current_variant_t>(variant_value), data);
      return;
    } else {
      return;
    }
  }
  ASSERT(false) << "Failed to serialize variant... For some reason";
  return;
}
}  // namespace detail

template <typename... MArgs>
void serialize(const std::variant<MArgs...>& variant_value, SerializedData& data) {
  serialize(variant_value.index(), data);
  detail::impl_variant_serialize<0, MArgs...>(variant_value, data);
  return;
}

namespace detail {
template <int I, typename... MArgs>
void impl_variant_deserialize(std::variant<MArgs...>& variant_value, const SerializedData& data,
                              int& offset, std::size_t index) {
  //
  using variant_type_t = std::remove_const_t<std::remove_reference_t<decltype(variant_value)>>;
  if constexpr (I < std::variant_size_v<variant_type_t>) {
    using current_variant_t = std::variant_alternative_t<I, variant_type_t>;

    if (I == index) {
      current_variant_t value{};  // Must be default constructible!
      deserialize(value, data, offset);
      variant_value = value;
    } else {
      impl_variant_deserialize<I + 1, MArgs...>(variant_value, data, offset, index);
    }
    return;
  } else {
    ASSERT(false) << "Failed to deserialize variant... For some reason";
    return;
  }
}
}  // namespace detail

template <typename... MArgs>
void deserialize(std::variant<MArgs...>& variant_value, const SerializedData& data, int& offset) {
  std::size_t index{0};
  deserialize(index, data, offset);
  detail::impl_variant_deserialize<0, MArgs...>(variant_value, data, offset, index);
}

template <typename FirstT, typename SecondT>
void serialize(const std::pair<FirstT, SecondT>& pair_value, SerializedData& data) {
  serialize(pair_value.first, data);
  serialize(pair_value.second, data);
  return;
}

template <typename FirstT, typename SecondT>
void deserialize(std::pair<FirstT, SecondT>& pair_value, const SerializedData& data, int& offset) {
  bool was_nullptr = false;
  deserialize(pair_value.first, data, offset);
  deserialize(pair_value.second, data, offset);
}

}  // namespace bse

// #define ENABLE_PRIVATE_SERIALIZATION(type) \
//   friend bse::SerializedData bse::serialize(const STRIP_PARENS(type) & self);  //\
//   // friend void ::bse::deserialize(STRIP_PARENS(type) & self, const ::bse::SerializedData& data, \
//   //                                int& offset)

#define SERFUN(type)                                                                  \
  void impl_serialize(const STRIP_PARENS(type) & self, ::bse::SerializedData & data); \
  namespace bse {                                                                     \
  template <>                                                                         \
  inline void serialize<STRIP_PARENS(type)>(const STRIP_PARENS(type) & self,          \
                                            ::bse::SerializedData& data) {            \
    impl_serialize(self, data);                                                       \
  }                                                                                   \
  }                                                                                   \
  inline void impl_serialize(const STRIP_PARENS(type) & self, ::bse::SerializedData & data)

#define TEMPLATE_SERFUN(template_args, type)                                          \
  template <STRIP_PARENS(template_args)>                                              \
  void impl_serialize(const STRIP_PARENS(type) & self, ::bse::SerializedData & data); \
  namespace bse {                                                                     \
  template <STRIP_PARENS(template_args)>                                              \
  void serialize(const STRIP_PARENS(type) & self, ::bse::SerializedData& data) {      \
    impl_serialize(self, data);                                                       \
  }                                                                                   \
  }                                                                                   \
  template <STRIP_PARENS(template_args)>                                              \
  void impl_serialize(const STRIP_PARENS(type) & self, ::bse::SerializedData & data)

#define CLASS_SERFUN(type)                                                                  \
  inline void serialize(::bse::SerializedData& data) const { impl_serialize(*this, data); } \
  inline void impl_serialize(const STRIP_PARENS(type) & self, ::bse::SerializedData & data) const

#define VIRTUAL_CLASS_SERFUN(type)                                   \
  virtual inline void serialize(::bse::SerializedData& data) const { \
    impl_serialize(*this, data);                                     \
  }                                                                  \
  inline void impl_serialize(const STRIP_PARENS(type) & self, ::bse::SerializedData & data) const

#define ABSTRACT_VIRTUAL_CLASS_SERFUN(type) \
  virtual inline void serialize(::bse::SerializedData& data) const = 0

#define SERIALIZE_BASE_CLASS(base_type) \
  STRIP_PARENS(base_type)::serialize(data);  // // // // // // // // // //
// data += serialize((const STRIP_PARENS(base_type)&)self);  ///

#define SERIALIZE(name) ::bse::serialize(name, data)

//////////////////////
//////////////////////
//////////////////////
//////////////////////
//////////////////////

#define DESERFUN(type)                                                                       \
  void impl_deserialize(STRIP_PARENS(type) & self, const ::bse::SerializedData& data,        \
                        int& offset);                                                        \
  namespace bse {                                                                            \
  inline void deserialize(STRIP_PARENS(type) & self, const ::bse::SerializedData& data,      \
                          int& offset) {                                                     \
    impl_deserialize(self, data, offset);                                                    \
  }                                                                                          \
  }                                                                                          \
  inline void impl_deserialize(STRIP_PARENS(type) & self, const ::bse::SerializedData& data, \
                               int& offset)

#define TEMPLATE_DESERFUN(template_args, type)                                                  \
  template <STRIP_PARENS(template_args)>                                                        \
  void impl_deserialize(STRIP_PARENS(type) & self, const ::bse::SerializedData& data,           \
                        int& offset);                                                           \
  namespace bse {                                                                               \
  template <STRIP_PARENS(template_args)>                                                        \
  void deserialize(STRIP_PARENS(type) & self, const ::bse::SerializedData& data, int& offset) { \
    impl_deserialize(self, data, offset);                                                       \
  }                                                                                             \
  }                                                                                             \
  template <STRIP_PARENS(template_args)>                                                        \
  void impl_deserialize(STRIP_PARENS(type) & self, const ::bse::SerializedData& data, int& offset)

#define CLASS_DESERFUN(type)                                                                 \
  inline void deserialize(const ::bse::SerializedData& data, int& offset) {                  \
    impl_deserialize(*this, data, offset);                                                   \
  }                                                                                          \
  inline void impl_deserialize(STRIP_PARENS(type) & self, const ::bse::SerializedData& data, \
                               int& offset)

#define VIRTUAL_CLASS_DESERFUN(type)                                                         \
  virtual inline void deserialize(const ::bse::SerializedData& data, int& offset) {          \
    impl_deserialize(*this, data, offset);                                                   \
  }                                                                                          \
  inline void impl_deserialize(STRIP_PARENS(type) & self, const ::bse::SerializedData& data, \
                               int& offset)

#define ABSTRACT_VIRTUAL_CLASS_DESERFUN(type) \
  virtual inline void deserialize(const ::bse::SerializedData& data, int& offset) = 0

#define DESERIALIZE_BASE_CLASS(base_type) \
  STRIP_PARENS(base_type)::deserialize(data, offset);  // // // // // // //
// deserialize((STRIP_PARENS(base_type)&)self, data, offset);  //

#define DESERIALIZE(name) ::bse::deserialize(name, data, offset)

SERFUN((bse::SerializedData)) { SERIALIZE(self.bytes); }

DESERFUN((bse::SerializedData)) { DESERIALIZE(self.bytes); }

#endif  // BSE_SERIALIZE_HPP
