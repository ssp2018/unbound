#pragma once

#ifndef BSE_TASK_HANDLE_HPP
#define BSE_TASK_HANDLE_HPP

namespace bse {

// A handle that is used to refer to tasks that have been queued up.
struct TaskHandle {
  static const TaskHandle NULL_HANDLE;
  using value_type = uint32_t;

  bool operator==(const TaskHandle& other);
  bool operator!=(const TaskHandle& other);

  union {
    struct {
      value_type index : 24;
      value_type iteration : 8;
    };
    value_type full_handle = ~value_type(0u);
  };
};

}  // namespace bse
#endif  // BSE_TASK_HANDLE_HPP
