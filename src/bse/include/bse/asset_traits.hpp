#pragma once

#ifndef BSE_ASSET_TRAITS_HPP
#define BSE_ASSET_TRAITS_HPP

#include "bse/file_path.hpp"
#include "result.hpp"

namespace bse {

// SFINAE construct for checking the presence of a static load function taking a path as argument
template <typename T, typename = Result<T>>
struct has_static_path_load : std::false_type {};

template <typename T>
struct has_static_path_load<T, decltype(T::load(bse::FilePath("")))> : std::true_type {};

template <typename T>
constexpr bool has_static_path_load_v = has_static_path_load<T>::value;

// A combined SFINAE construct for an asset
template <typename T>
using is_asset = std::conjunction<    //
    has_static_path_load<T>,          //
    std::is_move_assignable<T>,       //
    std::is_move_constructible<T>,    //
    std::negation<                    //
        std::is_copy_assignable<T>>,  //
    std::negation<                    //
        std::is_copy_constructible<T>>>;

template <typename T>
constexpr bool is_asset_v = is_asset<T>::value;

// The error message that should be displayed if an incorrect type is attempted to be passed as an
// asset.
#define UNBOUND_ASSET_ERROR_MESSAGE                             \
  "An asset of type T must fulfill the following criteria...\n" \
  "\tIt must have a static method which "                       \
  "\ttakes a bse::FilePath, and which returns "                 \
  "\tResult<T>.\n"                                              \
  "\tIt must be move assignable.\n"                             \
  "\tIt must be move constructible.\n"                          \
  "\tIt must NOT be copy assignable.\n"                         \
  "\tIt must NOT be copy constructible."

}  // namespace bse
#endif  // BSE_ASSET_TRAITS_HPP
