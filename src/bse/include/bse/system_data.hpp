#pragma once

#ifndef BSE_SYSTEM_DATA_HPP
#define BSE_SYSTEM_DATA_HPP

#include <ext/ext.hpp>

namespace bse {

// System relevant data used for certain operations or optimizations.
struct SystemData {
  uint32_t sector_size;
  uint16_t cache_line_size = 64u;
};

SystemData system_data();

// Initializes the SystemData structure.
// MUST be called at the start of main!
SystemData initialize_system_data();

// Aligns input to the nearest multiple
// (Placed in this header until a more appropriate header is needed)
template <typename T, typename U>
decltype(auto) align_to_multiple(T in, U align) {
  return static_cast<T>(std::ceil(in / static_cast<float>(align)) * align);
}

}  // namespace bse
#endif  // BSE_SYSTEM_DATA_HPP
