#pragma once

#ifndef BSE_EDIT_HPP
#define BSE_EDIT_HPP

#include <bse/reflect.hpp>

struct GLFWwindow;
struct ImDrawData;

// Add realtime edit entry for var.
// Invoking BSE_EDIT on your variable
// makes it editable through the bse::edit
// GUI in realtime.
#define BSE_EDIT(var) bse::Edit::edit(var, #var, __FILE__, __LINE__)

// As BSE_EDIT, but specify
// the name of the entry as it appears
// in the GUI.
#define BSE_EDIT_NAMED(var, name) bse::Edit::edit(var, #var, __FILE__, __LINE__, name)

// As BSE_EDIT, but explicitly
// a color picker.
#define BSE_EDIT_COLOR(var) bse::Edit::edit_color(var, #var, __FILE__, __LINE__)

// As BSE_EDIT_NAMED, but explicitly
// a color picker
#define BSE_EDIT_COLOR_NAMED(var, name) bse::Edit::edit_color(var, #var, __FILE__, __LINE__, name)

namespace bse {

class Edit {
 public:
  // Initialize bse::edit
  static bool initialize(bool (*initializer)(GLFWwindow*), void (*framer)(), void (*end_framer)(),
                         void (*drawer)(ImDrawData*), GLFWwindow* window);

  // destroy static instance of edit
  static void destroy();

  // Create new frame
  static void new_frame();

  // Draw current frame
  static void draw();

  // end frame
  static void end_frame();

  // Enable bse::edit
  static void enable();

  // Disable bse::edit
  static void disable();

  // Enable/disable bse::edit
  static void toggle();

  // Toggle the GUI, but keep updating
  // the variables from BSE_EDIT_*.
  static void toggle_gui();

  // Add realtime edit entry for variable.
  // Do not use this directly. Use BSE_EDIT
  // instead.
  template <typename T>
  static void edit(T& var, const char* var_name, const char* file, size_t line,
                   const char* name = nullptr);

  // As edit, but explicitly a color picker
  template <typename T>
  static void edit_color(T& var, const char* var_name, const char* file, size_t line,
                         const char* name = nullptr);

  // Check if bse::edit wants keyboard input
  static bool is_capturing_keys();

  // Check if bse::edit wants mouse input
  static bool is_capturing_mouse();

  // Get the paths of entries that the user has changed this frame.
  // "Changed" means that the user has finished editing an entry by
  // pressing Return.
  static const std::vector<std::string>& get_updated_paths();

  // Remove an entry from the GUI.
  static void remove_entry(const std::string& path);

 private:
  using Color = std::array<float, 4>;

  // Store key data for entry queue
  struct Key {
    std::string key;
    std::vector<std::string> path;
  };

  // Generic base for templated entries
  struct EntryBase {
    virtual ~EntryBase() {}

    // Fill self with data from imgui
    virtual void fill(std::string& path, std::vector<std::string>& updated_paths) = 0;

    bool is_dirty = false;
    std::string name;
  };

  // Store multiple occurrences of the same var
  // in the same entry.
  template <typename T>
  struct Entry : public EntryBase {
    virtual ~Entry() = default;
    // Fill self with data from imgui
    void fill(std::string& path, std::vector<std::string>& updated_paths) override;
    T var;
  };

  // Group vars in a tree structure.
  struct Group {
    // Recursively fill entries with
    // data from imgui
    void fill(std::string& path, std::vector<std::string>& updated_paths);

    std::string name;
    std::vector<Group*> children;
    std::vector<EntryBase*> entries;
  };

  Edit(void (*framer)(), void (*end_framer)(), void (*drawer)(ImDrawData*),
       bool do_init_thread = false);
  ~Edit();

  // Helper for the public edit method
  template <typename T>
  static void edit(T& var, const char* var_name, const char* file, size_t line,
                   std::vector<const char*>& prefix, const char* name);

  // Add realtime edit entry for a
  // reflectable variable.
  template <typename T>
  static void edit_reflectable(T& var, const char* var_name, const char* file, size_t line,
                               std::vector<const char*>& prefix, const char* name);

  // Extract basename and layer from file string
  static void extract_basename_and_layer(const char* file, std::string_view& basename,
                                         std::string_view& layer);

  // Generate a string key from entry metadata
  template <typename T>
  static std::string create_string_key(const std::string_view& basename,
                                       const std::string_view& layer, const char* var_name,
                                       size_t line, const std::vector<const char*>& prefix,
                                       const char* name = nullptr);

  // Push entry to entry queue
  template <typename T>
  void push_entry(const T& var, const std::string& key, const std::string_view& basename,
                  const std::string_view& layer, const char* var_name,
                  const std::vector<const char*>& prefix, const char* name);

  // Update entry and group maps from entry queues
  void update_maps();

  // Fill all entries with data from imgui
  void fill();

  // get pointer to boolean var entry, return null if clean
  bool* get_dirty_bool(const bool& var, const char* var_name, const char* file, size_t line,
                       const std::vector<const char*>& prefix, const char* name);

  // get pointer to string var entry, return null if clean
  std::string* get_dirty_string(const std::string& var, const char* var_name, const char* file,
                                size_t line, const std::vector<const char*>& prefix,
                                const char* name);

  // get pointer to float var entry, return null if clean
  float* get_dirty_float(const float& var, const char* var_name, const char* file, size_t line,
                         const std::vector<const char*>& prefix, const char* name);

  // get pointer to int var entry, return null if clean
  int* get_dirty_int(const int& var, const char* var_name, const char* file, size_t line,
                     const std::vector<const char*>& prefix, const char* name);

  // get pointer to color var entry, return null if clean
  Color* get_dirty_color(const Color& var, const char* var_name, const char* file, size_t line,
                         const std::vector<const char*>& prefix, const char* name);

  // generic helper for get_dirty_* methods
  template <typename T>
  T* get_dirty(const T& var, const char* var_name, const char* file, size_t line,
               const std::vector<const char*>& prefix, const char* name);

  void remove(std::string& path, Group* group);
  void remove(const std::string& path);

  void (*m_framer)();
  void (*m_end_framer)();
  void (*m_drawer)(ImDrawData*);
  bool m_is_enabled;
  bool m_is_gui_enabled;
  bool m_is_in_frame;
  std::unordered_map<std::string, EntryBase*> m_entries;
  std::unordered_map<std::string, Group*> m_groups;
  std::vector<std::pair<Key, EntryBase*>> m_entry_queue;
  std::vector<Group*> m_root_groups;
  std::vector<std::string> m_updated_paths;
  static std::vector<Edit*> m_thread_instances;
  static std::mutex m_lock;
  static Edit* m_instance;
  static thread_local Edit* m_thread_instance;
};  // namespace bse

///////////////////////////////////////////////////////////////////////////////
// impl ///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

template <typename T>
void Edit::edit(T& var, const char* var_name, const char* file, size_t line, const char* name) {
  std::vector<const char*> prefix;
  edit(var, var_name, file, line, prefix, name);
}

template <typename T>
void Edit::edit(T& var, const char* var_name, const char* file, size_t line,
                std::vector<const char*>& prefix, const char* name) {
  if constexpr (std::is_convertible_v<T, std::string>) {
    if (std::string* new_var =
            m_instance->get_dirty_string(var, var_name, file, line, prefix, name)) {
      var = *new_var;
    }
  } else if constexpr (std::is_same_v<T, bool>) {
    if (bool* new_var = m_instance->get_dirty_bool(var, var_name, file, line, prefix, name)) {
      var = *new_var;
    }
  } else if constexpr (std::is_integral_v<T>) {
    if (int* new_var = m_instance->get_dirty_int(var, var_name, file, line, prefix, name)) {
      var = *new_var;
    }
  } else if constexpr (std::is_convertible_v<T, float>) {
    if (float* new_var = m_instance->get_dirty_float(var, var_name, file, line, prefix, name)) {
      var = *new_var;
    }
  } else if constexpr (bse::meta<T>::is_defined) {
    edit_reflectable(var, var_name, file, line, prefix, name);
  } else if constexpr (is_convertible_from_string_v<T>) {
    if (std::string* new_var =
            m_instance->get_dirty_string("", var_name, file, line, prefix, name)) {
      std::stringstream stream(*new_var);
      var << stream;
    }
  } else {
    static_assert(bse::meta<T>::is_defined, "bse::edit does not support the specified type.");
  }
}

template <typename T>
void Edit::edit_color(T& var, const char* var_name, const char* file, size_t line,
                      const char* name) {
  Color old_var = {0, 0, 0, 0};
  size_t size = 0;
  if constexpr (std::is_same_v<T, glm::vec3> || std::is_same_v<T, glm::uvec3> ||
                std::is_same_v<T, glm::u8vec3> || std::is_same_v<T, glm::ivec3> ||
                std::is_same_v<T, glm::i8vec3>) {
    size = 3;
  } else if constexpr (std::is_same_v<T, glm::vec4> || std::is_same_v<T, glm::uvec4> ||
                       std::is_same_v<T, glm::u8vec4> || std::is_same_v<T, glm::ivec4> ||
                       std::is_same_v<T, glm::i8vec4>) {
    size = 4;

  } else {
    static_assert(std::is_same_v<T, glm::i8vec4>,
                  "bse::edit does not support the specified color type.");
  }

  for (size_t i = 0; i < size; i++) {
    if constexpr (sizeof(var[i]) < sizeof(float)) {
      old_var[i] = float(var[i]) / 255.f;
    } else {
      old_var[i] = var[i];
    }
  }
  if (Color* new_var = m_instance->get_dirty_color(old_var, var_name, file, line, {}, name)) {
    for (size_t i = 0; i < size; i++) {
      if constexpr (sizeof(var[i]) < sizeof(float)) {
        var[i] = (*new_var)[i] * 255.f;
      } else {
        var[i] = (*new_var)[i];
      }
    }
  }
}

template <typename T>
void Edit::edit_reflectable(T& var, const char* var_name, const char* file, size_t line,
                            std::vector<const char*>& prefix, const char* name) {
  prefix.push_back(var_name);
  bse::for_each_field(var, [var_name, file, line, &prefix, name](auto& field) {
    edit(field.value, field.name.data(), file, line, prefix, name);
  });
  prefix.pop_back();
}

}  // namespace bse

#endif