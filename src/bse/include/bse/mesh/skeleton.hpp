#pragma once
#ifndef BSE_SKELETON_HPP
#define BSE_SKELETON_HPP

#include "bse/mesh/animation_clip.hpp"
#include <ext/ext.hpp>

struct aiScene;
struct aiNode;
struct aiMesh;

/*
NOTE: Assimp uses the term "bones" while Maya uses "joints". They may be used interchangeably,
however joins are in fact different. We will use the term joints in this animation system as it is
consistent with Maya.

We assume that each animation is a separate FBX file.
*/

namespace bse {
class FilePath;
struct Joint;
// Contains all skeletal data for a given model
class Skeleton {
 public:
  Skeleton();
  // Skeleton(const Skeleton &&other);
  Skeleton(const aiScene *scene, bse::FilePath &path, const aiMesh *mesh);

  std::string m_name;

  // Fetch a read only reference of the joint map
  const std::map<std::string, std::unique_ptr<Joint>> &get_joint_map() const;
  // Fetch a read only reference of the animation map
  const std::map<std::string, AnimationClip> &get_animations_map() const;

  // Return number of joints present in the skeleton
  const int num_joints() const;

  const Joint *get_root_joint() const;

  // set a bone as skeleton root bone
  void set_skeleton_root(Joint *joint);

  // Load skeleton animations from an open SSPM file
  void load_animations_sspm(std::ifstream &i_file);

  // add a joint to the skeleton
  Joint *add_joint(Joint *joint);

  // Find and load any related animations
  void find_animations(bse::FilePath &path);  // TODO: Make sure it works with the new loading

  // Get name of joint from id. Returns "" if the joint specified does not exist
  std::string get_joint_name(int id) const;

 private:
  // void print_node_hierarchy(const aiNode *node);
  // Build the skeleton hierarchy
  void build_skeleton(const aiScene *scene, const aiMesh *mesh);

  // go throught the joint tree and add them to the skeleton
  Joint *process_joint(const aiNode *current_node, glm::mat4x4 parent_transform,
                       std::unordered_map<std::string, bool> &bone_names);

  glm::mat4x4 m_global_inverse_transform;
  int m_joint_count = 0;
  Joint *m_skeleton_root = nullptr;

  std::map<std::string, std::unique_ptr<Joint>> m_joint_name_map;
  std::map<int, std::string> m_joint_id_to_name;
  std::map<std::string, AnimationClip> m_animations;
};

};  // namespace bse

#endif  // !BSE_SKELETON_HPP
