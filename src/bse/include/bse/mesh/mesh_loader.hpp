#pragma once
#ifndef BSE_MESH_LOADER_HPP
#define BSE_MESH_LOADER_HPP

#include <bse/file_path.hpp>
#include <bse/flags.hpp>
#include <bse/mesh/joint.hpp>
#include <bse/mesh/skeleton.hpp>
#include <bse/result.hpp>

namespace bse {
// Enum of Vertex types
enum VertexTypes { POSITION, COLOR, NORMAL, UV, SKELETAL, COUNT };

// internal struct used as abstraction between assimp and sspm for mesh
struct MeshData {
  std::vector<unsigned int> indices;
  std::vector<float> positions;
  std::vector<float> normals;
  std::vector<float> color;
  std::vector<float> uv;
  std::vector<int> joint_indices;
  std::vector<float> joint_weights;
  Flags<VertexTypes::COUNT> available_vertex_data;
  unsigned vertex_count = 0;
  unsigned face_count = 0;
};
// internal struct used as abstraction between assimp and sspm for model
struct ModelData {
  MeshData mesh;
  std::unique_ptr<bse::Skeleton> skel;
};

// Static class for loading mesh files
class MeshLoader {
 public:
  // Loads a SSPM file from disk. If an SSPM file is not found, or if 'refresh' is true, it is
  // created from an FBX file
  static bse::Result<ModelData> load_mesh(bse::FilePath path, bool refresh = false);

 private:
  // load model from SSPM which is much faster and stores it in model
  // returns false if SSPM file is the wrong version
  static bool load_sspm(bse::FilePath path, ModelData &out_model);

  // load model with assimp
  static ModelData load_model_assimp(bse::FilePath path);

  // convert from assimp to internal mesh format and stores it in 'model_data'
  // 'transformation' contains a matrix that will be applied to vertex positions
  // 'normal_transformation' contains a matrix that will be applied to vertex normals
  static void convert_assimp_toimesh(aiMesh *amesh, ModelData &model_data,
                                     const glm::mat4 &transformation,
                                     const glm::mat4 &normal_transformation);

  // save the model into SSPM format
  static void save_to_custom_format(bse::FilePath path, bse::ModelData &model);

  // load joint recursivly
  static Joint *load_skeleton_joint(std::ifstream *i_file, Skeleton *skel);

  // save all joints recursivly
  static void save_skeleton_joint(const Joint *joint, std::ofstream *o_file);

  static std::bitset<VertexTypes::COUNT> get_available_vertex_data(MeshData *mesh);
};
}  // namespace bse
#endif  // BSE_MESH_LOADER_HPP