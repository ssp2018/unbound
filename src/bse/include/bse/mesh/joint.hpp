#pragma once
#ifndef BSE_JOINT_HPP
#define BSE_JOINT_HPP

namespace bse {

// Representing each joint in a skeleton
struct Joint {
  std::string m_name;
  int m_id;
  glm::mat4x4 m_offset_matrix;       // Inverse bind pose, joint to model space
  glm::mat4x4 m_relative_transform;  // Relative to parent
  std::vector<Joint *> m_children;
};
}  // namespace bse

#endif
