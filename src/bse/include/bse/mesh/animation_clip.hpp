#pragma once
#ifndef BSE_ANIMATION_CLIP_HPP
#define BSE_ANIMATION_CLIP_HPP

#include <bse/file_path.hpp>
#include <ext/ext.hpp>

struct aiQuatKey;
struct aiVectorKey;

namespace bse {
struct Joint;
// Contains the scale, rotation and transformation for a single joint in a single frame
struct SQT {
  // float m_scale; //Scaling not supported
  glm::quat m_rotation;
  glm::vec3 m_translation;
};

// A key frame in the animation containing poses for each joint at a certain point in time
struct KeyFrame {
  float m_time;
  std::map<int, SQT> m_joint_poses;
};

// A struct used to refer to an animation channel, possibly combined from multiple assimp channels
// Can't use assimp's built-in struct, it has a destructor that will be called from the container
// this struct will be stored in, which will cause assimp to crash when attempting to destroy the
// object
struct Channel {
  aiVectorKey *position_keys = nullptr;
  aiQuatKey *rotation_keys = nullptr;

  int num_position_keys = 0;
  int num_rotation_keys = 0;
};

// Single animation that can be used by a compatible skeleton
class AnimationClip {
 public:
  AnimationClip();

  std::string m_name;

  // Loads animation frame data with assimp
  void load_animation(const bse::FilePath &file,
                      std::map<std::string, std::unique_ptr<bse::Joint>> &joint_map);
  // Loads animation notifies, if there are any
  std::vector<int> load_notifies(const bse::FilePath &path);

  // Saves animation into an open SSPM file
  void save_animation_sspm(std::ofstream &file) const;

  // Loads animation from an open SSPM file
  void load_animation_sspm(std::ifstream &file, std::map<std::string, AnimationClip> &animations);

  const std::vector<KeyFrame> &get_keyframes() const;
  const std::vector<int> &get_notifies() const;
  int get_start_joint() const;

  int get_num_keys() const;
  float get_duration() const;
  float get_ticks_per_second() const;

 private:
  int m_num_keys;
  float m_duration;          // Ticks
  float m_ticks_per_second;  // Animation speed
  int m_start_joint;  // All joints including and below the start joint in the hierarchy will be
                      // affected by the animation
  std::vector<KeyFrame> m_animation_keys;
  std::vector<int> m_notify_frames;  // Contains animation notifies that act as triggers for events.

  // Converters from assimp data types to glm
  glm::quat convert_assimp_quat(aiQuaternion assimp_quat);
  glm::vec3 convert_assimp_vec3(aiVector3D assimp_vec3);

  // Set start joint based on file name. very ugly :(
  void set_start_joint(std::map<std::string, std::unique_ptr<bse::Joint>> &joint_map);
};
};  // namespace bse

#endif  // !BSE_ANIMATION_HPP
