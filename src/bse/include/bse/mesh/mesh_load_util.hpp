#pragma once
#include <ext/ext.hpp>
struct aiNode;

namespace bse {
// Recursively checks parents of node. If the parent name contains the name of 'node', the parent
// transform will be multiplied into the transform of 'node'. This will iterate until a parent
// name does not contain the name of 'node'. The final matrix is returned
glm::mat4 collapse_parent_transforms_by_name(aiNode *node);

// Convert assimp matrix to glm compatible matrix
glm::mat4 convert_assimp_matrix(const aiMatrix4x4 &matrix);
}  // namespace bse
