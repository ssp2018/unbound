#pragma once

#ifndef BSE_FILE_HPP
#define BSE_FILE_HPP

#include "bse/directory_path.hpp"
#include "bse/result.hpp"
#include "file_path.hpp"
namespace bse {

// The result from a call to list_dir()
struct Directory {
  std::vector<DirectoryPath> sub_dirs;
  std::vector<FilePath> files;
};

// Returns a Directory struct with all subdirectories and files about the given directory path
Directory list_dir(const DirectoryPath path);

// Loads the contents at path into a string
Result<std::string> load_file_str(bse::FilePath path);

// Loads the contents at path into a vector of chars
Result<std::vector<char>> load_file_raw(bse::FilePath path);

// Write string data to file. Returns false if failure.
bool write_file(const FilePath path, const std::string& data, bool append = false);

// Write char-array data to file. Returns false if failure.
bool write_file(const FilePath path, const std::vector<char> data, bool append = false);

// check if a file exists
bool file_exists(const FilePath path);

}  // namespace bse
#endif  // BSE_FILE_HPP