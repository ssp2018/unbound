#pragma once

#ifndef UNBOUND_BSE_UTILITY_HPP
#define UNBOUND_BSE_UTILITY_HPP

#include <ext/ext.hpp>

namespace bse {

// Split string along occurring delimiters
std::vector<std::string_view> explode(const std::string_view& string, char delimiter);

// Remove whitespace before and after string
std::string trim_whitespace(const std::string& string);

// Remove all occurrences of c in string
void remove(std::string& string, char c);

// Print the contents of a matrix to cout. 'prefix' is a string to add to the beginning of every
// line
void print_matrix(const glm::mat4& matrix, std::string prefix = "");

// Align pointer upwards
void* align_pointer_up(void* pointer, unsigned alignment);

}  // namespace bse

#endif  // UNBOUND_BSE_UTILITY_HPP