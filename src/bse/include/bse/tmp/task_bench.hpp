#pragma once

#ifndef BSE_TASK_BENCH_HPP
#define BSE_TASK_BENCH_HPP

namespace bse::tmp {
// Simple, temporary benchmarking of task scheduler
void run_benchmark();

}  // namespace bse::tmp
#endif  // BSE_TASK_BENCH_HPP
