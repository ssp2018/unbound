#pragma once

#ifndef BSE_SLOT_VECTOR_HPP
#define BSE_SLOT_VECTOR_HPP

#include <bse/slot_creator.hpp>

namespace bse {

// Sparse vector that does not
// rearrange elements on erasure.
template <typename T>
class SlotVector {
 public:
  // Insert element.
  // Return index.
  size_t insert(const T& t);

  // Erase element at slot.
  // Return true if slot was occupied.
  bool erase(size_t slot);

  // Check if slot is occupied.
  bool exists(size_t slot) const;

  // Get element at slot
  T& operator[](size_t slot);

  // Get element at slot
  const T& operator[](size_t slot) const;

  // Get number of occupied slots.
  size_t size() const;

  // Get number of total slots.
  size_t capacity() const;

  // Find element.
  // Return -1 if not found.
  size_t find(const T& t) const;

 private:
  SlotCreator m_slots;
  std::vector<T> m_data;
  size_t m_size = 0;
};

}  // namespace bse
#endif  // BSE_SLOT_VECTOR_HPP