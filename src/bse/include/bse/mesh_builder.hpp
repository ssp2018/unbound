#pragma once

#ifndef BSE_MESH_BUILDER_HPP
#define BSE_MESH_BUILDER_HPP

#include <bse/slot_vector.hpp>

namespace bse {

// Utility for forming a mesh by connecting vertices into triangular faces.
class MeshBuilder {
 public:
  using VertexId = uint16_t;
  using FaceId = VertexId;
  using Face = std::array<VertexId, 3>;

  // Get face from id
  const Face& get_face(FaceId id) const;

  // Create standalone vertex
  VertexId create_vertex();

  // Destroy vertex.
  // Return destroyed faces
  std::vector<FaceId> destroy_vertex(VertexId id);

  // Connect vertices with eachother.
  // Return created faces.
  // std::vector<FaceId> connect_vertices(const std::vector<VertexId>& vertices);
  std::vector<FaceId> connect_vertices(VertexId a, VertexId b);

  // Disconnect vertices from eachother.
  // Return destroyed faces.
  // std::vector<FaceId> disconnect_vertices(const std::vector<VertexId>& vertices);
  std::vector<FaceId> disconnect_vertices(VertexId a, VertexId b);

  std::vector<FaceId> get_face_neighbors(FaceId id) const;

 private:
  struct Vertex;

  // Create a face from vertices.
  static Face create_face(VertexId a, VertexId b, VertexId c);

  // Find neighbors shared by two vertices.
  static std::vector<VertexId> find_shared_neighbors(const Vertex& a, const Vertex& b);

  // Connect two vertices.
  // Return created faces.
  std::vector<FaceId> connect(VertexId a, VertexId b);

  // Disconnect two vertices.
  // Return destroyed faces.
  std::vector<FaceId> disconnect(VertexId a, VertexId b);

  // Internal data representation of a vertex.
  struct Vertex {
    std::vector<VertexId> neighbors;
  };

  SlotVector<Face> m_faces;
  SlotVector<Vertex> m_vertices;
};

}  // namespace bse
#endif  // BSE_MESH_BUILDER_HPP