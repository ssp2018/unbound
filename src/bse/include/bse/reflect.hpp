#pragma once

#ifndef BSE_REFLECT_HPP
#define BSE_REFLECT_HPP

#include <ext/ext.hpp>

namespace bse {

// Returns the length of a compile-time string at compile-time
constexpr int const_str_len(const char* str, const int i = 0) {
  return (str[i] == '\0' ? i : const_str_len(str, i + 1));
}

// Returns the location of the last ':' in a compile-time string at compile-time
constexpr int find_last_colon(const char* str, const int i) {
  return (i > 0 ? (str[i] == ':' ? i : find_last_colon(str, i - 1)) : -1);
}

// Returns a compile-time string_view of a type name without the preceeding namespaces
constexpr std::string_view strip_namespace_from_name(const char* name) {
  const auto length = const_str_len(name, 0);
  const auto last_colon = find_last_colon(name, length);
  const auto stripped_name = std::string_view(name + last_colon + 1, length - (last_colon + 1));
  return stripped_name;
}

// A meta representation of a type
template <typename T>
struct meta {
  static constexpr std::string_view name = "#UNDEFINED_META_NAME";
  static const inline std::string_view base_name = "#UNDEFINED_META_BASE_NAME";
  static constexpr unsigned int num_fields = 0;
  static constexpr std::size_t size = 0;
  static constexpr bool is_defined = false;
};

// A meta representation of a field (member) in a type
template <typename T, int I>
struct meta_field;

namespace detail {
// Implementation detail for for_each_field
template <typename T, typename FUNC, int N, int I>
void impl_for_each_field(T& t, FUNC func) {
  // using dereferenced_T = std::remove_reference_t<T>;
  if constexpr (I < N) {
    auto field = typename meta<T>::template field<I>(t);
    func(field);
    impl_for_each_field<T, FUNC, N, I + 1>(t, func);
  }
}
}  // namespace detail

// Iterates every field in a type and applies FUNC
template <typename T, typename FUNC>
void for_each_field(T& t, FUNC func) {
  // using dereferenced_T = std::remove_reference_t<T>;
  bse::detail::impl_for_each_field<T, FUNC, bse::meta<T>::num_fields, 0>(t, func);
}

template <typename, typename = void>
struct IsPrintable : std::false_type {};

template <typename T>
struct IsPrintable<T, std::void_t<decltype(std::declval<std::stringstream>() << std::declval<T>())>>
    : std::true_type {};

template <typename T>
constexpr bool is_printable_v = IsPrintable<T>::value;

template <typename, typename = void>
struct IsConvertibleFromString : std::false_type {};

template <typename T>
struct IsConvertibleFromString<T, std::void_t<decltype(std::declval<T>() << std::stringstream())>>
    : std::true_type {};

template <typename T>
constexpr bool is_convertible_from_string_v = IsConvertibleFromString<T>::value;

template <typename ClassT, typename FieldT>
constexpr size_t field_size(FieldT ClassT::*field) {
  return sizeof(FieldT);
}

template <typename ClassT, typename ReturnT, typename... ArgsT>
constexpr size_t field_size(ReturnT (ClassT::*method)(ArgsT...)) {
  return sizeof(method);
}

template <typename ClassT, typename ReturnT, typename... ArgsT>
constexpr size_t field_size(ReturnT (ClassT::*method)(ArgsT...) const) {
  return sizeof(method);
}

template <typename ClassT, typename FieldT>
auto& field_value(FieldT ClassT::*field) {
  if constexpr (std::is_member_function_pointer_v<decltype(field)>) {
    static auto f = field;
    return f;
  } else {
    static FieldT f;
    return f;
  }
}

template <typename Class1T, typename Class2T, typename FieldT>
auto& field_value(Class1T& t, FieldT Class2T::*field) {
  if constexpr (std::is_member_function_pointer_v<decltype(field)>) {
    static auto f = field;
    return f;
  } else {
    return t.*field;
  }
}

}  // namespace bse

// Operator << overload for outputting meta-defined types that don't otherwise have an overload
template <typename T, typename = typename std::enable_if<bse::meta<T>::is_defined>::type>
std::ostream& operator<<(std::ostream& out, T& t) {
  out << bse::meta<T>::name << ":\n";
  if constexpr (bse::meta<T>::num_fields > 0) {
    bse::for_each_field(t, [&out](auto& f) {
      if constexpr (bse::is_printable_v<decltype(f.value)>) {
        out << '\t' << f.name << '\t' << f.value << std::endl;
      } else {
        out << '\t' << f.name << '\t' << "<UNPRINTABLE>" << std::endl;
      }
    });
  } else {
    out << "\tEmpty type" << std::endl;
  }
  return out;
}

#define SINGLE_ARG(...) SINGLE_ARG1(__VA_ARGS__)
#define SINGLE_ARG1(...) SINGLE_ARG2(__VA_ARGS__)
#define SINGLE_ARG2(...) __VA_ARGS__

#define EXPAND(x) x
#define STRIP_PARENS(x) SINGLE_ARG x
#define STRINGIZE(arg) STRINGIZE1(arg)
#define STRINGIZE1(arg) STRINGIZE2(arg)
#define STRINGIZE2(arg) #arg

#define IMPL_REFLECT_TYPE(TEMPLATE_ARGS, TYPE, N)                           \
  template <STRIP_PARENS(TEMPLATE_ARGS)>                                    \
  struct bse::meta<STRIP_PARENS(TYPE)> {                                    \
    static constexpr std::string_view name = STRINGIZE(STRIP_PARENS(TYPE)); \
    static const inline std::string_view base_name =                        \
        bse::strip_namespace_from_name(STRINGIZE(STRIP_PARENS(TYPE)));      \
    static constexpr unsigned int num_fields = N;                           \
    static constexpr std::size_t size = sizeof(STRIP_PARENS(TYPE));         \
    static constexpr bool is_defined = true;                                \
    template <int I>                                                        \
    using field = bse::meta_field<STRIP_PARENS(TYPE), I>;                   \
  };

#define IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)                                    \
  template <STRIP_PARENS(TEMPLATE_ARGS)>                                                          \
  struct bse::meta_field<STRIP_PARENS(TYPE), INDEX> {                                             \
    meta_field(STRIP_PARENS(TYPE) & t) : value{field_value(t, &STRIP_PARENS(TYPE)::FIELD)} {};    \
    using type = std::remove_reference_t<decltype(bse::field_value(&STRIP_PARENS(TYPE)::FIELD))>; \
    static constexpr std::string_view name = #FIELD;                                              \
    static constexpr std::size_t size = bse::field_size(&STRIP_PARENS(TYPE)::FIELD);              \
    decltype(bse::field_value(&STRIP_PARENS(TYPE)::FIELD)) value;                                 \
    static constexpr auto member_ptr = &STRIP_PARENS(TYPE)::FIELD;                                \
    static constexpr auto index = INDEX;                                                          \
  };

#define CONCATENATE(arg1, arg2) CONCATENATE1(arg1, arg2)
#define CONCATENATE1(arg1, arg2) CONCATENATE2(arg1, arg2)
#define CONCATENATE2(arg1, arg2) arg1##arg2

#define IMPL_REFLECT_0(...)

#define IMPL_REFLECT_1(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)

#define IMPL_REFLECT_2(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)       \
  EXPAND(IMPL_REFLECT_1(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_3(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)       \
  EXPAND(IMPL_REFLECT_2(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_4(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)       \
  EXPAND(IMPL_REFLECT_3(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_5(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)       \
  EXPAND(IMPL_REFLECT_4(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_6(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)       \
  EXPAND(IMPL_REFLECT_5(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_7(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)       \
  EXPAND(IMPL_REFLECT_6(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_8(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)       \
  EXPAND(IMPL_REFLECT_7(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_9(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)       \
  EXPAND(IMPL_REFLECT_8(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_10(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_9(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_11(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_10(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_12(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_11(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_13(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_12(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_14(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_13(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_15(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_14(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_16(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_15(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_17(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_16(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_18(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_17(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_19(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_18(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_20(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_19(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_21(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_20(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_22(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_21(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_23(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_22(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_24(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_23(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_25(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_24(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_26(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_25(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_27(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_26(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_28(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_27(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_29(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_28(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_30(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_29(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_31(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_30(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_32(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_31(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_33(TEMPLATE_ARGS, TYPE, INDEX, FIELD, ...) \
  IMPL_REFLECT_MEMBER(TEMPLATE_ARGS, TYPE, INDEX, FIELD)        \
  EXPAND(IMPL_REFLECT_32(TEMPLATE_ARGS, TYPE, INDEX + 1, __VA_ARGS__))

#define IMPL_REFLECT_X(FIELD, ...)                          \
  static_assert(false,                                      \
                "Too many arguments in REFLECT call! Call " \
                "Karl!");

#define IMPL_REFLECT_NARG(...) EXPAND(IMPL_REFLECT_NARG_(__VA_ARGS__, IMPL_REFLECT_RSEQ_N()))
#define IMPL_REFLECT_NARG_(...) EXPAND(IMPL_REFLECT_ARG_N(__VA_ARGS__))
#define IMPL_REFLECT_ARG_N(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, \
                           _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27, _28, _29, _30,  \
                           _31, _32, _33, _34, _35, _36, _37, _38, _39, _40, _41, _42, _43, _44,  \
                           _45, _46, _47, _48, _49, _50, _51, _52, N, ...)                        \
  N
#define IMPL_REFLECT_RSEQ_N()                                                                      \
  51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28,  \
      27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, \
      2, 1, 0

#define ENABLE_PRIVATE_REFLECTION \
  template <typename T, int I>    \
  friend struct bse::meta_field

#define IMPL_REFLECT_(TEMPLATE_ARGS, N, TYPE, ...) \
  IMPL_REFLECT_TYPE(TEMPLATE_ARGS, TYPE, N)        \
  CONCATENATE(IMPL_REFLECT_, N)                    \
  (TEMPLATE_ARGS, TYPE, 0, __VA_ARGS__)

#define REFLECT(...) \
  EXPAND(IMPL_REFLECT_((), EXPAND(IMPL_REFLECT_NARG(__VA_ARGS__)), __VA_ARGS__, ))

#define REFLECT_TEMPLATE(TEMPLATE_ARGS, ...) \
  EXPAND(IMPL_REFLECT_(TEMPLATE_ARGS, EXPAND(IMPL_REFLECT_NARG(__VA_ARGS__)), __VA_ARGS__, ))

#endif  // BSE_REFLECT_HPP
