#pragma once

#ifndef BSE_DIRECTORY_PATH_HPP
#define BSE_DIRECTORY_PATH_HPP

#include "bse/serialize.hpp"
#include <ext/ext.hpp>

namespace bse {

// Represents a path to a directory
// Should NOT end with an intended filename! (See FilePath)
class DirectoryPath {
 public:
  DirectoryPath() = default;
  DirectoryPath(std::string str);
  DirectoryPath(const char* str);

  DirectoryPath(const DirectoryPath& other) = default;
  DirectoryPath(DirectoryPath&& other) = default;

  // Adds the designated path onto the current path
  void push(DirectoryPath dirs);

  // Removes a given number of entries off the end of the path.
  // equivalent to traversing up the directory tree.
  void pop(int levels = 1);

  DirectoryPath& operator=(const DirectoryPath& other) = default;
  DirectoryPath& operator=(DirectoryPath&& other) = default;

  DirectoryPath& operator=(std::string str);

  // DirectoryPath operator/(const char* other);
  DirectoryPath operator/(const DirectoryPath& other) const;
  DirectoryPath operator/(DirectoryPath&& other) const;

  DirectoryPath& operator/=(const DirectoryPath& other);
  DirectoryPath& operator/=(DirectoryPath&& other);

  bool operator==(const DirectoryPath& other) const;

  // Convert to string implicitly
  operator const std::string() const;
  const std::string& get_string() const;

  // Empties the entire path string
  void clear();

  CLASS_SERFUN((DirectoryPath)) {
    SERIALIZE(self.m_dir_str);
    SERIALIZE(self.m_dirty);
  }
  CLASS_DESERFUN((DirectoryPath)) {
    DESERIALIZE(self.m_dir_str);
    DESERIALIZE(self.m_dirty);
  }

 private:
  // Attempts to make the path as conformant to our requirements as possible
  void clean_path();

  // Resolves as many parent references ("/../") as possible
  void resolve_parents();

  // Returns the entry at the given index
  std::pair<std::string::iterator, std::string::iterator> get_entry(int index);
  std::string get_entry_str(int index);

  // Removes the entry at the given index
  void erase_entry(int index);

  // Returns the number of entries found within the string
  int get_num_entries() const;

  std::string m_dir_str;
  mutable bool m_dirty = false;
};

extern const bse::DirectoryPath ASSETS_ROOT;
extern const bse::DirectoryPath MATERIALS_ROOT;

}  // namespace bse

bse::DirectoryPath operator/(const char* left, bse::DirectoryPath right);
bse::DirectoryPath operator""_dp(const char* op, std::size_t size);

std::ostream& operator<<(std::ostream& os, const bse::DirectoryPath& dp);
#endif  // BSE_DIRECTORY_PATH_HPP
