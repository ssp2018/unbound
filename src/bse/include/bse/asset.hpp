#pragma once

#ifndef BSE_ASSET_HPP
#define BSE_ASSET_HPP

#include "bse/asset_traits.hpp"
#include "bse/file_path.hpp"
#include "bse/loader.hpp"
#include "bse/serialize.hpp"

namespace bse {

// A representation of an active resource.
// The resource is specified using the template-parameter and the path (constructor argument).
// The -> operator can be used to access the ACTUAL resource.
template <typename T>
class Asset {
 public:
  Asset() = default;
  Asset(bse::FilePath path);

  Asset(const Asset<T>& other);
  Asset(Asset<T>&& other);

  Asset<T>& operator=(const Asset<T>& other);
  Asset<T>& operator=(Asset<T>&& other);

  // Gives access to the raw resource.
  const T* operator->() const;

  // Converts to true or false if resource is valid.
  operator bool() const;
  bool is_valid() const;

  // Used to manually load the asset instead of at first use
  void pre_load() const;

  // Returns the ID of the asset
  AssetID<T> get_id() const;

  // Returns the path of the asset
  const bse::FilePath& get_path() const;

  CLASS_SERFUN((Asset<T>)) {
    // bool is_valid = is_valid();
    // SERIALIZE(is_valid);
    bool was_initialized = (m_id != -1);
    SERIALIZE(was_initialized);
    if (was_initialized) {
      SERIALIZE(m_id);
      // SERIALIZE(self.get_path());  //
    }
  }
  CLASS_DESERFUN((Asset<T>)) {
    // bool is_valid;
    // DESERIALIZE(is_valid);
    bool was_initialized;
    DESERIALIZE(was_initialized);

    if (was_initialized) {
      // bse::FilePath path;
      DESERIALIZE(m_id);
      // DESERIALIZE(path);
      // self = Asset(path);
    }
  }

 private:
  AssetID<T> m_id = -1;
};

template <typename T>
Asset<T>::Asset(bse::FilePath path) : m_id{Loader<T>::get_asset_id(path)} {
  static_assert(is_asset_v<T>, UNBOUND_ASSET_ERROR_MESSAGE);
}

template <typename T>
Asset<T>::Asset(const Asset<T>& other) {
  m_id = other.m_id;  //
}

template <typename T>
Asset<T>::Asset(Asset<T>&& other) {
  m_id = other.m_id;
  other.m_id = -1;
}

template <typename T>
Asset<T>& Asset<T>::operator=(const Asset<T>& other) {
  //
  if (this != &other) {
    m_id = other.m_id;
  }

  return *this;
}

template <typename T>
Asset<T>& Asset<T>::operator=(Asset<T>&& other) {
  //
  if (this != &other) {
    m_id = other.m_id;
    other.m_id = -1;
  }

  return *this;
}

template <typename T>
const T* Asset<T>::operator->() const {
  // ASSERT(is_valid()) << "Asset was not initialized with a valid resource! Check filenames.";
  return &Loader<T>::get(m_id);
}

template <typename T>
Asset<T>::operator bool() const {
  return is_valid();
}

template <typename T>
bool Asset<T>::is_valid() const {
  // return m_id != -1;
  return Loader<T>::is_valid(m_id);
}

template <typename T>
void Asset<T>::pre_load() const {
  Loader<T>::get(m_id);
}

template <typename T>
AssetID<T> Asset<T>::get_id() const {
  return m_id;
}

template <typename T>
const bse::FilePath& Asset<T>::get_path() const {
  return Loader<T>::get_path(m_id);
}

}  // namespace bse

// Override hash functionality for Assets
// (Required to be used in unordered_map)
namespace std {
template <typename T>
struct hash<bse::Asset<T>> {
  size_t operator()(const bse::Asset<T>& a) const {
    return hash<bse::FilePath>()(a.get_path());  //
  }
};
}  // namespace std

#endif  // BSE_ASSET_HPP
