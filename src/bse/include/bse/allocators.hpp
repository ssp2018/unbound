#pragma once

#ifndef BSE_ALLOCATORS_HPP
#define BSE_ALLOCATORS_HPP

#include <ext/ext.hpp>

namespace bse {
// Class that allows conditional locking of a mutex. Unlocked in destructor
class ConditionalLock {
 public:
  ConditionalLock(std::mutex& mutex, bool lock) : m_mutex(mutex), m_do_lock(lock) {
    if (m_do_lock) {
      m_mutex.lock();
    }
  }
  virtual ~ConditionalLock() {
    if (m_do_lock) {
      m_mutex.unlock();
    }
  }

 private:
  std::mutex& m_mutex;
  bool m_do_lock;
};

// Basic stack allocator. Only handles raw memory,
// does not call constructors/destructors
class StackAllocator {
 public:
  explicit StackAllocator(size_t size, bool thread_safe = false);
  StackAllocator(const StackAllocator&) = delete;
  // Not thread-safe
  StackAllocator(StackAllocator&& other);
  virtual ~StackAllocator();

  StackAllocator& operator=(StackAllocator&& other);
  StackAllocator& operator=(StackAllocator& other) = delete;

  // Allocates 'size' bytes
  void* allocate(size_t size);

  // Allocates sizeof(T) bytes and casts returned pointer to T
  template <typename T>
  T* allocate() {
    return static_cast<T*>(allocate(sizeof(T)));
  }

  // Free all memory allocated after 'p'
  void free_mem(void* p);

  // Free all allocated memory
  void clear();

  size_t get_size() const;
  size_t get_remaining_size();

 private:
  // Destroys the object. Called in destructor and move assignment
  void destroy();

  // Handles move in move assignment/constructor
  void move_from(StackAllocator&& moved);

  char* m_stack;
  size_t m_size;
  ptrdiff_t m_head;  // Points to the head of the stack
  bool m_thread_safe;
  std::mutex m_mutex;
};

// Basic pool allocator. Only handles raw memory,
// does not call constructors/destructors
class PoolAllocator {
 public:
  // Alignment must be a power of 2
  PoolAllocator(size_t chunk_size, size_t num_chunks, size_t alignment, bool thread_safe = false);
  PoolAllocator(const PoolAllocator&) = delete;
  // Not thread-safe
  PoolAllocator(PoolAllocator&& other);
  virtual ~PoolAllocator();

  PoolAllocator& operator=(PoolAllocator&& other);
  PoolAllocator& operator=(PoolAllocator& other) = delete;

  // Allocates 'size' bytes. Must be <= m_chunk_size
  void* allocate(size_t size);

  // Allocates sizeof(T) bytes and casts returned pointer to T
  template <typename T>
  T* allocate() {
    return static_cast<T*>(allocate(sizeof(T)));
  }

  // Frees a chunk allocated with this pool allocator
  void free_mem(void* p);

  // Free all allocated memory
  void clear();

  size_t get_chunk_size() const;
  size_t get_num_chunks() const;
  size_t get_remaining_chunks();

 private:
  // Destroys the object. Called in destructor and move assignment
  void destroy();

  // Handles move in move assignment/constructor
  void move_from(PoolAllocator&& moved);

  // Initializes chunks at construction or when clearing
  void chunk_setup();

  size_t m_chunk_size;
  size_t m_num_chunks;
  size_t m_remaining_chunks;
  ptrdiff_t m_base;  // Points to the first chunk in the pool
  void* m_first_free;
  bool m_thread_safe;
  std::mutex m_mutex;
};

}  // namespace bse
#endif  // BSE_STACK_ALLOCATOR_HPP