#include "bse/assert.hpp"
#include "bse/runtime_asset.hpp"
#include "bse/runtime_loader.hpp"
#include <ext/ext.hpp>

class TestRA {
 public:
  TestRA(const TestRA& other) {
    //
    m_hash = other.m_hash;
  }

  TestRA(TestRA&& other) {
    //
    m_hash = other.m_hash;

    other.m_hash = 0;
  }

  TestRA& operator=(TestRA&& other) {
    //
    if (this != &other) {
      m_hash = other.m_hash;

      other.m_hash = 0;
    }
    return *this;
  }

  void print_hash() {
    std::cout << m_hash << std::endl;  //
  }

  uint64_t get_hash() const {
    return m_hash;  //
  }

 private:
  struct Intermediate {
    std::string str;
    uint32_t num;
  };

 private:
  TestRA(std::string str, uint32_t num) {
    //
    ASSERT(str.length() == 4);

    memcpy(&m_hash, str.data(), sizeof(uint32_t));
    memcpy(((uint32_t*)&m_hash) + 1, &num, sizeof(uint32_t));
  }

  std::unique_ptr<Intermediate> store() const {
    //
    std::unique_ptr<Intermediate> intermediate = std::make_unique<Intermediate>();
    intermediate->str.resize(4);
    memcpy(intermediate->str.data(), &m_hash, sizeof(uint32_t));
    memcpy(&intermediate->num, ((uint32_t*)&m_hash) + 1, sizeof(uint32_t));

    return intermediate;
  }

  void restore(Intermediate& intermediate) {
    //
    *this = std::move(TestRA(intermediate.str, intermediate.num));
  }

  uint64_t m_hash = 0;

  friend class bse::RuntimeAsset<TestRA>;
};

//
//
//
//
//
//
//
//

TEST(RuntimeAssetTest, Creation) {
  //
  bse::RuntimeAsset<TestRA> rasset("abcd", 42);
  ASSERT_TRUE(rasset.is_valid());
  ASSERT_EQ(rasset->get_hash(), 182072861281);
}

TEST(RuntimeAssetTest, Move) {
  //
  bse::RuntimeAsset<TestRA> rasset("abcd", 42);
  bse::RuntimeAsset<TestRA> rasset2 = std::move(rasset);

  ASSERT_FALSE(rasset.is_valid());
  ASSERT_TRUE(rasset2.is_valid());

  ASSERT_DEATH(rasset->get_hash(), "Fatal..ASSERTION FAILED");
  ASSERT_EQ(rasset2->get_hash(), 182072861281);

  bse::RuntimeAsset<TestRA> rasset3("abcd", 42);
  bse::RuntimeAsset<TestRA> rasset4("gggg", 22);

  rasset4 = std::move(rasset3);

  ASSERT_FALSE(rasset3.is_valid());
  ASSERT_TRUE(rasset4.is_valid());

  ASSERT_DEATH(rasset3->get_hash(), "Fatal..ASSERTION FAILED");
  ASSERT_EQ(rasset4->get_hash(), 182072861281);
}

TEST(RuntimeAssetTest, StoreRestore) {
  //
  bse::RuntimeAsset<TestRA> rasset("abcd", 42);
  bse::RuntimeAsset<TestRA> rasset2 = std::move(rasset);

  ASSERT_FALSE(rasset.is_valid());
  ASSERT_TRUE(rasset2.is_valid());

  ASSERT_DEATH(rasset->get_hash(), "Fatal..ASSERTION FAILED");
  ASSERT_EQ(rasset2->get_hash(), 182072861281);

  bse::RuntimeLoader<TestRA>::store();
  bse::RuntimeLoader<TestRA>::restore();

  ASSERT_FALSE(rasset.is_valid());
  ASSERT_TRUE(rasset2.is_valid());

  ASSERT_DEATH(rasset->get_hash(), "Fatal..ASSERTION FAILED");
  ASSERT_EQ(rasset2->get_hash(), 182072861281);
}