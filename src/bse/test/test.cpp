#include "bse/allocators.hpp"
#include "bse/file.hpp"
#include "bse/file_path.hpp"
#include <ext/ext.hpp>

TEST(StackAllocator, Size) {
  bse::StackAllocator stack(1024);
  EXPECT_EQ(stack.get_size(), 1024);
}

TEST(StackAllocator, RemainingSize) {
  bse::StackAllocator stack(1024);
  int* i = nullptr;
  i = stack.allocate<int>();
  EXPECT_EQ(stack.get_remaining_size(), 1024 - sizeof(int));
}

TEST(StackAllocator, AllocateFree) {
  bse::StackAllocator stack(1024);
  int* i = nullptr;
  i = stack.allocate<int>();
  EXPECT_NE(i, nullptr);

  *i = 5;
  EXPECT_EQ(*i, 5);

  stack.free_mem(i);
  EXPECT_EQ(stack.get_remaining_size(), 1024);
}

TEST(StackAllocator, Clear) {
  bse::StackAllocator stack(sizeof(int) * 100);
  int* is[100];

  for (int i = 0; i < 100; i++) {
    is[i] = stack.allocate<int>();
  }

  stack.clear();
  EXPECT_EQ(stack.get_remaining_size(), sizeof(int) * 100);
}

TEST(StackAllocator, MoveMassAllocateDeallocate) {
  bse::StackAllocator moved_from(sizeof(int) * 100);
  bse::StackAllocator moved_to(std::move(moved_from));
  bse::StackAllocator stack(sizeof(int));
  stack = std::move(moved_to);
  stack = std::move(stack);
  int* is[100];

  for (int i = 0; i < 100; i++) {
    is[i] = stack.allocate<int>();
    *is[i] = i;
  }

  for (int i = 0; i < 100; i++) {
    EXPECT_EQ(*is[i], i);
  }

  for (int i = 0; i < 100; i++) {
    stack.free_mem(is[99 - i]);
  }

  EXPECT_EQ(stack.get_remaining_size(), sizeof(int) * 100);

  for (int i = 0; i < 100; i++) {
    is[i] = stack.allocate<int>();
    *is[i] = i;
  }
}

TEST(PoolAllocator, Size) {
  bse::PoolAllocator pool(32, 100, 32);
  EXPECT_EQ(pool.get_chunk_size(), 32);
  EXPECT_EQ(pool.get_num_chunks(), 100);
  EXPECT_EQ(pool.get_remaining_chunks(), 100);
}

TEST(PoolAllocator, Alignment) {
  for (int i = 0; i < 15; i++) {
    bse::PoolAllocator pool(32, 100, 32);
    ptrdiff_t a = reinterpret_cast<ptrdiff_t>(pool.allocate<int>());
    EXPECT_EQ(a % 32, 0);
  }
}

TEST(PoolAllocator, MoveAllocationFree) {
  bse::PoolAllocator moved_from(32, 100, 32);
  bse::PoolAllocator moved_to(std::move(moved_from));
  bse::PoolAllocator pool(100, 100, 128);
  pool = std::move(moved_to);
  pool = std::move(pool);
  int* is[100];

  for (int i = 0; i < 10; ++i) {
    for (int j = 0; j < 100; ++j) {
      is[j] = pool.allocate<int>();
      *is[j] = j;
    }

    for (int j = 99; j >= 0; --j) {
      EXPECT_EQ(*is[j], j);
      pool.free_mem(is[j]);
    }
  }
}

TEST(PoolAllocator, Clear) {
  bse::PoolAllocator pool(32, 100, 32);
  int* is[100];

  for (int i = 0; i < 100; ++i) {
    is[i] = pool.allocate<int>();
    *is[i] = i;
  }

  pool.clear();
  EXPECT_EQ(pool.get_remaining_chunks(), 100);

  for (int i = 0; i < 100; ++i) {
    is[i] = pool.allocate<int>();
    *is[i] = i;
  }
}

TEST(File, writeFileString) {
  // Output
  std::string output = "Some random data that should be the same!!!\nThis also!";
  bse::write_file(bse::FilePath("testfile.txt"), output);
  // Input
  std::string input = "";
  std::ifstream file("testfile.txt");
  std::string line;
  while (std::getline(file, line, '\0')) {
    input += line;
  }
  file.close();
  ASSERT_EQ(output, input);
}

TEST(File, writeFileStringAppend) {
  // Output
  std::string start = "Something to start with\n";
  bse::write_file(bse::FilePath("testfile.txt"), start);
  std::string output = "This was appended!";
  bse::write_file(bse::FilePath("testfile.txt"), output, true);
  // Input
  std::string input = "";
  std::ifstream file("testfile.txt");
  std::string line;
  while (std::getline(file, line, '\0')) {
    input += line;
  }
  file.close();
  ASSERT_EQ(start + output, input);
}

TEST(File, writeFileChar) {
  // Output
  std::string output = "Some random data that should be the same!!!\nThis also!";
  std::vector<char> vec;
  for (char c : output) vec.push_back(c);
  bse::write_file(bse::FilePath("testfile.txt"), output);
  // Input
  std::string input = "";
  std::ifstream file("testfile.txt");
  std::string line;
  while (std::getline(file, line, '\0')) {
    input += line;
  }
  file.close();
  ASSERT_EQ(output, input);
}

TEST(File, writeFileCharAppend) {
  // Output
  std::string start = "Something to start with\n";
  bse::write_file(bse::FilePath("testfile.txt"), start);
  std::string output = "This was appended!";
  std::vector<char> vec;
  for (char c : output) vec.push_back(c);
  bse::write_file(bse::FilePath("testfile.txt"), output, true);
  // Input
  std::string input = "";
  std::ifstream file("testfile.txt");
  std::string line;
  while (std::getline(file, line, '\0')) {
    input += line;
  }
  file.close();
  ASSERT_EQ(start + output, input);
}

// Uncomment this to be able to read the terminal output
// TEST(File, stop) { getchar(); }