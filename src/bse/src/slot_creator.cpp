#include <bse/assert.hpp>
#include <bse/slot_creator.hpp>

namespace bse {

SlotCreator::SlotCreator() : m_back_slot(0) {}

size_t SlotCreator::create() {
  if (m_free_slots.empty()) {
    return m_back_slot++;
  } else {
    size_t slot = m_free_slots.back();
    m_free_slots.pop_back();
    return slot;
  }
}

void SlotCreator::destroy(size_t slot) {
  ASSERT(exists(slot)) << "Destroying null slot.";
  m_free_slots.push_back(slot);
}

bool SlotCreator::exists(size_t slot) const {
  if (slot >= m_back_slot) {
    return false;
  }

  auto it = std::find(m_free_slots.begin(), m_free_slots.end(), slot);
  if (it != m_free_slots.end()) {
    return false;
  }

  return true;
}

}  // namespace bse