#include "bse/assert.hpp"

namespace bse::detail {
Assert::Assert(const bool cond) : m_cond{cond} {
  //
}

Assert::~Assert() {
  //
  if (!m_cond) {
    std::cout << std::flush;
    std::cerr << std::flush;
    std::abort();
  }
}
}  // namespace bse::detail