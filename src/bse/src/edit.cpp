#include <bse/assert.hpp>
#include <bse/edit.hpp>
#include <bse/utility.hpp>
#include <ext/ext.hpp>

namespace bse {

Edit *Edit::m_instance = nullptr;
std::vector<Edit *> Edit::m_thread_instances;
thread_local Edit *Edit::m_thread_instance = nullptr;
std::mutex Edit::m_lock;

bool Edit::initialize(bool (*initializer)(GLFWwindow *), void (*framer)(), void (*end_framer)(),
                      void (*drawer)(ImDrawData *), GLFWwindow *window) {
  ASSERT(!m_instance) << "Attempted to initialize bse::edit more than once.\n";

  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO &io = ImGui::GetIO();
  (void)io;
  // flags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
  // flags |= ImGuiConfigFlags_NavEnableGamepad;   // Enable Gamepad Controls

  bool result = initializer(window);

  // Setup style
  ImGui::StyleColorsDark();

  io.IniFilename = nullptr;

  m_instance = new Edit(framer, end_framer, drawer);
  return result;
}

void Edit::destroy() {
  ASSERT(m_instance) << "Attempted to delete bse::edit before allocated.\n";
  ImGui::DestroyContext();
  delete m_instance;
  m_instance = nullptr;
}

void Edit::update_maps() {
  for (Edit *thread : m_thread_instances) {
    for (auto pair : thread->m_entry_queue) {
      if (pair.second == nullptr) {
        m_instance->remove(pair.first.key);
        continue;
      }

      std::string path = pair.first.path.empty() ? "root" : std::string(pair.first.path.front());
      Group **group = &m_instance->m_groups[path];
      if (!*group) {
        *group = new Group();
        (*group)->name = path;
        m_instance->m_root_groups.push_back(*group);
      }

      for (size_t i = 1; i < pair.first.path.size(); i++) {
        path += "/" + std::string(pair.first.path[i]);
        Group *&sub_group = m_instance->m_groups[path];
        if (!sub_group) {
          sub_group = new Group();
          sub_group->name = pair.first.path[i];
          (*group)->children.push_back(sub_group);
        }

        group = &sub_group;
      }

      EntryBase *&entry = m_instance->m_entries[pair.first.key];
      if (entry) {
        delete pair.second;
        continue;
      }

      entry = pair.second;

      (*group)->entries.push_back(entry);
    }
    thread->m_entry_queue.clear();
  }
}

bool Edit::is_capturing_keys() {
  ASSERT(m_instance);

  return ImGui::GetIO().WantCaptureKeyboard;
}
bool Edit::is_capturing_mouse() {
  ASSERT(m_instance);

  return ImGui::GetIO().WantCaptureMouse;
}

void Edit::fill() {
  ImGui::Begin("bse::edit", nullptr, ImGuiWindowFlags_AlwaysAutoResize);
  static std::string path;
  m_updated_paths.clear();
  for (Group *group : m_root_groups) {
    group->fill(path, m_updated_paths);
  }
  path.clear();
  ImGui::End();
}

void Edit::new_frame() {
  ASSERT(m_instance);
  if (!m_instance->m_is_enabled) {
    return;
  }

  m_instance->update_maps();

  m_instance->m_framer();
  m_instance->m_is_in_frame = true;

  m_instance->fill();
  m_instance->m_end_framer();
}

void Edit::draw() {
  ASSERT(m_instance);
  if (!m_instance->m_is_in_frame) {
    return;
  }
  if (!m_instance->m_is_gui_enabled) {
    return;
  }

  m_instance->m_drawer(nullptr);
}

void Edit::end_frame() {
  ASSERT(m_instance);

  m_instance->m_is_in_frame = false;
}

void Edit::enable() {
  ASSERT(m_instance);

  m_instance->m_is_enabled = true;
}
void Edit::disable() {
  ASSERT(m_instance);

  m_instance->m_is_enabled = false;
}

void Edit::toggle() {
  ASSERT(m_instance);

  m_instance->m_is_enabled = !m_instance->m_is_enabled;
}

void Edit::toggle_gui() {
  ASSERT(m_instance);

  m_instance->m_is_gui_enabled = !m_instance->m_is_gui_enabled;
}

Edit::Edit(void (*framer)(), void (*end_framer)(), void (*drawer)(ImDrawData *),
           bool do_init_thread)
    : m_framer(framer),
      m_end_framer(end_framer),
      m_drawer(drawer),
      m_is_enabled(true),
      m_is_in_frame(false),
      m_is_gui_enabled(true) {
  if (do_init_thread) {
    m_lock.lock();
    m_thread_instances.push_back(this);
    m_lock.unlock();
    m_thread_instance = this;
  }
}

Edit::~Edit() {
  m_lock.lock();
  auto it = std::find(m_thread_instances.begin(), m_thread_instances.end(), this);
  if (it != m_thread_instances.end()) {
    m_thread_instances.erase(it);
  }
  m_lock.unlock();
  for (auto itr = m_entries.begin(); itr != m_entries.end(); itr++) {
    delete itr->second;
  }
  for (auto itr = m_groups.begin(); itr != m_groups.end(); itr++) {
    delete itr->second;
  }
  for (auto itr = m_entry_queue.begin(); itr != m_entry_queue.end(); itr++) {
    delete itr->second;
  }
}

template <typename T>
void Edit::Entry<T>::fill(std::string &path, std::vector<std::string> &updated_paths) {
  size_t previous_length = path.size();
  path += "/" + name;
  if constexpr (std::is_same_v<T, std::string>) {
    var.reserve(512);
    if (ImGui::InputText(name.c_str(), (char *)var.c_str(), var.capacity(),
                         ImGuiInputTextFlags_EnterReturnsTrue)) {
      is_dirty = true;
      var = var.c_str();
      updated_paths.push_back(path);
    }
  } else if constexpr (std::is_same_v<T, int>) {
    if (ImGui::InputInt(name.c_str(), &var, 1, 100, ImGuiInputTextFlags_EnterReturnsTrue)) {
      is_dirty = true;
      updated_paths.push_back(path);
    }
  } else if constexpr (std::is_same_v<T, float>) {
    if (ImGui::InputFloat(name.c_str(), &var, 0.1f, 1.f, "%.9f",
                          ImGuiInputTextFlags_EnterReturnsTrue)) {
      is_dirty = true;
      updated_paths.push_back(path);
    }
  } else if constexpr (std::is_same_v<T, bool>) {
    if (ImGui::Checkbox(name.c_str(), &var)) {
      is_dirty = true;
      updated_paths.push_back(path);
    }
  } else if constexpr (std::is_same_v<T, Color>) {
    if (ImGui::ColorEdit4(name.c_str(), &var[0])) {
      is_dirty = true;
      updated_paths.push_back(path);
    }
  } else {
    static_assert(std::is_same_v<T, Color>, "Unimplemented type");
  }
  path.erase(previous_length, name.size() + 1);
}

const std::vector<std::string> &Edit::get_updated_paths() {
  ASSERT(m_instance);

  return m_instance->m_updated_paths;
}

void Edit::remove(std::string &path, Group *group) {
  ASSERT(m_instance);

  size_t num_erasions = m_groups.erase(path);
  ASSERT(num_erasions == 1);

  for (Group *child : group->children) {
    size_t previous_length = path.size();
    path += "/" + child->name;
    remove(path, child);
    path.erase(previous_length, path.size() - previous_length);
  }

  for (EntryBase *entry : group->entries) {
    size_t previous_length = path.size();
    path += "/" + entry->name;
    size_t num_erasions = m_instance->m_entries.erase(path);
    ASSERT(num_erasions == 1);
    path.erase(previous_length, path.size() - previous_length);
    delete entry;
  }

  delete group;
}

void Edit::remove_entry(const std::string &path) {
  ASSERT(m_instance);
  // if (m_thread_instance) {
  //  Key key;
  //  key.key = path;
  //  m_thread_instance->m_entry_queue.emplace_back(key, nullptr);
  //}
  m_instance->remove(path);

  auto &queue = m_thread_instance->m_entry_queue;
  auto it = std::remove_if(queue.begin(), queue.end(),
                           [path](const auto &lhs) { return lhs.first.key == path; });
  queue.erase(it, queue.end());
}

void Edit::remove(const std::string &path) {
  std::vector<std::string_view> parts = explode(path, '/');
  std::vector<Group *> &roots = m_instance->m_root_groups;

  Group *group = nullptr;
  for (auto it = roots.begin(); it != roots.end(); it++) {
    Group *root = *it;
    if (root->name == parts[0]) {
      if (parts.size() == 1) {
        roots.erase(it);
      }
      group = root;
      break;
    }
  }

  if (!group) {
    return;
  }

  static std::string tmp_path;
  tmp_path.clear();
  tmp_path += parts[0];
  if (parts.size() == 1) {
    m_instance->remove(tmp_path, group);
    return;
  }

  for (size_t i = 1; i < parts.size() - 1; i++) {
    Group *new_group = nullptr;
    for (Group *child : group->children) {
      if (child->name == parts[i]) {
        new_group = child;
        break;
      }
    }

    if (!new_group) {
      return;
    }

    group = new_group;

    tmp_path += "/" + std::string(parts[i]);
  }

  tmp_path += "/" + std::string(parts.back());
  for (auto it = group->children.begin(); it != group->children.end(); it++) {
    Group *child = *it;
    if (child->name == parts.back()) {
      group->children.erase(it);
      m_instance->remove(tmp_path, child);
      return;
    }
  }

  for (auto it = group->entries.begin(); it != group->entries.end(); it++) {
    EntryBase *entry = *it;
    if (entry->name == parts.back()) {
      group->entries.erase(it);
      size_t num_erasions = m_instance->m_entries.erase(tmp_path);
      ASSERT(num_erasions == 1);
      delete entry;
      return;
    }
  }
}

void Edit::Group::fill(std::string &path, std::vector<std::string> &updated_paths) {
  if (ImGui::TreeNode(name.c_str())) {
    size_t previous_length = path.size();
    path += "/" + name;
    for (Group *child : children) {
      child->fill(path, updated_paths);
    }

    for (EntryBase *entry : entries) {
      entry->fill(path, updated_paths);
    }

    path.erase(previous_length, name.size() + 1);
    ImGui::TreePop();
  }
}

void Edit::extract_basename_and_layer(const char *file, std::string_view &basename,
                                      std::string_view &layer) {
  static const std::unordered_map<std::string_view, bool> LAYERS = {
      {"ai", true},  {"aud", true}, {"bse", true}, {"client", true}, {"cor", true},
      {"ext", true}, {"gfx", true}, {"gmp", true}, {"phy", true},    {"scr", true},
  };
  basename = {};
  layer = {};
  const char *cur = file + strlen(file);
  const char *previous_separator = cur;
  while (cur >= file) {
    if (*cur == '/' || *cur == '\\') {
      if (basename.empty()) {
        basename = cur + 1;
      } else {
        layer = std::string_view(cur + 1, previous_separator - (cur + 1));

        if (LAYERS.find(layer) != LAYERS.end()) {
          break;
        }
      }
      previous_separator = cur;
    }
    cur--;
  }

  ASSERT(!basename.empty() && !layer.empty() && LAYERS.find(layer) != LAYERS.end())
      << "Failed to parse file path.\n";
}

template <typename T>
std::string Edit::create_string_key(const std::string_view &basename, const std::string_view &layer,
                                    const char *var_name, size_t line,
                                    const std::vector<const char *> &prefix, const char *name) {
  // char type;
  // if constexpr (std::is_same_v<T, std::string>) {
  //  type = 's';
  //} else if constexpr (std::is_same_v<T, int>) {
  //  type = 'i';
  //} else if constexpr (std::is_same_v<T, float>) {
  //  type = 'f';
  //} else if constexpr (std::is_same_v<T, bool>) {
  //  type = 'b';
  //} else if constexpr (std::is_same_v<T, Color>) {
  //  type = 'c';
  //} else {
  //  static_assert(std::is_same_v<T, Color>, "Unimplemented type");
  //}

  // std::string key = std::string(layer) + ":" + std::string(basename) + ":";
  // for (const char *part : prefix) {
  //  key += std::string(part) + ":";
  //}

  // key += std::string(var_name) + ":";
  // key += type;
  // key += ":" + std::to_string(line);

  // return key;

  Key k;
  if (!name) {
    k.key = std::string(layer) + "/" + std::string(basename);
    for (const char *part : prefix) {
      k.key += "/" + std::string(part);
    }
  } else {
    std::vector<std::string_view> parts = explode(name, '/');
    if (parts.size() == 1) {
      k.key = "root/" + std::string(parts[0]);
    } else {
      k.key = std::string(parts[0]);
    }
    for (size_t i = 1; i < parts.size(); i++) {
      k.key += "/" + std::string(parts[i]);
    }
    std::string name;
    if (prefix.empty()) {
      while (k.key.back() != '/') {
        k.key.pop_back();
      }
      k.key.pop_back();
      name = parts.back();
    } else {
      name = var_name;
    }

    for (size_t i = 1; i < prefix.size(); i++) {
      k.key += "/" + std::string(prefix[i]);
    }

    k.key += "/" + name;
  }
  return k.key;
}  // namespace bse

template <typename T>
void Edit::push_entry(const T &var, const std::string &key, const std::string_view &basename,
                      const std::string_view &layer, const char *var_name,
                      const std::vector<const char *> &prefix, const char *name) {
  Entry<T> *explicit_entry = new Entry<T>();
  explicit_entry->var = var;
  EntryBase *entry = explicit_entry;

  Key k;
  k.key = key;

  if (!name) {
    entry->name = std::string(var_name);
    k.path = {std::string(layer), std::string(basename)};
    for (const char *part : prefix) {
      k.path.push_back(part);
    }
  } else {
    std::vector<std::string_view> parts = explode(name, '/');
    for (size_t i = 0; i < parts.size(); i++) {
      k.path.emplace_back(parts[i]);
    }
    if (prefix.empty()) {
      entry->name = parts.back();
      k.path.pop_back();
    } else {
      entry->name = var_name;
    }

    for (size_t i = 1; i < prefix.size(); i++) {
      k.path.push_back(prefix[i]);
    }
  }

  m_entry_queue.push_back({k, entry});
}

template <typename T>
T *Edit::get_dirty(const T &var, const char *var_name, const char *file, size_t line,
                   const std::vector<const char *> &prefix, const char *name) {
  ASSERT(m_instance);

  static thread_local Edit thread = Edit(m_framer, m_end_framer, m_drawer, true);

  if (!m_is_in_frame || !m_is_enabled) {
    return nullptr;
  }

  std::string_view basename;
  std::string_view layer;

  extract_basename_and_layer(file, basename, layer);
  // std::string key;
  // if (name) {
  //  key = std::string(name);
  //}
  // key += create_string_key<T>(basename, layer, var_name, line, prefix, name);

  std::string key = create_string_key<T>(basename, layer, var_name, line, prefix, name);

  auto it = m_entries.find(key);
  if (it == m_entries.end()) {
    m_thread_instance->push_entry<T>(var, key, basename, layer, var_name, prefix, name);
    return nullptr;
  }

  if (!it->second->is_dirty) {
    return nullptr;
  }

  return &((Entry<T> *)it->second)->var;
}

bool *Edit::get_dirty_bool(const bool &var, const char *var_name, const char *file, size_t line,
                           const std::vector<const char *> &prefix, const char *name) {
  return get_dirty<bool>(var, var_name, file, line, prefix, name);
}

std::string *Edit::get_dirty_string(const std::string &var, const char *var_name, const char *file,
                                    size_t line, const std::vector<const char *> &prefix,
                                    const char *name) {
  return get_dirty<std::string>(var, var_name, file, line, prefix, name);
}
float *Edit::get_dirty_float(const float &var, const char *var_name, const char *file, size_t line,
                             const std::vector<const char *> &prefix, const char *name) {
  return get_dirty<float>(var, var_name, file, line, prefix, name);
}
int *Edit::get_dirty_int(const int &var, const char *var_name, const char *file, size_t line,
                         const std::vector<const char *> &prefix, const char *name) {
  return get_dirty<int>(var, var_name, file, line, prefix, name);
}

Edit::Color *Edit::get_dirty_color(const Color &var, const char *var_name, const char *file,
                                   size_t line, const std::vector<const char *> &prefix,
                                   const char *name) {
  return get_dirty<Color>(var, var_name, file, line, prefix, name);
}

}  // namespace bse