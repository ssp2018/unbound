#include "bse/task_queue.hpp"

#include "bse/assert.hpp"

namespace bse {

void TaskQueue::queue_task(TaskHandle handle) {
  //
  {
    std::lock_guard lock(m_queue_lock);
    ASSERT(m_next_free_task != (m_next_queued_task - 1) % MAX_TASKS)
        << "TaskQueue ran out of space!";
    m_queued_tasks[m_next_free_task] = handle;
    m_next_free_task = (m_next_free_task + 1) % MAX_TASKS;
  }
  m_queue_cv.notify_one();
}

TaskHandle TaskQueue::dequeue_task() {
  //
  std::unique_lock lock(m_queue_lock);
  if (m_queue_cv.wait_for(lock, std::chrono::milliseconds(4),
                          [this] { return m_next_free_task != m_next_queued_task; })) {
    auto task = m_queued_tasks[m_next_queued_task];
    m_next_queued_task = (m_next_queued_task + 1) % MAX_TASKS;
    lock.unlock();
    m_queue_cv.notify_one();

    return task;
  }
  return TaskHandle::NULL_HANDLE;
}

TaskHandle TaskQueue::dequeue_task_if_available() {
  //
  std::unique_lock lock(m_queue_lock);
  if (m_queue_cv.wait_for(lock, std::chrono::microseconds(0),
                          [this] { return m_next_free_task != m_next_queued_task; })) {
    auto task = m_queued_tasks[m_next_queued_task];
    m_next_queued_task = (m_next_queued_task + 1) % MAX_TASKS;

    lock.unlock();
    m_queue_cv.notify_one();
    return task;
  }

  return TaskHandle::NULL_HANDLE;
}

}  // namespace bse