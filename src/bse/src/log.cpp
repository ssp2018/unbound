#include "bse/log.hpp"

namespace bse {

std::mutex g_log_mutex;

#if !defined(UNBOUND_NO_LOG)
Log::Log(const int level) : m_level{level}, m_lock{g_log_mutex} {
  //
}
#else
Log::Log(const int level) {
  //
}
#endif

Log::~Log() { *this << '\n'; }

template <>
Log& Log::operator<<<const std::string_view&>(const std::string_view& string) {
  return *this << string.front();
}

}  // namespace bse
