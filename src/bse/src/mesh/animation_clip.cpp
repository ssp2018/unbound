#include "bse/mesh/animation_clip.hpp"

#include "bse/file_path.hpp"
#include "bse/log.hpp"
#include "bse/mesh/joint.hpp"
#include "bse/result.hpp"
#include <bse/assert.hpp>
#include <bse/file.hpp>

#define NOTIFYKEY \
  "_notify"  // All animation notify files are formatted as
             // 'modelname'_ANIMKEY_'animationname'_notify.fbx
namespace bse {

AnimationClip::AnimationClip() {}

void AnimationClip::load_animation(const bse::FilePath &file,
                                   std::map<std::string, std::unique_ptr<bse::Joint>> &joint_map) {
  // load file with assimp.
  std::vector<char> mesh_file = *bse::load_file_raw(file);
  Assimp::Importer importer;
  const aiScene *scene = importer.ReadFileFromMemory(&mesh_file[0], mesh_file.size(), NULL);
  ASSERT(scene) << "Failed to load animation";

  if (scene->HasAnimations()) {
    int num_animations = scene->mNumAnimations;
    ASSERT(num_animations) << "File contains no animations\n";

    aiAnimation *animation = nullptr;

    if (num_animations > 1) {
      for (int i = 0; i < num_animations; i++) {
        // Check if animation name contains file name (3d programs can add extra characters)
        std::string file_name = file.get_name();
        std::string animation_name = scene->mAnimations[i]->mName.C_Str();
        if (animation_name.find(file_name) != std::string::npos) {
          animation = scene->mAnimations[i];
          break;
        }
      }
      // If the animation with the correct name was not found, use the first one
      if (!animation) {
        animation = scene->mAnimations[0];
      }
    } else {
      animation = scene->mAnimations[0];
    }

    m_name = file.get_name();
    m_duration = animation->mDuration;
    m_ticks_per_second = animation->mTicksPerSecond;

    // This will contain the number of frames in the animation
    // Assume all channels have the same number of keys
    // TODO: Should probably improve this in the future
    m_num_keys = 0;

    // Store a custom aiNodeAnim for every bone in the skeleton. Every joint can have up to 3
    // channels (Maya sometimes splits bones into scale, rotation and translation). The aiNodeAnim
    // will be used to collect scale, rotation and translation for a single bone from possibly split
    // channels
    std::map<std::string, Channel> channel_map;
    for (int i = 0; i < animation->mNumChannels; i++) {
      // Will contain the name of the joint that this assimp_channel applies to
      std::string joint_name = "";

      aiNodeAnim *assimp_channel = animation->mChannels[i];
      std::string channel_name = assimp_channel->mNodeName.C_Str();

      // If channel name contains '$', it is assumed to be automatically generated bullshit and
      // not an actual bone
      bool extra_channel = (channel_name.find("$") != std::string::npos);

      // Find the bone that this assimp_channel applies to. The assimp_channel name is assumed to
      // contain the full bone name. This means that a bone whose name completely contains another
      // bone name will not behave correctly
      auto it = joint_map.begin();
      while (it != joint_map.end()) {
        auto &[mapped_joint_name, joint] = *it;

        // If channel name completely matches, or if it is an extra channel and it contains the bone
        // name, this bone will be used
        if (channel_name == mapped_joint_name ||
            (extra_channel && channel_name.find(mapped_joint_name) != std::string::npos)) {
          // Correct joint found
          joint_name = mapped_joint_name;

          // Stop searching
          break;
        }
        it++;
      }

      // If no appropriate bone was found, print a warning and continue to the next assimp_channel
      if (joint_name == "") {
        LOG(WARNING) << file.get_string()
                     << ": Could not find an appropriate bone for animation channel \""
                     << channel_name << "\"\n";
        continue;
      }

      // Store position keys in map
      if (assimp_channel->mNumPositionKeys > 1) {
        ASSERT(channel_map[joint_name].num_position_keys == 0)
            << file.get_string() << ": Joint " << joint_name
            << " contained position keys from more than one channel! Make sure that no bones "
               "completely contain the name of another bone.";

        channel_map[joint_name].num_position_keys = assimp_channel->mNumPositionKeys;
        channel_map[joint_name].position_keys = assimp_channel->mPositionKeys;

        if (m_num_keys == 0) {
          m_num_keys = channel_map[joint_name].num_position_keys;
        } else {
          ASSERT(channel_map[joint_name].num_position_keys == m_num_keys)
              << file.get_string()
              << ": Different animation channels contained different numbers of keys. Expected: "
              << m_num_keys << ".\nChannel \"" << channel_name
              << "\": " << channel_map[joint_name].num_position_keys << "\n";
        }
      }

      // Store rotation keys in map
      if (assimp_channel->mNumRotationKeys > 1) {
        ASSERT(channel_map[joint_name].num_rotation_keys == 0)
            << file.get_string() << ": Joint" << joint_name
            << " contained rotation keys from more than one channel! Make sure that no bones "
               "completely contain the name of another bone.";
        channel_map[joint_name].num_rotation_keys = assimp_channel->mNumRotationKeys;
        channel_map[joint_name].rotation_keys = assimp_channel->mRotationKeys;

        if (m_num_keys == 0) {
          m_num_keys = channel_map[joint_name].num_rotation_keys;
        } else {
          ASSERT(channel_map[joint_name].num_rotation_keys == m_num_keys)
              << file.get_string()
              << ": Different animation channels contained different numbers of keys. Expected: "
              << m_num_keys << ".\nChannel \"" << channel_name
              << "\": " << channel_map[joint_name].num_rotation_keys << "\n";
        }
      }

      // Ignore scaling keys, they aren't used
    }

    ASSERT(m_num_keys > 0) << file.get_string() << ": No animation keyframes found.\n";

    // Create keyframes for every key in every joint
    m_animation_keys = std::vector<KeyFrame>(m_num_keys);

    for (auto &[joint_name, animation_node] : channel_map) {
      for (int i = 0; i < m_num_keys; i++) {
        // Ignore keyframes for root bone
        if (joint_name.compare("root")) {
          ASSERT(animation_node.num_position_keys > 1 && animation_node.num_rotation_keys > 1)
              << file.get_string() << ": Joint \"" << joint_name
              << "\" did not contain the expected number of keys. Expected: " << m_num_keys
              << "\nTranslation keys: " << animation_node.num_position_keys
              << "\nRotation keys: " << animation_node.num_rotation_keys << "\n";

          // TODO: Find better way to get this information
          m_animation_keys[i].m_time = animation_node.position_keys[i].mTime;

          // Get skeleton joint id from map
          int joint_id = joint_map[joint_name]->m_id;

          // Store translation keys
          m_animation_keys[i].m_joint_poses[joint_id].m_translation =
              convert_assimp_vec3(animation_node.position_keys[i].mValue);

          // Store rotation keys
          m_animation_keys[i].m_joint_poses[joint_id].m_rotation =
              convert_assimp_quat(animation_node.rotation_keys[i].mValue);
        }
      }
    }
  }

  // Find and load animation notifies
  m_notify_frames = load_notifies(file);

  set_start_joint(joint_map);
}

std::vector<int> AnimationClip::load_notifies(const bse::FilePath &path) {
  std::vector<int> notify_frames;

  std::string notify_file = path.get_name().append(NOTIFYKEY);
  bse::FilePath notify_path = path;
  notify_path.set_name(notify_file);

  if (file_exists(notify_path)) {
    // load notify file with assimp.
    std::vector<char> mesh_file = *bse::load_file_raw(notify_path);
    Assimp::Importer importer;
    const aiScene *scene = importer.ReadFileFromMemory(&mesh_file[0], mesh_file.size(), NULL);
    ASSERT(scene) << "Failed to load animation notify file";
    ASSERT(scene->HasAnimations()) << "Notify file does not contain notify keys";

    for (int i = 0; i < scene->mNumAnimations; i++) {
      for (int j = 0; j < scene->mAnimations[i]->mChannels[0]->mNumPositionKeys; j++) {
        int notify_frame = scene->mAnimations[i]->mChannels[0]->mPositionKeys[j].mTime;
        if (notify_frame >
            0) {  // Awful hack to allow for a single notify in the notify fbx. (If only one key is
                  // present assimp removes it so we have to put an extra frame at the start)
          notify_frames.push_back(notify_frame);
        }
      }
    }
  }

  return notify_frames;
}

void AnimationClip::set_start_joint(std::map<std::string, std::unique_ptr<bse::Joint>> &joint_map) {
  // TODO: Remove temporary implementation
  // Start joint needs to be defined externally on a per animation basis as opposed to being
  // hardcoded here

  if (m_name == "player_mesh_anm_bowdraw" || m_name == "player_mesh_anm_bowshoot" ||
      m_name == "player_mesh_anm_bowdrawloop") {
    m_start_joint = joint_map.at("spine_01").get()->m_id;
  }
}
const std::vector<KeyFrame> &AnimationClip::get_keyframes() const { return m_animation_keys; }
const std::vector<int> &AnimationClip::get_notifies() const { return m_notify_frames; }

int AnimationClip::get_num_keys() const { return m_num_keys; }

float AnimationClip::get_duration() const { return m_duration; }

float AnimationClip::get_ticks_per_second() const { return m_ticks_per_second; }

int AnimationClip::get_start_joint() const { return m_start_joint; }

glm::quat AnimationClip::convert_assimp_quat(aiQuaternion assimp_quat) {
  glm::quat converted_quat;
  converted_quat.x = assimp_quat.x;
  converted_quat.y = assimp_quat.y;
  converted_quat.z = assimp_quat.z;
  converted_quat.w = assimp_quat.w;

  return converted_quat;
}

glm::vec3 AnimationClip::convert_assimp_vec3(aiVector3D assimp_vec3) {
  glm::vec3 converted_vec3;
  converted_vec3.x = assimp_vec3.x;
  converted_vec3.y = assimp_vec3.y;
  converted_vec3.z = assimp_vec3.z;

  return converted_vec3;
}

void AnimationClip::save_animation_sspm(std::ofstream &file) const {
  size_t name_size = m_name.size();
  file.write((char *)&name_size, sizeof(size_t));
  file.write((char *)m_name.data(), sizeof(char *) * name_size);

  file.write((char *)&m_num_keys, sizeof(int));
  file.write((char *)&m_duration, sizeof(float));
  file.write((char *)&m_ticks_per_second, sizeof(float));
  file.write((char *)&m_start_joint, sizeof(int));

  // For every keyframe
  for (int i = 0; i < m_num_keys; i++) {
    const KeyFrame &key_frame = m_animation_keys[i];
    file.write((char *)&key_frame.m_time, sizeof(float));

    size_t num_joint_poses = key_frame.m_joint_poses.size();
    file.write((char *)&num_joint_poses, sizeof(size_t));

    // For every joint pose
    auto it = key_frame.m_joint_poses.begin();
    while (it != key_frame.m_joint_poses.end()) {
      auto &[joint_id, sqt] = *it;

      file.write((char *)&joint_id, sizeof(int));
      file.write((char *)&sqt, sizeof(SQT));

      it++;
    }
  }
  size_t num_notifies = m_notify_frames.size();
  file.write((char *)&num_notifies, sizeof(size_t));
  if (num_notifies > 0) {
    file.write((char *)&m_notify_frames[0], sizeof(int) * m_notify_frames.size());
  }
}

void AnimationClip::load_animation_sspm(std::ifstream &file,
                                        std::map<std::string, AnimationClip> &animations) {
  size_t name_size = 0;
  file.read((char *)&name_size, sizeof(size_t));

  char buffer[256];
  file.read((char *)buffer, sizeof(char *) * name_size);
  m_name = buffer;

  file.read((char *)&m_num_keys, sizeof(int));
  file.read((char *)&m_duration, sizeof(float));
  file.read((char *)&m_ticks_per_second, sizeof(float));
  file.read((char *)&m_start_joint, sizeof(int));

  m_animation_keys.resize(m_num_keys);

  // For every keyframe
  for (int i = 0; i < m_animation_keys.size(); i++) {
    file.read((char *)&m_animation_keys[i].m_time, sizeof(float));

    size_t num_joint_poses = 0;
    file.read((char *)&num_joint_poses, sizeof(size_t));

    for (int j = 0; j < num_joint_poses; j++) {
      int id;
      SQT sqt;
      file.read((char *)&id, sizeof(int));
      file.read((char *)&sqt, sizeof(SQT));
      m_animation_keys[i].m_joint_poses[id] = sqt;
    }
  }

  size_t num_notifies = 0;
  file.read((char *)&num_notifies, sizeof(size_t));
  if (num_notifies > 0) {
    m_notify_frames.resize(num_notifies);
    file.read((char *)&m_notify_frames[0], sizeof(int) * m_notify_frames.size());
  }

  animations.insert(std::make_pair(m_name, *this));
}
}  // namespace bse
