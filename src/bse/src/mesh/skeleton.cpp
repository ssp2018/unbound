#include "bse/mesh/skeleton.hpp"

#include "bse/directory_path.hpp"
#include "bse/file.hpp"
#include "bse/file_path.hpp"
#include "bse/mesh/joint.hpp"
#include <bse/assert.hpp>
#include <bse/mesh/mesh_load_util.hpp>

#define ANIMKEY \
  "_anm_"  // All animation files are formatted as 'modelname'_ANIMKEY_'animationname'.fbx

namespace bse {

Skeleton::Skeleton() {}

Skeleton::Skeleton(const aiScene *scene, bse::FilePath &path, const aiMesh *mesh) {
  ASSERT(scene->HasMeshes()) << "Scene contains no meshes\n";
  ASSERT(scene->mMeshes[0]->HasBones())
      << "Mesh has no joints";  // TODO: Assume there may be more than one mesh in the future?
  m_name = path.get_name();
  build_skeleton(scene, mesh);
  find_animations(path);
}

void print_node_structure(aiNode *node, int level = 0) {
  for (int j = 0; j < level; j++) {
    std::cout << "  ";
  }

  std::cout << node->mName.C_Str() << "\n";

  for (int i = 0; i < node->mNumChildren; i++) {
    print_node_structure(node->mChildren[i], level + 1);
  }
}

// Recursive reading of assimp node with calculated parent transform (absolute transform of all
// existing nodes) (including before the skeleton)
// 'mesh' is the mesh whose bones will be considered
void Skeleton::build_skeleton(const aiScene *scene, const aiMesh *mesh) {
  // Loop through the bone matrix of the mesh and save their names. This allows us to only use nodes
  // that correspond to a bone when building the skeleton. The bool stored in the map is not used
  std::unordered_map<std::string, bool> bone_names;
  for (int i = 0; i < mesh->mNumBones; i++) {
    bone_names[mesh->mBones[i]->mName.C_Str()] = true;
  }

  // Create root joint
  aiNode *root_joint_node = scene->mRootNode->FindNode("root");
  std::unique_ptr<Joint> root_joint = std::make_unique<Joint>();
  root_joint->m_name = root_joint_node->mName.C_Str();
  root_joint->m_id = m_joint_count;

  // Debug print
  // print_node_structure(scene->mRootNode);

  glm::mat4 root_transform = convert_assimp_matrix(root_joint_node->mTransformation);
  glm::mat4 root_parent_transform =
      convert_assimp_matrix(root_joint_node->mParent->mTransformation);

  // These seem to be 0 sometimes
  root_transform[3][3] = 1.0f;
  root_parent_transform[3][3] = 1.0f;

  root_joint->m_relative_transform = root_parent_transform * root_transform;
  root_joint->m_offset_matrix = glm::inverse(root_joint->m_relative_transform);

  // Go through the node tree and build the joint map
  for (int i = 0; i < root_joint_node->mNumChildren; i++) {
    root_joint->m_children.push_back(
        process_joint(root_joint_node->mChildren[i], root_joint->m_relative_transform, bone_names));
  }
  m_joint_count += 1;

  // Finally save root
  m_joint_id_to_name[root_joint->m_id] = "root";
  m_joint_name_map[root_joint->m_name] = std::move(root_joint);
  m_skeleton_root = m_joint_name_map["root"].get();
}

Joint *Skeleton::process_joint(const aiNode *current_node, glm::mat4x4 parent_transform,
                               std::unordered_map<std::string, bool> &bone_names) {
  // If this node is not a bone, do not create a new joint, and instead return child directly
  std::string current_name = current_node->mName.C_Str();
  // Maya splits rotation, translation and scale of a bone by splitting them up into separate nodes
  // (translation is parent of rotation, which is parent of scale, which is parent of actual bone).
  // The names of these extra nodes contain '$', so if a node contains '$', treat is as an extra
  // bullshit node
  if (!bone_names.count(current_name) && current_name.find("$") != std::string::npos) {
    ASSERT(current_node->mNumChildren <= 1) << "An extra bullshit joint has more than one child.";

    glm::mat4 current_transform = convert_assimp_matrix(current_node->mTransformation);

    // Maya scaling matrices end with "Scaling". These should not be used because every single bone
    // is scaled when exporting from Maya with a scale factor, instead of just the root bone. It's
    // unclear how this would affect animated scaling, but we're not supporting that :D
    if (current_name.find("Scaling") != std::string::npos) {
      current_transform = glm::mat4(1.0f);
    }

    if (current_node->mNumChildren) {
      return process_joint(current_node->mChildren[0], parent_transform * current_transform,
                           bone_names);
    }
  }

  std::unique_ptr<Joint> new_joint = std::make_unique<Joint>();

  new_joint->m_name = current_node->mName.C_Str();

  new_joint->m_id = ++m_joint_count;
  new_joint->m_relative_transform = convert_assimp_matrix(current_node->mTransformation);

  // This can sometimes be 0
  new_joint->m_relative_transform[3][3] = 1.0f;
  new_joint->m_offset_matrix = glm::inverse(parent_transform * new_joint->m_relative_transform);

  // This can sometimes be 0
  new_joint->m_relative_transform[3][3] = 1.0f;
  new_joint->m_offset_matrix = glm::inverse(parent_transform * new_joint->m_relative_transform);

  for (int i = 0; i < current_node->mNumChildren; i++) {
    new_joint->m_children.push_back(
        process_joint(current_node->mChildren[i],
                      parent_transform * new_joint->m_relative_transform, bone_names));
  }

  m_joint_id_to_name[new_joint->m_id] = new_joint->m_name;

  // Add joint to map
  m_joint_name_map[new_joint->m_name] = std::move(new_joint);

  return m_joint_name_map.at(current_node->mName.C_Str()).get();
}

void Skeleton::find_animations(bse::FilePath &path) {
  bse::Directory dir = bse::list_dir(path.get_directory());
  // Find all related animations
  for (auto file : dir.files) {
    size_t found = file.get_name().find(path.get_name().append(ANIMKEY));
    size_t found_notify = file.get_name().find("notify");
    // Ignore all notify files
    if (found_notify != std::string::npos) {
      continue;
    }
    if (found != std::string::npos) {
      AnimationClip new_animation;
      new_animation.load_animation(path.get_directory() / file, m_joint_name_map);
      m_animations[file.get_name()] = new_animation;  // Save animation in the map for future use
    }
  }
}

const std::map<std::string, std::unique_ptr<Joint>> &Skeleton::get_joint_map() const {
  return m_joint_name_map;
}

const std::map<std::string, AnimationClip> &Skeleton::get_animations_map() const {
  return m_animations;
}

const int Skeleton::num_joints() const { return m_joint_count; }

const Joint *Skeleton::get_root_joint() const { return m_skeleton_root; }

void Skeleton::set_skeleton_root(Joint *joint) { m_skeleton_root = joint; }

Joint *Skeleton::add_joint(Joint *joint) {
  std::string name = joint->m_name;

  std::unique_ptr<Joint> new_joint = std::make_unique<Joint>();
  new_joint->m_name = joint->m_name;
  new_joint->m_id = joint->m_id;
  new_joint->m_offset_matrix = joint->m_offset_matrix;
  new_joint->m_relative_transform = joint->m_relative_transform;
  for (int i = 0; i < joint->m_children.size(); i++) {
    new_joint->m_children.push_back(joint->m_children[i]);
  }
  m_joint_count += 1;

  m_joint_id_to_name[new_joint->m_id] = new_joint->m_name;

  m_joint_name_map[name] = std::move(new_joint);
  return m_joint_name_map.at(name).get();
}

void Skeleton::load_animations_sspm(std::ifstream &i_file) {
  size_t num_animations = 0;
  i_file.read((char *)&num_animations, sizeof(size_t));

  for (int i = 0; i < num_animations; i++) {
    AnimationClip clip;
    clip.load_animation_sspm(i_file, m_animations);
  }
}

std::string Skeleton::get_joint_name(int id) const {
  if (m_joint_id_to_name.count(id)) {
    return m_joint_id_to_name.at(id);
  } else {
    return "";
  }
}

};  // namespace bse