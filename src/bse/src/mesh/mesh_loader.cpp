#include "bse/assert.hpp"
#include "bse/file.hpp"
#include "bse/file_path.hpp"
#include "bse/log.hpp"
#include "bse/mesh/joint.hpp"
#include "bse/mesh/skeleton.hpp"
#include "bse/result.hpp"
#include <bse/mesh/mesh_load_util.hpp>
#include <bse/mesh/mesh_loader.hpp>
#include <bse/utility.hpp>

namespace bse {
class AnimationClip;
}
#include <ext/ext.hpp>

// Increase number when SSPM format changes
#define SSPM_VERSION 8

namespace bse {
bse::Result<ModelData> MeshLoader::load_mesh(bse::FilePath path, bool refresh) {
  ModelData model_data;
  // refresh = true;

  if (refresh) {
    model_data = load_model_assimp(path);
  } else {
    path.set_extension("SSPM");
    if (bse::file_exists(path)) {
      // load_sspm returns false and does not load the model if the loaded SSPM file is the wrong
      // version
      bool loaded = load_sspm(path, model_data);

      if (!loaded) {
        path.set_extension("fbx");
        model_data = load_model_assimp(path);
      }
    } else {
      path.set_extension("fbx");
      model_data = load_model_assimp(path);
    }
  }

  return model_data;
}

bool MeshLoader::load_sspm(bse::FilePath path, ModelData &out_model) {
  LOG(NOTICE) << "LOADING FROM CUSTOM FORMAT\n";
  path.set_extension("SSPM");

  std::ifstream i_file(path, std::ios::in | std::ios::binary);

  int version = 0;
  // Load SSPM version
  i_file.read((char *)&version, sizeof(int));

  if (version != (int)SSPM_VERSION) {
    i_file.close();
    return false;
  }

  // load the mesh
  MeshData &mesh = out_model.mesh;

  // load bitfield
  i_file.read((char *)&mesh.available_vertex_data, sizeof(mesh.available_vertex_data));

  // load indices
  size_t index_count;
  i_file.read((char *)&index_count, sizeof(size_t));
  mesh.indices.resize(index_count);

  mesh.face_count = index_count / 3;

  i_file.read((char *)&mesh.indices[0], sizeof(unsigned int) * index_count);

  // load vertices.
  size_t vertex_count;
  i_file.read((char *)&vertex_count, sizeof(size_t));

  mesh.vertex_count = vertex_count;

  // positions
  if (mesh.available_vertex_data.test(VertexTypes::POSITION)) {
    mesh.positions.resize(vertex_count * 3);

    i_file.read((char *)&mesh.positions[0], sizeof(float) * mesh.positions.size());
  }
  // normals
  if (mesh.available_vertex_data.test(VertexTypes::NORMAL)) {
    mesh.normals.resize(vertex_count * 3);

    i_file.read((char *)&mesh.normals[0], sizeof(float) * mesh.normals.size());
  }
  // color
  if (mesh.available_vertex_data.test(VertexTypes::COLOR)) {
    mesh.color.resize(vertex_count * 3);

    i_file.read((char *)&mesh.color[0], sizeof(float) * mesh.color.size());
  }
  // uv
  if (mesh.available_vertex_data.test(VertexTypes::UV)) {
    mesh.uv.resize(vertex_count * 2);

    i_file.read((char *)&mesh.uv[0], sizeof(float) * mesh.uv.size());
  }
  // joint
  if (mesh.available_vertex_data.test(VertexTypes::SKELETAL)) {
    mesh.joint_indices.resize(vertex_count * 4);
    mesh.joint_weights.resize(vertex_count * 4);

    // joint_indices
    i_file.read((char *)&mesh.joint_indices[0], sizeof(int) * mesh.joint_indices.size());
    // joint_weights
    i_file.read((char *)&mesh.joint_weights[0], sizeof(float) * mesh.joint_weights.size());
  }

  // load skeleton
  bool has_skeleton;
  i_file.read((char *)&has_skeleton, sizeof(bool));
  if (has_skeleton) {
    out_model.skel = std::make_unique<Skeleton>();
    out_model.skel->set_skeleton_root(load_skeleton_joint(&i_file, out_model.skel.get()));
    out_model.skel->load_animations_sspm(i_file);
  }

  i_file.close();
  return true;
}

void dump_hierarchy_node(aiNode *node, int level = 0) {
  std::string prefix;
  for (int i = 0; i < level; i++) {
    prefix.push_back(' ');
    prefix.push_back(' ');
  }
  std::cout << prefix << node->mName.C_Str();
  print_matrix(convert_assimp_matrix(node->mTransformation), prefix);
  std::cout << "\n";

  for (int i = 0; i < node->mNumChildren; i++) {
    dump_hierarchy_node(node->mChildren[i], level + 1);
  }
}

ModelData MeshLoader::load_model_assimp(bse::FilePath path) {
  std::vector<char> mesh_file = *bse::load_file_raw(path);

  Assimp::Importer importer;
  const aiScene *scene =
      importer.ReadFileFromMemory(&mesh_file[0], mesh_file.size(),
                                  aiProcess_CalcTangentSpace | aiProcess_Triangulate |
                                      aiProcess_SortByPType | aiProcess_JoinIdenticalVertices);

  ASSERT(scene) << "Failed to load asset";

  ModelData model_data;
  // Create skeleton
  if (scene->HasMeshes() && scene->mMeshes[0]->HasBones()) {
    model_data.skel = std::make_unique<Skeleton>(scene, path, scene->mMeshes[0]);
  }

  for (unsigned int i = 0; i < scene->mNumMeshes; i++) {
    glm::mat4 transformation(1.0f);
    glm::mat4 normal_transformation(1.0f);

    // Try to find a node with the same name as the mesh
    if (aiNode *mesh_node = scene->mRootNode->FindNode(scene->mMeshes[i]->mName.C_Str())) {
      transformation = collapse_parent_transforms_by_name(mesh_node);
      normal_transformation = glm::transpose(glm::inverse(transformation));
    }
    // If not found, try to use the file name instead
    else if (aiNode *mesh_node = scene->mRootNode->FindNode(path.get_name().c_str())) {
      transformation = collapse_parent_transforms_by_name(mesh_node);
      normal_transformation = glm::transpose(glm::inverse(transformation));
    } else {
      LOG(WARNING) << "Could not find transform node of mesh '" << scene->mMeshes[i]->mName.C_Str()
                   << "'\n";
    }

    convert_assimp_toimesh(scene->mMeshes[i], model_data, transformation, normal_transformation);
  }

  save_to_custom_format(path, model_data);

  return model_data;
}

void MeshLoader::convert_assimp_toimesh(aiMesh *amesh, ModelData &model_data,
                                        const glm::mat4 &transformation,
                                        const glm::mat4 &normal_transformation) {
  // The mesh might contain other vertices, so the indices in this mesh need to be offset
  unsigned index_offset = model_data.mesh.vertex_count;

  // Store indices
  for (unsigned int i = 0; i < amesh->mNumFaces; i++) {
    aiFace face = amesh->mFaces[i];
    ASSERT(face.mNumIndices == 3)
        << "Mesh loading encountered a face that did not contain 3 vertices.\n";
    for (unsigned int j = 0; j < face.mNumIndices; j++) {
      model_data.mesh.indices.push_back(face.mIndices[j] + index_offset);
    }
  }

  // Assume that the mesh is triangulated
  model_data.mesh.face_count += amesh->mNumFaces;
  model_data.mesh.vertex_count += amesh->mNumVertices;

  if (amesh->HasPositions()) {
    model_data.mesh.available_vertex_data.set(VertexTypes::POSITION, 1);

    model_data.mesh.positions.reserve(model_data.mesh.positions.size() + amesh->mNumVertices * 3);

    for (unsigned int j = 0; j < amesh->mNumVertices; j++) {
      // Store vertex pos in a glm::vec4 and multiply with transformation matrix
      glm::vec4 position(0, 0, 0, 1);

      position.x = amesh->mVertices[j].x;
      position.y = amesh->mVertices[j].y;
      position.z = amesh->mVertices[j].z;

      position = transformation * position;

      // Should technically divide position by position.w, but won't be needed

      model_data.mesh.positions.push_back(position.x);
      model_data.mesh.positions.push_back(position.y);
      model_data.mesh.positions.push_back(position.z);
    }
  }
  if (amesh->HasNormals()) {
    model_data.mesh.available_vertex_data.set(VertexTypes::NORMAL, 1);

    model_data.mesh.normals.reserve(model_data.mesh.normals.size() + amesh->mNumVertices * 3);

    for (unsigned int j = 0; j < amesh->mNumVertices; j++) {
      // Store vertex normal in a glm::vec4 and multiply with normal_transformation matrix
      glm::vec4 normal(0, 0, 0, 0);

      normal.x = amesh->mNormals[j].x;
      normal.y = amesh->mNormals[j].y;
      normal.z = amesh->mNormals[j].z;

      normal = normal_transformation * normal;

      // Should technically divide normal by normal.w, but won't be needed

      glm::vec3 normalized_normal = glm::normalize(glm::vec3(normal));

      model_data.mesh.normals.push_back(normalized_normal.x);
      model_data.mesh.normals.push_back(normalized_normal.y);
      model_data.mesh.normals.push_back(normalized_normal.z);
    }
  }
  if (amesh->HasVertexColors(0) && amesh->mColors[0] != NULL) {
    model_data.mesh.available_vertex_data.set(VertexTypes::COLOR, 1);

    model_data.mesh.color.reserve(model_data.mesh.color.size() + amesh->mNumVertices * 3);

    for (unsigned int j = 0; j < amesh->mNumVertices; j++) {
      // Do gamma correction here
      model_data.mesh.color.push_back(pow(amesh->mColors[0][j].r, 2.2f));
      model_data.mesh.color.push_back(pow(amesh->mColors[0][j].g, 2.2f));
      model_data.mesh.color.push_back(pow(amesh->mColors[0][j].b, 2.2f));
    }
  }
  if (amesh->HasTextureCoords(0) && amesh->mTextureCoords[0] != NULL) {
    model_data.mesh.available_vertex_data.set(VertexTypes::UV, 1);

    model_data.mesh.uv.reserve(model_data.mesh.uv.size() + amesh->mNumVertices * 2);

    for (unsigned int j = 0; j < amesh->mNumVertices; j++) {
      model_data.mesh.uv.push_back(amesh->mTextureCoords[0][j].x);
      model_data.mesh.uv.push_back(amesh->mTextureCoords[0][j].y);
    }
  }

  // Save joint indices and weights
  if (amesh->HasBones()) {
    model_data.mesh.available_vertex_data.set(VertexTypes::SKELETAL, 1);

    // indices_per_bone[i] contains number of saved indices for vertex i
    std::vector<unsigned char> indices_per_bone(amesh->mNumVertices * 4);  // Initialized to 0
    model_data.mesh.joint_indices.resize(model_data.mesh.joint_indices.size() +
                                         amesh->mNumVertices * 4);
    model_data.mesh.joint_weights.resize(model_data.mesh.joint_weights.size() +
                                         amesh->mNumVertices * 4);

    // For every bone
    for (unsigned int k = 0; k < amesh->mNumBones; k++) {
      // For every weight of the bone
      for (unsigned int l = 0; l < amesh->mBones[k]->mNumWeights; l++) {
        unsigned vertex_index = amesh->mBones[k]->mWeights[l].mVertexId;
        float vertex_weight = amesh->mBones[k]->mWeights[l].mWeight;

        // Get number of weights already assigned to this vertex
        unsigned char current_weights = indices_per_bone[vertex_index];

        ASSERT(current_weights < 4)
            << "A vertex used more than 4 weights! This is not supported.\n";

        // Add index_offset to avoid overwriting previous vertices
        model_data.mesh.joint_indices[index_offset + vertex_index * 4 + current_weights] =
            model_data.skel->get_joint_map().at(amesh->mBones[k]->mName.C_Str())->m_id;
        model_data.mesh.joint_weights[index_offset + vertex_index * 4 + current_weights] =
            vertex_weight;
        indices_per_bone[vertex_index]++;
      }
    }
  }
}

void MeshLoader::save_to_custom_format(bse::FilePath path, bse::ModelData &model) {
  LOG(NOTICE) << "SAVING TO CUSTOM FORMAT\n";
  path.set_extension("SSPM");

  std::ofstream o_file(path, std::ios::out | std::ios::binary);

  int version = (int)SSPM_VERSION;
  // Save SSPM version
  o_file.write((char *)&version, sizeof(int));

  // save the mesh
  bse::MeshData &mesh = model.mesh;

  // save bitfield
  std::bitset<VertexTypes::COUNT> avaliable_vertex_data = get_available_vertex_data(&mesh);
  o_file.write((char *)&avaliable_vertex_data, sizeof(avaliable_vertex_data));

  // save indices
  size_t index_count = mesh.indices.size();
  o_file.write((char *)&index_count, sizeof(size_t));

  o_file.write((char *)&mesh.indices[0], sizeof(unsigned int) * index_count);

  // save vertices.
  size_t vertex_count = mesh.vertex_count;
  o_file.write((char *)&vertex_count, sizeof(size_t));
  // positions
  if (avaliable_vertex_data.test(VertexTypes::POSITION)) {
    o_file.write((char *)&mesh.positions[0], sizeof(float) * mesh.positions.size());
  }
  // normals
  if (avaliable_vertex_data.test(VertexTypes::NORMAL)) {
    o_file.write((char *)&mesh.normals[0], sizeof(float) * mesh.normals.size());
  }
  // color
  if (avaliable_vertex_data.test(VertexTypes::COLOR)) {
    o_file.write((char *)&mesh.color[0], sizeof(float) * mesh.color.size());
  }
  // uv
  if (avaliable_vertex_data.test(VertexTypes::UV)) {
    o_file.write((char *)&mesh.uv[0], sizeof(float) * mesh.uv.size());
  }
  // joint
  if (avaliable_vertex_data.test(VertexTypes::SKELETAL)) {
    // joint_indices
    o_file.write((char *)&mesh.joint_indices[0], sizeof(int) * mesh.joint_indices.size());
    // joint_weights
    o_file.write((char *)&mesh.joint_weights[0], sizeof(float) * mesh.joint_weights.size());
  }

  // save skeleton
  if (model.skel != nullptr) {
    const Joint *root_joint = model.skel->get_root_joint();
    bool has_skeleton = true;
    o_file.write((char *)&has_skeleton, sizeof(bool));

    // recursive write
    save_skeleton_joint(root_joint, &o_file);

    // Write animations
    const std::map<std::string, AnimationClip> &animations = model.skel->get_animations_map();
    size_t num_animations = animations.size();
    o_file.write((char *)&num_animations, sizeof(size_t));

    auto it = animations.begin();

    while (it != animations.end()) {
      auto &[name, clip] = *it;

      clip.save_animation_sspm(o_file);

      it++;
    }
  } else {
    bool has_skeleton = false;
    o_file.write((char *)&has_skeleton, sizeof(bool));
  }

  o_file.close();
}

Joint *MeshLoader::load_skeleton_joint(std::ifstream *i_file, Skeleton *skel) {
  Joint *joint = new Joint();

  // Write into this buffer instead of directly into string, this causes very weird
  // crashes when the string is too long to use short-string optimizations
  char joint_name[256];

  // load name
  size_t name_length;
  i_file->read((char *)&name_length, sizeof(size_t));
  ASSERT(name_length <= 256) << "Skeleton bone name was too long.";

  i_file->read((char *)joint_name, sizeof(char *) * name_length);
  joint->m_name = joint_name;
  // load id
  i_file->read((char *)&joint->m_id, sizeof(int));
  // load offset matrix
  i_file->read((char *)&joint->m_offset_matrix[0][0], sizeof(glm::mat4));
  // load relative transform matrix
  i_file->read((char *)&joint->m_relative_transform[0][0], sizeof(glm::mat4));

  // load number of children.
  size_t num_children;
  i_file->read((char *)&num_children, sizeof(size_t));

  for (size_t i = 0; i < num_children; i++) {
    // if (joint->m_children[i] != null) {
    // save_skeleton_joint(joint->m_children[i], o_file);
    // }
    joint->m_children.push_back(load_skeleton_joint(i_file, skel));
  }
  // populate map.
  // skel.m_joint_name_map[joint->m_name] = std::move(joint);

  bse::Joint *ret = skel->add_joint(joint);
  delete joint;
  return ret;
  // return map value
  // return joint;
}

void MeshLoader::save_skeleton_joint(const Joint *joint, std::ofstream *o_file) {
  // save name
  size_t name_length = joint->m_name.length();
  o_file->write((char *)&name_length, sizeof(size_t));
  o_file->write((char *)&(joint->m_name.c_str()[0]), sizeof(char *) * name_length);
  // save id
  o_file->write((char *)&joint->m_id, sizeof(int));
  // save offset matrix
  o_file->write((char *)&joint->m_offset_matrix[0][0], sizeof(glm::mat4));
  // save relative transform matrix
  o_file->write((char *)&joint->m_relative_transform[0][0], sizeof(glm::mat4));

  // write number of children.
  size_t num_children = joint->m_children.size();
  o_file->write((char *)&num_children, sizeof(size_t));
  for (size_t i = 0; i < num_children; i++) {
    // if (joint->m_children[i] != null) {
    save_skeleton_joint(joint->m_children[i], o_file);
    // }
  }
}

std::bitset<VertexTypes::COUNT> MeshLoader::get_available_vertex_data(MeshData *mesh) {
  std::bitset<VertexTypes::COUNT> avaliable_vertex_data;
  // check avaliable data
  if (mesh->positions.size() > 0) {
    avaliable_vertex_data.set(VertexTypes::POSITION, 1);
  }
  if (mesh->normals.size() > 0) {
    avaliable_vertex_data.set(VertexTypes::NORMAL, 1);
  }
  if (mesh->color.size() > 0) {
    avaliable_vertex_data.set(VertexTypes::COLOR, 1);
  }
  if (mesh->uv.size() > 0) {
    avaliable_vertex_data.set(VertexTypes::UV, 1);
  }
  if (mesh->joint_indices.size() > 0) {
    avaliable_vertex_data.set(VertexTypes::SKELETAL, 1);
  }
  return avaliable_vertex_data;
}

}  // namespace bse