#include <bse/mesh/mesh_load_util.hpp>

namespace bse {
glm::mat4 collapse_parent_transforms_by_name(aiNode *node) {
  aiMatrix4x4 transform = node->mTransformation;

  aiNode *parent = node->mParent;

  std::string node_name = node->mName.C_Str();

  while (parent) {
    std::string parent_name = parent->mName.C_Str();

    if (parent_name.find(node_name) != std::string::npos) {
      transform = parent->mTransformation * transform;
    } else {
      break;
    }

    parent = parent->mParent;
  }

  return convert_assimp_matrix(transform);
}

glm::mat4x4 convert_assimp_matrix(const aiMatrix4x4 &matrix) {
  glm::mat4x4 m;
  m[0] = glm::vec4(matrix.a1, matrix.b1, matrix.c1, matrix.d1);
  m[1] = glm::vec4(matrix.a2, matrix.b2, matrix.c2, matrix.d2);
  m[2] = glm::vec4(matrix.a3, matrix.b3, matrix.c3, matrix.d3);
  m[3] = glm::vec4(matrix.a4, matrix.b4, matrix.c4, matrix.d4);
  return m;
}
}  // namespace bse