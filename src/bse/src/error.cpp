#include "bse/error.hpp"

#include "bse/assert.hpp"
#include "bse/log.hpp"

namespace bse {
Error::Error() {
  //
}

Error::Error(const Error& other) {
  //
  m_stream.clear();
  m_stream << other.m_stream.rdbuf();
}

Error::Error(Error&& other) {
  //
  m_stream = std::move(other.m_stream);
}

Error& Error::operator=(const Error& other) {
  //
  if (this != &other) {
    m_stream.clear();
    m_stream << other.m_stream.rdbuf();
  }
  return *this;
}

Error& Error::operator=(Error&& other) {
  //
  if (this != &other) {
    m_stream = std::move(other.m_stream);
  }
  return *this;
}

void Error::break_and_log() {
  bool no_error = true;
  ASSERT(!no_error) << m_stream.rdbuf();  //
}

void Error::log() { LOG(FATAL) << m_stream.rdbuf(); }

std::string Error::get_message() const {
  //
  return m_stream.str();
}
}  // namespace bse
