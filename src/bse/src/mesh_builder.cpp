#include <bse/assert.hpp>
#include <bse/mesh_builder.hpp>
#include <bse/slot_vector.tcc>

namespace bse {

const MeshBuilder::Face& MeshBuilder::get_face(FaceId id) const { return m_faces[id]; }

MeshBuilder::VertexId MeshBuilder::create_vertex() { return m_vertices.insert(Vertex()); }

std::vector<MeshBuilder::FaceId> MeshBuilder::destroy_vertex(VertexId id) {
  std::vector<FaceId> destroyed_faces;
  for (VertexId neighbor : m_vertices[id].neighbors) {
    std::vector<FaceId> destroyed = disconnect(id, neighbor);
    destroyed_faces.insert(destroyed_faces.end(), destroyed.begin(), destroyed.end());
  }

  return destroyed_faces;
}

std::vector<MeshBuilder::FaceId> MeshBuilder::connect_vertices(VertexId a, VertexId b) {
  return connect(a, b);
}

std::vector<MeshBuilder::FaceId> MeshBuilder::disconnect_vertices(VertexId a, VertexId b) {
  return disconnect(a, b);
}

//
// std::vector<MeshBuilder::FaceId> MeshBuilder::connect_vertices(
//    const std::vector<VertexId>& vertices) {
//  std::vector<FaceId> created_faces;
//  for (size_t i = 0; i < vertices.size(); i++) {
//    for (size_t j = i + 1; j < vertices.size(); j++) {
//      std::vector<FaceId> created = connect(vertices[i], vertices[j]);
//      created_faces.insert(created_faces.end(), created.begin(), created.end());
//    }
//  }
//
//  return created_faces;
//}
//
// std::vector<MeshBuilder::FaceId> MeshBuilder::disconnect_vertices(
//    const std::vector<VertexId>& vertices) {
//  std::vector<FaceId> destroyed_faces;
//  for (size_t i = 0; i < vertices.size(); i++) {
//    for (size_t j = i + 1; j < vertices.size(); j++) {
//      std::vector<FaceId> destroyed = disconnect(vertices[i], vertices[j]);
//      destroyed_faces.insert(destroyed_faces.end(), destroyed.begin(), destroyed.end());
//    }
//  }
//
//  return destroyed_faces;
//}

MeshBuilder::Face MeshBuilder::create_face(VertexId a, VertexId b, VertexId c) {
  Face f = {a, b, c};
  std::sort(f.begin(), f.end());
  return f;
}

std::vector<MeshBuilder::FaceId> MeshBuilder::get_face_neighbors(FaceId id) const {
  std::vector<FaceId> neighbors;
  const Face& face = m_faces[id];
  for (uint16_t i = 0; i < 3; i++) {
    uint16_t a = face[i];
    uint16_t b = face[(i + 1) % 3];
    uint16_t c = face[(i + 2) % 3];
    std::vector<VertexId> shared = find_shared_neighbors(m_vertices[a], m_vertices[b]);
    for (VertexId v : shared) {
      if (v == c) {
        continue;
      }

      size_t neighbor = m_faces.find(create_face(a, b, v));
      if (neighbor != -1) {
        neighbors.push_back(neighbor);
      }
    }
  }
  return neighbors;
}

std::vector<MeshBuilder::VertexId> MeshBuilder::find_shared_neighbors(const Vertex& a,
                                                                      const Vertex& b) {
  std::vector<VertexId> shared;
  for (VertexId a_neighbor : a.neighbors) {
    for (VertexId b_neighbor : b.neighbors) {
      if (a_neighbor == b_neighbor) {
        shared.push_back(a_neighbor);
      }
    }
  }

  return shared;
}

std::vector<MeshBuilder::FaceId> MeshBuilder::connect(VertexId a, VertexId b) {
  std::vector<FaceId> created_faces;
  Vertex& va = m_vertices[a];
  auto it = std::find(va.neighbors.begin(), va.neighbors.end(), b);
  if (it != va.neighbors.end()) {
    // Already our neighbor
    return created_faces;
  }

  Vertex& vb = m_vertices[b];
  it = std::find(vb.neighbors.begin(), vb.neighbors.end(), a);
  // If other is not our neighbor, then vice versa should apply.
  ASSERT(it == vb.neighbors.end());

  // Create faces with shared neighbors
  for (VertexId shared : find_shared_neighbors(va, vb)) {
    Face face = create_face(a, b, shared);
    size_t slot = m_faces.find(face);
    if (slot == -1) {
      slot = m_faces.insert(face);
    }
    created_faces.push_back(slot);
  }
  // Form neighborhood
  va.neighbors.push_back(b);
  vb.neighbors.push_back(a);

  return created_faces;
}

std::vector<MeshBuilder::FaceId> MeshBuilder::disconnect(VertexId a, VertexId b) {
  std::vector<FaceId> destroyed_faces;

  Vertex& va = m_vertices[a];
  auto it = std::find(va.neighbors.begin(), va.neighbors.end(), b);
  if (it == va.neighbors.end()) {
    // Is not our neighbor
    return destroyed_faces;
  }
  va.neighbors.erase(it);

  Vertex& vb = m_vertices[b];
  it = std::find(vb.neighbors.begin(), vb.neighbors.end(), a);
  // If other is our neighbor, then vice versa should apply.
  ASSERT(it != vb.neighbors.end());

  vb.neighbors.erase(it);

  for (VertexId shared : find_shared_neighbors(va, vb)) {
    Face face = create_face(a, b, shared);
    size_t slot = m_faces.find(face);
    ASSERT(slot != -1);
    destroyed_faces.push_back(slot);
  }
  return destroyed_faces;
}

}  // namespace bse