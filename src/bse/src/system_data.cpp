#include "bse/system_data.hpp"

namespace bse {
//

SystemData system_data() {
  static SystemData data = initialize_system_data();
  return data;
}

SystemData initialize_system_data() {
  //

#if defined(WIN32)
  STORAGE_ACCESS_ALIGNMENT_DESCRIPTOR alignment = {0};
  WCHAR disk[] = L"\\\\.\\C:";

  DWORD bytes = 0;
  STORAGE_PROPERTY_QUERY query;

  ZeroMemory(&query, sizeof(query));

  HANDLE device = CreateFileW(disk,                                //
                              STANDARD_RIGHTS_READ,                //
                              FILE_SHARE_READ | FILE_SHARE_WRITE,  //
                              nullptr,                             //
                              OPEN_EXISTING,                       //
                              FILE_ATTRIBUTE_NORMAL,               //
                              nullptr                              //
  );

  query.QueryType = PropertyStandardQuery;
  query.PropertyId = StorageAccessAlignmentProperty;

  DeviceIoControl(device,                                       //
                  IOCTL_STORAGE_QUERY_PROPERTY,                 //
                  &query,                                       //
                  sizeof(STORAGE_PROPERTY_QUERY),               //
                  &alignment,                                   //
                  sizeof(STORAGE_ACCESS_ALIGNMENT_DESCRIPTOR),  //
                  &bytes,                                       //
                  nullptr                                       //
  );

  CloseHandle(device);

  SystemData data;
  data.sector_size = alignment.BytesPerPhysicalSector;
  return data;

#else
  SystemData data;
  struct stat fi;
  stat("/", &fi);
  data.sector_size = fi.st_blksize;
  return data;
#endif
}
}  // namespace bse
