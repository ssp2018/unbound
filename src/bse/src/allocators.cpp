#include "bse/allocators.hpp"

#include "bse/assert.hpp"

namespace bse {

StackAllocator::StackAllocator(size_t size, bool thread_safe)
    : m_size(size), m_thread_safe(thread_safe) {
  m_stack = new char[m_size];
  m_head = reinterpret_cast<ptrdiff_t>(m_stack);
}

StackAllocator::StackAllocator(StackAllocator&& other) { move_from(std::move(other)); }

StackAllocator::~StackAllocator() { destroy(); }

StackAllocator& StackAllocator::operator=(StackAllocator&& other) {
  if (&other != this) {
    destroy();
    move_from(std::move(other));
  }
  return *this;
}

void* StackAllocator::allocate(size_t size) {
  ConditionalLock(m_mutex, m_thread_safe);

  ptrdiff_t start = reinterpret_cast<ptrdiff_t>(m_stack);
  size_t remainingSize = static_cast<size_t>(m_size - (m_head - start));
  ASSERT(size <= remainingSize);

  void* allocated = reinterpret_cast<void*>(m_head);

  m_head += size;

  return allocated;
}

void StackAllocator::free_mem(void* p) {
  ptrdiff_t freed = reinterpret_cast<ptrdiff_t>(p);

  ConditionalLock(m_mutex, m_thread_safe);

  ptrdiff_t start = reinterpret_cast<ptrdiff_t>(m_stack);

  ASSERT(freed < m_head);
  ASSERT(freed >= start);

  m_head = freed;
}

void StackAllocator::clear() {
  ConditionalLock(m_mutex, m_thread_safe);

  m_head = reinterpret_cast<ptrdiff_t>(m_stack);
  // memset((void*)m_head, 0, m_size);
}

size_t StackAllocator::get_size() const { return m_size; }

size_t StackAllocator::get_remaining_size() {
  ConditionalLock(m_mutex, m_thread_safe);

  ptrdiff_t start = reinterpret_cast<ptrdiff_t>(m_stack);
  return static_cast<size_t>(m_size - (m_head - start));
}

void StackAllocator::destroy() { delete[] m_stack; }

void StackAllocator::move_from(StackAllocator&& moved) {
  m_stack = moved.m_stack;
  moved.m_stack = nullptr;

  m_size = moved.m_size;
  moved.m_size = 0;

  m_head = moved.m_head;
  m_thread_safe = moved.m_thread_safe;
}

PoolAllocator::PoolAllocator(size_t chunk_size, size_t num_chunks, size_t alignment,
                             bool thread_safe) {
  // Verify that alignment is above 0 and a power of 2
  ASSERT(alignment > 0);
  ASSERT((alignment & (alignment - 1)) == 0);

  ASSERT(chunk_size >= sizeof(void*));
  ASSERT(num_chunks > 0);

  // Add extra space to allow for alignment adjustment
  unsigned char* m_pool = new unsigned char[chunk_size * num_chunks + alignment];

  // Find misalignment
  ptrdiff_t allocation = reinterpret_cast<ptrdiff_t>(m_pool);
  ptrdiff_t misalignment = allocation - (allocation & (~(alignment - 1)));

  // Make sure misalignment is not 0 to ensure there is space for storing offset
  if (misalignment == 0) misalignment = alignment;

  // In the byte before the first pool element, save the offset from original allocation
  unsigned char* alignmentStorage = reinterpret_cast<unsigned char*>(allocation + misalignment - 1);
  *alignmentStorage = static_cast<unsigned char>(misalignment - 1);

  m_base = reinterpret_cast<ptrdiff_t>(alignmentStorage) + 1;

  m_chunk_size = chunk_size;
  m_num_chunks = num_chunks;
  m_remaining_chunks = num_chunks;
  m_thread_safe = thread_safe;

  chunk_setup();
}

PoolAllocator::PoolAllocator(PoolAllocator&& other) { move_from(std::move(other)); }
PoolAllocator::~PoolAllocator() { destroy(); }

PoolAllocator& PoolAllocator::operator=(PoolAllocator&& other) {
  if (&other != this) {
    move_from(std::move(other));
  }

  return *this;
}

void* PoolAllocator::allocate(size_t size) {
  ASSERT(size <= m_chunk_size);
  ConditionalLock(m_mutex, m_thread_safe);
  ASSERT(m_remaining_chunks > 0);

  m_remaining_chunks--;

  void* allocation = m_first_free;

  if (m_remaining_chunks > 0) {
    uintptr_t* next_free = static_cast<uintptr_t*>(m_first_free);
    m_first_free = reinterpret_cast<void*>(*next_free);
  } else {
    m_first_free = nullptr;
  }

  return allocation;
}

void PoolAllocator::free_mem(void* p) {
  ptrdiff_t f = reinterpret_cast<ptrdiff_t>(p);
  ConditionalLock(m_mutex, m_thread_safe);
  ASSERT(f >= m_base);
  ASSERT(f < static_cast<ptrdiff_t>(m_base + m_chunk_size * m_num_chunks));
  ASSERT((f - m_base) % m_chunk_size == 0);

  ptrdiff_t* freed = reinterpret_cast<ptrdiff_t*>(p);

  // Set contents of freed chunk to a pointer to the free chunk pointed to by m_first_free
  *freed = reinterpret_cast<ptrdiff_t>(m_first_free);

  // Set the freed pointer to the new m_first_free
  m_first_free = p;

  m_remaining_chunks++;
}
void PoolAllocator::clear() {
  ConditionalLock(m_mutex, m_thread_safe);
  chunk_setup();
  m_remaining_chunks = m_num_chunks;
}

size_t PoolAllocator::get_chunk_size() const { return m_chunk_size; }
size_t PoolAllocator::get_num_chunks() const { return m_num_chunks; }
size_t PoolAllocator::get_remaining_chunks() {
  ConditionalLock(m_mutex, m_thread_safe);
  return m_remaining_chunks;
}

void PoolAllocator::destroy() {
  if (m_base) {
    unsigned char* offsetLocation = reinterpret_cast<unsigned char*>(m_base - 1);
    size_t offset = *offsetLocation;
    delete[] reinterpret_cast<unsigned char*>(m_base - offset - 1);
  }
}
void PoolAllocator::move_from(PoolAllocator&& moved) {
  m_chunk_size = moved.m_chunk_size;
  m_num_chunks = moved.m_num_chunks;
  m_remaining_chunks = moved.m_remaining_chunks;
  m_base = moved.m_base;
  m_first_free = moved.m_first_free;
  m_thread_safe = moved.m_thread_safe;

  moved.m_base = 0;
  moved.m_chunk_size = 0;
  moved.m_num_chunks = 0;
  moved.m_remaining_chunks = 0;
  moved.m_first_free = nullptr;
}

void PoolAllocator::chunk_setup() {
  m_first_free = reinterpret_cast<void*>(m_base);
  ptrdiff_t current = m_base;

  // Fill chunks with pointers to next chunk
  for (size_t i = 0; i < m_num_chunks; i++) {
    *reinterpret_cast<void**>(current) = reinterpret_cast<void*>(current + m_chunk_size);
    current += m_chunk_size;
  }
}

}  // namespace bse