#include "bse/directory_path.hpp"

#include <bse/assert.hpp>

namespace bse {

const bse::DirectoryPath ASSETS_ROOT = "assets/";
const bse::DirectoryPath MATERIALS_ROOT = ASSETS_ROOT / "materials/";

DirectoryPath::DirectoryPath(std::string str) : m_dir_str{str}, m_dirty{true} {
  //
  clean_path();
}

DirectoryPath::DirectoryPath(const char* str) : m_dir_str{str}, m_dirty{true} {
  //
  clean_path();
}

void DirectoryPath::push(DirectoryPath dirs) {  //
  // Append and mark path as dirty
  m_dir_str += dirs.m_dir_str;
  m_dirty = true;
  clean_path();
}

void DirectoryPath::pop(int levels) {
  //

  // Do nothing if the string is empty
  if (m_dir_str.empty() || levels == 0) {
    return;
  }

  // Find the last occurrence of the / character (except the one at the very end of the string)
  auto last_slash = std::find(std::rbegin(m_dir_str) + 1, std::rend(m_dir_str), '/');

  // Create a new string from the start to the last /
  m_dir_str = std::move(std::string(std::begin(m_dir_str), last_slash.base()));
  m_dirty = false;

  pop(levels - 1);
}

DirectoryPath& DirectoryPath::operator=(std::string str) {
  //
  m_dir_str = str;
  m_dirty = true;
  clean_path();

  return *this;
}

// DirectoryPath DirectoryPath::operator/(const char* other) {
//   //
//   return *this / DirectoryPath(other);
// }

DirectoryPath DirectoryPath::operator/(const DirectoryPath& other) const {
  //
  DirectoryPath result = *this;
  result.push(other);
  return result;
}

DirectoryPath DirectoryPath::operator/(DirectoryPath&& other) const {
  //
  DirectoryPath result = *this;
  result.push(std::move(other));
  return result;
}

DirectoryPath& DirectoryPath::operator/=(const DirectoryPath& other) {
  //
  push(other);
  return *this;
}

DirectoryPath& DirectoryPath::operator/=(DirectoryPath&& other) {
  //
  push(std::move(other));
  return *this;
}

bool DirectoryPath::operator==(const DirectoryPath& other) const {
  //
  return m_dir_str == other.m_dir_str;
}
DirectoryPath::operator const std::string() const {
  return get_string();  //
}

const std::string& DirectoryPath::get_string() const {  //
  return m_dir_str;
}

void DirectoryPath::clear() {
  m_dir_str.clear();  //
}

void DirectoryPath::clean_path() {
  //

  // Do nothing if the string is empty or not dirty
  if (m_dir_str.empty() && !m_dirty) {
    return;
  }

  // Replaces all instances of \ with /
  std::replace(std::begin(m_dir_str), std::end(m_dir_str), '\\', '/');

  // Removes all / duplicates, eg. /// becomes /.
  // Does not change the size of the string, so new_end is recorded.
  auto new_end = std::unique(std::begin(m_dir_str), std::end(m_dir_str),  //
                             [](char a, char b) {                         //
                               return a == '/' && b == a;
                             });

  // Resize to fit the new path.
  m_dir_str.resize(std::distance(std::begin(m_dir_str), new_end));

  //
  // Must never be absolute (start with / or C:/)
  if (m_dir_str[0] == '/') {
    m_dir_str.erase(std::begin(m_dir_str));
  } else if (m_dir_str.size() >= 3 && m_dir_str[1] == ':' && m_dir_str[2] == '/') {
    m_dir_str.erase(std::begin(m_dir_str), std::begin(m_dir_str) + 2);
  }

  // Must end with a /
  if (!m_dir_str.empty()) {
    if (m_dir_str.back() != '/') {
      m_dir_str.append("/");
    }
  }

  // Removes occurrences of ".." that can be resolved by removing other entries in the path
  resolve_parents();

  // We place the restriction that all paths must be relative to the root of the project.
  // Since we cannot resolve paths outside of it without making a very complex implementation, there
  // should _never_ be a ".." at the beginning of the path.
  ASSERT(get_entry_str(0) != "..")
      << "Path must not begin with \"..\"! All paths must be at the asset root or below!";

  // No longer dirty
  m_dirty = false;
}

void DirectoryPath::resolve_parents() {
  //
  auto num_entries = get_num_entries();
  for (int i = 0; i < num_entries - 1; i++) {
    //
    if (get_entry_str(i) != ".." && get_entry_str(i + 1) == "..") {
      //
      erase_entry(i);
      erase_entry(i);
      num_entries -= 2;
      i -= 1;
    }
  }
}

std::pair<std::string::iterator, std::string::iterator> DirectoryPath::get_entry(int index) {
  //
  std::pair<std::string::iterator, std::string::iterator> it_pair;

  auto num_entries = get_num_entries();

  it_pair.first = std::begin(m_dir_str);

  if (index <= 0) {
    it_pair.second = std::find(it_pair.first, std::end(m_dir_str), '/');
  } else {
    for (int i = 0; i < index && i < num_entries - 1; i++) {
      it_pair.first = std::find(it_pair.first, std::end(m_dir_str), '/') + 1;
      it_pair.second = std::find(it_pair.first, std::end(m_dir_str), '/');
    }
  }

  return it_pair;
}

std::string DirectoryPath::get_entry_str(int index) {
  auto entry = get_entry(index);
  return std::string(entry.first, entry.second);
}

void DirectoryPath::erase_entry(int index) {
  //
  auto entry = get_entry(index);
  m_dir_str.erase(entry.first, entry.second + 1);
}

int DirectoryPath::get_num_entries() const {
  //
  return std::count(std::begin(m_dir_str), std::end(m_dir_str), '/');
}

}  // namespace bse

bse::DirectoryPath operator/(const char* left, bse::DirectoryPath right) {
  return bse::DirectoryPath(left) / right;  //
}

bse::DirectoryPath operator""_dp(const char* op, std::size_t) { return bse::DirectoryPath(op); }

std::ostream& operator<<(std::ostream& os, const bse::DirectoryPath& dp) {
  os << dp.get_string();  //
  return os;
}