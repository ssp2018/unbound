#include "bse/tmp/task_bench.hpp"

#include "bse/input_stream.hpp"
#include "bse/log.hpp"
#include "bse/output_stream.hpp"
#include "bse/task_handle.hpp"
#include <bse/task_scheduler.hpp>
#include <ext/ext.hpp>

struct KernelData {
  int start = 0;
  int end = 32;
};

// constexpr uint32_t TOTAL_WORKLOAD = 10000000;
constexpr uint32_t TOTAL_WORKLOAD = 4096 * 4096 * 8;
constexpr uint32_t BENCH_RUNS = 20;

std::atomic<int> g_total;
decltype(std::this_thread::get_id()) g_main_id;

std::array<int, TOTAL_WORKLOAD> g_data;

bse::TaskScheduler::Diagnostics average_diagnostic(bse::TaskScheduler::Diagnostics first,
                                                   int weight,
                                                   bse::TaskScheduler::Diagnostics second) {
  bse::TaskScheduler::Diagnostics result;

  result.total_util = (first.total_util * weight + second.total_util) / (weight + 1);
  for (int i = 0; i < result.thread_util.size(); i++) {
    result.thread_util[i] = (first.thread_util[i] * weight + second.thread_util[i]) / (weight + 1);
  }

  return result;
}

void print_diagnostics(bse::TaskScheduler::Diagnostics diag) {
  std::cout << "Diagnostics:\n"
            << "\tUtil: " << diag.total_util << '\n';
  int i = 0;
  for (auto t : diag.thread_util) {
    std::cout << "\tThread " << i << ": " << t << "\n";
  }
  std::cout << std::endl;
}

class BenchTask : public bse::Task {
 public:
  virtual ~BenchTask() = default;
  void kernel() override;
  KernelData kernel_data;
};

class BenchUniTask : public bse::UniTask {
 public:
  virtual ~BenchUniTask() = default;
  void kernel() override;
};

class StreamBenchTask : public bse::StreamingTask<int, int> {
 public:
  virtual ~StreamBenchTask() = default;
  void kernel(bse::InputStream<int>& input, bse::OutputStream<int>& output,
              bse::StreamInfo& stream_info) override;
};

void BenchTask::kernel() {
  //
  int weird_sum = 0;

  for (int i = kernel_data.start; i < kernel_data.end; i++) {
    weird_sum = 2 * g_data[i] + 3 * weird_sum;
  }

  g_total += weird_sum;
}

void BenchUniTask::kernel() {
  //
  if (std::this_thread::get_id() == g_main_id) {
    LOG(NOTICE) << "Greetings from main thread!\n";
  } else {
    LOG(NOTICE) << "Greetings from thread: " << std::this_thread::get_id() << "!\n";
  }
}

void StreamBenchTask::kernel(bse::InputStream<int>& input, bse::OutputStream<int>& output,
                             bse::StreamInfo& stream_info) {
  //
  for (int i = 0; i < stream_info.num_slices; i++) {
    int weird_sum = 0;
    for (int j = 0; j < stream_info.inputs_per_slice; j++) {
      //
      const int in_data = input[j + i * stream_info.inputs_per_slice];
      weird_sum = 2 * in_data + 3 * weird_sum;
    }
    output[i] = weird_sum;
  }
}

uint32_t benchmark_st() {
  //
  auto st_start_time = std::chrono::high_resolution_clock::now();
  BenchTask bt;
  bt.kernel_data.start = 0;
  bt.kernel_data.end = TOTAL_WORKLOAD;
  bt.kernel();

  auto st_end_time = std::chrono::high_resolution_clock::now();

  return std::chrono::duration_cast<std::chrono::milliseconds>(st_end_time - st_start_time).count();
}

std::pair<uint32_t, bse::TaskScheduler::Diagnostics> benchmark_tasks(int n) {
  //
  std::vector<bse::TaskHandle> task_handles;
  task_handles.resize(n);

  bse::TaskScheduler* ts = bse::TaskScheduler::get();
  ts->compile_diagnostics();  // Resets diagnostics...

  auto mt_start_time = std::chrono::high_resolution_clock::now();
  const uint32_t slice = (TOTAL_WORKLOAD / n);
  for (int i = 0; i < n; i++) {
    // task_handles[i] = ts->add_task(nullptr, &kd[i], &work);
    BenchTask bt;
    bt.kernel_data.start = slice * i;
    bt.kernel_data.end = bt.kernel_data.start + slice;
    task_handles[i] = ts->add_task(bt);
  }

  // for (auto& handle : task_handles) {
  //   ts->wait_on_task(handle);
  // }

  ts->wait_until_idle();

  auto mt_end_time = std::chrono::high_resolution_clock::now();

  bse::TaskScheduler::Diagnostics diag = ts->compile_diagnostics();

  return {
      std::chrono::duration_cast<std::chrono::milliseconds>(mt_end_time - mt_start_time).count(),
      diag};
}

std::pair<uint32_t, bse::TaskScheduler::Diagnostics> benchmark_streaming_tasks() {
  //
  bse::TaskScheduler* ts = bse::TaskScheduler::get();
  ts->compile_diagnostics();  // Resets diagnostics...

  std::array<int, 16 * 128> result = {0};

  auto mt_start_time = std::chrono::high_resolution_clock::now();
  bse::InputStream<int> is;
  bse::OutputStream<int> os;
  is.set_range(&g_data[0], g_data.size());
  os.set_range(&result[0], result.size());
  StreamBenchTask sbt;
  bse::TaskHandle handle = ts->add_streaming_task(sbt, is, os, g_data.size() / result.size(), 1);

  // ts->wait_on_task(handle);

  ts->wait_until_idle();

  int sum = 0;
  for (int e : result) {
    sum += e;
  }
  g_total += sum;

  auto mt_end_time = std::chrono::high_resolution_clock::now();

  bse::TaskScheduler::Diagnostics diag = ts->compile_diagnostics();

  return {
      std::chrono::duration_cast<std::chrono::milliseconds>(mt_end_time - mt_start_time).count(),
      diag};
}

namespace bse::tmp {
void run_benchmark() {
  //

  g_main_id = std::this_thread::get_id();

  for (auto& i : g_data) {
    i = rand();
  }

  LOG(NOTICE) << "Starting benchmark...\n";

  bse::TaskScheduler* ts = bse::TaskScheduler::get();
  BenchUniTask but;
  bse::TaskHandle handle = ts->add_uni_task(but);
  ts->wait_on_task(handle);

  uint32_t st_result = 0;
  for (int i = 0; i < BENCH_RUNS; i++) {
    st_result += benchmark_st();
  }
  LOG(NOTICE) << "Single threaded " << ((float)st_result) / BENCH_RUNS << "\n";

  {
    uint32_t mt_stream_result = 0;
    bse::TaskScheduler::Diagnostics previous_diag;
    for (int i = 0; i < BENCH_RUNS; i++) {
      auto [time, diag] = benchmark_streaming_tasks();
      if (i > 0) {
        previous_diag = average_diagnostic(previous_diag, i, diag);
      } else {
        previous_diag = diag;
      }
      mt_stream_result += time;
    }
    LOG(NOTICE) << "Multi threaded (STREAMING)" << ((float)mt_stream_result) / BENCH_RUNS << "\n";
    print_diagnostics(previous_diag);
  }

  for (int j = 1; j < 20; j++) {
    uint32_t mt_result = 0;
    bse::TaskScheduler::Diagnostics previous_diag;
    for (int i = 0; i < BENCH_RUNS; i++) {
      auto [time, diag] = benchmark_tasks(j);
      if (i > 0) {
        previous_diag = average_diagnostic(previous_diag, i, diag);
      } else {
        previous_diag = diag;
      }
      mt_result += time;
    }
    LOG(NOTICE) << "Multi threaded (" << j << ")" << ((float)mt_result) / BENCH_RUNS << "\n";
    print_diagnostics(previous_diag);
  }

  std::cout << "g_total = " << g_total << std::endl;
}
}  // namespace bse::tmp
