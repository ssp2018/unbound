
#include "bse/task_scheduler.hpp"

#include "bse/timer.hpp"
#include <bse/system_data.hpp>

namespace bse {

TaskScheduler* TaskScheduler::m_singleton = nullptr;

struct ThreadData {
  uint32_t id;
  uint32_t work_depth;
};
thread_local ThreadData g_thread_data;

decltype(std::chrono::high_resolution_clock::now()) g_last_diagnostic_point;

///////////////////////////////
///////////////////////////////
///////////////////////////////
///////////////////////////////

TaskScheduler* TaskScheduler::get() {
  //
  if (m_singleton == nullptr) {
    // m_singleton.reset(new TaskScheduler());
    m_singleton = new TaskScheduler();
  }
  return m_singleton;
}

void TaskScheduler::destroy() {
  //
  if (m_singleton != nullptr) {
    m_singleton->wait_until_idle();
    // m_singleton.reset();
    delete m_singleton;
    m_singleton = nullptr;
  }
}

TaskScheduler::TaskScheduler() {
  //
  for (auto& task : m_tasks) {
    task.unused_next = &task + 1;
    task.handle.iteration = 0u;
    task.handle.index = static_cast<TaskHandle::value_type>(&task - m_tasks.data());
    task.open_tasks = 0;
  }
  m_tasks.back().unused_next = nullptr;

  m_next_free = &m_tasks.front();

  g_thread_data.id = 0;

  m_threads.reserve(NUM_THREADS);
  for (int i = 0; i < m_threads.capacity(); i++) {
    m_threads.emplace_back([i, this]() {
      task_worker(i + 1);  //
    });
  }

  g_last_diagnostic_point = std::chrono::high_resolution_clock::now();
}

TaskScheduler::~TaskScheduler() {
  m_should_finish = true;
  for (auto& thread : m_threads) {
    thread.join();
  }
}

void TaskScheduler::wait_until_idle() {
  //
  ASSERT(g_thread_data.work_depth == 0) << "A thread may never call wait_until_idle inside a task!";

  while (m_num_running_tasks > 0) {
    help_with_work();
  }
}

void TaskScheduler::wait_on_task(TaskHandle handle) {
  //
  while (!is_finished(handle)) {
    help_with_work();
  }
}

TaskScheduler::Diagnostics TaskScheduler::compile_diagnostics() {
  //
  Diagnostics result;

  auto end = std::chrono::high_resolution_clock::now();
  auto total_time =
      std::chrono::duration_cast<std::chrono::duration<float>>(end - g_last_diagnostic_point)
          .count();
  //
  result.total_util = 0.f;

  for (int i = 0; i < NUM_THREADS + 1; i++) {
    result.thread_util[i] = m_thread_work_time[i] / total_time;
    result.total_util += result.thread_util[i];

    m_thread_work_time[i] = 0.f;
  }

  result.total_util /= (NUM_THREADS + 1);

  g_last_diagnostic_point = std::chrono::high_resolution_clock::now();

  return result;
}

uint32_t TaskScheduler::get_thread_id() {
  return g_thread_data.id;  //
}

void TaskScheduler::task_worker(int id) {
  //
  g_thread_data.id = id;
  g_thread_data.work_depth = 0;
  m_thread_work_time[g_thread_data.id] = 0.f;

  while (!m_should_finish) {
    TaskScheduler::ScheduledTask* task = dequeue_task();
    if (task) {
      work_on_task(task);
    }
  }
}

void TaskScheduler::work_on_task(TaskScheduler::ScheduledTask* task) {
  //
  while (!can_execute_task(task)) {
    help_with_work();
  }

  // if (!g_thread_data.is_working) {
  //   g_thread_data.is_working = true;
  // }
  g_thread_data.work_depth++;
  m_thread_work_timer[g_thread_data.id].start();

  task->task->kernel();

  m_thread_work_time[g_thread_data.id] +=
      m_thread_work_timer[g_thread_data.id].stop().value_or(0.f);
  g_thread_data.work_depth--;

  // g_thread_data.is_working = false;

  finish_task(task);
}

bool TaskScheduler::is_finished(TaskHandle handle) {
  //
  TaskScheduler::ScheduledTask* task = get_task(handle);
  if (!task || task->handle.iteration != handle.iteration || task->open_tasks == 0) {
    return true;
  }
  return false;
}

TaskScheduler::ScheduledTask* TaskScheduler::dequeue_task() {
  //
  if (g_thread_data.id == (int)Thread::RENDER) {
    return get_task(m_thread_specific_task_queues[g_thread_data.id].dequeue_task());
  }
  TaskScheduler::ScheduledTask* task =
      get_task(m_thread_specific_task_queues[g_thread_data.id].dequeue_task_if_available());
  if (task) {
    return task;
  }

  task = get_task(m_task_queue.dequeue_task());
  return task;
}

TaskScheduler::ScheduledTask* TaskScheduler::dequeue_task_if_available() {
  //
  TaskScheduler::ScheduledTask* task =
      get_task(m_thread_specific_task_queues[g_thread_data.id].dequeue_task_if_available());
  if (task) {
    return task;
  }

  if (g_thread_data.id == (int)Thread::RENDER) {
    return task;
  }

  task = get_task(m_task_queue.dequeue_task_if_available());
  return task;
}

TaskScheduler::ScheduledTask* TaskScheduler::get_task(TaskHandle handle) {
  //
  // ASSERT(handle.index < m_tasks.size()) << "Tried to use an invalid TaskHandle!";
  if (handle == TaskHandle::NULL_HANDLE || handle.index >= TaskQueue::MAX_TASKS) {
    return nullptr;
  }

  return &m_tasks[handle.index];
}

bool TaskScheduler::can_execute_task(TaskScheduler::ScheduledTask* task) {
  //
  return task->open_tasks == 1;
}

void TaskScheduler::help_with_work() {
  //
  TaskScheduler::ScheduledTask* task = dequeue_task_if_available();
  if (task) {
    work_on_task(task);
  } else {
    m_thread_work_timer[g_thread_data.id].pause();
    std::this_thread::yield();
    m_thread_work_timer[g_thread_data.id].resume();
  }
}

void TaskScheduler::finish_task(TaskScheduler::ScheduledTask* task) {
  //
  const uint32_t open_tasks = (--task->open_tasks);
  if (task->parent_handle != TaskHandle::NULL_HANDLE) {
    TaskScheduler::ScheduledTask* parent_task = get_task(task->parent_handle);
    finish_task(parent_task);
  }

  if (open_tasks == 0) {
    deallocate_task(task);
  }
}

TaskScheduler::StreamDivision TaskScheduler::calculate_stream_division(uint32_t input_size,
                                                                       uint32_t output_size,
                                                                       uint32_t output_element_size,
                                                                       int inputs_per_slice,
                                                                       int outputs_per_slice) {
  //
  StreamDivision result;

  const float INPUTS_PER_OUTPUT = (float)inputs_per_slice / outputs_per_slice;

  const uint32_t NUM_SLICES = input_size / INPUTS_PER_OUTPUT;
  const uint32_t OUTPUT_BYTES_PER_SLICE = (output_element_size * output_size) / NUM_SLICES;

  const auto CACHE_LINE_SIZE = bse::system_data().cache_line_size;

  const uint32_t SLICES_PER_CACHE = CACHE_LINE_SIZE / OUTPUT_BYTES_PER_SLICE;
  const uint32_t MAX_SLICES_PER_TASK = SLICES_PER_CACHE;

  result.num_slices = NUM_SLICES;
  result.max_slices_per_task = MAX_SLICES_PER_TASK;

  return result;
}

TaskScheduler::ScheduledTask* TaskScheduler::allocate_task() {
  //
  TaskScheduler::ScheduledTask* head;
  {
    std::lock_guard lock(m_allocation_lock);

    head = m_next_free;
    ASSERT(head != nullptr) << "Allocated too many tasks! (MAX: " << m_tasks.size() << ")";
    m_next_free = head->unused_next;

    head->parent_handle = TaskHandle::NULL_HANDLE;
    head->handle.iteration++;
  }

  m_num_running_tasks++;

  return head;
}

void TaskScheduler::deallocate_task(TaskScheduler::ScheduledTask* task) {
  //
  {
    std::lock_guard lock(m_allocation_lock);

    task->unused_next = m_next_free;
    m_next_free = task;
    task->handle.iteration++;
  }

  m_num_running_tasks--;
}

}  // namespace bse
