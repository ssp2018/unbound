#include "bse/timer.hpp"

namespace bse {
void Timer::start() {
  //
  if (m_depth == 0) {
    m_accumulated_time = 0.f;
    m_last_point = Clock::now();
  }
  ++m_depth;
}

void Timer::pause() {
  //
  m_accumulated_time += calculate_current_difference();
  m_last_point = Clock::now();
}

void Timer::resume() {
  //
  m_last_point = Clock::now();
}

std::optional<float> Timer::stop() {
  //
  --m_depth;
  if (m_depth == 0) {
    m_accumulated_time += calculate_current_difference();
    m_last_point = Clock::now();
  }
  return m_accumulated_time;
}

float Timer::reset() {
  //
  auto result = *stop();
  start();
  return result;
}

float Timer::calculate_current_difference() {
  return std::chrono::duration_cast<std::chrono::duration<float>>(Clock::now() - m_last_point)
      .count();
}

}  // namespace bse
