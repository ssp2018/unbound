#include "bse/task_handle.hpp"

namespace bse {

const TaskHandle TaskHandle::NULL_HANDLE;

bool TaskHandle::operator==(const TaskHandle& other) {
  //
  return this->full_handle == other.full_handle;
}

bool TaskHandle::operator!=(const TaskHandle& other) {  //
  return !this->operator==(other);
}
}  // namespace bse
