#include <bse/assert.hpp>
#include <bse/utility.hpp>
#include <cwctype>

namespace bse {

std::string trim_whitespace(const std::string &string) {
  std::string str;

  if (string.empty()) {
    return str;
  }

  size_t head = 0;
  size_t tail = 0;
  while (head < string.size() && std::iswspace(string[head])) {
    head++;
  }

  tail = head;
  while (tail < string.size() && !std::iswspace(string[tail])) {
    tail++;
  }

  if (string.size() >= tail) {
    str.assign(&string[head], &string[0] + tail);
  }
  return str;
}

std::vector<std::string_view> explode(const std::string_view &string, char delimiter) {
  std::vector<std::string_view> parts;

  const char *previous_delimiter = string.data();
  const char *cur = previous_delimiter;
  for (; cur != string.data() + string.size(); cur++) {
    if (*cur == delimiter) {
      if (cur > previous_delimiter) {
        parts.emplace_back(previous_delimiter, cur - previous_delimiter);
      }
      previous_delimiter = cur + 1;
    }
  }

  if (cur > previous_delimiter) {
    parts.emplace_back(previous_delimiter, cur - previous_delimiter);
  }

  return parts;
}

void remove(std::string &string, char c) {
  string.erase(std::remove(string.begin(), string.end(), c), string.end());
}

void print_matrix(const glm::mat4 &matrix, std::string prefix) {
  std::cout << "\n";
  for (int i = 0; i < 4; i++) {
    std::cout << prefix << "[ ";
    for (int j = 0; j < 3; j++) {
      std::cout << std::setw(9) << std::fixed << std::setprecision(6);
      std::cout << matrix[i][j] << ", ";
    }
    std::cout << std::setw(9) << std::fixed << std::setprecision(6);
    std::cout << matrix[i][3] << " ]\n";
  }
}

void *align_pointer_up(void *pointer, unsigned alignment) {
  // Assert that alignment is non-zero and power of two
  ASSERT(alignment && (alignment & (alignment - 1)) == 0)
      << "Alignment needs to be >0 and a power of two.";

  // For converting
  union {
    void *void_pointer;
    char *char_pointer;
    ptrdiff_t ptrdiff_pointer;
  };

  void_pointer = pointer;
  ptrdiff_t as_int = ptrdiff_pointer;

  unsigned misalignment = as_int & (alignment - 1);

  char_pointer += (alignment - misalignment);

  return void_pointer;
}

}  // namespace bse