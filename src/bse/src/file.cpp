#include "bse/file.hpp"

#include "bse/error.hpp"
#include "bse/file.hpp"
#include "bse/file_path.hpp"
#include "bse/log.hpp"
#include "bse/result.hpp"
#include "bse/system_data.hpp"
#include <ext/ext.hpp>

namespace bse {

// This function is OS dependent and currently only works on Windows
Directory list_dir(const DirectoryPath path) {
  //
#if defined(WIN32)
  //
  WIN32_FIND_DATA findfiledata;
  HANDLE hFind = INVALID_HANDLE_VALUE;

  char fullpath[MAX_PATH];
  GetFullPathName(path.get_string().c_str(), MAX_PATH, fullpath, 0);
  std::string fp(fullpath);

  Directory result;

  hFind = FindFirstFile((LPCSTR)(fp + "\\*").c_str(), &findfiledata);
  // Make sure anything is found
  if (hFind != INVALID_HANDLE_VALUE) {
    do {
      // Make sure the name does not start with ".". The API will add two results that are "." and
      // ".." and these needs to be removed.
      if (findfiledata.cFileName[0] != '.') {
        // Check if directory
        if (findfiledata.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
          result.sub_dirs.push_back(findfiledata.cFileName);
        } else {
          // Otherwise, handle as file
          result.files.push_back(findfiledata.cFileName);
        }
      }
      // Run until there are not more files (or folders)
    } while (FindNextFile(hFind, &findfiledata) != 0);
  }

  return result;

#else

  Directory result;
  DIR* d;
  struct dirent* dir;
  // d = opendir(path.get_string().c_str());
  // if (d) {
  //   while ((dir = readdir(d)) != NULL) {
  //     if (dir->d_name[0] == '.') {
  //       continue;
  //     }

  //     if (dir->d_type == DT_REG) {
  //       result.files.push_back(dir->d_name);
  //     } else if (dir->d_type == DT_DIR) {
  //       result.sub_dirs.push_back(dir->d_name);
  //     }
  //   }
  //   closedir(d);
  // }

  return result;
#endif
}

// The actual implementation of load_file is OS dependent.
// To ease the introduction of multiple implementations and to avoid repetition, lambdas
// are given that resize and return the size of the data in the given container.
Result<int> load_file_implementation(bse::FilePath path,
                                     std::function<void(unsigned int size)> resize_func,
                                     std::function<char*()> data_func) {
#if defined(WIN32)
  // ERROR_FILE_NOT_FOUND

  // Gets an unbuffered file handle
  HANDLE file_handle = CreateFileA(path.get_string().c_str(),  //
                                   GENERIC_READ,               //
                                   FILE_SHARE_READ,            //
                                   nullptr,                    //
                                   OPEN_EXISTING,              //
                                   FILE_FLAG_NO_BUFFERING,     //
                                   nullptr                     //
  );

  if (file_handle == INVALID_HANDLE_VALUE) {
    DWORD error = GetLastError();

    CloseHandle(file_handle);

    if (error == ERROR_FILE_NOT_FOUND) {
      return bse::Error() << "Failed to load file (File did not exist) " << path;
    } else {
      return bse::Error() << "Failed to load file (Unknown error) " << path;
    }
  }

  // Gets the file size
  DWORD file_size_high = 0;
  DWORD file_size_low = GetFileSize(file_handle, &file_size_high);

  ASSERT(file_size_high == 0)
      << "The file you tried to load in was pretty big, huh? Please check with Karl, he made a bit "
         "of a shortcut here based on the assumption that files won't ever be larger than 4 "
         "gigabytes. I mean, what a weirdo am I right? The file in question was: "
      << path << [&]() {
           CloseHandle(file_handle);
           return "";
         }();

  // Align the size to the system's sector size
  DWORD aligned_size =
      align_to_multiple(file_size_low, static_cast<DWORD>(system_data().sector_size));

  resize_func(aligned_size);
  char* data = data_func();

  // Read the file
  DWORD bytes_read = 0;
  BOOL read_result = ReadFile(file_handle,   //
                              data,          //
                              aligned_size,  //
                              &bytes_read,   //
                              nullptr        //
  );
  CloseHandle(file_handle);

  if (!read_result) {
    DWORD error = GetLastError();

    if (error == ERROR_IO_PENDING) {
      return bse::Error() << "Performing async IO";
    } else {
      return bse::Error() << "Failed to read file (Unknown error) " << path;
    }
  }

  if (bytes_read != file_size_low) {
    return bse::Error() << "Failed to read the entire file: " << path;
  }

  // Resize the resulting string to the size of the actual file
  // result.resize(bytes_read);
  resize_func(bytes_read);
  return 0;

#else
  std::ifstream file(path.get_string());

  if (!file.good()) {
    return bse::Error() << "Failed to load file (Unknown error) " << path;
  }

  file.seekg(0, file.end);
  size_t size = file.tellg();
  file.seekg(0, file.beg);

  resize_func(size);
  file.read(data_func(), size);
  file.close();
  return 0;
#endif
}

Result<std::string> load_file_str(bse::FilePath path) {
  //
  std::string str_result;
  auto result = load_file_implementation(path,
                                         [&](unsigned int size) {
                                           str_result.resize(size);  //
                                         },
                                         [&]() -> char* {
                                           return str_result.data();  //
                                         });
  if (result) {
    return str_result;
  } else {
    return result.get_error();
  }
}

Result<std::vector<char>> load_file_raw(bse::FilePath path) {
  std::vector<char> raw_result;
  auto result = load_file_implementation(path,
                                         [&](unsigned int size) {
                                           raw_result.resize(size);  //
                                         },
                                         [&]() -> char* {
                                           return raw_result.data();  //
                                         });
  if (result) {
    return raw_result;
  } else {
    return result.get_error();
  }
}

bool write_file(const FilePath path, const std::string& data, bool append) {
  std::ofstream file;

  // Choose to append or not
  if (append)
    file.open(path, std::ofstream::app);
  else
    file.open(path);

  if (file.is_open()) {
    file.write(data.c_str(), sizeof(char) * data.length());
  } else {
    LOG(FATAL) << "Could not open file: " << path << "\n";
    return false;
  }

  file.close();
  return true;
}

bool write_file(const FilePath path, const std::vector<char> data, bool append) {
  std::ofstream file;

  // Choose to append or not
  if (append)
    file.open(path, std::ofstream::binary | std::ofstream::app);
  else
    file.open(path, std::ofstream::binary);

  if (file.is_open()) {
    file.write(data.data(), sizeof(char) * data.size());
  } else {
    LOG(FATAL) << "Could not open file: " << path << "\n";
    return false;
  }

  file.close();
  return true;
}

bool file_exists(const FilePath path) {
  std::ifstream f(path.get_string().c_str());
  return f.good();
  // struct stat buffer;
  // return (stat(path.get_name().c_str(), &buffer) == 0);
}

}  // namespace bse