#include <bse/memory_debug.hpp>
#include <ext/ext.hpp>
#undef new
#undef malloc
#undef free

#ifdef UNBOUND_CUSTOM_ALLOCATION

extern std::map<void *, AllocInfo> *m_allocation_map;
extern std::map<std::string, size_t> *m_size_per_file;
void report_memory_leaks() {
  auto &it = get_allocation_map().begin();
  while (it != get_allocation_map().end()) {
    std::cout << it->second.file << " : line " << it->second.line << " : " << it->second.size
              << " bytes\n";

    it++;
  }

  if (get_allocation_map().size() > 0) {
    int plsstop = 0;
  }
}

void report_size_per_file() {
  std::cout << "\n\nSize per file:\n";
  auto &it = m_size_per_file->begin();
  while (it != m_size_per_file->end()) {
    std::cout << it->first << " : " << it->second << " bytes\n";

    it++;
  }
}

void *operator new(std::size_t size, const char *file, int line) {
  void *p = malloc(size);
  if (m_allocation_map) {
    (*m_allocation_map)[p] = {file, line, size};
  }
  if (m_size_per_file) {
    (*m_size_per_file)[file] += size;
  }

  return p;
}

void operator delete(void *ptr) {
  if (m_allocation_map && (*m_allocation_map).count(ptr)) {
    if (m_size_per_file) {
      (*m_size_per_file)[(*m_allocation_map)[ptr].file] -= (*m_allocation_map)[ptr].size;
    }

    m_allocation_map->erase(ptr);
  }

  free(ptr);
}

void *operator new[](size_t size, const char *file, int line) {
  void *p = malloc(size);

  if (m_allocation_map) {
    (*m_allocation_map)[p] = {file, line, size};
  }
  if (m_size_per_file) {
    (*m_size_per_file)[file] += size;
  }

  return p;
}
void operator delete[](void *ptr) {
  if (m_allocation_map && (*m_allocation_map).count(ptr)) {
    if (m_size_per_file) {
      (*m_size_per_file)[(*m_allocation_map)[ptr].file] -= (*m_allocation_map)[ptr].size;
    }

    m_allocation_map->erase(ptr);
  }

  free(ptr);
}

#endif