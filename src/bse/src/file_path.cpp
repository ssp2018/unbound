
#include "bse/file_path.hpp"

#include "bse/assert.hpp"
#include "bse/directory_path.hpp"

namespace bse {
//

FilePath::FilePath(std::string str) : m_file_str{str} {
  //
  clean_path();
}

FilePath::FilePath(const char* str) : m_file_str{str} {
  //
  clean_path();
}

FilePath::~FilePath() {}

bool FilePath::operator==(const FilePath& other) const {
  return m_directory == other.m_directory && m_file_str == other.m_file_str;
}

FilePath::operator const std::string() const {
  //
  return get_string();
}

std::string FilePath::get_name() const {
  //
  return std::string(std::cbegin(m_file_str), std::cbegin(m_file_str) + m_extension_point);
}

void FilePath::set_name(std::string name) {
  //
  std::string new_file_str = name.append(".").append(get_extension());
  m_file_str = std::move(new_file_str);
  m_extension_point =
      std::find(std::begin(m_file_str), std::end(m_file_str), '.') - std::begin(m_file_str);
}

std::string FilePath::get_extension() {
  //
  return std::string(std::begin(m_file_str) + m_extension_point + 1, std::end(m_file_str));
}

void FilePath::set_extension(std::string extension) {
  //
  ASSERT(!extension.empty()) << "A file must have an extension!";
  std::string new_file_str = get_name().append(".").append(extension);
  m_file_str = std::move(new_file_str);
  m_extension_point =
      std::find(std::begin(m_file_str), std::end(m_file_str), '.') - std::begin(m_file_str);
}

std::string FilePath::get_string() const {
  //
  return m_directory.get_string() + m_file_str;
}

const DirectoryPath& FilePath::get_directory() const {
  //
  return m_directory;
}

std::string FilePath::get_file() const {
  return m_file_str;  //
}

void FilePath::set_directory(DirectoryPath path) {
  //
  m_directory = std::move(path);
}

void FilePath::clean_path() {
  //
  ASSERT(!m_file_str.empty()) << "File may not be an empty string!";
  // if (m_file_str.empty()) {
  // return;
  // }

  auto last_slash = std::find(std::rbegin(m_file_str), std::rend(m_file_str), '/');
  if (last_slash != std::rend(m_file_str)) {
    m_directory = std::string(std::begin(m_file_str), last_slash.base() - 1);
    m_file_str = std::string(last_slash.base(), std::end(m_file_str));
  }

  auto ext_found = std::find(std::begin(m_file_str), std::end(m_file_str), '.');
  ASSERT(ext_found != std::end(m_file_str)) << "A file must have an extension!";
  m_extension_point = ext_found - std::begin(m_file_str);
}

std::ostream& operator<<(std::ostream& os, const bse::FilePath& fp) {
  os << fp.get_string();  //
  return os;
}
}  // namespace bse

bse::FilePath operator/(const bse::DirectoryPath& left, const bse::FilePath& right) {
  bse::FilePath fp = right;
  fp.set_directory(left / right.get_directory());
  return fp;
}

bse::FilePath operator""_fp(const char* op, std::size_t) {
  return bse::FilePath(op);  //
}
