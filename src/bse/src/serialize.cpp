#include "bse/serialize.hpp"

namespace bse {

// SerializedData& operator+=(SerializedData& lhs, const SerializedData& rhs) {
//   //
//   lhs.bytes.insert(lhs.bytes.end(), rhs.bytes.begin(), rhs.bytes.end());
//   return lhs;
// }

void SerializedData::reset() {
  bytes.clear();
  if (bytes.size() == 0) {
    bytes.resize(1 << 21);
  }
  head = 0U;
}

void serialize(const std::string& str_value, SerializedData& data) {
  //
  // data.bytes.resize(sizeof(std::string::size_type) + str_value.length());

  std::string::size_type string_size = str_value.length();

  // Write string length
  serialize(string_size, data);

  // Write string
  memcpy(&data.bytes[data.head], &str_value[0], string_size * sizeof(str_value[0]));
  data.head += string_size * sizeof(str_value[0]);
}

void deserialize(std::string& str_value, const SerializedData& data, int& offset) {
  //
  std::string::size_type size{0};
  deserialize(size, data, offset);
  if (size > 1000) {
    LOG(WARNING) << "Deserializing a very large string, was this intentional? (" << size
                 << " bytes)";
  }

  str_value.resize(size);

  // Read string
  memcpy(&str_value[0], &data.bytes[offset], size);
  offset += size;
}

void serialize(const glm::vec2& vec_value, SerializedData& data) {
  //
  serialize(vec_value.x, data);
  serialize(vec_value.y, data);
  return;
}

void serialize(const glm::vec3& vec_value, SerializedData& data) {
  //
  serialize(vec_value.x, data);
  serialize(vec_value.y, data);
  serialize(vec_value.z, data);
  return;
}

void serialize(const glm::vec4& vec_value, SerializedData& data) {
  //
  serialize(vec_value.x, data);
  serialize(vec_value.y, data);
  serialize(vec_value.z, data);
  serialize(vec_value.w, data);
  return;
}

void serialize(const glm::ivec2& vec_value, SerializedData& data) {
  //
  serialize(vec_value.x, data);
  serialize(vec_value.y, data);
  return;
}

void serialize(const glm::ivec3& vec_value, SerializedData& data) {
  //
  serialize(vec_value.x, data);
  serialize(vec_value.y, data);
  serialize(vec_value.z, data);
  return;
}

void serialize(const glm::ivec4& vec_value, SerializedData& data) {
  //
  serialize(vec_value.x, data);
  serialize(vec_value.y, data);
  serialize(vec_value.z, data);
  serialize(vec_value.w, data);
  return;
}

void serialize(const glm::mat2& mat_value, SerializedData& data) {
  //
  memcpy(&data.bytes[data.head], (void*)(&mat_value[0][0]), 4 * sizeof(float));
  data.head += 4 * sizeof(float);
  return;
}

void serialize(const glm::mat3& mat_value, SerializedData& data) {
  //
  memcpy(&data.bytes[data.head], (void*)(&mat_value[0][0]), 9 * sizeof(float));
  data.head += 9 * sizeof(float);
  return;
}

void serialize(const glm::mat4& mat_value, SerializedData& data) {
  //
  memcpy(&data.bytes[data.head], (void*)(&mat_value[0][0]), 16 * sizeof(float));
  data.head += 16 * sizeof(float);
  return;
}

void deserialize(glm::vec2& vec_value, const SerializedData& data, int& offset) {
  //
  deserialize(vec_value.x, data, offset);
  deserialize(vec_value.y, data, offset);
}

void deserialize(glm::vec3& vec_value, const SerializedData& data, int& offset) {
  //
  deserialize(vec_value.x, data, offset);
  deserialize(vec_value.y, data, offset);
  deserialize(vec_value.z, data, offset);
}

void deserialize(glm::vec4& vec_value, const SerializedData& data, int& offset) {
  //
  deserialize(vec_value.x, data, offset);
  deserialize(vec_value.y, data, offset);
  deserialize(vec_value.z, data, offset);
  deserialize(vec_value.w, data, offset);
}

void deserialize(glm::ivec2& vec_value, const SerializedData& data, int& offset) {
  //
  deserialize(vec_value.x, data, offset);
  deserialize(vec_value.y, data, offset);
}

void deserialize(glm::ivec3& vec_value, const SerializedData& data, int& offset) {
  //
  deserialize(vec_value.x, data, offset);
  deserialize(vec_value.y, data, offset);
  deserialize(vec_value.z, data, offset);
}

void deserialize(glm::ivec4& vec_value, const SerializedData& data, int& offset) {
  //
  deserialize(vec_value.x, data, offset);
  deserialize(vec_value.y, data, offset);
  deserialize(vec_value.z, data, offset);
  deserialize(vec_value.w, data, offset);
}

void deserialize(glm::mat2& mat_value, const SerializedData& data, int& offset) {
  //
  deserialize(mat_value[0], data, offset);
  deserialize(mat_value[1], data, offset);
}

void deserialize(glm::mat3& mat_value, const SerializedData& data, int& offset) {
  //
  deserialize(mat_value[0], data, offset);
  deserialize(mat_value[1], data, offset);
  deserialize(mat_value[2], data, offset);
}

void deserialize(glm::mat4& mat_value, const SerializedData& data, int& offset) {
  //
  deserialize(mat_value[0], data, offset);
  deserialize(mat_value[1], data, offset);
  deserialize(mat_value[2], data, offset);
  deserialize(mat_value[3], data, offset);
}


void serialize(const std::deque<float>& value, bse::SerializedData& data) {
  serialize(value.size(), data);
  for (auto& item : value) {
    serialize(item, data);
  }
}

void deserialize(std::deque<float>& value, const bse::SerializedData& data, int& offset) {
  std::deque<float>::size_type size;
  deserialize(size, data, offset);
  for (int i = 0; i < size; i++) {
    float item;
    deserialize(item, data, offset);
    value.push_back(item);
  }
}

}  // namespace bse