#include "bse/reflect.hpp"  // for IMPL_REFLECT_1, REFLECT

#include "scr/constructors.hpp"    // for Constructors
#include "scr/object.hpp"          // for Object
#include "test_lock.hpp"           // for test_lock
#include <scr/dynamic_object.hpp>  // for DynamicObject, DynamicObject::Use...
#include <scr/scr.tcc>             // for expose_type
#include <scr/script.hpp>          // for Script
namespace scr {
struct DynamicTable;
}
using namespace scr;
using namespace detail;

namespace merp {
struct BarMerp {
  bool wow = true;
  std::vector<std::string> strings = {"str1", "str2"};
};

struct FooMerp {
  int a = 1;
  std::string b = "yo";
  BarMerp bar;
};
}  // namespace merp

using namespace merp;

TEST(ScriptReflect, Copy) {
  std::lock_guard lock_guard(test_lock);
  scr::expose_type<FooMerp>("Foo", scr::Constructors<FooMerp()>(), "a", &FooMerp::a, "b",
                            &FooMerp::b, "bar", &FooMerp::bar);
  scr::expose_type<BarMerp>("Bar", scr::Constructors<BarMerp()>(), "wow", &BarMerp::wow, "strings",
                            &BarMerp::strings);
  Script script("", R"(
      local cpp = require("cpp")
      local ref = require("reflect")
      function copy(input, ref_out)
        local out = ref.type(input).new()
  
        for i,member in ipairs(ref.members(input)) do
          out[member] = input[member]
          ref_out[member] = input[member]
        end
  
        return out
      end
  )");

  std::function<FooMerp(FooMerp&, FooMerp&)> copy = script.return_value()["copy"];

  FooMerp in;
  in.a = 2;
  in.b = "hi";
  in.bar.wow = false;
  in.bar.strings = {"tjoho"};
  FooMerp ref_out;
  FooMerp out = copy(in, ref_out);

  EXPECT_EQ(in.a, out.a);
  EXPECT_EQ(in.b, out.b);
  EXPECT_EQ(in.bar.wow, out.bar.wow);
  EXPECT_EQ(in.bar.strings, out.bar.strings);

  EXPECT_EQ(in.a, ref_out.a);
  EXPECT_EQ(in.b, ref_out.b);
  EXPECT_EQ(in.bar.wow, ref_out.bar.wow);
  EXPECT_EQ(in.bar.strings, ref_out.bar.strings);
}
