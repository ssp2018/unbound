#include "test_lock.hpp"         // for test_lock
#include <scr/detail/state.hpp>  // for State
#include <scr/object.hpp>        // for Object
#include <scr/script.hpp>        // for Script
using namespace scr;
using namespace detail;

TEST(Script, TableReturn) {
  std::lock_guard lock_guard(test_lock);

  Script script("", R"(

 def = {
         ghi = {
                 bark = 50,
                 woof = "abc",
         },
         foo = 1,
 }
 )");

  Object return_value = script.return_value()["def"];

  EXPECT_FALSE(return_value.is_null());
  EXPECT_TRUE(return_value.has_children());

  Object ghi = return_value["ghi"];
  EXPECT_FALSE(ghi.is_null());
  EXPECT_TRUE(ghi.has_children());

  Object bark = ghi["bark"];
  EXPECT_FALSE(bark.is_null());
  EXPECT_EQ(50, int(bark));

  Object woof = ghi["woof"];
  EXPECT_FALSE(woof.is_null());
  EXPECT_EQ("abc", woof.get<std::string>());
}

TEST(Script, ReadOnly) {
  std::lock_guard lock_guard(test_lock);

  sol::state& lua = scr::detail::State::get();

  Script script("", R"EOF(
 readonly = {
         ghi = {
                 bark = 50,
                 woof = "abc",
         },
         foo = 1,
 }
 )EOF");

  sol::table readonly = script.return_value()["readonly"];

  lua["readonly"] = readonly;

  sol::table keys = lua.script(R"(

keys = {}
for k,v in pairs(readonly) do keys[k] = v end

 readonly.foo = 2
 readonly["bar"] = 3
 readonly.ghi.bark = 42

return keys
)");

  EXPECT_TRUE(keys["ghi"].valid());
  EXPECT_TRUE(keys["foo"].valid());
  EXPECT_EQ(1, readonly["foo"].get<int>());
  EXPECT_FALSE(readonly["bar"].valid());
  EXPECT_EQ(50, readonly["ghi"]["bark"].get<int>());
}
