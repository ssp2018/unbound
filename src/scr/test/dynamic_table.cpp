#include "test_lock.hpp"
#include <ext/ext.hpp>
#include <scr/dynamic_object.hpp>
#include <scr/dynamic_table.hpp>
#include <scr/object.hpp>
#include <scr/script.hpp>
using namespace scr;
using namespace detail;

TEST(DynamicTable, CopyObject) {
  std::lock_guard lock_guard(test_lock);
  Script script("", R"(

a = 1
b = "c"


function func(self)
  return self.b
end

)");

  DynamicTable table(script.return_value());

  DynamicObject* a = table.get_if("a");
  DynamicObject* b = table.get_if("b");
  DynamicObject* func = table.get_if("func");

  EXPECT_NE(nullptr, a);
  EXPECT_NE(nullptr, b);
  EXPECT_NE(nullptr, func);

  EXPECT_EQ(1, a->get<int>());
  EXPECT_EQ("c", b->get<std::string>());
  EXPECT_EQ("c", func->get<std::function<std::string(DynamicTable&)>>()(table));
}

TEST(DynamicTable, Modify) {
  DynamicTable table;
  table["a"] = 2;
  table["b"] = "d";

  detail::State::get()["table"] = &table;
  detail::State::get().script(R"(

table.a = 3
table.b = "f"

function func(self)
  return self.b
end

table.func = cpp.Object.new("func")

)");

  DynamicObject* a = table.get_if("a");
  DynamicObject* b = table.get_if("b");
  DynamicObject* func = table.get_if("func");

  EXPECT_NE(nullptr, a);
  EXPECT_NE(nullptr, b);
  EXPECT_NE(nullptr, func);

  EXPECT_EQ(3, a->get<int>());
  EXPECT_EQ("f", b->get<std::string>());
  EXPECT_EQ("f", func->get<std::function<std::string(const DynamicTable&)>>()(table));
}

TEST(DynamicTable, InvokeMethod) {
  std::lock_guard lock_guard(test_lock);

  DynamicTable table;
  table["a"] = 2;
  table["b"] = "d";

  Script script("", R"(
function func(self)
  self.a = 3
  self.b = "f"
end
)");

  table["func"] = script.return_value()["func"];
  detail::State::get()["table"] = &table;
  detail::State::get().script(R"(
table:func()
)");

  DynamicObject* a = table.get_if("a");
  DynamicObject* b = table.get_if("b");

  EXPECT_NE(nullptr, a);
  EXPECT_NE(nullptr, b);

  EXPECT_EQ(3, a->get<int>());
  EXPECT_EQ("f", b->get<std::string>());
}

TEST(DynamicTable, Nesting) {
  std::lock_guard lock_guard(test_lock);
  Script script("", R"(
local cpp = require("cpp")

a = {b = {le_c = "c"}}

function func(self)
  self.t = { u = { le_v = "v" }}
end

)");

  DynamicTable table(script.return_value());

  DynamicObject* a = table.get_if("a");
  EXPECT_NE(nullptr, a);
  DynamicTable* a_table = a->get_if<DynamicTable>();
  EXPECT_NE(nullptr, a_table);

  DynamicObject* b = a_table->get_if("b");
  EXPECT_NE(nullptr, b);
  DynamicTable* b_table = b->get_if<DynamicTable>();
  EXPECT_NE(nullptr, b_table);

  DynamicObject* le_c = b_table->get_if("le_c");
  EXPECT_NE(nullptr, le_c);
  EXPECT_EQ("c", le_c->get<std::string>());

  std::function<void(DynamicTable&)> func = script.return_value()["func"];
  func(table);

  DynamicObject* t = table.get_if("t");
  EXPECT_NE(nullptr, t);
  DynamicTable* t_table = t->get_if<DynamicTable>();
  EXPECT_NE(nullptr, t_table);

  DynamicObject* u = t_table->get_if("u");
  EXPECT_NE(nullptr, u);
  DynamicTable* u_table = u->get_if<DynamicTable>();
  EXPECT_NE(nullptr, u_table);

  EXPECT_EQ("v", (*u_table)["le_v"].get<std::string>());
}