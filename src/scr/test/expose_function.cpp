#include <scr/object.hpp>  // for Object
#include <scr/scr.tcc>     // for expose_function
using namespace scr;
using namespace detail;

TEST(ExposeFunction, ReturnArgLambda) {
  std::string suffix = "suffix";
  expose_function("func", [suffix](const std::string& prefix) { return prefix + " - " + suffix; });

  std::function<std::string(const std::string&)> func = Object("cpp")["func"];

  std::string prefix = "prefix";
  std::string result = func(prefix);
  EXPECT_EQ(prefix + " - " + suffix, result);
}

namespace {
std::string c_func(const std::string& prefix) { return prefix; }
}  // namespace

TEST(ExposeFunction, ReturnArgC) {
  expose_function("func", c_func);

  std::function<std::string(const std::string&)> func = Object("cpp")["func"];

  std::string prefix = "prefix";
  std::string result = func(prefix);
  EXPECT_EQ(prefix, result);
}

namespace {
struct Foo {
  std::string method(const std::string& prefix) { return prefix + " - " + suffix; }

  std::string suffix;
};
}  // namespace

TEST(ExposeFunction, ReturnArgMethod) {
  expose_function("func", &Foo::method);

  std::function<std::string(Foo*, const std::string)> func = Object("cpp")["func"];
  std::string prefix = "prefix";
  std::string suffix = "suffix";

  Foo foo;
  foo.suffix = suffix;
  std::string result = func(&foo, prefix);
  EXPECT_EQ(prefix + " - " + suffix, result);
}
