#include "scr/constructors.hpp"  // for Constructors
#include <scr/detail/state.hpp>  // for State
#include <scr/dynamic_object.hpp>
#include <scr/scr.tcc>  // for expose_type
using namespace scr;
using namespace detail;

namespace {
struct Foo {
  Foo() : a(1001) {}
  Foo(int i) : a(i) {}

  int get_a() const { return a; }
  void set_a(int i) { a = i; }

  int a = 1000;

  DynamicObject getter(const std::string& key) { return a; }

  void setter(const std::string& key, const DynamicObject& object) { a = object; }
};
}  // namespace

TEST(ExposeType, Constructors) {
  expose_type<Foo>("Foo", Constructors<Foo(), Foo(int)>(), "a", &Foo::a, "get_a", &Foo::get_a,
                   "set_a", &Foo::set_a);

  sol::state& lua = State::get();

  lua.script(R"(
    f1 = cpp.Foo.new()
    a1 = f1.a
    f2 = cpp.Foo.new(3)
    a2 = f2.a
)");

  int a1 = lua["a1"];
  int a2 = lua["a2"];

  EXPECT_EQ(1001, a1);
  EXPECT_EQ(3, a2);
}

TEST(ExposeType, Members) {
  expose_type<Foo>("Foo", Constructors<Foo(), Foo(int)>(), "a", &Foo::a, "get_a", &Foo::get_a,
                   "set_a", &Foo::set_a);
  sol::state& lua = State::get();

  lua.script(R"(
    f1 = cpp.Foo.new()
    f1.a = 1337
    a1 = f1.a
)");

  int a1 = lua["a1"];
  EXPECT_EQ(1337, a1);
}

TEST(ExposeType, Methods) {
  expose_type<Foo>("Foo", Constructors<Foo(), Foo(int)>(), "a", &Foo::a, "get_a", &Foo::get_a,
                   "set_a", &Foo::set_a);
  sol::state& lua = State::get();

  lua.script(R"(
    f1 = cpp.Foo.new()
    f1:set_a(1337)
    a1 = f1:get_a()
)");

  int a1 = lua["a1"];
  EXPECT_EQ(1337, a1);
}

TEST(ExposeType, StandardContainers) {
  expose_type<Foo>("Foo", Constructors<Foo()>(), "a", &Foo::a);
  sol::state& lua = State::get();

  sol::function func = lua.script(R"(
    
  return function(container)
    container[1] = nil
    container[1] = 0
    return container[2]
  end

)");

  {
    std::function<int(std::vector<int>&)> f = func;
    std::vector<int> v = {10, 11, 12};
    int first = func(v);
    EXPECT_EQ(2, v.size());
    EXPECT_EQ(v[1], first);
    EXPECT_EQ(0, v[0]);
  }

  func = lua.script(R"(
    
  return function(container)
    container[1] = nil
    container[1].a = 0
    return container[2]
  end

)");

  {
    std::function<Foo&(std::vector<Foo>&)> f = func;
    std::vector<Foo> v = {10, 11, 12};
    Foo& first = func(v);
    EXPECT_EQ(2, v.size());
    EXPECT_EQ(v[1].a, first.a);
    EXPECT_EQ(0, v[0].a);
  }
}

TEST(ExposeType, GlmTypes) {
  expose_type<glm::vec3>("vec3", Constructors<glm::vec3()>(), "x", &glm::vec3::x, "y",
                         &glm::vec3::y, "z", &glm::vec3::z);
  sol::state& lua = State::get();

  sol::function func = lua.script(R"(
    
  return function(glm)
    glm.x = 5
    return glm.y
  end

)");

  {
    std::function<float(glm::vec3&)> f = func;
    glm::vec3 v = {1, 2, 3};
    float first = func(v);
    EXPECT_FLOAT_EQ(5, v.x);
    EXPECT_FLOAT_EQ(v.y, first);
  }
}

DynamicObject foo_getter(Foo& foo, const std::string& key) { return foo.a; }

void foo_setter(Foo& foo, const std::string& key, const DynamicObject& object) { foo.a = object; }

TEST(ExposeType, GetterSetterStatic) {
  expose_type<Foo>("Foo", Constructors<Foo(), Foo(int)>(), "a", &Foo::a, "get_a", &Foo::get_a,
                   "set_a", &Foo::set_a);
  scr::expose_getter<Foo>("Foo", foo_getter);
  scr::expose_setter<Foo>("Foo", foo_setter);

  sol::state& lua = State::get();

  lua.script(R"(
    f1 = cpp.Foo.new()
    a1 = f1.a
    f1.a = 2
    a2 = f1.a
)");
  int a1 = lua["a1"];
  int a2 = lua["a2"];

  EXPECT_EQ(1001, a1);
  EXPECT_EQ(2, a2);
}

TEST(ExposeType, GetterSetterMethod) {
  expose_type<Foo>("Foo", Constructors<Foo(), Foo(int)>(), "a", &Foo::a, "get_a", &Foo::get_a,
                   "set_a", &Foo::set_a);
  scr::expose_getter<Foo>("Foo", &Foo::getter);
  scr::expose_setter<Foo>("Foo", &Foo::setter);

  sol::state& lua = State::get();

  lua.script(R"(
    f1 = cpp.Foo.new()
    a1 = f1.a
    f1.a = 2
    a2 = f1.a
)");
  int a1 = lua["a1"];
  int a2 = lua["a2"];

  EXPECT_EQ(1001, a1);
  EXPECT_EQ(2, a2);
}