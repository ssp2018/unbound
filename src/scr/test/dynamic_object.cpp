#include "bse/reflect.hpp"         // for IMPL_REFLECT_1, REFLECT
#include "scr/constructors.hpp"    // for Constructors
#include "scr/object.hpp"          // for Object
#include "test_lock.hpp"           // for test_lock
#include <scr/dynamic_object.hpp>  // for DynamicObject, DynamicObject::Use...
#include <scr/dynamic_table.hpp>
#include <scr/scr.tcc>     // for expose_type
#include <scr/script.hpp>  // for Script

using namespace scr;
using namespace detail;

struct Foo {
  int a = 1;
  Foo() { a = 1; }
  Foo(int v) { a = v; }
  Foo(const Foo& other) { a = other.a; }
};

REFLECT((Foo), a)

TEST(DynamicObject, GetAndSetCopy) {
  std::lock_guard lock_guard(test_lock);

  DynamicObject obj;
  {
    double n;
    bool b;
    std::string str;
    Foo foo;
    DynamicTable table;

    obj = true;
    n = obj;
    b = obj;
    str = obj;
    foo = obj;
    table = obj;
    EXPECT_TRUE(b);
    EXPECT_FLOAT_EQ(1.f, n);
    EXPECT_EQ("", str);
    EXPECT_EQ(1, foo.a);
    EXPECT_EQ(0, table.size());

    obj = "str1";
    n = obj;
    b = obj;
    str = obj;
    foo = obj;
    table = obj;
    EXPECT_FALSE(b);
    EXPECT_FLOAT_EQ(0.f, n);
    EXPECT_EQ("str1", str);
    EXPECT_EQ(1, foo.a);
    EXPECT_EQ(0, table.size());

    obj = 5;
    n = obj;
    b = obj;
    str = obj;
    foo = obj;
    table = obj;
    EXPECT_TRUE(b);
    EXPECT_FLOAT_EQ(5.f, n);
    EXPECT_EQ("", str);
    EXPECT_EQ(5, foo.a);
    EXPECT_EQ(0, table.size());

    obj = Foo(2);
    n = obj;
    b = obj;
    str = obj;
    foo = obj;
    EXPECT_FALSE(b);
    EXPECT_FLOAT_EQ(0.f, n);
    EXPECT_EQ("", str);
    EXPECT_EQ(2, foo.a);

    DynamicTable tmp_table;
    tmp_table["i"] = 6;
    obj = tmp_table;
    n = obj;
    b = obj;
    str = obj;
    table = obj;
    EXPECT_FALSE(b);
    EXPECT_FLOAT_EQ(0.f, n);
    EXPECT_EQ("", str);
    EXPECT_EQ(1, table.size());
    EXPECT_EQ(6, table["i"].get<int>());
  }

  Foo my_foo(1);
  obj = &my_foo;
  Foo foo = obj;
  EXPECT_EQ(1, foo.a);

  my_foo.a = 2;
  EXPECT_EQ(1, foo.a);

  obj = my_foo;
  EXPECT_EQ(1, foo.a);

  Foo foo2 = obj;
  my_foo.a = 1;
  EXPECT_EQ(2, foo2.a);
}

TEST(DynamicObject, GetAndSetRef) {
  std::lock_guard lock_guard(test_lock);

  DynamicObject obj;
  {
    obj = true;
    bool* b = obj.get_if<bool>();
    EXPECT_NE(nullptr, b);
    EXPECT_TRUE(b);

    obj = "str1";
    std::string* str = obj.get_if<std::string>();
    EXPECT_NE(nullptr, str);
    EXPECT_EQ("str1", *str);

    obj = std::string("str2");
    EXPECT_EQ("str2", *str);

    obj = 5;
    double* n = obj.get_if<double>();
    EXPECT_NE(nullptr, n);
    EXPECT_FLOAT_EQ(5, *n);

    obj = Foo(2);
    Foo* foo = obj.get_if<Foo>();
    EXPECT_NE(nullptr, foo);
    EXPECT_EQ(2, foo->a);

    DynamicTable* table = obj.get_if<DynamicTable>();  // TODO MH: Add test for this when
                                                       // DynamicTable uses DynamicObject
  }

  Foo my_foo(1);
  obj = &my_foo;
  Foo* foo = obj.get_if<Foo>();
  EXPECT_NE(nullptr, foo);
  EXPECT_EQ(1, foo->a);

  my_foo.a = 2;
  EXPECT_EQ(2, foo->a);

  obj = my_foo;
  EXPECT_EQ(2, foo->a);

  Foo* foo2 = obj.get_if<Foo>();
  my_foo.a = 1;
  EXPECT_EQ(2, foo2->a);
}

scr::DynamicObject hmm() { return Foo(1337); }

TEST(DynamicObject, Userdata) {
  std::lock_guard lock_guard(test_lock);
  scr::expose_type<Foo>("Foo", scr::Constructors<Foo(), Foo(int)>(), "a", &Foo::a);
  Script script("", R"(
    local cpp = require("cpp")
    function func(obj, obj2)      
      obj2.a = obj.a
      local foo = cpp.Foo.new(3)
      return foo
    end

    function func2(foo)   
      return cpp.hmm()
    end
)");
  std::function<DynamicObject::Userdata(DynamicObject::Userdata&, DynamicObject::Userdata&)> func =
      script.return_value()["func"];

  Foo foo1 = {1};
  Foo foo2 = {2};

  DynamicObject::Userdata obj = DynamicObject::Userdata::create(foo1);
  DynamicObject::Userdata obj2 = DynamicObject::Userdata::create(foo2);
  DynamicObject::Userdata obj3 = func(obj, obj2);

  Foo* foo3 = (Foo*)obj3.data;

  EXPECT_EQ(1, foo1.a);
  EXPECT_EQ(1, foo2.a);
  EXPECT_EQ(3, foo3->a);

  scr::expose_function("hmm", hmm);
  std::function<Foo(DynamicObject::Userdata)> func2 = script.return_value()["func2"];
  Foo foo4 = func2(obj);

  EXPECT_EQ(1337, foo4.a);
}