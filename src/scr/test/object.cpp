#include "scr/constructors.hpp"  // for Constructors
#include "test_lock.hpp"         // for test_lock
#include <scr/detail/state.hpp>  // for State
#include <scr/object.hpp>        // for Object
#include <scr/scr.tcc>           // for expose_type
#include <scr/script.hpp>        // for Script
using namespace scr;
using namespace detail;

namespace {
struct Foo {
  Foo() : a(1001) {}
  Foo(int i) : a(i) {}

  int get_a() const { return a; }
  void set_a(int i) { a = i; }

  int a = 1000;
};
}  // namespace

TEST(Object, Constructors) {
  expose_type<Foo>("Foo", Constructors<Foo(), Foo(int)>(), "a", &Foo::a, "get_a", &Foo::get_a,
                   "set_a", &Foo::set_a);

  sol::state& lua = State::get();

  lua.script(R"(
    a1 = cpp.Foo.new()
    a2 = cpp.Foo.new(3)
)");

  Foo a1 = lua["a1"];
  Foo* a2 = lua["a2"];

  EXPECT_EQ(1001, a1.a);
  EXPECT_EQ(3, a2->a);
}

TEST(Object, Members) {
  expose_type<Foo>("Foo", Constructors<Foo(), Foo(int)>(), "a", &Foo::a, "get_a", &Foo::get_a,
                   "set_a", &Foo::set_a);
  sol::state& lua = State::get();

  lua.script(R"(
    a1 = cpp.Foo.new()
    a1.a = 1337
)");

  Foo* a1 = lua["a1"];
  EXPECT_EQ(1337, a1->a);

  a1->a = 7331;

  lua.safe_script("a = a1.a");
  int a2 = lua["a"];
  EXPECT_EQ(7331, a2);
}

TEST(Object, Methods) {
  expose_type<Foo>("Foo", Constructors<Foo(), Foo(int)>(), "a", &Foo::a, "get_a", &Foo::get_a,
                   "set_a", &Foo::set_a);
  sol::state& lua = State::get();

  lua.script(R"(
    a1 = cpp.Foo.new()
    a1:set_a(1337)
)");

  Foo* a1 = lua["a1"];
  EXPECT_EQ(1337, a1->a);

  a1->set_a(7331);

  lua.safe_script("a = a1.a");
  int a2 = lua["a"];
  EXPECT_EQ(7331, a2);
}

TEST(Object, Assign) {
  Object obj("obj");

  sol::state& lua = State::get();

  int i = obj;
  EXPECT_EQ(0, i);

  lua.safe_script("obj = \"hello\"");

  std::string str = obj;
  EXPECT_EQ("hello", str);
}

TEST(Object, AssignPtr) {
  expose_type<Foo>("Foo", Constructors<Foo(), Foo(int)>(), "a", &Foo::a, "get_a", &Foo::get_a,
                   "set_a", &Foo::set_a);
  Object obj("obj");

  sol::state& lua = State::get();

  Foo* i = obj;
  EXPECT_EQ(nullptr, i);

  lua.safe_script("obj = cpp.Foo.new(5)");
  i = obj;
  EXPECT_NE(nullptr, i);
  EXPECT_EQ(5, i->a);

  i->a = 3;

  lua.safe_script("obj2 = obj");
  Object obj2("obj2");

  Foo* j = obj2;
  EXPECT_NE(nullptr, j);
  EXPECT_EQ(3, j->a);
  EXPECT_EQ(i, j);
}

TEST(Object, Iterate) {
  std::lock_guard lock_guard(test_lock);
  sol::state& lua = State::get();
  Script script("", R"(
entity = {
  Position = {
    x = 1,
    y = 5,
  },
  
  Force = {
    n = 10,
  },

}
)");

  Object entity = script.return_value()["entity"];

  EXPECT_TRUE(entity.has_children());

  size_t i = 0;
  for (std::pair<std::variant<std::string, size_t>, Object> comp : entity) {
    std::string* key = std::get_if<std::string>(&comp.first);
    EXPECT_NE(nullptr, key);

    if (!key) {
      continue;
    }

    EXPECT_TRUE(entity[*key].get<sol::table>().valid());
    EXPECT_TRUE(comp.second.has_children());

    for (std::pair<std::variant<std::string, size_t>, Object> val : comp.second) {
      std::string* sub_key = std::get_if<std::string>(&val.first);
      EXPECT_NE(nullptr, sub_key);

      if (!sub_key) {
        continue;
      }
      EXPECT_TRUE(entity[*key][*sub_key].get<sol::object>().valid());
    }
    i++;
  }
  EXPECT_EQ(2, i);
}

TEST(Object, BracketOp) {
  sol::state& lua = State::get();
  lua.safe_script(R"(
entity = {
  2,

  Position = {
    x = 1,
    y = 5,
  },
  
  Force = {
    n = 10,
  },

  "hi",
}
)");

  Object entity("entity");
  int x = entity["Position"]["x"];
  int y = entity["Position"]["y"];
  int n = entity["Force"]["n"];
  int bogus = entity["a"]["b"]["c"]["d"];

  int idx0 = entity[0];
  std::string idx1 = entity[1];

  EXPECT_EQ(1, x);
  EXPECT_EQ(5, y);
  EXPECT_EQ(10, n);
  EXPECT_EQ(0, bogus);

  EXPECT_EQ(2, idx0);
  EXPECT_EQ("hi", idx1);
}