#pragma once
#ifndef SCR_TEST_LOCK_HPP
#define SCR_TEST_LOCK_HPP

#include <ext/ext.hpp>

namespace scr {
static std::mutex test_lock;
}

#endif