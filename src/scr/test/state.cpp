#include "scr/detail/state.hpp"  // for State

#include "scr/object.hpp"         // for Object
#include "test_lock.hpp"          // for test_lock
#include <scr/dynamic_table.hpp>  // for DynamicTable
#include <scr/script.hpp>         // for Script
using namespace scr;
using namespace detail;

TEST(State, MultiThreaded) {
  sol::state& lua = State::get();

  volatile bool do_prepare = false;
  volatile bool do_start = false;
  std::atomic<int> num_ready = 0;

  std::vector<std::unique_ptr<std::thread>> threads;
  Object obj("obj");
  for (size_t i = 0; i < 3; i++) {
    threads.emplace_back(new std::thread(
        [&](int thread_id) {
          while (!do_prepare) {
          }

          sol::state& lua = State::get();

          lua.safe_script(std::string("obj = {child = ") + std::to_string(thread_id) +
                          std::string("}"));
          num_ready++;

          while (!do_start) {
          }

          EXPECT_EQ(thread_id, obj["child"].get<int>());
        },
        i));
  }

  do_prepare = true;
  while (num_ready < 3) {
  }
  do_start = true;

  for (const std::unique_ptr<std::thread>& thread : threads) {
    thread->join();
  }
}