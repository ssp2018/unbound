#include <scr/detail/state.hpp>  // for State
#include <scr/object.hpp>        // for Object
using namespace scr;
using namespace detail;
TEST(Function, TooFewArgs) {
  State::get().script(R"(
        func = function(str) return str end
	)");

  struct Foo {
    int a = 1;
  };

  struct Bar {
    int b = 0;
  };

  std::function<Foo()> func = Object("func");

  Foo foo = func();
  EXPECT_EQ(1, foo.a);
}

TEST(Function, TooManyArgs) {
  State::get().script(R"(
        func = function(str) return str end
	)");

  struct Foo {
    int a = 1;
  };

  struct Bar {
    int b = 0;
  };

  std::function<Foo(Bar, Bar, Bar)> func = Object("func");

  Foo foo = func(Bar(), Bar(), Bar());
  EXPECT_EQ(0, foo.a);
}

TEST(Function, InvalidReturn) {
  State::get().script(R"(
        func = function(str) end
	)");

  struct Foo {
    int a = 1;
  };

  struct Bar {
    int b = 0;
  };

  std::function<Foo(Bar, Bar, Bar)> func = Object("func");

  Foo foo = func(Bar(), Bar(), Bar());
  EXPECT_EQ(1, foo.a);
}
