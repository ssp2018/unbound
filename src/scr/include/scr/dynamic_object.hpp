#pragma once

#ifndef SCR_DYNAMIC_OBJECT_HPP
#define SCR_DYNAMIC_OBJECT_HPP

#include <bse/reflect.hpp>
#include <scr/object.hpp>
namespace scr {
class DynamicObject;
}

namespace sol {
namespace stack {
struct record;
}
}  // namespace sol
namespace sol {
template <typename T>
struct lua_size;
}
namespace sol {
template <typename T>
struct lua_type_of;
}

namespace scr {
namespace detail {
void get(lua_State* L, int index, sol::stack::record& tracking, scr::DynamicObject& out);
int push(lua_State* L, const scr::DynamicObject& things);
template <typename Handler>
bool check_dynamic_object(lua_State* L, int index, Handler&& handler, sol::stack::record& tracking);
}  // namespace detail

// Provides an interface to a mutable script object
class DynamicObject {
 public:
  // Holds a pointer to any C++ type.
  struct Userdata {
    Userdata() = default;
    virtual ~Userdata() = default;
    template <typename T>
    void set(const T& other);

    template <typename T>
    static Userdata create(const T& other);
    std::string type_name;
    void* data = nullptr;
    sol::table (*object_creator)(lua_State*, void*) = nullptr;
  };

 private:
  // Owns a pointer to any C++ type.
  struct OwningUserdata : public Userdata {
    OwningUserdata() = default;
    template <typename T>
    void set(const T& other);

    template <typename T>
    static OwningUserdata create(const T& other);

    OwningUserdata(const OwningUserdata& other);
    OwningUserdata& operator=(const OwningUserdata& other);
    virtual ~OwningUserdata();

    void (*destructor)(void* data);
    void* (*copier)(void* data);
  };

  using Data = std::variant<std::nullptr_t, double, std::string, Userdata, OwningUserdata, bool>;
  template <typename T>
  struct IsInitializerList : public std::false_type {
    virtual ~IsInitializerList() = default;
  };

  template <typename T>
  struct IsInitializerList<std::initializer_list<T>> : public std::true_type {};

  template <typename T>
  static constexpr bool is_initializer_list_v = IsInitializerList<T>::value;

  template <typename T>
  static constexpr bool is_data_type_impl_v =
      std::is_same_v<std::nullptr_t, T> || std::is_same_v<double, T> ||
      std::is_same_v<std::string, T> || std::is_same_v<std::string_view, T> ||
      std::is_same_v<Userdata, T> || std::is_same_v<OwningUserdata, T> || std::is_same_v<bool, T>;

  template <typename T>
  static constexpr bool is_data_type_v =
      is_data_type_impl_v<std::remove_reference_t<std::remove_const_t<T>>>;

 public:
  template <typename T>
  DynamicObject(const T& other);
  DynamicObject(const DynamicObject&) = default;
  DynamicObject();

  // Check if object is convertiple to T.
  template <typename T>
  bool is() const;

  // Check if object is an explicit type.
  bool is_number() const;
  bool is_string() const;
  bool is_boolean() const;
  bool is_table() const;
  bool is_userdata() const;

  // Serialize to string.
  std::string to_string() const;

  // Assign any type to object.
  template <typename T>
  DynamicObject& operator=(const T& other);

  // Set explicitly as userdata.
  template <typename T>
  void set_userdata(const T& other);

  // Get pointer to object. Return nullptr
  // if stored object is not same type.
  template <typename T>
  const T* get_if() const;

  // Get pointer to object. Return nullptr
  // if stored object is not same type.
  template <typename T>
  T* get_if();

  // Get copy of object. Returns a conversion
  // if possible, else a default-constructed instance.
  template <typename T = void, typename = typename std::enable_if_t<!std::is_pointer_v<T> &&
                                                                    !is_initializer_list_v<T>>>
  operator T() const;
  template <typename T = void, typename = typename std::enable_if_t<!std::is_pointer_v<T> &&
                                                                    !is_initializer_list_v<T>>>
  T get() const;

 private:
  // Get pointer to userdata
  template <typename T>
  const T* get_userdata() const;

  // Helper function for get_if
  template <typename T>
  const T* get_if_impl() const;

  // Assign object to this
  template <typename T>
  void set(const T& other);

  // Get copy of object if convertible.
  template <typename T>
  std::optional<T> get_impl() const;

  Data m_data;

  friend void ::scr::detail::get(lua_State* L, int index, sol::stack::record& tracking,
                                 scr::DynamicObject& out);
  friend int ::scr::detail::push(lua_State* L, const scr::DynamicObject& things);

  template <typename Handler>
  friend bool ::scr::detail::check_dynamic_object(lua_State* L, int index, Handler&& handler,
                                                  sol::stack::record& tracking);
};

///////////////////////////////////////////////////////////////////////////////
// impl ///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

template <typename T>
DynamicObject::DynamicObject(const T& other) {
  set(other);
}

template <typename T>
DynamicObject& DynamicObject::operator=(const T& other) {
  set(other);
  return *this;
}

template <typename T>
const T* DynamicObject::get_if_impl() const {
  if constexpr (std::is_class_v<T> && !std::is_same_v<T, std::string> &&
                !is_initializer_list_v<T>) {
    return get_userdata<T>();
  } else {
    return std::get_if<T>(&m_data);
  }
}

template <typename T>
const T* DynamicObject::get_userdata() const {
  if (const Userdata* data = std::get_if<Userdata>(&m_data)) {
    return (const T*)data->data;
  } else if (const OwningUserdata* data = std::get_if<OwningUserdata>(&m_data)) {
    return (const T*)data->data;
  }
  return nullptr;
}

template <typename T>
const T* DynamicObject::get_if() const {
  return get_if_impl<T>();
}

template <typename T>
T* DynamicObject::get_if() {
  return (T*)get_if_impl<T>();
}

template <typename T>
void DynamicObject::set(const T& other) {
  static_assert(std::is_base_of_v<Userdata, Userdata>);
  if constexpr (std::is_same_v<T, Object>) {
    m_data = OwningUserdata::create(other);
  } else if constexpr (std::is_base_of_v<Userdata, T>) {
    m_data = other;
  } else if constexpr (std::is_same_v<T, bool>) {
    m_data = other;
  } else if constexpr (std::is_same_v<T, std::nullptr_t>) {
    m_data = other;
  } else if constexpr (std::is_convertible_v<T, std::string>) {
    m_data = std::string(other);
  } else if constexpr (std::is_same_v<T, std::nullptr_t>) {
    m_data = nullptr;
  } else if constexpr (std::is_pointer_v<T>) {
    m_data = Userdata::create(*other);
  } else if constexpr (std::is_arithmetic_v<T>) {
    m_data = double(other);
  } else if constexpr (std::is_class_v<T>) {
    m_data = OwningUserdata::create(other);
  } else {
    static_assert(std::is_class_v<T>, "Invalid type in assignment to DynamicObject.");
  }
}

template <typename T>
void DynamicObject::set_userdata(const T& other) {
  m_data = OwningUserdata(other);
}

template <typename T>
std::optional<T> DynamicObject::get_impl() const {
  static_assert(std::is_default_constructible_v<T>,
                "Can only retrieve copy of default-constructible types.");
  std::optional<T> t;
  if (const double* data = std::get_if<double>(&m_data)) {
    if constexpr (std::is_convertible_v<double, T>) {
      t = *data;
    }
  } else if (const std::string* data = std::get_if<std::string>(&m_data)) {
    if constexpr (std::is_convertible_v<std::string, T>) {
      t = *data;
    }
  } else if (const bool* data = std::get_if<bool>(&m_data)) {
    if constexpr (std::is_convertible_v<bool, T>) {
      t = *data;
    }
  } else if (const Userdata* data = std::get_if<Userdata>(&m_data)) {
    if constexpr (std::is_convertible_v<Userdata, T>) {
      t = *data;
    } else if (data->data) {
      if (data->type_name == "Object") {
        t = ((Object*)data->data)->get<T>();
      } else if constexpr (!is_data_type_v<T>) {
        t = *(T*)data->data;
      }
    }
  } else if (const OwningUserdata* data = std::get_if<OwningUserdata>(&m_data)) {
    static_assert(std::is_convertible_v<OwningUserdata, Userdata>);
    if constexpr (std::is_convertible_v<OwningUserdata, T>) {
      t = *data;
    } else if (data->data) {
      if (data->type_name == "Object") {
        t = ((Object*)data->data)->get<T>();
      } else if constexpr (!is_data_type_v<T>) {
        t = *(T*)data->data;
      }
    }
  }

  return t;
}

template <typename T, typename>
DynamicObject::operator T() const {
  return get_impl<T>().value_or(T());
}

template <typename T, typename>
T DynamicObject::get() const {
  return get_impl<T>().value_or(T());
}

template <typename T>
bool DynamicObject::is() const {
  return get_impl<T>().has_value();
}

template <typename T>
void generic_destructor(void* data) {
  delete (T*)data;
}

template <typename T>
void* generic_copier(void* data) {
  return new T(*(T*)data);
}

template <typename T>
sol::table value_creator(lua_State* state, void* data) {
  if constexpr (std::is_copy_constructible_v<T> && std::is_copy_assignable_v<T>) {
    return sol::make_object(state, *(T*)data);
  } else {
    ASSERT(false) << "Attempting to create value of non-copyable object.";
    return sol::nil;
  }
}

template <typename T>
sol::table pointer_creator(lua_State* state, void* data) {
  return sol::make_object(state, (T*)data);
}

template <typename T>
void DynamicObject::Userdata::set(const T& other) {
  type_name =
      bse::meta<T>::is_defined ? bse::meta<T>::base_name : detail::State::get_type_name<T>();
  data = (void*)&other;
  object_creator = pointer_creator<std::remove_pointer_t<T>>;

  if constexpr (std::is_same_v<T, Userdata> || std::is_same_v<T, OwningUserdata>) {
    type_name = other.type_name;
    data = other.data;
  } else if constexpr (std::is_same_v<T, DynamicObject>) {
    Userdata user_data(other.operator Userdata());
    type_name = user_data.type_name;
    data = user_data.data;
  }
}

template <typename T>
void DynamicObject::OwningUserdata::set(const T& other) {
  Userdata::set(other);
  destructor = generic_destructor<T>;
  copier = generic_copier<T>;

  if (data) {
    data = copier(data);
  }
  object_creator = value_creator<T>;
}

template <typename T>
DynamicObject::Userdata DynamicObject::Userdata::create(const T& other) {
  Userdata d;
  d.set(other);

  return d;
}
template <typename T>
DynamicObject::OwningUserdata DynamicObject::OwningUserdata::create(const T& other) {
  OwningUserdata d;
  d.set(other);
  return d;
}

namespace detail {
template <typename Handler>
bool check_dynamic_object(lua_State* L, int index, Handler&& handler,
                          sol::stack::record& tracking) {
  return ::sol::stack::checker<decltype(DynamicObject::m_data)>::check(L, index, handler, tracking);
}
}  // namespace detail
}  // namespace scr

namespace sol {

template <>
struct lua_size<scr::DynamicObject> : lua_size<sol::object> {};

template <>
struct lua_type_of<scr::DynamicObject> : lua_type_of<sol::object> {};

namespace stack {

template <>
struct checker<scr::DynamicObject> {
  template <typename Handler>
  static bool check(lua_State* L, int index, Handler&& handler, record& tracking) {
    return scr::detail::check_dynamic_object(L, index, std::forward<Handler>(handler), tracking);
  }
};

template <>
struct getter<scr::DynamicObject> {
  static scr::DynamicObject get(lua_State* L, int index, record& tracking);
};

template <>
struct pusher<scr::DynamicObject> {
  static int push(lua_State* L, const scr::DynamicObject& things);
};
}  // namespace stack

}  // namespace sol

namespace sol {

template <>
struct lua_size<scr::DynamicObject::Userdata> : lua_size<sol::table> {};

template <>
struct lua_type_of<scr::DynamicObject::Userdata> : lua_type_of<sol::table> {};

namespace stack {

template <>
struct checker<scr::DynamicObject::Userdata> {
  template <typename Handler>
  static bool check(lua_State* L, int index, Handler&& handler, record& tracking) {
    return checker<sol::table>::check(L, index, handler, tracking);
  }
};

template <>
struct getter<scr::DynamicObject::Userdata> {
  static scr::DynamicObject::Userdata get(lua_State* L, int index, record& tracking);
};

template <>
struct pusher<scr::DynamicObject::Userdata> {
  static int push(lua_State* L, const scr::DynamicObject::Userdata& things);
};

}  // namespace stack

}  // namespace sol

#endif
