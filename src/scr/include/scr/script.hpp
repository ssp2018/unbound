#pragma once
#ifndef SCR_SCRIPT_HPP
#define SCR_SCRIPT_HPP

#include <bse/asset.hpp>
#include <bse/file_path.hpp>
#include <bse/result.hpp>
#include <scr/detail/token.hpp>

namespace scr {

class Object;

// A loaded and executed script.
class Script {
 public:
  Script(const std::string &name, const std::string &script);

  Script(Script &&other);
  Script &operator=(Script &&other);

  Script(const Script &other) = delete;
  Script &operator=(const Script &other) = delete;

  ~Script();

  // Load script from file.
  static bse::Result<Script> load(bse::FilePath path);

  // Get return value from script, if any.
  Object return_value() const;

  // Attempt to find global object by script pointer
  // Returns null object if not found.
  static Object find_global_object(const void *script_pointer);

 private:
  // get object from this script
  sol::object get_object() const;

  detail::Token m_token;
};

}  // namespace scr
extern template class bse::Asset<scr::Script>;

#endif