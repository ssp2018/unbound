#pragma once

#ifndef SCR_CONSTRUCTORS_HPP
#define SCR_CONSTRUCTORS_HPP

namespace scr {

// Helper for type exposition (see scr::expose_type).
template <typename... T>
struct Constructors {};
}  // namespace scr

#endif
