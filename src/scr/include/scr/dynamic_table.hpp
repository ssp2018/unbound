#pragma once

#ifndef SCR_DYNAMIC_TABLE_HPP
#define SCR_DYNAMIC_TABLE_HPP

#include <bse/serialize.hpp>
#include <scr/dynamic_object.hpp>

namespace scr {
namespace detail {
class State;
}
class Object;
class DynamicTable;
}  // namespace scr
namespace sol {
class state;
}

namespace sol {
template <>
struct is_container<scr::DynamicTable> : std::false_type {};
}  // namespace sol

namespace scr {

// Runtime script table stored in C++
struct DynamicTable {
 public:
  DynamicTable();
  DynamicTable(const scr::Object& object);

  // Get child by string key. Create if none exists.
  DynamicObject& operator[](const std::string& key);

  // Get child by string key. Return null if none exists.
  const DynamicObject* get_if(const std::string& key) const;
  DynamicObject* get_if(const std::string& key);

  // Serialize to string
  std::string to_string() const;

  // Deserialize from string
  void from_string(const std::string& str);

  // get size
  size_t size() const;

  // clear table
  void clear();

  std::unordered_map<std::variant<std::string, size_t>, DynamicObject>::iterator begin();
  std::unordered_map<std::variant<std::string, size_t>, DynamicObject>::iterator end();

  std::unordered_map<std::variant<std::string, size_t>, DynamicObject>::const_iterator begin()
      const;
  std::unordered_map<std::variant<std::string, size_t>, DynamicObject>::const_iterator end() const;

  CLASS_SERFUN((DynamicTable)) {
    SERIALIZE(self.to_string());  //
  }
  CLASS_DESERFUN((DynamicTable)) {
    std::string str;
    DESERIALIZE(str);
    self.from_string(str);
  }

 private:
  friend class ::scr::detail::State;
  // expose this type to script
  static void expose(sol::table& lua);

  // script get overload
  DynamicObject index(const sol::object& key);

  // script set overload
  void new_index(const sol::object& key, const DynamicObject& value);

  std::unordered_map<std::variant<std::string, size_t>, DynamicObject> m_entries;
};

}  // namespace scr

#endif