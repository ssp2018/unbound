#pragma once
#ifndef SCR_STATE_HPP
#define SCR_STATE_HPP

#include <bse/asset.hpp>
#include <ext/ext.hpp>
#include <scr/constructors.hpp>
#include <scr/detail/token.hpp>

namespace scr {
class DynamicObject;
}
namespace scr::detail {
// Lua state
class State {
 public:
  ~State();

  // destroy thread local vars
  static void destroy_thread();

  // destroy state variables
  static void destroy();

  // Get lua state
  static sol::state& get();

  // Register script
  static Token register_script(const std::string& name, const std::string& script);

  // Replace script
  // Unregisters the old script.
  // Useful for sneaky reload without invalidating
  // existing tokens.
  static void replace_script(Token old_script, Token new_script);

  // Unregister script
  static void unregister_script(Token script);

  // Get object from script
  static sol::object get_object(Token token);

  // Get script name
  static std::string get_name(Token token);

  // Load script into this thread
  static sol::table load_script(const std::string& script, const std::string& name);

  // Expose usertype
  template <typename TypeT, typename... ConstructorsT, typename... MembersAndMethodsT>
  static void expose_type(std::string_view name, const Constructors<ConstructorsT...>& constructors,
                          const MembersAndMethodsT&... members_and_methods);

  template <typename FunctionT>
  static void expose_function(std::string_view name, const FunctionT& func);

  // Attach a meta function to an exposed type.
  template <typename FunctionT>
  static void expose_meta_function(std::string_view type_name, sol::meta_function key,
                                   FunctionT&& function);

  // Check if State is initialized.
  static bool is_valid();

  // Create script owning userdata object
  static sol::table create_userdata_value(const std::string& type_name, void* data);

  // Create script userdata object
  static sol::table create_userdata_pointer(const std::string& type_name, void* data);

  // Refers to the global Lua object
  static const Token GLOBAL_TOKEN;

  // Get type name of exposed type.
  template <typename T>
  static std::string get_type_name();

 private:
  State();

  // Initialize state object on this thread.
  static void initialize_thread();

  // Update state for any changes.
  void update();

  // Error printer
  static void print_error(const std::string& what);

  // Require Lua function
  static sol::object require(const std::string& path);

  // Reserve a token
  static Token grab_token();

  // Print Lua function
  static void print(const sol::object& object, const sol::object& info);

  // Print error object
  static void print_error_object(const sol::object& object, const sol::object& traceback);

  // Create pairs overload
  void create_readonly_make_pairs();

  // Helper for common exposition operations
  template <typename TypeT>
  static void expose_type_standard(const std::string& name);

  // Builtin type exposer
  template <typename T>
  static void expose_builtin_type(sol::table& lua, const std::string& name);

  // Internal representation of script file
  struct Script {
    Token token;
    std::string name;
    std::string script;
  };

  // Internal representation of script object
  struct Object {
    Token token;
    sol::object object;
  };

  // Stored function to expose a type.
  using TypeExposer = std::function<void(State& state, sol::table&)>;

  static constexpr size_t M_MAX_SCRIPT_COUNT = 512;

  sol::state m_sol_state;
  size_t m_num_exposed_types;
  std::array<Object, M_MAX_SCRIPT_COUNT> m_objects;

  static std::vector<TypeExposer>* m_type_exposers;
  static std::unordered_map<std::string, sol::table (*)(sol::state&, void*)>* m_value_creators;
  static std::unordered_map<std::string, sol::table (*)(sol::state&, void*)>* m_pointer_creators;
  static std::unordered_map<std::string, std::vector<std::string>>* m_type_members;
  static std::unordered_map<void (*)(), std::string>* m_type_names;
  static std::mutex m_script_lock;
  static std::array<std::unique_ptr<Script>, M_MAX_SCRIPT_COUNT> m_scripts;
  static std::atomic<uint32_t> m_current_token_id;
  static thread_local bool m_is_valid;
  static thread_local State* m_state;
};

///////////////////////////////////////////////////////////////////////////////
// impl ///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

template <typename T>
sol::table value_creator(sol::state& state, void* data) {
  if constexpr (std::is_copy_constructible_v<T> && std::is_copy_assignable_v<T>) {
    return sol::make_object(state, *(T*)data);
  } else {
    ASSERT(false) << "Attempting to create value of non-copyable object.";
    return sol::nil;
  }
}

template <typename T>
sol::table pointer_creator(sol::state& state, void* data) {
  return sol::make_object(state, (T*)data);
}

template <typename NameT, typename MemberT, typename... TailT>
void get_member_names(std::vector<std::string>& names, const NameT& name, const MemberT&,
                      const TailT&... tail) {
  names.push_back(std::string(name));
  if constexpr (sizeof...(TailT) > 1) {
    get_member_names(names, tail...);
  }
}

template <typename... T>
std::vector<std::string> get_member_names(const T&... members) {
  std::vector<std::string> names;

  if constexpr (sizeof...(T) > 1) {
    get_member_names(names, members...);
  }
  return names;
}

template <typename TypeT>
void State::expose_type_standard(const std::string& name) {
  if (!m_type_exposers) {
    m_type_exposers = new std::vector<TypeExposer>();
    m_value_creators = new std::unordered_map<std::string, sol::table (*)(sol::state&, void*)>();
    m_pointer_creators = new std::unordered_map<std::string, sol::table (*)(sol::state&, void*)>();
    m_type_members = new std::unordered_map<std::string, std::vector<std::string>>();
    m_type_names = new std::unordered_map<void (*)(), std::string>();
  }
  (*m_value_creators)[name] = value_creator<TypeT>;
  if constexpr (std::is_copy_constructible_v<TypeT> && std::is_copy_assignable_v<TypeT>) {
    (*m_pointer_creators)["std::vector<" + name + ">"] = pointer_creator<std::vector<TypeT>>;
  }
  (*m_pointer_creators)[name] = pointer_creator<TypeT>;
  (*m_type_names)[(void (*)())value_creator<TypeT>] = name;
  (*m_type_members)[name] = {};
}

template <typename UserTypeT, typename MemberT, typename... TailT>
void set_ut_helper(UserTypeT& ut, const std::string_view& name, const MemberT& member,
                   const TailT&... tail) {
  ut.set(name, member);

  if constexpr (sizeof...(TailT) > 1) {
    set_ut_helper(ut, tail...);
  }
}

template <typename UserTypeT, typename... MembersAndMethodsT>
void set_ut(UserTypeT& ut, const MembersAndMethodsT&... members) {
  if constexpr (sizeof...(MembersAndMethodsT) > 1) {
    set_ut_helper(ut, members...);
  }
}

template <typename TypeT, typename... ConstructorsT, typename... MembersAndMethodsT>
void State::expose_type(std::string_view name, const Constructors<ConstructorsT...>& constructors,
                        const MembersAndMethodsT&... members_and_methods) {
  std::string name_str = name.data();
  expose_type_standard<TypeT>(name_str);
  m_type_exposers->push_back([name_str, members_and_methods...](State& state, sol::table& lua) {
    // lua.new_usertype<TypeT>(name_str, sol::constructors<ConstructorsT...>(),
    //                         members_and_methods...);

    auto ut = lua.create_simple_usertype<TypeT>(sol::constructors<ConstructorsT...>());
    set_ut(ut, members_and_methods...);
    lua.set_usertype(name_str, ut);
  });

  std::vector<std::string> names = get_member_names(members_and_methods...);
  (*m_type_members)[name_str] = names;
}

template <typename FunctionT>
void State::expose_function(std::string_view name, const FunctionT& func) {
  if (!m_type_exposers) {
    m_type_exposers = new std::vector<TypeExposer>();
    m_value_creators = new std::unordered_map<std::string, sol::table (*)(sol::state&, void*)>();
    m_pointer_creators = new std::unordered_map<std::string, sol::table (*)(sol::state&, void*)>();
    m_type_members = new std::unordered_map<std::string, std::vector<std::string>>();
    m_type_names = new std::unordered_map<void (*)(), std::string>();
  }

  std::string name_str = name.data();
  m_type_exposers->push_back([name_str, func](State& state, sol::table& lua) {
    // lua.new_usertype<TypeT>(name_str, sol::constructors<ConstructorsT...>(),
    //                         members_and_methods...);
    lua.set_function(name_str, func);
  });
}

template <typename FunctionT>
void State::expose_meta_function(std::string_view type_name, sol::meta_function key,
                                 FunctionT&& function) {
  std::string name_str = std::string(type_name);
  m_type_exposers->push_back(
      [name_str, key, function](State& state, sol::table& lua) { lua[name_str][key] = function; });
}

template <typename T>
std::string State::get_type_name() {
  static const std::string unexposed = "#UNEXPOSED_SCRIPT_TYPE";
  std::string name;
  if (!m_type_names) {
    name = unexposed;
    return name;
  }

  auto it = m_type_names->find((void (*)())value_creator<T>);
  if (it == m_type_names->end()) {
    name = unexposed;
    return name;
  }

  name = it->second;
  return name;
}

}  // namespace scr::detail

#endif