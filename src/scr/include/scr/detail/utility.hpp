#pragma once
#ifndef SCR_UTILITY_HPP
#define SCR_UTILITY_HPP

#include <ext/ext.hpp>
#include <scr/detail/config.hpp>

namespace scr::detail {

// Get table size directly from sol
size_t get_raw_size(const sol::table& table);

// Get table size via pairs overload
size_t get_size(const sol::table& table);

// Set object as read only
void set_readonly(sol::object& object, sol::state& lua);

// Invoke func on table children
template <typename FuncT>
void for_each(sol::table& table, FuncT&& func);

// Invoke func on table children
template <typename FuncT>
void for_each(const sol::table& table, FuncT&& func);

// Write object to string
std::string to_string(const sol::object& object, bool do_sort = false);

// Attempt to find global path to object by pointer
std::optional<std::vector<std::string>> find_path(const void* ptr, const sol::table& root);

///////////////////////////////////////////////////////////////////////////////
// impl ///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

template <typename FuncT>
void for_each(sol::table& table, FuncT&& func) {
  if constexpr (is_readonly_enforced) {
    if (table["__unbound_readonly"].valid()) {
      using Nexter = std::function<std::pair<sol::object, sol::object>()>;
      using Pairs = std::function<Nexter()>;
      sol::object pairs_object = table[sol::meta_function::pairs];
      Pairs pairs = pairs_object.as<Pairs>();
      Nexter nexter = pairs();

      for (std::pair<sol::object, sol::object> pair = nexter(); pair.first != sol::nil;
           pair = nexter()) {
        func(pair.first, pair.second);
      }
    } else {
      table.for_each(std::forward<FuncT>(func));
    }

  } else {
    table.for_each(std::forward<FuncT>(func));
  }
}

template <typename FuncT>
void for_each(const sol::table& table, FuncT&& func) {
  if constexpr (is_readonly_enforced) {
    if (table["__unbound_readonly"].valid()) {
      using Nexter = std::function<std::pair<sol::object, sol::object>()>;
      using Pairs = std::function<Nexter()>;
      sol::object pairs_object = table[sol::meta_function::pairs];
      Pairs pairs = pairs_object.as<Pairs>();
      Nexter nexter = pairs();

      for (std::pair<sol::object, sol::object> pair = nexter(); pair.first != sol::nil;
           pair = nexter()) {
        func(pair.first, pair.second);
      }
    } else {
      table.for_each(std::forward<FuncT>(func));
    }

  } else {
    table.for_each(std::forward<FuncT>(func));
  }
}

}  // namespace scr::detail

#endif