#pragma once
#ifndef SCR_TO_STD_FUNC_HPP
#define SCR_TO_STD_FUNC_HPP

#include <ext/ext.hpp>

namespace scr::detail {

template <typename T, typename TT = void>
struct to_std_func_helper;

template <typename ReturnT, typename ClassT, typename... ArgsT>
struct to_std_func_helper<ReturnT (ClassT::*)(ArgsT...) const> {
  using type = std::function<ReturnT(ArgsT...)>;
};

template <typename ReturnT, typename... ArgsT>
std::function<ReturnT(ArgsT...)> to_std_func(ReturnT (*func)(ArgsT...)) {
  return func;
}

template <typename ReturnT, typename ClassT, typename... ArgsT>
std::function<ReturnT(ClassT*, ArgsT...)> to_std_func(ReturnT (ClassT::*func)(ArgsT...)) {
  return std::function<ReturnT(ClassT*, ArgsT...)>(
      [func](ClassT* self, ArgsT... args) { return self->*func(args...); });
}

template <typename FunctionT>
auto to_std_func(const FunctionT& func)
    -> decltype(&FunctionT::operator(),
                typename to_std_func_helper<decltype(&FunctionT::operator())>::type(func)) {
  return typename to_std_func_helper<decltype(&FunctionT::operator())>::type(func);
}

template <class T>
auto is_std_function_convertible_helper2(const T& t, int)
    -> decltype(to_std_func(t), std::true_type()) {
  return std::true_type();
}

template <class T>
std::false_type is_std_function_convertible_helper2(const T& t, ...) {
  return std::false_type();
}

template <class T>
auto is_std_function_convertible_helper(const T& t)
    -> decltype(is_std_function_convertible_helper2(t, 0)) {
  return is_std_function_convertible_helper2(t, 0);
}

template <typename T>
struct is_std_function_convertible
    : decltype(is_std_function_convertible_helper(std::declval<T>())) {};

template <typename T>
constexpr bool is_std_function_convertible_v = is_std_function_convertible<T>::value;

}  // namespace scr::detail

#endif
