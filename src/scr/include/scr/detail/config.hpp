#pragma once
#ifndef SCR_CONFIG_HPP
#define SCR_CONFIG_HPP

namespace scr::detail {
constexpr bool is_readonly_enforced = true;
constexpr size_t readonly_meta_size = 4;
}  // namespace scr::detail

#endif