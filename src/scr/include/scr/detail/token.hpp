#pragma once
#ifndef SCR_TOKEN_HPP
#define SCR_TOKEN_HPP

namespace scr::detail {
// Internal representation of a generic "thing"
struct Token {
  // check equality with other token
  bool operator==(const Token& other) const;

  // return true if non-null
  operator bool() const;

  uint32_t index = 0;
  uint32_t id = 0;
};

static constexpr Token NULL_TOKEN = {0, 0};

}  // namespace scr::detail

#endif
