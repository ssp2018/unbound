#include <scr/detail/state.hpp>
#include <scr/detail/to_std_func.hpp>
#include <scr/object.hpp>
#include <scr/scr.hpp>
namespace scr {

template <typename FunctionT>
void expose_function(std::string_view name, const FunctionT& func)
/*-> decltype(detail::to_std_func(func), void())*/ {
  detail::State::expose_function(name, func);
  /*detail::State::get().set_function(name, func);
  detail::State::get()["cpp"][std::string(name)] = detail::State::get()[std::string(name)];*/
}

template <typename TypeT, typename... ConstructorsT, typename... MembersAndMethodsT>
void expose_type(std::string_view name, const Constructors<ConstructorsT...>& constructors,
                 const MembersAndMethodsT&... members_and_methods) {
  detail::State::expose_type<TypeT>(name, constructors, members_and_methods...);
}

namespace detail {

template <typename T>
struct IsVector : public std::false_type {};

template <typename T>
struct IsVector<std::vector<T>> : public std::true_type {};

template <typename T>
constexpr bool is_vector_v = IsVector<T>::value;

template <typename T>
struct IsArray : std::is_array<T> {};

template <typename T, size_t N>
struct IsArray<std::array<T, N>> : std::true_type {};

template <typename T>
constexpr bool is_array_v = IsArray<T>::value;

template <typename T>
struct IsMap : public std::false_type {};

template <typename KeyT, typename ValueT>
struct IsMap<std::map<KeyT, ValueT>> : std::true_type {};

template <typename KeyT, typename ValueT>
struct IsMap<std::unordered_map<KeyT, ValueT>> : std::true_type {};

template <typename T>
constexpr bool is_map_v = IsMap<T>::value;

template <typename T>
constexpr bool is_container_v =
    bse::meta<T>::is_defined || is_vector_v<T> || is_array_v<T> || is_map_v<T>;

static_assert(!is_vector_v<float>);
static_assert(is_vector_v<std::vector<float>>);
static_assert(!is_vector_v<float>);
static_assert(is_vector_v<std::vector<float>>);

static_assert(!is_array_v<float>);
static_assert(is_array_v<std::array<float, 3>>);

static_assert(!is_map_v<float>);
static_assert(is_map_v<std::map<bool, float>>);
static_assert(is_map_v<std::unordered_map<bool, float>>);

template <typename T>
void from_table_helper(T& t, const sol::object& object) {
  using NoRefT = std::remove_reference_t<T>;

  if constexpr (std::is_member_function_pointer_v<NoRefT>) {
  } else if constexpr (is_container_v<NoRefT>) {
    if (!object.is<sol::table>()) {
      return;
    }
    const sol::table table = object;

    if constexpr (bse::meta<NoRefT>::is_defined) {
      if (!object.is<sol::table>()) {
        return;
      }
      bse::for_each_field(
          t, [&table](auto& field) { from_table_helper(field.value, table[field.name]); });
    } else if constexpr (is_array_v<NoRefT>) {
      size_t i = 1;
      sol::object obj = table[i];
      while (obj.valid() && i - 1 <= t.size()) {
        from_table_helper(t[i - 1], obj);
        i++;
        obj = table[i];
      }
    } else if constexpr (is_vector_v<NoRefT>) {
      size_t i = 1;
      sol::object obj = table[i];
      while (obj.valid()) {
        t.emplace_back();
        from_table_helper(t.back(), obj);
        i++;
        obj = table[i];
      }
    } else if constexpr (is_map_v<NoRefT>) {
      for_each(table, [&t](const sol::object& lua_key, const sol::object& lua_value) {
        if (lua_key.is<typename NoRefT::key_type>()) {
          typename NoRefT::mapped_type& value = t[lua_key.as<typename NoRefT::key_type>()];
          from_table_helper(value, lua_value);
        }
      });
    } else {
      static_assert(is_vector_v<NoRefT>, "Unsupported container type.");
    }
  } else {
    t = object.as<NoRefT>();
  }
}
}  // namespace detail

template <typename T>
T from_table(const scr::Object& table) {
  static_assert(bse::meta<T>::is_defined, "Can only convert table to reflectable types.");

  T t = T();
  const sol::table lua_table = table;
  if (!lua_table.valid()) {
    return t;
  }

  bse::for_each_field(t, [&lua_table](auto& field) {
    detail::from_table_helper(field.value, lua_table[field.name]);
  });

  return t;
}

template <typename T>
T from_table_sol(const sol::table& lua_table) {
  static_assert(bse::meta<T>::is_defined, "Can only convert table to reflectable types.");

  T t = T();
  // const sol::table lua_table = table;
  if (!lua_table.valid()) {
    return t;
  }

  bse::for_each_field(t, [&lua_table](auto& field) {
    detail::from_table_helper(field.value, lua_table[field.name]);
  });

  return t;
}

template <typename T>
void expose_setter(std::string_view type_name,
                   void (*setter)(T& t, const std::string& key, const scr::DynamicObject& object)) {
  detail::State::expose_meta_function(type_name, sol::meta_function::new_index, setter);
}

template <typename T>
void expose_setter(std::string_view type_name,
                   void (T::*setter)(const std::string& key, const scr::DynamicObject& object)) {
  detail::State::expose_meta_function(type_name, sol::meta_function::new_index, setter);
}

template <typename T>
void expose_getter(std::string_view type_name,
                   scr::DynamicObject (*getter)(T& t, const std::string& key)) {
  detail::State::expose_meta_function(type_name, sol::meta_function::index, getter);
}

template <typename T>
void expose_getter(std::string_view type_name,
                   scr::DynamicObject (T::*getter)(const std::string& key)) {
  detail::State::expose_meta_function(type_name, sol::meta_function::index, getter);
}
}  // namespace scr