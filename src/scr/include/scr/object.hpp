#pragma once
#ifndef SCR_OBJECT_HPP
#define SCR_OBJECT_HPP

#include <bse/asset.hpp>
#include <bse/serialize.hpp>
#include <ext/ext.hpp>
#include <scr/detail/state.hpp>
#include <scr/detail/to_std_func.hpp>

namespace scr {
class Object;
}

namespace sol {
template <>
struct is_container<scr::Object> : std::false_type {};
}  // namespace sol

namespace scr {

// Represents a weak handle to a Lua object.
class Object {
 public:
  class Iterator;

  Object();

  // Create C++ handle on global Lua object.
  Object(const std::string& name);

  // Create C++ handle on script Lua object.
  Object(const std::string& script_path, const std::string& name);

  // Implicitly get Lua object as TypeT.
  template <typename TypeT>
  operator TypeT() const;

  // Explicitly get Lua object as TypeT.
  // Returns default-constructed TypeT if
  // object type does not match.
  template <typename TypeT>
  TypeT get() const;

  // Returns the path to the script
  std::string get_script_path() const;

  // Returns the name of the object
  std::string get_name() const;

  // Explicitly get Lua object as TypeT.
  // Returns null_opt if object type
  // does not match.
  template <typename TypeT>
  std::optional<TypeT> get_if() const;

  // Get Lua object as string.
  std::string str() const;

  // Check if Lua object is of type TypeT.
  template <typename TypeT>
  bool is() const;

  // Get iterator to first child.
  Iterator begin() const;

  // Get iterator beyond last child.
  Iterator end() const;

  // Check if object has children.
  bool has_children() const;

  // Get child by string key.
  Object operator[](const std::string& key) const;

  // Get child by string key.
  Object operator[](const std::string_view& key) const;

  // Get child by string key.
  Object operator[](const char* key) const;

  // Get child by numeric key.
  Object operator[](int key) const;

  // Get child by numeric key.
  Object operator[](size_t key) const;

  // Check if object is null.
  bool is_null() const;

  // Used for iterating through children
  class Iterator {
   public:
    Iterator(const std::vector<std::pair<std::variant<std::string, size_t>, Object>>& elements);

    // Check equality
    bool operator!=(const Iterator& other) const;

    // Increment iterator to next element.
    Iterator& operator++();

    // Dereference iterator as key-value-pair.
    std::pair<std::variant<std::string, size_t>, Object> operator*() const;

   private:
    std::vector<std::pair<std::variant<std::string, size_t>, Object>> m_elements;
    int m_index = 0;
  };

  static const Object NULL_OBJECT;

  CLASS_SERFUN((Object)) {
    std::string script_path = scr::detail::State::get_name(m_root);
    SERIALIZE(script_path);
    if (script_path != "__global" && !script_path.empty()) {
      SERIALIZE(m_path);
    }
  }

  CLASS_DESERFUN((Object)) {
    std::string script_path;
    DESERIALIZE(script_path);
    if (script_path != "__global" && !script_path.empty()) {
      decltype(m_path) name;
      DESERIALIZE(name);

      // I make the assumptions that:
      // * name contains at least one element (and probably at most).
      // * the first element of name is a string
      if (script_path != "") {
        if (name.size() > 0) {
          self = Object{script_path, std::get<std::string>(name[0])};
        } else {
          self = Object{script_path, ""};
        }
      }
    }
  }

 private:
  friend class Script;
  friend class ::scr::detail::State;

  Object(detail::Token script);

  // create function wrapper to object
  template <typename ReturnT, typename... ArgsT>
  std::function<ReturnT(ArgsT...)> get_function(const sol::object& object,
                                                const std::function<ReturnT(ArgsT...)>& func) const;

  // get object with obj as root (helper to get_object)
  sol::object get_object(sol::object obj) const;

  // get object from root -> path.
  sol::object get_object() const;

  // Call overload
  sol::protected_function_result call(sol::variadic_args args);

  static void expose(sol::table& lua);

  detail::Token m_root = detail::State::GLOBAL_TOKEN;
  std::vector<std::variant<std::string, size_t>> m_path;
};

///////////////////////////////////////////////////////////////////////////////
// impl ///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
struct FooBooB {
  detail::Token m_root = detail::State::GLOBAL_TOKEN;
  std::vector<std::variant<std::string, size_t>> m_path;
  static const FooBooB NULL_OBJECT;
  void begin();
  void end();
};

template <typename ReturnT, typename... ArgsT>
std::function<ReturnT(ArgsT...)> Object::get_function(
    const sol::object& object, const std::function<ReturnT(ArgsT...)>& func) const {
  if (!object.is<sol::protected_function>()) {
    return std::function<ReturnT(ArgsT...)>([](ArgsT... args) { return ReturnT(); });
  }

  sol::protected_function function = object;
  ASSERT(function.valid());
  function.error_handler = detail::State::get()["error_handler"];
  if constexpr (std::is_same_v<ReturnT, void>) {
    return std::function<ReturnT(ArgsT...)>(function);
  } else {
    return std::function<ReturnT(ArgsT...)>([function](ArgsT... args) {
      sol::protected_function_result result = function(args...);

      if (!result.valid() || result.return_count() == 0) {
        return ReturnT();
      }

      sol::object result_object = result.get<sol::object>();
      if (!result_object.valid()) {
        return ReturnT();
      }

      if (result_object.is<Object>()) {
        if constexpr (std::is_same_v<ReturnT, Object>) {
          return result.get<Object>();
        } else {
          return result.get<Object>().get<ReturnT>();
        }
      } else {
        return result.get<ReturnT>();
      }
    });
  }
}

template <typename TypeT>
Object::operator TypeT() const {
  return get<TypeT>();
}

template <typename TypeT>
TypeT Object::get() const {
  sol::object obj = get_object();
  if (obj.is<TypeT>()) {
    if constexpr (detail::is_std_function_convertible_v<TypeT>) {
      return get_function(obj, detail::to_std_func(TypeT()));
    } else {
      return obj.as<TypeT>();
    }
  } else {
    using non_const = std::remove_const_t<TypeT>;
    if constexpr (std::is_same_v<non_const, std::string> ||
                  std::is_same_v<non_const, std::string&> ||
                  std::is_same_v<non_const, std::string*>) {
      return obj.as<TypeT>();
    } else if constexpr (std::is_arithmetic_v<TypeT>) {
      return TypeT(0);
    } else if constexpr (std::is_pointer_v<TypeT>) {
      return nullptr;
    } else if constexpr (detail::is_std_function_convertible_v<TypeT>) {
      return get_function(obj, detail::to_std_func(TypeT()));
    } else {
      // Let's hope this will never be a problem.
      static_assert(std::is_default_constructible_v<TypeT>,
                    "Type must be default constructible. Contact SCR responsible.");
    }

    return TypeT();
  }
}

template <typename TypeT>
std::optional<TypeT> Object::get_if() const {
  if (is<TypeT>()) {
    return get<TypeT>();
  } else {
    return std::nullopt;
  }
}

template <typename TypeT>
bool Object::is() const {
  return get_object().is<TypeT>();
}

}  // namespace scr

#endif
