#pragma once
#ifndef SCR_SCR_HPP
#define SCR_SCR_HPP

#include <bse/reflect.hpp>
#include <scr/constructors.hpp>

namespace scr {
class Object;
class DynamicObject;

}  // namespace scr

namespace scr {

// Expose C++ function to Lua.
template <typename FunctionT>
void expose_function(std::string_view name, const FunctionT& func)
    /*-> decltype(detail::to_std_func(func), void())*/;

/*
Expose C++ type to Lua.

Example usage:

struct Foo {
  Foo() {}
  Foo(int) {}

  void set_a(int) {}
  int get_a() { return a; }

  int a;
};

scr::expose_type<Foo>("Foo", scr::Constructors<Foo(), Foo(int)>(), "set_a", &Foo::set_a, "get_a",
                      &Foo::get_a, "a", &Foo::a);
*/
template <typename TypeT, typename... ConstructorsT, typename... MembersAndMethodsT>
void expose_type(std::string_view name, const Constructors<ConstructorsT...>& constructors,
                 const MembersAndMethodsT&... members_and_methods);

// Expose a type's setter, giving it a table-like interface
template <typename T>
void expose_setter(std::string_view type_name,
                   void (*setter)(T& t, const std::string& key, const scr::DynamicObject& object));

// Expose a type's setter, giving it a table-like interface
template <typename T>
void expose_setter(std::string_view type_name,
                   void (T::*setter)(const std::string& key, const scr::DynamicObject& object));
// Expose a type's getter, giving it a table-like interface
template <typename T>
void expose_getter(std::string_view type_name,
                   scr::DynamicObject (*getter)(T& t, const std::string& key));

// Expose a type's getter, giving it a table-like interface
template <typename T>
void expose_getter(std::string_view type_name,
                   scr::DynamicObject (T::*getter)(const std::string& key));

// Attempt to convert a table to a C++ object.
template <typename T>
T from_table(const scr::Object& table);

// Attempt to convert a table to a C++ object.
template <typename T>
T from_table_sol(const sol::table& lua_table);
}  // namespace scr

#endif