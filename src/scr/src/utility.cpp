#include "scr/detail/config.hpp"
#include <bse/assert.hpp>
#include <bse/utility.hpp>
#include <scr/detail/utility.hpp>

namespace scr::detail {

size_t get_raw_size(const sol::table &table) {
  size_t size = 0;
  table.for_each([&size](const sol::object &key, const sol::object &value) { size++; });
  return size;
}

size_t get_size(const sol::table &table) {
  size_t size = 0;

  for_each(table, [&size](const sol::object &key, const sol::object &value) { size++; });
  return size;
}

void set_readonly(sol::object &object, sol::state &lua) {
  if (!object || !object.is<sol::table>() || object.get_type() == sol::type::userdata) {
    return;
  }

  sol::table meta = lua.create_table_with();
  size_t object_size = detail::get_raw_size(object.as<sol::table>());
  object.as<sol::table>().for_each([&meta, &lua](sol::object &key, sol::object &value) {
    sol::object proxy = value;
    set_readonly(proxy, lua);
    meta[key] = proxy;
  });

  sol::table proxy = lua.create_table_with();
  meta[sol::meta_function::new_index] = lua["readonly_handler"];
  meta[sol::meta_function::index] = meta;
  meta[sol::meta_function::pairs] =
      std::function<sol::object(sol::table)>(lua["readonly_make_pairs"])(meta);
  meta["__unbound_readonly"] = true;

  size_t raw_size = detail::get_raw_size(meta);
  ASSERT(raw_size - object_size == detail::readonly_meta_size)
      << "Inconsistent meta sizes. Something has changed on either end.";

  proxy[sol::metatable_key] = meta;
  object = proxy;
}

void to_string(const sol::object &object, std::stringstream &stream, std::string &indentation,
               bool do_sort) {
  if (!object.valid()) {
    stream << "nil";
  } else if (object.is<sol::table>()) {
    sol::table table = object.as<sol::table>();
    if (table[sol::metatable_key].valid()) {
      stream << sol::object(object.as<sol::table>()[sol::metatable_key]["__name"]).as<std::string>()
             << "_userdata";
    } else {
      std::vector<std::pair<int, sol::object>> index_children;
      std::vector<std::pair<std::string, sol::object>> key_children;
      for_each(object.as<sol::table>(),
               [&index_children, &key_children](const sol::object &key, const sol::object &value) {
                 if (key.get_type() == sol::type::number) {
                   index_children.emplace_back(key.as<int>(), value);
                 } else {
                   key_children.emplace_back(key.as<std::string>() + " = ", value);
                 }
               });

      if (do_sort) {
        std::sort(index_children.begin(), index_children.end(),
                  [](const auto &lhs, const auto &rhs) { return lhs.first < rhs.first; });
        std::sort(key_children.begin(), key_children.end(),
                  [](const auto &lhs, const auto &rhs) { return lhs.first < rhs.first; });
      }

      if (do_sort && key_children.empty()) {
        for (const auto &[i, value] : index_children) {
          key_children.emplace_back("", value);
        }
      } else {
        for (const auto &[i, value] : index_children) {
          key_children.emplace_back("[" + std::to_string(i) + "] = ", value);
        }
      }

      if (key_children.empty()) {
        stream << "{}";
      } else {
        stream << "{" << std::endl;
        indentation += "  ";
        for (size_t i = 0; i < key_children.size(); i++) {
          const auto &[key, value] = key_children[i];
          stream << indentation << key;
          to_string(value, stream, indentation, do_sort);

          if (i < key_children.size() - 1) {
            stream << ",";
          }
          stream << std::endl;
        }
        indentation.pop_back();
        indentation.pop_back();
        stream << indentation << "}";
      }
      index_children.clear();
      key_children.clear();
    }

  } else if (object.is<bool>()) {
    if (object.as<bool>()) {
      stream << "true";
    } else {
      stream << "false";
    }
  } /*else if (object.is<sol::function>()) {
    stream << "func_" << object.pointer() << std::endl;

  }*/
  else {
    if (object.get_type() == sol::type::string) {
      stream << "\"" << object.as<std::string>() << "\"";
    } else {
      stream << object.as<std::string>();
    }
  }
}

std::string to_string(const sol::object &object, bool do_sort) {
  std::string string;

  std::stringstream stream;
  std::string indentation;
  to_string(object, stream, indentation, do_sort);
  string = stream.str();
  bse::remove(string, '\r');
  while (!string.empty() && (string.back() == '\r' || string.back() == '\n')) {
    string.pop_back();
  }
  string += '\n';
  return string;
}

void create_pointer_map(const sol::table &parent, std::vector<char> &path,
                        std::map<const void *, std::vector<char>> &map) {
  std::vector<std::pair<std::string, sol::table>> tables;

  for_each(parent, [&path, &tables, &map](const sol::object &key, const sol::object &value) {
    std::vector<char> &entry = map[value.pointer()];
    std::string key_str = key.as<std::string>();
    path.insert(path.end(), key_str.begin(), key_str.end());
    path.push_back('\0');
    entry = path;

    path.erase(path.end() - key_str.size() - 1, path.end());

    if (value.is<sol::table>()) {
      // tables.push_back({key_str, value.as<sol::table>()});
    }
  });

  for (const std::pair<std::string, sol::table> &pair : tables) {
    path.insert(path.end(), pair.first.begin(), pair.first.end());
    path.push_back('\0');
    create_pointer_map(pair.second, path, map);
    path.erase(path.end() - pair.first.size() - 1, path.end());
  }
}
//
// bool find_path(const void *ptr, const sol::table &parent, std::vector<std::string> &path) {
//  size_t pre_path_size = path.size();
//  for_each(parent, [ptr, pre_path_size, &path](const sol::object &key, const sol::object &value) {
//    if (path.size() > pre_path_size) {
//      return;
//    }
//
//    path.push_back(key.as<std::string>());
//    if (value.pointer() == ptr) {
//      return;
//    }
//
//    if (value.is<sol::table>() && find_path(ptr, value.as<sol::table>(), path)) {
//      return;
//    }
//
//    path.pop_back();
//  });
//
//  return path.size() > pre_path_size;
//}
//
// std::optional<std::vector<std::string>> find_path(const void *ptr, const sol::table &root) {
//  std::vector<std::string> path;
//
//  if (root.pointer() == ptr) {
//    return path;
//  }
//
//  if (find_path(ptr, root, path)) {
//    return path;
//  }
//
//  return std::nullopt;
//}

std::optional<std::vector<std::string>> find_path(const void *ptr, const sol::table &root) {
  ASSERT(!false) << "UNIMPLEMENTED\n";
  std::vector<std::string> path;

  if (root.pointer() == ptr) {
    return path;
  }

  if (!root) {
    std::nullopt;
  }

  std::vector<char> tmp_path;
  std::map<const void *, std::vector<char>> map;
  create_pointer_map(root, tmp_path, map);

  auto it = map.find(ptr);
  if (it == map.end()) {
    return std::nullopt;
  }

  const std::vector<char> &raw_path = it->second;

  ASSERT(!raw_path.empty()) << "Cached path empty.\n";
  for (const char *cur = raw_path.data(); cur < raw_path.data() + raw_path.size();) {
    path.push_back(std::string(cur));
    cur += path.back().size() + 1;
  }
  return path;
}

}  // namespace scr::detail