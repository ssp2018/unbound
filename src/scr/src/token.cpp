#include <scr/detail/token.hpp>

namespace scr::detail {

bool Token::operator==(const Token& other) const { return index == other.index && id == other.id; }
Token::operator bool() const { return !(*this == NULL_TOKEN); }

}  // namespace scr::detail