#include "bse/file_path.hpp"
#include "bse/result.hpp"
#include "scr/detail/token.hpp"
#include <bse/file.hpp>
#include <scr/detail/state.hpp>
#include <scr/object.hpp>
#include <scr/script.hpp>
template class bse::Asset<scr::Script>;

namespace scr {

Script::Script(Script &&other) : m_token(other.m_token) { other.m_token = detail::NULL_TOKEN; }
Script &Script::operator=(Script &&other) {
  detail::State::replace_script(m_token, other.m_token);
  other.m_token = detail::NULL_TOKEN;
  return *this;
}

bse::Result<Script> Script::load(bse::FilePath path) {
  bse::Result<std::string> script = bse::load_file_str(path);
  if (!script) {
    return bse::Result<Script>(script.get_error());
  }

  return std::move(Script(path.get_string(), *script));
}

Object Script::return_value() const { return Object(m_token); }

Script::Script(const std::string &name, const std::string &script)
    : m_token(detail::State::register_script(name, script)) {}

Script::~Script() {
  if (m_token && detail::State::is_valid()) {
    detail::State::unregister_script(m_token);
  }
}

Object Script::find_global_object(const void *script_pointer) { return Object::NULL_OBJECT; }

}  // namespace scr
