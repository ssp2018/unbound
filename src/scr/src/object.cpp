#include "bse/assert.hpp"
#include "bse/log.hpp"
#include "scr/detail/token.hpp"
#include <bse/asset.hpp>
#include <scr/detail/config.hpp>
#include <scr/detail/state.hpp>
#include <scr/detail/utility.hpp>
#include <scr/object.hpp>
#include <scr/scr.hpp>
#include <scr/script.hpp>
namespace sol {
namespace stack {
struct record;
}
}  // namespace sol

namespace scr {
using namespace detail;

const Object Object::NULL_OBJECT(NULL_TOKEN);

Object::Object() {}
Object::Object(const std::string& name) { m_path = {name}; }
Object::Object(const std::string& script_path, const std::string& name) {
  bse::Asset<Script> script(script_path);
  if (script) {
    Object obj = script->return_value();
    m_root = obj.m_root;
    m_path = {name};
  }
}
Object::Object(Token script) : m_root(script) {}

// Returns the path to the script
std::string Object::get_script_path() const {
  return scr::detail::State::get_name(m_root);  //
}

// Returns the name of the object
std::string Object::get_name() const {
  std::string script_path;
  for (auto part : m_path) {
    if (auto* str = std::get_if<std::string>(&part)) {
      //
      script_path += *str;
      script_path += "/";

    } else if (auto* num = std::get_if<size_t>(&part)) {
      //

      script_path += *num;
      script_path += "/";
    }
  }
  return script_path;
}

std::string Object::str() const { return detail::to_string(get_object()); }

Object::Iterator Object::begin() const {
  if (!has_children()) {
    return end();
  }

  sol::table table = get<sol::table>();

  std::vector<std::pair<std::variant<std::string, size_t>, Object>> children;
  detail::for_each(table, [&children, this](sol::object& key, sol::object& value) {
    if (key.is<double>()) {
      size_t key_number = size_t(key.as<double>());
      children.emplace_back(key_number - 1, (*this)[key_number - 1]);
    } else {
      std::string key_str = key.as<std::string>();
      children.emplace_back(key_str, (*this)[key_str]);
    }
  });

  return Iterator(children);
}
Object::Iterator Object::end() const { return Iterator({}); }

bool Object::has_children() const {
  sol::object object = *this;
  if (object.get_type() != sol::type::table) {
    return false;
  }

  sol::table table = object.as<sol::table>();
  if (table.valid()) {
    if constexpr (is_readonly_enforced) {
      ASSERT(table["__unbound_readonly"].valid())
          << "Script object fetched from C++ is not marked readonly.";
      return detail::get_size(table) > 0;
    } else {
      return !table.empty();
    }
  } else {
    return false;
  }
}

Object Object::operator[](const std::string_view& key) const {
  Object child;
  child.m_path = m_path;
  child.m_path.push_back(std::string(key));
  child.m_root = m_root;

  return child;
}

Object Object::operator[](const std::string& key) const {
  return operator[](std::string_view(key));
}

Object Object::operator[](const char* key) const { return operator[](std::string_view(key)); }

Object Object::operator[](int key) const { return operator[](size_t(key)); }

Object Object::operator[](size_t key) const {
  Object child;
  child.m_path = m_path;
  child.m_path.push_back(key + 1);
  child.m_root = m_root;

  return child;
}

Object::Iterator::Iterator(
    const std::vector<std::pair<std::variant<std::string, size_t>, Object>>& elements)
    : m_elements(elements) {
  if (m_elements.empty()) {
    m_index = -1;
  }
}

bool Object::Iterator::operator!=(const Iterator& other) const {
  if (m_index >= m_elements.size() && other.m_index == -1) {
    return false;
  }

  return m_index != other.m_index;
}

Object::Iterator& Object::Iterator::operator++() {
  m_index++;
  return *this;
}

std::pair<std::variant<std::string, size_t>, Object> Object::Iterator::operator*() const {
  return m_elements[m_index];
}

bool Object::is_null() const {
  sol::object obj = get_object();
  return !obj.valid() || obj == sol::nil;
}

sol::object Object::get_object(sol::object obj) const {
  for (const std::variant<std::string, size_t>& part : m_path) {
    if (!obj) {
      return obj;
    }

    if (const std::string* str = std::get_if<std::string>(&part)) {
      obj = obj.as<sol::table>()[*str];
    } else if (const size_t* index = std::get_if<size_t>(&part)) {
      obj = obj.as<sol::table>()[*index];
    } else {
      ASSERT(false) << "Unimplemented path part variation.";
    }
  }

  return obj;
}

sol::object Object::get_object() const {
  if (m_root == detail::State::GLOBAL_TOKEN) {
    return get_object(detail::State::get().globals());
  } else
    return get_object(detail::State::get_object(m_root));
}

sol::protected_function_result Object::call(sol::variadic_args args) {
  sol::protected_function func = get_object();
  return func(args);
}

void Object::expose(sol::table& lua) {
  lua.new_usertype<Object>("Object",
                           sol::constructors<Object(), Object(const std::string&),               //
                                             Object(const std::string&, const std::string&)>(),  //
                           sol::meta_function::call, &Object::call,                              //
                           "get_script_path", &Object::get_script_path,                          //
                           "get_name", &Object::get_name                                         //
  );
}

}  // namespace scr