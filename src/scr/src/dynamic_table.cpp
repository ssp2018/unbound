#include "bse/assert.hpp"
#include "bse/log.hpp"
#include "scr/detail/state.hpp"
#include "scr/detail/utility.hpp"
#include "scr/object.hpp"
#include <scr/dynamic_object.hpp>
#include <scr/dynamic_table.hpp>

namespace scr {
DynamicTable::DynamicTable() {}

DynamicTable::DynamicTable(const scr::Object& object) {
  for (const std::pair<std::variant<std::string, size_t>, scr::Object>& pair : object) {
    if (pair.second.has_children()) {
      DynamicTable child(pair.second);
      m_entries[pair.first] = child;
    } else {
      m_entries[pair.first] = pair.second;
    }
  }
}

void DynamicTable::expose(sol::table& lua) {
  lua.new_usertype<DynamicTable>(
      "DynamicTable", sol::constructors<DynamicTable()>(), sol::meta_function::index,
      &DynamicTable::index, sol::meta_function::new_index, &DynamicTable::new_index,
      sol::meta_function::length, &DynamicTable::size, "clear", &DynamicTable::clear);
}

DynamicObject DynamicTable::index(const sol::object& key) {
  auto it = m_entries.end();
  if (key.get_type() == sol::type::number) {
    it = m_entries.find(key.as<size_t>());
  } else if (key.get_type() == sol::type::string) {
    it = m_entries.find(key.as<std::string>());
  } else {
    LOG(WARNING) << "Invalid key type.";
  }

  if (it == m_entries.end()) {
    return nullptr;
  }

  return it->second;
}

void DynamicTable::new_index(const sol::object& key, const DynamicObject& value) {
  if (key.get_type() == sol::type::number) {
    m_entries[key.as<size_t>()] = value;
  } else if (key.get_type() == sol::type::string) {
    m_entries[key.as<std::string>()] = value;
  } else {
    LOG(WARNING) << "Invalid key type.";
  }
}

size_t DynamicTable::size() const { return m_entries.size(); }

DynamicObject& DynamicTable::operator[](const std::string& key) { return m_entries[key]; }
const DynamicObject* DynamicTable::get_if(const std::string& key) const {
  auto it = m_entries.find(key);
  if (it == m_entries.end()) {
    return nullptr;
  }
  return &it->second;
}

DynamicObject* DynamicTable::get_if(const std::string& key) {
  auto it = m_entries.find(key);
  if (it == m_entries.end()) {
    return nullptr;
  }
  return &it->second;
}

std::string DynamicTable::to_string() const {
  std::stringstream sstream;
  sstream << "{" << std::endl;
  for (const auto& [key, value] : m_entries) {
    if (const std::string* str = std::get_if<std::string>(&key)) {
      sstream << *str;
    } else if (const size_t* i = std::get_if<size_t>(&key)) {
      sstream << "[" << *i << "]";
    } else {
      ASSERT(false) << ":(";
    }

    if (value.is<std::string>()) {
      sstream << " = \"" << value.to_string() << "\"," << std::endl;
    } else {
      sstream << " = " << value.to_string() << "," << std::endl;
    }
  }

  sstream << "}";
  return sstream.str();
}

void DynamicTable::from_string(const std::string& str) {
  sol::state& state = scr::detail::State::get();

  // std::cout << str << std::endl;
  sol::table table = state.script("return " + str);

  scr::detail::for_each(table, [this](sol::object& key, sol::object& value) {
    //
    DynamicObject& obj = this->operator[](key.as<std::string>());
    if (value.is<std::string>()) {
      obj = value.as<std::string>();
    } else if (value.is<double>()) {
      obj = value.as<double>();
    } else if (value.is<bool>()) {
      obj = value.as<bool>();
    } else {
      ASSERT(false)
          << "Table contained values that could not be interpreted as either a string or a double";
    }
  });
}

void DynamicTable::clear() { m_entries.clear(); }

std::unordered_map<std::variant<std::string, size_t>, DynamicObject>::iterator
DynamicTable::begin() {
  return m_entries.begin();
}
std::unordered_map<std::variant<std::string, size_t>, DynamicObject>::iterator DynamicTable::end() {
  return m_entries.end();
}

std::unordered_map<std::variant<std::string, size_t>, DynamicObject>::const_iterator
DynamicTable::begin() const {
  return m_entries.begin();
}
std::unordered_map<std::variant<std::string, size_t>, DynamicObject>::const_iterator
DynamicTable::end() const {
  return m_entries.end();
}

}  // namespace scr
