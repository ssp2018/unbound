#include "bse/assert.hpp"
#include "bse/asset.hpp"
#include "bse/file_path.hpp"
#include "bse/log.hpp"
#include "bse/task_scheduler.hpp"
#include "scr/detail/token.hpp"
#include "scr/object.hpp"
#include <bse/utility.hpp>
#include <scr/detail/config.hpp>
#include <scr/detail/state.hpp>
#include <scr/detail/utility.hpp>
#include <scr/dynamic_object.hpp>
#include <scr/dynamic_table.hpp>
#include <scr/script.hpp>

namespace scr::detail {

std::mutex State::m_script_lock;
std::array<std::unique_ptr<State::Script>, 512> State::m_scripts = {{0}};
std::atomic<uint32_t> State::m_current_token_id = 1;

const Token State::GLOBAL_TOKEN = {State::m_scripts.size(), 0};

thread_local State* State::m_state = nullptr;
thread_local bool State::m_is_valid = false;
std::vector<State::TypeExposer>* State::m_type_exposers = nullptr;
std::unordered_map<std::string, sol::table (*)(sol::state&, void*)>* State::m_value_creators =
    nullptr;
std::unordered_map<std::string, sol::table (*)(sol::state&, void*)>* State::m_pointer_creators =
    nullptr;
std::unordered_map<std::string, std::vector<std::string>>* State::m_type_members = nullptr;
std::unordered_map<void (*)(), std::string>* State::m_type_names = nullptr;

void State::destroy_thread() { delete m_state; }

void State::destroy() {
  // delete m_state;
  delete m_type_exposers;
  delete m_value_creators;
  delete m_pointer_creators;
  delete m_type_members;
  delete m_type_names;

  struct Task : bse::UniTask {
    virtual ~Task() = default;
    void kernel() override {
      // initialize_thread();
      // State::m_state->m_objects[token.index] = {token, sol::nil};
      State::destroy_thread();
    }
    // Token token;
    State* state;
  };
  bse::TaskScheduler* scheduler = bse::TaskScheduler::get();

  Task t;
  t.state = m_state;
  scheduler->wait_on_task(scheduler->add_uni_task(t));
}

void State::create_readonly_make_pairs() {
  m_sol_state.script(R"EOF(
function readonly_make_pairs(real_table)
  local function pairs()
    local key, value, real_key = nil, nil, nil
    local function nexter() -- both args dummy
      key, value = next(real_table, real_key)
      while key and string.sub(key, 1, 2) == "__" do
        key, value = next(real_table, key)
      end
      real_key = key
      return key, value
    end
    return nexter -- values 2 and 3 dummy
  end
  return pairs
end
 )EOF");
}

template <typename T>
void State::expose_builtin_type(sol::table& lua, const std::string& name) {
  expose_type_standard<T>(name);
  T::expose(lua);
}

State::State() : m_num_exposed_types(0) {
  m_is_valid = true;
  m_sol_state.open_libraries(sol::lib::base, sol::lib::string, sol::lib::debug, sol::lib::math,
                             sol::lib::os, sol::lib::table);
  m_sol_state.set_function("require", State::require);
  m_sol_state.set_function("print_cpp", State::print);
  m_sol_state.script(R"(
function error_handler(e)
  print_error_object(e, debug.traceback())
end
function readonly_handler(e)
  print_error_object("Attempt to modify immutable value", debug.traceback())
end
function print(e)
  local i = debug.getinfo(2)
  print_cpp(e, i.source .. ":" .. i.currentline .. ": in " .. i.what)
end
)");

  m_sol_state.set_function("print_error_object", State::print_error_object);

  if constexpr (is_readonly_enforced) {
    create_readonly_make_pairs();
    sol::object obj = m_sol_state["math"];
    detail::set_readonly(obj, m_sol_state);
    m_sol_state["math"] = obj;

    obj = m_sol_state["debug"];
    detail::set_readonly(obj, m_sol_state);
    m_sol_state["debug"] = obj;
  }

  m_sol_state.create_named_table("cpp");
  sol::table cpp = m_sol_state["cpp"];
  expose_builtin_type<DynamicTable>(cpp, "DynamicTable");
  expose_builtin_type<::scr::Object>(cpp, "Object");

  m_sol_state.create_named_table("reflect");

  m_sol_state.script(R"(

  reflect.__members = {}
  reflect.__types = {}
  reflect.__element_types = {}
  reflect.__type_names = {}

  function reflect.members(obj)
    local meta = getmetatable(obj)
    if meta and meta.__name then
      return reflect.__members[meta.__name]
    end
    return nil
  end

  function reflect.type(obj)
    local meta = getmetatable(obj)
    if meta and meta.__name then
      return reflect.__types[meta.__name]
    end
    return nil
  end

  function reflect.type_name(obj)
    local meta = getmetatable(obj)
    if meta and meta.__name and reflect.__type_names[meta.__name] then
      return reflect.__type_names[meta.__name]
    else
      return type(obj)
    end
  end 

  function reflect.element_type(obj)
    local meta = getmetatable(obj)
    if meta and meta.__name then
      return reflect.__element_types[meta.__name]
    end
    return nil
  end 

  function reflect.has_ipairs(obj)
    local meta = getmetatable(obj)
    return meta and meta.__ipairs
  end

  function reflect.has_pairs(obj)
    local meta = getmetatable(obj)
    return meta and meta.__pairs
  end

  function reflect.is_valid_userdata(obj)
    local meta = getmetatable(obj)
    return meta and meta.new
  end

  function reflect.is_invalid_userdata(obj)
    local meta = getmetatable(obj)
    return meta and not meta.new and type(obj) == "userdata"
  end

)");

  update();
}

void State::print(const sol::object& object, const sol::object& info) {
  if (info.valid()) {
    std::string info_str = info.as<std::string>();
    bse::remove(info_str, '\r');
    LOG(NOTICE) << "[lua] " << info_str << ": " << detail::to_string(object) << '\n';
  } else {
    LOG(NOTICE) << "[lua] " << detail::to_string(object) << '\n';
  }
}

void State::print_error(const std::string& what) {
  std::string clean = what;
  bse::remove(clean, '\r');
  LOG(FATAL) << "[lua] " << clean << '\n';
}

void State::print_error_object(const sol::object& object, const sol::object& traceback) {
  // Remove the line referring to our error handler from the traceback
  std::string traceback_str = traceback.as<std::string>();
  size_t first_newline = traceback_str.find('\n');
  size_t second_newline = traceback_str.find('\n', first_newline + 1);

  if (first_newline != std::string::npos && second_newline != std::string::npos) {
    traceback_str.replace(first_newline, second_newline - first_newline, "");
  }

  print_error(object.as<std::string>() + "\n    " + traceback_str);
}

sol::object State::require(const std::string& path) {
  if (path == "cpp" || path == "math" || path == "debug" || path == "reflect" || path == "os" ||
      path == "table" || path == "string") {
    return m_state->m_sol_state[path];
  }

  bse::Asset<scr::Script> mod("assets/script/" + path + ".lua");
  if (!mod) {
    return sol::nil;
  }

  return mod->return_value();
}

void State::update() {
  if (m_type_exposers && m_num_exposed_types < m_type_exposers->size()) {
    sol::table cpp = m_sol_state["cpp"];
    for (size_t i = m_num_exposed_types; i < m_type_exposers->size(); i++) {
      (*m_type_exposers)[i](*this, cpp);
    }

    m_num_exposed_types = m_type_exposers->size();

    for (auto& [type_name, member_names] : *m_type_members) {
      sol::table metatable = cpp[type_name];  //[sol::metatable_key];
      std::string meta_type_name = metatable["__name"];
      meta_type_name.erase(meta_type_name.end() - 5, meta_type_name.end());

      auto vec_it = m_pointer_creators->find("std::vector<" + type_name + ">");
      if (vec_it != m_pointer_creators->end()) {
        sol::table vec = vec_it->second(m_sol_state, (void*)1);
        sol::table vec_metatable = vec[sol::metatable_key];
        std::string vec_type_name = vec_metatable["__name"];
        vec_type_name.pop_back();
        m_sol_state["reflect"]["__element_types"][vec_type_name] = metatable;
        m_sol_state["reflect"]["__element_types"][vec_type_name + "*"] = metatable;
      }

      m_sol_state["reflect"]["__types"][meta_type_name] = metatable;
      m_sol_state["reflect"]["__types"][meta_type_name + "*"] = metatable;
      m_sol_state["reflect"]["__type_names"][meta_type_name] = type_name;
      m_sol_state["reflect"]["__type_names"][meta_type_name + "*"] = type_name;
      m_sol_state["reflect"]["__members"][meta_type_name] = member_names;
      m_sol_state["reflect"]["__members"][meta_type_name + "*"] = member_names;
    }
  }
}  // namespace scr::detail

sol::state& State::get() {
  initialize_thread();

  m_state->update();

  return m_state->m_sol_state;
}

Token State::grab_token() {
  m_script_lock.lock();
  uint32_t i = 0;
  for (; i < m_scripts.size(); i++) {
    if (!m_scripts[i]) {
      m_scripts[i].reset(new Script());
      break;
    }
  }
  m_script_lock.unlock();

  ASSERT(i < m_scripts.size()) << "Too many scripts loaded! Only " << m_scripts.size()
                               << "supported.";

  if (i >= m_scripts.size()) {
    return NULL_TOKEN;
  } else {
    return {i, m_current_token_id++};
  }
}

Token State::register_script(const std::string& name, const std::string& script) {
  initialize_thread();

  Token token = grab_token();
  if (!token) {
    return token;
  }
  *m_scripts[token.index] = {token, name, script};

  struct Task : bse::UniTask {
    virtual ~Task() = default;
    void kernel() override {
      initialize_thread();
      State::m_state->m_objects[token.index] = {token, sol::nil};
    }
    Token token;
  };
  bse::TaskScheduler* scheduler = bse::TaskScheduler::get();

  Task t;
  t.token = token;
  scheduler->wait_on_task(scheduler->add_uni_task(t));  // TODO MH: temp fix
  return token;
}  // namespace scr

void State::initialize_thread() {
  if (!m_state) {
    // m_state.reset(new State());
    m_state = new State();
  }
}

sol::object State::get_object(Token token) {
  initialize_thread();
  if (token.index >= m_scripts.size()) {
    LOG(FATAL) << "Attempted to get object with out-of-range token.";
    return sol::nil;
  }

  Object& object = m_state->m_objects[token.index];
  if (object.token.id != token.id) {
    return sol::nil;
  }

  if (object.object) {
    return object.object;
  }

  // Object was nil, which means that we need to load it.
  // Get script from global store.
  Script script;
  m_script_lock.lock();
  std::unique_ptr<Script>& script_ptr = m_scripts[token.index];
  if (script_ptr && script_ptr->token.id == token.id) {
    script = *script_ptr;
  }
  m_script_lock.unlock();

  // Was script missing? It is probably being unregistered
  if (!script.token) {
    LOG(WARNING) << "Attempting to fetch object from unregistered script.";
    return sol::nil;
  }

  // Load script into this thread.
  sol::object env_object = load_script(script.script, script.name);

  if constexpr (detail::is_readonly_enforced) {
    detail::set_readonly(env_object, m_state->m_sol_state);
  }
  object.object = env_object;
  return object.object;
}

std::string State::get_name(Token token) {
  //
  initialize_thread();

  if (token == State::GLOBAL_TOKEN) {
    return "__global";
  }

  if (token.index >= m_scripts.size()) {
    LOG(FATAL) << "Attempted to get object with out-of-range token.";
    return "";
  }

  return m_scripts[token.index]->name;
}

sol::table State::load_script(const std::string& script, const std::string& name) {
  initialize_thread();
  sol::state& lua = m_state->m_sol_state;
  sol::environment env(lua, sol::create);
  env["require"] = lua["require"];
  env["tostring"] = lua["tostring"];
  env["print"] = lua["print"];
  env["tonumber"] = lua["tonumber"];
  env["pairs"] = lua["pairs"];
  env["ipairs"] = lua["ipairs"];
  env["type"] = lua["type"];

  sol::protected_function_result result =
      lua.safe_script(script, env, &sol::script_pass_on_error, name);
  if (!result.valid()) {
    sol::error error = result;
    print_error(error.what());
  }

  return env.as<sol::table>();
}

State::~State() {
  m_is_valid = false;
  // m_state.reset();
  // delete m_state;
}

bool State::is_valid() { return m_is_valid; }

void State::unregister_script(Token token) {
  initialize_thread();
  if (token.index >= m_scripts.size()) {
    LOG(FATAL) << "Attempted to unregister script with out-of-range token.";
    return;
  }

  m_script_lock.lock();
  m_scripts[token.index] = nullptr;
  m_script_lock.unlock();

  struct Task : bse::UniTask {
    virtual ~Task() = default;
    void kernel() override {
      initialize_thread();
      State::m_state->m_objects[token.index] = {0};
    }
    Token token;
  };
  bse::TaskScheduler* scheduler = bse::TaskScheduler::get();

  Task t;
  t.token = token;
  scheduler->wait_on_task(scheduler->add_uni_task(t));  // TODO MH: temp fix
}

void State::replace_script(Token old_script, Token new_script) {
  initialize_thread();
  if (old_script.index >= m_scripts.size() || new_script.index >= m_scripts.size()) {
    LOG(FATAL) << "Attempted to replace script with out-of-range token.";
    return;
  }

  m_script_lock.lock();
  std::unique_ptr<Script>& old_script_ptr = m_scripts[old_script.index];
  std::unique_ptr<Script>& new_script_ptr = m_scripts[new_script.index];
  if (new_script_ptr) {
    new_script_ptr->token = old_script;
  }
  old_script_ptr = nullptr;
  new_script_ptr.swap(old_script_ptr);
  m_script_lock.unlock();

  struct Task : bse::UniTask {
    virtual ~Task() = default;
    void kernel() override {
      initialize_thread();
      Object& old_object = State::m_state->m_objects[old_script.index];
      Object& new_object = State::m_state->m_objects[new_script.index];
      new_object.token = old_script;
      new_object.object = sol::object(sol::nil);
      old_object = {0};
      std::swap(old_object, new_object);
    }

    Token old_script;
    Token new_script;
  };
  bse::TaskScheduler* scheduler = bse::TaskScheduler::get();

  Task t;
  t.old_script = old_script;
  t.new_script = new_script;
  scheduler->wait_on_task(scheduler->add_uni_task(t));  // TODO MH: temp fix
}

sol::table State::create_userdata_value(const std::string& type_name, void* data) {
  auto it = m_value_creators->find(type_name);
  if (it == m_value_creators->end()) {
    LOG(WARNING) << "Cannot create userdata value of unknown type '" << type_name << "'.";
    return sol::nil;
  }
  return it->second(State::get(), data);
}
sol::table State::create_userdata_pointer(const std::string& type_name, void* data) {
  auto it = m_pointer_creators->find(type_name);
  if (it == m_pointer_creators->end()) {
    LOG(WARNING) << "Cannot create userdata pointer of unknown type '" << type_name << "'.";
    return sol::nil;
  }

  return it->second(State::get(), data);
}

}  // namespace scr::detail