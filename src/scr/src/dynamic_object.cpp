#include "bse/assert.hpp"
#include "scr/detail/state.hpp"
#include <scr/dynamic_object.hpp>
#include <scr/dynamic_table.hpp>

namespace scr {
struct DynamicTable;
}
namespace sol {
namespace stack {
struct record;
}
}  // namespace sol
namespace scr {

DynamicObject::DynamicObject() : m_data(nullptr) {}

bool DynamicObject::is_number() const { return std::get_if<double>(&m_data); }
bool DynamicObject::is_string() const { return std::get_if<std::string>(&m_data); }
bool DynamicObject::is_boolean() const { return std::get_if<bool>(&m_data); }
bool DynamicObject::is_table() const {
  const Userdata* data = std::get_if<Userdata>(&m_data);
  if (!data) {
    data = std::get_if<OwningUserdata>(&m_data);
  }
  return data && data->type_name == "DynamicTable";
}

bool DynamicObject::is_userdata() const {
  return std::get_if<Userdata>(&m_data) || std::get_if<OwningUserdata>(&m_data);
}

std::string DynamicObject::to_string() const {
  if (is_number()) {
    return std::to_string(get<double>());
  } else if (is_string()) {
    return get<std::string>();
  } else if (is_boolean()) {
    return get<bool>() ? "true" : "false";
  } else if (is_table()) {
    return get_if<DynamicTable>()->to_string();
  } else if (is_userdata()) {
    return get<Userdata>().type_name + "_userdata";
  } else {
    return "!!UNKNOWN!!";
  }
}

DynamicObject::OwningUserdata::~OwningUserdata() {
  if (data) {
    destructor(data);
  }
}

DynamicObject::OwningUserdata::OwningUserdata(const OwningUserdata& other)
    : DynamicObject::Userdata((const Userdata&)other),
      destructor(other.destructor),
      copier(other.copier) {
  if (data) {
    data = copier(data);
  }
}
DynamicObject::OwningUserdata& DynamicObject::OwningUserdata::operator=(
    const DynamicObject::OwningUserdata& other) {
  if (data) {
    destructor(data);
  }

  type_name = other.type_name;
  data = other.data;
  destructor = other.destructor;
  copier = other.copier;
  if (data) {
    data = copier(data);
  }

  return *this;
}

namespace detail {
void populate_table(DynamicTable& table, const sol::table& sol_table) {
  std::vector<std::pair<sol::object, sol::object>> pairs;
  std::vector<std::pair<std::string, sol::table>> queue;
  sol_table.for_each([&pairs](const sol::object& key, const sol::object& value) {
    pairs.push_back({key, value});
  });

  for (const auto& [key, value] : pairs) {
    if (value.get_type() == sol::type::table) {
      sol::table child_sol_table = value.as<sol::table>();
      if (!child_sol_table[sol::metatable_key].valid()) {
        queue.push_back({key.as<std::string>(), child_sol_table});
        continue;
      }
    } else if (value.get_type() == sol::type::function) {
      LOG(WARNING) << "Runtime assignment of function to dynamic table is not allowed.";
      continue;
    }

    table[key.as<std::string>()] = value.as<DynamicObject>();
  }

  for (const auto& [key, child] : queue) {
    DynamicObject& child_object = table[key] = DynamicTable();
    populate_table(*child_object.get_if<DynamicTable>(), child);
  }

}  // namespace detail

void get(lua_State* L, int index, sol::stack::record& tracking, scr::DynamicObject& obj) {
  obj.m_data = sol::stack::getter<decltype(obj.m_data)>::get(L, index, tracking);

  if (DynamicObject::Userdata* data = std::get_if<DynamicObject::Userdata>(&obj.m_data)) {
    if (data->data && data->type_name == "#OwnMyDynamicTable") {
      DynamicTable* table = (DynamicTable*)data->data;
      obj.m_data = DynamicObject::OwningUserdata::create(*table);
      delete table;
    }
  }
}

int push(lua_State* L, const scr::DynamicObject& things) {
  if (const scr::DynamicObject::Userdata* data =
          std::get_if<scr::DynamicObject::Userdata>(&things.m_data)) {
    return sol::stack::pusher<scr::DynamicObject::Userdata>::push(L, *data);
  } else if (const scr::DynamicObject::OwningUserdata* data =
                 std::get_if<scr::DynamicObject::OwningUserdata>(&things.m_data)) {
    return sol::stack::pusher<sol::table>::push(
        L, data->object_creator
               ? data->object_creator(L, data->data)
               : scr::detail::State::create_userdata_value(data->type_name, data->data));
  }

  return sol::stack::pusher<decltype(things.m_data)>::push(L, things.m_data);
}
}  // namespace detail

}  // namespace scr

namespace sol {

namespace stack {

scr::DynamicObject getter<scr::DynamicObject>::get(lua_State* L, int index, record& tracking) {
  scr::DynamicObject obj;

  scr::detail::get(L, index, tracking, obj);
  return obj;
}

int ::sol::stack::pusher<scr::DynamicObject>::push(lua_State* L, const scr::DynamicObject& things) {
  return scr::detail::push(L, things);
}

}  // namespace stack
}  // namespace sol

namespace sol {

namespace stack {

scr::DynamicObject::Userdata getter<scr::DynamicObject::Userdata>::get(lua_State* L, int index,
                                                                       record& tracking) {
  struct Dummy {};

  sol::table obj = getter<sol::table>::get(L, index, tracking);
  scr::DynamicObject::Userdata data;
  if (obj.valid() && obj.get_type() != sol::type::userdata &&
      obj.get_type() != sol::type::lightuserdata) {
    scr::DynamicTable* table = new scr::DynamicTable();
    scr::detail::populate_table(*table, obj);
    data.type_name = "#OwnMyDynamicTable";
    data.data = table;
  } else {
    data.type_name = obj[sol::metatable_key]["__name"];
    data.data = obj.as<Dummy*>();
    ASSERT(data.type_name.size() > 4 && data.type_name[3] == '.');
    data.type_name = &data.type_name[4];

    size_t colon = data.type_name.find_last_of(':');
    if (colon != data.type_name.npos) {
      data.type_name = &data.type_name[colon + 1];
    }
  }

  return data;
}

int ::sol::stack::pusher<scr::DynamicObject::Userdata>::push(
    lua_State* L, const scr::DynamicObject::Userdata& things) {
  return pusher<sol::table>::push(
      L, things.object_creator
             ? things.object_creator(L, things.data)
             : scr::detail::State::create_userdata_pointer(things.type_name, things.data));
}

}  // namespace stack
}  // namespace sol
