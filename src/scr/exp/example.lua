main = function()
  local person = Person.new("1")

  local int_name = str_to_int(person:get_name())

  if(int_name == 1) then
    return function()
      return person
    end
  else
    return function()
      person = Person.new("2")
      return person
    end
  end

end