#include "scr/constructors.hpp"  // for Constructors
#include <bse/reflect.hpp>       // for IMPL_REFLECT_1, REFLECT
#include <scr/detail/utility.hpp>
#include <scr/dynamic_object.hpp>  // for DynamicObject, DynamicObject::User...
#include <scr/object.hpp>          // for Object
#include <scr/scr.tcc>             // for expose_type
#include <scr/script.hpp>          // for Script
using namespace scr;
using namespace detail;

struct FooBase {
  void base() {}
  void derived() {}
  void base_const() {}
  void derived_const() const {}
};

struct Foo : public FooBase {
  int a = 1;
  std::vector<float> floats;

  void merp(const int& a, std::vector<float>& b) {}
  void flerp(const glm::mat4& mat) {}
  void derived() {}
  void derived_const() const {}

  std::vector<std::unique_ptr<int>> flerps;
};
struct Bar {
  Bar() = default;
  Bar(const Bar&) = delete;
  Foo foo;
};

template <typename T, typename = std::void_t<>>
struct is_copyable : std::true_type {};

template <typename T>
struct is_copyable<T, std::void_t<decltype(std::declval<T>() = std::declval<T>())>>
    : std::false_type {};

template <typename T>
constexpr bool is_copyable_v = is_copyable<T>::value;

// static_assert(!is_copyable_v<Bar>);
// static_assert(is_copyable_v<FooBase>);
// static_assert(is_copyable_v<float>);

REFLECT((Foo), a, floats, merp, flerp, base, derived, base_const, derived_const, flerps)
REFLECT((Bar))

std::unique_ptr<int> baba() { return std::unique_ptr<int>(); }

#include "../../cor/include/cor/script_type.hpp"
#include <bse/edit.hpp>

// void edit(std::string path, scr::DynamicObject object) {
//   if (object.is_number()) {
//     int a = object;
//     int flerp = 0;
//   } else if (object.is_userdata()) {
//     std::vector<float> floats = object;
//     int flerp = 0;
//   }
// }

int main() {
  auto aaaa = baba();
  cor::ScriptType::expose_as_type<Bar>();

  // scr::expose_type<Foo>("Foo", scr::Constructors<Foo()>(), "a", &Foo::a, "floats", &Foo::floats);
  // std::string type_name = State::get_type_name<Foo>();
  // scr::expose_function("edit", edit);

  // bse::Asset<Script> script("assets/script/editor/edit.lua");

  // std::function<void(std::string path, scr::DynamicObject obj)> script_edit =
  //     script->return_value()["edit"];

  // Foo foo;
  // foo.a = 5;
  // foo.floats = {1, 2, 3};
  // script_edit("foo", foo);

  return 0;
}
////////////
