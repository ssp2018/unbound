#pragma once

#ifndef COR_SCRIPT_TYPE_HPP
#define COR_SCRIPT_TYPE_HPP

#include "scr/constructors.hpp"
#include <bse/reflect.hpp>
#include <scr/scr.hpp>
namespace cor {
class Entity;
}

namespace scr {
class Object;
}

namespace cor {

// Exposes a type to the scripts
class ScriptType {
 public:
  // Expose type to script as component.
  template <typename T>
  static bool expose_as_component();

  // Expose type to script as userdata
  template <typename T, typename Indices = std::make_index_sequence<bse::meta<T>::num_fields * 2>>
  static bool expose_as_type();

  // Load a component from a script object and
  // attach it to an entity. Return false on failure.
  static bool load_component(const std::string& component_name, const scr::Object& component,
                             Entity& entity);

  static void destroy();

 private:
  // Load reflectable component field
  template <typename T>
  static bool load_reflectable(T&& t, const scr::Object& object);

  // Expand metafield to name and member_ptr
  template <typename T, size_t I>
  static auto expand_field();

  // Helper for expose_as_type
  template <typename T, size_t... I>
  static void expose_as_type_impl(std::index_sequence<I...>);

  // Load a component from an object and attach it to an entity.
  using ComponentLoader = std::function<bool(Entity& entity, const scr::Object& object)>;

  static std::map<std::string_view, ComponentLoader>* m_component_loaders;
};

///////////////////////////////////////////////////////////////////////////////
// impl ///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

template <typename T>
bool ScriptType::expose_as_component() {
  /* if (!m_component_loaders) {
     m_component_loaders = new std::map<std::string_view, ComponentLoader>();
   }

   static_assert(bse::meta<T>::is_defined, "Cannot set script type of non-reflectable type.");
   (*m_component_loaders)[bse::meta<T>::base_name] = [](Entity& entity, const scr::Object& object) {
     T component;
     bool is_successful = true;
     bse::for_each_field(component, [&is_successful, &component, &object](const auto& field) {
       if constexpr (!std::is_member_function_pointer_v<decltype(field.value)> &&
                     std::is_copy_assignable_v<decltype(field.value)>) {
         scr::Object field_object = object[field.name];
         using FieldType = std::remove_reference_t<decltype(field.value)>;

         if (field_object.is_null()) {
           field.value = std::move(FieldType());
           return;
         }

         if constexpr (bse::meta<FieldType>::is_defined) {
           if (!load_reflectable(field.value, field_object)) {
             is_successful = false;
             LOG(WARNING) << "Failed to load reflectable component field '"
                          << std::string(field.name) << "'. See previous warnings for details.\n";
           }
         } else {
           component.*field.member_ptr = std::move(field_object.get<FieldType>());
           if (!field_object.is<FieldType>()) {
             is_successful = false;
             LOG(WARNING) << "Failed to load non-reflectable component field '"
                          << std::string(field.name) << "'. Got '" << field_object.str() << "'.\n";
           }
         }
       }
     });

     entity = std::move(component);

     return is_successful;
   };*/

  expose_as_type<T>();

  return true;
}

template <typename T, size_t I>
auto ScriptType::expand_field() {
  if constexpr (I % 2 == 0) {
    return bse::meta_field<T, I / 2>::name;
  } else {
    return bse::meta_field<T, I / 2>::member_ptr;
  }
}

template <typename T, size_t... I>
void ScriptType::expose_as_type_impl(std::index_sequence<I...>) {
  scr::expose_type<T>(std::string(bse::meta<T>::base_name), scr::Constructors<T()>(),
                      expand_field<T, I>()...);
}

template <typename T, typename Indices>
bool ScriptType::expose_as_type() {
  expose_as_type_impl<T>(Indices{});
  return true;
}

#define UNBOUND_SCRIPT_TYPE(t, ...)                                            \
  namespace bse::detail::script_type_instantiation::STRIP_PARENS(t) {          \
    static bool flag = ::cor::ScriptType::expose_as_type<::STRIP_PARENS(t)>(); \
  }

#define UNBOUND_SCRIPT_COMPONENT(t, ...)                                            \
  namespace bse::detail::script_type_instantiation::STRIP_PARENS(t) {               \
    static bool flag = ::cor::ScriptType::expose_as_component<::STRIP_PARENS(t)>(); \
  }

}  // namespace cor

#endif