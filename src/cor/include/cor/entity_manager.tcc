#include "bse/assert.hpp"
#include "cor/component_id.hpp"
#include "cor/component_name.hpp"
#include "cor/component_store.hpp"
#include "cor/key.hpp"
#include <bse/serialize.hpp>
#include <cor/entity_handle.hpp>
#include <cor/entity_manager.hpp>

namespace cor {
template <typename T>
T& EntityManager::attach(EntityHandle handle) {
  ASSERT(is_valid(handle));
  //
  auto component_id = ComponentID<T>::get_id();

  auto added_component_index = m_component_stores[component_id]->add();

  Register& reg = get_register(handle);
  if (reg.key.has(component_id)) {
    return get<T>(handle);
  }
  reg.key.include<T>();
  reg.component_index_map[component_id] = added_component_index;

  return *static_cast<T*>(m_component_stores[component_id]->get(added_component_index));
}

template <typename T>
void EntityManager::detach(EntityHandle handle) {
  ASSERT(is_valid(handle));
  //
  auto component_id = ComponentID<T>::get_id();

  Register& reg = get_register(handle);
  if (!reg.key.has(component_id)) {
    return;
  }
  reg.key.exclude<T>();
  auto removed_component_index = reg.component_index_map[component_id];
  // reg.component_index_map.erase(component_id);
  reg.component_index_map[component_id] = uint16_t(-1);

  m_component_stores[component_id]->remove(removed_component_index);
}

template <typename T>
T& EntityManager::get(EntityHandle handle) {
  ASSERT(is_valid(handle));
  //
  auto component_id = ComponentID<T>::get_id();

  Register& reg = get_register(handle);

  ASSERT(reg.key.has<T>()) << "This entity did not have a component of type: "
                           << ComponentName::get_name<T>();

  auto index = reg.component_index_map[component_id];

  return *static_cast<T*>(m_component_stores[component_id]->get(index));
}

template <typename T>
const T& EntityManager::get(EntityHandle handle) const {
  ASSERT(is_valid(handle));
  //
  auto component_id = ComponentID<T>::get_id();

  const Register& reg = get_register(handle);

  ASSERT(reg.key.has<T>()) << "This entity did not have a component of type: "
                           << ComponentName::get_name<T>();

  auto index = reg.component_index_map[component_id];

  return *static_cast<const T*>(m_component_stores[component_id]->get(index));
}

template <typename T1, typename T2>
const std::vector<EntityManager::EntityHandleWithComponent<T1, T2>>
EntityManager::get_entities_with_components() const {
  std::vector<EntityManager::EntityHandleWithComponent<T1, T2>> entities;
  entities.reserve(128);

  auto component_id1 = ComponentID<T1>::get_id();
  auto component_id2 = ComponentID<T2>::get_id();
  Key key = Key::create<T1, T2>();
  for (auto& reg : m_registers) {
    if (register_has(reg, key)) {
      // EntityHandleWithComponent<T1, T2> handle_with_components;

      auto index1 = reg.component_index_map[component_id1];
      auto index2 = reg.component_index_map[component_id2];

      // handle_with_components.handle = reg.entity_handle;
      // handle_with_components.comp1 =
      //     *static_cast<const T1*>(m_component_stores[component_id1]->get(index1));
      // handle_with_components.comp2 =
      //     *static_cast<const T2*>(m_component_stores[component_id2]->get(index2));
      // entities.push_back(reg.entity_handle);
      entities.emplace_back(
          reg.entity_handle,
          *static_cast<const T1*>(m_component_stores[component_id1]->get(index1)),
          *static_cast<const T2*>(m_component_stores[component_id2]->get(index2)));
    }
  }
  return entities;
}

}  // namespace cor