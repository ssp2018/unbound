#include <cor/entity.hpp>
#include <cor/entity_manager.hpp>

namespace cor {

template <typename... Ts>
bool ConstEntity::has() const {
  //
  return has(Key::create<typename remove_cvref<Ts>::type...>());
}

template <typename T>
T& Entity::get() {
  //
  return m_man->get<typename remove_cvref<T>::type>(m_handle);
}

template <typename T>
const T& ConstEntity::get() const {
  //
  return m_man->get<typename remove_cvref<T>::type>(m_handle);
}

template <typename T>
Entity::operator T&() {
  //
  return get<T>();
}

template <typename T>
ConstEntity::operator const T&() const {
  //
  return get<T>();
}

template <typename T>
T* Entity::get_if() {
  //
  if (has<T>()) {
    return &m_man->get<typename remove_cvref<T>::type>(m_handle);
  }
  return nullptr;
}

template <typename T>
const T* ConstEntity::get_if() const {
  //
  if (has<T>()) {
    return &m_man->get<typename remove_cvref<T>::type>(m_handle);
  }
  return nullptr;
}

template <typename T>
Entity::operator T*() {
  //
  return get_if<T>();
}

template <typename T>
ConstEntity::operator const T*() const {
  //
  return get_if<T>();
}

template <typename T>
T& Entity::attach() {
  //
  static_assert(!std::is_same_v<T, ConstEntity>, "Cannot assign Entity = ConstEntity");
  return m_man->attach<typename remove_cvref<T>::type>(m_handle);
}

template <typename T>
T& Entity::attach(T&& component) {
  //
  T& added_comp = attach<typename remove_cvref<T>::type>();
  if constexpr (std::is_rvalue_reference_v<decltype(component)>) {
    added_comp = std::move(std::forward<T>(component));
  } else {
    added_comp = component;
  }
  return added_comp;
}

template <typename T>
Entity& Entity::operator=(T&& component) {
  attach(std::forward<decltype(component)>(component));
  return *this;
}

template <typename T>
void Entity::detach() {
  //
  m_man->detach<typename remove_cvref<T>::type>(m_handle);
}

}  // namespace cor
