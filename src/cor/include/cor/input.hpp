#pragma once

#ifndef COR_INPUT_HPP
#define COR_INPUT_HPP

#include "cor/event.hpp"
namespace cor {
class EventManager;
}
namespace gfx {
class Window;
}

namespace inp {

// Handling user input
class InputManager {
 public:
  InputManager(gfx::Window& w, cor::EventManager& e);

  // Poll menu input
  void poll_menu_input();

  // Poll user input
  void poll_input();

  // Poll editor input
  void poll_editor_input();

  // Lock mouse
  void lock_mouse();

  // Unlock mouse
  void unlock_mouse();

  // Poll gamepad
  void gamepad();

 private:
  // Struct to define a Key
  struct Key {
    int bind;
    int mode = -1;
    bool pressed = false;
    bool clicked = false;
    bool released = false;
  };

  // Define all used Keys
  Key m_key_move_forward;
  Key m_key_move_left;
  Key m_key_move_back;
  Key m_key_move_right;
  Key m_key_jump;
  Key m_key_run;
  Key m_key_dash;
  Key m_key_cancel;
  Key m_key_draw;
  Key m_key_aim;
  Key m_key_piercing_arrow;
  Key m_key_explosive_arrow;
  Key m_key_rope_arrow;
  Key m_key_arrow_up;
  Key m_key_arrow_left;
  Key m_key_arrow_down;
  Key m_key_arrow_right;
  Key m_key_number_one;
  Key m_key_number_two;
  Key m_key_number_three;
  Key m_key_number_four;
  Key m_key_number_five;
  Key m_key_number_six;
  Key m_key_number_seven;
  Key m_key_number_eight;
  Key m_key_number_nine;
  Key m_key_number_zero;
  Key m_key_escape;
  Key m_key_toggle_mouse_input;
  Key m_key_refresh_assets;
  Key m_key_toggle_bse_edit;
  Key m_key_change_camera;
  Key m_key_toggle_debug_camera_movement;
  Key m_key_plus;
  Key m_key_minus;
  Key m_key_pause;
  Key m_key_editor;
  Key m_key_quick_save;
  Key m_key_quick_load;
  Key m_key_start_recording;
  Key m_key_stop_recording;
  Key m_key_start_playback;

  // Editor keys
  Key m_editor_q;
  Key m_editor_return;
  Key m_editor_w;
  Key m_editor_a;
  Key m_editor_s;
  Key m_editor_d;
  Key m_editor_shift;
  Key m_editor_space;
  Key m_editor_e;
  Key m_editor_c;
  Key m_editor_v;
  Key m_editor_p;
  Key m_editor_i;
  Key m_editor_ctrl;
  Key m_editor_lmb;
  Key m_editor_rmb;
  Key m_editor_r;
  Key m_editor_x;
  Key m_editor_y;
  Key m_editor_z;
  Key m_editor_g;
  Key m_editor_f;
  Key m_editor_delete;
  Key m_editor_left_click;
  Key m_editor_quit;
  Key m_editor_toggle_bse_edit;
  Key m_editor_play;

  // Mouse movement variables
  int m_mouse_origin_x;
  int m_mouse_origin_y;
  double m_mouse_x_pos;
  double m_mouse_y_pos;
  double m_mouse_delta_x;
  double m_mouse_delta_y;
  double m_mouse_sensitivity = 0.002;

  // Gamepad variables
  double m_pad_sensitivity = 0.1;
  bool m_pad_buttons[14];
  bool m_pad_aim = false;
  bool m_pad_drawing = false;
  int m_pad_inverted = -1;  // 1 if inverted, otherwise -1

  // Movement variables
  float m_forward = 0;
  float m_right = 0;

  bool m_aiming = false;
  bool m_drawing = false;

  bool m_editor_camera = false;

  // Event manager reference
  cor::EventManager& m_em;

  // Window reference
  gfx::Window& m_w;

  // Write to console
  bool input_debug = false;

  // Creates Keys for polling
  void bind_keys();
  // Bind editor keys
  void bind_editor_keys();
  // Poll a Key
  void poll_key(Key* k);
  // Poll Key for press only
  void poll_press(Key* k);
  // Poll mouse button
  template <typename Event1T, typename Event2T>
  void poll_mouse_button(Key* k, const Event1T& startEvent, const Event2T& stopEvent);
  // Poll mouse movement
  void poll_mouse_movement();

  // Send mouse movement event
  void send_mouse_movement_event();

  // Send mouse movement events during normal screen mouse movements
  void send_editor_mouse_movement_event();

  // Send move event
  void send_move_event();

  // Toggle input debug
  void toggle_input_debug();
};

}  // namespace inp

#endif  // COR_INPUT_HPP