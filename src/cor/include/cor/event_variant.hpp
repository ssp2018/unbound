#pragma once

#ifndef COR_EVENT_VARIANT_HPP
#define COR_EVENT_VARIANT_HPP

////////////////////////////////
// DO NOT INCLUDE THIS FILE
////////////////////////////////
// If you think you need to include this
// file, ask MH first.

#include <cor/event.hpp>
struct Event {
  std::variant<EVENT_TYPE_LIST> data;
};



#endif  // COR_EVENT_VARIANT_HPP