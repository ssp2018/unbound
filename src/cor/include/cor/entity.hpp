#pragma once

#ifndef COR_ENTITY_HPP
#define COR_ENTITY_HPP

#include "cor/component_id.hpp"
#include "cor/entity_handle.hpp"
#include "cor/key.hpp"
namespace cor {
class EntityManager;
}  // namespace cor

namespace cor {

// Forward declarations
class Key;
class EntityManager;

// The const-interface for most entity-system interactions

class ConstEntity {
 public:
  ConstEntity();
  ConstEntity(const ConstEntity& other);
  virtual ~ConstEntity() = default;

  // Assignment
  ConstEntity& operator=(ConstEntity&& other);
  ConstEntity& operator=(const ConstEntity& other);

  // returns true iff the entity has all specified components
  template <typename... Ts>
  bool has() const;
  bool has(Key key) const;

  // Returns a reference to the specified component
  template <typename T>
  const T& get() const;
  template <typename T>
  operator const T&() const;

  // Returns a pointer to the specified component if it exists
  template <typename T>
  const T* get_if() const;
  template <typename T>
  operator const T*() const;

  // Returns true iff the handle is still valid
  bool is_valid() const;

  // Returns a key with all held components
  Key get_key() const;

  // Returns the actual handle representing this entity
  EntityHandle get_handle() const;

  const void* raw_get(std::string_view component_name) const;

 protected:
  ConstEntity(const EntityManager* man, EntityHandle handle);

  EntityHandle m_handle;
  EntityManager* m_man = nullptr;

  friend class EntityManager;
};

// The interface for most entity-system interactions
class Entity : public ConstEntity {
 public:
  Entity();
  Entity(const Entity& other);
  virtual ~Entity() = default;

  // Assignment
  Entity& operator=(Entity&& other);
  Entity& operator=(Entity& other);
  Entity& operator=(const ConstEntity& other) = delete;

  // Destroys the held entity. From now on, all calls to is_valid will return false
  void destroy();

  // Returns a reference to the specified component
  template <typename T>
  T& get();
  template <typename T>
  operator T&();

  // Returns a pointer to the specified component if it exists
  template <typename T>
  T* get_if();
  template <typename T>
  operator T*();

  void raw_set(std::string_view component_name, void* data);
  void* raw_attach(std::string_view component_name);
  void raw_detach(std::string_view component_name);

  // Attaches the specified component to the entity
  template <typename T>
  T& attach();
  template <typename T>
  T& attach(T&& component);
  template <typename T>
  Entity& operator=(T&& component);

  // Detaches the specified component from the entity
  template <typename T>
  void detach();

 private:
  Entity(EntityManager* man, EntityHandle handle);

  friend class EntityManager;
};

}  // namespace cor

#endif  // COR_ENTITY_HPP