#pragma once

#ifndef COR_EVENT_MANAGER_HPP
#define COR_EVENT_MANAGER_HPP

#include "bse/reflect.hpp"
#include <bse/serialize.hpp>
#include <cor/cor.hpp>

struct Event;

namespace cor::event_manager::detail {
// The number of separate event buffers to keep
static const size_t m_num_buffers = 2;
}  // namespace cor::event_manager::detail
extern template class std::vector<std::tuple<bool, std::function<void(const Event*)>, size_t>>;
extern template class std::unique_ptr<
    std::array<std::vector<Event>, cor::event_manager::detail::m_num_buffers>>;

namespace cor {
template <class T, class Variant>
struct Index;
}  // namespace cor

#define EVENT_REGISTER_MEMBER_FUNCTION_I(event_type, event_manager, member_func) \
  (event_manager).register_handler<event_type>([this](const event_type* event) { \
    (member_func)(event);                                                        \
  })

#define EVENT_REGISTER_MEMBER_FUNCTION_E(event_type, event_manager, instance, member_func) \
  (event_manager).register_handler<event_type>([&instance](const event_type* event) {      \
    (instance).member_func(event);                                                         \
  })

namespace cor {
using namespace event_manager::detail;
// Template classes for generating indices in event map
template <class T, class Variant>
struct Index;
template <class T, class... Types>
struct Index<T, std::variant<T, Types...>> {
  static constexpr std::size_t value = 0;
};
template <class T, class U, class... Types>
struct Index<T, std::variant<U, Types...>> {
  static constexpr std::size_t value = 1 + Index<T, std::variant<Types...>>::value;
};

// Handles sending of events to interested listeners
// Events are defined in cor/event.hpp
class EventManager {
 public:
  // Small class returned when registering a listening function
  // When removing a listener, the returned ListenID is used
  // The destructor automatically removes the listener
  class ListenID {
    friend class EventManager;

   public:
    virtual ~ListenID() { destroy(); };
    ListenID() : m_em(nullptr), m_ID(0){};
    ListenID(ListenID& other) = delete;
    ListenID& operator=(const ListenID& other) = delete;
    ListenID(ListenID&& other) {
      move_from(std::forward<decltype(other)>(other));  //
    };
    ListenID& operator=(ListenID&& other) {
      destroy();
      move_from(std::forward<decltype(other)>(other));
      return *this;
    };

   private:
    ListenID(EventManager* em, size_t ID) : m_em(em), m_ID(ID){};

    // Moves 'other' into this
    void move_from(ListenID&& other) {
      if (&other != this) {
        m_em = other.m_em;
        other.m_em = nullptr;

        m_ID = other.m_ID;
        other.m_ID = 0;
      }
    }

    // Cleans up resources
    void destroy() {
      if (m_em) {
        m_em->remove_handler((*this));
      }
    }
    EventManager* m_em;
    size_t m_ID;
  };

  EventManager();
  EventManager(const EventManager& other) = delete;
  EventManager(EventManager&& other) = delete;
  virtual ~EventManager();

  EventManager& operator=(const EventManager& other) = delete;
  EventManager& operator=(EventManager&& other) = delete;

  // Register a function object to listen to events of type 'EventT'
  // The returned ListenID needs to be used to remove the listener
  template <typename EventT>
  ListenID register_handler(const std::function<void(const EventT*)>& handler);

  // Register a function object to listen all events
  // The returned ListenID needs to be used to remove the listener
  ListenID register_omni_handler(const std::function<void(const Event*)>& handler);

  // Removes the listener whose allocation returned the ID object
  void remove_handler(ListenID& ID);

  // Add an event to send to listeners. Events are sent during the next update cycle
  // The event is copied and stored in the EventManager
  template <typename T>
  void send_event(const T& e);

  // Modifies the state of a handler
  void enable_handler(ListenID& ID);
  void disable_handler(ListenID& ID);
  void toggle_handler(ListenID& ID);

  // Sends events stored in next event buffer
  void update();

  // Returns the number of total registered listeners
  size_t get_total_listeners();

  size_t get_num_events() const;

  std::vector<size_t> get_event_indices() const;

  CLASS_SERFUN((EventManager)) {
    size_t num_handlers = m_handlers.size();
    SERIALIZE(num_handlers);

    for (auto& event_type : m_handlers) {
      // Event ID
      SERIALIZE(event_type.first);

      size_t num_events = event_type.second.size();
      SERIALIZE(num_events);

      for (auto& handler : event_type.second) {
        // Serialize enabled bool
        SERIALIZE(std::get<0>(handler));
      }
    }

    size_t num_omni_handlers = m_omni_handlers.size();
    SERIALIZE(num_omni_handlers);

    for (auto& handler : m_omni_handlers) {
      // Serialize enabled bool
      SERIALIZE(std::get<0>(handler));
    }
  }

  CLASS_DESERFUN((EventManager)) {
    size_t num_handlers = 0;
    DESERIALIZE(num_handlers);

    num_handlers = std::min(num_handlers, m_handlers.size());

    for (size_t i = 0; i < num_handlers; i++) {
      // Event ID
      size_t event_type = 0;
      DESERIALIZE(event_type);

      size_t num_events = 0;
      DESERIALIZE(num_events);

      if (m_handlers.count(event_type)) {
        num_events = std::min(num_events, m_handlers.at(event_type).size());
      }

      // Deserialize enabled bools
      for (size_t j = 0; j < num_events; j++) {
        bool enabled = true;
        DESERIALIZE(enabled);

        if (m_handlers.count(event_type)) {
          auto& [ena, func, size] = m_handlers.at(event_type)[j];
          ena = enabled;
        }
      }
    }

    size_t num_omni_handlers = 0;
    DESERIALIZE(num_omni_handlers);

    num_omni_handlers = std::min(num_omni_handlers, m_omni_handlers.size());

    // Deserialize enabled bools
    for (size_t i = 0; i < num_omni_handlers; i++) {
      auto& [ena, func, size] = m_omni_handlers[i];
      DESERIALIZE(ena);
    }
  }

 private:
  // Current buffer used
  unsigned m_frame_ind = 0;

  size_t m_next_ID;

  // Function objects organized according to event listened to
  // An ID is stored along with every listener, used for removal
  std::map<size_t, std::vector<std::tuple<bool, std::function<void(const void*)>, size_t>>>
      m_handlers;

  // Stores function objects registered to listen to all events
  std::vector<std::tuple<bool, std::function<void(const Event*)>, size_t>> m_omni_handlers;

  // Stores events awaiting sending
  std::unique_ptr<std::array<std::vector<Event>, m_num_buffers>> m_events;
};

}  // namespace cor

#endif  // COR_EVENT_MANAGER_HPP