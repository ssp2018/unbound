#pragma once

#ifndef COR_SYSTEM_ID_PROXY_HPP
#define COR_SYSTEM_ID_PROXY_HPP

#include "cor/system_manager.hpp"

namespace cor {
class SystemManager;
}

namespace cor {

// Provides an interface for retrieving the index of a system without exposing the entire
// SystemManager
class SystemIDProxy {
 public:
  SystemIDProxy(SystemManager* system_mgr = nullptr);

  // Returns the index of the given system in the system manager
  template <typename T>
  std::string get_name() const;

 private:
  SystemManager* m_system_mgr;
};

template <typename T>
std::string SystemIDProxy::get_name() const {
  return m_system_mgr->get_name<T>();  //
}

//
}  // namespace cor
#endif  // COR_SYSTEM_ID_PROXY_HPP
