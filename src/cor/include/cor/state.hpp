#pragma once

#ifndef COR_STATE_HPP
#define COR_STATE_HPP

#include <aud/audio_manager.hpp>
#include <cor/frame_context.hpp>
#include <cor/input.hpp>
#include <cor/system_manager.hpp>
#include <gfx/window.hpp>

namespace cor {

class Game;

// The base for all game-related states
class State {
 public:
  State(cor::Game& game, gfx::Window& window, aud::AudioManager& audio_mgr);
  virtual ~State(){};

  // Initializes the generic necessities for states
  virtual void initialize();

  // Updates the context and systems. Returns true if the state allows the underlying state to
  // update.
  virtual bool update(float dt);

  // [Semi-Obsolete] Commits state related visuals to the screen. Returns true if the state allows
  // the underlying state to display.
  virtual bool display();

  const cor::SystemManager& get_system_manager() const;

 protected:
  FrameContext m_context;

  gfx::Window& m_window;
  cor::Game& m_game;

  inp::InputManager m_input_mgr;
  cor::SystemManager m_system_mgr;

  cor::EventManager::ListenID m_refresh_listener;
  cor::EventManager::ListenID m_system_enable_listener;
  cor::EventManager::ListenID m_system_disable_listener;
  cor::EventManager::ListenID m_system_toggle_listener;
};
}  // namespace cor
#endif  // COR_STATE_HPP
