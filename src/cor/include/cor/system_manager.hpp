#pragma once

#ifndef COR_SYSTEM_MANAGER_HPP
#define COR_SYSTEM_MANAGER_HPP

#include "cor/system.hpp"
#include <bse/assert.hpp>
#include <bse/task_handle.hpp>
#include <ext/ext.hpp>

namespace cor {
struct FrameContext;
}

namespace cor {
struct FrameContext;

// Class for registering and updating systems.
// May be used to simplify threading later.
// May also provide ease when/if the ability to enable/disable systems becomes necessary.
class SystemManager {
 public:
  struct ActiveFingerprint {
   public:
   private:
    std::map<std::string, bool> active_map;
    friend class SystemManager;
  };

 public:
  SystemManager(FrameContext& context);
  ~SystemManager(){};

  // Adds a system of type T and initializes it through the constructor with SArgs.
  template <typename T, typename... SArgs>
  void add_system(SArgs&&... s_args);

  template <typename T, typename... SArgs>
  void add_disabled_system(SArgs&&... s_args);

  // Runs the read-only update method in each system
  void read_update();

  // Runs the write-only update method in each system
  void write_update();

  // Runs write_update_late for each system
  void write_update_late();

  // // Returns the index of the given system
  // template <typename T>
  // uint32_t get_index() const;

  // Returns the string identifier of the given system
  template <typename T>
  std::string get_name();

  // Enables a system to run in the next update
  void enable_system(std::string id);

  // Disables a system from running in the next update
  void disable_system(std::string id);

  // Toggles a system from running in the next update
  void toggle_system(std::string id);

  // Disables all systems
  void disable_all_systems();

  // Returns a map of bools representing which systems are active and which are not.
  ActiveFingerprint get_active_fingerprint() const;

  // Restores the system manager to a previous point in terms of active systems.
  void set_active_fingerprint(const ActiveFingerprint& af);

  CLASS_SERFUN((SystemManager)) {
    SERIALIZE(m_systems);  //
  }
  CLASS_DESERFUN((SystemManager)) {
    DESERIALIZE(m_systems);  //
  }

  struct Diagnostics {
    float read_time;
    float write_time;
    std::unordered_map<std::string, float> system_read_times;
    std::unordered_map<std::string, float> system_write_times;
  };

  Diagnostics compile_diagnostics() const;

 private:
  // std::vector<std::pair<bool, std::unique_ptr<System>>> m_systems;
  std::map<std::string, std::pair<bool, std::unique_ptr<System>>> m_systems;
  float m_read_time;
  float m_write_time;
  std::unordered_map<std::string, float> m_system_read_times;
  std::unordered_map<std::string, float> m_system_write_times;
  FrameContext& m_context;

  // moved from cpp
  struct Task {
    bse::TaskHandle handle;
    std::unique_ptr<float> dt;
    std::string name;
  };

  std::vector<SystemManager::Task> m_tasks;
};

template <typename T, typename... SArgs>
void SystemManager::add_system(SArgs&&... s_args) {
  //
  // static bool initialized = false;

  // ASSERT(!initialized) << "May not register the same system twice!";

  static_assert(std::is_base_of_v<System, T>, "The registered system must derive from System.");
  // static_assert(has_system_read_v<T>,
  //               "The registered system did not have a proper read_update method!");
  // static_assert(has_system_write_v<T>,
  //               "The registered system did not have a proper write_update method!");

  // auto index = get_index<T>();
  // if (index >= m_systems.size()) {
  //   m_systems.resize(index + 1);
  // }

  std::string id = get_name<T>();
  auto found = m_systems.find(id);
  ASSERT(found == m_systems.end()) << "May not register the same system twice!";

  m_systems[id] = {true, std::make_unique<T>(std::forward<SArgs>(s_args)...)};

  // initialized = true;
}

template <typename T, typename... SArgs>
void SystemManager::add_disabled_system(SArgs&&... s_args) {
  //
  add_system<T>(std::forward<SArgs>(s_args)...);
  disable_system(get_name<T>());
}

template <typename T>
std::string SystemManager::get_name() {
  //
  return sol::detail::ctti_get_type_name<T>();
}

// template <typename T>
// uint32_t SystemManager::get_index() const {
//   //
//   static uint32_t s_id = m_systems.size();
//   return s_id;
// }

}  // namespace cor
#endif  // COR_SYSTEM_MANAGER_HPP
