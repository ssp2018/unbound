#pragma once

#ifndef UNBOUND_UNBOUND_COMPONENT_HPP
#define UNBOUND_UNBOUND_COMPONENT_HPP

#include <bse/reflect.hpp>
#include <cor/component_register.hpp>
#include <cor/script_type.hpp>

#define UNBOUND_COMPONENT_EXPLICIT(...)         \
  EXPAND(REFLECT(__VA_ARGS__))                  \
  EXPAND(UNBOUND_SCRIPT_COMPONENT(__VA_ARGS__)) \
  EXPAND(REGISTER_COMPONENT(__VA_ARGS__))

#endif  // UNBOUND_UNBOUND_COMPONENT_HPP