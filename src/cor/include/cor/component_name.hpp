#pragma once

#ifndef COR_COMPONENT_NAME_HPP
#define COR_COMPONENT_NAME_HPP

#include <cor/component_id.hpp>

namespace cor {

// Represents and stores a given name for a component.
class ComponentName {
 public:
  // Specifies the name to store. (May only be called once).
  template <typename T>
  static void give_name(std::string_view name);

  // Retrieves the stored name based on either type or a previously assigned ID in ComponentID.
  template <typename T>
  static std::string_view get_name();
  static std::string_view get_name(const ComponentIDInteger id);

  // Get names of all registered components.
  static std::vector<std::string_view> get_registered_names();

  // Retrieves a previously assigned ID in ComponentID by name.
  static ComponentIDInteger get_id(std::string_view name);

 private:
  static std::array<std::string_view, UNBOUND_MAX_COMPONENTS>& get_names();
};

template <typename T>
void ComponentName::give_name(std::string_view name) {
  static_assert(is_component_v<T>,
                "The registered type does not fulfill the requirements for is_component!");

  get_names()[ComponentID<T>::get_id()] = name;
}

template <typename T>
std::string_view ComponentName::get_name() {
  static_assert(is_component_v<T>,
                "The registered type does not fulfill the requirements for is_component!");

  return get_names().at(ComponentID<T>::get_id());
}

}  // namespace cor
#endif  // COR_COMPONENT_NAME_HPP
