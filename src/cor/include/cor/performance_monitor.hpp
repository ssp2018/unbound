#ifndef COR_PERFORMANCE_MONITOR_HPP
#define COR_PERFORMANCE_MONITOR_HPP

#pragma once

// #include <psapi.h>
// #include <string>

namespace cor {

float ram_usage();

}  // namespace cor

#endif