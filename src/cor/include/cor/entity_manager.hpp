#pragma once

#ifndef COR_ENTITY_MANAGER_HPP
#define COR_ENTITY_MANAGER_HPP

#include "cor/component_id.hpp"
#include "cor/component_name.hpp"
#include "cor/component_store.hpp"
#include "cor/key.hpp"
#include <bse/serialize.hpp>
#include <cor/entity_handle.hpp>

namespace cor {
class Entity;
class ConstEntity;
// Used to iterate component IDs
// namespace detail {
// static int s_id = 0;
// }

// The class responsible for managing the lifetimes of all entities and their components.
// It is through this class that entities are created, destroyed, and given components.
// The "Entity" class also provides a more convenient interface to this class.
class EntityManager {
 public:
  EntityManager();
  EntityManager(const EntityManager& other) = delete;

  EntityManager& operator=(EntityManager&& other) = default;

  // Returns a fresh EntityHandle.
  EntityHandle create_entity();

  // Returns true or false if handle is valid or invalid, respectively
  bool is_valid(EntityHandle handle) const;

  // Destroys the underlying entity (and all components) related to the given handle
  void destroy_entity(EntityHandle handle);

  // Destroy all entities.
  void clear();

  // Registers a type as a component that can be used in the entity manager
  // template <typename T>
  // constexpr void register_component(std::string_view name);
  template <typename T1, typename T2>
  struct EntityHandleWithComponent {
    EntityHandle handle;
    const T1& comp1;
    const T2& comp2;

    EntityHandleWithComponent(EntityHandle handle, const T1& comp1, const T2& comp2)
        : handle(handle), comp1(comp1), comp2(comp2) {}
  };

  template <typename T1, typename T2>
  const std::vector<EntityHandleWithComponent<T1, T2>> get_entities_with_components() const;

  // Returns a more convenient interface for an entity
  Entity get_entity(EntityHandle handle);
  const ConstEntity get_entity(EntityHandle handle) const;
  std::vector<EntityHandle> get_entities(Key key);
  const std::vector<EntityHandle> get_entities(Key key) const;
  std::vector<EntityHandle> get_entities();

  CLASS_SERFUN((EntityManager)) {
    SERIALIZE(self.m_free_handles);
    SERIALIZE(self.m_registers);
    SERIALIZE(self.m_component_stores);
  }
  CLASS_DESERFUN((EntityManager)) {
    DESERIALIZE(self.m_free_handles);
    DESERIALIZE(self.m_registers);
    DESERIALIZE(self.m_component_stores);
  }
  // CLASS_DESERFUN() {
  //   DESERIALIZE(self.m_free_handles);
  //   END_DESERFUN();
  // }

  // inline ::bse::SerializedData serialize() {
  //   ::bse::SerializedData data;
  //   impl_serialize(*this, data);
  //   return data;
  // }
  // inline void impl_serialize(const EntityManager& self, ::bse::SerializedData& data) {
  //   data += ::bse::serialize(self.m_free_handles);
  // }

 private:
  struct Register {
    EntityHandle entity_handle;
    Key key;
    std::array<uint16_t, UNBOUND_MAX_COMPONENTS> component_index_map;
    // std::unordered_map<ComponentIDInteger, uint16_t> component_index_map;

    CLASS_SERFUN((Register)) {
      SERIALIZE(self.entity_handle);
      SERIALIZE(self.key);
      SERIALIZE(self.component_index_map);
    }
    CLASS_DESERFUN((Register)) {
      DESERIALIZE(self.entity_handle);
      DESERIALIZE(self.key);
      DESERIALIZE(self.component_index_map);
    }
  };

  // Attaches a given component to the underlying entity related to the given handle
  template <typename T>
  T& attach(EntityHandle handle);
  void* raw_attach(EntityHandle handle, std::string_view component_name);
  void raw_set(EntityHandle handle, std::string_view component_name, void* data);

  // Detaches a given component to the underlying entity related to the given handle
  template <typename T>
  void detach(EntityHandle handle);
  void raw_detach(EntityHandle handle, std::string_view component_name);

  // Returns true iff the underlying entity has all the same components that are specified in key
  bool has(EntityHandle handle, Key key) const;

  // Returns the component belonging to the given handle
  template <typename T>
  T& get(EntityHandle handle);
  template <typename T>
  const T& get(EntityHandle handle) const;
  void* raw_get(EntityHandle handle, std::string_view component_name);

  // Returns the key belonging to the given handle
  Key get_key(EntityHandle handle) const;

  // Clears the key, map and all components at this place in the register
  void clear_register(Register& reg);

  // Returns true if the register's key matches the given key
  bool register_has(const Register& reg, Key key) const;

  // Returns the corresponding register
  Register& get_register(EntityHandle handle);
  const Register& get_register(EntityHandle handle) const;

  std::vector<EntityHandle> m_free_handles;
  std::vector<Register> m_registers;
  std::vector<std::unique_ptr<AbstractComponentStore>> m_component_stores;

  friend class Entity;
  friend class ConstEntity;
  // friend bse::SerializedData bse::serialize(const cor::EntityManager& self);
};

}  // namespace cor
#endif  // COR_ENTITY_MANAGER_HPP