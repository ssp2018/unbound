#pragma once

#ifndef COR_KEY_HPP
#define COR_KEY_HPP

#include "cor/component_id.hpp"

namespace cor {

// Represents a grouping of components that can be passed to the EntityManager for easy retrieval
class Key {
 public:
  // Returns a key initialized with the given template parameters.
  // May be empty to create an empty key.
  template <typename... Types>
  static Key create();

  bool operator==(const Key& other) const;

  // Includes the given component(s) into the bitfield
  template <typename First, typename Second, typename... Rest>
  void include();
  template <typename Last>
  void include();
  void include(ComponentIDInteger component_id);

  // Excludes the given component(s) from the bitfield
  template <typename First, typename Second, typename... Rest>
  void exclude();
  template <typename Last>
  void exclude();
  void exclude(ComponentIDInteger component_id);

  // Returns true if the given component(s) are in the bitfield
  template <typename First, typename Second, typename... Rest>
  bool has();
  template <typename Last>
  bool has();
  bool has(ComponentIDInteger component_id) const;

  // Returns the internal bitfield
  const ComponentFlags& get_bits() const;

 private:
  ComponentFlags m_bits;
};

template <typename... Types>
Key Key::create() {
  Key key;
  if constexpr (sizeof...(Types) > 0) {
    key.include<Types...>();
  }
  return key;
}

template <typename First, typename Second, typename... Rest>
void Key::include() {
  //
  include<First>();
  include<Second>();

  if constexpr (sizeof...(Rest) > 0) {
    include<Rest...>();
  }
}

template <typename Last>
void Key::include() {
  //
  m_bits.set(ComponentID<Last>::get_id());
}

template <typename First, typename Second, typename... Rest>
void Key::exclude() {
  //
  exclude<First>();
  exclude<Second>();

  if constexpr (sizeof...(Rest) > 0) {
    exclude<Rest...>();
  }
}

template <typename Last>
void Key::exclude() {
  //
  m_bits.reset(ComponentID<Last>::get_id());
}

template <typename First, typename Second, typename... Rest>
bool Key::has() {
  //
  bool result = has<First>();
  result = result && has<Second>();

  if constexpr (sizeof...(Rest) > 0) {
    result = result && has<Rest...>();
  }
  return result;
}

template <typename Last>
bool Key::has() {
  //
  return m_bits.test(ComponentID<Last>::get_id());
}

}  // namespace cor

#endif  // COR_KEY_HPP