#pragma once

#ifndef UNBOUND_UNBOUND_TYPE_HPP
#define UNBOUND_UNBOUND_TYPE_HPP

#include <bse/reflect.hpp>
#include <cor/script_type.hpp>

#define UNBOUND_TYPE_EXPLICIT(...) \
  EXPAND(REFLECT(__VA_ARGS__))     \
  EXPAND(UNBOUND_SCRIPT_TYPE(__VA_ARGS__))

#endif  // UNBOUND_UNBOUND_TYPE_HPP