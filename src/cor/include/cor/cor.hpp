#pragma once

#ifndef UNBOUND_CORE_HPP
#define UNBOUND_CORE_HPP

#ifdef UNBOUND_DO_EXPOSE_TYPES
#include <cor/unbound_component_explicit.hpp>
#include <cor/unbound_type_explicit.hpp>
#define UNBOUND_TYPE(...) EXPAND(UNBOUND_TYPE_EXPLICIT(__VA_ARGS__))
#define UNBOUND_COMPONENT(...) EXPAND(UNBOUND_COMPONENT_EXPLICIT(__VA_ARGS__))
#elif defined(UNBOUND_DO_REFLECT)
#include <bse/reflect.hpp>
#define UNBOUND_TYPE(...) EXPAND(REFLECT(__VA_ARGS__))

#define UNBOUND_COMPONENT(...) EXPAND(REFLECT(__VA_ARGS__))
#else
#define UNBOUND_TYPE(...)

#define UNBOUND_COMPONENT(...)
#endif

namespace scr {
class Object;
}

namespace cor {
class Entity;
class EntityManager;

// // Create entity from script object.
// void create_entity(Entity& entity, const scr::Object& object);

// // Create entities from script object.
// void load_scene(EntityManager& man, const scr::Object& object);

// // Create entities from script object loaded from file.
// // Path is relative to "assets/script/scene" without file
// // extension.
// // Example: "test", not "assets/script/scene/test.lua"
// void load_scene(EntityManager& man, const std::string& path);

}  // namespace cor

#endif  // UNBOUND_COR_HPP