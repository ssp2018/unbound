#pragma once

#ifndef COR_COMPONENT_ID_HPP
#define COR_COMPONENT_ID_HPP

#include "bse/assert.hpp"
#include "bse/flags.hpp"
#include "cor/component_traits.hpp"

#if !defined(UNBOUND_MAX_COMPONENTS)
#define UNBOUND_MAX_COMPONENTS 128
#endif

namespace cor {

// Utility to remove const and reference from a type T
template <typename T>
struct remove_cvref {
  typedef std::remove_cv_t<std::remove_reference_t<T>> type;
};

// Alias for easy use of the component bitmap
using ComponentFlags = bse::Flags<UNBOUND_MAX_COMPONENTS>;

// The internal ID type
using ComponentIDInteger = int;

// Represents and stores a unique ID for a given component.
template <typename T>
class ComponentID {
  static_assert(is_component_v<T>,
                "The registered type does not fulfill the requirements for is_component!");

  static_assert(!std::is_const_v<T>, "Should not register a const type as a component!");
  static_assert(!std::is_lvalue_reference_v<T>,
                "Should not register a reference type as a component!");

 public:
  // Specifies the ID to store. (May only be called once).
  static constexpr bool give_id(const ComponentIDInteger id);

  // Retrieves the stored ID.
  static constexpr ComponentIDInteger get_id();

 private:
  static ComponentIDInteger m_id;
};

template <typename T>
constexpr bool ComponentID<T>::give_id(const ComponentIDInteger id) {
  if (m_id != -1) {
    return false;
  }

  // if (m_id != -1) {
  //   LOG(FATAL) << "Component ID may only be set once! You may have registered a component
  //   twice!"; return;
  // }

  ASSERT(id < UNBOUND_MAX_COMPONENTS)
      << "Exceeded the maximum number of allowed components! (Assigned ID was greater than "
      << UNBOUND_MAX_COMPONENTS - 1 << ")";
  m_id = id;

  return true;
}

template <typename T>
constexpr ComponentIDInteger ComponentID<T>::get_id() {
  ASSERT(m_id != -1) << "This component was not registered!\nMake sure to register your components "
                        "using the UNBOUND_COMPONENT macro!";
  return m_id;
}

template <typename T>
ComponentIDInteger ComponentID<T>::m_id = -1;

}  // namespace cor
#endif  // COR_COMPONENT_ID_HPP
