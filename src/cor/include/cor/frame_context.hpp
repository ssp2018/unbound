#pragma once

#ifndef COR_CONTEXT_HPP
#define COR_CONTEXT_HPP

#include <ai/navmesh.hpp>
#include <aud/audio_manager.hpp>
#include <cor/entity_manager.hpp>
#include <cor/event.hpp>
#include <cor/event_manager.hpp>
#include <cor/system_id_proxy.hpp>
#include <phy/physics_manager.hpp>
namespace cor {

// Contains the result of a raycast
struct RayResult {
  RayResult() {}
  RayResult(glm::vec3 hit_point, glm::vec3 hit_normal, float fraction, cor::EntityHandle handle)
      : hit_point(hit_point), hit_normal(hit_normal), fraction(fraction), handle(handle) {}
  glm::vec3 hit_point;
  glm::vec3 hit_normal;

  // Entity handle of hit object
  cor::EntityHandle handle;

  // Indicates where along the ray the hit occured
  // 0 = at start of ray, 1 = at end of ray
  float fraction;
};

// Contains the result of a sphere sweep
struct SweepResult {
  SweepResult() : hit(false) {}
  SweepResult(glm::vec3 hit_point, glm::vec3 hit_normal, float fraction, cor::EntityHandle handle,
              bool hit)
      : hit_point(hit_point),
        hit_normal(hit_normal),
        fraction(fraction),
        handle(handle),
        hit(hit) {}
  glm::vec3 hit_point;
  glm::vec3 hit_normal;

  // Entity handle of hit object
  cor::EntityHandle handle;

  // Indicates where along the ray the hit occured
  // 0 = at start of ray, 1 = at end of ray
  float fraction;

  bool hit;
};

// Class exposing various system utilities to users
class FrameContextUtilities {
 public:
  // Cast a ray into the world and return info on objects hit
  // 'direction' will be normalized
  std::vector<RayResult> raycast(glm::vec3 origin, glm::vec3 direction, float length) {
    std::vector<RayResult> results_vector;
    if (m_physics) {
      btDiscreteDynamicsWorld::AllHitsRayResultCallback result =
          m_physics->raycast(origin, direction, length);

      if (result.m_collisionObjects.size() > 0) {
        results_vector.resize(result.m_collisionObjects.size());

        for (int i = 0; i < result.m_collisionObjects.size(); i++) {
          results_vector[i] =
              RayResult(phy::vc(result.m_hitPointWorld[i]), phy::vc(result.m_hitNormalWorld[i]),
                        result.m_hitFractions[i], result.m_collisionObjects[i]->getUserIndex());
        }
      }
    }
    return results_vector;
  }

  // Sweep a sphere into the world and return info on hit object
  SweepResult sphere_sweep(glm::vec3 origin, glm::vec3 direction, float length,
                           float sphere_radius) {
    SweepResult result;
    if (m_physics) {
      btDiscreteDynamicsWorld::ClosestConvexResultCallback sweep_result =
          m_physics->sphere_sweep(origin, direction, length, sphere_radius);

      if (sweep_result.hasHit())
        result =
            SweepResult(phy::vc(sweep_result.m_hitPointWorld),
                        phy::vc(sweep_result.m_hitNormalWorld), sweep_result.m_closestHitFraction,
                        sweep_result.m_hitCollisionObject->getUserIndex(), true);
    }
    return result;
  }

  // Set the physics manager to use for raycasting. Should only be called by PhysicsSystem
  void set_physics_manager(phy::PhysicsManager* physics_manager) { m_physics = physics_manager; }

  // get mouse picking data
  MousePickData get_picking_data(glm::ivec2 mouse_pos, glm::ivec2 window_size) {
    MousePickData pick_data;

    // if mouse_pos is outside the window, return an invalid result
    if (mouse_pos.x < 0 || mouse_pos.x > window_size.x || mouse_pos.y < 1 ||
        mouse_pos.y > window_size.y) {
      pick_data.entity_handle = -1;
      return pick_data;
    }
    gl::glBindFramebuffer(gl::GL_FRAMEBUFFER, m_mouse_fbo);

    // access picking coord
    gl::glPixelStorei(gl::GL_UNPACK_ALIGNMENT, 1);

    unsigned char data[4];
    gl::glReadPixels(mouse_pos.x, window_size.y - mouse_pos.y, 1, 1, gl::GL_RGBA,
                     gl::GL_UNSIGNED_BYTE, data);
    pick_data.entity_handle =
        data[0] + data[1] * 256 + data[2] * 256 * 256 + data[3] * 256 * 256 * 256;

    // get position
    float* position_pixels = (float*)malloc(window_size.x * window_size.y * 3 * sizeof(float));
    gl::glBindTexture(gl::GL_TEXTURE_2D, m_mouse_world_pos_texture);
    gl::glGetTexImage(gl::GL_TEXTURE_2D, 0, gl::GL_RGB, gl::GL_FLOAT, position_pixels);
    pick_data.world_position = glm::vec3(
        position_pixels[(window_size.y - mouse_pos.y) * window_size.x * 3 + mouse_pos.x * 3],
        position_pixels[(window_size.y - mouse_pos.y) * window_size.x * 3 + mouse_pos.x * 3 + 1],
        position_pixels[(window_size.y - mouse_pos.y) * window_size.x * 3 + mouse_pos.x * 3 + 2]);

    // get normal
    gl::glBindTexture(gl::GL_TEXTURE_2D, m_mouse_normal_texture);
    gl::glGetTexImage(gl::GL_TEXTURE_2D, 0, gl::GL_RGB, gl::GL_FLOAT, position_pixels);
    pick_data.normal = glm::vec3(
        position_pixels[(window_size.y - mouse_pos.y) * window_size.x * 3 + mouse_pos.x * 3],
        position_pixels[(window_size.y - mouse_pos.y) * window_size.x * 3 + mouse_pos.x * 3 + 1],
        position_pixels[(window_size.y - mouse_pos.y) * window_size.x * 3 + mouse_pos.x * 3 + 2]);

    free(position_pixels);
    return pick_data;
  }

  void set_mouse_picking_data(unsigned int fbo, unsigned int pos_texture,
                              unsigned int normal_texture) {
    m_mouse_fbo = fbo;
    m_mouse_world_pos_texture = pos_texture;
    m_mouse_normal_texture = normal_texture;
  }

 private:
  // Pointer to PhysicsManager, for exposing raycasting
  phy::PhysicsManager* m_physics = nullptr;

  // for exposing mousepicking data
  unsigned int m_mouse_fbo = 0;
  unsigned int m_mouse_world_pos_texture = 0;
  unsigned int m_mouse_normal_texture = 0;
};

// Contains items necessary during a System update
struct FrameContext {
  FrameContext(aud::AudioManager& audio_manager) : audio_mgr(audio_manager){};
  FrameContext(const FrameContext& other) = delete;
  FrameContext(FrameContext&& other) = default;
  FrameContext& operator=(FrameContext&& other) = default;

  EventManager event_mgr;
  EntityManager entity_mgr;
  SystemIDProxy system_id_proxy;
  FrameContextUtilities utilities;
  aud::AudioManager& audio_mgr;
  std::unordered_map<std::string, ai::Navmesh> navmeshes;

  float dt;
  // Factor (0-1) indicating how much time has been slowed. 1=normal time, 0.5=half speed.
  float speed_factor = 1.0f;
  float unslowed_dt;

  // Counter incremented every frame
  uint16_t frame_num = 0;

  bool is_replay = false;
  float replay_time_left = 0.f;
  float replay_time_factor = 1.f;
};
}  // namespace cor

// UNBOUND_TYPE((cor::FrameContext), m_entity_mgr, m_event_mgr, m_system_id_proxy, m_utilities)

#endif  // COR_CONTEXT_HPP
