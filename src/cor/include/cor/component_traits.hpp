#pragma once

#ifndef COR_IS_COMPONENT_HPP
#define COR_IS_COMPONENT_HPP

#include <bse/reflect.hpp>

namespace cor {

template <typename T>
using is_component = std::conjunction<  //
    std::negation<std::is_array<T>>,    //
    std::is_aggregate<T>                //
    // bse::meta_has_type<T>               //
    // std::negation<std::is_fundamental<T>>,       //
    // std::negation<std::is_lvalue_reference<T>>,  //
    >;
template <typename T>
inline constexpr bool is_component_v = is_component<T>::value;

}  // namespace cor
#endif  // COR_IS_COMPONENT_HPP
