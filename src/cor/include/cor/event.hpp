#pragma once

#ifndef COR_EVENT_HPP
#define COR_EVENT_HPP

#include "aud/sound_buffer.hpp"
#include "aud/sound_source.hpp"
#include "bse/asset.hpp"
#include "bse/file_path.hpp"
#include "entity_handle.hpp"
#include <bse/serialize.hpp>
#include <cor/cor.hpp>
#include <ext/ext.hpp>
#include <scr/dynamic_object.hpp>
#include <scr/dynamic_table.hpp>

// Used to test that events work by the unit tests
struct TestEvent {
  int num;
  std::string str;
};

SERFUN((TestEvent)) {
  SERIALIZE(self.num);
  SERIALIZE(self.str);
}
DESERFUN((TestEvent)) {
  DESERIALIZE(self.num);
  DESERIALIZE(self.str);
}

// Used to test that events work by the unit tests
struct TestEvent2 {
  float f1;
  float f2;
  float f3;
  float f4;
};

struct AIEntityStateChangeEvent {
  cor::EntityHandle id;
  std::string previous_state;
  std::string new_state;
};

SERFUN((AIEntityStateChangeEvent)) {
  SERIALIZE(self.id);
  SERIALIZE(self.previous_state);
  SERIALIZE(self.new_state);
}
DESERFUN((AIEntityStateChangeEvent)) {
  DESERIALIZE(self.id);
  DESERIALIZE(self.previous_state);
  DESERIALIZE(self.new_state);
}

struct RefreshAssetsEvent {};
struct ToggleBseEditEvent {};

struct AIAttackStartEvent {
  cor::EntityHandle handle;
  std::string ai_state;
};

SERFUN((AIAttackStartEvent)) {
  SERIALIZE(self.handle);
  SERIALIZE(self.ai_state);
}
DESERFUN((AIAttackStartEvent)) {
  DESERIALIZE(self.handle);
  DESERIALIZE(self.ai_state);
}

struct PlaySoundEvent {
  PlaySoundEvent(cor::EntityHandle id, bse::FilePath buffer_path, aud::SoundSettings settings)
      : id(id), buffer_path(buffer_path), settings(settings) {}
  PlaySoundEvent(){};
  cor::EntityHandle id;
  // bse::Asset<aud::SoundBuffer> buffer;
  bse::FilePath buffer_path;
  aud::SoundSettings settings;
};

SERFUN((PlaySoundEvent)) {
  SERIALIZE(self.id);
  SERIALIZE(self.buffer_path);
  SERIALIZE(self.settings);
}
DESERFUN((PlaySoundEvent)) {
  DESERIALIZE(self.id);
  DESERIALIZE(self.buffer_path);
  DESERIALIZE(self.settings);
}

struct PlaySoundDelayedEvent {
  cor::EntityHandle id;
  bse::FilePath buffer_path;
  aud::SoundSettings settings;
  float delay_time;
};

SERFUN((PlaySoundDelayedEvent)) {
  SERIALIZE(self.id);
  SERIALIZE(self.buffer_path);
  SERIALIZE(self.settings);
  SERIALIZE(self.delay_time);
}
DESERFUN((PlaySoundDelayedEvent)) {
  DESERIALIZE(self.id);
  DESERIALIZE(self.buffer_path);
  DESERIALIZE(self.settings);
  DESERIALIZE(self.delay_time);
}

struct ChangeMasterVolumeEvent {
  ChangeMasterVolumeEvent() = default;
  ChangeMasterVolumeEvent(bool var) : increase(var){};
  bool increase;
};

struct ChangeMasterPitchEvent {
  ChangeMasterPitchEvent() = default;
  float new_pitch_val;
};

struct StopSoundEvent {
  cor::EntityHandle id;
};

struct MouseMovementEvent {
  float dx;
  float dy;
  int posx;
  int posy;
};

struct StartLeftMousePress {
  glm::ivec2 position;
};

struct EndLeftMousePress {
  glm::ivec2 position;
};

struct BeginMousePicking {
  glm::ivec2 position;
};

struct MousePickData {
  glm::vec3 world_position;
  glm::vec3 normal;
  int entity_handle;
};

struct CtrlPressed {};

struct CtrlReleased {};

struct StartAimEvent {};

struct StopAimEvent {};

struct StartDrawEvent {};

struct StopDrawEvent {};

struct CancelActionEvent {};

struct JumpEvent {};

struct DashEvent {};

struct SlowMotionEvent {};

struct RopeEvent {};

struct NormalArrowModeEvent {};

struct ExplosiveArrowModeEvent {};

struct AddExplosiveArrowsEvent {};

struct ExplosiveArrowCountChanged {
  unsigned int new_count;
};

struct CloseApplication {};

struct MoveEvent {
  float forward;
  float right;
};

struct ChangeCameraEvent {
  cor::EntityHandle entity_handle;
};

struct ToggleDebugCameraMovementEvent {};

struct DebugDrawLineEvent {
  glm::vec3 from;
  glm::vec3 to;
  glm::vec3 color;
};

struct DebugDrawFrustum {
  glm::mat4 matrix;
  glm::vec3 color;
};

struct DebugDrawPointEvent {
  glm::vec3 position;
  glm::vec3 color;
};

struct CharacterControllerJumpEvent {
  glm::vec3 jump_vector;
  cor::EntityHandle id;
};

struct PlayerZeroHealth {};

struct WeakSpotHit {
  cor::EntityHandle target_entity;
  glm::vec3 hit_point;
  glm::vec3 hit_point_normal;
};

struct CollisionEvent {
  cor::EntityHandle entity_1;  // entity 1
  cor::EntityHandle entity_2;  // entity 2
  glm::vec3 hit_point;         // point of impact
  glm::vec3 hit_point_normal;  // collision normal of the hit point
};

struct AddThreatEvent {
  cor::EntityHandle self;
  cor::EntityHandle other;
  float threat_level;
};

struct IncreaseThreatEvent {
  cor::EntityHandle self;
  cor::EntityHandle other;
  float threat_addition;
};

struct EnableSystem {
  std::string system_name;
};

SERFUN((EnableSystem)) { SERIALIZE(self.system_name); }
DESERFUN((EnableSystem)) { DESERIALIZE(self.system_name); }

struct DisableSystem {
  std::string system_name;
};

SERFUN((DisableSystem)) { SERIALIZE(self.system_name); }
DESERFUN((DisableSystem)) { DESERIALIZE(self.system_name); }

struct ToggleSystem {
  std::string system_name;
};

SERFUN((ToggleSystem)) { SERIALIZE(self.system_name); }
DESERFUN((ToggleSystem)) { DESERIALIZE(self.system_name); }

struct StunEvent {
  cor::EntityHandle target;
  float duration_time;
  std::string source;
};

SERFUN((StunEvent)) {
  SERIALIZE(self.target);
  SERIALIZE(self.duration_time);
  SERIALIZE(self.source);
}
DESERFUN((StunEvent)) {
  DESERIALIZE(self.target);
  DESERIALIZE(self.duration_time);
  DESERIALIZE(self.source);
}

struct KnockBackEvent {
  cor::EntityHandle target;
  float max_duration = 2;
  float force = 5;
  glm::vec3 dir = glm::vec3(0, 0, 1);
  std::string source;
};

SERFUN((KnockBackEvent)) {
  SERIALIZE(self.target);
  SERIALIZE(self.max_duration);
  SERIALIZE(self.force);
  SERIALIZE(self.dir);
  SERIALIZE(self.source);
}
DESERFUN((KnockBackEvent)) {
  DESERIALIZE(self.target);
  DESERIALIZE(self.max_duration);
  DESERIALIZE(self.force);
  DESERIALIZE(self.dir);
  DESERIALIZE(self.source);
}

struct RopeReachEvent {
  bool can_reach;
};

/* Quest events */
struct TookHornNoteEvent {};

struct TookHornEvent {};

struct KilledGolemEvent {
  cor::EntityHandle golem_handle;
  cor::EntityHandle killing_arrow_handle;
  uint16_t arrow_frame_num;
  glm::vec3 arrow_final_point;
};

struct KilledSkullEvent {};

struct SpawnColeEvent {};

struct KilledColeEvent {};

struct GameQuestEvent {};
/* no more quest events */

struct PauseGameEvent {};

struct EnterEditorEvent {};

struct EnterStartEvent {};

using ScriptEvent = sol::table;  // Use this only for the editor. Not thread safe. To be fixed.

SERFUN((ScriptEvent)) {}
DESERFUN((ScriptEvent)) {}

struct EditorTransform {
  enum Flags {
    NONE = 0b000,
    TRANSLATE = 0b0001,
    ROTATE = 0b0010,
    SCALE = 0b0100,
    ATTACH = 0b1000
  } flags;
};

struct EditorPlayEvent {};

enum Axis {
  NONE = 0,   //
  X = 0b001,  //
  Y = 0b010,  //
  Z = 0b100   //
};

struct EditorTransformSetAxis {
  Axis axis;
};

struct EditorTransformAddAxis {
  Axis axis;
};

struct EditorMouseMovementEvent {
  float dx;
  float dy;
  int posx;
  int posy;
  float screen_posx;
  float screen_posy;
};

struct MoraleLossEvent {
  MoraleLossEvent() = default;
  MoraleLossEvent(int t, std::string p, cor::EntityHandle h, float m = 0)
      : type(t), party(p), handle(h), misc(m){};
  int type;
  std::string party = "";
  cor::EntityHandle handle;
  float misc;
};

SERFUN((MoraleLossEvent)) {
  SERIALIZE(self.type);
  SERIALIZE(self.party);
  SERIALIZE(self.handle);
  SERIALIZE(self.misc);
}
DESERFUN((MoraleLossEvent)) {
  DESERIALIZE(self.type);
  DESERIALIZE(self.party);
  DESERIALIZE(self.handle);
  DESERIALIZE(self.misc);
}

struct MoraleBoostEvent {
  MoraleBoostEvent() = default;
  MoraleBoostEvent(cor::EntityHandle h) : handle(h){};
  cor::EntityHandle handle;
};

struct AnimationEvent {
  AnimationEvent() {}
  AnimationEvent(std::string name, cor::EntityHandle handle, float blend_time)
      : animation_name(name), handle(handle), blend_time(blend_time) {}
  std::string animation_name;
  cor::EntityHandle handle;
  float blend_time;
};

SERFUN((AnimationEvent)) {
  SERIALIZE(self.animation_name);
  SERIALIZE(self.handle);
  SERIALIZE(self.blend_time);
}

DESERFUN((AnimationEvent)) {
  DESERIALIZE(self.animation_name);
  DESERIALIZE(self.handle);
  DESERIALIZE(self.blend_time);
}

struct AnimationNotifyEvent {
  std::string animation_name;
  cor::EntityHandle handle;
};

SERFUN((AnimationNotifyEvent)) {
  SERIALIZE(self.animation_name);
  SERIALIZE(self.handle);
}
DESERFUN((AnimationNotifyEvent)) {
  DESERIALIZE(self.animation_name);
  DESERIALIZE(self.handle);
}

struct QuickSave {};
struct QuickLoad {};

struct StartEventRecording {};
struct StopEventRecording {};
struct StartEventPlayback {};

struct EditorToggleGlobalTransform {};

struct EditorConfirmTransform {};
struct EditorCancelTransform {};
struct EditorStartMultiselect {};
struct EditorStopMultiselect {};
struct EditorStartAttach {};
struct EditorStopAttach {};
struct MousePickingResult {
  unsigned int fbo;
  unsigned int pos_texture;
  unsigned int normal_texture;
};
struct EditorCtrlClick {};

struct MenuConfirm {};

struct SpawnHealthOrb {
  glm::vec3 position;
  float delay = 0.f;
};

struct CutsceneFinished {};

struct SaveCheckpoint {};

struct LoadCheckpoint {};

struct CheckpointLoaded {};
struct ReplayStarted {};
struct ReplayEnded {};

struct DamageTaken {
  cor::EntityHandle handle;
};

// clang-format off
#define EVENT_TYPE_LIST                               \
  ScriptEvent,                                        \
  TestEvent,                                          \
  TestEvent2,                                         \
  int,                                                \
  AIAttackStartEvent,                                 \
  AIEntityStateChangeEvent,                           \
  RefreshAssetsEvent,                                 \
  MouseMovementEvent,                                 \
  StartAimEvent,                                      \
  StopAimEvent,                                       \
  StartDrawEvent,                                     \
  StopDrawEvent,                                      \
  JumpEvent,                                          \
  DashEvent,                                          \
  MoveEvent,                                          \
  SlowMotionEvent,                                    \
  RopeEvent,                                          \
  PlayerZeroHealth,                                   \
  WeakSpotHit,                                        \
  DebugDrawLineEvent,                                 \
  DebugDrawPointEvent,                                \
  CharacterControllerJumpEvent,                       \
  CollisionEvent,                                     \
  ChangeMasterVolumeEvent,                            \
  ChangeMasterPitchEvent,							                \
  PlaySoundEvent,                                     \
  PlaySoundDelayedEvent,                              \
  StopSoundEvent,                                     \
  CloseApplication,                                   \
  CancelActionEvent,                                  \
  AddThreatEvent,                                     \
  IncreaseThreatEvent,                                \
  ToggleBseEditEvent,                                 \
  ChangeCameraEvent,                                  \
  ToggleDebugCameraMovementEvent,                     \
  EnableSystem,                                       \
  DisableSystem,                                      \
  ToggleSystem,                                       \
  StunEvent,                                          \
  KnockBackEvent,                                     \
  RopeReachEvent,                                     \
  PauseGameEvent,                                     \
  EnterEditorEvent,                                   \
  EnterStartEvent,                                    \
  EditorTransform,                                    \
  EditorTransformSetAxis,                             \
  EditorTransformAddAxis,                             \
  EditorMouseMovementEvent,                           \
  ExplosiveArrowModeEvent,                            \
  NormalArrowModeEvent,                               \
  StartLeftMousePress,                                \
  EndLeftMousePress,                                  \
  BeginMousePicking,                                  \
  MousePickData,                                      \
  MoraleLossEvent,                                    \
  MoraleBoostEvent,                                   \
  AnimationEvent,																			\
  AnimationNotifyEvent,																\
  QuickSave,										                      \
  QuickLoad,										                      \
  StartEventRecording,                                \
  StopEventRecording,                                 \
  StartEventPlayback,                                 \
  EditorToggleGlobalTransform,                        \
  EditorConfirmTransform,                             \
  EditorCancelTransform,                              \
  EditorStartMultiselect,                             \
  EditorStopMultiselect,                              \
  TookHornNoteEvent,                                  \
  TookHornEvent,								                      \
  KilledGolemEvent,									                  \
  KilledSkullEvent,									                  \
  SpawnColeEvent,                                     \
  KilledColeEvent,									                  \
  GameQuestEvent,									                    \
  EditorStartAttach,                                  \
  EditorStopAttach,                                   \
  MousePickingResult,                                 \
  EditorCtrlClick,                                    \
  EditorPlayEvent,                                    \
  MenuConfirm,                                        \
  AddExplosiveArrowsEvent,                            \
  CutsceneFinished,                                   \
  ExplosiveArrowCountChanged,                         \
  SpawnHealthOrb,                                     \
  SaveCheckpoint,                                     \
  LoadCheckpoint,                                     \
  CheckpointLoaded,                                   \
  ReplayStarted,                                      \
  ReplayEnded,                                        \
  DamageTaken
// clang-format on

struct Event;

namespace bse {
template <>
void serialize<Event>(const Event& self, ::bse::SerializedData& data);
void deserialize(Event& self, const ::bse::SerializedData& data, int& offset);
}  // namespace bse

#endif  // COR_EVENT_HPP