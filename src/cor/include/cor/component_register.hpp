#pragma once

#ifndef COR_COMPONENT_REGISTER_HPP
#define COR_COMPONENT_REGISTER_HPP

#include "cor/component_id.hpp"
#include "cor/component_name.hpp"
#include "cor/component_store.hpp"
#include <bse/reflect.hpp>
#include <ext/ext.hpp>

#define REGISTER_COMPONENT(name, ...)                                                        \
  namespace cor::detail::component_register::STRIP_PARENS(name) {                            \
    static bool flag = ::cor::ComponentRegister::register_component<::STRIP_PARENS(name)>(); \
  }

namespace cor {
class ComponentRegister {
 public:
  // Registers a component (only to be used internally)
  template <typename T>
  static bool register_component();

  static std::vector<std::unique_ptr<AbstractComponentStore>> copy_component_store_vector();
  // static std::vector<int> get_component_store_vector();

 private:
  static std::vector<std::unique_ptr<AbstractComponentStore>>& get_component_stores();

  static int m_current_id;
  // static std::vector<std::unique_ptr<AbstractComponentStore>> m_component_stores;
  // static std::vector<int> m_component_stores;
};

template <typename T>
bool ComponentRegister::register_component() {
  if (ComponentID<T>::give_id(m_current_id)) {
    if (ComponentID<T>::get_id() == m_current_id) {
      m_current_id++;
    }
    ComponentName::give_name<T>(bse::meta<T>::base_name);

    get_component_stores().resize(m_current_id);
    get_component_stores()[ComponentID<T>::get_id()] =
        std::move(std::make_unique<ComponentStore<T>>());
    // m_component_stores.resize(m_current_id);
    // m_component_stores[ComponentID<T>::get_id()] = 5;
  }
  return true;
}

// std::vector<std::unique_ptr<AbstractComponentStore>>
// ComponentRegister::get_component_store_vector() {
//   return m_component_stores;
// }

}  // namespace cor

#endif  // COR_COMPONENT_REGISTER_HPP
