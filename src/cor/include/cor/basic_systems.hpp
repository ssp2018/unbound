#pragma once

#ifndef COR_BASIC_SYSTEMS_HPP
#define COR_BASIC_SYSTEMS_HPP

#include "cor/system.hpp"
namespace cor {
struct FrameContext;
}

namespace cor {
// A test system
class TestSystem : public System {
 public:
  virtual ~TestSystem() = default;
  void read_update(const FrameContext& context) override;
  void write_update(FrameContext& context) override;

 private:
};
}  // namespace cor
#endif  // COR_BASIC_SYSTEMS_HPP
