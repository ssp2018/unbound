#pragma once

#ifndef COR_SYSTEM_TRAITS_HPP
#define COR_SYSTEM_TRAITS_HPP

// #include "cor/frame_context.hpp"

namespace cor {

// // SFINAE construct for checking whether a class has a proper read_update method
// template <typename T, typename = void>
// struct has_system_read : std::false_type {};

// template <typename T>
// struct has_system_read<T, decltype(std::declval<T>().read_update(FrameContext()))>
//     : std::true_type {};

// template <typename T>
// constexpr bool has_system_read_v = has_system_read<T>::value;

// // SFINAE construct for checking whether a class has a proper write_update method
// template <typename T, typename = void>
// struct has_system_write : std::false_type {};

// template <typename T>
// struct has_system_write<T, decltype(std::declval<T>().read_update(FrameContext()))>
//     : std::true_type {};

// template <typename T>
// constexpr bool has_system_write_v = has_system_write<T>::value;

//
}  // namespace cor
#endif  // COR_SYSTEM_TRAITS_HPP
