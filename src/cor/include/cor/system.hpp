#pragma once

#ifndef COR_SYSTEM_HPP
#define COR_SYSTEM_HPP

#include <bse/serialize.hpp>
#include <cor/event_manager.hpp>

namespace cor {
struct FrameContext;
}  // namespace cor
namespace cor {

struct FrameContext;
// The base for all systems
class System {
 public:
  virtual ~System() = default;

  // Updates everything in the system that can be done in a read-only context
  virtual void read_update(const FrameContext& context) = 0;

  // Updates everything in the system that must be done in a write-only context
  virtual void write_update(FrameContext& context) = 0;

  // Another write update, executed after all write_updates
  virtual void write_update_late(FrameContext& context);

  // Enables all listeners created in the system
  void enable_listeners(FrameContext& context);

  // Disables all listeners created in the system
  void disable_listeners(FrameContext& context);

  // Toggles all listeners created in the system
  void toggle_listeners(FrameContext& context);

  // Adds a listener to the system
  void add_listener(cor::EventManager::ListenID lid);

  ABSTRACT_VIRTUAL_CLASS_SERFUN((System));
  ABSTRACT_VIRTUAL_CLASS_DESERFUN((System));

 private:
  std::vector<cor::EventManager::ListenID> m_lids;
};
}  // namespace cor
#endif  // COR_SYSTEM_HPP
