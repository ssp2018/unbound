#pragma once

#ifndef COR_ENTITY_HANDLE_HPP
#define COR_ENTITY_HANDLE_HPP

namespace cor {

// Represents an entity in the entity manager
// Has no real externally usable interface
struct EntityHandle {
  using value_type = uint32_t;
  static const EntityHandle NULL_HANDLE;

  EntityHandle();
  EntityHandle(int handle);

  operator value_type();
  bool operator==(const cor::EntityHandle& other) const;
  bool operator!=(const cor::EntityHandle& other) const;

  union {
    struct {
      value_type register_index : 24;
      value_type iteration : 8;
    };
    value_type full_handle = ~value_type(0);
  };
};

}  // namespace cor

namespace std {
template <>
struct hash<cor::EntityHandle> {
  size_t operator()(const cor::EntityHandle& eh) const {
    return hash<cor::EntityHandle::value_type>()(eh.full_handle);  //
  }
};
}  // namespace std

#endif  // COR_ENTITY_HANDLE_HPP
