#include <cor/script_type.hpp>
#include <scr/object.hpp>
namespace cor {
template <typename T>
bool ScriptType::load_reflectable(T&& t, const scr::Object& object) {
  bool is_successful = true;
  bse::for_each_field(t, [&is_successful, &object](auto& field) {
    if constexpr (!std::is_member_function_pointer_v<decltype(field.value)> &&
                  std::is_copy_assignable_v<decltype(field.value)>) {
      using FieldType = std::remove_reference_t<decltype(field.value)>;
      scr::Object field_object = object[field.name];
      if (field_object.is_null()) {
        field_object = object[field.index];
        if (field_object.is_null()) {
          LOG(WARNING) << "Failed to load field '" << std::string(field.name)
                       << "' of reflectable type '" << std::string(bse::meta<T>::name)
                       << "'. Expected key of '" << std::string(field.name) << "' or '"
                       << field.index << "'.\n";
          is_successful = false;
          field.value = std::move(FieldType());
          return;
        }
      }

      if constexpr (bse::meta<FieldType>::is_defined) {
        if (!load_reflectable(field.value, field_object)) {
          is_successful = false;
        }
      } else {
        field.value = std::move(field_object.get<FieldType>());
        if (!field_object.is<FieldType>()) {
          is_successful = false;

          LOG(WARNING) << "Failed to load field '" << std::string(field.name)
                       << "' of non-reflectable type '" << std::string(bse::meta<T>::name)
                       << "'. Mismatching types. Got '" << field_object.str() << "'.\n";
        }
      }
    }
  });

  return is_successful;
}
}  // namespace cor