#pragma once

#ifndef GMP_EDITOR_STATE_HPP
#define GMP_EDITOR_STATE_HPP

#include <cor/entity_handle.hpp>
#include <cor/state.hpp>

namespace cor {
//

// EditorState represents the base for any future Editor implementations.
class EditorState : public cor::State {
 public:
  EditorState(cor::Game& game, gfx::Window& window, aud::AudioManager& audio_mgr);
  virtual ~EditorState();

  void initialize() override;

  bool update(float dt) override;
  bool display() override;

  // void select(cor::EntityHandle handle);
  // void deselect();
  // cor::EntityHandle get_selected_entity() const;

 protected:
  std::vector<cor::EventManager::ListenID> m_lids;

 private:
  // cor::EntityHandle m_selected_entity;
};
}  // namespace cor
#endif  // GMP_EDITOR_STATE_HPP
