#pragma once

#ifndef COR_COMPONENTS_HPP
#define COR_COMPONENTS_HPP

#include "bse/reflect.hpp"

namespace cor {

// A test component
struct TestComponent {
  int a = 0;
  int b = 20;
  int c = 40;

  // UNBOUND_SERIALIZABLE(TestComponent);
};

}  // namespace cor

REFLECT((cor::TestComponent), a)

#endif  // COR_COMPONENTS_HPP