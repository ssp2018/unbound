#pragma once

#ifndef COR_COMPONENT_STORE_HPP
#define COR_COMPONENT_STORE_HPP

#include <bse/serialize.hpp>
#include <ext/ext.hpp>

namespace cor {

// Abstract representation for component storage.
// Is to be implemented per component type via template
class AbstractComponentStore {
 public:
  virtual ~AbstractComponentStore() = default;

  virtual std::unique_ptr<AbstractComponentStore> create_new() = 0;

  // AbstractComponentStore(const AbstractComponentStore& other) = default;
  // AbstractComponentStore& operator=(const AbstractComponentStore& other) = default;
  // AbstractComponentStore(AbstractComponentStore&& other) = default;
  // AbstractComponentStore& operator=(AbstractComponentStore&& other) = default;

  // Adds a component and returns its index
  virtual uint16_t add() = 0;
  virtual void set(uint16_t index, void* data) = 0;

  // Returns the address of the component at the given index
  virtual void* get(uint16_t index) = 0;

  // Removes the component at the given index. Is free to simply leave the address unused until
  // a future "add" call.
  virtual void remove(uint16_t index) = 0;

  VIRTUAL_CLASS_SERFUN((AbstractComponentStore)) {}
  VIRTUAL_CLASS_DESERFUN((AbstractComponentStore)) {}
};

// Specialization of the above component store.
// This is a trick to be able to reuse code for different types of components
template <typename T>
class ComponentStore : public AbstractComponentStore {
 public:
  virtual ~ComponentStore() = default;

  // ComponentStore(const ComponentStore& other) = default;
  // ComponentStore& operator=(const ComponentStore& other) = default;
  // ComponentStore(ComponentStore&& other) = default;
  // ComponentStore& operator=(ComponentStore&& other) = default;

  std::unique_ptr<AbstractComponentStore> create_new() final override;

  // Overrides of the above methods
  uint16_t add() final override;
  void set(uint16_t index, void* data) final override;
  void* get(uint16_t index) final override;
  void remove(uint16_t index) final override;

  VIRTUAL_CLASS_SERFUN((ComponentStore<T>)) {
    SERIALIZE_BASE_CLASS((AbstractComponentStore));
    SERIALIZE(m_components);
    SERIALIZE(m_free_indices);
  }

  VIRTUAL_CLASS_DESERFUN((ComponentStore<T>)) {
    DESERIALIZE_BASE_CLASS((AbstractComponentStore));
    DESERIALIZE(m_components);
    DESERIALIZE(m_free_indices);
  }

 private:
  std::vector<T> m_components;
  std::vector<uint16_t> m_free_indices;
};

template <typename T>
std::unique_ptr<AbstractComponentStore> ComponentStore<T>::create_new() {
  return std::make_unique<ComponentStore<T>>();
}

template <typename T>
uint16_t ComponentStore<T>::add() {
  //
  if (m_free_indices.empty()) {
    m_components.emplace_back();
    return static_cast<uint16_t>(m_components.size() - 1);
  } else {
    uint16_t free_index = m_free_indices.back();
    m_components[free_index] = T{};
    m_free_indices.pop_back();
    return free_index;
  }
}

template <typename T>
void ComponentStore<T>::set(uint16_t index, void* data) {
  if constexpr (std::is_copy_constructible_v<T> && std::is_copy_assignable_v<T>) {
    m_components[index] = *(T*)data;
  } else if constexpr (std::is_move_constructible_v<T> && std::is_move_assignable_v<T>) {
    m_components[index] = std::move(*(T*)data);
  } else {
    ASSERT(false) << "Attempting to set non-copyable data.";
  }
}

template <typename T>
void* ComponentStore<T>::get(uint16_t index) {
  //
  return &m_components[index];
}

template <typename T>
void ComponentStore<T>::remove(uint16_t index) {
  //
  m_free_indices.push_back(index);
}

}  // namespace cor
#endif  // COR_COMPONENT_STORE_HPP
