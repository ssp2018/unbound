#pragma once

#ifndef COR_GAME_HPP
#define COR_GAME_HPP

#include <aud/audio_manager.hpp>
#include <cor/state.hpp>
#include <ext/ext.hpp>

namespace gfx {
class Window;
}
namespace cor {
// Represents the game loop
class Game {
 public:
  Game(gfx::Window& window, unsigned int max_frame_count = 0);
  virtual ~Game();

  // Starts the game loop
  void start();

  // Methods for pushing, popping and replacing the current game-state
  void push_state(std::unique_ptr<cor::State> state);
  void pop_state();
  void replace_state(std::unique_ptr<cor::State> state);

  // Peek at the state below this state in the stack.
  // Return null if none below
  const cor::State* peek_below(const cor::State* state) const;

  // Peek at the state above this state in the stack.
  // Return null if none above
  const cor::State* peek_above(const cor::State* state) const;

 protected:
  // Initializes the game by registering components and systems
  // The actual game should override this to register game specific entities and components
  virtual void initialize();

  // Number of frames to run before automatically shutting the program down. 0 = run forever.
  unsigned int m_frame_num;
  unsigned int m_max_frame_count;

 private:
  struct StateRequest {
    enum class Type { PUSH, POP, REPLACE };
    Type type;
    std::unique_ptr<cor::State> state;
  };

 private:
  void update_states(float dt);
  void display_states();
  void handle_state_requests();

 protected:
  gfx::Window& m_window;
  aud::AudioManager m_audio_mgr;

 private:
  // Put all private members to Game here

  std::vector<StateRequest> m_state_requests;
  std::vector<std::unique_ptr<cor::State>> m_state_stack;
};
}  // namespace cor
#endif  // COR_GAME_HPP
