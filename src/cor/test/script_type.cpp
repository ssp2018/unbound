#define UNBOUND_DO_EXPOSE_TYPES
#include "bse/reflect.hpp"
#include "scr/detail/state.hpp"
#include "test_lock.hpp"
#include <cor/cor.hpp>
#include <cor/entity.tcc>
#include <cor/entity_manager.tcc>
#include <scr/object.hpp>
#include <scr/scr.tcc>
#include <scr/script.hpp>
using namespace cor;

namespace {
struct Foo {
  int a;

  int b;
};

struct Bar {
  float c;
  std::string d;
};
}  // namespace

UNBOUND_COMPONENT((Foo), a, b)
UNBOUND_COMPONENT((Bar), c, d)

// TEST(ScriptType, LoadFromScript) {
//   std::lock_guard lock(test_lock);
//   scr::Script script("", R"(
// entity = {
//   Foo = {
//     a = 5,
//     b = 4,
//   },

//   Bar = {
//     c = 1.5,
//     d = "hello",
//   },
// }
// )");

//   scr::Object templ = script.return_value()["entity"];

//   EntityManager man;
//   // man.register_component<Foo>("Foo");
//   // man.register_component<Bar>("Bar");

//   Entity entity = man.get_entity(man.create_entity());
//   create_entity(entity, templ);

//   Foo* foo = entity;
//   Bar* bar = entity;

//   EXPECT_NE(nullptr, foo);
//   EXPECT_NE(nullptr, bar);

//   EXPECT_EQ(5, foo->a);
//   EXPECT_EQ(4, foo->b);
//   EXPECT_FLOAT_EQ(1.5, bar->c);
//   EXPECT_EQ("hello", bar->d);
// }

TEST(ScriptType, PtrModify) {
  std::lock_guard lock(test_lock);
  sol::state& lua = scr::detail::State::get();
  std::function<void(Foo*)> foo_modifier = lua.safe_script(R"(
return function(foo)
  foo.a = 2
end
)");

  Foo foo;
  foo.a = 1;

  foo_modifier(&foo);

  EXPECT_EQ(2, foo.a);
}

TEST(ScriptType, RefModify) {
  std::lock_guard lock(test_lock);
  sol::state& lua = scr::detail::State::get();
  std::function<void(Foo&)> foo_modifier = lua.safe_script(R"(
return function(foo)
  foo.a = 2
end
)");

  Foo foo;
  foo.a = 1;

  foo_modifier(foo);

  EXPECT_EQ(2, foo.a);
}

TEST(ScriptType, CopyModify) {
  std::lock_guard lock(test_lock);
  sol::state& lua = scr::detail::State::get();
  std::function<void(Foo)> foo_modifier = lua.safe_script(R"(
return function(foo)
  foo.a = 2
end
)");

  Foo foo;
  foo.a = 1;

  foo_modifier(foo);

  EXPECT_EQ(1, foo.a);
}

TEST(ScriptType, ReturnPtr) {
  std::lock_guard lock(test_lock);
  sol::state& lua = scr::detail::State::get();
  std::function<Foo*()> foo_factory = lua.safe_script(R"(
return function()
  foo = cpp.Foo.new()
  foo.a = 1337
  return foo
end
)");

  Foo* foo = foo_factory();
  EXPECT_NE(nullptr, foo);
  EXPECT_EQ(1337, foo->a);
}
