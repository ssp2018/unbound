#include "cor/component_id.hpp"      // for ComponentFlags
#include "cor/component_traits.hpp"  // for is_component_v
#include "cor/entity_handle.hpp"     // for EntityHandle, EntityHandle::NULL...
#include "cor/key.hpp"               // for Key
#include "unit_components.hpp"       // for UnitTestComponent01, UnitTestCom...
#include <cor/entity.tcc>            // for Entity, ConstEntity
#include <cor/entity_manager.tcc>    // for EntityManager

using namespace cor;

constexpr int CREATED_ENTITES = 3;

static_assert(CREATED_ENTITES % 3 == 0,
              "CREATED_ENTITIES must be divisible by 3 to run these tests");

TEST(ECSTest, RegisteringComponents) {
  //
  EntityManager em;
  // em.register_component<UnitTestComponent01>("UnitTestComponent01");
  // em.register_component<UnitTestComponent02>("UnitTestComponent02");
  // em.register_component<UnitTestComponent03>("UnitTestComponent03");

  //// Must not be able to register the same components twice!
  // ASSERT_DEATH(em.register_component<UnitTestComponent03>("UnitTestComponent03"),
  //             "Fatal..ASSERTION FAILED");
  // ASSERT_DEATH(em.register_component<UnitTestComponent02>("UnitTestComponent02"),
  //             "Fatal..ASSERTION FAILED");
  // ASSERT_DEATH(em.register_component<UnitTestComponent01>("UnitTestComponent01"),
  //             "Fatal..ASSERTION FAILED");

  // Must be able to distinguish proper components from other types!
  ASSERT_TRUE(is_component_v<UnitTestComponent01>);

  ASSERT_FALSE(is_component_v<int>);
  ASSERT_FALSE(is_component_v<float>);
  ASSERT_FALSE(is_component_v<UnitTestComponent01&>);
  ASSERT_FALSE(is_component_v<std::string>);
  ASSERT_FALSE(is_component_v<std::vector<UnitTestComponent01>>);
  ASSERT_FALSE(is_component_v<BadUnitTestComponent01>);
}

TEST(ECSTest, CreatingEntities) {
  //
  EntityManager em;
  // em.register_component<UnitTestComponent01>("UnitTestComponent01");
  // em.register_component<UnitTestComponent02>("UnitTestComponent02");
  // em.register_component<UnitTestComponent03>("UnitTestComponent03");

  std::vector<EntityHandle> handles;
  handles.resize(CREATED_ENTITES);
  for (int i = 0; i < CREATED_ENTITES; i++) {
    handles[i] = em.create_entity();
  }

  for (auto& handle : handles) {
    ASSERT_TRUE(em.is_valid(handle));

    Entity ent = em.get_entity(handle);
    ASSERT_TRUE(ent.is_valid());
  }
}

TEST(ECSTest, DestroyingEntities) {
  //
  EntityManager em;
  // em.register_component<UnitTestComponent01>("UnitTestComponent01");
  // em.register_component<UnitTestComponent02>("UnitTestComponent02");
  // em.register_component<UnitTestComponent03>("UnitTestComponent03");

  std::vector<EntityHandle> handles;
  handles.resize(CREATED_ENTITES);
  for (int i = 0; i < CREATED_ENTITES; i++) {
    handles[i] = em.create_entity();
  }

  for (auto& handle : handles) {
    em.destroy_entity(handle);
  }

  for (auto& handle : handles) {
    ASSERT_FALSE(em.is_valid(handle));
  }
  ASSERT_NO_FATAL_FAILURE(Entity ent = em.get_entity(handles[0]));
  Entity ent = em.get_entity(handles[0]);
  ASSERT_DEATH(ent.has<UnitTestComponent01>(), "Fatal..ASSERTION FAILED");
}

TEST(ECSTest, AttachingComponents) {
  EntityManager em;
  // em.register_component<UnitTestComponent01>("UnitTestComponent01");
  // em.register_component<UnitTestComponent02>("UnitTestComponent02");
  // em.register_component<UnitTestComponent03>("UnitTestComponent03");

  std::vector<EntityHandle> handles;
  handles.resize(CREATED_ENTITES);
  for (int i = 0; i < CREATED_ENTITES; i++) {
    handles[i] = em.create_entity();
    switch (i % 3) {
      case 0: {
        Entity ent = em.get_entity(handles[i]);
        ent = UnitTestComponent01();
      } break;
      case 1: {
        Entity ent = em.get_entity(handles[i]);
        ent = UnitTestComponent02();
      } break;
      case 2: {
        Entity ent = em.get_entity(handles[i]);
        ent = UnitTestComponent03();
      } break;
    }
  }

  ASSERT_EQ(em.get_entities(Key::create<UnitTestComponent01>()).size(), CREATED_ENTITES / 3);
  ASSERT_EQ(em.get_entities(Key::create<UnitTestComponent02>()).size(), CREATED_ENTITES / 3);
  ASSERT_EQ(em.get_entities(Key::create<UnitTestComponent03>()).size(), CREATED_ENTITES / 3);
}

TEST(ECSTest, DetachingComponents) {
  EntityManager em;
  // em.register_component<UnitTestComponent01>("UnitTestComponent01");
  // em.register_component<UnitTestComponent02>("UnitTestComponent02");
  // em.register_component<UnitTestComponent03>("UnitTestComponent03");

  std::vector<EntityHandle> handles;
  handles.resize(CREATED_ENTITES);
  for (int i = 0; i < CREATED_ENTITES; i++) {
    handles[i] = em.create_entity();
    switch (i % 3) {
      case 0: {
        Entity ent = em.get_entity(handles[i]);
        ent = UnitTestComponent01();
      } break;
      case 1: {
        Entity ent = em.get_entity(handles[i]);
        ent = UnitTestComponent02();
      } break;
      case 2: {
        Entity ent = em.get_entity(handles[i]);
        ent = UnitTestComponent03();
      } break;
    }
  }

  ASSERT_NO_FATAL_FAILURE(em.get_entity(handles[0]).get<UnitTestComponent01>());
  ASSERT_NO_FATAL_FAILURE(em.get_entity(handles[1]).get<UnitTestComponent02>());
  ASSERT_NO_FATAL_FAILURE(em.get_entity(handles[2]).get<UnitTestComponent03>());

  for (auto& ent : em.get_entities(Key::create<UnitTestComponent01>())) {
    //
    em.get_entity(ent).detach<UnitTestComponent01>();
  }

  for (auto& ent : em.get_entities(Key::create<UnitTestComponent02>())) {
    //
    em.get_entity(ent).detach<UnitTestComponent02>();
  }

  // Check that all components except UnitTestComponent03 were removed.
  ASSERT_EQ(em.get_entities(Key::create<UnitTestComponent01>()).size(), 0);
  ASSERT_EQ(em.get_entities(Key::create<UnitTestComponent02>()).size(), 0);
  ASSERT_EQ(em.get_entities(Key::create<UnitTestComponent03>()).size(), CREATED_ENTITES / 3);

  ASSERT_DEATH(em.get_entity(handles[0]).get<UnitTestComponent01>(), "Fatal..ASSERTION FAILED");
  ASSERT_DEATH(em.get_entity(handles[1]).get<UnitTestComponent02>(), "Fatal..ASSERTION FAILED");
  ASSERT_NO_FATAL_FAILURE(em.get_entity(handles[2]).get<UnitTestComponent03>());
}

TEST(ECSTest, EntityHas) {
  EntityManager em;
  // em.register_component<UnitTestComponent01>("UnitTestComponent01");
  // em.register_component<UnitTestComponent02>("UnitTestComponent02");
  // em.register_component<UnitTestComponent03>("UnitTestComponent03");

  Entity ent = em.get_entity(em.create_entity());
  ent = UnitTestComponent01();

  ASSERT_TRUE(ent.has<UnitTestComponent01>());
  ASSERT_TRUE(ent.has(Key::create<UnitTestComponent01>()));

  ASSERT_FALSE(ent.has<UnitTestComponent02>());
  ASSERT_FALSE(ent.has(Key::create<UnitTestComponent02>()));

  ent = UnitTestComponent02();

  bool a = ent.has<UnitTestComponent01, UnitTestComponent02>();
  ASSERT_TRUE(a);
  Key b = Key::create<UnitTestComponent01, UnitTestComponent02>();
  ASSERT_TRUE(ent.has(b));
}

TEST(ECSTest, EntityDestroy) {
  EntityManager em;
  // em.register_component<UnitTestComponent01>("UnitTestComponent01");
  // em.register_component<UnitTestComponent02>("UnitTestComponent02");
  // em.register_component<UnitTestComponent03>("UnitTestComponent03");

  Entity ent = em.get_entity(em.create_entity());
  ASSERT_TRUE(ent.is_valid());

  ent.destroy();
  ASSERT_FALSE(ent.is_valid());
}

TEST(ECSTest, EntityGet) {
  EntityManager em;
  // em.register_component<UnitTestComponent01>("UnitTestComponent01");
  // em.register_component<UnitTestComponent02>("UnitTestComponent02");
  // em.register_component<UnitTestComponent03>("UnitTestComponent03");

  Entity ent = em.get_entity(em.create_entity());
  ent = UnitTestComponent01();

  ASSERT_NO_FATAL_FAILURE(UnitTestComponent01& uc0 = ent);
  ASSERT_NO_FATAL_FAILURE(const UnitTestComponent01& uc1 = ent);
  ASSERT_NO_FATAL_FAILURE(UnitTestComponent01& uc2 = ent.get<UnitTestComponent01>());
  ASSERT_NO_FATAL_FAILURE(const UnitTestComponent01& uc3 = ent.get<UnitTestComponent01>());

  ASSERT_DEATH(UnitTestComponent02& uc0 = ent, "Fatal..ASSERTION FAILED");
  ASSERT_DEATH(const UnitTestComponent02& uc1 = ent, "Fatal..ASSERTION FAILED");
  ASSERT_DEATH(UnitTestComponent02& uc2 = ent.get<UnitTestComponent02>(),
               "Fatal..ASSERTION FAILED");
  ASSERT_DEATH(const UnitTestComponent02& uc3 = ent.get<UnitTestComponent02>(),
               "Fatal..ASSERTION FAILED");
}

TEST(ECSTest, EntityGetIf) {
  EntityManager em;
  // em.register_component<UnitTestComponent01>("UnitTestComponent01");
  // em.register_component<UnitTestComponent02>("UnitTestComponent02");
  // em.register_component<UnitTestComponent03>("UnitTestComponent03");

  Entity ent = em.get_entity(em.create_entity());
  ent = UnitTestComponent01();

  UnitTestComponent01* uc0 = ent;
  const UnitTestComponent01* uc1 = ent;
  UnitTestComponent01* uc2 = ent.get_if<UnitTestComponent01>();
  const UnitTestComponent01* uc3 = ent.get_if<UnitTestComponent01>();

  ASSERT_NE(uc0, nullptr);
  ASSERT_NE(uc1, nullptr);
  ASSERT_NE(uc2, nullptr);
  ASSERT_NE(uc3, nullptr);

  UnitTestComponent02* bc0 = ent;
  const UnitTestComponent02* bc1 = ent;
  UnitTestComponent02* bc2 = ent.get_if<UnitTestComponent02>();
  const UnitTestComponent02* bc3 = ent.get_if<UnitTestComponent02>();

  ASSERT_EQ(bc0, nullptr);
  ASSERT_EQ(bc1, nullptr);
  ASSERT_EQ(bc2, nullptr);
  ASSERT_EQ(bc3, nullptr);
}

TEST(ECSTest, EntityAttachDetach) {
  EntityManager em;
  // em.register_component<UnitTestComponent01>("UnitTestComponent01");
  // em.register_component<UnitTestComponent02>("UnitTestComponent02");
  // em.register_component<UnitTestComponent03>("UnitTestComponent03");

  Entity ent = em.get_entity(em.create_entity());
  ent = UnitTestComponent01();
  ent = UnitTestComponent02();
  ent = UnitTestComponent03();

  Key key = Key::create<UnitTestComponent01, UnitTestComponent02, UnitTestComponent03>();

  ASSERT_TRUE(ent.has(key));

  ent.detach<UnitTestComponent01>();
  ent.detach<UnitTestComponent02>();
  ent.detach<UnitTestComponent03>();

  ASSERT_FALSE(ent.has(key));

  ent.attach<UnitTestComponent01>();
  ent.attach<UnitTestComponent02>();
  ent.attach<UnitTestComponent03>();

  ASSERT_TRUE(ent.has(key));
}

TEST(ECSTest, EntityGetKey) {
  EntityManager em;
  // em.register_component<UnitTestComponent01>("UnitTestComponent01");
  // em.register_component<UnitTestComponent02>("UnitTestComponent02");
  // em.register_component<UnitTestComponent03>("UnitTestComponent03");

  Entity ent = em.get_entity(em.create_entity());
  ent = UnitTestComponent01();
  ent = UnitTestComponent03();

  Key key = Key::create<UnitTestComponent01, UnitTestComponent03>();

  ASSERT_TRUE(ent.has(key));
  ASSERT_TRUE(ent.get_key() == key);
  ASSERT_TRUE(ent.get_key().get_bits().test(cor::ComponentID<UnitTestComponent01>::get_id()));
  ASSERT_FALSE(ent.get_key().get_bits().test(cor::ComponentID<UnitTestComponent02>::get_id()));
  ASSERT_TRUE(ent.get_key().get_bits().test(cor::ComponentID<UnitTestComponent03>::get_id()));
}

TEST(ECSTest, ConstEntity) {
  EntityManager em;
  // em.register_component<UnitTestComponent01>("UnitTestComponent01");
  // em.register_component<UnitTestComponent02>("UnitTestComponent02");
  // em.register_component<UnitTestComponent03>("UnitTestComponent03");

  Entity ent = em.get_entity(em.create_entity());
  ent = UnitTestComponent01();
  ent = UnitTestComponent03();

  ConstEntity cent = ent;

  Key key = Key::create<UnitTestComponent01, UnitTestComponent03>();

  ASSERT_TRUE(cent.has(key));
  ASSERT_TRUE(cent.get_key() == key);
  ASSERT_TRUE(cent.get_key().get_bits().test(cor::ComponentID<UnitTestComponent01>::get_id()));
  ASSERT_FALSE(cent.get_key().get_bits().test(cor::ComponentID<UnitTestComponent02>::get_id()));
  ASSERT_TRUE(cent.get_key().get_bits().test(cor::ComponentID<UnitTestComponent03>::get_id()));
}

TEST(ECSTest, NullEntity) {
  EntityManager em;
  // em.register_component<UnitTestComponent01>("UnitTestComponent01");
  // em.register_component<UnitTestComponent02>("UnitTestComponent02");
  // em.register_component<UnitTestComponent03>("UnitTestComponent03");

  EntityHandle handle;
  ASSERT_EQ(handle, EntityHandle::NULL_HANDLE);

  Entity ent;
  ASSERT_FALSE(ent.is_valid());
  ASSERT_DEATH(ent.get_key(), "Fatal..ASSERTION FAILED");

  ent = em.get_entity(handle);
  ASSERT_FALSE(ent.is_valid());
  ASSERT_DEATH(ent.get_key(), "Fatal..ASSERTION FAILED");

  handle = em.create_entity();
  ASSERT_NE(handle, EntityHandle::NULL_HANDLE);

  ent = em.get_entity(handle);
  ASSERT_TRUE(ent.is_valid());
  ASSERT_NO_FATAL_FAILURE(ent.get_key());
}
