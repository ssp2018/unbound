#pragma once

#ifndef COR_UNIT_COMPONENTS_HPP
#define COR_UNIT_COMPONENTS_HPP

#include <bse/reflect.hpp>
#include <cor/cor.hpp>

struct UnitTestComponent01 {
  int a = 0;
  int b = 0;
};

struct UnitTestComponent02 {
  int c = 0;
  int d = 0;
};

struct UnitTestComponent03 {
  int e = 0;
  int f;
};

UNBOUND_COMPONENT((UnitTestComponent01), a, b)

UNBOUND_COMPONENT((UnitTestComponent02), c, d)

UNBOUND_COMPONENT((UnitTestComponent03), e, f)

struct BadUnitTestComponent01 {
  BadUnitTestComponent01() { g = 20; }
  int g = 0;
};

#endif  // COR_UNIT_COMPONENTS_HPP
