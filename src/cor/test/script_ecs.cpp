#define UNBOUND_DO_EXPOSE_TYPES

#include "test_lock.hpp"
#include "unit_components.hpp"
#include <cor/cor.hpp>
#include <cor/entity.tcc>
#include <cor/entity_manager.tcc>
#include <scr/dynamic_object.hpp>
#include <scr/scr.tcc>
#include <scr/script.hpp>

using namespace cor;

TEST(ScriptEcs, GetAndSetComponent) {
  std::lock_guard lock(test_lock);

  EntityManager em;

  Entity entity = em.get_entity(em.create_entity());
  scr::Script script("", R"(

local cpp = require("cpp")
function func1(entity)
  local c1 = cpp.UnitTestComponent01.new()
  local c2 = cpp.UnitTestComponent02.new()
  c1.a = 1
  c1.b = 2
  c2.c = 3
  c2.d = 4

  entity.UnitTestComponent01 = c1
  entity.UnitTestComponent02 = c2

  c1 = entity.UnitTestComponent01
  c2 = entity.UnitTestComponent02
  return c1.a + c1.b + c2.c + c2.d    
end

function func2(entity)
  entity.UnitTestComponent01 = nil
  entity.UnitTestComponent02 = nil

  local c1 = cpp.UnitTestComponent01.new()
  c1.a = 5
  c1.b = 6
  entity.UnitTestComponent01 = c1

  return not entity.UnitTestComponent02
end

)");

  std::function<int(Entity)> func = script.return_value()["func1"];
  int i = func(entity);
  EXPECT_EQ(10, i);

  UnitTestComponent01* c1 = entity.get_if<UnitTestComponent01>();
  UnitTestComponent02* c2 = entity.get_if<UnitTestComponent02>();
  EXPECT_NE(nullptr, c1);
  EXPECT_NE(nullptr, c2);
  EXPECT_EQ(1, c1->a);
  EXPECT_EQ(2, c1->b);
  EXPECT_EQ(3, c2->c);
  EXPECT_EQ(4, c2->d);

  std::function<bool(Entity)> func2 = script.return_value()["func2"];
  bool is_c2_nil = func2(entity);
  EXPECT_TRUE(is_c2_nil);

  c1 = entity.get_if<UnitTestComponent01>();
  c2 = entity.get_if<UnitTestComponent02>();
  EXPECT_NE(nullptr, c1);
  EXPECT_EQ(nullptr, c2);
  EXPECT_EQ(5, c1->a);
  EXPECT_EQ(6, c1->b);
}

TEST(ScriptEcs, GetEntities) {
  std::lock_guard lock(test_lock);

  EntityManager em;
  {
    Entity entity = em.get_entity(em.create_entity());
    entity = UnitTestComponent01({1, 2});
  }

  {
    Entity entity = em.get_entity(em.create_entity());
    entity = UnitTestComponent02({3, 4});
  }

  {
    Entity entity = em.get_entity(em.create_entity());
    entity = UnitTestComponent01({5, 6});
    entity = UnitTestComponent02({7, 8});
  }

  scr::Script script("", R"(

local function get_sum(entities)
  local sum = 0
  for i,entity in ipairs(entities) do
    local c1 = entity.UnitTestComponent01
    local c2 = entity.UnitTestComponent02


    if c1 then
      sum = sum + c1.a + c1.b
    end

    if c2 then
      sum = sum + c2.c + c2.d
    end
  end 
 
  return sum
end

function func1(em)
  local sum = 0
  local entities = em:get_entities({"UnitTestComponent01", "UnitTestComponent02"})
  sum = sum + get_sum(entities)
  entities = em:get_entities({"UnitTestComponent01"})
  sum = sum + get_sum(entities)
  entities = em:get_entities({"UnitTestComponent02"})
  sum = sum + get_sum(entities)
  
  return sum
end


)");

  std::function<int(EntityManager&)> func = script.return_value()["func1"];
  int i = func(em);

  EXPECT_EQ(1 + 2 + 3 + 4 + 3 * (5 + 6 + 7 + 8), i);
}

TEST(ScriptEcs, CreateEntity) {
  std::lock_guard lock(test_lock);

  EntityManager em;
  scr::Script script("", R"(

function func1(em)
  local entity = em:create_entity()
  return entity
end

function is_valid(entity)
  return entity.is_valid
end

function get_handle(entity)
  return entity.get_handle
end

function get_key(entity)
  return entity.get_key
end

)");

  std::function<Entity(EntityManager&)> func1 = script.return_value()["func1"];
  Entity entity = func1(em);
  EXPECT_TRUE(entity.is_valid());

  std::function<bool(Entity&)> is_valid_func = script.return_value()["is_valid"];
  bool is_valid = is_valid_func(entity);
  EXPECT_TRUE(is_valid);

  std::function<EntityHandle(Entity&)> get_handle = script.return_value()["get_handle"];
  EntityHandle handle = get_handle(entity);
  EXPECT_EQ(entity.get_handle(), handle);

  std::function<Key(Entity&)> get_key = script.return_value()["get_key"];
  Key key = get_key(entity);
  EXPECT_EQ(entity.get_key(), key);
}