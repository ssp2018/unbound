#include "cor/event.hpp"

#include <cor/event_manager.hpp>
#include <cor/event_variant.hpp>

class CoolTest {
 public:
  int m_num;
  int m_increment;
  int m_omni_test;

  CoolTest(int increment) {
    m_num = 0;
    m_increment = increment;
    m_omni_test = 0;
  }
  std::function<void(const int* event)> get_cooltest_func() {
    return [this](const int* event) { testf(event); };
  }
  void testf(const int* event) { m_num += m_increment; };

  cor::EventManager::ListenID register_f(cor::EventManager& em) {
    return em.register_handler<int>([this](const int* event) { testf(event); });
  }

  cor::EventManager::ListenID register_with_macro(cor::EventManager& em) {
    return EVENT_REGISTER_MEMBER_FUNCTION_I(int, em, testf);
  }

  void omni_listen(const Event* e) {
    if (const TestEvent* te = std::get_if<TestEvent>(&e->data)) {
      m_omni_test += te->num;
    } else {
      m_omni_test--;
    }
  }
};
static int global = 0;

void increment_global(const int* event) { global++; }

TEST(EventTest, BasicLambda) {
  cor::EventManager em;
  int result = 0;

  cor::EventManager::ListenID lid1, lid2;

  lid1 = em.register_handler<int>([&result](const int* event) { result += *event; });

  em.send_event(10);
  em.update();

  EXPECT_EQ(result, 10);

  lid2 = em.register_handler<int>([&result](const int* event) { result++; });

  em.send_event(1);
  em.update();

  EXPECT_EQ(result, 12);

  em.remove_handler(lid1);
  em.remove_handler(lid2);

  EXPECT_EQ(em.get_total_listeners(), 0);
}

TEST(EventTest, GlobalFunc) {
  cor::EventManager em;
  {
    cor::EventManager::ListenID lid1;  // Check that ListenID's destructor removes listener

    lid1 = em.register_handler<int>(increment_global);

    em.send_event(1);
    em.update();

    EXPECT_EQ(global, 1);
  }
  EXPECT_EQ(em.get_total_listeners(), 0);
}

TEST(EventTest, MemberFunction) {
  cor::EventManager em;

  cor::EventManager::ListenID lid1, lid2, lid3, lid4;

  // On an int event, ct1 increments its m_num by 1
  // ct2 increments by 2
  CoolTest ct1(1), ct2(2), ct3(3), ct4(4);

  lid1 = ct1.register_f(em);
  lid2 = em.register_handler(ct2.get_cooltest_func());
  lid3 = ct3.register_with_macro(em);
  lid4 = EVENT_REGISTER_MEMBER_FUNCTION_E(int, em, ct4, testf);

  em.send_event(1);
  em.update();
  em.send_event(1);
  em.update();

  EXPECT_EQ(ct1.m_num, 2);
  EXPECT_EQ(ct2.m_num, 4);
  EXPECT_EQ(ct3.m_num, 6);
  EXPECT_EQ(ct4.m_num, 8);

  em.remove_handler(lid1);
  em.remove_handler(lid2);
  em.remove_handler(lid3);
  em.remove_handler(lid4);

  EXPECT_EQ(em.get_total_listeners(), 0);
}

TEST(EventTest, Loop) {
  cor::EventManager em;

  cor::EventManager::ListenID lid1, lid2, lid3;

  int result = 0;

  // On an int event, ct1 increments its m_num by 1
  // ct2 increments by 2
  CoolTest ct1(1), ct2(2);

  lid1 = ct1.register_f(em);
  lid2 = em.register_handler(ct2.get_cooltest_func());
  lid3 = em.register_handler<int>([&result](const int* event) { result++; });

  for (int i = 0; i < 1000; i++) {
    em.send_event(1);
    em.update();
  }

  EXPECT_EQ(ct1.m_num, 1000);
  EXPECT_EQ(ct2.m_num, 2000);
  EXPECT_EQ(result, 1000);

  em.remove_handler(lid2);
  em.remove_handler(lid3);

  for (int i = 0; i < 1000; i++) {
    em.send_event(1);
    em.update();
  }
  EXPECT_EQ(ct1.m_num, 2000);
  em.remove_handler(lid1);

  EXPECT_EQ(em.get_total_listeners(), 0);
}

TEST(EventTest, CustomEventType) {
  cor::EventManager em;
  int result = 0;
  std::string str;
  cor::EventManager::ListenID lid1;

  lid1 = em.register_handler<TestEvent>([&result, &str](const TestEvent* event) {
    result += event->num;
    str = event->str;
  });

  // Delete the TestEvent before update()
  {
    TestEvent te;
    te.num = 5;
    te.str = "xD";
    em.send_event(te);
  }
  em.update();

  EXPECT_EQ(result, 5);
  EXPECT_EQ(str, "xD");

  em.remove_handler(lid1);

  EXPECT_EQ(em.get_total_listeners(), 0);
}

TEST(EventTest, Omni) {
  cor::EventManager em;
  cor::EventManager::ListenID lid1;

  TestEvent te;
  te.num = 5;
  te.str = "xD";

  CoolTest ct(1);
  lid1 = em.register_omni_handler([&ct](const Event* e) { ct.omni_listen(e); });

  em.send_event(te);
  em.update();

  EXPECT_EQ(ct.m_omni_test, 5);

  em.send_event(1);
  em.update();

  EXPECT_EQ(ct.m_omni_test, 4);

  em.remove_handler(lid1);

  EXPECT_EQ(em.get_total_listeners(), 0);
}