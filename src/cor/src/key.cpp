#include "cor/key.hpp"

#include "cor/component_name.hpp"
#include <scr/scr.tcc>

namespace cor {

bool Key::operator==(const Key& other) const {
  //
  return m_bits == other.m_bits;
}

const ComponentFlags& Key::get_bits() const {
  //
  return m_bits;
}

bool Key::has(ComponentIDInteger component_id) const { return m_bits.test(component_id); }
void Key::include(ComponentIDInteger component_id) { m_bits.set(component_id); }
void Key::exclude(ComponentIDInteger component_id) { m_bits.reset(component_id); }

bool has(const Key& key, std::vector<std::string> component_names) {
  for (const std::string& name : component_names) {
    if (!key.has(ComponentName::get_id(name))) {
      return false;
    }
  }
  return true;
}
void include(Key& key, std::vector<std::string> component_names) {
  for (const std::string& name : component_names) {
    key.include(ComponentName::get_id(name));
  }
}
void exclude(Key& key, std::vector<std::string> component_names) {
  for (const std::string& name : component_names) {
    key.exclude(ComponentName::get_id(name));
  }
}

Key create(std::vector<std::string> component_names) {
  Key key;
  include(key, component_names);
  return key;
}

static bool initialize_script_exposition = []() {
  scr::expose_type<Key>("Key", scr::Constructors<Key()>(), "has", has, "include", include,
                        "exclude", exclude, "create", create);
  return true;
}();

}  // namespace cor
