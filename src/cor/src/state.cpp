#include "cor/state.hpp"

#include <bse/asset.hpp>
#include <bse/edit.hpp>
#include <bse/runtime_asset.hpp>
#include <ext/ext.hpp>
#include <gfx/context.hpp>
#include <gfx/font.hpp>
#include <gfx/text_cache.hpp>
#include <scr/script.hpp>

namespace cor {

State::State(cor::Game& game, gfx::Window& window, aud::AudioManager& audio_mgr)
    : m_window{window},
      m_game{game},
      m_context{audio_mgr},
      m_input_mgr{window, m_context.event_mgr},
      m_system_mgr{m_context} {
  //

  m_context.system_id_proxy = cor::SystemIDProxy(&m_system_mgr);

  m_refresh_listener =
      m_context.event_mgr.register_handler<RefreshAssetsEvent>([](const RefreshAssetsEvent* event) {
        //
        LOG(NOTICE) << "Reloading assets...\n";
        // bse::Loader<gfx::Model>::refresh();
        gfx::Context::run([]() {
          // Do not change the ordering of these
          // just willy-nilly. They depend on each other.

          // bse::Loader<gfx::Model>::refresh();
          bse::Loader<gfx::Shader>::refresh();
          bse::Loader<gfx::Material>::refresh();  // Must come after shader
          bse::Loader<gfx::Font>::refresh();
          bse::RuntimeLoader<gfx::TextCache>::refresh();  // Must come after font
        });

        bse::Loader<aud::SoundBuffer>::refresh();
        // bse::Loader<phy::CollisionMesh>::Refresh(); // This sometimes crashes
        bse::Loader<scr::Script>::refresh();

        LOG(NOTICE) << "Finished!\n";
      });

  m_system_enable_listener =
      m_context.event_mgr.register_handler<EnableSystem>([this](const EnableSystem* event) {
        //
        m_system_mgr.enable_system(event->system_name);
      });

  m_system_disable_listener =
      m_context.event_mgr.register_handler<DisableSystem>([this](const DisableSystem* event) {
        //
        m_system_mgr.disable_system(event->system_name);
      });

  m_system_toggle_listener =
      m_context.event_mgr.register_handler<ToggleSystem>([this](const ToggleSystem* event) {
        //
        m_system_mgr.toggle_system(event->system_name);
      });

  ///////////////////////
  ///////////////////////
  ///////////////////////
}

void State::initialize() {
  //
}
const cor::SystemManager& State::get_system_manager() const { return m_system_mgr; }

bool State::update(float dt) {
  //
  m_context.dt = m_context.speed_factor * dt;
  m_context.unslowed_dt = dt;

  // Send events from previous frame
  // m_context.event_mgr.update();

  // Poll and send input events
  m_context.event_mgr.update();

  // Increment context's frame counter
  m_context.frame_num++;

  struct FunctorTask : public bse::Task {
    void kernel() override { func(); }

    std::function<void()> func;
  };

  FunctorTask task;
  task.func = [this]() {
    m_system_mgr.read_update();
    m_system_mgr.write_update();
    m_system_mgr.write_update_late();
  };

  bse::TaskScheduler* scheduler = bse::TaskScheduler::get();
  scheduler->wait_on_task(scheduler->add_task(task));

  return true;
}

bool State::display() {
  //
  return true;
}

}  // namespace cor
