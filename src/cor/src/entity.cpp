#include "cor/entity.hpp"

#include "bse/assert.hpp"
#include "cor/entity_manager.hpp"
#include <scr/dynamic_object.hpp>
#include <scr/scr.tcc>

REFLECT((cor::Entity))
REFLECT((cor::EntityHandle))
REFLECT((cor::Key))

namespace cor {

ConstEntity::ConstEntity()
    : m_handle{EntityHandle::NULL_HANDLE},  //
      m_man{nullptr} {
  //
}

ConstEntity::ConstEntity(const EntityManager* man, EntityHandle handle)
    : m_man{const_cast<EntityManager*>(man)},  //
      m_handle{handle} {
  //
}

ConstEntity::ConstEntity(const ConstEntity& other) : m_man{other.m_man}, m_handle{other.m_handle} {
  //
}

ConstEntity& ConstEntity::operator=(ConstEntity&& other) {
  if (this != &other) {
    m_man = other.m_man;
    m_handle = other.m_handle;

    other.m_handle = EntityHandle::NULL_HANDLE;
  }
  return *this;
}

ConstEntity& ConstEntity::operator=(const ConstEntity& other) {
  if (this != &other) {
    m_man = other.m_man;
    m_handle = other.m_handle;
  }
  return *this;
}

bool ConstEntity::has(Key key) const {
  //
  ASSERT(m_man != nullptr) << "Tried to use an uninitialized/NULL Entity!";
  return m_man->has(m_handle, key);
}

bool ConstEntity::is_valid() const {
  //
  return m_man != nullptr && m_man->is_valid(m_handle);
}

Key ConstEntity::get_key() const {
  //
  ASSERT(m_man != nullptr) << "Tried to use an uninitialized/NULL Entity!";
  return m_man->get_key(m_handle);
}

Entity::Entity() : ConstEntity() {
  //
}

Entity::Entity(const Entity& other) : ConstEntity(other) {
  //
}

Entity::Entity(EntityManager* man, EntityHandle handle) : ConstEntity(man, handle) {
  //
}

EntityHandle ConstEntity::get_handle() const {
  //
  return m_handle;
}

Entity& Entity::operator=(Entity&& other) {
  if (this != &other) {
    m_man = other.m_man;
    m_handle = other.m_handle;

    other.m_handle = EntityHandle::NULL_HANDLE;
  }
  return *this;
}

Entity& Entity::operator=(Entity& other) {
  if (this != &other) {
    m_man = other.m_man;
    m_handle = other.m_handle;
  }
  return *this;
}

const void* ConstEntity::raw_get(std::string_view component_name) const {
  return m_man ? m_man->raw_get(m_handle, component_name) : nullptr;
}

void Entity::raw_set(std::string_view component_name, void* data) {
  if (m_man) {
    m_man->raw_set(m_handle, component_name, data);
  }
}

void* Entity::raw_attach(std::string_view component_name) {
  return m_man ? m_man->raw_attach(m_handle, component_name) : nullptr;
}
void Entity::raw_detach(std::string_view component_name) {
  if (m_man) {
    m_man->raw_detach(m_handle, component_name);
  }
}

void Entity::destroy() {
  //
  ASSERT(m_man != nullptr) << "Tried to use an uninitialized/NULL Entity!";
  m_man->destroy_entity(m_handle);
}

scr::DynamicObject component_getter_const(ConstEntity& entity, const std::string& key);

scr::DynamicObject component_getter(Entity& entity, const std::string& key) {
  return component_getter_const(entity, key);
}

scr::DynamicObject component_getter_const(ConstEntity& entity, const std::string& key) {
  if (key == "is_valid") {
    return entity.is_valid();
  }

  if (key == "get_handle") {
    return entity.get_handle();
  }

  // if (!entity.is_valid()) {
  if (entity.get_handle().register_index > 1500000) {
    return nullptr;
  }

  if (key == "get_key") {
    return entity.get_key();
  } else if (key == "get_component_names") {
    std::vector<std::string> names;
    ComponentFlags flags = entity.get_key().get_bits();
    for (size_t i = 0; i < flags.size(); i++) {
      if (flags.test(i)) {
        names.push_back(std::string(ComponentName::get_name(i)));
      }
    }
    return names;
  }

  const void* v = entity.raw_get(key);
  if (!v) {
    return nullptr;
  }

  scr::DynamicObject::Userdata data;
  data.data = const_cast<void*>(v);
  data.type_name = key;
  return data;
}

void component_setter(Entity& entity, const std::string& key, const scr::DynamicObject& object) {
  if (object.get_if<std::nullptr_t>()) {
    entity.raw_detach(key);
  }

  scr::DynamicObject::Userdata data = object;
  if (data.type_name != key || !data.data) {
    return;
  }

  if (!entity.raw_get(key)) {
    entity.raw_attach(key);
  }
  entity.raw_set(key, data.data);
}

static bool initialize_script_exposition = []() {
  scr::expose_type<Entity>("Entity", scr::Constructors<Entity()>(), "is_valid", &Entity::is_valid,
                           "get_handle", &Entity::get_handle, "get_key", &Entity::get_key,
                           "destroy", &Entity::destroy);
  scr::expose_getter<Entity>("Entity", component_getter);
  scr::expose_setter<Entity>("Entity", component_setter);
  scr::expose_type<ConstEntity>("ConstEntity", scr::Constructors<ConstEntity()>(), "is_valid",
                                &ConstEntity::is_valid, "get_handle", &ConstEntity::get_handle,
                                "get_key", &ConstEntity::get_key);
  scr::expose_getter<ConstEntity>("ConstEntity", component_getter_const);
  return true;
}();

}  // namespace cor