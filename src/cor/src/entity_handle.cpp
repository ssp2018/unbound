#include "cor/entity_handle.hpp"

#include <scr/scr.tcc>

namespace cor {
const EntityHandle EntityHandle::NULL_HANDLE;

EntityHandle::EntityHandle() {}
EntityHandle::EntityHandle(int handle) { full_handle = handle; }

EntityHandle::operator value_type() {
  return full_handle;  //
}

bool EntityHandle::operator==(const cor::EntityHandle& other) const {
  return full_handle == other.full_handle;
}

bool EntityHandle::operator!=(const cor::EntityHandle& other) const {
  return !operator==(other);  //
}

static bool initialize_script_exposition = []() {
  scr::expose_type<EntityHandle>("EntityHandle", scr::Constructors<EntityHandle()>());
  return true;
}();

}  // namespace cor
