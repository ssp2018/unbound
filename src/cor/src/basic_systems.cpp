#include "cor/basic_systems.hpp"

#include "bse/log.hpp"
#include "cor/basic_components.hpp"
#include "cor/entity_manager.tcc"
#include "cor/key.hpp"
#include <cor/entity.tcc>
#include <cor/frame_context.hpp>

namespace cor {
void TestSystem::read_update(const FrameContext& context) {
  //
  Key key = Key::create<TestComponent>();

  auto entities = context.entity_mgr.get_entities(key);
  for (auto& entity : entities) {
    //
    const TestComponent& tc = context.entity_mgr.get_entity(entity);

    // Send whatever you need from the ECS to your layer for processing.
    LOG(NOTICE) << "Read layer " << tc.a << "\n";
  }
}

void TestSystem::write_update(FrameContext& context) {
  //
  Key key = Key::create<TestComponent>();

  auto entities = context.entity_mgr.get_entities(key);
  for (auto& entity : entities) {
    TestComponent& tc = context.entity_mgr.get_entity(entity);

    // Update the ECS to match any changes made in your layer.
    // Only do this if other layers are at all interested in the state of your layer.
    tc.a++;
  }
}

};  // namespace cor
