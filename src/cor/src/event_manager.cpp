#include "cor/event.hpp"
#include <cor/event_manager.hpp>
#include <cor/event_variant.hpp>
#include <scr/dynamic_table.hpp>

template class std::vector<std::tuple<bool, std::function<void(const Event*)>, size_t>>;
template class std::unique_ptr<
    std::array<std::vector<Event>, cor::event_manager::detail::m_num_buffers>>;

namespace bse {
template <>
void serialize<Event>(const Event& self, ::bse::SerializedData& data) {
  serialize(self.data, data);
  return;
}

void deserialize(Event& self, const ::bse::SerializedData& data, int& offset) {
  deserialize(self.data, data, offset);
}
}  // namespace bse

namespace cor {

using namespace event_manager::detail;

EventManager::EventManager()
    : m_events(new std::array<std::vector<Event>, cor::event_manager::detail::m_num_buffers>()) {}

cor::EventManager::~EventManager() {}

template <typename EventT>
EventManager::ListenID EventManager::register_handler(
    const std::function<void(const EventT*)>& handler) {
  m_handlers[Index<EventT, decltype(Event::data)>::value].push_back(
      std::make_tuple(true, std::function<void(const void*)>([handler](const void* event) {
                        handler((const EventT*)event);
                      }),
                      m_next_ID));
  return ListenID(this, m_next_ID++);
};

template <typename T>
void cor::EventManager::send_event(const T& e) {
  (*m_events)[(m_frame_ind + 1) % m_num_buffers].push_back({e});
}

template <typename T, typename... TailT>
void instantiator() {
  ((EventManager*)1)->send_event(*(T*)1);
  ((EventManager*)1)->register_handler(*(std::function<void(const T*)>*)1);
  if constexpr (sizeof...(TailT) > 0) {
    instantiator<TailT...>();
  }
}

template void instantiator<EVENT_TYPE_LIST>();
template void EventManager::send_event<Event>(const Event&);
template void EventManager::send_event<decltype(Event::data)>(const decltype(Event::data)&);

EventManager::ListenID EventManager::register_omni_handler(
    const std::function<void(const Event*)>& handler) {
  m_omni_handlers.push_back(std::make_tuple(true, handler, m_next_ID));
  return ListenID(this, m_next_ID++);
};

void cor::EventManager::update() {
  (*m_events)[m_frame_ind].clear();
  m_frame_ind = (m_frame_ind + 1) % m_num_buffers;
  for (Event& e : (*m_events)[m_frame_ind]) {
    // Notify regular listeners
    for (auto& [enabled, func, func_ID] : m_handlers[e.data.index()]) {
      if (enabled) {
        func((const void*)&e);
      }
    }

    // Notify omni listeners
    for (auto& [enabled, func, func_ID] : m_omni_handlers) {
      if (enabled) {
        func(&e);
      }
    }
  }
}

void cor::EventManager::remove_handler(ListenID& ID) {
  // Loop through handlers and remove the requested one
  bool removed = false;

  // First check regular event handlers
  // Loop through pair of indices and vectors
  for (auto& [event_ID, handler_vec] : m_handlers) {
    if (removed) {
      break;
    }

    // Loop through tuple of functions and ID's in vector
    for (auto it = handler_vec.begin(); it != handler_vec.end(); it++) {
      auto& [enabled, func, func_ID] = *it;
      if (func_ID == ID.m_ID) {
        handler_vec.erase(it);
        removed = true;
        break;
      }
    }
  }

  // Check omni handlers
  if (!removed) {
    for (auto it = m_omni_handlers.begin(); it != m_omni_handlers.end(); it++) {
      auto& [enabled, func, func_ID] = *it;
      if (func_ID == ID.m_ID) {
        m_omni_handlers.erase(it);
        removed = true;
        break;
      }
    }
  }

  // Clear ListenID's pointer to prevent destructor from calling remove_handler()
  ID.m_em = nullptr;
}

size_t EventManager::get_total_listeners() {
  size_t num = 0;
  // Loop through map and sum the sizes of containing arrays
  for (auto& [event_ID, handler_vec] : m_handlers) {
    num += handler_vec.size();
  }
  return num + m_omni_handlers.size();
}

void EventManager::enable_handler(ListenID& ID) {
  //
  // Find listener among regular event handlers
  for (auto& [event_ID, handler_vec] : m_handlers) {
    for (auto& [enabled, func, func_ID] : handler_vec) {
      if (func_ID == ID.m_ID) {
        enabled = true;
        return;
      }
    }
  }
}

void EventManager::disable_handler(ListenID& ID) {
  //
  for (auto& [event_ID, handler_vec] : m_handlers) {
    for (auto& [enabled, func, func_ID] : handler_vec) {
      if (func_ID == ID.m_ID) {
        enabled = false;
        return;
      }
    }
  }
}

void EventManager::toggle_handler(ListenID& ID) {
  //
  for (auto& [event_ID, handler_vec] : m_handlers) {
    for (auto& [enabled, func, func_ID] : handler_vec) {
      if (func_ID == ID.m_ID) {
        enabled = !enabled;
        return;
      }
    }
  }
}

size_t EventManager::get_num_events() const {
  // size_t size = 0;
  // for (auto& buffer : ) {
  //   size += buffer.size();
  // }
  return (*m_events)[m_frame_ind].size();
}

std::vector<size_t> EventManager::get_event_indices() const {
  //
  std::vector<size_t> result;
  for (auto& event : (*m_events)[m_frame_ind]) {
    //
    result.push_back(event.data.index());
  }
  return result;
}

}  // namespace cor