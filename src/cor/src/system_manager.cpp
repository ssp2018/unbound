#include "cor/system_manager.hpp"

#include <bse/task_handle.hpp>
#include <bse/task_scheduler.hpp>
namespace cor {

SystemManager::SystemManager(FrameContext& context)
    : m_context{context}, m_read_time(0.f), m_write_time(0.f) {
  m_tasks.reserve(30);
}

//
// void SystemManager::register_system(ReadSystemFunc read_sys_func, WriteSystemFunc write_sys_func)
// {
//   //
//   m_read_funcs.push_back(read_sys_func);
//   m_write_funcs.push_back(write_sys_func);
// }

SystemManager::Diagnostics SystemManager::compile_diagnostics() const {
  return SystemManager::Diagnostics{m_read_time, m_write_time, m_system_read_times,
                                    m_system_write_times};
}

void SystemManager::read_update() {
  std::chrono::time_point<std::chrono::steady_clock> pre_read = std::chrono::steady_clock::now();
  //
  // for (auto& func : m_read_funcs) {
  // std::invoke(func, context);
  // }

  struct UpdateTask : public bse::Task {
    virtual void kernel() override {
      std::chrono::time_point<std::chrono::steady_clock> pre = std::chrono::steady_clock::now();
      system->read_update(*context);
      std::chrono::time_point<std::chrono::steady_clock> post = std::chrono::steady_clock::now();

      *dt = static_cast<float>(
                std::chrono::duration_cast<std::chrono::microseconds>(post - pre).count()) /
            1000000.f;
    }

    System* system;
    const FrameContext* context;
    float* dt;
  };

  struct Bucket {
    struct Entry {
      bool operator>(const Entry& other) const { return length > other.length; }

      UpdateTask task;
      float length;
    };

    float length = 0.f;
    std::vector<Entry> entries;
  };

  struct UpdateBucketTask : public bse::Task {
    void kernel() override {
      for (Bucket::Entry& entry : bucket.entries) {
        entry.task.kernel();
      }
    }

    Bucket bucket;
  };

  m_tasks.clear();
  bse::TaskScheduler* scheduler = bse::TaskScheduler::get();

  std::array<Bucket, bse::TaskScheduler::NUM_THREADS> buckets;

  std::vector<Bucket::Entry> entries;
  for (auto& [key, pair] : m_systems) {
    auto& [enabled, system] = pair;
    if (enabled) {
      m_tasks.emplace_back();
      Task& data = m_tasks.back();
      data.dt.reset(new float(0.f));
      data.name = key;

      UpdateTask task;
      task.system = system.get();
      task.context = &m_context;
      task.dt = data.dt.get();

      Bucket::Entry entry;
      entry.task = task;
      entry.length = m_system_read_times[key];
      entries.push_back(entry);
    }
  }

  std::sort(entries.begin(), entries.end(), std::greater<>());

  for (const Bucket::Entry& entry : entries) {
    Bucket* min_bucket = &buckets[0];
    for (size_t i = 1; i < buckets.size(); i++) {
      if (buckets[i].length < min_bucket->length) {
        min_bucket = &buckets[i];
      }
    }
    min_bucket->length += entry.length;
    min_bucket->entries.push_back(entry);
  }

  std::vector<bse::TaskHandle> tasks;
  for (const Bucket& bucket : buckets) {
    UpdateBucketTask bucket_task;
    bucket_task.bucket = bucket;
    tasks.push_back(scheduler->add_task(bucket_task));
  }

  for (const bse::TaskHandle& task : tasks) {
    scheduler->wait_on_task(task);
  }

  for (const Task& task : m_tasks) {
    m_system_read_times[task.name] = m_system_read_times[task.name] * 0.95f + *task.dt * 0.05f;
  }

  std::chrono::time_point<std::chrono::steady_clock> post_read = std::chrono::steady_clock::now();
  m_read_time =
      static_cast<float>(
          std::chrono::duration_cast<std::chrono::microseconds>(post_read - pre_read).count()) /
      1000000.f;
}

void SystemManager::write_update() {
  std::chrono::time_point<std::chrono::steady_clock> pre_write = std::chrono::steady_clock::now();
  //
  // for (auto& func : m_write_funcs) {
  // std::invoke(func, context);
  // }

  for (auto& [key, pair] : m_systems) {
    auto& [enabled, system] = pair;
    if (enabled) {
      std::chrono::time_point<std::chrono::steady_clock> pre = std::chrono::steady_clock::now();
      system->write_update(m_context);
      std::chrono::time_point<std::chrono::steady_clock> post = std::chrono::steady_clock::now();

      m_system_write_times[key] =
          static_cast<float>(
              std::chrono::duration_cast<std::chrono::microseconds>(post - pre).count()) /
          1000000.f;
    }
  }
  std::chrono::time_point<std::chrono::steady_clock> post_write = std::chrono::steady_clock::now();

  m_write_time =
      static_cast<float>(
          std::chrono::duration_cast<std::chrono::microseconds>(post_write - pre_write).count()) /
      1000000.f;
}

void SystemManager::write_update_late() {
  for (auto& [key, pair] : m_systems) {
    auto& [enabled, system] = pair;
    if (enabled) {
      system->write_update_late(m_context);
    }
  }
}

void SystemManager::enable_system(std::string id) {
  //
  auto& [enabled, system] = m_systems[id];
  enabled = true;
  if (system != nullptr) {
    system->enable_listeners(m_context);
  } else {
    LOG(WARNING) << "Tried to enable a system that was not registered! (" << id << ")";
  }
}

void SystemManager::disable_system(std::string id) {
  //
  auto& [enabled, system] = m_systems[id];
  enabled = false;
  if (system != nullptr) {
    system->disable_listeners(m_context);
  } else {
    LOG(WARNING) << "Tried to disable a system that was not registered! (" << id << ")";
  }
}

void SystemManager::toggle_system(std::string id) {
  //
  auto& [enabled, system] = m_systems[id];
  enabled = !enabled;
  if (system != nullptr) {
    system->toggle_listeners(m_context);
  } else {
    LOG(WARNING) << "Tried to toggle a system that was not registered! (" << id << ")";
  }
}

void SystemManager::disable_all_systems() {
  for (auto& [id, pair] : m_systems) {
    auto& [enabled, system] = pair;
    enabled = false;
    system->disable_listeners(m_context);
  }
}

SystemManager::ActiveFingerprint SystemManager::get_active_fingerprint() const {
  //
  ActiveFingerprint af;
  for (const auto& [id, pair] : m_systems) {
    const auto& [enabled, system] = pair;
    af.active_map[id] = enabled;
  }
  return af;
}

void SystemManager::set_active_fingerprint(const ActiveFingerprint& af) {
  //
  for (auto& [id, enabled] : af.active_map) {
    if (enabled) {
      enable_system(id);
    } else {
      disable_system(id);
    }
  }
}

}  // namespace cor
