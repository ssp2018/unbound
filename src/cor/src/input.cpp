#include "cor/input.hpp"

#include "cor/event_manager.hpp"
#include "gfx/window.hpp"
#include <bse/edit.hpp>
#include <scr/dynamic_object.hpp>
#include <scr/dynamic_table.hpp>

namespace inp {

InputManager::InputManager(gfx::Window& w, cor::EventManager& e) : m_em{e}, m_w{w} {
  glfwGetWindowSize(w.get_window(), &m_mouse_origin_x, &m_mouse_origin_y);
  m_mouse_origin_x = m_mouse_origin_x / 2;
  m_mouse_origin_y = m_mouse_origin_y / 2;
  bind_keys();
  bind_editor_keys();
}

// PUBLIC

void InputManager::poll_menu_input() {
  //
  poll_key(&m_key_jump);
  poll_key(&m_key_refresh_assets);
  if (m_key_jump.clicked) m_em.send_event(MenuConfirm());
  if (m_key_refresh_assets.clicked) m_em.send_event(RefreshAssetsEvent());
}

void InputManager::lock_mouse() {
  glfwSetInputMode(m_w.get_window(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
  input_debug = true;
}

void InputManager::unlock_mouse() {
  glfwSetInputMode(m_w.get_window(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
  input_debug = false;
}

void InputManager::poll_input() {
  gamepad();
  // Poll Keys
  poll_press(&m_key_move_forward);
  poll_press(&m_key_move_left);
  poll_press(&m_key_move_back);
  poll_press(&m_key_move_right);
  poll_press(&m_key_run);
  poll_press(&m_key_plus);
  poll_press(&m_key_minus);
  poll_key(&m_key_jump);
  poll_key(&m_key_dash);
  poll_key(&m_key_cancel);
  poll_key(&m_key_piercing_arrow);
  poll_key(&m_key_explosive_arrow);
  poll_key(&m_key_rope_arrow);
  poll_key(&m_key_arrow_up);
  poll_key(&m_key_arrow_left);
  poll_key(&m_key_arrow_down);
  poll_key(&m_key_arrow_right);
  poll_key(&m_key_number_one);
  poll_key(&m_key_number_two);
  poll_key(&m_key_number_three);
  poll_key(&m_key_number_four);
  poll_key(&m_key_number_five);
  poll_key(&m_key_number_six);
  poll_key(&m_key_number_seven);
  poll_key(&m_key_number_eight);
  poll_key(&m_key_number_nine);
  poll_key(&m_key_number_zero);
  poll_key(&m_key_escape);
  poll_key(&m_key_toggle_mouse_input);
  poll_key(&m_key_refresh_assets);
  poll_key(&m_key_toggle_bse_edit);
  poll_key(&m_key_change_camera);
  poll_key(&m_key_toggle_debug_camera_movement);
  poll_key(&m_key_pause);
  poll_key(&m_key_editor);
  poll_key(&m_key_quick_save);
  poll_key(&m_key_quick_load);
  poll_key(&m_key_start_recording);
  poll_key(&m_key_stop_recording);
  poll_key(&m_key_start_playback);
  poll_mouse_button(&m_key_aim, StartAimEvent(), StopAimEvent());
  poll_mouse_button(&m_key_draw, StartDrawEvent(), StopDrawEvent());

  poll_mouse_movement();

  // Reset movement
  m_forward = 0.0;
  m_right = 0.0;

  // Calculate new movement
  if (m_key_move_forward.pressed) m_forward += 1.0;
  if (m_key_move_left.pressed) m_right -= 1.0;
  if (m_key_move_back.pressed) m_forward -= 1.0;
  if (m_key_move_right.pressed) m_right += 1.0;

  // Normalize direction vector
  if (m_forward != 0 && m_right != 0) {
    m_forward = m_forward * 0.7;
    m_right = m_right * 0.7;
  }

  if (!bse::Edit::is_capturing_keys()) {
    if (m_forward != 0 || m_right != 0) send_move_event();
    if (m_key_jump.clicked) m_em.send_event(JumpEvent());
    if (m_key_dash.clicked) m_em.send_event(DashEvent());
    if (m_key_cancel.clicked) m_em.send_event(CancelActionEvent());
    if (m_key_run.pressed) m_em.send_event(SlowMotionEvent());
    if (m_key_piercing_arrow.clicked) m_em.send_event(NormalArrowModeEvent());
    if (m_key_explosive_arrow.clicked) m_em.send_event(ExplosiveArrowModeEvent());
    if (m_key_rope_arrow.clicked) m_em.send_event(RopeEvent());
    if (m_key_arrow_up.clicked) std::cout << "up" << std::endl;
    if (m_key_arrow_left.clicked) std::cout << "left" << std::endl;
    if (m_key_arrow_down.clicked) std::cout << "down" << std::endl;
    if (m_key_arrow_right.clicked) std::cout << "right" << std::endl;
    if (m_key_number_one.clicked) std::cout << "1" << std::endl;
    if (m_key_number_two.clicked) std::cout << "2" << std::endl;
    if (m_key_number_three.clicked) std::cout << "3" << std::endl;
    if (m_key_number_four.clicked) std::cout << "4" << std::endl;
    if (m_key_number_five.clicked) std::cout << "5" << std::endl;
    if (m_key_number_six.clicked) m_em.send_event(TookHornEvent());
    if (m_key_number_seven.clicked) m_em.send_event(SpawnColeEvent());
    if (m_key_number_eight.clicked) m_em.send_event(KilledSkullEvent());
    if (m_key_number_nine.clicked) m_em.send_event(AddExplosiveArrowsEvent());
    if (m_key_number_zero.clicked) std::cout << "0" << std::endl;
    if (m_key_escape.clicked) m_em.send_event(PauseGameEvent());
    if (m_key_toggle_mouse_input.clicked) toggle_input_debug();
    if (m_key_refresh_assets.clicked) m_em.send_event(RefreshAssetsEvent());
    if (m_key_toggle_bse_edit.clicked) m_em.send_event(ToggleBseEditEvent());
    if (m_key_change_camera.clicked) m_em.send_event(ChangeCameraEvent());
    if (m_key_toggle_debug_camera_movement.clicked)
      m_em.send_event(ToggleDebugCameraMovementEvent());
    if (m_key_plus.pressed) m_em.send_event(ChangeMasterVolumeEvent(true));
    if (m_key_minus.pressed) m_em.send_event(ChangeMasterVolumeEvent(false));
    if (m_key_pause.pressed) m_em.send_event(PauseGameEvent());
    if (m_key_editor.pressed) m_em.send_event(EnterEditorEvent());
    if (m_key_quick_save.clicked) m_em.send_event(QuickSave{});
    if (m_key_quick_load.clicked) m_em.send_event(QuickLoad{});
    if (m_key_start_recording.clicked) m_em.send_event(StartEventRecording{});
    if (m_key_stop_recording.clicked) m_em.send_event(StopEventRecording{});
    if (m_key_start_playback.clicked) m_em.send_event(StartEventPlayback{});
  }

  if ((m_mouse_delta_x != 0 || m_mouse_delta_y != 0) && input_debug) send_mouse_movement_event();
}

void InputManager::poll_editor_input() {
  poll_mouse_movement();

  if (!bse::Edit::is_capturing_keys()) {
    poll_key(&m_editor_q);
    poll_key(&m_editor_w);
    poll_key(&m_editor_a);
    poll_key(&m_editor_s);
    poll_key(&m_editor_d);
    poll_key(&m_editor_shift);
    poll_key(&m_editor_space);
    poll_key(&m_editor_e);
    poll_key(&m_editor_c);
    poll_key(&m_editor_v);
    poll_key(&m_editor_p);
    poll_key(&m_editor_i);
    poll_key(&m_editor_ctrl);

    poll_key(&m_editor_r);
    poll_key(&m_editor_x);
    poll_key(&m_editor_y);
    poll_key(&m_editor_z);
    poll_key(&m_editor_f);
    poll_key(&m_editor_g);
    poll_key(&m_editor_delete);
    poll_key(&m_editor_quit);
  }

  if (!bse::Edit::is_capturing_mouse()) {
    poll_mouse_button(&m_editor_lmb, int{0}, int{0});
    poll_mouse_button(&m_editor_rmb, int{0}, int{0});
    if (m_editor_ctrl.pressed) {
      poll_mouse_button(&m_editor_left_click, EditorCtrlClick(), EndLeftMousePress());
    } else {
      poll_mouse_button(&m_editor_left_click,
                        StartLeftMousePress{glm::ivec2(m_mouse_x_pos, m_mouse_y_pos)},
                        EndLeftMousePress());
    }
    // poll_mouse_button(&m_key_draw, StartDrawEvent(), StopDrawEvent());
  }

  poll_key(&m_key_toggle_bse_edit);
  poll_key(&m_key_refresh_assets);
  poll_key(&m_editor_play);

  // poll_mouse_button(&m_key_draw, StartDrawEvent(), StopDrawEvent());

  if (m_editor_quit.clicked) m_em.send_event(CloseApplication());
  if (m_key_refresh_assets.clicked) m_em.send_event(RefreshAssetsEvent());

  if (m_editor_q.clicked) {
    toggle_input_debug();
    m_em.send_event(EditorTransform{EditorTransform::NONE});
    m_editor_camera = !m_editor_camera;
  }
  if (m_key_toggle_bse_edit.clicked) m_em.send_event(ToggleBseEditEvent());

  if (m_editor_ctrl.pressed) {
    if (m_editor_shift.pressed && m_editor_r.clicked) {
      // Send Reload all event
    } else if (m_editor_z.clicked) {
      // Send Undo event
    } else if (m_editor_y.clicked) {
      // Send Redo event
    } else if (m_editor_s.clicked) {
      // Send Save event
    } else if (m_editor_r.clicked) {
      // Send Reload event
    }
  }

  if (m_editor_play.clicked) {
    m_em.send_event(EditorPlayEvent());
  }

  if (m_editor_ctrl.pressed) {
    if (m_editor_s.clicked) {
      ScriptEvent e = scr::detail::State::get().create_table_with();
      e["type"] = "save_scene";
      m_em.send_event(e);
    }
  }

  if (m_editor_camera) {
    float forward = 0.f;
    float right = 0.f;
    float speed = 5.f;
    if (m_editor_shift.pressed) {
      speed = 20.f;
    }

    // Camera on
    if (m_editor_w.pressed) {
      // Send move forward event
      forward += speed;
    } else if (m_editor_a.pressed) {
      // Send move left event
      right -= speed;
    } else if (m_editor_s.pressed) {
      // Send move back event
      forward -= speed;
    } else if (m_editor_d.pressed) {
      // Send move right event
      right += speed;
    } else if (m_editor_shift.pressed) {
      // Send move lower event
    } else if (m_editor_space.pressed) {
      // Send move higher event
    }

    if (forward != 0 || right != 0) {
      MoveEvent ev;
      ev.forward = forward;
      ev.right = right;
      m_em.send_event(ev);
    }
  } else {
    // Camera off
    if (m_editor_ctrl.pressed) {
      if (m_editor_shift.pressed) {
        if (m_editor_rmb.clicked) {
          // Send attach event
        } else if (m_editor_lmb.clicked) {
          // Send detach event
        }
      } else {
        if (m_editor_lmb.clicked) {
          // Send add selection event
        } else if (m_editor_e.clicked) {
          ScriptEvent e = scr::detail::State::get().create_table_with();
          e["type"] = "create_entity";
          m_em.send_event(e);
        } else if (m_editor_c.clicked) {
          ScriptEvent e = scr::detail::State::get().create_table_with();
          e["type"] = "copy";
          m_em.send_event(e);
        } else if (m_editor_v.clicked) {
          ScriptEvent e = scr::detail::State::get().create_table_with();
          e["type"] = "paste";
          m_em.send_event(e);
        } else if (m_editor_i.clicked) {
          ScriptEvent e = scr::detail::State::get().create_table_with();
          e["type"] = "create_preset_instance";
          m_em.send_event(e);
        } else if (m_editor_p.clicked) {
          ScriptEvent e = scr::detail::State::get().create_table_with();
          e["type"] = "create_preset";
          m_em.send_event(e);
        } else if (m_editor_y.clicked) {
          ScriptEvent e = scr::detail::State::get().create_table_with();
          e["type"] = "redo";
          m_em.send_event(e);
        } else if (m_editor_z.clicked) {
          ScriptEvent e = scr::detail::State::get().create_table_with();
          e["type"] = "undo";
          m_em.send_event(e);
        } else if (m_editor_f.clicked) {
          ScriptEvent e = scr::detail::State::get().create_table_with();
          e["type"] = "connect_mesh_build_nodes";
          m_em.send_event(e);
        } else if (m_editor_g.clicked) {
          ScriptEvent e = scr::detail::State::get().create_table_with();
          e["type"] = "disconnect_mesh_build_nodes";
          m_em.send_event(e);
        }
      }
    } else {
      if (m_editor_shift.clicked) {
        // Send multiselect event
        m_em.send_event(EditorStartMultiselect{});
      } else if (m_editor_shift.released) {
        // Send multiselect event
        m_em.send_event(EditorStopMultiselect{});
      }

      if (m_editor_shift.pressed) {
        if (m_editor_x.clicked) {
          // Send transform x event (toggles said axis)
          m_em.send_event(EditorTransformAddAxis{Axis::X});
        } else if (m_editor_y.clicked) {
          // Send transform y event (toggles said axis)
          m_em.send_event(EditorTransformAddAxis{Axis::Y});
        } else if (m_editor_z.clicked) {
          // Send transform z event (toggles said axis)
          m_em.send_event(EditorTransformAddAxis{Axis::Z});
        }
      } else {
        if (m_editor_w.clicked) {
          // Send translate event
          m_em.send_event(EditorTransform{EditorTransform::TRANSLATE});
        } else if (m_editor_e.clicked) {
          // Send rotate event
          m_em.send_event(EditorTransform{EditorTransform::ROTATE});
        } else if (m_editor_r.clicked) {
          // Send scale event
          m_em.send_event(EditorTransform{EditorTransform::SCALE});
        } else if (m_editor_x.clicked) {
          // Send transform x event
          m_em.send_event(EditorTransformSetAxis{Axis::X});
        } else if (m_editor_y.clicked) {
          // Send transform y event
          m_em.send_event(EditorTransformSetAxis{Axis::Y});
        } else if (m_editor_z.clicked) {
          // Send transform z event
          m_em.send_event(EditorTransformSetAxis{Axis::Z});
        } else if (m_editor_delete.clicked) {
          // Send delete event
          ScriptEvent e = scr::detail::State::get().create_table_with();
          e["type"] = "delete";
          m_em.send_event(e);
        } else if (m_editor_lmb.clicked) {
          // Send select event

          // Send confirm transform event
          m_em.send_event(EditorConfirmTransform{});
        } else if (m_editor_rmb.clicked) {
          // Send cancel transform event
          m_em.send_event(EditorCancelTransform{});
        } else if (m_editor_g.clicked) {
          // Send toggle global transform event
          m_em.send_event(EditorToggleGlobalTransform{});
        }
      }
    }
    // attach event
    if (m_editor_ctrl.clicked) {
      // Send attach event
      m_em.send_event(EditorStartAttach{});
    } else if (m_editor_ctrl.released) {
      // Send attach event
      m_em.send_event(EditorStopAttach{});
    }
  }

  if (m_mouse_delta_x != 0 || m_mouse_delta_y != 0) {
    if (input_debug) {
      send_mouse_movement_event();
    } else {
      send_editor_mouse_movement_event();
    }
  }
}

// PRIVATE

// Key bindings
void InputManager::bind_keys() {
  m_key_move_forward.bind = GLFW_KEY_W;
  m_key_move_left.bind = GLFW_KEY_A;
  m_key_move_back.bind = GLFW_KEY_S;
  m_key_move_right.bind = GLFW_KEY_D;
  m_key_jump.bind = GLFW_KEY_SPACE;
  m_key_run.bind = GLFW_KEY_LEFT_SHIFT;
  m_key_dash.bind = GLFW_KEY_V;
  m_key_cancel.bind = GLFW_KEY_F;
  m_key_draw.bind = GLFW_MOUSE_BUTTON_LEFT;
  m_key_aim.bind = GLFW_MOUSE_BUTTON_RIGHT;
  m_key_piercing_arrow.bind = GLFW_KEY_Q;
  m_key_explosive_arrow.bind = GLFW_KEY_R;
  m_key_rope_arrow.bind = GLFW_KEY_E;
  m_key_arrow_up.bind = GLFW_KEY_UP;
  m_key_arrow_left.bind = GLFW_KEY_LEFT;
  m_key_arrow_down.bind = GLFW_KEY_DOWN;
  m_key_arrow_right.bind = GLFW_KEY_RIGHT;
  m_key_number_one.bind = GLFW_KEY_1;
  m_key_number_two.bind = GLFW_KEY_2;
  m_key_number_three.bind = GLFW_KEY_3;
  m_key_number_four.bind = GLFW_KEY_4;
  m_key_number_five.bind = GLFW_KEY_5;
  m_key_number_six.bind = GLFW_KEY_6;
  m_key_number_seven.bind = GLFW_KEY_7;
  m_key_number_eight.bind = GLFW_KEY_8;
  m_key_number_nine.bind = GLFW_KEY_9;
  m_key_number_zero.bind = GLFW_KEY_0;
  m_key_escape.bind = GLFW_KEY_ESCAPE;
  m_key_toggle_mouse_input.bind = GLFW_KEY_F1;
  m_key_toggle_bse_edit.bind = GLFW_KEY_F2;
  m_key_change_camera.bind = GLFW_KEY_F3;
  m_key_toggle_debug_camera_movement.bind = GLFW_KEY_F4;
  m_key_refresh_assets.bind = GLFW_KEY_F5;
  m_key_plus.bind = GLFW_KEY_KP_ADD;
  m_key_minus.bind = GLFW_KEY_KP_SUBTRACT;
  m_key_pause.bind = GLFW_KEY_P;
  m_key_editor.bind = GLFW_KEY_F6;
  m_key_quick_save.bind = GLFW_KEY_F8;
  m_key_quick_load.bind = GLFW_KEY_F9;
  m_key_start_recording.bind = GLFW_KEY_I;
  m_key_stop_recording.bind = GLFW_KEY_O;
  m_key_start_playback.bind = GLFW_KEY_K;
}

void InputManager::bind_editor_keys() {
  m_editor_q.bind = GLFW_KEY_Q;
  m_editor_play.bind = GLFW_KEY_F6;
  m_editor_w.bind = GLFW_KEY_W;
  m_editor_a.bind = GLFW_KEY_A;
  m_editor_s.bind = GLFW_KEY_S;
  m_editor_d.bind = GLFW_KEY_D;
  m_editor_shift.bind = GLFW_KEY_LEFT_SHIFT;
  m_editor_space.bind = GLFW_KEY_SPACE;
  m_editor_e.bind = GLFW_KEY_E;
  m_editor_c.bind = GLFW_KEY_C;
  m_editor_v.bind = GLFW_KEY_V;
  m_editor_p.bind = GLFW_KEY_P;
  m_editor_ctrl.bind = GLFW_KEY_LEFT_CONTROL;
  m_editor_lmb.bind = GLFW_MOUSE_BUTTON_LEFT;
  m_editor_rmb.bind = GLFW_MOUSE_BUTTON_RIGHT;
  m_editor_r.bind = GLFW_KEY_R;
  m_editor_x.bind = GLFW_KEY_X;
  m_editor_y.bind = GLFW_KEY_Y;
  m_editor_z.bind = GLFW_KEY_Z;
  m_editor_g.bind = GLFW_KEY_G;
  m_editor_f.bind = GLFW_KEY_F;
  m_editor_i.bind = GLFW_KEY_I;
  m_editor_delete.bind = GLFW_KEY_DELETE;
  m_editor_left_click.bind = GLFW_MOUSE_BUTTON_1;
  m_editor_quit.bind = GLFW_KEY_ESCAPE;
  m_key_toggle_bse_edit.bind = GLFW_KEY_F2;
  m_key_refresh_assets.bind = GLFW_KEY_F5;
}

void InputManager::poll_key(Key* k) {
  k->mode = glfwGetKey(m_w.get_window(), k->bind);
  k->released = false;

  if (k->mode == GLFW_PRESS && k->pressed == false) {
    k->pressed = true;
    k->clicked = true;
  } else if (k->mode == GLFW_PRESS && k->pressed == true) {
    k->clicked = false;
  } else if (k->mode == GLFW_RELEASE && k->pressed == true) {
    k->pressed = false;
    k->clicked = false;
    k->released = true;
  }
}

void InputManager::poll_press(Key* k) {
  k->mode = glfwGetKey(m_w.get_window(), k->bind);
  k->released = false;

  if (k->mode == GLFW_PRESS && k->pressed == false) {
    k->pressed = true;
  } else if (k->mode == GLFW_RELEASE && k->pressed == true) {
    k->pressed = false;
    k->released = true;
  }
}

template <typename Event1T, typename Event2T>
void InputManager::poll_mouse_button(Key* k, const Event1T& startEvent, const Event2T& stopEvent) {
  k->mode = glfwGetMouseButton(m_w.get_window(), k->bind);
  k->released = false;

  if (k->mode == GLFW_PRESS && k->pressed == false) {
    k->pressed = true;
    k->clicked = true;
    m_em.send_event(startEvent);
  } else if (k->mode == GLFW_PRESS && k->pressed == true) {
    k->clicked = false;
  } else if (k->mode == GLFW_RELEASE && k->pressed == true) {
    k->pressed = false;
    k->clicked = false;
    k->released = true;
    m_em.send_event(stopEvent);
  }
}

void InputManager::poll_mouse_movement() {
  // Get cursor position and reset it to middle of screen
  glfwGetCursorPos(m_w.get_window(), &m_mouse_x_pos, &m_mouse_y_pos);
  if (input_debug) glfwSetCursorPos(m_w.get_window(), m_mouse_origin_x, m_mouse_origin_y);

  m_mouse_delta_x = (m_mouse_x_pos - m_mouse_origin_x) * m_mouse_sensitivity;
  m_mouse_delta_y = (m_mouse_y_pos - m_mouse_origin_y) * m_mouse_sensitivity;
}

void InputManager::send_mouse_movement_event() {
  MouseMovementEvent mme;

  mme.dx = m_mouse_delta_x;
  mme.dy = m_mouse_delta_y;
  mme.posx = m_mouse_x_pos;
  mme.posy = m_mouse_y_pos;

  m_em.send_event(mme);
};

void InputManager::send_editor_mouse_movement_event() {
  EditorMouseMovementEvent mme;

  mme.dx = m_mouse_delta_x;
  mme.dy = m_mouse_delta_y;
  mme.posx = m_mouse_x_pos;
  mme.posy = m_mouse_y_pos;

  m_em.send_event(mme);
};

void InputManager::send_move_event() {
  MoveEvent me;

  me.forward = m_forward;
  me.right = m_right;
  m_em.send_event(me);
}

void InputManager::toggle_input_debug() {
  if (input_debug) {
    glfwSetInputMode(m_w.get_window(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    input_debug = false;
  } else {
    glfwSetInputMode(m_w.get_window(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
    input_debug = true;
  }
}

float den(float x) {
  if (x < 0) {
    return -1.0;
  } else {
    return 1.0;
  }
}

void InputManager::gamepad() {
  if (glfwJoystickPresent(GLFW_JOYSTICK_1)) {
    int count;
    const float* axes = glfwGetJoystickAxes(GLFW_JOYSTICK_1, &count);
    // Move
    float forward = axes[1];
    float right = axes[0];
    float move = std::sqrt(std::pow(forward, 2) + std::pow(right, 2));
    if (move > .1) {
      MoveEvent me;
      me.forward = forward / move;
      me.right = right / move;
      m_em.send_event(me);
    }

    const float dead_zone = .3;
    float aim_up = axes[3];
    float aim_right = axes[2];
    float sum = std::sqrt(std::pow(aim_up, 2) + std::pow(aim_right, 2));
    if (sum > .01) {
      MouseMovementEvent mme;
      mme.dx = 0;
      mme.dy = 0;
      float audz = std::abs(aim_up) * dead_zone;
      if (std::abs(aim_right) > audz) {
        mme.dx = den(aim_right) * std::pow((std::abs(aim_right) - audz) / (1 - dead_zone), 1.5) *
                 m_pad_sensitivity;
      }
      float ardz = std::abs(aim_right) * dead_zone;
      if (std::abs(aim_up) > ardz) {
        mme.dy = m_pad_inverted * den(aim_up) *
                 std::pow((std::abs(aim_up) - ardz) / (1 - dead_zone), 1.5) * m_pad_sensitivity;
      }
      m_em.send_event(mme);
    }

    float aim = axes[4];
    if (aim > 0 && m_pad_aim == false) {
      m_em.send_event(StartAimEvent());
      m_pad_aim = true;
    } else if (aim < 0 && m_pad_aim == true) {
      m_em.send_event(StopAimEvent());
      m_pad_aim = false;
    }

    float draw = axes[5];
    if (draw > 0 && m_pad_drawing == false) {
      m_em.send_event(StartDrawEvent());
      m_pad_drawing = true;
    } else if (draw < 0 && m_pad_drawing == true) {
      m_em.send_event(StopDrawEvent());
      m_pad_drawing = false;
    }

    const unsigned char* buttons = glfwGetJoystickButtons(GLFW_JOYSTICK_1, &count);

    // Jump - A
    if (buttons[0] == GLFW_PRESS) {
      if (m_pad_buttons[0] == false) {
        m_em.send_event(JumpEvent());
      }
      m_pad_buttons[0] = true;
    } else {
      m_pad_buttons[0] = false;
    }
    // Cancel - B
    if (buttons[1] == GLFW_PRESS) {
      if (m_pad_buttons[1] == false) {
        m_em.send_event(CancelActionEvent());
      }
      m_pad_buttons[1] = true;
    } else {
      m_pad_buttons[1] = false;
    }
    // Rope arrow - X
    if (buttons[2] == GLFW_PRESS) {
      if (m_pad_buttons[2] == false) {
        m_em.send_event(RopeEvent());
      }
      m_pad_buttons[2] = true;
    } else {
      m_pad_buttons[2] = false;
    }
    // Dash - Y
    if (buttons[3] == GLFW_PRESS) {
      if (m_pad_buttons[3] == false) {
        m_em.send_event(DashEvent());
      }
      m_pad_buttons[3] = true;
    } else {
      m_pad_buttons[3] = false;
    }
    // Piercing arrow - LB
    if (buttons[4] == GLFW_PRESS) {
      if (m_pad_buttons[4] == false) {
        m_em.send_event(NormalArrowModeEvent());
      }
      m_pad_buttons[4] = true;
    } else {
      m_pad_buttons[4] = false;
    }
    // Explosive arrow - RB
    if (buttons[5] == GLFW_PRESS) {
      if (m_pad_buttons[5] == false) {
        m_em.send_event(ExplosiveArrowModeEvent());
      }
      m_pad_buttons[5] = true;
    } else {
      m_pad_buttons[5] = false;
    }
    // Pause
    if (buttons[7] == GLFW_PRESS) {
      if (m_pad_buttons[7] == false) {
        m_em.send_event(PauseGameEvent());
      }
      m_pad_buttons[7] = true;
    } else {
      m_pad_buttons[7] = false;
    }
    // Slow motion - LS
    if (buttons[8] == GLFW_PRESS) {
      m_em.send_event(SlowMotionEvent());
    }
  }
}

}  // namespace inp