#include "cor/component_name.hpp"

#include "cor/component_id.hpp"

namespace cor {

std::array<std::string_view, UNBOUND_MAX_COMPONENTS>& ComponentName::get_names() {
  static std::array<std::string_view, UNBOUND_MAX_COMPONENTS> names;
  return names;
}

std::string_view ComponentName::get_name(const ComponentIDInteger id) {
  return get_names().at(id);  //
}

std::vector<std::string_view> ComponentName::get_registered_names() {
  std::vector<std::string_view> names;

  for (std::string_view name : get_names()) {
    if (name != "") {
      names.push_back(name);
    }
  }
  return names;
}

ComponentIDInteger ComponentName::get_id(std::string_view name) {
  //
  auto found = std::find(std::begin(get_names()), std::end(get_names()), name);
  if (found != std::end(get_names())) {
    return static_cast<ComponentIDInteger>(found - std::begin(get_names()));
  }
  return -1;
}
}  // namespace cor