// #include "bse/file_path.hpp"
// #include "cor/entity_manager.tcc"
// #include <bse/asset.hpp>
// #include <cor/cor.hpp>
// #include <cor/entity.tcc>
// #include <cor/script_type.hpp>
// #include <scr/object.hpp>
// #include <scr/script.hpp>

// namespace cor {

// void create_entity(Entity& entity, const scr::Object& object) {
//   for (std::pair<std::variant<std::string, size_t>, scr::Object> component : object) {
//     if (std::string* key = std::get_if<std::string>(&component.first)) {
//       ScriptType::load_component(*key, component.second, entity);
//     }
//   }
// }

// void load_scene(EntityManager& man, const scr::Object& object) {
//   for (std::pair<std::variant<std::string, size_t>, scr::Object> scene : object) {
//     if (std::string* key = std::get_if<std::string>(&scene.first)) {
//       if (*key == "math") {
//         continue;
//       }
//     }

//     for (std::pair<std::variant<std::string, size_t>, scr::Object> entity_object : scene.second)
//     {
//       if (!entity_object.second.has_children()) {
//         continue;
//       }

//       cor::Entity entity = man.get_entity(man.create_entity());
//       create_entity(entity, entity_object.second);
//     }
//   }
// }

// void load_scene(EntityManager& man, const std::string& path) {
//   static const std::string prefix = "assets/script/scene/";

//   bse::Asset<scr::Script> script(prefix + path + ".lua");
//   if (!script) {
//     return;
//   }

//   load_scene(man, script->return_value());
// }

// }  // namespace cor
