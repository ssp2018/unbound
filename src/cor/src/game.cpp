#include "cor/game.hpp"

#include "aud/sound_buffer.hpp"
#include "bse/loader.hpp"
#include "bse/log.hpp"
#include "cor/event.hpp"
#include "cor/frame_context.hpp"
#include "cor/input.hpp"
#include "cor/performance_monitor.hpp"
#include "cor/system_id_proxy.hpp"
#include "cor/system_manager.hpp"
#include "gfx/material.hpp"
#include "gfx/shader.hpp"
#include "gfx/window.hpp"
#include <bse/edit.hpp>
#include <bse/runtime_asset.hpp>
#include <bse/runtime_loader.hpp>
#include <gfx/context.hpp>
#include <gfx/font.hpp>
#include <gfx/helper.hpp>
#include <gfx/text_cache.hpp>
#include <scr/script.hpp>

namespace gfx {
class Model;
}
namespace gfx {
class Texture;
}
namespace phy {
class CollisionMesh;
}

namespace cor {
//

Game::Game(gfx::Window& window, unsigned int max_frame_count)
    : m_window{window}, m_max_frame_count{max_frame_count} {
  //
  using T = bse::TaskScheduler::Thread;
  bse::Loader<gfx::Model>::set_properties(true, T::RENDER, false, false, "#.#");
  bse::Loader<gfx::Mesh>::set_properties(true, T::RENDER, false, false, "#.#");
  bse::Loader<gfx::Material>::set_properties(true, T::RENDER, false, false, "#.#");
  bse::Loader<gfx::Shader>::set_properties(true, T::RENDER, false, false, "#.#");
  bse::Loader<gfx::Font>::set_properties(true, T::RENDER, false, false, "#.#");
  bse::Loader<gfx::Texture>::set_properties(true, T::RENDER, false, true,
                                            "assets/texture/fallback.png");
  bse::Loader<phy::CollisionMesh>::set_properties(true, T::MAIN, false, false, "#.#");
  bse::Loader<scr::Script>::set_properties(true, T::MAIN, false, false, "#.#");

  initialize();
}

Game::~Game() {
  gfx::Context::run([this]() {
    bse::Loader<gfx::Model>::destroy();     //
    bse::Loader<gfx::Mesh>::destroy();      //
    bse::Loader<gfx::Material>::destroy();  //
    bse::Loader<gfx::Shader>::destroy();    //
    bse::Loader<gfx::Font>::destroy();      //
    bse::Loader<gfx::Texture>::destroy();   //
  });
  bse::Loader<phy::CollisionMesh>::destroy();  //
  bse::Loader<scr::Script>::destroy();         //
}

void Game::initialize() {
  //
}

void Game::start() {
  std::chrono::time_point<std::chrono::steady_clock> last_frame_time =
      std::chrono::steady_clock::now();
  float ups = 60.f;
  float accumulated_dt = 0.f;
  bse::TaskScheduler::Diagnostics thread_diagnostics =
      bse::TaskScheduler::get()->compile_diagnostics();
  SystemManager::Diagnostics sys_diag;
  SystemManager::Diagnostics sys_diag_perc;
  sys_diag.read_time = 0.f;
  sys_diag.write_time = 0.f;
  sys_diag_perc.read_time = 0.f;
  sys_diag_perc.write_time = 0.f;
  bse::Timer title_update_timer;
  title_update_timer.start();
  while (m_window.should_run()) {
    std::chrono::time_point<std::chrono::steady_clock> now = std::chrono::steady_clock::now();
    float dt =
        static_cast<float>(
            std::chrono::duration_cast<std::chrono::milliseconds>(now - last_frame_time).count()) /
        1000.f;
    last_frame_time = now;

    if (dt > 0.00001f) {
      ups = ups * 0.95f + (1.f / dt) * 0.05f;
    }

    accumulated_dt += dt;

    if (title_update_timer.calculate_current_difference() > 3.f) {
      float vram = gfx::get_vram_usage();
      float all_memory = ram_usage();
      float ram = all_memory - vram;
      m_window.set_title("Unbound | FPS (%swp): " + std::to_string((int)m_window.get_fps()) + " (" +
                         std::to_string((int)(m_window.get_swap_time_percent() * 100.f)) + "%)  " +
                         "UPS: " + std::to_string((int)ups) + "  Memory usage: " +
                         std::to_string(ram) + "  VRAM usage: " + std::to_string(vram) + "MiB");

      title_update_timer.reset();
    }
    bse::TaskScheduler::Diagnostics new_thread_diagnostics =
        bse::TaskScheduler::get()->compile_diagnostics();
    for (size_t i = 0; i < thread_diagnostics.thread_util.size(); i++) {
      float& util = thread_diagnostics.thread_util[i];
      util = util * 0.95f + new_thread_diagnostics.thread_util[i] * 0.05f;
    }
    thread_diagnostics.total_util =
        thread_diagnostics.total_util * 0.95f + new_thread_diagnostics.total_util * 0.05f;

    if (!m_state_stack.empty()) {
      cor::SystemManager::Diagnostics diag =
          m_state_stack.back()->get_system_manager().compile_diagnostics();
      sys_diag.read_time = sys_diag.read_time * 0.95f + diag.read_time * 0.05f;
      sys_diag.write_time = sys_diag.write_time * 0.95f + diag.write_time * 0.05f;

      for (const auto& [name, time] : diag.system_read_times) {
        if (!sys_diag.system_read_times.count(name)) {
          sys_diag.system_read_times[name] = 0.f;
        }
        sys_diag.system_read_times[name] = sys_diag.system_read_times[name] * 0.95f + time * 0.05f;
      }
      for (const auto& [name, time] : diag.system_write_times) {
        if (!sys_diag.system_write_times.count(name)) {
          sys_diag.system_write_times[name] = 0.f;
        }
        sys_diag.system_write_times[name] =
            sys_diag.system_write_times[name] * 0.95f + time * 0.05f;
      }

      if (dt > 0.00001f) {
        sys_diag_perc.read_time = sys_diag_perc.read_time * 0.95f + (diag.read_time / dt) * 0.05f;
        sys_diag_perc.write_time =
            sys_diag_perc.write_time * 0.95f + (diag.write_time / dt) * 0.05f;

        for (const auto& [name, time] : diag.system_read_times) {
          sys_diag_perc.system_read_times[name] =
              sys_diag_perc.system_read_times[name] * 0.95f + (time / dt) * 0.05f;
        }
        for (const auto& [name, time] : diag.system_write_times) {
          sys_diag_perc.system_write_times[name] =
              sys_diag_perc.system_write_times[name] * 0.95f + (time / dt) * 0.05f;
        }
      }
    }

    if (accumulated_dt > 15.f) {
      accumulated_dt = 0.f;
      // Percentages are relative to dt
      // The system diagnostics won't work very well if there are multiple
      // states or if systems get toggled.

#ifdef UNBOUND_PROFILE
      std::stringstream sstream;
      sstream << "== SYSTEM DIAGNOSTICS ==" << std::endl;
      sstream << "Read: " << sys_diag.read_time << " (" << (int)(100.f * sys_diag_perc.read_time)
              << "%)" << std::endl;
      for (const auto& [name, time] : sys_diag.system_read_times) {
        sstream << "\t" << name << ": " << time << " ("
                << 0.01f * std::floor(10000.f * sys_diag_perc.system_read_times[name]) << "%)"
                << std::endl;
      }
      sstream << "Write: " << sys_diag.write_time << " (" << (int)(100.f * sys_diag_perc.write_time)
              << "%)" << std::endl;
      for (const auto& [name, time] : sys_diag.system_write_times) {
        sstream << "\t" << name << ": " << time << " ("
                << 0.01f * std::floor(10000.f * sys_diag_perc.system_write_times[name]) << "%)"
                << std::endl;
      }
      sstream << "== END SYSTEM DIAGNOSTICS ==" << std::endl;

      float render_util = thread_diagnostics.thread_util[(int)bse::TaskScheduler::Thread::RENDER];
      sstream << "Render thread utilization : " << (int)(render_util * 100.f) << " %\t\t "
              << m_window.get_fps() << " FPS\t\t" << (int)(m_window.get_swap_time_percent() * 100.f)
              << "% dtswp" << std::endl;
      std::cout << sstream.str() << std::endl;
#endif
    }

    // Limit dt to avoid physical glitches when pausing/debugging
    dt = std::min(dt, 1.f / 20.f);

    update_states(dt);
    display_states();
    m_window.swap();

    handle_state_requests();

    if (++m_frame_num > m_max_frame_count && m_max_frame_count != 0) {
      m_window.close();
    }
  }
}

void Game::push_state(std::unique_ptr<cor::State> state) {
  //
  m_state_requests.push_back(StateRequest{StateRequest::Type::PUSH, std::move(state)});
}

void Game::pop_state() {
  //
  m_state_requests.push_back(StateRequest{StateRequest::Type::POP, nullptr});
}

void Game::replace_state(std::unique_ptr<cor::State> state) {
  //
  m_state_requests.push_back(StateRequest{StateRequest::Type::REPLACE, std::move(state)});
}

void Game::update_states(float dt) {
  //
  auto top_it = m_state_stack.rbegin();
  while (top_it != m_state_stack.rend()) {
    //
    if (!(*top_it)->update(dt)) {
      // If the layer is not "logically" transparent, break
      break;
    }

    ++top_it;
  }
}

void Game::display_states() {
  auto top_it = m_state_stack.rbegin();
  while (top_it != m_state_stack.rend()) {
    //
    if (!(*top_it)->display()) {
      // If the layer is not graphically transparent, break
      break;
    }

    ++top_it;
  }
}

void Game::handle_state_requests() {
  for (auto& state_request : m_state_requests) {
    //
    switch (state_request.type) {
      case StateRequest::Type::PUSH: {
        m_state_stack.emplace_back(std::move(state_request.state));
        break;
      }
      case StateRequest::Type::POP: {
        m_state_stack.pop_back();
        break;
      }
      case StateRequest::Type::REPLACE: {
        m_state_stack.pop_back();
        m_state_stack.emplace_back(std::move(state_request.state));
        break;
      }
    }
  }

  m_state_requests.clear();
}

const cor::State* Game::peek_below(const cor::State* state) const {
  auto it = m_state_stack.begin();
  while (it != m_state_stack.end() && it->get() != state) {
    it++;
  }
  if (it == m_state_stack.end()) {
    return nullptr;
  }

  if (it == m_state_stack.begin()) {
    return nullptr;
  }

  it--;
  return it->get();
}

const cor::State* Game::peek_above(const cor::State* state) const {
  auto it = m_state_stack.begin();
  while (it != m_state_stack.end() && it->get() != state) {
    it++;
  }
  if (it == m_state_stack.end()) {
    return nullptr;
  }

  it++;
  if (it == m_state_stack.end()) {
    return nullptr;
  }
  return it->get();
}

}  // namespace cor
