#include "scr/object.hpp"
#include <cor/script_type.hpp>
namespace cor {
class Entity;
}

namespace cor {
std::map<std::string_view, ScriptType::ComponentLoader>* ScriptType::m_component_loaders = nullptr;

bool ScriptType::load_component(const std::string& component_name, const scr::Object& component,
                                Entity& entity) {
  ASSERT(false);
  return false;
  // auto it = m_component_loaders->find(std::string_view(component_name));
  // if (it == m_component_loaders->end()) {
  //  LOG(WARNING) << "Failed to load unregistered component '" << component_name << "'.\n";
  //  return false;
  //}

  // if (!it->second(entity, component)) {
  //  LOG(WARNING) << "Failed to load component '" << component_name
  //               << "'. See previous warnings for details.\n";
  //  return false;
  //}
  // return true;
}

void ScriptType::destroy() {
  if (m_component_loaders != nullptr) {
    delete m_component_loaders;
  }
}

}  // namespace cor