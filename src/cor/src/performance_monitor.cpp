#include "cor/performance_monitor.hpp"

// #include <d3d11_4.h>
// #include <dxgi1_6.h>

// #pragma comment(lib, "dxgi.lib")

#include <bse/log.hpp>
#include <string>

#if defined(WIN32)
#include <psapi.h>

#endif
namespace cor {

float ram_usage() {
#if defined(WIN32)
  float res = -1;
  // src:
  //
  // https://docs.microsoft.com/en-us/windows/desktop/api/psapi/ns-psapi-_process_memory_counters

  DWORD currentProcessID = GetCurrentProcessId();

  HANDLE hProcess =
      OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, currentProcessID);

  if (NULL == hProcess) return -1;

  PROCESS_MEMORY_COUNTERS pmc{};
  if (GetProcessMemoryInfo(hProcess, &pmc, sizeof(pmc))) {
    // PagefileUsage is the:
    // The Commit Charge value in bytes for this process.
    // Commit Charge is the total amount of memory that the memory manager has committed for a
    // running process.

    res = float(pmc.PagefileUsage / 1024.0 / 1024.0);  // MiB
  }

  CloseHandle(hProcess);
  return res;
#else
  return 0.f;
#endif
}

}  // namespace cor