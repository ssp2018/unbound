#include "cor/system.hpp"

#include <cor/event_manager.hpp>
#include <cor/frame_context.hpp>

namespace cor {

void System::enable_listeners(FrameContext& context) {
  //
  for (auto& lid : m_lids) {
    context.event_mgr.enable_handler(lid);
  }
}

void System::disable_listeners(FrameContext& context) {
  //
  for (auto& lid : m_lids) {
    context.event_mgr.disable_handler(lid);
  }
}

void System::toggle_listeners(FrameContext& context) {
  //
  for (auto& lid : m_lids) {
    context.event_mgr.toggle_handler(lid);
  }
}

void System::add_listener(cor::EventManager::ListenID lid) {
  //
  m_lids.emplace_back(std::move(lid));
}

void System::write_update_late(FrameContext& context) {}

}  // namespace cor
