#include "cor/component_register.hpp"

namespace cor {

int ComponentRegister::m_current_id = 0;
// std::vector<std::unique_ptr<AbstractComponentStore>> ComponentRegister::m_component_stores;
// std::vector<int> ComponentRegister::m_component_stores;

std::vector<std::unique_ptr<AbstractComponentStore>>& ComponentRegister::get_component_stores() {
  static std::vector<std::unique_ptr<AbstractComponentStore>> component_stores;
  return component_stores;
}

std::vector<std::unique_ptr<AbstractComponentStore>>
ComponentRegister::copy_component_store_vector() {
  std::remove_reference_t<decltype(get_component_stores())> copy;

  for (auto& store : get_component_stores()) {
    copy.emplace_back(std::move(store->create_new()));
  }

  return copy;  //
}

}  // namespace cor
