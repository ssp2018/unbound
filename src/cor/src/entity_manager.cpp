#include "cor/entity_manager.hpp"

#include "cor/component_register.hpp"
#include "cor/entity.tcc"
#include "cor/entity_handle.hpp"
#include <scr/dynamic_table.hpp>
#include <scr/scr.tcc>

namespace cor {
//
EntityManager::EntityManager() {
  m_component_stores = std::move(cor::ComponentRegister::copy_component_store_vector());
  // cor::ComponentRegister::get_component_store_vector();
}

EntityHandle EntityManager::create_entity() {
  //

  // If there are no allocated free handles, allocate a batch and initialize them
  if (m_free_handles.empty()) {
    constexpr size_t RESIZE_STEP = 50;

    m_free_handles.resize(RESIZE_STEP);
    m_registers.reserve(m_registers.size() + RESIZE_STEP);

    for (int i = 0; i < RESIZE_STEP; i++) {
      EntityHandle new_handle;
      new_handle.iteration = 0;
      new_handle.register_index = m_registers.size();
      m_free_handles[RESIZE_STEP - i - 1] = new_handle;

      Register new_register;
      new_register.entity_handle = new_handle;
      new_register.component_index_map.fill(uint16_t(-1));
      m_registers.push_back(new_register);
    }
  }

  // Take a free handle
  auto handle = m_free_handles.back();
  m_free_handles.pop_back();

  return handle;
}

void* EntityManager::raw_get(EntityHandle handle, std::string_view component_name) {
  ASSERT(is_valid(handle));
  //

  auto component_id = ComponentName::get_id(component_name);
  Register& reg = get_register(handle);
  if (!reg.key.has(component_id)) {
    return nullptr;
  }

  auto index = reg.component_index_map[component_id];
  return m_component_stores[component_id]->get(index);
}

void* EntityManager::raw_attach(EntityHandle handle, std::string_view component_name) {
  ASSERT(is_valid(handle));
  auto component_id = ComponentName::get_id(component_name);

  auto added_component_index = m_component_stores[component_id]->add();

  Register& reg = get_register(handle);
  if (reg.key.has(component_id)) {
    return raw_get(handle, component_name);
  }
  reg.key.include(component_id);
  reg.component_index_map[component_id] = added_component_index;

  return m_component_stores[component_id]->get(added_component_index);
}

void EntityManager::raw_detach(EntityHandle handle, std::string_view component_name) {
  ASSERT(is_valid(handle));
  //
  auto component_id = ComponentName::get_id(component_name);

  Register& reg = get_register(handle);
  if (!reg.key.has(component_id)) {
    return;
  }
  reg.key.exclude(component_id);
  auto removed_component_index = reg.component_index_map[component_id];
  reg.component_index_map[component_id] = uint16_t(-1);
  // reg.component_index_map.erase(component_id);
  m_component_stores[component_id]->remove(removed_component_index);
}

void EntityManager::raw_set(EntityHandle handle, std::string_view component_name, void* data) {
  ASSERT(is_valid(handle));
  auto component_id = ComponentName::get_id(component_name);
  Register& reg = get_register(handle);
  if (!reg.key.has(component_id)) {
    return;
  }

  auto index = reg.component_index_map[component_id];
  m_component_stores[component_id]->set(index, data);
}

bool EntityManager::is_valid(EntityHandle handle) const {
  bool result = ((handle != EntityHandle::NULL_HANDLE) && handle.register_index >= 0 &&
                 handle.register_index < m_registers.size());
  if (result) {
    const Register& reg = m_registers[handle.register_index];
    result = result && (reg.entity_handle.iteration == handle.iteration);
  }
  return result;
}

void EntityManager::destroy_entity(EntityHandle handle) {
  //
  // Add this check if arrows start dissappearing again!!
  // ASSERT(handle.register_index < 1239 || handle.register_index > 1242)
  //     << "Arrow was incorrectly destroyed here!";

  ASSERT(is_valid(handle));
  if (is_valid(handle)) {
    Register& reg = get_register(handle);

    // Iterate the register's handle to assure that outdated handles cannot be used
    reg.entity_handle.iteration++;

    // Remove all previous entity state from the register
    clear_register(reg);

    handle.iteration++;
    m_free_handles.push_back(handle);
  }
}

bool EntityManager::has(EntityHandle handle, Key key) const {
  // ASSERT(is_valid(handle));
  //
  const Register& reg = get_register(handle);

  return register_has(reg, key);
}

Entity EntityManager::get_entity(EntityHandle handle) {
  //
  // ASSERT(is_valid(handle)) << "The handle was invalid! The entity was probably already
  // destroyed!";
  return Entity(this, handle);
}

const ConstEntity EntityManager::get_entity(EntityHandle handle) const {
  //
  // ASSERT(is_valid(handle)) << "The handle was invalid! The entity was probably already
  // destroyed!";
  return ConstEntity(this, handle);
}

std::vector<EntityHandle> EntityManager::get_entities(Key key) {
  std::vector<EntityHandle> entities;
  entities.reserve(128);
  for (auto& reg : m_registers) {
    if (register_has(reg, key)) {
      entities.push_back(reg.entity_handle);
    }
  }
  return entities;
}

std::vector<EntityHandle> EntityManager::get_entities() {
  std::vector<EntityHandle> entities;
  entities.reserve(1024);
  for (auto& reg : m_registers) {
    entities.push_back(reg.entity_handle);
  }
  return entities;
}

const std::vector<EntityHandle> EntityManager::get_entities(Key key) const {
  std::vector<EntityHandle> entities;
  entities.reserve(128);
  for (auto& reg : m_registers) {
    if (register_has(reg, key)) {
      entities.push_back(reg.entity_handle);
    }
  }
  return entities;
}

void EntityManager::clear_register(Register& reg) {
  //
  /*for (auto& entry : reg.component_index_map) {
    m_component_stores[entry.first]->remove(entry.second);
  }*/
  for (ComponentIDInteger i = 0; i < m_component_stores.size(); i++) {
    if (reg.component_index_map[i] != uint16_t(-1)) {
      m_component_stores[i]->remove(reg.component_index_map[i]);
    }
  }
  reg.key = Key::create();
  reg.component_index_map.fill(uint16_t(-1));
  // reg.component_index_map.clear();
}

bool EntityManager::register_has(const Register& reg, Key key) const {
  return (reg.key.get_bits() & key.get_bits()) == key.get_bits();
}

Key EntityManager::get_key(EntityHandle handle) const {
  //
  return get_register(handle).key;
}

EntityManager::Register& EntityManager::get_register(EntityHandle handle) {
  ASSERT(is_valid(handle)) << "Handle was invalid! The entity was probably already destroyed!";
  return m_registers[handle.register_index];
}

const EntityManager::Register& EntityManager::get_register(EntityHandle handle) const {
  ASSERT(is_valid(handle)) << "Handle was invalid! The entity was probably already destroyed!";
  return m_registers[handle.register_index];
}

void EntityManager::clear() {
  std::vector<EntityHandle> entities = get_entities();
  for (EntityHandle entity : entities) {
    destroy_entity(entity);
  }
  m_registers.clear();
  m_free_handles.clear();
}

std::vector<Entity> get_entities(EntityManager& manager, std::vector<std::string> component_names) {
  Key key;
  for (const std::string& name : component_names) {
    key.include(ComponentName::get_id(name));
  }

  std::vector<EntityHandle> handles = manager.get_entities(key);
  std::vector<Entity> entities;
  entities.reserve(handles.size());
  for (EntityHandle handle : handles) {
    entities.push_back(manager.get_entity(handle));
  }
  return entities;
}

sol::object get_entities_from_handles(EntityManager& manager,
                                      std::vector<cor::EntityHandle> handles) {
  std::vector<Entity> entities;
  entities.reserve(handles.size());
  for (EntityHandle handle : handles) {
    if (!manager.is_valid(handle)) {
      return nullptr;
    }
    entities.push_back(manager.get_entity(handle));
  }
  return sol::make_object(scr::detail::State::get(), entities);
}

sol::object get_entity(EntityManager& manager, EntityHandle handle) {
  if (manager.is_valid(handle)) {
    return sol::make_object(scr::detail::State::get(), manager.get_entity(handle));
  } else {
    return nullptr;
  }
}

std::vector<Entity> get_all_entities(EntityManager& manager) {
  std::vector<EntityHandle> handles = manager.get_entities();
  std::vector<Entity> entities;
  entities.reserve(handles.size());
  for (EntityHandle handle : handles) {
    entities.push_back(manager.get_entity(handle));
  }
  return entities;
}

Entity create_entity(EntityManager& manager) { return manager.get_entity(manager.create_entity()); }

void destroy_entity(EntityManager& manager, Entity& entity) {
  manager.destroy_entity(entity.get_handle());
}

static bool initialize_script_exposition = []() {
  scr::expose_type<EntityManager>(
      "EntityManager", scr::Constructors<EntityManager()>(), "get_entities", get_entities,
      "get_entity", get_entity, "create_entity", create_entity, "destroy_entity", destroy_entity,
      "get_all_entities", get_all_entities, "get_entities_from_handles", get_entities_from_handles,
      "clear", &EntityManager::clear, "is_valid", &EntityManager::is_valid);

  return true;
}();

}  // namespace cor
