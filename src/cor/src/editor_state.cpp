#include "cor/editor_state.hpp"

namespace cor {
EditorState::EditorState(cor::Game& game, gfx::Window& window, aud::AudioManager& audio_mgr)
    : State(game, window, audio_mgr) {
  //
  initialize();
}

EditorState::~EditorState() {
  //
}

void EditorState::initialize() {
  //
}

bool EditorState::update(float dt) {
  //
  m_input_mgr.poll_editor_input();
  State::update(dt);

  return false;
}

bool EditorState::display() {
  //
  State::display();
  return false;
}

// void EditorState::select(cor::EntityHandle handle) {
//   //
//   // m_selected_entity = handle;
// }

// void EditorState::deselect() {
//   //
//   // m_selected_entity = cor::EntityHandle::NULL_HANDLE;
// }

// cor::EntityHandle EditorState::get_selected_entity() const {
//   //
//   // return m_selected_entity;
// }

}  // namespace cor
