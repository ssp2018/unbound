set(LAYER_BIN_NAME "${PROJECT_NAME}_cor")

add_library_pch(${LAYER_BIN_NAME}
  src/cor.cpp
  src/event_manager.cpp
  src/input.cpp
  src/key.cpp
  src/entity_manager.cpp
  src/entity.cpp
  src/entity_handle.cpp
  src/system_manager.cpp
  src/component_name.cpp
  src/script_type.cpp
  src/system.cpp
  src/game.cpp
  src/basic_systems.cpp
  src/system_id_proxy.cpp
  src/state.cpp
  src/component_register.cpp
  src/editor_state.cpp
  src/performance_monitor.cpp
)

target_link_libraries(${LAYER_BIN_NAME}
  PUBLIC
  	${PROJECT_NAME}_ai
	${PROJECT_NAME}_aud
	${PROJECT_NAME}_gfx
	${PROJECT_NAME}_phy
	${PROJECT_NAME}_scr
	${PROJECT_NAME}_phy
)

target_include_directories(${LAYER_BIN_NAME}
  PUBLIC
    include/
)


add_unbound_test(${LAYER_BIN_NAME}
  test/addition.cpp
  test/event_test.cpp
  test/script_type.cpp
  test/ecs_test.cpp
  test/script_ecs.cpp
)