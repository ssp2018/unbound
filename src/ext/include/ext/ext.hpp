#pragma once

#ifndef EXT_EXT_HPP
#define EXT_EXT_HPP

// clang-format off
#pragma warning(disable : 4701)
#pragma warning(disable : 4172)
#pragma warning(disable : 4723)
#pragma warning(push, 0)
#include <ext/detail/stdcpp.hpp>
#include <ext/detail/assimp.hpp>
#include <ext/detail/bullet.hpp>
#include <ext/detail/freetype.hpp>
#include <ext/detail/glbinding.hpp>
#include <ext/detail/glfw.hpp>
#include <ext/detail/glm.hpp>
#include <ext/detail/gtest.hpp>
#include <ext/detail/openal.hpp>
#include <ext/detail/sndfile.hpp>
#include <ext/detail/sol2.hpp>
#include <ext/detail/windows.hpp>
#include <ext/detail/unix.hpp>
#include <ext/detail/imgui.hpp>
#include "xmmintrin.h"	// AVX intrinsics
#pragma warning(pop)
#pragma warning(default : 4701)
#pragma warning(default : 4172)
#pragma warning(default : 4723)

// clang-format on

#endif