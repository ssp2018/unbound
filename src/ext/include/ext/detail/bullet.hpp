#pragma once

#ifndef EXT_BULLET_HPP
#define EXT_BULLET_HPP

#include <bullet/BulletCollision/BroadphaseCollision/btCollisionAlgorithm.h>
#include <bullet/BulletCollision/BroadphaseCollision/btOverlappingPairCache.h>
#include <bullet/BulletCollision/CollisionDispatch/btCollisionWorld.h>
#include <bullet/BulletCollision/CollisionDispatch/btGhostObject.h>
#include <bullet/BulletCollision/CollisionShapes/btMultiSphereShape.h>
#include <bullet/BulletCollision/CollisionShapes/btTriangleShape.h>
#include <bullet/BulletDynamics/Character/btCharacterControllerInterface.h>
#include <bullet/BulletDynamics/Character/btKinematicCharacterController.h>
#include <bullet/LinearMath/btDefaultMotionState.h>
#include <bullet/btBulletCollisionCommon.h>
#include <bullet/btBulletDynamicsCommon.h>

#endif
