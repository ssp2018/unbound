#pragma once

#ifndef EXT_FREETYPE_HPP
#define EXT_FREETYPE_HPP

// clang-format off
#include <ft2build.h>
#include FT_FREETYPE_H
// clang-format on

#endif
