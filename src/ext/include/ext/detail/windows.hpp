#pragma once

#ifndef EXT_WINDOWS_HPP
#define EXT_WINDOWS_HPP

// clang-format off
#if defined(WIN32)
#define NOMINMAX
#include <windows.h>
#undef far
#undef near
#endif
// clang-format on
#endif
