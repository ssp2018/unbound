#include <phy/collision_shapes.hpp>
#include <phy/physics_manager.hpp>

TEST(PhysicsManager, ConstructionDestruction) {
  phy::PhysicsManager pm(glm::vec3(1.0f, 0.0f, 10.0f), 3);
}

TEST(PhysicsManager, GetSet) {
  phy::PhysicsManager pm(glm::vec3(1.0f, 0.0f, 10.0f), 3);
  pm.set_max_steps(4);
  EXPECT_EQ(pm.get_max_steps(), 4);

  pm.set_gravity(glm::vec3(2.0f, 3.0f, 1.0f));
  EXPECT_EQ(pm.get_gravity(), glm::vec3(2.0f, 3.0f, 1.0f));
}

TEST(PhysicsManager, Simulate) {
  phy::PhysicsManager pm(glm::vec3(1.0f, 0.0f, 10.0f), 3);
  pm.add_static_object(0, glm::mat4(1.0f), glm::vec3(1.0f),
                       std::make_shared<phy::CapsuleCollisionShape>(1.0f, 6.0f), false);
  pm.add_dynamic_object(1, glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 5)),
                        std::make_shared<phy::BoxCollisionShape>(glm::vec3(1.0f, 1.0f, 1.0f)),
                        10.0f);

  btCollisionWorld::AllHitsRayResultCallback rr =
      pm.raycast(glm::vec3(-5, 0, 5), glm::vec3(1, 0, 0), 10.0f);

  EXPECT_EQ(rr.m_collisionObjects.size() > 0, true);
  EXPECT_FLOAT_EQ(phy::vc(rr.m_hitPointWorld[0]).x, glm::vec3(-1, 0, 5).x);
  EXPECT_FLOAT_EQ(phy::vc(rr.m_hitPointWorld[0]).y, glm::vec3(-1, 0, 5).y);
  EXPECT_FLOAT_EQ(phy::vc(rr.m_hitPointWorld[0]).z, glm::vec3(-1, 0, 5).z);

  pm.update_static_object(0);
  pm.update_dynamic_object(1);

  pm.set_dynamic_transform(1, glm::translate(glm::mat4(1.0f), glm::vec3(2, 6, 4)));
  pm.set_static_transform(0, glm::translate(glm::mat4(1.0f), glm::vec3(6, 66, 8)), glm::vec3(1.0f));

  pm.set_gravity(glm::vec3(1.0f, 5.0f, 3.0f));
  EXPECT_EQ(pm.get_gravity(), glm::vec3(1.0f, 5.0f, 3.0f));

  pm.simulate(1 / 60.0f);

  pm.activate_static_object(0);
  pm.activate_dynamic_object(1);
}