#include <phy/collision_shapes.hpp>

TEST(CollisionShapes, Box) {
  phy::BoxCollisionShape* box = new phy::BoxCollisionShape(glm::vec3(5.0f, 1.5f, 0.1f));

  // EXPECT_FLOAT_EQ(box->get_radius(), glm::length(glm::vec3(5.0f, 1.5f, 0.1f)));

  EXPECT_EQ(box->get_type(), phy::CollisionShapeType::BOX);

  delete box;
}

TEST(CollisionShapes, Sphere) {
  phy::SphereCollisionShape* sphere = new phy::SphereCollisionShape(5.0f);

  // EXPECT_FLOAT_EQ(sphere->get_radius(), 5.0f);

  EXPECT_EQ(sphere->get_type(), phy::CollisionShapeType::SPHERE);

  delete sphere;
}

TEST(CollisionShapes, Capsule) {
  phy::CapsuleCollisionShape* capsule = new phy::CapsuleCollisionShape(5.0f, 1.0f);

  // EXPECT_FLOAT_EQ(capsule->get_radius(), 5.0f);

  EXPECT_EQ(capsule->get_type(), phy::CollisionShapeType::CAPSULE);

  delete capsule;

  capsule = new phy::CapsuleCollisionShape(1.0f, 5.0f);

  // EXPECT_FLOAT_EQ(capsule->get_radius(), 5.0f);

  EXPECT_EQ(capsule->get_type(), phy::CollisionShapeType::CAPSULE);
}

TEST(CollisionShapes, Cylinder) {
  phy::CylinderCollisionShape* cylinder =
      new phy::CylinderCollisionShape(glm::vec3(1.0f, 2.0f, 3.0f));

  // EXPECT_FLOAT_EQ(cylinder->get_radius(), glm::length(glm::vec3(1.0f, 2.0f, 3.0f)));

  EXPECT_EQ(cylinder->get_type(), phy::CollisionShapeType::CYLINDER);

  delete cylinder;
}