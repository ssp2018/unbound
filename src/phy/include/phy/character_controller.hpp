#pragma once

#ifndef PHY_CHARACTER_CONTROLLER_HPP
#define PHY_CHARACTER_CONTROLLER_HPP

#include <ext/ext.hpp>
#include <phy/collision_groups.hpp>
#include <phy/collision_shapes.hpp>

namespace phy {
class PhysicsManager;
class CollisionShape;

// A capsule shape that allows movement for game characters in the physics world
class CharacterController {
 public:
  CharacterController() {}
  CharacterController(float radius,                    //
                      float height,                    //
                      glm::vec3 position,              //
                      glm::vec3 up_direction,          //
                      glm::vec3 gravity,               //
                      float terminal_velocity,         //
                      float turning_speed,             //
                      float angle,                     //
                      bool face_moving_direction,      //
                      int id,                          //
                      btDiscreteDynamicsWorld* world,  //
                      float mass,                      //
                      bool navmesh_collision,          //
                      bool is_flying,                  //
                      glm::quat desired_direction,     //
                      glm::quat facing_direction,      //
                      bool on_ground,                  //
                      glm::vec3 velocity);
  ~CharacterController();
  CharacterController(const CharacterController& other) = delete;
  CharacterController(CharacterController&& other) = delete;

  CharacterController& operator=(const CharacterController& other) = delete;
  CharacterController& operator=(CharacterController&& other) = delete;

  // Attempt to move to current position + move_vector
  void move(glm::vec3 move_vector, btDiscreteDynamicsWorld* world, float dt);

  // Sets vertical velocity
  void jump(float jump_strength);

  // Fixes velocity for a duration
  void dash(glm::vec3 dash_velocity, float duration);

  // Returns true if character is dashing
  bool dashing();

  glm::mat4 get_transform() const;
  void set_transform(glm::mat4 transform);

  glm::vec3 get_up() const;
  void set_up(glm::vec3 up_vector);

  glm::vec3 get_gravity() const;
  void set_gravity(glm::vec3 gravity);

  unsigned char get_frame_updated() const;
  void set_frame_updated(unsigned char frame_updated);

  bool get_on_ground() const;

  // Activates a sleeping physics object
  void activate();

  glm::vec3 get_velocity() const;

  glm::quat get_orientation() const;
  void set_orientation(glm::quat orientation);

  // Store Bullet's AABB in min and max
  void get_aabb(btVector3& min, btVector3& max) const;

  // Returns true if the character is in swinging mode
  bool get_is_swinging() const;

  // Attatch character to 'center' and start swinging
  void start_swing(glm::vec3 center);

  // Stops a swing
  void end_swing();

  // Returns the distance between character controller and swing center
  float get_rope_length() const;

  // Sets the distance between character controller and swing center
  void set_rope_length(float length);

  // Returns swing center
  glm::vec3 get_swing_center() const;

  // Returns desired direction
  glm::quat get_desired_direction();

  // Returns current direction
  glm::quat get_current_direction();

  // Adds a force to the character controller if swinging
  void add_swing_velocity(glm::vec3 velocity);

  // Initializes or updates capsule shape, gravity and up direction
  // Up is assumed to be normalized
  void update_properties(float radius, float height, glm::vec3 up, glm::vec3 gravity,
                         float turning_speed, float angle, bool face_moving_direction,
                         bool navmesh_collision, bool is_flying);

  // Destroys the object and cleans up Bullet resources
  void destroy(btDiscreteDynamicsWorld* world);

 private:
  // Performs a sweep test with the ghost object
  btCollisionWorld::ClosestConvexResultCallback sweep_test(btTransform& from, btTransform& to,
                                                           btDiscreteDynamicsWorld* world);

  // Performs a ray test
  btCollisionWorld::AllHitsRayResultCallback ray_test(btVector3 from, btVector3 to,
                                                      btDiscreteDynamicsWorld* world);

  // Create capsule shape
  void create_shape(float radius, float height);

  // Updates gravity and up direction and sets collision shape on ghost object and rigid body
  // Up is assumed to be normalized
  void set_properties(glm::vec3 up, glm::vec3 gravity, float turning_speed, float angle,
                      bool face_moving_direction, bool navmesh_collision, bool is_flying);

  // Collision groups and masks for different
  const int m_normal_group = CollisionGroup::CHARACTER_CONTROLLER;
  const int m_navmesh_group = CollisionGroup::CHARACTER_CONTROLLER | CollisionGroup::NAVMESH;

  const int m_normal_mask = CollisionGroup::STATIC | CollisionGroup::DYNAMIC |
                            CollisionGroup::TRIGGER | CollisionGroup::CHARACTER_CONTROLLER /*|
                            CollisionGroup::PROJECTILE*/
      ;
  const int m_navmesh_mask = CollisionGroup::TRIGGER | CollisionGroup::CHARACTER_CONTROLLER |
                             /*CollisionGroup::PROJECTILE |*/ CollisionGroup::NAVMESH |
                             CollisionGroup::DYNAMIC;

  // Contains results of the iterative_sweeping function
  struct SweepResult {
    btTransform result_transform;
    btVector3 hit_normal;
    btVector3 hit_point;
    bool hit;
  };

  // Performs multiple sweep tests until no obstacle is detected (or at most 'm_sweep_iterations'
  // times), adjusting the target position between every iteration if an obstacle is hit
  // Returns result transform
  // If ignore_ground_terminate is true, sweeping will not terminate when hitting ceiling/floor
  // If adjust_along_up is true, adjustment will only happen along up vector
  SweepResult iterative_sweeping(btTransform start_transform, btVector3 target_position,
                                 btDiscreteDynamicsWorld* world,
                                 bool ignore_ground_terminate = false,
                                 bool adjust_along_up = false);

  // Handles character rotation. Returns quaternion to rotate from (0,0,1) to up
  btQuaternion handle_rotation(btVector3 horizontal_move_vector, btVector3 up, float dt);

  // Returns the distance between position and swing center
  float get_rope_length_from(btVector3 position) const;

  std::shared_ptr<CollisionShape> m_shape;
  std::unique_ptr<btPairCachingGhostObject> m_ghost_object;

  btVector3 m_last_pos;
  btVector3 m_pos;
  btVector3 m_swinging_velocity;
  btQuaternion m_facing_direction;
  btQuaternion m_desired_direction;

  btVector3 m_dash_velocity;

  btVector3 m_up;

  // Attatchment point of a swing
  btVector3 m_swing_center;

  btVector3 m_gravity;

  float m_mass;

  float m_dash_duration;
  float m_dash_stop_velocity;

  float m_last_dt;

  // Velocity along the up axis. Positive values indicate downward velocity
  float m_vertical_velocity;
  float m_terminal_velocity;
  float m_height;
  float m_radius;
  float m_up_step;
  float m_down_step;

  float m_turning_speed;
  float m_facing_angle;

  // Current max length of rope
  float m_rope_length;

  // When colliding during swinging, reduce velocity along hit normal to this fraction
  float m_swing_bounce_friction;

  // Velocity^2 * m_rope_friction * dt is reduced from velocity every frame
  float m_rope_friction;

  // If the dot product between m_up and a collision normal is higher than this value, the surface
  // will be considered possible to stand on
  float m_max_slope;

  // The length of each adjustment step
  float m_adjustment_length;

  // Max number of times to sweep and adjust position for every sweep test
  int m_sweep_iterations;

  // Counter for the number of frames the character has been in the air
  unsigned m_in_air;

  // The number of frames a character has to not hit a ground object in order to be considered not
  // grounded
  unsigned m_in_air_threshold;

  unsigned char m_frame_updated;

  bool m_face_moving_direction;

  bool m_is_swinging;

  // If true, only collide with navmesh and other character controllers
  bool m_use_navmesh_collision;

  bool m_is_flying;
};
}  // namespace phy

#endif  // PHY_CHARACTER_CONTROLLER_HPP