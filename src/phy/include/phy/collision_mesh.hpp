#pragma once

#ifndef PHY_COLLISION_MESH_HPP
#define PHY_COLLISION_MESH_HPP

#include <bse/asset.hpp>
#include <bse/file_path.hpp>
#include <bse/result.hpp>
#include <ext/ext.hpp>

namespace phy {
// Asset containing a mesh to be used in the physics system
class CollisionMesh {
 public:
  // Loads CollisionMesh from file
  static bse::Result<CollisionMesh> load(bse::FilePath path);

  CollisionMesh();
  virtual ~CollisionMesh();

  CollisionMesh(const CollisionMesh& other) = delete;
  CollisionMesh(CollisionMesh&& other);

  CollisionMesh& operator=(const CollisionMesh& other) = delete;
  CollisionMesh& operator=(CollisionMesh&& other);

  // Returns pointer to the array of vertices
  const std::vector<float>& get_vertex_array() const;

  // Returns pointer to the array of indices
  const std::vector<unsigned>& get_index_array() const;

  // Returns number of vertices
  unsigned get_num_vertices() const;

  // Returns number of indices
  unsigned get_num_indices() const;

  // Returns Bullet representation of mesh data
  btTriangleIndexVertexArray* get_mesh_data() const;

 private:
  // Destroys this object
  void destroy();

  // Moves other into this
  void move_from(CollisionMesh&& other);

  // Bullet needs direct access to mesh memory
  // These should not change after the constructor
  std::vector<float> m_vertices;
  std::vector<unsigned> m_indices;

  btTriangleIndexVertexArray* m_mesh_data = nullptr;
};
}  // namespace phy
extern template class bse::Asset<phy::CollisionMesh>;

#endif  // PHY_COLLISION_MESH_HPP