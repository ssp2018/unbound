#pragma once

#ifndef PHY_COLLISION_SHAPES_HPP
#define PHY_COLLISION_SHAPES_HPP

#include <bse/assert.hpp>
#include <bse/asset.hpp>
#include <bse/log.hpp>
#include <bse/serialize.hpp>
#include <ext/ext.hpp>
#include <phy/collision_mesh.hpp>

namespace phy {
class PhysicsManager;

// This file contains the collision shapes used by the physics system

enum CollisionShapeType {
  BOX = 1 << 0,
  SPHERE = 1 << 1,
  CAPSULE = 1 << 2,
  CYLINDER = 1 << 3,
  CONVEX_HULL = 1 << 4,
  TRIANGLE_MESH = 1 << 5,
  COMPOUND = 1 << 6
};

// Vector conversion function
glm::vec3 vc(btVector3 v);
// Vector conversion function
btVector3 vc(glm::vec3 v);

// Matrix conversion function
glm::mat4 mc(const btTransform& m);
// Matrix conversion function
btTransform mc(const glm::mat4& m);

// Quaternion conversion function
glm::quat qc(const btQuaternion& q);
// Quaternion conversion function
btQuaternion qc(const glm::quat& q);

// Virtual base class for collision shapes
// Collision shapes are used to define bounding boxes for physics objects
class CollisionShape {
 public:
  virtual ~CollisionShape(){};

  // NOTE: get_radius have been removed, Bullet does not seem to calculate
  // bounding volumes correctly for some objects

  // Get radius of smallest sphere containing the shape
  // virtual float get_radius() const {
  //  LOG(FATAL) << "Called getRadius() on a type that has not defined it!";
  //  return 0.0f;
  //}

  // Returns a CollisionShapeType with corresponding bits set for every type of shape contained
  // within this shape
  virtual CollisionShapeType get_type() const = 0;

  // Get underlying representation
  virtual btCollisionShape* get_shape() const = 0;

  VIRTUAL_CLASS_SERFUN((CollisionShape)) {}
  VIRTUAL_CLASS_DESERFUN((CollisionShape)) {}
};

// Box bounding shape
class BoxCollisionShape : public CollisionShape {
  friend class PhysicsManager;

 public:
  BoxCollisionShape() {}
  BoxCollisionShape(glm::vec3 half_legths);
  BoxCollisionShape(BoxCollisionShape&& other);
  BoxCollisionShape& operator=(BoxCollisionShape&& other);

  virtual ~BoxCollisionShape();

  // Get radius of smallest sphere containing the shape
  // virtual float get_radius() const override;

  // Returns a CollisionShapeType with corresponding bits set for every type of shape contained
  // within this shape
  virtual CollisionShapeType get_type() const override { return CollisionShapeType::BOX; }

  // Get underlying representation
  virtual btCollisionShape* get_shape() const override;

  VIRTUAL_CLASS_SERFUN((BoxCollisionShape)) {
    CollisionShapeType type = CollisionShapeType::BOX;
    SERIALIZE(type);
    SERIALIZE(self.m_half_lengths);
  }
  VIRTUAL_CLASS_DESERFUN((BoxCollisionShape)) {
    DESERIALIZE(self.m_half_lengths);
    *this = std::move(BoxCollisionShape(m_half_lengths));
  }

 private:
  std::unique_ptr<btBoxShape> m_box;
  glm::vec3 m_half_lengths;
};

// Sphere bounding shape
class SphereCollisionShape : public CollisionShape {
 public:
  SphereCollisionShape() {}
  SphereCollisionShape(float radius);
  SphereCollisionShape(SphereCollisionShape&& other);
  SphereCollisionShape& operator=(SphereCollisionShape&& other);

  virtual ~SphereCollisionShape();

  // Get radius of smallest sphere containing the shape
  // virtual float get_radius() const override;

  // Returns a CollisionShapeType with corresponding bits set for every type of shape contained
  // within this shape
  virtual CollisionShapeType get_type() const override { return CollisionShapeType::SPHERE; }

  // Get underlying representation
  virtual btCollisionShape* get_shape() const override;

  VIRTUAL_CLASS_SERFUN((SphereCollisionShape)) {
    CollisionShapeType type = CollisionShapeType::SPHERE;
    SERIALIZE(type);
    SERIALIZE(self.m_radius);
  }
  VIRTUAL_CLASS_DESERFUN((SphereCollisionShape)) {
    DESERIALIZE(self.m_radius);
    *this = std::move(SphereCollisionShape(m_radius));
  }

 private:
  std::unique_ptr<btSphereShape> m_sphere;
  float m_radius;
};

// Capsule bounding shape
class CapsuleCollisionShape : public CollisionShape {
 public:
  CapsuleCollisionShape() {}
  CapsuleCollisionShape(float radius, float height);
  CapsuleCollisionShape(CapsuleCollisionShape&& other);
  CapsuleCollisionShape& operator=(CapsuleCollisionShape&& other);

  virtual ~CapsuleCollisionShape();

  // Get radius of smallest sphere containing the shape
  // virtual float get_radius() const override;

  // Returns a CollisionShapeType with corresponding bits set for every type of shape contained
  // within this shape
  virtual CollisionShapeType get_type() const override { return CollisionShapeType::CAPSULE; }

  // Get underlying representation
  virtual btCollisionShape* get_shape() const override;

  VIRTUAL_CLASS_SERFUN((CapsuleCollisionShape)) {
    CollisionShapeType type = CollisionShapeType::CAPSULE;
    SERIALIZE(type);
    SERIALIZE(self.m_radius);
    SERIALIZE(self.m_height);
  }
  VIRTUAL_CLASS_DESERFUN((CapsuleCollisionShape)) {
    DESERIALIZE(self.m_radius);
    DESERIALIZE(self.m_height);

    *this = std::move(CapsuleCollisionShape(m_radius, m_height));
  }

 private:
  std::unique_ptr<btCapsuleShapeZ> m_capsule;
  float m_radius;
  float m_height;
};

// Cylinder bounding shape
class CylinderCollisionShape : public CollisionShape {
 public:
  CylinderCollisionShape() {}
  CylinderCollisionShape(glm::vec3 half_legths);
  CylinderCollisionShape(CylinderCollisionShape&& other);
  CylinderCollisionShape& operator=(CylinderCollisionShape&& other);

  virtual ~CylinderCollisionShape();

  // Get radius of smallest sphere containing the shape
  // virtual float get_radius() const override;

  // Returns a CollisionShapeType with corresponding bits set for every type of shape contained
  // within this shape
  virtual CollisionShapeType get_type() const override { return CollisionShapeType::CYLINDER; }

  // Get underlying representation
  virtual btCollisionShape* get_shape() const override;

  VIRTUAL_CLASS_SERFUN((CylinderCollisionShape)) {
    CollisionShapeType type = CollisionShapeType::CYLINDER;
    SERIALIZE(type);
    SERIALIZE(self.m_half_legths);
  }
  VIRTUAL_CLASS_DESERFUN((CylinderCollisionShape)) {
    DESERIALIZE(self.m_half_legths);

    *this = std::move(CylinderCollisionShape(m_half_legths));
  }

 private:
  std::unique_ptr<btCylinderShapeZ> m_cylinder;
  glm::vec3 m_half_legths;
};

// A convex hull shape created from a set of vertices
class ConvexHullCollisionShape : public CollisionShape {
 public:
  ConvexHullCollisionShape() {}
  ConvexHullCollisionShape(bse::Asset<CollisionMesh> mesh);
  ConvexHullCollisionShape(ConvexHullCollisionShape&& other);
  ConvexHullCollisionShape& operator=(ConvexHullCollisionShape&& other);

  virtual ~ConvexHullCollisionShape();

  // Get radius of smallest sphere containing the shape
  // virtual float get_radius() const override;

  // Returns a CollisionShapeType with corresponding bits set for every type of shape contained
  // within this shape
  virtual CollisionShapeType get_type() const override { return CollisionShapeType::CONVEX_HULL; }

  // Get underlying representation
  virtual btCollisionShape* get_shape() const override;

  bse::Asset<CollisionMesh> get_mesh_asset() const;

  VIRTUAL_CLASS_SERFUN((ConvexHullCollisionShape)) {
    CollisionShapeType type = CollisionShapeType::CONVEX_HULL;
    SERIALIZE(type);
    SERIALIZE(self.m_mesh_asset);
  }
  VIRTUAL_CLASS_DESERFUN((ConvexHullCollisionShape)) {
    DESERIALIZE(self.m_mesh_asset);

    *this = std::move(ConvexHullCollisionShape(m_mesh_asset));
  }

 private:
  std::unique_ptr<btConvexHullShape> m_hull;
  bse::Asset<CollisionMesh> m_mesh_asset;
};

// A shape composed of triangles in a mesh
// An object with this shape cannot be dynamic
class TriangleMeshCollisionShape : public CollisionShape {
 public:
  TriangleMeshCollisionShape() {}
  TriangleMeshCollisionShape(bse::Asset<CollisionMesh> mesh);
  TriangleMeshCollisionShape(TriangleMeshCollisionShape&& other);
  TriangleMeshCollisionShape& operator=(TriangleMeshCollisionShape&& other);

  virtual ~TriangleMeshCollisionShape();

  // Get radius of smallest sphere containing the shape
  // virtual float get_radius() const override;

  // Returns a CollisionShapeType with corresponding bits set for every type of shape contained
  // within this shape
  virtual CollisionShapeType get_type() const override { return CollisionShapeType::TRIANGLE_MESH; }

  // Get underlying representation
  virtual btCollisionShape* get_shape() const override;

  bse::Asset<CollisionMesh> get_mesh_asset() const;

  VIRTUAL_CLASS_SERFUN((TriangleMeshCollisionShape)) {
    CollisionShapeType type = CollisionShapeType::TRIANGLE_MESH;
    SERIALIZE(type);
    SERIALIZE(self.m_mesh_asset);
  }
  VIRTUAL_CLASS_DESERFUN((TriangleMeshCollisionShape)) {
    DESERIALIZE(self.m_mesh_asset);

    *this = std::move(TriangleMeshCollisionShape(m_mesh_asset));
  }

  // Clear the cache of mesh shapes. Call at program exit
  static void clear_mesh_shapes();

 private:
  std::shared_ptr<btBvhTriangleMeshShape> m_unscaled_mesh = nullptr;
  std::unique_ptr<btScaledBvhTriangleMeshShape> m_mesh = nullptr;
  bse::Asset<CollisionMesh> m_mesh_asset;

  // Cache of mesh shapes
  static std::unordered_map<bse::AssetID<CollisionMesh>, std::shared_ptr<btBvhTriangleMeshShape>>
      m_mesh_shapes;
  static std::mutex m_mesh_shapes_lock;

  // Returns or creates a mesh shape in the cache
  static std::shared_ptr<btBvhTriangleMeshShape> get_triangle_mesh_shape(
      bse::Asset<CollisionMesh>& asset);
};

// A shape composed of other CollisionShapes
class CompoundCollisionShape : public CollisionShape {
 public:
  // CompoundCollisionShape will own child shapes
  // Shapes contain pairs of children and their transform matrices (local to the
  // CompoundCollisionShape)
  CompoundCollisionShape() {}
  CompoundCollisionShape(
      std::vector<std::pair<std::unique_ptr<CollisionShape>, glm::mat4>>&& shapes);
  CompoundCollisionShape(CompoundCollisionShape&& other);
  CompoundCollisionShape& operator=(CompoundCollisionShape&& other);

  virtual ~CompoundCollisionShape();

  // Get radius of smallest sphere containing the shape
  // virtual float get_radius() const override;

  // Returns a CollisionShapeType with corresponding bits set for every type of shape contained
  // within this shape
  virtual CollisionShapeType get_type() const override;

  // Returns pair of child collision shape and its local transform (relative to the
  // CompoundCollisionShape)
  std::pair<std::unique_ptr<CollisionShape>, glm::mat4>& get_child(size_t index) {
    ASSERT(index < m_children.size()) << "Child index out of range!";
    return m_children[index];
  }

  // Get underlying representation
  virtual btCollisionShape* get_shape() const override;

  VIRTUAL_CLASS_SERFUN((CompoundCollisionShape)) {
    CollisionShapeType type = self.get_type();
    int num_children = m_children.size();
    SERIALIZE(type);

    SERIALIZE(num_children);
    for (int i = 0; i < num_children; i++) {
      SERIALIZE(*m_children[i].first);
      SERIALIZE(m_children[i].second);
    }
  }
  VIRTUAL_CLASS_DESERFUN((CompoundCollisionShape)) {
    int num_children;
    ::bse::deserialize(num_children, data, offset);

    m_children.resize(num_children);

    for (int i = 0; i < num_children; i++) {
      CollisionShapeType child_type;
      ::bse::deserialize(child_type, data, offset);

      if (child_type & CollisionShapeType::COMPOUND) {
        m_children[i].first.reset(new CompoundCollisionShape());
      } else if (child_type & CollisionShapeType::BOX) {
        m_children[i].first.reset(new BoxCollisionShape());
      } else if (child_type & CollisionShapeType::SPHERE) {
        m_children[i].first.reset(new SphereCollisionShape());
      } else if (child_type & CollisionShapeType::CAPSULE) {
        m_children[i].first.reset(new CapsuleCollisionShape());
      } else if (child_type & CollisionShapeType::CYLINDER) {
        m_children[i].first.reset(new CylinderCollisionShape());
      } else if (child_type & CollisionShapeType::CONVEX_HULL) {
        m_children[i].first.reset(new ConvexHullCollisionShape());
      } else if (child_type & CollisionShapeType::TRIANGLE_MESH) {
        m_children[i].first.reset(new TriangleMeshCollisionShape());
      } /*else if (child_type & CollisionShapeType::ANIMATED_TRIANGLE_MESH) {
        ASSERT(false) << "Attempted to deserialize AnimatedTriangleMeshCollisionShape";
      }*/
      else {
        ASSERT(false) << "Unknown child type when deserializing CompoundCollisionShape. Type: "
                      << child_type;
      }

      DESERIALIZE(*m_children[i].first);
      DESERIALIZE(m_children[i].second);
    }
  }

 private:
  std::unique_ptr<btCompoundShape> m_compound;

  // Children and their local transforms
  std::vector<std::pair<std::unique_ptr<CollisionShape>, glm::mat4>> m_children;
};

}  // namespace phy

namespace bse {
inline void serialize(const phy::CollisionShapeType& value, SerializedData& data) {
  //
  memcpy(&data.bytes[data.head], &value, sizeof(phy::CollisionShapeType));
  data.head += sizeof(phy::CollisionShapeType);
  return;
}

inline void deserialize(phy::CollisionShapeType& value, const SerializedData& data, int& offset) {
  //
  memcpy(&value, &data.bytes[offset], sizeof(phy::CollisionShapeType));
  offset += sizeof(phy::CollisionShapeType);
}
}  // namespace bse
#endif  // PHY_COLLISION_SHAPES_HPP