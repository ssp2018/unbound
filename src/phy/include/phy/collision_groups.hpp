#pragma once

#ifndef PHY_COLLISION_GROUPS_HPP
#define PHY_COLLISION_GROUPS_HPP

#include <bse/serialize.hpp>

namespace phy {
struct CollisionGroup {
  enum Group {
    STATIC = 1 << 0,
    DYNAMIC = 1 << 1,
    TRIGGER = 1 << 2,
    PROJECTILE = 1 << 3,
    CHARACTER_CONTROLLER = 1 << 4,
    NAVMESH = 1 << 5,
    NONE = 1 << 6,
    PLAYER = 1 << 7,
    ENEMY = 1 << 8
  };
};
}  // namespace phy

namespace bse {
inline void serialize(const phy::CollisionGroup& value, SerializedData& data) {
  //
  memcpy(&data.bytes[data.head], &value, sizeof(phy::CollisionGroup));
  data.head += sizeof(phy::CollisionGroup);
  return;
}

inline void deserialize(phy::CollisionGroup& value, const SerializedData& data, int& offset) {
  //
  memcpy(&value, &data.bytes[offset], sizeof(phy::CollisionGroup));
  offset += sizeof(phy::CollisionGroup);
}
}  // namespace bse

#endif  // PHY_COLLISION_GROUPS_HPP