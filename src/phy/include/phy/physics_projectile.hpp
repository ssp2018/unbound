#pragma once
#ifndef PHY_PHYSICS_PROJECTILE_HPP
#define PHY_PHYSICS_PROJECTILE_HPP

#include <ext/ext.hpp>
#include <phy/collision_shapes.hpp>

namespace phy {

// An object representing a projectile that can collide with objects in the physics world
class PhysicsProjectile {
 public:
  PhysicsProjectile() {}
  PhysicsProjectile(uint32_t id, glm::vec3 position, float radius, float length, glm::vec3 velocity,
                    glm::vec3 gravity, float mass, btDiscreteDynamicsWorld* world);
  ~PhysicsProjectile();
  PhysicsProjectile(const PhysicsProjectile& other) = delete;
  PhysicsProjectile(PhysicsProjectile&& other) = delete;

  PhysicsProjectile& operator=(const PhysicsProjectile& other) = delete;
  PhysicsProjectile& operator=(PhysicsProjectile&& other) = delete;

  void set_position(glm::vec3 position);

  glm::mat4 get_transform() const;
  glm::mat4 get_last_transform() const;

  glm::vec3 get_velocity() const;
  glm::vec3 get_gravity() const;
  float get_length() const;
  float get_radius() const;
  float get_mass() const;

  // Store Bullet's AABB in min and max
  void get_aabb(btVector3& min, btVector3& max) const;

  // Store Bullet's AABB relative to a transform in min and max
  void get_aabb_relative(btTransform& transform, btVector3& min, btVector3& max) const;

  // Sets various properties of projectile
  void update_properties(float radius, float length, glm::vec3 velocity, glm::vec3 gravity,
                         float mass);

  unsigned char get_frame_updated() const;

  // Sets the frame_updated bit
  void set_frame_updated(unsigned char frame_updated);

  // Simulates the projectile
  void simulate(float dt, btDiscreteDynamicsWorld* world);

  // Destroys the object
  void destroy(btDiscreteDynamicsWorld* world);

  // Enables or disables collsion
  void set_collision_properties(bool reporting_enabled, bool destruction_enabled,
                                bool collide_with_animated, btDiscreteDynamicsWorld* world);

  // Returns pointer to shape
  btCapsuleShape* get_shape();

 private:
  // Performs a sweep test with the collision object
  btCollisionWorld::ClosestConvexResultCallback sweep_test(btTransform& from, btTransform& to,
                                                           btDiscreteDynamicsWorld* world);

  // Creates appropriate collision shape, assigns it to m_shape, and sets it as m_collision_object's
  // collision shape
  void create_shape();

  // Adds projectile to physics world
  void add_to_world(btDiscreteDynamicsWorld* world);

  // Removes projectile from physics world
  void remove_from_world(btDiscreteDynamicsWorld* world);

  btTransform m_current_transform;
  btTransform m_last_transform;

  btVector3 m_velocity;
  btVector3 m_gravity;

  btQuaternion m_facing_direction;

  float m_length = 0.3f;
  float m_radius = 0.1f;
  float m_mass = 1.0f;

  std::unique_ptr<btCapsuleShape> m_shape;
  std::unique_ptr<btCollisionObject> m_collision_object;

  unsigned char m_frame_updated;

  bool m_destroy_on_collision;
  bool m_report_collisions;
  bool m_in_world;
  bool m_collide_with_animated;
};
}  // namespace phy

#endif  // PHY_PHYSICS_PROJECTILE_HPP
