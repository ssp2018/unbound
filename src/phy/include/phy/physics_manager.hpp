#pragma once

#ifndef PHY_PHYSICS_MANAGER_HPP
#define PHY_PHYSICS_MANAGER_HPP

#include <bse/result.hpp>
#include <ext/ext.hpp>
#include <phy/character_controller.hpp>
#include <phy/collision_shapes.hpp>
#include <phy/physics_projectile.hpp>

namespace phy {
class CharacterController;
class CollisionShape;

// Manages objects present in physics simulation
// Different physics components should be mutually exclusive (only one should exist on an entitiy
// at a time) but this check should be done at the entity component system level
class PhysicsManager {
  struct PhysicsObjectBase {
    // Collision shape of the object
    // shared_ptr because Bullet collsion shapes can be shared
    std::shared_ptr<phy::CollisionShape> m_shape;

    // Set to the PhysicsManager's 'm_frame_bit' when updated. Not updated
    // objects are assumed to have been deleted and removed from the system
    unsigned char m_frame_updated;
  };
  struct PhysicsObject : public PhysicsObjectBase {
    std::unique_ptr<btRigidBody> m_body;
  };

 public:
  struct AABB {
    glm::vec3 min;
    glm::vec3 max;
  };

  PhysicsManager() = delete;

  // Creates PhysicsManager with specified gravity constant
  // 'max_steps' determines max number of simulation ticks per call to simulate()
  PhysicsManager(glm::vec3 gravity, uint32_t max_steps = 3);
  PhysicsManager(const PhysicsManager& other) = delete;
  PhysicsManager(PhysicsManager&& other) = delete;

  PhysicsManager& operator=(const PhysicsManager& other) = delete;
  PhysicsManager& operator=(PhysicsManager&& other) = delete;

  virtual ~PhysicsManager();

  // Sets max number of simulation ticks per call to simulate()
  void set_max_steps(uint32_t max_steps);
  uint32_t get_max_steps() const;

  // Sets gravity. Note: positive Z is up
  void set_gravity(glm::vec3 gravity);
  glm::vec3 get_gravity() const;

  // Updates the physics simulation
  void simulate(float dt);

  // Sets a debug drawing object
  void set_debug_drawer(btIDebugDraw* drawer);

  // Do debug drawing this frame
  void debug_draw();

  // Cast a ray into the world and return info on objects hit
  // 'direction' will be normalized
  btDiscreteDynamicsWorld::AllHitsRayResultCallback raycast(glm::vec3 origin, glm::vec3 direction,
                                                            float length);

  // Sweep a sphere into the world and return info on objects hit
  // 'direction' will be normalized
  btDiscreteDynamicsWorld::ClosestConvexResultCallback sphere_sweep(glm::vec3 origin,
                                                                    glm::vec3 direction,
                                                                    float length,
                                                                    float sphere_radius);

  struct ProjectileTriangleCollision {
    // Rotation from triangle normal to projectile direction
    glm::quat rotation;

    // Projectile position on the plane of the triangle
    glm::vec3 barycentric;

    // Projectile's distance from triangle's plane
    float distance_from_plane;

    unsigned face_index;
    bool hit;
  };

  // Performs a collision test between a PhysicsProjectile and an animated mesh
  ProjectileTriangleCollision projectile_animated_mesh_collision_test(
      uint32_t projectile_id, btBvhTriangleMeshShape* animated_shape,
      const glm::mat4& animated_transform, CollisionGroup::Group projectile_group,
      CollisionGroup::Group animated_group);

  // ------------------------------------
  // ---------- Update objects ----------
  // ------------------------------------

  // Updates static object with 'id'. Mainly just marks it as active this frame
  // and returns true if the object exists
  bool update_static_object(uint32_t id);

  // Updates dynamic object with 'id'. Mainly just marks it as active this frame
  // and returns true if the object exists
  bool update_dynamic_object(uint32_t id);

  // Updates character controller with 'id'. Mainly just marks it as active this frame
  // and returns true if the object exists
  bool update_character_controller(uint32_t id);

  // Updates projectile with 'id'. Mainly just marks it as active this frame
  // and returns true if the object exists
  bool update_projectile(uint32_t id);

  // ---------------------------------
  // ---------- Add objects ----------
  // ---------------------------------

  // Adds a static object with 'id'
  void add_static_object(uint32_t id, glm::mat4 transform_no_scale, glm::vec3 scale,
                         std::shared_ptr<phy::CollisionShape> shape, bool is_trigger);

  // Adds a static object with 'id'
  void add_dynamic_object(uint32_t id, glm::mat4 transform,
                          std::shared_ptr<phy::CollisionShape> shape, float mass);

  // Adds a character controller with 'id'
  void add_character_controller(uint32_t id,                  //
                                glm::vec3 position,           //
                                float radius,                 //
                                float height,                 //
                                float turning_speed,          //
                                float angle,                  //
                                bool face_moving_direction,   //
                                float mass,                   //
                                glm::vec3 gravity,            //
                                glm::vec3 velocity,           //
                                bool navmesh_collision,       //
                                bool is_flying,               //
                                glm::vec3 up,                 //
                                glm::quat desired_direction,  //
                                glm::quat facing_direction,   //
                                bool on_ground);

  // Adds a projectile with 'id'
  void add_projectile(uint32_t id, glm::vec3 position, float radius, float length,
                      glm::vec3 velocity, glm::vec3 gravity, float mass);

  // ------------------------------------
  // ---------- Remove objects ----------
  // ------------------------------------

  // Checks static objects and remove stale ones. Objects are stale if an update function hasn't
  // been called on it this frame
  void remove_stale_static_objects();

  // Checks dynamic objects and remove stale ones. Objects are stale if an update function hasn't
  // been called on it this frame
  void remove_stale_dynamic_objects();

  // Checks character controllers and remove stale ones. Objects are stale if an update function
  // hasn't been called on it this frame
  void remove_stale_character_controllers();

  // Checks projectiles and remove stale ones. Objects are stale if an update function
  // hasn't been called on it this frame
  void remove_stale_projectiles();

  // -------------------------------------------
  // ---------- Get object properties ----------
  // -------------------------------------------

  // Get the transform matrix of static object with 'id'
  bse::Result<glm::mat4> get_static_transform(uint32_t id);

  // Get the transform matrix of dynamic object with 'id'
  bse::Result<glm::mat4> get_dynamic_transform(uint32_t id);

  // Get the transform matrix of character controller with 'id'
  bse::Result<glm::mat4> get_character_controller_transform(uint32_t id);

  // Get the transform matrix of projectile with 'id'
  bse::Result<glm::mat4> get_projectile_transform(uint32_t id);

  // Get Bullet's AABB for static object
  AABB get_static_object_aabb(uint32_t id);

  // Get Bullet's AABB for dynamic object
  AABB get_dynamic_object_aabb(uint32_t id);

  // Get Bullet's AABB for character controller
  AABB get_character_controller_aabb(uint32_t id);

  // Get Bullet's AABB for projectile
  AABB get_projectile_aabb(uint32_t id);

  // Returns the velocity of specified character controller
  glm::vec3 get_character_controller_velocity(uint32_t id);

  // Returns true if specified character controller is standing on ground
  bool get_character_controller_on_ground(uint32_t id);

  // Get velocity for dynamic object
  glm::vec3 get_dynamic_object_velocity(uint32_t id);

  // Get the scale of static object with 'id'
  glm::vec3 get_static_scale(uint32_t id);

  // Get the scale of dynamic object with 'id'
  glm::vec3 get_dynamic_scale(uint32_t id);

  // Get the velocity of projectile with 'id'
  glm::vec3 get_projectile_velocity(uint32_t id);

  // Get the desired direction of character controller with 'id'
  glm::quat get_character_controller_desired_direction(uint32_t id);

  // Get the current direction of character controller with 'id'
  glm::quat get_character_controller_current_direction(uint32_t id);

  // -------------------------------------------
  // ---------- Set object properties ----------
  // -------------------------------------------

  // Set the transform matrix of static object with 'id'
  void set_static_transform(uint32_t id, const glm::mat4& transform_no_scale, glm::vec3 scale);

  // Set the transform matrix of dynamic object with 'id'
  void set_dynamic_transform(uint32_t id, const glm::mat4& transform);

  // Set the transform matrix of character controller with 'id'
  void set_character_controller_transform(uint32_t id, const glm::mat4& transform);

  // Set the position of projectile with 'id'
  void set_projectile_position(uint32_t id, const glm::vec3 position);

  // --------------------------------------
  // ---------- Activate objects ----------
  // --------------------------------------

  // Activates a static object, resuming simulation if it is asleep
  void activate_static_object(uint32_t id);

  // Activates a dynamic object, resuming simulation if it is asleep
  void activate_dynamic_object(uint32_t id);

  // Activates a character controller, resuming simulation if it is asleep
  void activate_character_controller(uint32_t id);

  // ----------------------------------------------
  // ---------- Update object properties ----------
  // ----------------------------------------------

  // Updates various properties of a static object
  void update_static_object_properties(uint32_t id, std::shared_ptr<phy::CollisionShape> shape,
                                       bool is_trigger);

  // Updates various properties of a dynamic object
  void update_dynamic_object_properties(uint32_t id, std::shared_ptr<phy::CollisionShape> shape,
                                        float mass);

  // Updates various properties of a character controller
  void update_character_controller_properties(uint32_t id, float mass, float radius, float height,
                                              glm::vec3 gravity, float turning_speed, float angle,
                                              bool face_moving_direction, bool navmesh_collision,
                                              bool is_flying, glm::vec3 up);

  // Updates various properties of a projectile
  void update_projectile_properties(uint32_t id, float radius, float length, glm::vec3 velocity,
                                    glm::vec3 gravity, float mass);

  // -------------------------------------------------
  // ---------- Dynamic component functions ----------
  // -------------------------------------------------

  void dynamic_object_add_force(uint32_t id, glm::vec3 force);
  void dynamic_object_add_impulse(uint32_t id, glm::vec3 impulse);

  // ----------------------------------------------------
  // ---------- Character controller functions ----------
  // ----------------------------------------------------

  // Moves the character controller
  void character_controller_move(uint32_t id, glm::vec3 move_vector, float dt);

  // Adds upward velocity to make the character controller jump
  void character_controller_jump(uint32_t id, float jump_strength);

  // Fixes the character velocity for a duration
  void character_controller_dash(uint32_t id, glm::vec3 dash_velocity, float duration);

  // End a character controller swing
  void character_controller_end_swing(uint32_t id);

  // Starts a character controller swing
  void character_controller_start_swing(uint32_t id, glm::vec3 swing_center);

  // Sets character controller rope length for active swing
  void character_controller_set_rope_length(uint32_t id, float length);

  // Returns character controller rope length for active swing
  float character_controller_get_rope_length(uint32_t id);

  // Applies a velocity to a swinging character
  void character_controller_add_swing_velocity(uint32_t id, glm::vec3 velocity);

  // Returns true if character controller is in swinging mode
  bool character_controller_get_is_swinging(uint32_t id);

  // ------------------------------------------
  // ---------- Projectile functions ----------
  // ------------------------------------------

  // Updates projectile simulation
  void projectile_simulate(uint32_t id, float dt);

  // Set a projectile's collision status. If reporting_enabled is true, collisions will be
  // registered. If destruction_enabled is true, the projectile will use sweep tests (and stop on
  // collision). Destruction cannot happen if reporting_enabled is false
  void projectile_set_collision_enabled(uint32_t id, bool reporting_enabled,
                                        bool destruction_enabled, bool collide_with_animated);

  // ----------------------------------------------
  // ---------- Collision event handling ----------
  // ----------------------------------------------

  // Handles a tick callback. Should only be called by bullet_tick_callback()
  void handle_callback(btDynamicsWorld* world);

  // Struct containing info on collision events
  struct Collision {
    uint32_t object1;
    uint32_t object2;

    glm::vec3 collision_point;
    glm::vec3 collision_normal;
  };

  // Return vector of new collisions that need broadcasting
  const std::vector<Collision>& get_new_collisions() const;
  // Return vector of ended collisions that need broadcasting
  const std::vector<Collision>& get_ended_collisions() const;

  // Clear new and ended collisions vector
  void clear_collisions();

  // Clears all current objects. Intended for use on deserialization
  void reset() const;

  // Update the m_frame_bit variable. Must only be called once per frame!
  void swap_frame();

  // Add navmesh triangle shapes to world
  void add_navmesh_triangle_shapes(
      const std::vector<std::unique_ptr<btBvhTriangleMeshShape>>& shapes);

  // Remove navmesh triangle shapes from world
  void remove_navmesh_triangle_shapes();

 private:
  // Get 0 or 1 on alternating update cycles
  unsigned char current_frame() const;

  // Get opposite of current_frame()
  unsigned char next_frame() const;

  // Finds the object with 'id' and assigns a collision shape to it
  void set_collision_shape(uint32_t id, std::unique_ptr<CollisionShape>&& new_collision_shape);

  // Add new collision to m_new_collisions
  void add_new_collision(btPersistentManifold* manifold, uint32_t id1, uint32_t id2);

  // Add an ended collision to m_ended_collision
  void add_new_collision_ended(uint32_t id1, uint32_t id2);

  // Decomposes and returns the scaling represented by the matrix
  glm::vec3 get_scale_from_matrix(const glm::mat4& matrix) const;

  std::vector<std::unique_ptr<btRigidBody>> m_navmesh_bodies;

  // Maps of different types of objects
  // Static and dynamic objects are separate because they need different setup/teardown
  // Key is the EventHandle ID of the associated object
  mutable std::unordered_map<uint32_t, PhysicsObject> m_static_objects;
  mutable std::unordered_map<uint32_t, PhysicsObject> m_dynamic_objects;
  mutable std::unordered_map<uint32_t, CharacterController> m_character_controllers;
  mutable std::unordered_map<uint32_t, PhysicsProjectile> m_projectiles;

  // Map of object contacts. Key is first object id, contents of nested map is the frame updated,
  // and the key is the id of the touching object. The contact is only stored
  // for the object with the lower id
  mutable std::unordered_map<uint32_t, std::unordered_map<uint32_t, unsigned char>> m_contacts;

  // Contains new collision events that need to be broadcasted
  mutable std::vector<Collision> m_new_collisions;
  // Contains ended collision events that need to be broadcasted
  std::vector<Collision> m_ended_collisions;

  // Objects for physics world
  std::unique_ptr<btDefaultCollisionConfiguration> m_collision_configuration;
  std::unique_ptr<btCollisionDispatcher> m_dispatcher;
  std::unique_ptr<btBroadphaseInterface> m_broadphase_interface;
  std::unique_ptr<btSequentialImpulseConstraintSolver> m_solver;
  std::unique_ptr<btDiscreteDynamicsWorld> m_world;  // change type to btSoftRigidDynamicsWorld?
  std::unique_ptr<btGhostPairCallback> m_ghost_pair_callback;

  // Max physics simulation steps per update
  uint32_t m_max_steps;

  // Flips between 0 and 1 every update cycle
  unsigned char m_frame_bit;
};

}  // namespace phy
#endif  // PHY_PHYSICS_MANAGER_HPP
