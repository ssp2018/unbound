#include "phy/collision_shapes.hpp"
#include <phy/physics_manager.hpp>
#include <phy/physics_projectile.hpp>
class btCollisionShape;
class btConvexShape;

namespace phy {

PhysicsProjectile::PhysicsProjectile(uint32_t id, glm::vec3 position, float radius, float length,
                                     glm::vec3 velocity, glm::vec3 gravity, float mass,
                                     btDiscreteDynamicsWorld* world) {
  // Create collision object
  m_collision_object = std::make_unique<btCollisionObject>();

  // Set user index to id. This will be used to identify the object during collision checking
  m_collision_object->setUserIndex(id);

  update_properties(radius, length, velocity, gravity, mass);
  m_facing_direction = btQuaternion(1, 0, 0, 0);

  // Set position
  m_current_transform = btTransform::getIdentity();
  m_current_transform.setOrigin(vc(position));

  m_last_transform = m_current_transform;

  m_collision_object->setWorldTransform(m_current_transform);

  m_destroy_on_collision = false;
  m_report_collisions = true;
  m_collide_with_animated = false;
  m_in_world = false;
  add_to_world(world);

  // If velocity is not zero, update facing direction
  if (!m_velocity.fuzzyZero()) {
    m_facing_direction = qc(glm::rotation(glm::vec3(0, 1, 0), vc(m_velocity.normalized())));
  }
}

PhysicsProjectile::~PhysicsProjectile() {}

void PhysicsProjectile::update_properties(float radius, float length, glm::vec3 velocity,
                                          glm::vec3 gravity, float mass) {
  m_velocity = vc(velocity);
  m_gravity = vc(gravity);
  m_length = length;
  m_radius = radius;
  m_mass = mass;

  create_shape();
}

unsigned char PhysicsProjectile::get_frame_updated() const { return m_frame_updated; }

void PhysicsProjectile::set_frame_updated(unsigned char frame_updated) {
  m_frame_updated = frame_updated;
}

glm::mat4 PhysicsProjectile::get_transform() const { return mc(m_current_transform); }
glm::mat4 PhysicsProjectile::get_last_transform() const { return mc(m_last_transform); }

void PhysicsProjectile::simulate(float dt, btDiscreteDynamicsWorld* world) {
  m_last_transform = m_current_transform;

  m_velocity += m_gravity * dt;

  btTransform start = m_collision_object->getWorldTransform();
  btTransform end = start;

  end.setOrigin(start.getOrigin() + m_velocity * dt);

  // If velocity is not zero, update facing direction
  if (!m_velocity.fuzzyZero()) {
    m_facing_direction = qc(glm::rotation(glm::vec3(0, 1, 0), vc(m_velocity.normalized())));
  }

  if (m_report_collisions) {
    add_to_world(world);
  } else {
    remove_from_world(world);
  }

  end.setRotation(m_facing_direction);

  if (m_destroy_on_collision && m_report_collisions) {
    remove_from_world(world);
    btCollisionWorld::ClosestConvexResultCallback callback = sweep_test(start, end, world);
    add_to_world(world);

    // If sweep test hits, adjust end transform
    if (callback.hasHit()) {
      btVector3 position;
      position.setInterpolate3(start.getOrigin(), end.getOrigin(), callback.m_closestHitFraction);

      end.setOrigin(position);
    }
  }
  m_collision_object->setWorldTransform(end);

  m_current_transform = end;
}

void PhysicsProjectile::destroy(btDiscreteDynamicsWorld* world) {
  remove_from_world(world);
  m_shape.reset();
  m_collision_object.reset();
}

void PhysicsProjectile::set_position(glm::vec3 position) {
  btTransform transform = m_collision_object->getWorldTransform();
  transform.setOrigin(vc(position));
  m_collision_object->setWorldTransform(transform);
}

glm::vec3 PhysicsProjectile::get_velocity() const { return vc(m_velocity); }

glm::vec3 PhysicsProjectile::get_gravity() const { return vc(m_gravity); }

float PhysicsProjectile::get_length() const { return m_length; }
float PhysicsProjectile::get_radius() const { return m_radius; }
float PhysicsProjectile::get_mass() const { return m_mass; }

void PhysicsProjectile::get_aabb(btVector3& min, btVector3& max) const {
  m_shape->getAabb(m_collision_object->getWorldTransform(), min, max);
}

void PhysicsProjectile::get_aabb_relative(btTransform& transform, btVector3& min,
                                          btVector3& max) const {
  m_shape->getAabb(transform, min, max);
}

void PhysicsProjectile::create_shape() {
  m_shape = std::make_unique<btCapsuleShape>(m_radius, m_length);
  m_collision_object->setCollisionShape(m_shape.get());
}

void PhysicsProjectile::add_to_world(btDiscreteDynamicsWorld* world) {
  if (!m_in_world && m_report_collisions) {
    if (m_collide_with_animated) {
      world->addCollisionObject(m_collision_object.get(), CollisionGroup::PROJECTILE,
                                CollisionGroup::STATIC | CollisionGroup::DYNAMIC |
                                    CollisionGroup::TRIGGER | CollisionGroup::PROJECTILE);
    } else {
      world->addCollisionObject(m_collision_object.get(), CollisionGroup::PROJECTILE,
                                CollisionGroup::STATIC | CollisionGroup::DYNAMIC |
                                    CollisionGroup::TRIGGER | CollisionGroup::PROJECTILE |
                                    CollisionGroup::CHARACTER_CONTROLLER);
    }

    m_collision_object->setCollisionFlags(m_collision_object->getCollisionFlags() |
                                          btCollisionObject::CF_NO_CONTACT_RESPONSE);

    m_in_world = true;
  }
}

void PhysicsProjectile::remove_from_world(btDiscreteDynamicsWorld* world) {
  if (m_in_world) {
    world->removeCollisionObject(m_collision_object.get());
    m_in_world = false;
  }
}

btCollisionWorld::ClosestConvexResultCallback PhysicsProjectile::sweep_test(
    btTransform& from, btTransform& to, btDiscreteDynamicsWorld* world) {
  btCollisionWorld::ClosestConvexResultCallback callback(from.getOrigin(), to.getOrigin());
  callback.m_collisionFilterGroup = CollisionGroup::PROJECTILE;
  callback.m_collisionFilterMask =
      CollisionGroup::STATIC | CollisionGroup::DYNAMIC | CollisionGroup::PROJECTILE;
  btCollisionShape* col_shape = m_shape.get();
  btConvexShape* shape = reinterpret_cast<btConvexShape*>(col_shape);

  float length = (from.getOrigin() - to.getOrigin()).length();
  if (length > 0.001f) {
    world->convexSweepTest(shape, from, to, callback,
                           world->getDispatchInfo().m_allowedCcdPenetration);
  } else {
    // Distance is too small
    callback.m_closestHitFraction = 0.0f;
    callback.m_hitPointWorld = btVector3(0, 0, 0);
    callback.m_hitNormalWorld = btVector3(0, 0, 1);
  }

  return callback;
}

void PhysicsProjectile::set_collision_properties(bool reporting_enabled, bool destruction_enabled,
                                                 bool collide_with_animated,
                                                 btDiscreteDynamicsWorld* world) {
  m_destroy_on_collision = destruction_enabled;
  m_report_collisions = reporting_enabled;

  if (m_collide_with_animated != collide_with_animated && m_in_world) {
    m_collide_with_animated = collide_with_animated;
    remove_from_world(world);
    add_to_world(world);
  } else {
    m_collide_with_animated = collide_with_animated;
  }
}

btCapsuleShape* PhysicsProjectile::get_shape() { return m_shape.get(); }

}  // namespace phy