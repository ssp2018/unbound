#include "bse/file_path.hpp"
#include "bse/log.hpp"
#include "bse/result.hpp"
#include <bse/assert.hpp>
#include <bse/file.hpp>
#include <bse/mesh/mesh_loader.hpp>
#include <phy/collision_mesh.hpp>
template class bse::Asset<phy::CollisionMesh>;

// Don't use overloaded new/delete here, bullet has them overloaded
#undef new

namespace phy {
bse::Result<CollisionMesh> CollisionMesh::load(bse::FilePath path) {
  std::vector<char> mesh_file = *bse::load_file_raw(path);

  // Assimp::Importer importer;
  // const aiScene* scene = importer.ReadFileFromMemory(&mesh_file[0], mesh_file.size(), 0);

  bse::Result<bse::ModelData> model = bse::MeshLoader::load_mesh(path);

  ASSERT(model.is_valid()) << "Failed to load collision mesh!";

  CollisionMesh cm;

  cm.m_vertices = std::move(model->mesh.positions);
  cm.m_indices = std::move(model->mesh.indices);

  // Create a matrix to rotate objects into our Z-up system
  glm::mat4 rotation =
      glm::rotate(glm::rotate(glm::mat4(1.0f), glm::pi<float>(), glm::vec3(1.0f, 0.0f, 0.0f)),
                  glm::pi<float>(), glm::vec3(0, 0, 1));

  // Rotate the vertices and invert Z axis
  for (size_t i = 0; i < cm.get_num_vertices(); i++) {
    glm::vec3* vertex = reinterpret_cast<glm::vec3*>(&(cm.m_vertices[i * 3]));

    glm::vec4 vertex4 = glm::vec4(*vertex, 1.0f);

    vertex4 = vertex4 * rotation;

    vertex->x = -vertex4.x;
    vertex->y = vertex4.y;
    vertex->z = -vertex4.z;
  }

  // This object is used by Bullet to refer to the mesh
  btIndexedMesh mesh_ref;
  mesh_ref.m_numTriangles = cm.get_num_indices() / 3;
  mesh_ref.m_numVertices = cm.get_num_vertices();
  mesh_ref.m_triangleIndexBase =
      reinterpret_cast<const unsigned char*>(cm.get_index_array().data());
  mesh_ref.m_triangleIndexStride = 3 * sizeof(unsigned);
  mesh_ref.m_vertexBase = reinterpret_cast<const unsigned char*>(cm.get_vertex_array().data());
  mesh_ref.m_vertexStride = 3 * sizeof(float);

  cm.m_mesh_data = new btTriangleIndexVertexArray;
  cm.m_mesh_data->addIndexedMesh(mesh_ref);

  return cm;
}

CollisionMesh::CollisionMesh() {}

CollisionMesh::~CollisionMesh() { destroy(); }

CollisionMesh::CollisionMesh(CollisionMesh&& other) { move_from(std::move(other)); }

CollisionMesh& CollisionMesh::operator=(CollisionMesh&& other) {
  if (&other != this) {
    destroy();
    move_from(std::move(other));
  }

  return *this;
}

const std::vector<float>& CollisionMesh::get_vertex_array() const { return m_vertices; }

const std::vector<unsigned>& CollisionMesh::get_index_array() const { return m_indices; }

unsigned CollisionMesh::get_num_vertices() const {
  return static_cast<unsigned>(m_vertices.size() / 3);
}

unsigned CollisionMesh::get_num_indices() const { return static_cast<unsigned>(m_indices.size()); }

btTriangleIndexVertexArray* CollisionMesh::get_mesh_data() const { return m_mesh_data; }

void CollisionMesh::destroy() { delete m_mesh_data; }
void CollisionMesh::move_from(CollisionMesh&& other) {
  m_vertices = std::move(other.m_vertices);

  m_indices = std::move(other.m_indices);

  m_mesh_data = other.m_mesh_data;
  other.m_mesh_data = nullptr;
}
}  // namespace phy