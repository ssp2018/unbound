#include "bse/error.hpp"
#include "bse/log.hpp"
#include "bse/result.hpp"
#include "phy/character_controller.hpp"
#include "phy/collision_shapes.hpp"
#include "phy/physics_projectile.hpp"
#include <bse/assert.hpp>
#include <phy/physics_manager.hpp>

class btIDebugDraw;

// Don't use overloaded new/delete here, bullet has them overloaded
#undef new

namespace phy {

// Callback invoked after a Bullet physics simulation tick
static void bullet_tick_callback(btDynamicsWorld* world, btScalar time_step) {
  // Cast world user info to PhysicsManager instance
  PhysicsManager* pm = static_cast<PhysicsManager*>(world->getWorldUserInfo());

  // Call pm function
  pm->handle_callback(world);
}

PhysicsManager::PhysicsManager(glm::vec3 gravity, uint32_t max_steps) {
  // Create physics world
  m_collision_configuration = std::make_unique<btDefaultCollisionConfiguration>();
  m_dispatcher = std::make_unique<btCollisionDispatcher>(m_collision_configuration.get());
  m_broadphase_interface = std::make_unique<btDbvtBroadphase>();
  m_solver = std::make_unique<btSequentialImpulseConstraintSolver>();
  m_world =
      std::make_unique<btDiscreteDynamicsWorld>(m_dispatcher.get(), m_broadphase_interface.get(),
                                                m_solver.get(), m_collision_configuration.get());

  // Needed for Bullet ghost objects
  // m_world->getBroadphase()->getOverlappingPairCache()->setinternalGhostPairCallback(
  //  new btGhostPairCallback()); // not needed?
  m_ghost_pair_callback = std::make_unique<btGhostPairCallback>();
  m_world->getPairCache()->setInternalGhostPairCallback(m_ghost_pair_callback.get());

  // Set tick callback
  m_world->setInternalTickCallback(bullet_tick_callback, static_cast<void*>(this));

  set_max_steps(max_steps);
  set_gravity(gravity);

  m_frame_bit = 0;
}

PhysicsManager::~PhysicsManager() {
  reset();
  remove_navmesh_triangle_shapes();
  m_world.reset();
  m_solver.reset();
  m_broadphase_interface.reset();
  m_dispatcher.reset();
  m_collision_configuration.reset();
  m_ghost_pair_callback.reset();
}

void PhysicsManager::set_gravity(glm::vec3 gravity) { m_world->setGravity(vc(gravity)); }
glm::vec3 PhysicsManager::get_gravity() const { return vc(m_world->getGravity()); }

void PhysicsManager::simulate(float dt) { m_world->stepSimulation(dt, m_max_steps); }

void PhysicsManager::set_max_steps(uint32_t max_steps) {
  ASSERT(max_steps > 0) << "Max number of simulation steps need to be greater than 0. Was "
                        << max_steps << ".";
  m_max_steps = max_steps;
}

uint32_t PhysicsManager::get_max_steps() const { return m_max_steps; }

void PhysicsManager::set_debug_drawer(btIDebugDraw* drawer) { m_world->setDebugDrawer(drawer); }
void PhysicsManager::debug_draw() { m_world->debugDrawWorld(); }

btDiscreteDynamicsWorld::AllHitsRayResultCallback PhysicsManager::raycast(glm::vec3 origin,
                                                                          glm::vec3 direction,
                                                                          float length) {
  btVector3 from(vc(origin));
  btVector3 to(vc(glm::normalize(direction) * length + origin));

  btDiscreteDynamicsWorld::AllHitsRayResultCallback result(from, to);
  result.m_collisionFilterGroup = CollisionGroup::DYNAMIC;
  result.m_collisionFilterMask =
      CollisionGroup::STATIC | CollisionGroup::DYNAMIC | CollisionGroup::CHARACTER_CONTROLLER;

  m_world->rayTest(from, to, result);

  return result;
}

btDiscreteDynamicsWorld::ClosestConvexResultCallback PhysicsManager::sphere_sweep(
    glm::vec3 origin, glm::vec3 direction, float length, float sphere_radius) {
  btVector3 from(vc(origin));
  btVector3 to(vc(glm::normalize(direction) * length + origin));

  btDiscreteDynamicsWorld::ClosestConvexResultCallback result(from, to);
  result.m_collisionFilterGroup = CollisionGroup::DYNAMIC;
  result.m_collisionFilterMask = CollisionGroup::STATIC | CollisionGroup::DYNAMIC;

  btTransform from_transform = btTransform::getIdentity();
  from_transform.setOrigin(from);
  btTransform to_transform = btTransform::getIdentity();
  to_transform.setOrigin(to);

  btSphereShape* sphere = new btSphereShape(sphere_radius);
  m_world->convexSweepTest(sphere, from_transform, to_transform, result);
  delete sphere;

  return result;
}

bool PhysicsManager::update_static_object(uint32_t id) {
  if (m_static_objects.count(id)) {
    m_static_objects[id].m_frame_updated = current_frame();
    return true;
  } else {
    return false;
  }
}
bool PhysicsManager::update_dynamic_object(uint32_t id) {
  if (m_dynamic_objects.count(id)) {
    m_dynamic_objects[id].m_frame_updated = current_frame();
    return true;
  } else {
    return false;
  }
}  // namespace phy
bool PhysicsManager::update_character_controller(uint32_t id) {
  if (m_character_controllers.count(id)) {
    m_character_controllers[id].set_frame_updated(current_frame());
    return true;
  } else {
    return false;
  }
}
void PhysicsManager::add_static_object(uint32_t id, glm::mat4 transform_no_scale, glm::vec3 scale,
                                       std::shared_ptr<phy::CollisionShape> shape,
                                       bool is_trigger) {
  if (m_static_objects.count(id)) {
    LOG(WARNING) << "Attempted to add a static object that already exists!\n";
  } else {
    PhysicsObject po;
    po.m_shape = shape;
    po.m_shape->get_shape()->setLocalScaling(vc(scale));
    po.m_frame_updated = current_frame();

    btTransform object_transform = mc(transform_no_scale);

    // A static object has a mass of 0.0f
    btVector3 local_inertia(0, 0, 0);
    btDefaultMotionState* motion_state = new btDefaultMotionState(object_transform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(0.0f, motion_state, po.m_shape->get_shape(),
                                                    local_inertia);
    po.m_body = std::make_unique<btRigidBody>(rbInfo);

    // Disable debug drawing
    int flags = po.m_body->getCollisionFlags();
    if (is_trigger) {
      po.m_body->setCollisionFlags(flags | btCollisionObject::CF_NO_CONTACT_RESPONSE);
    } else if (shape->get_type() == phy::CollisionShapeType::TRIANGLE_MESH) {
      po.m_body->setCollisionFlags(flags | btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT |
                                   btCollisionObject::CF_STATIC_OBJECT);
    }

    // Set user index to entity ID
    po.m_body->setUserIndex(static_cast<int>(id));

    if (!is_trigger) {
      // This object is part of collision group static and collides with dynamic, trigger, character
      // controller and projectile
      m_world->addRigidBody(po.m_body.get(), CollisionGroup::STATIC,
                            CollisionGroup::DYNAMIC | CollisionGroup::CHARACTER_CONTROLLER |
                                CollisionGroup::PROJECTILE);
    } else {
      // This object is part of collision group trigger and collides with dynamic
      m_world->addRigidBody(po.m_body.get(), CollisionGroup::TRIGGER,
                            CollisionGroup::DYNAMIC | CollisionGroup::CHARACTER_CONTROLLER |
                                CollisionGroup::PROJECTILE);
    }
    // Update AABB after scaling
    m_world->updateSingleAabb(po.m_body.get());

    m_static_objects[id] = std::move(po);
  }
}
void PhysicsManager::add_dynamic_object(uint32_t id, glm::mat4 transform,
                                        std::shared_ptr<phy::CollisionShape> shape, float mass) {
  if (m_dynamic_objects.count(id)) {
    LOG(WARNING) << "Attempted to add a static object that already exists!\n";
  } else {
    if (shape == nullptr) {
      return;
    }

    PhysicsObject po;
    po.m_shape = shape;
    po.m_shape->get_shape()->setLocalScaling(vc(get_scale_from_matrix(transform)));
    po.m_frame_updated = current_frame();

    if (po.m_shape->get_type() & CollisionShapeType::TRIANGLE_MESH) {
      ASSERT(0) << "Attempted to create a dynamic physics object with a "
                << "TriangleMeshCollisionShape. Only "
                << "static objects can use this shape.";
    }

    btTransform object_transform = mc(transform);

    // A non-dynamic object has a mass of 0.0f
    btVector3 local_inertia(0, 0, 0);
    po.m_shape->get_shape()->calculateLocalInertia(mass, local_inertia);

    btDefaultMotionState* motion_state = new btDefaultMotionState(object_transform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, motion_state, po.m_shape->get_shape(),
                                                    local_inertia);
    po.m_body = std::make_unique<btRigidBody>(rbInfo);

    // Set user index to entity ID
    po.m_body->setUserIndex(static_cast<int>(id));

    float fric = po.m_body->getFriction();
    po.m_body->setFriction(4.0f);
    // This object is part of collision group dynamic and collides with static, dynamic and trigger
    m_world->addRigidBody(po.m_body.get(), CollisionGroup::DYNAMIC,
                          CollisionGroup::STATIC | CollisionGroup::DYNAMIC |
                              CollisionGroup::TRIGGER | CollisionGroup::CHARACTER_CONTROLLER |
                              CollisionGroup::PROJECTILE);

    // Update AABB after scaling
    m_world->updateSingleAabb(po.m_body.get());
    m_dynamic_objects[id] = std::move(po);
  }
}

void PhysicsManager::add_character_controller(uint32_t id,                  //
                                              glm::vec3 position,           //
                                              float radius,                 //
                                              float height,                 //
                                              float turning_speed,          //
                                              float angle,                  //
                                              bool face_moving_direction,   //
                                              float mass,                   //
                                              glm::vec3 gravity,            //
                                              glm::vec3 velocity,           //
                                              bool navmesh_collision,       //
                                              bool is_flying,               //
                                              glm::vec3 up,                 //
                                              glm::quat desired_direction,  //
                                              glm::quat facing_direction,   //
                                              bool on_ground) {
  if (glm::length(up) < 0.01f) {
    up = {0, 0, 1};
  }

  const float terminal_velocity = 90.0f;

  // Emplace in unordered map, 10/10 syntax
  m_character_controllers.emplace(std::piecewise_construct, std::forward_as_tuple(id),
                                  std::forward_as_tuple(radius,                 //
                                                        height,                 //
                                                        position,               //
                                                        up,                     //
                                                        gravity,                //
                                                        terminal_velocity,      //
                                                        turning_speed,          //
                                                        angle,                  //
                                                        face_moving_direction,  //
                                                        static_cast<int>(id),   //
                                                        m_world.get(),          //
                                                        mass,                   //
                                                        navmesh_collision,      //
                                                        is_flying,              //
                                                        desired_direction,      //
                                                        facing_direction,       //
                                                        on_ground,              //
                                                        velocity));

  // Set the updated frame
  m_character_controllers[id].set_frame_updated(current_frame());
}

glm::quat PhysicsManager::get_character_controller_desired_direction(uint32_t id) {
  return m_character_controllers.at(id).get_desired_direction();
}

glm::quat PhysicsManager::get_character_controller_current_direction(uint32_t id) {
  return m_character_controllers.at(id).get_current_direction();
}

void PhysicsManager::remove_stale_static_objects() {
  // Loop through objects and remove ones that have not been updated this frame
  auto it = m_static_objects.begin();
  while (it != m_static_objects.end()) {
    auto& [id, static_object] = *it;
    if (static_object.m_frame_updated != current_frame()) {
      m_world->removeRigidBody(static_object.m_body.get());
      delete static_object.m_body.get()->getMotionState();
      it = m_static_objects.erase(it);
    } else {
      it++;
    }
  }
}

void PhysicsManager::remove_stale_dynamic_objects() {
  // Loop through objects and remove ones that have not been updated this frame
  auto it = m_dynamic_objects.begin();
  while (it != m_dynamic_objects.end()) {
    auto& [id, dynamic_object] = *it;
    if (dynamic_object.m_frame_updated != current_frame()) {
      m_world->removeRigidBody(dynamic_object.m_body.get());
      delete dynamic_object.m_body.get()->getMotionState();
      it = m_dynamic_objects.erase(it);
    } else {
      it++;
    }
  }
}

void PhysicsManager::remove_stale_character_controllers() {
  // Loop through objects and remove ones that have not been updated this frame
  auto it = m_character_controllers.begin();
  while (it != m_character_controllers.end()) {
    auto& [id, character_controller] = *it;
    if (character_controller.get_frame_updated() != current_frame()) {
      character_controller.destroy(m_world.get());
      it = m_character_controllers.erase(it);
    } else {
      it++;
    }
  }
}

bse::Result<glm::mat4> PhysicsManager::get_static_transform(uint32_t id) {
  if (m_static_objects.count(id)) {
    btTransform t = m_static_objects[id].m_body->getWorldTransform();
    return mc(t);
  } else {
    LOG(WARNING) << "Attempted to retrieve the transform of a static object that does not exist!\n";
    return bse::Error() << "Object does not exist\n";
  }
}

bse::Result<glm::mat4> PhysicsManager::get_dynamic_transform(uint32_t id) {
  if (m_dynamic_objects.count(id)) {
    btTransform t;
    m_dynamic_objects[id].m_body->getMotionState()->getWorldTransform(t);
    return mc(t);
  } else {
    LOG(WARNING)
        << "Attempted to retrieve the transform of a dynamic object that does not exist!\n";
    return bse::Error() << "Object does not exist\n";
  }
}

bse::Result<glm::mat4> PhysicsManager::get_character_controller_transform(uint32_t id) {
  if (m_character_controllers.count(id)) {
    return m_character_controllers[id].get_transform();
  } else {
    LOG(WARNING)
        << "Attempted to retrieve the transform of a character controller that does not exist!\n";
    return bse::Error() << "Object does not exist\n";
  }
}

void PhysicsManager::set_static_transform(uint32_t id, const glm::mat4& transform_no_scale,
                                          glm::vec3 scale) {
  if (m_static_objects.count(id)) {
    // Both of these are needed for some reason
    m_static_objects[id].m_body->getMotionState()->setWorldTransform(mc(transform_no_scale));
    m_static_objects[id].m_body->setWorldTransform(mc(transform_no_scale));
    m_static_objects[id].m_shape->get_shape()->setLocalScaling(vc(scale));
    // Update AABB after scaling
    m_world->updateSingleAabb(m_static_objects[id].m_body.get());
  } else {
    LOG(WARNING) << "Attempted to set the transform of a static object that does not exist!\n";
  }
}

void PhysicsManager::set_dynamic_transform(uint32_t id, const glm::mat4& transform) {
  if (m_dynamic_objects.count(id)) {
    // Both of these are needed for some reason
    m_dynamic_objects[id].m_body->getMotionState()->setWorldTransform(mc(transform));
    m_dynamic_objects[id].m_body->setWorldTransform(mc(transform));
    m_dynamic_objects[id].m_shape->get_shape()->setLocalScaling(
        vc(get_scale_from_matrix(transform)));
    // Update AABB after scaling
    m_world->updateSingleAabb(m_dynamic_objects[id].m_body.get());
  } else {
    LOG(WARNING) << "Attempted to set the transform of a dynamic object that does not exist!\n";
  }
}

void PhysicsManager::set_character_controller_transform(uint32_t id, const glm::mat4& transform) {
  if (m_character_controllers.count(id)) {
    m_character_controllers[id].set_transform(transform);
  } else {
    LOG(WARNING)
        << "Attempted to set the transform of a character controller that does not exist!\n";
  }
}

void PhysicsManager::set_collision_shape(uint32_t id,
                                         std::unique_ptr<CollisionShape>&& new_collision_shape) {
  if (m_static_objects.count(id)) {
    m_static_objects[id].m_shape = std::move(new_collision_shape);
  } else if (m_dynamic_objects.count(id)) {
    m_dynamic_objects[id].m_shape = std::move(new_collision_shape);
  } else {
    LOG(WARNING) << "Attempted to set collision shape to a physics object that does not exist!\n";
  }
}

unsigned char PhysicsManager::current_frame() const { return m_frame_bit; }
unsigned char PhysicsManager::next_frame() const {
  // Could be return !m_frame_bit, but this is probably safer
  return (m_frame_bit + 1) % 2;
}
void PhysicsManager::swap_frame() { m_frame_bit = next_frame(); }

void PhysicsManager::activate_static_object(uint32_t id) {
  m_static_objects[id].m_body->activate();
}

void PhysicsManager::activate_dynamic_object(uint32_t id) {
  if (m_dynamic_objects.count(id)) {
    m_dynamic_objects[id].m_body->activate();
  }
}

void PhysicsManager::activate_character_controller(uint32_t id) {
  m_character_controllers[id].activate();
}

glm::vec3 PhysicsManager::get_character_controller_velocity(uint32_t id) {
  return m_character_controllers[id].get_velocity();
}

bool PhysicsManager::get_character_controller_on_ground(uint32_t id) {
  return m_character_controllers[id].get_on_ground();
}

void PhysicsManager::update_static_object_properties(uint32_t id,
                                                     std::shared_ptr<phy::CollisionShape> shape,
                                                     bool is_trigger) {
  if (m_static_objects.count(id)) {
    PhysicsObject& po = m_static_objects[id];

    // Temporarily remove object from world
    m_world->removeRigidBody(po.m_body.get());

    po.m_shape = shape;
    po.m_body->setCollisionShape(po.m_shape->get_shape());

    int flags = po.m_body->getCollisionFlags();
    if (is_trigger) {
      po.m_body->setCollisionFlags(flags | btCollisionObject::CF_NO_CONTACT_RESPONSE);
    } else {
      po.m_body->setCollisionFlags(flags & ~btCollisionObject::CF_NO_CONTACT_RESPONSE);
    }

    if (!is_trigger) {
      m_world->addRigidBody(po.m_body.get(), CollisionGroup::STATIC,
                            CollisionGroup::DYNAMIC | CollisionGroup::TRIGGER |
                                CollisionGroup::CHARACTER_CONTROLLER | CollisionGroup::PROJECTILE);
    } else {
      m_world->addRigidBody(po.m_body.get(), CollisionGroup::TRIGGER,
                            CollisionGroup::DYNAMIC | CollisionGroup::CHARACTER_CONTROLLER |
                                CollisionGroup::PROJECTILE);
    }
  }
}

void PhysicsManager::update_dynamic_object_properties(uint32_t id,
                                                      std::shared_ptr<phy::CollisionShape> shape,
                                                      float mass) {
  if (!m_dynamic_objects.count(id)) {
    return;
  }

  PhysicsObject& po = m_dynamic_objects[id];

  // Temporarily remove object from world
  m_world->removeRigidBody(po.m_body.get());

  po.m_shape = shape;
  po.m_body->setCollisionShape(po.m_shape->get_shape());

  btVector3 local_inertia(0, 0, 0);
  po.m_shape->get_shape()->calculateLocalInertia(mass, local_inertia);
  po.m_body->setMassProps(mass, local_inertia);

  po.m_body->updateInertiaTensor();

  m_world->addRigidBody(po.m_body.get(), CollisionGroup::DYNAMIC,
                        CollisionGroup::STATIC | CollisionGroup::DYNAMIC | CollisionGroup::TRIGGER |
                            CollisionGroup::CHARACTER_CONTROLLER | CollisionGroup::PROJECTILE);
}

void PhysicsManager::update_character_controller_properties(
    uint32_t id, float mass, float radius, float height, glm::vec3 gravity, float turning_speed,
    float angle, bool face_moving_direction, bool navmesh_collision, bool is_flying, glm::vec3 up) {
  if (glm::length(up) < 0.01f) {
    up = {0, 0, 1};
  }

  m_character_controllers[id].update_properties(radius, height, up, gravity, turning_speed, angle,
                                                face_moving_direction, navmesh_collision,
                                                is_flying);
}

void PhysicsManager::character_controller_move(uint32_t id, glm::vec3 move_vector, float dt) {
  m_character_controllers[id].move(move_vector, m_world.get(), dt);
}

void PhysicsManager::character_controller_jump(uint32_t id, float jump_strength) {
  m_character_controllers[id].jump(jump_strength);
}

void PhysicsManager::dynamic_object_add_force(uint32_t id, glm::vec3 force) {
  if (m_dynamic_objects.count(id)) {
    m_dynamic_objects[id].m_body->applyCentralForce(vc(force));
  } else {
    LOG(WARNING) << "Attempted to add force a physics object that does not exist!\n";
  }
}
void PhysicsManager::dynamic_object_add_impulse(uint32_t id, glm::vec3 impulse) {
  if (m_dynamic_objects.count(id)) {
    m_dynamic_objects[id].m_body->applyCentralImpulse(vc(impulse));
  } else {
    LOG(WARNING) << "Attempted to add force a physics object that does not exist!\n";
  }
}

void PhysicsManager::handle_callback(btDynamicsWorld* world) {
  int num_manifolds = world->getDispatcher()->getNumManifolds();

  // Loop through all contacts
  for (int i = 0; i < num_manifolds; i++) {
    btPersistentManifold* contact_manifold = world->getDispatcher()->getManifoldByIndexInternal(i);

    const btCollisionObject* ob1 = contact_manifold->getBody0();
    const btCollisionObject* ob2 = contact_manifold->getBody1();

    uint32_t id1 = ob1->getUserIndex();
    uint32_t id2 = ob2->getUserIndex();

    // The lower key is used as a key to the m_contacts map, so swap ids if id2 is lower
    if (id2 < id1) std::swap(id2, id1);

    // Check if this contact is new
    bool new_contact = false;
    if (m_contacts.count(id1)) {
      // Don't add if the frame index of the saved object is up to date. This can happen when
      // multiple simulation ticks happen in one frame
      if (!m_contacts[id1].count(id2)) {
        new_contact = true;
      }
    } else {
      new_contact = true;
    }

    // Only add or update if there is contact
    if (contact_manifold->getNumContacts() > 0) {
      if (new_contact) {
        add_new_collision(contact_manifold, id1, id2);
      } else if (m_contacts[id1].count(id2)) {
        m_contacts[id1][id2] = current_frame();
      }
    }
  }

  // Remove stale contacts
  for (auto& c : m_contacts) {
    auto& [id1, contact_map] = c;

    auto it = contact_map.begin();

    while (it != contact_map.end()) {
      auto& [id2, frame_bit] = *it;

      if (frame_bit != current_frame()) {
        it = contact_map.erase(it);
        // send exit callback
        add_new_collision_ended(id1, id2);
      } else {
        it++;
      }
    }
  }
}

const std::vector<PhysicsManager::Collision>& PhysicsManager::get_new_collisions() const {
  return m_new_collisions;
}

const std::vector<PhysicsManager::Collision>& PhysicsManager::get_ended_collisions() const {
  return m_ended_collisions;
}

void PhysicsManager::clear_collisions() {
  m_new_collisions.clear();
  m_ended_collisions.clear();
}

void PhysicsManager::add_new_collision(btPersistentManifold* manifold, uint32_t id1, uint32_t id2) {
  btVector3 average_position = btVector3(0, 0, 0);
  btVector3 average_normal = btVector3(0, 0, 0);

  int num_contacts = manifold->getNumContacts();
  for (int j = 0; j < num_contacts; j++) {
    btManifoldPoint& pt = manifold->getContactPoint(j);
    if (pt.getDistance() < 0.f) {
      average_position += (pt.getPositionWorldOnA() + pt.getPositionWorldOnB()) * 0.5f;
      average_normal += pt.m_normalWorldOnB;
    }
  }

  average_position /= (float)num_contacts;

  if (average_normal.length() > 0.01f) average_normal.normalize();

  Collision new_collision;
  new_collision.object1 = id1;
  new_collision.object2 = id2;
  new_collision.collision_point = vc(average_position);
  new_collision.collision_normal = vc(average_normal);

  m_new_collisions.push_back(new_collision);

  // Add to map
  m_contacts[id1][id2] = current_frame();
}

void PhysicsManager::add_new_collision_ended(uint32_t id1, uint32_t id2) {
  Collision ended_collision;
  ended_collision.object1 = id1;
  ended_collision.object2 = id2;
  m_ended_collisions.push_back(ended_collision);
}

void PhysicsManager::character_controller_dash(uint32_t id, glm::vec3 dash_velocity,
                                               float duration) {
  m_character_controllers[id].dash(dash_velocity, duration);
}

PhysicsManager::AABB PhysicsManager::get_static_object_aabb(uint32_t id) {
  PhysicsManager::AABB aabb;
  btVector3 min;
  btVector3 max;

  m_static_objects[id].m_body->getAabb(min, max);

  aabb.min = vc(min);
  aabb.max = vc(max);

  return aabb;
}

PhysicsManager::AABB PhysicsManager::get_dynamic_object_aabb(uint32_t id) {
  if (!m_dynamic_objects.count(id)) {
    return {{0, 0, 0}, {0, 0, 0}};
  }

  PhysicsManager::AABB aabb;
  btVector3 min;
  btVector3 max;

  m_dynamic_objects[id].m_body->getAabb(min, max);

  aabb.min = vc(min);
  aabb.max = vc(max);

  return aabb;
}

PhysicsManager::AABB PhysicsManager::get_character_controller_aabb(uint32_t id) {
  PhysicsManager::AABB aabb;
  btVector3 min;
  btVector3 max;

  m_character_controllers[id].get_aabb(min, max);

  aabb.min = vc(min);
  aabb.max = vc(max);

  return aabb;
}

glm::vec3 PhysicsManager::get_dynamic_object_velocity(uint32_t id) {
  if (!m_dynamic_objects.count(id)) {
    return {0.f, 0.f, 0.f};
  }

  btVector3 center(0, 0, 0);
  return vc(m_dynamic_objects[id].m_body->getVelocityInLocalPoint(center));
}

glm::vec3 PhysicsManager::get_scale_from_matrix(const glm::mat4& matrix) const {
  // Unused ------------
  glm::quat orientation;
  glm::vec3 position;
  glm::vec3 skew;
  glm::vec4 persp;
  // -------------------

  glm::vec3 scale;

  glm::decompose(matrix, scale, orientation, position, skew, persp);

  return scale;
}

glm::vec3 PhysicsManager::get_static_scale(uint32_t id) {
  return vc(m_static_objects[id].m_shape->get_shape()->getLocalScaling());
}

glm::vec3 PhysicsManager::get_dynamic_scale(uint32_t id) {
  if (!m_dynamic_objects.count(id)) {
    return {1.f, 1.f, 1.f};
  }

  return vc(m_dynamic_objects[id].m_shape->get_shape()->getLocalScaling());
}

void PhysicsManager::character_controller_end_swing(uint32_t id) {
  m_character_controllers[id].end_swing();
}

void PhysicsManager::character_controller_start_swing(uint32_t id, glm::vec3 swing_center) {
  m_character_controllers[id].start_swing(swing_center);
}

void PhysicsManager::character_controller_set_rope_length(uint32_t id, float length) {
  m_character_controllers[id].set_rope_length(length);
}

float PhysicsManager::character_controller_get_rope_length(uint32_t id) {
  return m_character_controllers[id].get_rope_length();
}

void PhysicsManager::character_controller_add_swing_velocity(uint32_t id, glm::vec3 velocity) {
  m_character_controllers[id].add_swing_velocity(velocity);
}

bool PhysicsManager::update_projectile(uint32_t id) {
  if (m_projectiles.count(id)) {
    m_projectiles[id].set_frame_updated(current_frame());
    return true;
  } else {
    return false;
  }
}

void PhysicsManager::add_projectile(uint32_t id, glm::vec3 position, float radius, float length,
                                    glm::vec3 velocity, glm::vec3 gravity, float mass) {
  // Emplace in unordered map, 10/10 syntax
  m_projectiles.emplace(
      std::piecewise_construct, std::forward_as_tuple(id),
      std::forward_as_tuple(id, position, radius, length, velocity, gravity, mass, m_world.get()));

  // Set the updated frame
  m_projectiles[id].set_frame_updated(current_frame());
}

void PhysicsManager::set_projectile_position(uint32_t id, const glm::vec3 position) {
  if (m_projectiles.count(id)) {
    m_projectiles[id].set_position(position);
  } else {
    LOG(WARNING) << "Attempted to set the position of a projectile that does not exist!\n";
  }
}

void PhysicsManager::update_projectile_properties(uint32_t id, float radius, float length,
                                                  glm::vec3 velocity, glm::vec3 gravity,
                                                  float mass) {
  m_projectiles[id].update_properties(radius, length, velocity, gravity, mass);
}

void PhysicsManager::remove_stale_projectiles() {
  // Loop through objects and remove ones that have not been updated this frame
  auto it = m_projectiles.begin();
  while (it != m_projectiles.end()) {
    auto& [id, projectile] = *it;
    if (projectile.get_frame_updated() != current_frame()) {
      projectile.destroy(m_world.get());
      it = m_projectiles.erase(it);

    } else {
      it++;
    }
  }
}

bse::Result<glm::mat4> PhysicsManager::get_projectile_transform(uint32_t id) {
  if (m_projectiles.count(id)) {
    return m_projectiles[id].get_transform();
  } else {
    LOG(WARNING) << "Attempted to retrieve the transform of a projectile that does not exist!\n";
    return bse::Error() << "Object does not exist\n";
  }
}

void PhysicsManager::projectile_simulate(uint32_t id, float dt) {
  m_projectiles[id].simulate(dt, m_world.get());
}

PhysicsManager::AABB PhysicsManager::get_projectile_aabb(uint32_t id) {
  PhysicsManager::AABB aabb;
  btVector3 min;
  btVector3 max;

  m_projectiles[id].get_aabb(min, max);

  aabb.min = vc(min);
  aabb.max = vc(max);

  return aabb;
}

glm::vec3 PhysicsManager::get_projectile_velocity(uint32_t id) {
  return m_projectiles[id].get_velocity();
}

void PhysicsManager::projectile_set_collision_enabled(uint32_t id, bool reporting_enabled,
                                                      bool destruction_enabled,
                                                      bool collide_with_animated) {
  m_projectiles[id].set_collision_properties(reporting_enabled, destruction_enabled,
                                             collide_with_animated, m_world.get());
}

// Struct handling triangle callbacks from Bullet
struct TriangleCallback : public btTriangleCallback {
  TriangleCallback(std::vector<btTriangleShape>& triangles) : triangles(triangles) {}
  virtual ~TriangleCallback() = default;
  virtual void processTriangle(btVector3* triangle, int partId, int triangleIndex) override {
    triangles.push_back(btTriangleShape(triangle[0], triangle[1], triangle[2]));

    // Set user index to the triangle index, allowing us to easily obtain it after collision
    // checking
    triangles[triangles.size() - 1].setUserIndex(triangleIndex);
  }

  std::vector<btTriangleShape>& triangles;
};

PhysicsManager::ProjectileTriangleCollision PhysicsManager::projectile_animated_mesh_collision_test(
    uint32_t projectile_id, btBvhTriangleMeshShape* animated_shape,
    const glm::mat4& animated_transform, CollisionGroup::Group projectile_group,
    CollisionGroup::Group animated_group) {
  bool perform_collision_test = false;

  // Only perform collision test if projectile's group is set to none (default) or if the mesh and
  // projectile are in the same groups
  if (projectile_group == CollisionGroup::NONE || (projectile_group & animated_group)) {
    perform_collision_test = true;
  }

  if (!perform_collision_test || !m_projectiles.count(projectile_id)) {
    PhysicsManager::ProjectileTriangleCollision hit_triangle;
    hit_triangle.hit = false;
    hit_triangle.face_index = 0;
    return hit_triangle;
  }

  PhysicsProjectile& projectile = m_projectiles[projectile_id];
  btTransform projectile_transform = mc(projectile.get_transform());
  btTransform projectile_last_transform = mc(projectile.get_last_transform());

  // Transform position to be relative to animated_transform
  btVector3 projectile_pos =
      vc(glm::inverse(animated_transform) * mc(projectile_transform) * glm::vec4(0, 0, 0, 1));
  btVector3 projectile_last_pos =
      vc(glm::inverse(animated_transform) * mc(projectile_last_transform) * glm::vec4(0, 0, 0, 1));

  // AABB at origin, but rotated to fit projectile's rotation
  btVector3 projectile_aabb_min = btVector3(0, 0, 0);
  btVector3 projectile_aabb_max = btVector3(0, 0, 0);
  btTransform rotated_identity = projectile_transform;
  rotated_identity.setOrigin(btVector3(0, 0, 0));
  projectile.get_aabb_relative(rotated_identity, projectile_aabb_min, projectile_aabb_max);

  // Create object handling triangle callbacks
  std::vector<btTriangleShape> triangles;
  TriangleCallback callback(triangles);

  // m_world->debugDrawObject(mc(animated_transform), animated_shape, btVector3(1.0f, 0.5f, 0.5f));

  // Perform collision test between projectile AABB and triangle mesh. Any collisions will be sent
  // to 'callback'
  // Only perform AABB sweep if distance is not zero
  // Transform AABB to be relative to the animated mesh position, because the mesh shape does not
  // know about its transform
  if (!(projectile_last_transform.getOrigin() - projectile_transform.getOrigin()).fuzzyZero()) {
    animated_shape->performConvexcast(&callback, projectile_last_pos, projectile_pos,
                                      projectile_aabb_min, projectile_aabb_max);
  } else {
    animated_shape->processAllTriangles(&callback, projectile_aabb_min + projectile_pos,
                                        projectile_aabb_max + projectile_pos);
  }

  // Now test hit triangles against projectile shape
  btCapsuleShape* projectile_shape = projectile.get_shape();

  PhysicsManager::ProjectileTriangleCollision hit_triangle;

  if (triangles.size()) {
    // Value for triangles' collision group
    const int triangle_collision_group = 1 << 20;

    // Vector of temporary collision objects
    std::vector<btCollisionObject> cos;

    // Need to resize, or vector expansion will change addresses (bad timmie does bad programming)
    cos.resize(triangles.size());

    // Create temporary collision objects and add them to physics world
    for (unsigned i = 0; i < triangles.size(); i++) {
      cos[i].setCollisionShape(&triangles[i]);
      cos[i].setWorldTransform(btTransform::getIdentity());
      m_world->addCollisionObject(&cos[i], triangle_collision_group, triangle_collision_group);
    }

    if (!(projectile_last_transform.getOrigin() - projectile_transform.getOrigin()).fuzzyZero()) {
      // Perform a sweep test to check for collisions between triangles and projectile
      btCollisionWorld::ClosestConvexResultCallback ccrc(projectile_last_pos, projectile_pos);
      ccrc.m_collisionFilterGroup = triangle_collision_group;
      ccrc.m_collisionFilterMask = triangle_collision_group;
      m_world->convexSweepTest(
          projectile_shape, mc(glm::inverse(animated_transform) * mc(projectile_last_transform)),
          mc(glm::inverse(animated_transform) * mc(projectile_transform)), ccrc);

      if (ccrc.hasHit()) {
        hit_triangle.hit = true;
        hit_triangle.face_index = ccrc.m_hitCollisionObject->getCollisionShape()->getUserIndex();

        const btTriangleShape* triangle =
            dynamic_cast<const btTriangleShape*>(ccrc.m_hitCollisionObject->getCollisionShape());

        // Calculate the variables needed to place the projectile properly
        btVector3 projectile_position_at_collision;
        projectile_position_at_collision.setInterpolate3(projectile_last_pos, projectile_pos,
                                                         ccrc.m_closestHitFraction);

        btVector3 v0 = triangle->m_vertices1[1] - triangle->m_vertices1[0];
        btVector3 v1 = triangle->m_vertices1[2] - triangle->m_vertices1[0];

        // Project the projectile position onto triangle plane
        btVector3 tn = btCross(v0, v1).normalized();
        btVector3 diff = triangle->m_vertices1[0] - projectile_position_at_collision;
        float plane_dist = btDot(diff, tn);
        hit_triangle.distance_from_plane = plane_dist;

        btVector3 projected_pos = projectile_position_at_collision + tn * plane_dist;

        btVector3 u = triangle->m_vertices1[1] - triangle->m_vertices1[0];
        btVector3 v = triangle->m_vertices1[2] - triangle->m_vertices1[0];
        btVector3 n = btCross(u, v);

        btVector3 w = projected_pos - triangle->m_vertices1[0];

        hit_triangle.barycentric.x = (btDot(btCross(u, w), n) / btDot(n, n));
        hit_triangle.barycentric.y = (btDot(btCross(w, v), n) / btDot(n, n));
        hit_triangle.barycentric.z = 1.0f - hit_triangle.barycentric.x - hit_triangle.barycentric.y;

        glm::vec3 normal = vc(n.normalized());
        // From vertex 2 to middle of edge between vertex 0 and 1
        glm::vec3 tri_forward =
            vc(((triangle->m_vertices1[0] + v0 * 0.5f) - triangle->m_vertices1[2]).normalized());

        // Matrix for converting into triangle space
        glm::mat4 triangle_space;
        triangle_space[0] = glm::vec4(tri_forward, 0.0f);
        triangle_space[1] = glm::vec4(normal, 0.0f);
        triangle_space[2] = glm::vec4(glm::cross(tri_forward, normal), 0.0f);
        triangle_space[3] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);

        glm::vec3 tri_space_normal =
            glm::vec3(glm::inverse(triangle_space) * glm::vec4(normal, 0.0f));
        glm::vec3 tri_space_forward = glm::normalize(
            glm::vec3(glm::inverse(triangle_space) * glm::inverse(animated_transform) *
                      mc(projectile_transform) * glm::vec4(0, 1, 0, 0)));

        // Rotation is defined as the rotation from the triangle normal in triangle space to the
        // projectile forward direction in triangle space
        hit_triangle.rotation = glm::rotation(tri_space_normal, tri_space_forward);
      } else {
        hit_triangle.hit = false;
      }
    } else {
      hit_triangle.hit = false;
    }

    // Remove temporary objects from world
    for (unsigned i = 0; i < cos.size(); i++) {
      m_world->removeCollisionObject(&cos[i]);
    }
  } else {
    hit_triangle.hit = false;
  }

  return hit_triangle;
}

void PhysicsManager::reset() const {
  // Remove static objects
  {
    auto it = m_static_objects.begin();
    while (it != m_static_objects.end()) {
      auto& [id, static_object] = *it;
      m_world->removeRigidBody(static_object.m_body.get());
      if (static_object.m_body.get()->getMotionState() != nullptr) {
        delete static_object.m_body.get()->getMotionState();
      }
      it = m_static_objects.erase(it);
    }
  }

  // Remove dynamic objects
  {
    auto it = m_dynamic_objects.begin();
    while (it != m_dynamic_objects.end()) {
      auto& [id, dynamic_object] = *it;
      m_world->removeRigidBody(dynamic_object.m_body.get());
      if (dynamic_object.m_body.get()->getMotionState() != nullptr) {
        delete dynamic_object.m_body.get()->getMotionState();
      }
      it = m_dynamic_objects.erase(it);
    }
  }

  // Remove character controllers
  {
    auto it = m_character_controllers.begin();
    while (it != m_character_controllers.end()) {
      auto& [id, character_controller] = *it;
      character_controller.destroy(m_world.get());
      it = m_character_controllers.erase(it);
    }
  }

  {
    // Remove projectiles
    auto it = m_projectiles.begin();
    while (it != m_projectiles.end()) {
      auto& [id, projectile] = *it;
      projectile.destroy(m_world.get());
      it = m_projectiles.erase(it);
    }
  }

  m_static_objects.clear();
  m_dynamic_objects.clear();
  m_character_controllers.clear();
  m_projectiles.clear();
  m_contacts.clear();
  m_new_collisions.clear();

  m_broadphase_interface->resetPool(m_dispatcher.get());
  m_solver->reset();
}

bool PhysicsManager::character_controller_get_is_swinging(uint32_t id) {
  return m_character_controllers[id].get_is_swinging();
}

void PhysicsManager::add_navmesh_triangle_shapes(
    const std::vector<std::unique_ptr<btBvhTriangleMeshShape>>& shapes) {
  // Create rigid body for every shape
  for (auto& shape : shapes) {
    btTransform object_transform = btTransform::getIdentity();

    // A static object has a mass of 0.0f
    btVector3 local_inertia(0, 0, 0);
    btDefaultMotionState* motion_state = new btDefaultMotionState(object_transform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(0.0f, motion_state, shape.get(), local_inertia);

    std::unique_ptr<btRigidBody> body = std::make_unique<btRigidBody>(rbInfo);

    // Disable debug drawing
    int flags = body->getCollisionFlags();
    // body->setCollisionFlags(flags | btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT);

    // This object is part of collision group static and collides with dynamic, trigger, character
    // controller and projectile
    m_world->addRigidBody(body.get(), CollisionGroup::NAVMESH, CollisionGroup::NAVMESH);

    // Update AABB after scaling
    m_world->updateSingleAabb(body.get());

    m_navmesh_bodies.push_back(std::move(body));
  }
}

void PhysicsManager::remove_navmesh_triangle_shapes() {
  for (auto& body : m_navmesh_bodies) {
    if (body->getMotionState()) {
      delete body->getMotionState();
    }

    m_world->removeRigidBody(body.get());
  }

  m_navmesh_bodies.clear();
}

}  // namespace phy
