#include "phy/collision_shapes.hpp"
#include <phy/character_controller.hpp>
#include <phy/physics_manager.hpp>

class btConvexShape;

bool check_nan(btVector3 v) {
  if (v.x() != v.x() || v.y() != v.y() || v.z() != v.z()) {
    return true;
  }
  return false;
}

bool check_nan(glm::vec3 v) {
  if (v.x != v.x || v.y != v.y || v.z != v.z) {
    return true;
  }
  return false;
}

namespace phy {
CharacterController::CharacterController(float radius,                    //
                                         float height,                    //
                                         glm::vec3 position,              //
                                         glm::vec3 up_direction,          //
                                         glm::vec3 gravity,               //
                                         float terminal_velocity,         //
                                         float turning_speed,             //
                                         float angle,                     //
                                         bool face_moving_direction,      //
                                         int id,                          //
                                         btDiscreteDynamicsWorld* world,  //
                                         float mass,                      //
                                         bool navmesh_collision,          //
                                         bool is_flying,                  //
                                         glm::quat desired_direction,     //
                                         glm::quat facing_direction,      //
                                         bool on_ground,                  //
                                         glm::vec3 velocity) {
  m_ghost_object = std::make_unique<btPairCachingGhostObject>();

  create_shape(radius, height);

  m_facing_direction = qc(facing_direction);
  m_desired_direction = qc(desired_direction);

  m_dash_velocity = btVector3(0, 0, 0);
  m_dash_duration = 0.0f;

  m_swing_center = btVector3(0, 0, 0);

  m_mass = mass;

  m_dash_stop_velocity = 0.3f;

  // These should be the same, or up/down velocity will be affected. Fix?
  m_up_step = 6.0f;
  m_down_step = 6.0f;

  m_swing_bounce_friction = 0.3f;
  m_rope_friction = 0.007f;
  m_max_slope = 0.7f;
  m_adjustment_length = 0.01f;
  m_sweep_iterations = 20;
  m_in_air_threshold = 6;
  m_is_swinging = false;
  m_last_dt = 1.0f / 60.0f;

  btTransform start;
  start.setIdentity();
  start.setOrigin(vc(position));
  m_ghost_object->setWorldTransform(start);

  // m_ghost_object->setCollisionFlags(btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT);

  m_ghost_object->setUserIndex(id);

  set_properties(up_direction, gravity, turning_speed, angle, face_moving_direction,
                 navmesh_collision, is_flying);

  m_terminal_velocity = terminal_velocity;

  world->addCollisionObject(
      m_ghost_object.get(), m_use_navmesh_collision ? m_navmesh_group : m_normal_group,
      ~CollisionGroup::STATIC & (m_use_navmesh_collision ? m_navmesh_mask : m_normal_mask));

  m_vertical_velocity = -velocity.z;
  m_in_air = on_ground ? 0 : m_in_air_threshold;
  m_pos = vc(position);
  m_last_pos = vc(position);
}

CharacterController::~CharacterController() {}

void CharacterController::update_properties(float radius, float height, glm::vec3 up,
                                            glm::vec3 gravity, float turning_speed, float angle,
                                            bool face_moving_direction, bool navmesh_collision,
                                            bool is_flying) {
  create_shape(radius, height);
  set_properties(up, gravity, turning_speed, angle, face_moving_direction, navmesh_collision,
                 is_flying);
}

void CharacterController::set_properties(glm::vec3 up, glm::vec3 gravity, float turning_speed,
                                         float angle, bool face_moving_direction,
                                         bool navmesh_collision, bool is_flying) {
  m_ghost_object->setCollisionShape(m_shape->get_shape());

  if (up == glm::vec3(0, 0, 0)) {
    up = glm::vec3(0, 0, 1);
  }

  m_up = vc(glm::normalize(up));
  m_gravity = vc(gravity);
  m_turning_speed = turning_speed;
  m_facing_angle = angle;
  m_face_moving_direction = face_moving_direction;
  m_use_navmesh_collision = navmesh_collision;
  m_is_flying = is_flying;

  btBroadphaseProxy* proxy = m_ghost_object->getBroadphaseHandle();
  if (proxy) {
    if (m_use_navmesh_collision) {
      proxy->m_collisionFilterGroup = m_navmesh_group;
      proxy->m_collisionFilterMask = m_navmesh_mask;
    } else {
      proxy->m_collisionFilterGroup = m_normal_group;
      proxy->m_collisionFilterMask = m_normal_mask;
    }
  }
}

glm::quat CharacterController::get_desired_direction() { return qc(m_desired_direction); }

glm::quat CharacterController::get_current_direction() { return qc(m_facing_direction); }

void CharacterController::move(glm::vec3 move_vector, btDiscreteDynamicsWorld* world, float dt) {
  m_last_dt = dt;

  btVector3 normalized_gravity = m_gravity;

  if (!normalized_gravity.fuzzyZero()) {
    normalized_gravity.normalize();
  }

  // Temporarily change filter group to prevent self-collision
  btBroadphaseProxy* proxy = m_ghost_object->getBroadphaseHandle();
  proxy->m_collisionFilterGroup = CollisionGroup::NONE;
  if (!get_is_swinging()) {
    // Make move_vector horizontal
    btVector3 move_v = vc(move_vector);
    if (!m_is_flying && !dashing()) {
      move_v -= -normalized_gravity * btDot(-normalized_gravity, move_v);
    }

    if (dashing()) {
      // Override previous move vector with dash vector
      move_v = m_dash_velocity * dt;

      m_dash_duration -= dt;
    }

    btQuaternion rotate_to_up = handle_rotation(move_v, m_up, dt);

    if (m_in_air > 1000) m_in_air = 1000;

    //// If on ground, check slope and reduce move vector horizontal component to reduce speed if
    //// going up/downhill
    // if (get_on_ground()) {
    //  btVector3 t = m_pos - m_up * (m_height * 0.5f + 2.0f);
    //  btCollisionWorld::AllHitsRayResultCallback callback =
    //      ray_test(m_pos, m_pos - m_up * (m_height * 0.5f + 2.0f), world);
    //  if (callback.hasHit()) {
    //    int i = 0;
    //    while (i < callback.m_hitFractions.size()) {
    //      move_vector *= btDot(m_up, callback.m_hitNormalWorld[i]);
    //      break;
    //    }
    //  }
    //}

    // Accelerate vertically
    if (!dashing()) {
      m_vertical_velocity += m_gravity.length() * dt;
    } else {
      m_vertical_velocity = 0.0f;
    }

    if (m_vertical_velocity > m_terminal_velocity) {
      m_vertical_velocity = m_terminal_velocity;
    }

    btTransform start;
    start.setIdentity();
    start.setRotation(m_facing_direction * rotate_to_up);
    start.setOrigin(m_ghost_object->getWorldTransform().getOrigin());

    // Sweep up
    float move_up_length = 0.0f;
    if (!m_is_flying) {
      SweepResult sr = iterative_sweeping(
          start, start.getOrigin() + (m_up_step * dt) * -normalized_gravity, world, false, true);

      move_up_length = (start.getOrigin() - sr.result_transform.getOrigin()).length();

      start = sr.result_transform;
    }

    // Sweep horizontally
    if (move_v.length() > 0.01f) {
      SweepResult sr = iterative_sweeping(start, start.getOrigin() + move_v, world, true);
      start = sr.result_transform;
    }

    // Sweep down
    if (!m_is_flying) {
      btVector3 end = start.getOrigin() - move_up_length * -normalized_gravity;

      if (!dashing()) {
        end -= (m_vertical_velocity + m_down_step) * dt * -normalized_gravity;
      }

      if ((start.getOrigin() - end).length() > 0.01f) {
        SweepResult sr = iterative_sweeping(start, end, world, false, true);

        start = sr.result_transform;

        // Make sure that hit point is below position if it's going to be considered a hit
        if (sr.hit && btDot(start.getOrigin() - sr.hit_point, -normalized_gravity) >= 0.0f) {
          m_in_air = 0;
          m_vertical_velocity = 0.0f;
        } else {
          m_in_air++;
        }
      }
    }

    btTransform result = start;

    m_ghost_object->setWorldTransform(result);

    // Update last pos and pos
    m_last_pos = m_pos;
    m_pos = m_ghost_object->getWorldTransform().getOrigin();

    // If dashing and velocity is lower than threshold, stop dash
    if (dashing() && glm::length(get_velocity()) < m_dash_stop_velocity) {
      m_dash_duration = 0.0f;
    }
  } else {
    btTransform start_transform = m_ghost_object->getWorldTransform();
    btTransform new_transform = start_transform;

    btVector3 start_pos = start_transform.getOrigin();
    btVector3 new_origin = start_pos;

    const float swing_terminal_velocity = 170.f;
    if (m_swinging_velocity.length() > swing_terminal_velocity)
      m_swinging_velocity = m_swinging_velocity.normalize() * swing_terminal_velocity;
    m_swinging_velocity -= -m_gravity * dt;

    new_origin += m_swinging_velocity * dt;

    if (get_rope_length_from(new_origin) > get_rope_length()) {
      btVector3 to_character = new_origin - m_swing_center;

      // Set vector length to rope length
      float length_ratio = get_rope_length() / to_character.length();

      btVector3 before_correction = to_character;

      // Adjust length from rope center to keep character within rope distance
      to_character *= length_ratio;

      new_origin = m_swing_center + to_character;

      // Accelerate towards rope center
      btVector3 to_center = -to_character;

      if (to_center.length() > 0.01f) {
        m_swinging_velocity -=
            btDot(m_swinging_velocity, to_center.normalized()) * to_center.normalized();

        // Acceleration toward middle is v^2 / r
        m_swinging_velocity +=
            to_center.normalized() * m_swinging_velocity.length2() / to_center.length() * dt;
      }
    }

    // Apply friction
    if (m_swinging_velocity.length() > 0.01f) {
      float velocity_length = m_swinging_velocity.length();

      m_swinging_velocity =
          m_swinging_velocity.normalized() *
          (velocity_length - velocity_length * velocity_length * m_rope_friction * dt);
    }

    // Sweep to ensure no geometry is hit
    if ((start_pos - new_origin).length() > 0.01f) {
      btTransform from = start_transform;
      btTransform to = from;
      to.setOrigin(new_origin);

      // btCollisionWorld::ClosestConvexResultCallback rr = sweep_test(from, to, world);

      SweepResult sr = iterative_sweeping(from, to.getOrigin(), world, true);

      btVector3 dir = start_pos - new_origin;

      if (sr.hit) {
        new_origin = sr.result_transform.getOrigin();
        // new_origin.setInterpolate3(from.getOrigin(), to.getOrigin(),
        //                           rr.m_closestHitFraction - 0.01f);

        btVector3 normal = sr.hit_normal;
        // btVector3 normal = rr.m_hitNormalWorld;

        if (btDot(normal, m_swinging_velocity) > 0.0f) {
          normal = -normal;
        }

        // Remove velocity in normal direction
        if (btDot(normal, m_swinging_velocity) < 0.0f) {
          m_swinging_velocity -= btDot(m_swinging_velocity, normal) * normal;
          m_swinging_velocity *= (1.0f - m_swing_bounce_friction);
        }
      }
    }

    // Handle rotation
    if (m_swinging_velocity.length() > 1.5f) {
      btQuaternion rotate_to_up =
          handle_rotation(vc(get_velocity()) - btDot(vc(get_velocity()), m_up) * m_up, m_up, dt);
      new_transform.setRotation(m_facing_direction * rotate_to_up);
    }

    // Set new position
    new_transform.setOrigin(new_origin);
    m_ghost_object->setWorldTransform(new_transform);

    // Update last pos and pos
    m_last_pos = m_pos;
    m_pos = m_ghost_object->getWorldTransform().getOrigin();

    // Raycast down to check if character is grounded
    btTransform from = m_ghost_object->getWorldTransform();
    from.setOrigin(m_pos);
    btTransform to = from;
    to.setOrigin(from.getOrigin() + normalized_gravity * m_down_step * dt);

    if ((from.getOrigin() - to.getOrigin()).length() > 0.01f) {
      btCollisionWorld::ClosestConvexResultCallback rr = sweep_test(from, to, world);

      if (rr.hasHit() && btDot(rr.m_hitNormalWorld, -normalized_gravity) > m_max_slope) {
        m_in_air = 0;
      } else {
        m_in_air++;
      }
    }
  }

  // Reset filter group
  proxy->m_collisionFilterGroup = m_use_navmesh_collision ? m_navmesh_group : m_normal_group;
}
void CharacterController::jump(float jump_strength) {
  m_vertical_velocity = -jump_strength;
  m_in_air += m_in_air_threshold;
}

glm::mat4 CharacterController::get_transform() const {
  return mc(m_ghost_object->getWorldTransform());
}
void CharacterController::set_transform(glm::mat4 transform) {
  m_ghost_object->setWorldTransform(mc(transform));
}

glm::vec3 CharacterController::get_up() const { return vc(m_up); }
void CharacterController::set_up(glm::vec3 up_vector) { m_up = vc(up_vector); }

glm::vec3 CharacterController::get_gravity() const { return vc(m_gravity); }
void CharacterController::set_gravity(glm::vec3 gravity) { m_gravity = vc(gravity); }

unsigned char CharacterController::get_frame_updated() const { return m_frame_updated; }
void CharacterController::set_frame_updated(unsigned char frame_updated) {
  m_frame_updated = frame_updated;
}

bool CharacterController::get_on_ground() const { return m_in_air < m_in_air_threshold; }

void CharacterController::activate() { m_ghost_object->activate(); }

glm::vec3 CharacterController::get_velocity() const {
  return vc(m_pos - m_last_pos) / (m_last_dt < 0.0001f ? 0.0016f : m_last_dt);
}

glm::quat CharacterController::get_orientation() const {
  return qc(m_ghost_object->getWorldTransform().getRotation());
}
void CharacterController::set_orientation(glm::quat orientation) {
  m_ghost_object->getWorldTransform().setRotation(qc(orientation));
}

void CharacterController::destroy(btDiscreteDynamicsWorld* world) {
  world->removeCollisionObject(m_ghost_object.get());
}

void CharacterController::create_shape(float radius, float height) {
  if (radius != m_radius || height != m_height || !m_shape) {
    m_shape = std::make_shared<CapsuleCollisionShape>(radius, height);

    m_height = height;
    m_radius = radius;
  }
}

btCollisionWorld::ClosestConvexResultCallback CharacterController::sweep_test(
    btTransform& from, btTransform& to, btDiscreteDynamicsWorld* world) {
  btCollisionWorld::ClosestConvexResultCallback callback(from.getOrigin(), to.getOrigin());
  callback.m_collisionFilterGroup = m_use_navmesh_collision ? m_navmesh_group : m_normal_group;
  callback.m_collisionFilterMask =
      (~CollisionGroup::TRIGGER) & (m_use_navmesh_collision ? m_navmesh_mask : m_normal_mask);

  btCollisionShape* col_shape = m_shape->get_shape();
  btConvexShape* shape = reinterpret_cast<btConvexShape*>(col_shape);

  float length = (from.getOrigin() - to.getOrigin()).length();
  if (length > 0.001f) {
    world->convexSweepTest(shape, from, to, callback,
                           world->getDispatchInfo().m_allowedCcdPenetration);
  } else {
    // Distance is too small
    callback.m_closestHitFraction = 2.0f;  // m_closestHitFraction > 1.0 signifies miss
    callback.m_hitPointWorld = to.getOrigin();
    callback.m_hitNormalWorld = btVector3(0, 0, 1);
  }

  return callback;
}

btCollisionWorld::AllHitsRayResultCallback CharacterController::ray_test(
    btVector3 from, btVector3 to, btDiscreteDynamicsWorld* world) {
  btCollisionWorld::AllHitsRayResultCallback callback(from, to);
  callback.m_collisionFilterGroup = m_normal_group;
  callback.m_collisionFilterMask = (~CollisionGroup::TRIGGER) & m_normal_mask;

  world->rayTest(from, to, callback);

  return callback;
}

CharacterController::SweepResult CharacterController::iterative_sweeping(
    btTransform start_transform, btVector3 target_position, btDiscreteDynamicsWorld* world,
    bool ignore_ground_terminate, bool adjust_along_up) {
  SweepResult result;
  result.hit = false;

  btVector3 sweep_vector = target_position - start_transform.getOrigin();
  btTransform end_transform = start_transform;
  end_transform.setOrigin(end_transform.getOrigin() + sweep_vector);

  for (int i = 0; i < m_sweep_iterations; i++) {
    btCollisionWorld::ClosestConvexResultCallback callback =
        sweep_test(start_transform, end_transform, world);

    // Set up result
    bool hit = callback.hasHit();
    if (hit) {
      result.hit = true;
      result.hit_normal = callback.m_hitNormalWorld;
      result.hit_point = callback.m_hitPointWorld;
    }

    result.result_transform = end_transform;
    btVector3 result_pos;

    btVector3 normal = callback.m_hitNormalWorld;

    if (hit) {
      result_pos.setInterpolate3(start_transform.getOrigin(), result.result_transform.getOrigin(),
                                 callback.m_closestHitFraction);

      btVector3 adjustment = m_adjustment_length * normal;

      if (adjust_along_up) {
        // Project along up vector

        if (btDot(adjustment, m_up) > 0.0f) {
          adjustment = m_adjustment_length * m_up;
        } else {
          adjustment = m_adjustment_length * -m_up;
        }
      }

      result.result_transform.setOrigin(result_pos + adjustment);

      // Remove the moved distance from the sweep vector
      sweep_vector -= result.result_transform.getOrigin() - start_transform.getOrigin();

      // Remove the component of sweep_vector that is parallell to hit normal. This ensures that
      // the next iteration will move parallell to the surface hit
      sweep_vector -= btDot(sweep_vector, normal) * normal;

      // Set up transforms for next iteration
      start_transform = result.result_transform;
      end_transform.setOrigin(start_transform.getOrigin() + sweep_vector);
    } else {
      break;
    }

    float hit_dot = btDot(normal, m_up);

    // Check if the ground or a cieling has been hit
    bool hit_ground = hit ? hit_dot > m_max_slope : false;
    bool hit_ceiling = hit ? hit_dot < -m_max_slope : false;

    if (hit_ceiling && m_vertical_velocity < 0.0f) {
      m_vertical_velocity = 0.0f;
    }

    // Cancel iteration if sweep misses, or it hits ground or ceiling
    if ((hit_ground || hit_ceiling) && !ignore_ground_terminate) {
      break;
    }
  }

  return result;
}

btQuaternion CharacterController::handle_rotation(btVector3 horizontal_move_vector, btVector3 up,
                                                  float dt) {
  // Create rotation from canonical up direction (0, 0, 1) to the up vector of the character
  // controller
  float up_dot = glm::dot(glm::vec3(0, 0, 1), vc(up));

  btQuaternion rotate_to_up;
  if (up_dot > -0.999f) {
    rotate_to_up = qc(glm::rotation(glm::vec3(0, 0, 1), vc(up)));
  } else {
    rotate_to_up = qc(glm::rotation(glm::vec3(0, 0, 1), glm::vec3(0, 1, 0)) *
                      glm::rotation(glm::vec3(0, 1, 0), vc(up)));
  }

  // Create rotation from canonical forward direction (0, 1, 0) to character's move direction
  if (m_face_moving_direction) {
    float move_dot = glm::dot(glm::normalize(vc(horizontal_move_vector)), vc(up));
    float face_dot =
        glm::dot(glm::normalize(vc(horizontal_move_vector)), qc(rotate_to_up) * glm::vec3(0, 1, 0));

    if (glm::length(vc(horizontal_move_vector)) > 0.01f && move_dot > -0.999f) {
      glm::vec3 normalized_planar_move_vector = glm::normalize(
          vc(horizontal_move_vector) - vc(up) * glm::dot(vc(up), vc(horizontal_move_vector)));

      if (face_dot > -0.999f) {
        m_desired_direction =
            qc(glm::rotation(qc(rotate_to_up) * glm::vec3(0, 1, 0), normalized_planar_move_vector));
      } else {
        glm::quat face_rotation = glm::rotation(glm::vec3(0, 1, 0), glm::vec3(1, 0, 0)) *
                                  glm::rotation(glm::vec3(1, 0, 0), normalized_planar_move_vector);

        m_desired_direction = qc(qc(rotate_to_up) * face_rotation);
      }
    }
  } else {
    if (fabs(m_facing_angle - glm::pi<float>()) > 0.01f) {
      glm::vec3 result_vector =
          glm::rotate(glm::mat4(1.0f), m_facing_angle, qc(rotate_to_up) * glm::vec3(0, 0, 1)) *
          (qc(rotate_to_up) * glm::vec4(0, 1, 0, 0));

      m_desired_direction = qc(glm::rotation(qc(rotate_to_up) * glm::vec3(0, 1, 0), result_vector));
    } else {
      glm::vec3 result_vector =
          glm::rotate(glm::mat4(1.0f), m_facing_angle, qc(rotate_to_up) * glm::vec3(0, 0, 1)) *
          (qc(rotate_to_up) * glm::vec4(0, 1, 0, 0));

      m_desired_direction = qc(glm::rotation(qc(rotate_to_up) * glm::vec3(0, 1, 0),
                                             qc(rotate_to_up) * glm::vec3(1, 0, 0)) *
                               glm::rotation(qc(rotate_to_up) * glm::vec3(1, 0, 0), result_vector));
    }
  }

  float angle = m_facing_direction.angleShortestPath(m_desired_direction);

  if (angle < glm::pi<float>() - 0.03f) {
    m_facing_direction = m_facing_direction.slerp(m_desired_direction, dt * m_turning_speed);
  } else {
    glm::vec3 side_vector =
        glm::cross(qc(m_facing_direction) * qc(rotate_to_up) * glm::vec3(0, 1, 0), vc(up));

    glm::quat q = glm::rotation(qc(rotate_to_up) * glm::vec3(0, 1, 0), side_vector);

    m_facing_direction = m_facing_direction.slerp(qc(q), dt * 15.0f);
  }

  return rotate_to_up;
}

void CharacterController::dash(glm::vec3 dash_velocity, float duration) {
  m_dash_velocity = vc(dash_velocity);
  m_dash_duration = duration;
}

bool CharacterController::dashing() { return m_dash_duration > 0.0f; }

void CharacterController::get_aabb(btVector3& min, btVector3& max) const {
  m_ghost_object->getCollisionShape()->getAabb(m_ghost_object->getWorldTransform(), min, max);
}

bool CharacterController::get_is_swinging() const { return m_is_swinging; }

void CharacterController::start_swing(glm::vec3 center) {
  if (get_is_swinging()) {
    end_swing();
  }

  m_swinging_velocity = vc(get_velocity());

  m_dash_duration = 0.0f;
  m_swing_center = vc(center);

  m_is_swinging = true;

  set_rope_length(get_rope_length_from(m_pos));
}

void CharacterController::end_swing() {
  if (m_is_swinging) {
    m_is_swinging = false;
    m_vertical_velocity = 0.0f;
  }
}

float CharacterController::get_rope_length() const { return m_rope_length; }

float CharacterController::get_rope_length_from(btVector3 position) const {
  return (position - m_swing_center).length();
}

void CharacterController::set_rope_length(float length) { m_rope_length = length; }

glm::vec3 CharacterController::get_swing_center() const { return vc(m_swing_center); }

void CharacterController::add_swing_velocity(glm::vec3 velocity) {
  if (get_is_swinging()) {
    m_swinging_velocity += vc(velocity);
  }
}

}  // namespace phy