#include "bse/assert.hpp"
#include "bse/asset.hpp"
#include "phy/collision_mesh.hpp"
#include <phy/collision_shapes.hpp>
class btCollisionShape;

// Don't use overloaded new/delete here, bullet has them overloaded
#undef new

namespace phy {

glm::vec3 vc(btVector3 v) { return glm::vec3(v.x(), v.y(), v.z()); }
btVector3 vc(glm::vec3 v) { return btVector3(v.x, v.y, v.z); }

glm::mat4 mc(const btTransform& m) {
  float t[16];

  // Transfer bullet matrix data to array
  m.getOpenGLMatrix(static_cast<btScalar*>(t));

  // Create glm matrix from array and return
  return glm::make_mat4(t);
}
btTransform mc(const glm::mat4& m) {
  float t[16];

  // Transfer glm matrix data to array
  const float* p_source = (const float*)glm::value_ptr(m);
  for (int i = 0; i < 16; ++i) t[i] = p_source[i];

  // Create bullet matrix from array and return
  btTransform ret;
  ret.setFromOpenGLMatrix(t);
  return ret;
}

glm::quat qc(const btQuaternion& q) {
  glm::quat r;
  r.w = q.getW();
  r.x = q.getX();
  r.y = q.getY();
  r.z = q.getZ();
  return r;
}

btQuaternion qc(const glm::quat& q) {
  btQuaternion r;
  r.setW(q.w);
  r.setX(q.x);
  r.setY(q.y);
  r.setZ(q.z);
  return r;
}

BoxCollisionShape::BoxCollisionShape(glm::vec3 half_legths) {
  m_box.reset(new btBoxShape(vc(half_legths)));
  m_half_lengths = half_legths;
}

BoxCollisionShape::BoxCollisionShape(BoxCollisionShape&& other) {
  m_box = std::move(other.m_box);
  m_half_lengths = other.m_half_lengths;
}

BoxCollisionShape& BoxCollisionShape::operator=(BoxCollisionShape&& other) {
  if (&other != this) {
    m_box = std::move(other.m_box);
    m_half_lengths = other.m_half_lengths;
  }
  return *this;
}

BoxCollisionShape::~BoxCollisionShape() { /*delete m_box;*/
}

// float BoxCollisionShape::get_radius() const { return m_box->getHalfExtentsWithMargin().length();
// }

btCollisionShape* BoxCollisionShape::get_shape() const { return m_box.get(); }

SphereCollisionShape::SphereCollisionShape(float radius) {
  m_sphere.reset(new btSphereShape(radius));
  m_radius = radius;
}

SphereCollisionShape::SphereCollisionShape(SphereCollisionShape&& other) {
  m_sphere = std::move(other.m_sphere);
  m_radius = other.m_radius;
}
SphereCollisionShape& SphereCollisionShape::operator=(SphereCollisionShape&& other) {
  if (&other != this) {
    m_sphere = std::move(other.m_sphere);
    m_radius = other.m_radius;
  }
  return *this;
}

SphereCollisionShape::~SphereCollisionShape() { /*delete m_sphere;*/
}

// float SphereCollisionShape::get_radius() const { return m_sphere->getRadius(); }

btCollisionShape* SphereCollisionShape::get_shape() const { return m_sphere.get(); }

CapsuleCollisionShape::CapsuleCollisionShape(float radius, float height) {
  m_capsule.reset(new btCapsuleShapeZ(radius, height));
  m_radius = radius;
  m_height = height;
}
CapsuleCollisionShape::CapsuleCollisionShape(CapsuleCollisionShape&& other) {
  m_capsule = std::move(other.m_capsule);
  m_radius = other.m_radius;
  m_height = other.m_height;
}
CapsuleCollisionShape& CapsuleCollisionShape::operator=(CapsuleCollisionShape&& other) {
  if (&other != this) {
    m_capsule = std::move(other.m_capsule);
    m_radius = other.m_radius;
    m_height = other.m_height;
  }
  return *this;
}

CapsuleCollisionShape::~CapsuleCollisionShape() { /*delete m_capsule;*/
}

// float CapsuleCollisionShape::get_radius() const {
//  float radius, height;
//  radius = m_capsule->getRadius();
//  height = m_capsule->getHalfHeight() * 2.0f;
//  return radius > height ? radius : height;
//}

btCollisionShape* CapsuleCollisionShape::get_shape() const { return m_capsule.get(); }

CylinderCollisionShape::CylinderCollisionShape(glm::vec3 half_legths) {
  m_cylinder.reset(new btCylinderShapeZ(vc(half_legths)));
  m_half_legths = half_legths;
}
CylinderCollisionShape::CylinderCollisionShape(CylinderCollisionShape&& other) {
  m_cylinder = std::move(other.m_cylinder);
  m_half_legths = other.m_half_legths;
}
CylinderCollisionShape& CylinderCollisionShape::operator=(CylinderCollisionShape&& other) {
  if (&other != this) {
    m_cylinder = std::move(other.m_cylinder);
    m_half_legths = other.m_half_legths;
  }
  return *this;
}

CylinderCollisionShape::~CylinderCollisionShape() { /*delete m_cylinder;*/
}

// float CylinderCollisionShape::get_radius() const {
//  return m_cylinder->getHalfExtentsWithMargin().length();
//}

btCollisionShape* CylinderCollisionShape::get_shape() const { return m_cylinder.get(); }

CompoundCollisionShape::CompoundCollisionShape(
    std::vector<std::pair<std::unique_ptr<CollisionShape>, glm::mat4>>&& shapes) {
  ASSERT(shapes.size() > 0) << "Tried to create empty CompoundCollisionShape!";

  m_children = std::move(shapes);
  m_compound.reset(new btCompoundShape(true, static_cast<int>(shapes.size())));

  for (std::pair<std::unique_ptr<CollisionShape>, glm::mat4>& p : m_children) {
    btTransform child_transform;

    child_transform = mc(p.second);

    m_compound->addChildShape(child_transform, p.first->get_shape());
  }
}
CompoundCollisionShape::CompoundCollisionShape(CompoundCollisionShape&& other) {
  m_compound = std::move(other.m_compound);
  m_children = std::move(other.m_children);
}
CompoundCollisionShape& CompoundCollisionShape::operator=(CompoundCollisionShape&& other) {
  if (&other != this) {
    m_compound = std::move(other.m_compound);
    m_children = std::move(other.m_children);
  }
  return *this;
}

CompoundCollisionShape::~CompoundCollisionShape() {
  /*delete m_compound;*/
  // for (std::pair<CollisionShape*, glm::mat4> p : m_children) {
  //   delete p.first;
  // }
}

// float CompoundCollisionShape::get_radius() const {
//  float radius = 0;
//  for (std::pair<CollisionShape*, glm::mat4> p : m_children) {
//    glm::vec3 scale;
//    glm::quat rotation;
//    glm::vec3 translation;
//    glm::vec3 skew;
//    glm::vec4 perspective;
//    glm::decompose(p.second, scale, rotation, translation, skew, perspective);
//    float child_radius = p.first->get_radius() + glm::length(translation);
//
//    if (child_radius > radius) radius = child_radius;
//  }
//
//  return radius;
//}

btCollisionShape* CompoundCollisionShape::get_shape() const { return m_compound.get(); }

CollisionShapeType CompoundCollisionShape::get_type() const {
  CollisionShapeType types = CollisionShapeType::COMPOUND;
  for (auto& p : m_children) {
    CollisionShapeType child_type = p.first->get_type();
    types = static_cast<CollisionShapeType>(types | child_type);
  }

  return types;
}

TriangleMeshCollisionShape::TriangleMeshCollisionShape(bse::Asset<CollisionMesh> mesh) {
  m_mesh_asset = mesh;

  m_unscaled_mesh = get_triangle_mesh_shape(mesh);
  m_mesh.reset(
      new btScaledBvhTriangleMeshShape(m_unscaled_mesh.get(), btVector3(1.0f, 1.0f, 1.0f)));
}
TriangleMeshCollisionShape::TriangleMeshCollisionShape(TriangleMeshCollisionShape&& other) {
  m_unscaled_mesh = std::move(other.m_unscaled_mesh);
  m_mesh = std::move(other.m_mesh);
  m_mesh_asset = other.m_mesh_asset;
}
TriangleMeshCollisionShape& TriangleMeshCollisionShape::operator=(
    TriangleMeshCollisionShape&& other) {
  if (&other != this) {
    m_unscaled_mesh = std::move(other.m_unscaled_mesh);
    m_mesh = std::move(other.m_mesh);
    m_mesh_asset = other.m_mesh_asset;
  }
  return *this;
}

TriangleMeshCollisionShape::~TriangleMeshCollisionShape() {
  /*delete m_mesh;*/
  /*delete m_unscaled_mesh;*/
}

// float TriangleMeshCollisionShape::get_radius() const {
//  btVector3 center;
//  float radius;
//  m_mesh->getBoundingSphere(center, radius);
//
//  return radius + center.length();
//}

btCollisionShape* TriangleMeshCollisionShape::get_shape() const { return m_mesh.get(); }

bse::Asset<CollisionMesh> TriangleMeshCollisionShape::get_mesh_asset() const {
  return m_mesh_asset;  //
}

ConvexHullCollisionShape::ConvexHullCollisionShape(bse::Asset<CollisionMesh> mesh) {
  m_hull.reset(new btConvexHullShape(mesh->get_vertex_array().data(),
                                     static_cast<int>(mesh->get_num_vertices()),
                                     sizeof(float) * 3));
}
ConvexHullCollisionShape::ConvexHullCollisionShape(ConvexHullCollisionShape&& other) {
  m_hull = std::move(other.m_hull);
  m_mesh_asset = other.m_mesh_asset;
}

ConvexHullCollisionShape& ConvexHullCollisionShape::operator=(ConvexHullCollisionShape&& other) {
  if (&other != this) {
    m_hull = std::move(other.m_hull);
    m_mesh_asset = other.m_mesh_asset;
  }
  return *this;
}

bse::Asset<CollisionMesh> ConvexHullCollisionShape::get_mesh_asset() const { return m_mesh_asset; }

ConvexHullCollisionShape::~ConvexHullCollisionShape() { /*delete m_hull;*/
}

// float ConvexHullCollisionShape::get_radius() const {
//  btVector3 center;
//  float radius;
//
//  m_hull->getBoundingSphere(center, radius);
//
//  return radius + center.length();
//}

btCollisionShape* ConvexHullCollisionShape::get_shape() const { return m_hull.get(); }

// Cache of mesh shapes
std::unordered_map<bse::AssetID<CollisionMesh>, std::shared_ptr<btBvhTriangleMeshShape>>
    TriangleMeshCollisionShape::m_mesh_shapes;

std::mutex TriangleMeshCollisionShape::m_mesh_shapes_lock;

std::shared_ptr<btBvhTriangleMeshShape> TriangleMeshCollisionShape::get_triangle_mesh_shape(
    bse::Asset<CollisionMesh>& asset) {
  // std::scoped_lock lock(m_mesh_shapes_lock);
  if (!m_mesh_shapes.count(asset.get_id())) {
    m_mesh_shapes[asset.get_id()].reset(new btBvhTriangleMeshShape(asset->get_mesh_data(), true));
  }

  return m_mesh_shapes.at(asset.get_id());
}
void TriangleMeshCollisionShape::clear_mesh_shapes() { m_mesh_shapes.clear(); }

}  // namespace phy