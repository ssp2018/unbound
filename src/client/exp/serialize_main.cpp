// Serialization experiment

#include <bse/serialize.hpp>
#include <bse/system_data.hpp>     // for initialize_system_data
#include <bse/task_scheduler.hpp>  // for TaskScheduler
#include <gfx/window.hpp>          // for Window
#include <gmp/unbound.hpp>         // for Unbound
#include <iostream>
#include <scr/script.hpp>  // for Unbound

std::ostream& operator<<(std::ostream& out, const bse::SerializedData& data) {
  for (const auto& byte : data.bytes) {
    out << std::hex << +byte << " " << std::dec;
  }
  return out;
}

struct NonTrivial {
  int a;
  int b;
  std::string c;
};

REFLECT((NonTrivial), a, b, c)

struct NonTrivialB {
  int a;
  int b;
  std::string c;
};

SERFUN((NonTrivialB)) {
  SERIALIZE(self.a);
  SERIALIZE(self.b);
  SERIALIZE(self.c);
}

DESERFUN((NonTrivialB)) {
  DESERIALIZE(self.a);
  DESERIALIZE(self.b);
  DESERIALIZE(self.c);
}

struct NonTrivialBaseType {
  std::string base_string;
  VIRTUAL_CLASS_SERFUN((NonTrivialBaseType)) { SERIALIZE(self.base_string); }
  VIRTUAL_CLASS_DESERFUN((NonTrivialBaseType)) { DESERIALIZE(self.base_string); }
};

struct NonTrivialDerivedType : public NonTrivialBaseType {
  std::string derived_string;

  VIRTUAL_CLASS_SERFUN((NonTrivialDerivedType)) {
    SERIALIZE_BASE_CLASS((NonTrivialBaseType))
    SERIALIZE(self.derived_string);
  }

  VIRTUAL_CLASS_DESERFUN((NonTrivialDerivedType)) {
    DESERIALIZE_BASE_CLASS((NonTrivialBaseType))
    DESERIALIZE(self.derived_string);
  }
};

template <typename T, int N>
struct NonTrivialTemplateType {
  T a;
  std::array<T, N> b;
};

TEMPLATE_SERFUN((typename T, int N), (NonTrivialTemplateType<T, N>)) {
  SERIALIZE(self.a);
  SERIALIZE(self.b);
}

TEMPLATE_DESERFUN((typename T, int N), (NonTrivialTemplateType<T, N>)) {
  DESERIALIZE(self.a);
  DESERIALIZE(self.b);
}

template <typename T, int N>
struct NonTrivialTemplateTypeB : public NonTrivialBaseType {
  T a;
  std::array<T, N> b;

  VIRTUAL_CLASS_SERFUN((NonTrivialTemplateTypeB<T, N>)) {
    SERIALIZE_BASE_CLASS((NonTrivialBaseType));
    SERIALIZE(self.a);
    SERIALIZE(self.b);
  }

  VIRTUAL_CLASS_DESERFUN((NonTrivialTemplateTypeB<T, N>)) {
    DESERIALIZE_BASE_CLASS((NonTrivialBaseType));
    DESERIALIZE(self.a);
    DESERIALIZE(self.b);
  }
};

int main(int argc, char* argv[]) {
  unsigned int max_frame_count = 0;
  if (argc >= 2) max_frame_count = std::stoi(argv[1]);

  bse::initialize_system_data();

  // Hello vs dfs
#if defined(UNBOUND_PROFILE) || defined(NDEBUG)
  constexpr size_t width = 1920;
  constexpr size_t height = 1080;
#else
  constexpr size_t width = 1280;
  constexpr size_t height = 720;
#endif

  ::bse::SerializedData data;
  data.reset();

  {
    bse::Asset<scr::Script> m_test_script(bse::ASSETS_ROOT /
                                          "script/scr/test/test_post_ser.lua"_fp);

    scr::Object scr = m_test_script->return_value()["my_func"];
    scr::Object tab = m_test_script->return_value()["my_table"];

    std::function<void(void)> func = scr;
    func();

    std::string map_value = tab["hello"];
    std::cout << map_value << std::endl;

    bse::serialize(scr, data);
    bse::serialize(tab, data);
  }

  {
    int offset = 0;

    scr::Object de_scr;
    scr::Object de_tab;

    bse::deserialize(de_scr, data, offset);
    bse::deserialize(de_tab, data, offset);

    std::function<void(void)> de_func = de_scr;
    de_func();

    std::string de_map_value = de_tab["hello"];
    std::cout << de_map_value << std::endl;

    std::cout << std::flush;
  }

  return 0;
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////
  //////////////////////////////////////////

  {
    data.reset();
    std::string test = "IAmAString";
    bse::serialize(test, data);
    std::cout << data << std::endl;
    std::string after;
    int offset = 0;
    bse::deserialize(after, data, offset);
    std::cout << "After: " << after << std::endl;
  }
  std::cout << "======" << std::endl;
  {
    int test = 52;
    data.reset();
    bse::serialize(test, data);
    std::cout << data << std::endl;
    int after;
    int offset = 0;
    bse::deserialize(after, data, offset);
    std::cout << "After: " << after << std::endl;
  }
  std::cout << "======" << std::endl;
  {
    struct Trivial {
      int a;
      int b;
      int c;
      // std::string c;
    } test{92, 10, 4};
    data.reset();
    bse::serialize(test, data);
    std::cout << data << std::endl;
    Trivial after;
    int offset = 0;
    bse::deserialize(after, data, offset);
    std::cout << "After: " << after.a << " " << after.b << " " << after.c << std::endl;
  }
  std::cout << "======" << std::endl;
  {
    NonTrivial test{92, 10, "OhWow"};
    data.reset();
    bse::serialize(test, data);
    std::cout << data << std::endl;
    NonTrivial after;
    int offset = 0;
    bse::deserialize(after, data, offset);
    std::cout << "After: " << after.a << " " << after.b << " " << after.c << std::endl;
  }
  std::cout << "======" << std::endl;
  {
    NonTrivialB test{22, 44, "B type is better than E type"};
    data.reset();
    bse::serialize(test, data);
    std::cout << data << std::endl;
    NonTrivialB after;
    int offset = 0;
    bse::deserialize(after, data, offset);
    std::cout << "After: " << after.a << " " << after.b << " " << after.c << std::endl;
  }
  std::cout << "======" << std::endl;
  {
    NonTrivialDerivedType test;
    test.base_string = "Base Type";
    test.derived_string = "Derived Type";
    data.reset();
    bse::serialize(test, data);
    std::cout << data << std::endl;
    NonTrivialDerivedType after;
    int offset = 0;
    bse::deserialize(after, data, offset);
    std::cout << "After: " << after.base_string << " " << after.derived_string << std::endl;
  }
  std::cout << "======" << std::endl;
  {
    NonTrivialTemplateType<float, 2> test{0.f, {1.f, 2.f}};

    data.reset();
    bse::serialize(test, data);
    std::cout << data << std::endl;
    NonTrivialTemplateType<float, 2> after;
    int offset = 0;
    bse::deserialize(after, data, offset);
    std::cout << "After: " << after.a << " " << after.b[0] << " " << after.b[1] << std::endl;
  }
  std::cout << "======" << std::endl;
  {
    NonTrivialTemplateTypeB<float, 2> test;
    test.base_string = "My String";
    test.a = 99.f;
    test.b[0] = 1.f;
    test.b[1] = 2.f;

    data.reset();
    bse::serialize(test, data);
    std::cout << data << std::endl;
    NonTrivialTemplateTypeB<float, 2> after;
    int offset = 0;
    bse::deserialize(after, data, offset);
    std::cout << "After: " << after.base_string << " " << after.a << " " << after.b[0] << " "
              << after.b[1] << std::endl;
  }
  std::cout << "======" << std::endl;

  using MT = NonTrivialTemplateType<float, 2>;

  std::cout << "Name: " << sol::detail::ctti_get_type_name<MT>() << std::endl;

  // gfx::Window w(width, height, "Title");
  // {
  //   gmp::Unbound unbound(w, max_frame_count);
  //   unbound.start();
  // }
  bse::TaskScheduler::destroy();  //
}

// namespace bse