
// for memory leaks
#define _CRTDBG_MAP_ALLOC
#include <bse/asset.hpp>
#include <bse/edit.hpp>
#include <bse/memory_debug.hpp>
#include <bse/system_data.hpp>     // for initialize_system_data
#include <bse/task_scheduler.hpp>  // for TaskScheduler
#include <cor/frame_context.hpp>
#include <cor/script_type.hpp>  // for Unbound

#if defined(WIN32)
#include <crtdbg.h>
#endif
#include <ext/ext.hpp>
#include <fstream>
#include <gfx/font.hpp>
#include <gfx/shader.hpp>
#include <gfx/window.hpp>  // for Window
#include <gmp/systems/graphics_system.hpp>
#include <gmp/unbound.hpp>  // for Unbound
#include <stdlib.h>
#include <string.h>

std::map<void *, AllocInfo> *m_allocation_map = nullptr;
std::map<std::string, size_t> *m_size_per_file = nullptr;

int main(int argc, char *argv[]) {
#ifdef UNBOUND_CUSTOM_ALLOCATION
  m_allocation_map = new std::map<void *, AllocInfo>();
  m_size_per_file = new std::map<std::string, size_t>();
#endif
#if defined(WIN32)
  _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif
  unsigned int max_frame_count = 0;
  if (argc >= 2) max_frame_count = std::stoi(argv[1]);

    // bse::initialize_system_data();
    // scr::detail::State::get();

    // Hello vs dfs
#if defined(UNBOUND_PROFILE) || defined(NDEBUG)
  size_t width = 1920;
  size_t height = 1080;
#else
  size_t width = 1280;
  size_t height = 720;
#endif
  bool fullscreen = false;
  std::ifstream settings("config");
  if (settings.is_open()) {
    std::string line;
    std::getline(settings, line);
    if (line[0] == '0') {
      fullscreen = false;
    } else {
      fullscreen = true;
      GLFWmonitor *monitor = glfwGetPrimaryMonitor();
    }
    std::getline(settings, line);
    width = std::stoi(line);
    std::getline(settings, line);
    height = std::stoi(line);
    settings.close();
  } else {
    std::cout << "Could not open config" << std::endl;
    std::ofstream file;
    file.open("config");
    file << "1" << std::endl;
    file << "1920" << std::endl;
    file << "1080" << std::endl;
    file.close();
  }

#if defined(UNBOUND_PROFILE)
  // fullscreen = true;  // true;
#endif

#ifndef NDEBUG
  fullscreen = false;
#endif

  gfx::Window w(width, height, "Title", fullscreen);
  {
    gmp::Unbound unbound(w, max_frame_count);
    unbound.start();
    bse::TaskScheduler::get()->wait_until_idle();
  }
  bse::TaskScheduler::get()->wait_until_idle();

  // Sleep(10000);
  gfx::Font::destroy();
  cor::ScriptType::destroy();
  bse::Edit::destroy();
  scr::detail::State::destroy();
  bse::TaskScheduler::destroy();  //
  phy::TriangleMeshCollisionShape::clear_mesh_shapes();
  //_CrtDumpMemoryLeaks();
#ifdef UNBOUND_CUSTOM_ALLOCATION
  report_memory_leaks();
  delete m_allocation_map;
  delete m_size_per_file;
#endif
}
