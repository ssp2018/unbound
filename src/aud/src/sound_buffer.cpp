#include "aud/sound_buffer.hpp"

#include "bse/error.hpp"
#include "bse/file_path.hpp"
#include "bse/result.hpp"
template class bse::Asset<aud::SoundBuffer>;

namespace aud {

bse::Result<SoundBuffer> SoundBuffer::load(bse::FilePath path) {
  SNDFILE* sound_file = nullptr;
  SF_INFO sound_info;
  sound_file = sf_open(path.get_string().c_str(), SFM_READ, &sound_info);

  if (sound_info.channels != 1 || sound_info.format != 2097248)
    return bse::Error() << "File failed to load (" << path << ")";

  float* sound_data_flt = nullptr;
  int16_t* sound_data_int = nullptr;

  sound_data_flt = (float*)malloc(sizeof(float) * (unsigned int)sound_info.frames);
  sound_data_int = (int16_t*)malloc(sizeof(int16_t) * (unsigned int)sound_info.frames);
  sf_read_float(sound_file, sound_data_flt, sound_info.frames);

  // Formating data to a range between -32767 to 32767 (actual range is -32768 to 32767 but who
  // cares...)
  for (sf_count_t i = 0; i < sound_info.frames; i++)
    sound_data_int[i] = int16_t(round(sound_data_flt[i] * 32767.f));

  free(sound_data_flt);
  sf_close(sound_file);

  SoundBuffer sound;
  sound.m_length = (unsigned int)sound_info.frames;
  sound.m_sample_rate = (unsigned int)sound_info.samplerate;
  sound.m_valid = true;

  alGenBuffers(1, &sound.m_sound_buffer);

  alBufferData(sound.m_sound_buffer, AL_FORMAT_MONO16, sound_data_int, sound_info.frames * 2,
               (ALsizei)sound_info.samplerate);

  free(sound_data_int);

  return std::move(sound);
}

SoundBuffer::SoundBuffer() {
  m_length = 0;
  m_sample_rate = 0;
  m_sound_buffer = 0;
}

SoundBuffer::~SoundBuffer() { destroy(); }

SoundBuffer::SoundBuffer(SoundBuffer&& other) {
  m_length = other.m_length;
  m_sample_rate = other.m_sample_rate;
  m_sound_buffer = other.m_sound_buffer;
  m_valid = other.m_valid;

  other.m_length = 0;
  other.m_sample_rate = 0;
  other.m_sound_buffer = 0;
  other.m_valid = false;
}

SoundBuffer& SoundBuffer::operator=(SoundBuffer&& other) {
  if (this != &other) {
    destroy();

    m_length = other.m_length;
    m_sample_rate = other.m_sample_rate;
    m_sound_buffer = other.m_sound_buffer;
    m_valid = other.m_valid;

    other.m_length = 0;
    other.m_sample_rate = 0;
    other.m_sound_buffer = 0;
    other.m_valid = false;
  }
  return *this;
}

int SoundBuffer::get_length() const { return m_length; }

int SoundBuffer::get_size_in_bytes() const {
  int tot;
  alGetBufferi(m_sound_buffer, AL_SIZE, &tot);
  return tot;
}

int SoundBuffer::get_sample_rate() const { return m_sample_rate; }

ALuint SoundBuffer::get_buffer_index() const { return m_sound_buffer; }

float SoundBuffer::get_buffer_time_length() const { return (float)m_length / (float)m_sample_rate; }

bool SoundBuffer::is_valid() const { return m_valid > 0 ? true : false; }

void SoundBuffer::destroy() { alDeleteBuffers(1, &m_sound_buffer); }

}  // namespace aud