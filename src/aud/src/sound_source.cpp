#include "aud/sound_source.hpp"

#include "aud/sound_buffer.hpp"
#include "bse/log.hpp"

namespace aud {

SoundSource::SoundSource() {
  alGenSources((ALuint)1, &m_source);
  set_hearing_distance(120);
}

SoundSource::SoundSource(bse::Asset<SoundBuffer> sound_buffer, SoundSettings settings) {
  alGenSources((ALuint)1, &m_source);
  attach_buffer(sound_buffer);

  set_source_val(PITCH, settings.pitch);
  set_source_val(GAIN, settings.gain);
  set_source_val(MIN_GAIN, settings.gain_min);
  set_source_val(MAX_GAIN, settings.gain_max);
  set_source_val(ROLLOFF_FACTOR, settings.rolloff_factor);
  set_source_val(MAX_DISTANCE, settings.max_distance);
  set_looping(settings.loop);

  set_hearing_distance(120);
}

SoundSource::~SoundSource() { destroy(); }

SoundSource::SoundSource(SoundSource&& other) {
  m_attached_buffer = std::move(other.m_attached_buffer);

  m_source = other.m_source;

  m_valid = other.m_valid;

  set_source_val(GAIN, other.get_source_val(GAIN));

  set_source_val(PITCH, other.get_source_val(PITCH));

  set_source_val(MIN_GAIN, other.get_source_val(MIN_GAIN));

  set_source_val(MAX_GAIN, other.get_source_val(MAX_GAIN));

  set_source_val(ROLLOFF_FACTOR, other.get_source_val(ROLLOFF_FACTOR));

  set_source_val(MAX_DISTANCE, other.get_source_val(MAX_DISTANCE));

  set_looping(other.get_looping());

  set_looping(other.get_looping());

  set_position(other.get_position());

  set_velocity(other.get_velocity());

  set_hearing_distance(other.get_hearing_distance());

  other.m_source = 0;

  other.m_valid = false;
}

SoundSource& SoundSource::operator=(SoundSource&& other) {
  if (this != &other) {
    destroy();

    m_attached_buffer = std::move(other.m_attached_buffer);

    m_source = other.m_source;

    m_valid = other.m_valid;

    set_source_val(GAIN, other.get_source_val(GAIN));

    set_source_val(PITCH, other.get_source_val(PITCH));

    set_source_val(MIN_GAIN, other.get_source_val(MIN_GAIN));

    set_source_val(MAX_GAIN, other.get_source_val(MAX_GAIN));

    set_source_val(ROLLOFF_FACTOR, other.get_source_val(ROLLOFF_FACTOR));

    set_source_val(MAX_DISTANCE, other.get_source_val(MAX_DISTANCE));

    set_looping(other.get_looping());

    set_position(other.get_position());

    set_velocity(other.get_velocity());

    set_hearing_distance(other.get_hearing_distance());

    other.m_source = 0;

    other.m_valid = false;
  }
  return *this;
}

bool SoundSource::attach_buffer(bse::Asset<SoundBuffer> sound_asset) {
  if (sound_asset.is_valid()) {
    m_attached_buffer = sound_asset;

    m_valid = true;
    // attaches the buffer to the source in openAL
    alSourcei(m_source, AL_BUFFER, m_attached_buffer->get_buffer_index());
    return true;
  } else {
    LOG(WARNING) << "The requested sound buffer pointer to attach was null";
    return false;
  }
}

void SoundSource::detach_buffer() { alSourcei(m_source, AL_BUFFER, NULL); }

const bse::Asset<SoundBuffer>& SoundSource::get_buffer() const { return m_attached_buffer; }

bool SoundSource::is_valid() const { return m_valid; }

void SoundSource::play(bool var) {
  if (var) {
    if (!is_playing()) alSourcePlay(m_source);
  } else {
    if (is_playing()) alSourceStop(m_source);
  }
}

bool SoundSource::is_playing() const {
  // Check if this has a sound buffer attached
  if (!is_valid()) {
    return false;
  }
  ALint val;
  // get status of source and save it in "val"
  alGetSourcei(m_source, AL_SOURCE_STATE, &val);

  if (val == AL_PLAYING)
    return true;
  else
    return false;
}

void SoundSource::apply_sound_settings(SoundSettings settings) {
  set_source_val(GAIN, settings.gain);

  set_source_val(PITCH, settings.pitch);

  set_source_val(MIN_GAIN, settings.gain_min);

  set_source_val(MAX_GAIN, settings.gain_max);

  set_source_val(ROLLOFF_FACTOR, settings.rolloff_factor);

  set_source_val(MAX_DISTANCE, settings.max_distance);

  set_looping(settings.loop);
}

void SoundSource::set_position(glm::vec3 pos) {
  alSource3f(m_source, AL_POSITION, pos[0], pos[2], pos[1]);
}

void SoundSource::set_velocity(glm::vec3 vel) {
  alSource3f(m_source, AL_VELOCITY, vel[0], vel[2], vel[1]);
}

void SoundSource::set_source_val(SoundSetting setting, float val) {
  switch (setting) {
    case PITCH:
      alSourcef(m_source, AL_PITCH, val);
      break;
    case GAIN:
      alSourcef(m_source, AL_GAIN, val);
      break;
    case MIN_GAIN:
      alSourcef(m_source, AL_MIN_GAIN, val);
      break;
    case MAX_GAIN:
      alSourcef(m_source, AL_MAX_GAIN, val);
      break;
    case ROLLOFF_FACTOR:
      alSourcef(m_source, AL_ROLLOFF_FACTOR, val);
      break;
    case MAX_DISTANCE:
      alSourcef(m_source, AL_MAX_DISTANCE, val);
      break;
    default:
      LOG(WARNING) << "Tried to set unrecognized sound source setting.";
      break;
  }
  // LOG(NOTICE) << "Audio manager status: " + std::string(alGetString(alGetError()) + '\n');
}

void SoundSource::set_looping(bool l) {
  if (l)
    alSourcei(m_source, AL_LOOPING, AL_TRUE);
  else
    alSourcei(m_source, AL_LOOPING, AL_FALSE);
}

void SoundSource::set_type(SoundType type) { m_type = type; }

void SoundSource::set_offset(float offset) {
  //
  if (m_valid) {
    auto buffer_size_in_bytes = m_attached_buffer->get_size_in_bytes();
    alSourcei(m_source, AL_BYTE_OFFSET, offset * buffer_size_in_bytes);
  }
}

void SoundSource::set_hearing_distance(float dist) { m_hearing_distance = dist; }

float SoundSource::get_time_left() const {
  int offset;
  alGetSourcei(m_source, AL_SAMPLE_OFFSET, &offset);

  return (float)(m_attached_buffer->get_length() - offset) /
         (float)m_attached_buffer->get_sample_rate();
}

// void SoundSource::set_hearing_distance(float dist) { m_hearing_distance = dist; }

glm::vec3 SoundSource::get_position() const {
  glm::vec3 result;
  alGetSource3f(m_source, AL_POSITION, &result[0], &result[2], &result[1]);
  return result;
}

glm::vec3 SoundSource::get_velocity() const {
  glm::vec3 result;
  alGetSource3f(m_source, AL_VELOCITY, &result[0], &result[2], &result[1]);
  return result;
}

float SoundSource::get_source_val(SoundSetting setting) const {
  float buffer = 0.f;
  switch (setting) {
    case PITCH:
      alGetSourcef(m_source, AL_PITCH, &buffer);
      break;
    case GAIN:
      alGetSourcef(m_source, AL_GAIN, &buffer);
      break;
    case MIN_GAIN:
      alGetSourcef(m_source, AL_MIN_GAIN, &buffer);
      break;
    case MAX_GAIN:
      alGetSourcef(m_source, AL_MAX_GAIN, &buffer);
      break;
    case ROLLOFF_FACTOR:
      alGetSourcef(m_source, AL_ROLLOFF_FACTOR, &buffer);
      break;
    case MAX_DISTANCE:
      alGetSourcef(m_source, AL_MAX_DISTANCE, &buffer);
      break;
    default:
      LOG(WARNING) << "Tried to set unrecognized sound source setting.";
      break;
  }
  return buffer;
}

bool SoundSource::get_looping() const {
  ALint val;
  alGetSourcei(m_source, AL_LOOPING, &val);
  return (bool)val;
}

SoundType SoundSource::get_type() const { return m_type; }

float SoundSource::get_hearing_distance() const { return m_hearing_distance; }

float SoundSource::get_offset() const {
  //
  if (m_valid) {
    auto buffer_size_in_bytes = m_attached_buffer->get_size_in_bytes();
    ALint byte_offset = 0;
    alGetSourcei(m_source, AL_BYTE_OFFSET, &byte_offset);
    return (float)(byte_offset) / buffer_size_in_bytes;
  }
  return 0.f;
}

std::unique_ptr<SoundSource::Intermediate> SoundSource::store() const {
  //
  auto intermediate = std::make_unique<SoundSource::Intermediate>();
  intermediate->pos = get_position();
  intermediate->vel = get_velocity();
  intermediate->settings.loop = get_looping();
  intermediate->settings.pitch = get_source_val(SoundSetting::PITCH);
  intermediate->settings.gain = get_source_val(SoundSetting::GAIN);
  intermediate->settings.gain_min = get_source_val(SoundSetting::MIN_GAIN);
  intermediate->settings.gain_max = get_source_val(SoundSetting::MAX_GAIN);
  intermediate->settings.rolloff_factor = get_source_val(SoundSetting::ROLLOFF_FACTOR);
  intermediate->settings.max_distance = get_source_val(SoundSetting::MAX_DISTANCE);
  intermediate->attached_buffer = m_attached_buffer;
  intermediate->type = m_type;
  // intermediate->hearing_distance = m_hearing_distance;
  intermediate->was_playing = is_playing();
  if (is_playing()) {
    intermediate->playing_offset = get_offset();
  } else {
    intermediate->playing_offset = 0.f;
  }
  return intermediate;
}

void SoundSource::restore(Intermediate& intermediate) {
  //
  *this = std::move(SoundSource{intermediate.attached_buffer, intermediate.settings});
  this->set_type(intermediate.type);
  this->set_position(intermediate.pos);
  this->set_velocity(intermediate.vel);
  // this->set_hearing_distance(intermediate.hearing_distance);
  if (intermediate.was_playing) {
    this->set_offset(intermediate.playing_offset);
    this->play(true);
  } else {
  }
}

void SoundSource::destroy() {
  if (m_source) {
    if (is_valid()) play(false);
    alDeleteSources(1, &m_source);
  }
}
}  // namespace aud