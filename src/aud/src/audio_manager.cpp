#include "aud/audio_manager.hpp"

#include "aud/sound_source.hpp"

namespace aud {
class SoundBuffer;
}
namespace aud {

AudioManager::AudioManager() {}

AudioManager::~AudioManager() {
  alcMakeContextCurrent(NULL);
  alcDestroyContext(m_context);
  alcCloseDevice(m_device);
}

void AudioManager::set_listener_forward(glm::vec3 cam_forward) {
  ALfloat listenerOrientation[6];
  alGetListenerfv(AL_ORIENTATION, listenerOrientation);

  listenerOrientation[0] = cam_forward[0];
  listenerOrientation[1] = cam_forward[2];
  listenerOrientation[2] = cam_forward[1];

  alListenerfv(AL_ORIENTATION, listenerOrientation);
}

void AudioManager::set_listener_up(glm::vec3 cam_up) {
  ALfloat listenerOrientation[6];
  alGetListenerfv(AL_ORIENTATION, listenerOrientation);

  listenerOrientation[3] = cam_up[0];
  listenerOrientation[4] = cam_up[2];
  listenerOrientation[5] = cam_up[1];

  alListenerfv(AL_ORIENTATION, listenerOrientation);
}

void AudioManager::set_listener_position(glm::vec3 pos) {
  alListener3f(AL_POSITION, pos[0], pos[2], pos[1]);
}

void AudioManager::set_listener_velocity(glm::vec3 vel) {
  alListener3f(AL_VELOCITY, vel[0], vel[2], vel[1]);
}

void AudioManager::set_listener_gain(float val) {
  //
  alListenerf(AL_GAIN, val);
}

void AudioManager::set_listener_pitch(float val) { alListenerf(AL_PITCH, val); }

glm::vec3 AudioManager::get_listener_forward() const {
  ALfloat listenerOrientation[6];
  alGetListenerfv(AL_ORIENTATION, listenerOrientation);

  glm::vec3 cam_forward;

  cam_forward[0] = listenerOrientation[0];
  cam_forward[2] = listenerOrientation[1];
  cam_forward[1] = listenerOrientation[2];

  return cam_forward;
}

glm::vec3 AudioManager::get_listener_up() const {
  ALfloat listenerOrientation[6];
  alGetListenerfv(AL_ORIENTATION, listenerOrientation);

  glm::vec3 cam_up;

  cam_up[0] = listenerOrientation[3];
  cam_up[2] = listenerOrientation[4];
  cam_up[1] = listenerOrientation[5];

  return cam_up;
}

glm::vec3 AudioManager::get_listener_position() const {
  glm::vec3 pos;
  alGetListenerfv(AL_POSITION, &pos[0]);
  return pos;
}

glm::vec3 AudioManager::get_listener_velocity() const {
  glm::vec3 vel;
  alGetListenerfv(AL_VELOCITY, &vel[0]);
  return vel;
}

float AudioManager::get_listener_gain() const {
  //
  float val;
  alGetListenerf(AL_GAIN, &val);
  return val;
}

void AudioManager::change_listener_gain(float val) {
  ALfloat gain;
  alGetListenerf(AL_GAIN, &gain);
  gain += val;
  gain = std::min(std::max(0.f, (float)gain), 1.f);
  alListenerf(AL_GAIN, gain);
}

void AudioManager::add_to_group(std::string group, std::string name, bse::FilePath path) {
  // If the sound group exists
  if (m_sound_groups.find(group) != m_sound_groups.end()) {
    // If the name is not used
    if (m_sound_groups[group].find(name) == m_sound_groups[group].end()) {
      // add the sound data to the sound group
      m_sound_groups[group][name] = bse::Asset<aud::SoundBuffer>(path);
    } else {
      LOG(WARNING) << "The name: " << name << ", is already being used in the sound group:" << group
                   << "\n";
    }
  } else {
    LOG(WARNING) << "There does not exist a sound group with the name: " << group << "\n";
  }
}

void AudioManager::add_sound_group(
    std::string group) {  // check if the periodic sound group already exists
  if (m_sound_groups.find(group) == m_sound_groups.end()) {
    // add a empty periodic sound group
    m_sound_groups[group] = aud::SoundGroup();
  } else {
    LOG(WARNING) << "There already exists a sound group with the name: " << group << "\n";
  }
}

std::optional<bse::Asset<aud::SoundBuffer>> AudioManager::get_sound_from_group(std::string group,
                                                                               std::string name) {
  // If the sound group exists
  if (m_sound_groups.find(group) != m_sound_groups.end()) {
    // If the name exists
    if (m_sound_groups[group].find(name) != m_sound_groups[group].end()) {
      // return a reference to the sound group item
      return m_sound_groups[group][name];
    } else {
      LOG(WARNING) << "The name: " << name << ", does not exist in the sound group:" << group
                   << "\n";
      return std::nullopt;
    }
  } else {
    LOG(WARNING) << "There does not exist a sound group with the name: " << group << "\n";
    return std::nullopt;
  }
}

std::string AudioManager::get_random_sound_from_group(
    std::string group) {  // If the sound group exists
  if (m_sound_groups.find(group) != m_sound_groups.end()) {
    auto num = std::rand() % m_sound_groups[group].size();
    auto it = std::next(m_sound_groups[group].begin(), std::rand() % m_sound_groups[group].size());
    return it->first;
  } else {
    LOG(WARNING) << "There does not exist a sound group with the name: " << group << "\n";
    return nullptr;
  }
}

bse::RuntimeAsset<SoundSource> AudioManager::create_sound_source(
    bse::Asset<SoundBuffer> sound_buffer, SoundSettings& settings) {
  return std::move(bse::RuntimeAsset<SoundSource>(sound_buffer, settings));
}

std::string AudioManager::check_for_error() {
  std::string str = "";
  str += "Audio manager status: " + std::string(alGetString(alGetError()) + '\n');
  return str;
}

void AudioManager::init_device() {
  if (m_device = alcOpenDevice(NULL))
    if (m_context = alcCreateContext(m_device, NULL))
      if (alcMakeContextCurrent(m_context)) m_valid = true;

  alDistanceModel(AL_INVERSE_DISTANCE_CLAMPED);

  ALfloat listenerOrientation[] = {0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f};
  alListenerfv(AL_ORIENTATION, listenerOrientation);
  alListener3f(AL_POSITION, 0.0f, 0.0f, 0.0f);
  alListener3f(AL_VELOCITY, 0.0f, 0.0f, 0.0f);
}

}  // namespace aud