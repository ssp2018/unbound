#pragma once

#ifndef AUD_AUDIO_MANAGER_HPP
#define AUD_AUDIO_MANAGER_HPP

#include "bse/asset.hpp"
#include <aud/sound_source.hpp>
#include <bse/serialize.hpp>
#include <optional>
namespace aud {

class SoundBuffer;
}  // namespace aud

namespace aud {

using SoundGroup = std::map<std::string, bse::Asset<aud::SoundBuffer>>;

// Class that handles loading and playing sounds
class AudioManager {
 public:
  AudioManager();
  ~AudioManager();

  // Define where the listener is looking. If not called the program defaults to
  // (0.0f, -1.0f, 0.0f)
  void set_listener_forward(glm::vec3 camLooking);

  // Define what the listeners up direction is. If not called the program defaults to (0.0f,
  // 0.0f, 1.0f)
  void set_listener_up(glm::vec3 camUp);

  // Set the listeners position. If not called the program defaults to a position of (0.0f, 0.0f,
  // 0.0f)
  void set_listener_position(glm::vec3 pos);

  // Set the listeners velocity  If not called the program defaults to a velocity of (0.0f, 0.0f,
  // 0.0f)
  void set_listener_velocity(glm::vec3 vel);

  // Set the listeners gain
  void set_listener_gain(float val);

  // Set the listener pitch
  void set_listener_pitch(float val);

  // Get where the listener is looking.
  glm::vec3 get_listener_forward() const;

  // Get what the listeners up direction is.
  glm::vec3 get_listener_up() const;

  // Get the listeners position.
  glm::vec3 get_listener_position() const;

  // Get the listeners velocity
  glm::vec3 get_listener_velocity() const;

  // Get the listeners gain
  float get_listener_gain() const;

  // increases or decreases the current gain
  void change_listener_gain(float val);

  // Simple check to see if the sound system is working
  operator bool() { return m_valid; }

  // adds a sound to a non looping sound group
  void add_to_group(std::string group, std::string name, bse::FilePath path);

  // adds a new non looping sound group
  void add_sound_group(std::string group);

  // gets a certain sound from a certain group, if both exists
  std::optional<bse::Asset<aud::SoundBuffer>> get_sound_from_group(std::string group,
                                                                   std::string name);

  // Retrieves a random sound in the sound group
  std::string get_random_sound_from_group(std::string group);

  // Adds a new sound source with the given parameters
  bse::RuntimeAsset<SoundSource> create_sound_source(bse::Asset<SoundBuffer> sound_buffer,
                                                     SoundSettings& settings);

  // Outputs a very nice string telling you if something went wrong
  std::string check_for_error();

  CLASS_SERFUN((AudioManager)) {
    SERIALIZE(self.get_listener_forward());
    SERIALIZE(self.get_listener_up());
    SERIALIZE(self.get_listener_position());
    SERIALIZE(self.get_listener_velocity());
    SERIALIZE(self.get_listener_gain());
  }
  CLASS_DESERFUN((AudioManager)) {
    glm::vec3 v;
    DESERIALIZE(v);
    self.set_listener_forward(v);
    DESERIALIZE(v);
    self.set_listener_up(v);
    DESERIALIZE(v);
    self.set_listener_position(v);
    DESERIALIZE(v);
    self.set_listener_velocity(v);
    float fv;
    DESERIALIZE(fv);
    self.set_listener_gain(fv);
  }

  // creates a device, must only be called once.
  void init_device();

  // used for storing a set of sounds to be played whenever wanted.
  std::unordered_map<std::string, SoundGroup> m_sound_groups;

 private:
  ALCdevice* m_device;
  ALCcontext* m_context;
  bool m_valid = false;
};

}  // namespace aud

#endif