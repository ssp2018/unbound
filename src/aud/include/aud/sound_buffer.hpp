#ifndef AUD_SOUND_BUFFER_HPP
#define AUD_SOUND_BUFFER_HPP

#include <bse/asset.hpp>
#include <bse/file_path.hpp>
#include <bse/result.hpp>

namespace aud {

// Asset that holds a sound buffer
// Sound only support one channel (mono)
// ONLY supports loading .ogg files with one channel
class SoundBuffer {
 public:
  // Loads a sound file
  static bse::Result<SoundBuffer> load(bse::FilePath path);

  SoundBuffer();
  ~SoundBuffer();

  SoundBuffer(const SoundBuffer& other) = delete;
  SoundBuffer(SoundBuffer&& other);  // Move constructor

  SoundBuffer& operator=(const SoundBuffer& other) = delete;
  SoundBuffer& operator=(SoundBuffer&& other);  // Move assignment

  // Returns the length of the audio clip
  int get_length() const;

  // Returns the size in bytes of the sample
  int get_size_in_bytes() const;

  // Returns the sample rate of the audio clip
  int get_sample_rate() const;

  // Returns the buffer index that is used by openAL to reference the buffer
  ALuint get_buffer_index() const;

  // Returns the length in time of the sound
  float get_buffer_time_length() const;

  // Checks if the buffer has data
  bool is_valid() const;

 private:
  // Helper function that destroys the instance data
  void destroy();

  bool m_valid = false;
  int m_length;
  int m_sample_rate;
  ALuint m_sound_buffer = 0;
};
}  // namespace aud

extern template class bse::Asset<aud::SoundBuffer>;

#endif