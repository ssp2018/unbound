#ifndef AUD_SOUND_SOURCE_HPP
#define AUD_SOUND_SOURCE_HPP

#include "bse/asset.hpp"
#include <aud/sound_buffer.hpp>
#include <bse/runtime_asset.hpp>
#include <ext/ext.hpp>

namespace aud {
class SoundBuffer;
}

namespace aud {

struct SoundSettings {
  SoundSettings() {}
  bool loop = false;
  float pitch = 1.f;             // [0, 1]
  float gain = 1.f;              // [0, 1]
  float gain_min = 0.f;          // [0, 1]
  float gain_max = 1.f;          // [0, 1]
  float rolloff_factor = 1.f;    // [0, infinity]
  float max_distance = FLT_MAX;  // [0, infinity]

  // Might be trivial...
  // CLASS_SERFUN((SoundSettings)) {
  //   SERIALIZE(self.pos);
  //   SERIALIZE(self.vel);
  //   SERIALIZE(self.loop);
  //   SERIALIZE(self.pitch);
  //   SERIALIZE(self.gain);
  //   SERIALIZE(self.gain_min);
  //   SERIALIZE(self.gain_max);
  //   SERIALIZE(self.rolloff_factor);
  //   SERIALIZE(self.max_distance);
  // }
  // CLASS_DESERFUN((SoundSettings)) {
  //   DESERIALIZE(self.pos);
  //   DESERIALIZE(self.vel);
  //   DESERIALIZE(self.loop);
  //   DESERIALIZE(self.pitch);
  //   DESERIALIZE(self.gain);
  //   DESERIALIZE(self.gain_min);
  //   DESERIALIZE(self.gain_max);
  //   DESERIALIZE(self.rolloff_factor);
  //   DESERIALIZE(self.max_distance);
  // }
};
// different settings for a sound
enum SoundSetting { PITCH, GAIN, MIN_GAIN, MAX_GAIN, ROLLOFF_FACTOR, MAX_DISTANCE };
// different types of sound, triggers different behaviour from AI
enum SoundType { AMBIENT, PLAYER_SOUND, GRUNT_SOUND, ANIMAL_SOUND, DEMON_SOUND };

// The class wraps a sound source in openAL
class SoundSource {
 public:
  ~SoundSource();

  SoundSource(const SoundSource& other) = delete;

  SoundSource& operator=(const SoundSource& other) = delete;
  SoundSource& operator=(SoundSource&& other);  // Move assignment

  // Attaches a sound buffer to the source
  bool attach_buffer(bse::Asset<SoundBuffer> sound_asset);

  // Detaches a sound buffer from the source
  void detach_buffer();

  // Returns the attached sound buffer resource
  const bse::Asset<SoundBuffer>& get_buffer() const;

  // Checks if there is a buffer attached to the source
  bool is_valid() const;

  // Either makes the source play or stop its sound, if it has a buffer attached
  void play(bool var);

  // Checks if the source is playing a sound
  bool is_playing() const;

  // applies all of the sound settings to the source
  void apply_sound_settings(SoundSettings settings);

  // sets the position of the source in openAL, defaults to (0.f, 0.f, 0.f).
  void set_position(glm::vec3 pos);

  // sets the velocity of the source in openAL, defaults to (0.f, 0.f, 0.f).
  void set_velocity(glm::vec3 vel);

  // sets various settings for a source
  void set_source_val(SoundSetting setting, float val);

  // enables looping or not of the source in openAL, defaults to true
  void set_looping(bool l);

  // get the type of sound (ambient, animal...)
  void set_type(SoundType type);

  // gets the current offset into the buffer
  void set_offset(float offset);

  // set the hearing range of the sound
  void set_hearing_distance(float dist);

  // get position from openAL
  glm::vec3 get_position() const;

  // get velocity from openAL
  glm::vec3 get_velocity() const;

  // gets various settings from a source
  float get_source_val(SoundSetting setting) const;

  // get if looping from openAL
  bool get_looping() const;

  // get the type of sound (ambient, animal...)
  SoundType get_type() const;

  // get the hearing range of the sound
  float get_hearing_distance() const;

  // gets the current offset into the buffer
  float get_offset() const;

  // returns the time untill the audio clip ends
  float get_time_left() const;

 private:
  struct Intermediate {
    SoundSettings settings;
    glm::vec3 pos;
    glm::vec3 vel;
    bse::Asset<SoundBuffer> attached_buffer;
    SoundType type;
    // float hearing_distance;
    bool was_playing;
    float playing_offset;

    CLASS_SERFUN((Intermediate)) {
      SERIALIZE(self.settings);
      SERIALIZE(self.pos);
      SERIALIZE(self.vel);
      SERIALIZE(self.attached_buffer);
      SERIALIZE(self.type);
      // SERIALIZE(self.hearing_distance);
      SERIALIZE(self.was_playing);
      SERIALIZE(self.playing_offset);
    }
    CLASS_DESERFUN((Intermediate)) {
      DESERIALIZE(self.settings);
      DESERIALIZE(self.pos);
      DESERIALIZE(self.vel);
      DESERIALIZE(self.attached_buffer);
      DESERIALIZE(self.type);
      // DESERIALIZE(self.hearing_distance);
      DESERIALIZE(self.was_playing);
      DESERIALIZE(self.playing_offset);
    }
  };

 private:
  SoundSource();

  // Sets the position, velocity, pitch, gain and looping for the source
  SoundSource(bse::Asset<SoundBuffer> sound_buffer, SoundSettings settings);
  SoundSource(SoundSource&& other);  // Move constructor

  std::unique_ptr<Intermediate> store() const;
  void restore(Intermediate& intermediate);

 private:
  bool m_valid = false;
  ALuint m_source = 0;
  bse::Asset<SoundBuffer> m_attached_buffer;

  SoundType m_type;
  float m_hearing_distance;

  void destroy();

  friend class bse::RuntimeAsset<SoundSource>;
};
}  // namespace aud

#endif