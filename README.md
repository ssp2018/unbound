# Unbound

# Build instructions (Windows) **RECOMMENDED**
1.  Install external libraries by running ci/install-libs.ps1. 
	* This will clone vcpkg (package-manager) into `C:\programs\vcpkg`, set environment variables, and install the packages.
	* May require running as administrator in order to automatically set the environment variables.
		* The variables that should be set before continuing onto the following steps are: 
			* VCPKG_ROOT = "C:\programs\vcpkg"
			* VCPKG_DEFAULT_TRIPLET = "x64-windows"
	* This may take several minutes.
2.  Relog out of Windows.
3.  Open the root folder in Visual Studio. Visual Studio will detect the project as a CMake project.
	* Requires Visual Studio 2017 or later. If the default installation does not recognize CMake then you are most likely missing a required toolset. 
		* The functionality may be included in the linux toolset. Modify the installation to include it with the Visual Studio Installer.
4.  Run CMake > Generate.
5.  Run CMake > Build All.

## On failure
If Visual Studio is opened before any of the previous steps are completed it may cache an incorrect state, meaning
it won't work even if the previous steps are later corrected. To remove the cached variables, open the folder in Visual Studio
and access the following menu item:

CMake > Cache (x64-{your build configuration} only) > Delete Cache Folders > unbound (or CMakeLists.txt)

After this, you may run CMake > Generate again.

# Command line
There is also a build script used by CI in ci/build.bat, if you prefer a command line approach (Not as thoroughly tested).
