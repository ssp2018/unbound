#!/bin/bash


source ~/.profile

set -e

cxx_files=`find ../src -path ../src/ext -prune -o \( -name "*.cpp" -o -name "*.hpp" \) -print`

set +e
found_diff=0
for file in $cxx_files
do
  unformatted=`cat "$file"`
  formatted=`clang-format -style=file "$file"`
  
  diff -u --label "$file" <(echo "$unformatted") --label "$file" <(echo "$formatted") | ./diff2html.sh >> ../format_results.html

  if [ $? -eq 2 ]
  then
   exit 2
  elif [ $? -eq 1 ]
  then
    found_diff=1
  fi
done
set -e


cmake "../src"
make

if [ $found_diff -eq 1 ]
then
  exit 1
fi
