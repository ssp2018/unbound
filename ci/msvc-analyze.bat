call "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvars64.bat"


call cmake -E env CXXFLAGS="/analyze /WX-" cmake -DUNBOUND_NWERROR=" " -DCMAKE_BUILD_TYPE=Release -G Ninja -DCMAKE_TOOLCHAIN_FILE="%VCPKG_ROOT%\scripts\buildsystems\vcpkg.cmake" ../src || goto :error
call ninja || goto :error
call ninja test  || goto :error


:error
  exit /b %ERRORLEVEL%