cd ci

call "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvars64.bat"

call cmake -DCMAKE_BUILD_TYPE=Release -G Ninja -DCMAKE_TOOLCHAIN_FILE="%VCPKG_ROOT%\scripts\buildsystems\vcpkg.cmake" ../src || goto :error
call ninja || goto :error

cd ..
REM call C:\programs\start-interactive\start-interactive.exe "./ci/bin/unbound_client.exe -f replay_file" "." 15000 || goto :error

:error
  exit /b %ERRORLEVEL%



exit /b
