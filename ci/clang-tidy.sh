#!/bin/bash

set -e

CC=clang-6.0 CXX=clang++-6.0 cmake -DCMAKE_CXX_CLANG_TIDY="clang-tidy-6.0" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_PREFIX_PATH=/home/ssp2018devops/unbound/ext/install ../src
make