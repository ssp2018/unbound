##############################################################
# INSTALL VCPKG AND UNBOUND EXTLIBS (WINDOWS)
##############################################################
#
# 1. Start powershell as admin.
# 2. Copy paste the code below into powershell and run it.
# 3. Relog out of Windows.
#
##############################################################

mkdir -F C:\programs
cd C:\programs

git clone https://github.com/Microsoft/vcpkg vcpkg
cd vcpkg
git reset --hard 40510c3aafc3d96876aaa57e42e2bbac801b25d7
.\bootstrap-vcpkg.bat
.\vcpkg integrate install

[Environment]::SetEnvironmentVariable("VCPKG_ROOT", "C:\programs\vcpkg", [System.EnvironmentVariableTarget]::Machine)
[Environment]::SetEnvironmentVariable("VCPKG_DEFAULT_TRIPLET", "x64-windows", [System.EnvironmentVariableTarget]::Machine)

.\vcpkg install --triplet x64-windows glm glbinding gtest glfw3 sol2 lua openal-soft bullet3 assimp imgui freetype libsndfile