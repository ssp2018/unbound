#!/bin/bash


cxx_files=`find ../src -path ../src/ext -prune -o \( -name "*.cpp" -o -name "*.hpp" \) -print`


found_diff=0
for file in $cxx_files
do
  unformatted=`cat "$file"`
  formatted=`clang-format-6.0 -style=file "$file"`
  
  result=`diff -u --label "$file" <(echo "$unformatted") --label "$file" <(echo "$formatted")`
  
  if [ $? -eq 2 ]
  then
   exit 2
  elif [[ ! "$result" =~ ^[[:blank:]]*$ ]]
  then
    found_diff=1
    echo "[format] Unformatted file: '$file'"
    echo "$result" | ./diff2html.sh >> ../format_results.html
    
    if [ $? -ne 0 ]
    then
      exit $?
    fi
  fi
done


if [ $found_diff -eq 1 ]
then
  echo "[format] Found unformatted files. See uploaded results for details."
fi

exit $found_diff
