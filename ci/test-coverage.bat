call "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvars64.bat"

call cmake -DUNBOUND_TEST_WHOLEARCHIVE=" " -DCMAKE_BUILD_TYPE=Debug -G Ninja -DCMAKE_TOOLCHAIN_FILE="%VCPKG_ROOT%\scripts\buildsystems\vcpkg.cmake" ../src || goto :error
call ninja || goto :error

cd ..

@echo off
setlocal ENABLEDELAYEDEXPANSION

set layers=ai aud bse cor gfx gmp phy scr
for %%f in (%layers%) do C:\programs\opencppcoverage\opencppcoverage.exe --sources "%cd%\src\%%f" --modules "%cd%\ci\bin\unbound_%%f" --export_type binary:%%f_test.bin -- "%cd%\ci\bin\unbound_%%f_test.exe"  || goto :error

set inputs=
for %%f in (%layers%) do set inputs=!inputs! --input_coverage %%f_test.bin  || goto :error

C:\programs\opencppcoverage\opencppcoverage.exe %inputs% --export_type cobertura:test_coverage.xml  || goto :error

C:\programs\reportgenerator\ReportGenerator.exe -reports:test_coverage.xml -targetdir:test_coverage -reporttypes:Html;Badges || goto :error



:error
  exit /b %ERRORLEVEL%