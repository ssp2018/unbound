call "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvars64.bat"


call cmake -G "Visual Studio 15 2017 Win64" -DCMAKE_BUILD_TYPE=Debug ../src || goto :error
call C:\programs\cppcheck\cppcheck.exe --project=unbound.sln --enable=all --inconclusive --xml --xml-version=2 --output-file=cppcheck.xml --suppress=*:C:\programs\vcpkg\* || goto :error
mkdir ..\cppcheck
call python C:\programs\cppcheck\htmlreport\cppcheck-htmlreport --file=cppcheck.xml --report-dir=..\cppcheck --source-dir=%cd%\..\src || goto :error



:error
  exit /b %ERRORLEVEL%
