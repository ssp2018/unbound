cd ..\..\
if not exist unbound-persistent git clone https://gitlab.com/ssp2018/unbound unbound-persistent
cd unbound-persistent


git fetch
git reset --hard %CI_COMMIT_SHA%
del assets\model\*.SSPM

cd ci

call "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvars64.bat"

call cmake -DCMAKE_BUILD_TYPE=Debug -G Ninja -DCMAKE_TOOLCHAIN_FILE="%VCPKG_ROOT%\scripts\buildsystems\vcpkg.cmake" ../src || goto :error
call ninja || goto :error
call ninja test || goto :error

cd ..
call C:\programs\start-interactive\start-interactive.exe "./ci/bin/unbound_client.exe 120" "." 60000 || goto :error

:error
  exit /b %ERRORLEVEL%



exit /b